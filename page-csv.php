<?php get_header(); ?>

<main role="main">
	<!-- section -->
	<section>
		<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>

				<!-- article -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php
					$user_id	 = get_current_user_id();
					// ユーザーメタからお気に入りデータ取得
					$posts_liked = get_user_meta( $user_id, 'recipe_likes', true );
					if ( $posts_liked ) {
						$posts_liked = array_map( 'intval', explode( ',', $posts_liked ) );
					} else {
						$posts_liked = array( 0 );
					}

					$args[ 'post_type' ]		 = 'wprm_recipe';
					$args[ 'posts_per_page' ]	 = -1;
					$args[ 'post_status' ]		 = 'any';
					$recipe_query				 = new WP_Query( $args );
					?>
					"post_id","Parent Name","wprm_nutrition_calcium","wprm_nutrition_iron"<br>
					<?php
					if ( $recipe_query->have_posts() ):
						while ( $recipe_query->have_posts() ): $recipe_query->the_post();
							?>
							"<?php echo get_the_ID(); ?>","<?php echo get_the_title( get_post_meta( get_the_ID(), "wprm_parent_post_id", true ) ) ?>","<?php echo get_post_meta( get_the_ID(), "wprm_nutrition_calcium", true ) ?>","<?php echo get_post_meta( get_the_ID(), "wprm_nutrition_iron", true ) ?>",<br>
							<?php
						endwhile;
						wp_reset_postdata();
					endif;
					?>


				</article>
				<!-- /article -->

			<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h1><?php _e( 'Sorry, nothing to display.' ); ?></h1>

			</article>
			<!-- /article -->

		<?php endif; ?>

	</section>
	<!-- /section -->
</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>

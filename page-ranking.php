<?php get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
                <?php wp_breadcrumb() ?>
            </div>
        </div>
    </div>
    <section class="section recipe end">
        <div class="container">
            <div class="section-recipe--left fadeup2">
                <div class="sectionEP-head">
                    <div class="sectionEP-titleWrap">
                        <h1 class="sectionEP-title ranking">レシピランキング</h1>
                        <div class="sectionEP-head--order-wrap">
                            <p class="sectionEP-head--order-label pc-only">カテゴリを選ぶ</p>
                            <div class="sectionEP-newOrders">
                                <div class="form-search">
                                    <select class="input" name="g" onchange="document.location.href='?g='+this.options[this.selectedIndex].value;">
                                        <option value="-1">すべて</option>
                                        <?php
                                        $recipe_cats = get_terms('recipe_category', 'hide_empty=0');
                                        if ( ! empty( $recipe_cats ) && ! is_wp_error( $recipe_cats ) ){
                                            foreach ($recipe_cats as $recipe_cat) {
                                                printf("<option value=\"%s\" %s>%s</option>", $recipe_cat->name, $_GET['g'] == $recipe_cat->name ? 'selected="true"': '', $recipe_cat->name);
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="section-recipe--row">
                    <div class="section-recipe--pickupList">
                        <?php
                        $recipe_genre = "";
                        $genre = isset($_GET['g']) && -1 != $_GET['g']? $_GET['g'] : '';
                        $genre = sanitize_text_field($genre);
                        if($genre != "")
                            $recipe_genre = array(
                                'taxonomy' => 'recipe_category',
                                'field' => 'name',
                                'terms' => $genre,
                                'operator' => 'IN'
                            );
                        $conditions = array(
                            'relation' => 'AND',
                            $recipe_genre,
                        );
                        
                        $args = array('post_type' => 'recipe', 'posts_per_page' => 4, 'ignore_sticky_posts' => 1, 'tax_query' => $conditions);
                        $popular_recipe_list = get_posts_views_ranking( $range = 'daily', $args, 'WP_Query' );
                        if ($popular_recipe_list->have_posts()):
                            while ($popular_recipe_list->have_posts()) : $popular_recipe_list->the_post();
                                $recipe_cat = get_the_terms(get_the_ID(), 'recipe_category');
                                
                                $recipes = WPRM_Recipe_Manager::get_recipe_ids_from_post();
                                if(!empty($recipes))
                                    $recipe_id = $recipes[0];
                                else {
                                    $recipe_id = preg_replace('/[^0-9]/', '', get_field('recipe_desc'));
                                }
                                $recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );
                                $recipe_pos = get_field('recipe_display');
                                ?>

                                <div class="section-recipe--pickup">
                                    <div class="section-recipe--pickup-inner">
                                         <div class="section-recipe--pickup-thumb link ranking">
                                            <?php if (!is_user_logged_in() && $recipe_pos === 'is_not_member') { ?>
                                                <a href="javascript:void:(0)" data-remodal-target="modal-clip">
                                                    <?php if (has_post_thumbnail()) : 
                                                        the_post_thumbnail('medium');
                                                    else: 
                                                        echo do_shortcode('[wprm-recipe-image size=\'medium\']');
                                                    endif;?>
                                                </a>
                                            <?php } else { ?>
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php if (has_post_thumbnail()) : 
                                                        the_post_thumbnail('medium');
                                                    else: 
                                                        echo do_shortcode('[wprm-recipe-image size=\'medium\']');
                                                    endif;?>
                                                </a>
                                            <?php } ?>                                            
                                        </div>
                                        <div class="section-recipe--pickup-cnt">
                                            <?php if (!is_user_logged_in() && $recipe_pos === 'is_not_member') { ?>
                                                <div class="restrict-box">
                                                    <span class="label-orange">会員限定</span><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-lock-orange.svg" style="width: auto;">                                                                                                        
                                                </div>
                                            <?php } ?>
                                            <p class="date big"><?php the_time('Y.m.d'); ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d')))?>]</p>
                                            <h3 class="pickup-title big link">
                                                <?php if(!is_user_logged_in() && $recipe_pos === 'is_not_member') {?>
                                                    <a href="javascript:void:(0)" data-remodal-target="modal-clip"><?php the_title() ?></a>
                                                <?php } else {?>
                                                    <a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
                                                <?php }?>
                                            </h3>
                                            <div class="pc-only">
                                                <?php
                                                if ($recipe || $recipe->tags($key)) {
                                                    $terms_course = $recipe->tags( 'course' );
                                                    foreach ( $terms_course as $term_course ) {
                                                        printf("<span class=\"tag\">#%s</span>", $term_course->name);
                                                    }
                                                }
                                                ?>                                                
                                            </div>
                                            <div class="pickup-infor pc-only">                                                
                                                <span><?php echo esc_html( $recipe_cat[0]->name );?></span>
                                                <span class="time"><?php echo $recipe->prep_time() + $recipe->cook_time(); ?>分</span>
                                                <span class="kcal"><?php echo $recipe->calories();?>kcal</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sp-only">
                                        <?php
                                        if ($recipe || $recipe->tags($key)) {
                                            $terms_course = $recipe->tags( 'course' );
                                            foreach ( $terms_course as $term_course ) {
                                                printf("<span class=\"tag\">#%s</span>", $term_course->name);
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="pickup-infor sp-only">
                                        <span><?php echo esc_html( $recipe_cat[0]->name );?></span>
                                        <span class="time"><?php echo $recipe->prep_time() + $recipe->cook_time(); ?>分</span>
                                        <span class="kcal"><?php echo $recipe->calories();?>kcal</span>
                                    </div>
                                </div><!-- ./section-recipe--pickup -->
                            <?php
                            endwhile; 
                            wp_reset_query();
                        endif;?>
                                
                    </div><!-- ./section-recipe--pickupList -->
                    
                    <div class="pickupList recipe-ranking">
                        <?php
                        $j = 4;
                        $args = array('post_type' => 'recipe', 'posts_per_page' => 6, 'ignore_sticky_posts' => 1, 'offset' => 4, 'tax_query' => $conditions);
                        $popular_recipe_list = get_posts_views_ranking( $range = 'daily', $args, 'WP_Query' );
                        if ($popular_recipe_list->have_posts()):
                            while ($popular_recipe_list->have_posts()) : $popular_recipe_list->the_post();
                                $recipe_cat = get_the_terms(get_the_ID(), 'recipe_category');
                                $j++;
                                $recipes = WPRM_Recipe_Manager::get_recipe_ids_from_post();
                                if(!empty($recipes))
                                    $recipe_id = $recipes[0];
                                else {
                                    $recipe_id = preg_replace('/[^0-9]/', '', get_field('recipe_desc'));
                                }
                                $recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );
                                $recipe_pos = get_field('recipe_display');
                                ?>

                                <div class="pickupList-item">
                                    <?php if(!is_user_logged_in() && $recipe_pos === 'is_not_member') {?>
                                        <a href="javascript:void:(0)" data-remodal-target="modal-clip">
                                            <div class="pickupList-thumb" data-id="<?php echo $j;?>">
                                                <?php if (has_post_thumbnail()) : 
                                                    the_post_thumbnail('medium');
                                                else: 
                                                    echo do_shortcode('[wprm-recipe-image size=\'medium\']');
                                                endif;?>
                                            </div>
                                            <div class="pickupList-des">
                                                <div class="restrict-box">
                                                    <span class="label-orange">会員限定</span><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-lock-orange.svg" style="width: auto;">                                                                                                        
                                                </div>
                                                <h3 class="titleLv5"><?php the_title()?></h3>
                                                <p class="pickupList-infor"><?php echo esc_html( $recipe_cat[0]->name );?></p>
                                            </div>
                                        </a>
                                    <?php } else {?>
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="pickupList-thumb" data-id="<?php echo $j;?>">
                                                <?php if (has_post_thumbnail()) : 
                                                    the_post_thumbnail('medium');
                                                else: 
                                                    echo do_shortcode('[wprm-recipe-image size=\'medium\']');
                                                endif;?>
                                            </div>
                                            <div class="pickupList-des">
                                                <div class="restrict-box"></div>
                                                <h3 class="titleLv5"><?php the_title()?></h3>
                                                <p class="pickupList-infor"><?php echo esc_html( $recipe_cat[0]->name );?></p>
                                            </div>
                                        </a>                                    
                                    <?php }?>
                                </div><!-- ./section-recipe--pickupItem -->
                                                        
                            <?php
                            endwhile; 
                            wp_reset_query();
                        endif;?>
                                
                    </div><!-- ./pickupList -->
                </div><!-- ./section-recipe--row -->                
            </div><!-- ./section-recipe--left -->
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
                    <?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section><!--End .recipe-->
    
    <?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>

<?php get_footer(); ?>

<?php get_header(); ?>

<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
		<main class="main">
			<?php the_content(); ?>

			<section class="main-visual">
				<div class="container">
					<?php
					$banner_box	 = get_field( 'top_banner' );
					$banner_main = $banner_box[ 0 ][ 'pos_title' ];

					if ( has_post_thumbnail( $banner_main ) ) {
						$banner_thumb1 = get_the_post_thumbnail_url( $banner_main, 'large' );
					} else {
						if ( get_post_type( $banner_main ) === 'recipe' )
							$banner_thumb1	 = do_shortcode( '[wprm-recipe-image size=\'large\']' );
						else
							$banner_thumb1	 = get_template_directory_uri() . "/assets/images/no-img.jpg";
					}
					?>
					<div class="main-visual--left fadeup2">
						<div class="pickup">
                                                    <?php $recipe_pos = get_field('recipe_display', $banner_main);?>
                                                    <?php if(!is_user_logged_in() && $recipe_pos === 'is_not_member' && get_post_type( $banner_main ) === 'recipe') {?>
                                                        <a href="javascript:void:(0)" class="link" data-remodal-target="modal-clip">
                                                            <div class="pickup-thumb big">
                                                                <div class="pickup-thumb--img">
                                                                    <img src="<?php echo $banner_thumb1 ?>" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="pickup-cnt">
                                                                <h3 class="pickup-title big"><div class="restrict-box"><span class="label-orange">会員限定</span><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-lock-orange.svg" style="width: auto;"></div><?php echo get_the_title($banner_main) ?></h3>
                                                                <div class="pickup-dateWrap"><span class="date big"><?php echo get_the_date('Y.m.d', $banner_main) ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d', $banner_main))) ?>]</span><span class="label"><?php echo $banner_box[0]['label']; ?></span></div>
                                                            </div>
                                                        </a>
                                                    <?php } else {?>
                                                        <a href="<?php echo get_permalink($banner_main) ?>" class="link">
                                                            <div class="pickup-thumb big">
                                                                <div class="pickup-thumb--img">
                                                                    <img src="<?php echo $banner_thumb1 ?>" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="pickup-cnt">
                                                                <h3 class="pickup-title big"><?php echo get_the_title($banner_main) ?></h3>
                                                                <div class="pickup-dateWrap"><span class="date big"><?php echo get_the_date('Y.m.d', $banner_main) ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d', $banner_main))) ?>]</span><span class="label"><?php echo $banner_box[0]['label']; ?></span></div>
                                                            </div>
                                                        </a>
                                                    <?php }?>
						</div>
					</div>
					<div class="main-visual--right fadeup2">
						<?php
						array_shift( $banner_box );
						foreach ( $banner_box as $elm_child ):
							if ( has_post_thumbnail( $elm_child[ 'pos_title' ] ) ) {
								$banner_thumb2 = get_the_post_thumbnail_url( $elm_child[ 'pos_title' ], 'large' );
							} else {
								if ( get_post_type( $elm_child[ 'pos_title' ] ) === 'recipe' )
									$banner_thumb2	 = do_shortcode( '[wprm-recipe-image size=\'medium\']' );
								else
									$banner_thumb2	 = get_template_directory_uri() . "/assets/images/no-img.jpg";
							}
							?>
							<div class="pickup">
                                                            <?php $recipe_pos = get_field('recipe_display', $elm_child[ 'pos_title' ]);?>
                                                            <?php if(!is_user_logged_in() && $recipe_pos === 'is_not_member' && get_post_type( $elm_child[ 'pos_title' ] ) === 'recipe') {?>
                                                                <a href="javascript:void:(0)" class="link" data-remodal-target="modal-clip">
                                                                    <div class="pickup-thumb mvTop">
                                                                        <img src="<?php echo $banner_thumb2; ?>" alt="">
                                                                    </div>
                                                                    <div class="pickup-cnt">
                                                                        <h3 class="pickup-title"><?php echo get_the_title($elm_child['pos_title']) ?></h3>
                                                                        <div class="pickup-dateWrap"><span class="label-orange">会員限定</span><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-lock-orange.svg" style="width: auto;"><span class="date" style="margin-left: 8px;"><?php echo get_the_date('Y.m.d', $elm_child['pos_title']) ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d', $elm_child['pos_title']))) ?>]</span><br class="sp-only"><span class="label"><?php echo $elm_child['label']; ?></span></div>
                                                                    </div>
                                                                </a>
                                                            <?php } else {?>
                                                                <a href="<?php echo get_permalink($elm_child['pos_title']) ?>" class="link">
                                                                    <div class="pickup-thumb mvTop">
                                                                        <img src="<?php echo $banner_thumb2; ?>" alt="">
                                                                    </div>
                                                                    <div class="pickup-cnt">
                                                                        <h3 class="pickup-title"><?php echo get_the_title($elm_child['pos_title']) ?></h3>
                                                                        <div class="pickup-dateWrap"><span class="date"><?php echo get_the_date('Y.m.d', $elm_child['pos_title']) ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d', $elm_child['pos_title']))) ?>]</span><br class="sp-only"><span class="label"><?php echo $elm_child['label']; ?></span></div>
                                                                    </div>
                                                                </a>
                                                            <?php }?>

							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</section><!-- ./main visual -->
			<section class="section recipe">
				<div class="container">
					<div class="section-recipe--left">
						<div class="section-recipe--row fadeup">
							<h2 class="section-title">New!<span>新着レシピ</span></h2>
							<div class="section-recipe--pickupList">
								<?php
								$args_recipe[ 'post_type' ]		 = 'recipe';
								$args_recipe[ 'post_status' ]	 = 'publish';
								$args_recipe[ 'orderby' ]		 = 'date';
								$args_recipe[ 'order' ]			 = 'DESC';
								$args_recipe[ 'posts_per_page' ] = 5;

								$recipe_query	 = null;
								$recipe_query	 = new WP_Query( $args_recipe );

								if ( $recipe_query->have_posts() ):
									while ( $recipe_query->have_posts() ): $recipe_query->the_post();
										$recipe_cat = get_the_terms( get_the_ID(), 'recipe_category' );

										$recipes	 = WPRM_Recipe_Manager::get_recipe_ids_from_post();
										if ( !empty( $recipes ) )
											$recipe_id	 = $recipes[ 0 ];
										else {
											$recipe_id = preg_replace( '/[^0-9]/', '', get_field( 'recipe_desc' ) );
										}

										$recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );
										?>

										<div class="section-recipe--pickup">
											<div class="section-recipe--pickup-inner">
                                                                                            <?php $recipe_pos = get_field('recipe_display');?>
                                                                                                <div class="section-recipe--pickup-thumb">
                                                                                                    <?php if (!is_user_logged_in() && $recipe_pos === 'is_not_member') { ?>
                                                                                                        <a href="javascript:void:(0)" data-remodal-target="modal-clip" class="link">
                                                                                                        <?php
                                                                                                        if (has_post_thumbnail()) :
                                                                                                            the_post_thumbnail('medium');
                                                                                                        else:
                                                                                                            echo do_shortcode('[wprm-recipe-image size=\'medium\']');
                                                                                                        endif;
                                                                                                        ?>
                                                                                                        </a>
                                                                                                    <?php } else { ?>
                                                                                                        <a href="<?php the_permalink(); ?>" class="link">
                                                                                                            <?php
                                                                                                            if (has_post_thumbnail()) :
                                                                                                                the_post_thumbnail('medium');
                                                                                                            else:
                                                                                                                echo do_shortcode('[wprm-recipe-image size=\'medium\']');
                                                                                                            endif;
                                                                                                            ?>
                                                                                                        </a>
                                                                                                    <?php } ?>
                                                                                                </div>
												<div class="section-recipe--pickup-cnt">
                                                                                                    <?php if (!is_user_logged_in() && $recipe_pos === 'is_not_member') { ?>
                                                                                                        <div class="restrict-box">
                                                                                                            <span class="label-orange">会員限定</span><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-lock-orange.svg" style="width: auto;">                                                                                                        
                                                                                                        </div>
                                                                                                    <?php } ?>
													<p class="date big"><?php the_time( 'Y.m.d' ); ?> [<?php echo strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) ?>]</p>
													<h3 class="pickup-title big link">
                                                                                                            
                                                                                                            <?php if(!is_user_logged_in() && $recipe_pos === 'is_not_member') {?>
                                                                                                                <a href="javascript:void:(0)" data-remodal-target="modal-clip"><?php the_title() ?></a>
                                                                                                            <?php } else {?>
                                                                                                                <a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
                                                                                                            <?php }?>
                                                                                                        </h3>
													<div class="pc-only">
														<?php
														if ( $recipe || $recipe->tags( $key ) ) {
															$terms_course = $recipe->tags( 'course' );
															foreach ( $terms_course as $term_course ) {
																printf( "<span class=\"tag\">#%s</span>", $term_course->name );
															}
														}
														?>
													</div>
													<div class="pickup-infor pc-only">
														<span><?php echo esc_html( $recipe_cat[ 0 ]->name ); ?></span>
														<span class="time"><?php echo $recipe->prep_time() + $recipe->cook_time(); ?>分</span>
														<span class="kcal"><?php echo $recipe->calories(); ?>kcal</span>
													</div>
												</div>
											</div>
											<div class="sp-only">
												<?php
												if ( $recipe || $recipe->tags( $key ) ) {
													$terms_course = $recipe->tags( 'course' );
													foreach ( $terms_course as $term_course ) {
														printf( "<span class=\"tag\">#%s</span>", $term_course->name );
													}
												}
												?>
											</div>
											<div class="pickup-infor sp-only">
												<span><?php echo esc_html( $recipe_cat[ 0 ]->name ); ?></span>
												<span class="time"><?php echo $recipe->prep_time() + $recipe->cook_time(); ?>分</span>
												<span class="kcal"><?php echo $recipe->calories(); ?>kcal</span>
											</div>
										</div><!-- ./section-recipe--pickup -->

										<?php
									endwhile;
									wp_reset_postdata();
								endif;
								?>

							</div><!-- ./section-recipe--pickupList -->
							<div class="view-more-wrap">
								<a href="/recipe" class="btn-view-more">レシピ一覧へ</a>
							</div>
						</div><!-- ./section-recipe--row -->
						<div class="section-recipe--row fadeup">
							<h2 class="section-title orange">Selection<span>レシピ特集</span></h2>
							<div class="section-recipe--selectionList">
								<?php
								$blog_args2	 = array(
									'post_type'		 => 'blog',
									'post_status'	 => 'publish',
									'posts_per_page' => 3,
									'orderby'		 => 'rand',
									'tax_query'		 => array(
										array(
											'taxonomy'	 => 'blog_category',
											'field'		 => 'slug',
											'terms'		 => 'recipe-feature' // レシピ特集
										)
									)
								);
								$blog_query	 = null;
								$blog_query	 = new WP_Query( $blog_args2 );
								if ( $blog_query->have_posts() ) :
									while ( $blog_query->have_posts() ) : $blog_query->the_post();
										?>

										<div class="section-recipe--selection">
											<a href="<?php the_permalink() ?>" class="section-recipe--selection-inner link">
												<div class="section-recipe--selection-thumb">
													<?php if ( in_array( 'pr_status', get_field( 'pr_icon' ) ) ): ?>
														<span class="blog-item--thumb-label">PR</span>
													<?php endif; ?>
													<?php if ( has_post_thumbnail() ) : ?>
														<?php the_post_thumbnail( 'medium' ); ?>
													<?php else: ?>
														<img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-img.jpg" />
													<?php endif; ?>
												</div>
												<div class="section-recipe--selection-cnt pickup-title"><?php the_title() ?></div>
											</a>
										</div>

										<?php
									endwhile;
								endif;
								wp_reset_postdata();
								?>
							</div><!-- ./section-recipe--selectionList -->
						</div><!-- ./section-recipe--row -->
					</div><!-- ./section-recipe--left -->
					<div class="section-recipe--right fadeup">
						<div class="section-recipe--right-inner">
							<?php get_sidebar(); ?>
						</div>
					</div><!-- ./section-recipe--right -->
				</div>
			</section><!-- ./section-recipe -->
			<section class="section column fadeup">
				<div class="container">
					<h2 class="section-column--title">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/column_ttl.svg" alt="Vegan column">
					</h2>
					<p class="section-column--des">ヴィーガンの基礎知識からお野菜の豆知識まで<br class="sp-only">幅広い情報を発信！</p>
					<div class="tabs-listWrap">
						<div class="tabs-list js-tabsList">
							<a href="/blog" class="tabs-item active">すべて</a>
							<?php
							$blog_cats = get_terms( 'blog_category', 'hide_empty=0&number=9' );
							foreach ( $blog_cats as $blog_cat ) {
								$blog_link = get_term_link( $blog_cat, 'blog_category' );
								echo '<a href="' . $blog_link . '" class="tabs-item js-tabsItem" data-attr="' . $blog_cat->term_id . '">' . $blog_cat->name . '</a>';
							}
							?>
						</div>
					</div>
					<div class="top-blog ajax-load">
						<div class="load_bg"><span class="l_o_a_d"></span></div>
						<ul class="section-column--list">
							<?php
							$blog_args	 = array(
								'post_type'		 => 'blog',
								'post_status'	 => 'publish',
								'posts_per_page' => 8,
								'orderby'		 => 'date',
								'order'			 => 'order',
							);
							$blog_query	 = null;
							$blog_query	 = new WP_Query( $blog_args );
							if ( $blog_query->have_posts() ) :
								while ( $blog_query->have_posts() ) : $blog_query->the_post();
									$terms_blog = get_the_terms( get_the_ID(), 'blog_category' );
									?>

									<li class="section-column--item">
										<a class="link" href="<?php the_permalink() ?>">
											<div class="section-column--item-thumb">
												<?php if ( in_array( 'pr_status', get_field( 'pr_icon' ) ) ): ?>
													<span class="blog-item--thumb-label">PR</span>
												<?php endif; ?>
												<?php if ( has_post_thumbnail() ) : ?>
													<?php the_post_thumbnail( 'medium' ); ?>
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-img.jpg" />
												<?php endif; ?>
											</div>
											<div class="section-column--item-cnt">
												<div>
													<span class="date big"><?php the_time( 'Y.m.d' ); ?> [<?php echo strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) ?>]</span><br class="sp-only">
													<span class="tag">#<?php echo esc_html( $terms_blog[ 0 ]->name ); ?></span>
												</div>
												<div class="section-column--item-des">
													<?php the_title() ?>
												</div>
											</div>
										</a>
									</li>

									<?php
								endwhile;
							endif;
							wp_reset_postdata();
							?>

						</ul><!-- ./section-column--list -->
					</div>
					<div class="view-more-wrap">
						<a href="/blog" class="btn-view-more">コラム一覧へ</a>
					</div>
				</div>
			</section><!-- ./section-column -->
			<section class="section restaurant">
				<div class="container">
					<div class="section-restaurant--head fadeup">
						<h2 class="section-title green02">Restaurant<span>レストランを探す</span></h2>
						<p class="pickup-title">ヴィーガンメニューを提供するレストランを<br class="sp-only">全国から検索できます。</p>
						<form class="form-one" action="/shop" method="get">
							<input class="input" type="text" name="keywork" placeholder="キーワードから探す" required="required">
							<input class="search" type="submit" value="">
						</form>
					</div><!-- ./section-restaurant--head -->
					<form class="section-restaurant--form fadeup" action="/shop" method="get">
						<div class="section-restaurant--form-cnt">
							<div class="section-restaurant--form-col">
								<p class="section-recipe--search-label">条件を選択して探す</p>
								<div class="form-search">
									<?php
									$shop_area = get_terms( 'shop_area', 'hide_empty=0&parent=0' );
									?>
									<script type="text/javascript">
					/* <![CDATA[ */
					jQuery( document ).ready( function ( $ ) {
						$( '#restaurant01' ).change( function () {
						var prefID = $( this ).val();
						// call ajax
						$.ajax( {
							url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
							type: 'POST',
							data: 'action=pref__action&pref_ID=' + prefID,
							success: function ( results )
							{
							$( "#restaurant02" ).html( results );
							}
						} );
						} );
					} );
					/* ]]> */
									</script>
									<select class="input" id="restaurant01" name="pref[]">
										<option>都道府県</option>
										<?php
										if ( !empty( $shop_area ) && !is_wp_error( $shop_area ) ):
											foreach ( $shop_area as $shop_area_pref ):
												foreach ( get_terms( 'shop_area', "hide_empty=0&parent={$shop_area_pref->term_id}" ) as $area_pref ):
													echo "<option value='{$area_pref->term_id}'>{$area_pref->name}</option>";
												endforeach;
											endforeach;
										endif;
										?>
									</select>
									<select name="pref[]" class="input" id="restaurant02">
										<option>エリア</option>
									</select>
								</div>
							</div>
							<div class="section-restaurant--form-col">
								<p class="section-recipe--search-label ctrl">ジャンルから探す<span class="js-ctrlToggle">開く</span></p>
								<ul class="section-restaurant--cboxList ctrl js-cboxList">
									<?php
									$j			 = 0;
									$shop_genres = get_terms( 'shop_genre', 'hide_empty=0' );
									foreach ( $shop_genres as $shop_genre ) {
										$j++;
										?>
										<li>
											<input name="genre[]" class="checkbox" id="genre<?php echo printf( '%02d', $j ) ?>" type="checkbox" value="<?php echo $shop_genre->term_id ?>">
											<label for="genre<?php echo printf( '%02d', $j ) ?>"><?php echo $shop_genre->name ?></label>
										</li>
									<?php } ?>
								</ul>
							</div>
							<div class="section-restaurant--form-col">
								<p class="section-recipe--search-label ctrl">シーンから探す<span class="js-ctrlToggle">開く</span></p>
								<ul class="section-restaurant--cboxList ctrl js-cboxList">
									<?php
									$j			 = 0;
									$shop_icons	 = get_terms( 'shop_icon', 'hide_empty=0' );
									foreach ( $shop_icons as $shop_icon ) {
										$j++;
										?>
										<li>
											<input name="icon[]" class="checkbox" id="icon<?php echo printf( '%02d', $j ) ?>" type="checkbox" value="<?php echo $shop_icon->term_id ?>">
											<label for="icon<?php echo printf( '%02d', $j ) ?>"><?php echo $shop_icon->name ?></label>
										</li>
									<?php } ?>
								</ul>
							</div>
						</div>
						<div class="form-action">
							<input class="btn-gray" type="submit" value="検索">
							<button type="reset" class="btn-gray sliver">リセット</button>
						</div>
					</form><!-- ./section-restaurant--form -->
				</div>
			</section><!-- ./section-restaurant -->
			<section class="section news">
				<div class="container">
					<div class="section-news--cnt fadeup">
						<div class="section-news--ttl">
							<h2 class="section-title2">News<br class="pc-only"><span>お知らせ</span></h2>
						</div>
						<div class="section-news--listWrap">
							<ul class="section-news--list">
								<?php
								$args_news[ 'post_type' ]		 = 'news';
								$args_news[ 'posts_per_page' ]	 = 3;
								$args_news[ 'post_status' ]		 = 'publish';
								$args_news[ 'orderby' ]			 = 'date';
								$args_news[ 'order' ]			 = 'DESC';
								$args_news[ 'meta_query' ]		 = array(
									array(
										'key'		 => 'news_type',
										'value'		 => array( 'member' ),
										'compare'	 => 'NOT IN',
									)
								);

								$news_query	 = null;
								$news_query	 = new WP_Query( $args_news );

								if ( $news_query->have_posts() ):
									while ( $news_query->have_posts() ): $news_query->the_post();
										switch ( get_field( 'news_type' ) ) {
											case 'file':
												?>

												<li>
													<a href="<?php the_field( 'news_file' ); ?>" class="link" target="_blank">
														<p class="date"><?php the_time( 'Y.m.d' ); ?> [<?php echo strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) ?>]</p>
														<p class="section-news--sum <?php echo get_file_type( get_field( 'news_file' ) ) ?>"><?php the_title() ?></p>
													</a>
												</li>

												<?php
												break;
											case 'blank':
												?>

												<li>
													<a href="<?php the_field( 'news_blank' ); ?>" class="link" target="_blank">
														<p class="date"><?php the_time( 'Y.m.d' ); ?> [<?php echo strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) ?>]</p>
														<p class="section-news--sum <?php echo get_file_type( get_field( 'news_file' ) ) ?>"><?php the_title() ?></p>
													</a>
												</li>

												<?php
												break;

											default:
												?>
												<li>
													<a href="<?php the_permalink(); ?>" class="link">
														<p class="date"><?php the_time( 'Y.m.d' ); ?> [<?php echo strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) ?>]</p>
														<p class="section-news--sum"><?php the_title() ?></p>
													</a>
												</li>
												<?php
												break;
										}
									endwhile;
									wp_reset_postdata();
								endif;
								?>
							</ul>
							<div class="view-more-wrap">
								<a href="/news" class="btn-view-more">お知らせ一覧へ</a>
							</div>
						</div>
					</div>
					<?php if ( have_rows( 'advs_code', 'option' ) ): ?>
						<div class="section-news--advers fadeup ">
							<?php while ( have_rows( 'advs_code', 'option' ) ): the_row();
								?>
								<?php the_sub_field( 'content', 'option' ) ?>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
			</section><!-- ./section-news -->
		</main>
	<?php endwhile; ?>
<?php endif; ?>


<?php if ( get_field( 'campaign_img', "option" ) ): ?>
	<div class="modal-overlay js-modal-overlay hide"></div>
	<div class="modal-wrapper type3 hide">
		<div class="modal js-modal">
			<a href="javascript:void(0)" class="modal-close btn-close js-modal-close"></a>
			<div class="modal-cnt">
				<a href="<?php the_field( "campaign_url", "option" ) ?>" target="_blank"><img src="<?php the_field( "campaign_img", "option" ) ?>" alt=""></a>
			</div>
		</div>
	</div>
<?php endif; ?>

    
<?php get_footer(); ?>

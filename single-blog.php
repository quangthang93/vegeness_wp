<?php get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
                <?php wp_breadcrumb() ?>
            </div>
        </div>
    </div>
    <section class="section recipe blog">
        <div class="container">
            <?php 
            if (have_posts()): while (have_posts()) : the_post(); 
                $blog_cat = get_the_terms(get_the_ID(), 'blog_category');
                $blog_tags = get_the_terms(get_the_ID(), 'blog_tags');
                ?>
                    <div class="section-recipe--left fadeup2">
                        <div class="p-restaurant">
                            <div class="sectionEP-head">
                                <div class="blog-item--dateWrap">
                                    <?php if ( $blog_cat && ! is_wp_error( $blog_cat ) ) :
                                        foreach($blog_cat as $blog__c): ?>                                                       
                                            <span class="label"><?php echo $blog__c->name;?></span>
                                        <?php
                                        endforeach;
                                    endif;?>
                                    <span class="date big"><?php the_time('Y.m.d');?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d')))?>]</span>
                                </div>
                                <div class="sectionEP-titleWrap">
                                    <h1 class="sectionEP-title no-icon"><?php the_title()?></h1>
                                </div>
                            </div>
                            <div class="section-recipe--row">
                                <div class="blog-detail no-reset">
                                    <?php if (has_post_thumbnail()) : ?>                                
                                        <div class="blog-item--thumb">
                                            <?php if(in_array('pr_status', get_field('pr_icon'))):?>
                                                <span class="blog-item--thumb-label type2">PR</span>
                                            <?php endif;?>
                                            <?php the_post_thumbnail('large'); ?>
                                        </div>                                                                        
                                    <?php endif; ?>
                                    
                                    <?php the_content();?>
                                    
                                    <div class="blog-detail--direct">
                                        <div class="blog-detail--direct-tag">
                                            <?php if ( $blog_tags && ! is_wp_error( $blog_tags ) ) :
                                                foreach($blog_tags as $blog_tag): 
                                                    $tag_link = get_term_link($blog_tag, 'blog_tags');
                                                    ?>
                                                    <a href="<?php echo $tag_link;?>"><span class="tag"><?php echo $blog_tag->name;?></span></a>
                                                <?php
                                                endforeach;
                                            endif;?>                                            
                                        </div>
                                        <div class="blog-detail--direct-sns">
                                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink() ?>" target="_blank" class="facebook link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-facebook.svg" alt=""></a>
                                            <a href="https://twitter.com/share?url=<?php echo get_the_permalink() ?>&text=<?php the_title(); ?>" target="_blank" class="link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-twitter.svg" alt=""></a>
                                            <a href="https://pinterest.com/pin/create/button/?url=<?php echo get_the_permalink() ?>&description=<?php echo get_the_title(); ?>&media=<?php echo get_the_post_thumbnail_url()?>" target="_blank" class="link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-pinterest.svg" alt=""></a>

                                        </div>
                                    </div>
                                </div><!--End .blog-detail-->
                                <div class="blog-ctrl">
                                    <?php
                                    if ($back_link = get_adjacent_post(false, '', true)) {
                                        ?>
                                        <a href="<?php echo get_permalink($back_link->ID); ?>" class="view-more link back">Back</a>
                                        <?php
                                    } else {
                                        $first = new WP_Query('posts_per_page=1&order=ASC&post_type=blog');
                                        $first->the_post();
                                        echo '<a href="' . get_permalink() . '" class="view-more link back">Back</a>';
                                        wp_reset_query();
                                    };
                                    ?>
                                    <a href="/blog" class="blog-ctrl--menu link">
                                        <span class="home">記事一覧へ</span>
                                    </a>
                                    <?php
                                    if ($next_link = get_adjacent_post(false, '', false)) {
                                        ?>
                                        <a href="<?php echo get_permalink($next_link->ID); ?>" class="view-more link next">Next</a>
                                        <?php
                                    } else {
                                        $last = new WP_Query('posts_per_page=1&order=DESC&post_type=blog');
                                        $last->the_post();
                                        echo '<a href="' . get_permalink() . '" class="view-more link next">Next</a>';
                                        wp_reset_query();
                                    };
                                    ?>
                                    
                                </div>
                                
                                <?php
                                $cat_ids = wp_list_pluck($blog_cat, 'term_id');
                                $args = array(
                                    'post_type' => 'blog',
                                    'tax_query' => array(
                                        'relation' => 'OR',
                                        array(
                                            'taxonomy' => 'blog_category',
                                            'field' => 'id',
                                            'terms' => $cat_ids,
                                            'operator' => 'IN',
                                        ),                                        
                                    ),
                                    'post_status' => 'publish',
                                    'posts_per_page' => 3,
                                    'orderby'   => 'rand',
                                    'post__not_in' => array(get_the_ID())
                                );
                                $second_query = new WP_Query($args);
                                if ($second_query->have_posts()) {?>                                
                                    <div class="recipe-selection type2">
                                        <h4 class="titleLv4">関連記事</h4><br>
                                        <div class="section-recipe--selectionList">
                                            <?php
                                            while ($second_query->have_posts()) : $second_query->the_post();
                                                $column_cat = get_the_terms(get_the_ID(), 'blog_category');
                                                ?>

                                                <div class="section-recipe--selection">
                                                    <a href="<?php the_permalink() ?>" class="section-recipe--selection-inner link">
                                                        <div class="section-recipe--selection-thumb">
                                                            <?php if ( has_post_thumbnail() ) : ?>
                                                                <?php the_post_thumbnail('medium'); ?>
                                                            <?php else:?>
                                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-img.jpg" />
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="section-recipe--selection-cnt pickup-title"><?php the_title() ?></div>
                                                    </a>
                                                </div>

                                            <?php
                                            endwhile;
                                            wp_reset_query();?>
                                        </div>
                                    </div>
                                <?php }?>
                                                                
                            </div><!--End .section-recipe--row-->
                        </div><!--End .p-restaurant-->
                    </div><!-- ./section-recipe--left -->
                    <?php endwhile; ?>
                <?php endif; ?>
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
                    <?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section><!--End .blog-->
    <?php get_footer('blog'); ?>
    <?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>

<?php get_footer(); ?>

<?php get_header(); ?>
<?php
$season_cond = array();
$args		 = array();
$cooking_time_key = array_keys($cooking_times);

$menu_arr		 = nk_type_menus();
$ingredient_arr	 = nk_type_ingredients();

$recipe_term_names	 = wp_list_pluck( get_terms( 'recipe_category', 'hide_empty=0' ), 'name' );
$recipe_menus		 = wp_list_pluck( $menu_arr, 'n' );
$recipe_ingredient	 = wp_list_pluck( $ingredient_arr, 'n' );

$g_param			 = isset( $_REQUEST[ 'genre' ] ) ? $_REQUEST[ 'genre' ] : '';
$season_param		 = isset( $_REQUEST[ 'season' ] ) ? $_REQUEST[ 'season' ] : '';
$menu_param			 = isset( $_REQUEST[ 'course' ] ) ? (int) $_REQUEST[ 'course' ] : 0;
$ingredient_param	 = isset( $_REQUEST[ 'ingredient' ] ) ? (int) $_REQUEST[ 'ingredient' ] : 0;
$cooking_time_param	 = isset( $_REQUEST[ 'time' ] ) ? (int) $_REQUEST[ 'time' ] : 0;
if ( isset( $_REQUEST[ 'keyclear' ] ) && $_REQUEST[ 'keyclear' ] != "" ) {
	if ( in_array( $_REQUEST[ 'keyclear' ], [ '春', '夏', '秋', '冬' ] ) )
		$season_param		 = '';
	if ( in_array( $_REQUEST[ 'keyclear' ], $recipe_term_names ) )
		$g_param			 = '';
	if ( in_array( $_REQUEST[ 'keyclear' ], $recipe_menus ) )
		$menu_param			 = '';
	if ( in_array( $_REQUEST[ 'keyclear' ], $recipe_ingredient ) )
		$ingredient_param	 = '';
        if ( in_array( $_REQUEST[ 'keyclear' ], $cooking_time_key ) )
            $cooking_time_param = '';
}
?>
<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
				<?php wp_breadcrumb() ?>
            </div>
        </div>
    </div>
    <section class="section recipe end">
        <div class="container">
            <div class="section-recipe--left fadeup2">
                <div class="sectionEP-head">
                    <div class="sectionEP-titleWrap">
                        <h1 class="sectionEP-title">レシピ一覧</h1>
                        <p class="sectionEP-resultSumary"></p>
                    </div>
                    <div class="sectionEP-sort">
                        <div class="tagX-list recipe-list">
							<?php if ( $g_param != "" ): ?>
								<span class="tagX"><?php echo $g_param; ?><i class="tagX-icon"></i></span>
								<input type="hidden" value="<?php echo $g_param ?>" />
							<?php endif; ?>
							<?php if ( (int) $menu_param > 0 ): ?>
								<span class="tagX"><?php echo $menu_arr[ $menu_param ][ 'n' ]; ?><i class="tagX-icon"></i></span>
								<input type="hidden" value="<?php echo $menu_arr[ $menu_param ][ 'n' ] ?>" />
							<?php endif; ?>
							<?php if ( (int) $ingredient_param > 0 ): ?>
								<span class="tagX"><?php echo $ingredient_arr[ $ingredient_param ][ 'n' ]; ?><i class="tagX-icon"></i></span>
								<input type="hidden" value="<?php echo $ingredient_arr[ $ingredient_param ][ 'n' ] ?>" />
							<?php endif; ?>
							<?php if ( (int)$cooking_time_param > 0 ): ?>
								<span class="tagX"><?php echo $cooking_times[$cooking_time_param]; ?><i class="tagX-icon"></i></span>
								<input type="hidden" value="<?php echo $cooking_time_param ?>" />
							<?php endif; ?>
							<?php if ( $season_param != "" ): ?>
								<span class="tagX"><?php echo $season_param; ?><i class="tagX-icon"></i></span>
								<input type="hidden" value="<?php echo $season_param ?>" />
							<?php endif; ?>
                        </div>
                        <div class="sectionEP-newOrders">
                            <div class="form-search">
                                <select class="input" name="sort" onchange="document.location.href = replaceUrlParam( document.location.href, 'view', this.options[this.selectedIndex].value )">
                                    <option value="newer" <?php selected( $_GET[ 'view' ], 'newer' ) ?>>新着順</option>
                                    <option value="popular" <?php selected( $_GET[ 'view' ], 'popular' ) ?>>人気順</option>
                                </select>
                                <script type="text/javascript">
					function replaceUrlParam( url, paramName, paramValue ) {
					var pattern = new RegExp( '(\\?|\\&)(' + paramName + '=).*?(&|$)' )
					var newUrl = url
					if ( url.search( pattern ) >= 0 ) {
						newUrl = url.replace( pattern, '$1$2' + paramValue + '$3' );
					} else {
						newUrl = newUrl + ( newUrl.indexOf( '?' ) > 0 ? '&' : '?' ) + paramName + '=' + paramValue
					}
					return newUrl;
					}
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-recipe--row">
                    <div class="section-recipe--pickupList">
						<?php
						global $wpdb;
						$s_lists		 = [];
						$s_lists2		 = [];
						$lists_post_ids	 = [];
						$post_ids		 = [];

						//キーワードで検索ここから
						$lists = [
							[ 'えだまめ', '枝豆' ],
							[ 'かぼちゃ', '南瓜' ],
							[ 'きゅうり', '胡瓜' ],
							[ 'こまつな', '小松菜' ],
							[ 'ごぼう', '牛蒡' ],
							[ 'さつまいも', 'さつま芋', '薩摩芋' ],
							[ 'さといも', 'さと芋', '里芋' ],
							[ 'しいたけ', '椎茸' ],
							[ 'じゃがいも', 'じゃが芋' ],
							[ 'しゅんぎく', '春菊' ],
							[ 'しょうが', '生姜' ],
							[ 'そらまめ', 'そら豆', '空豆' ],
							[ 'たまねぎ', '玉ねぎ', '玉葱' ],
							[ 'だいこん', '大根' ],
							[ 'ちんげんさい', '青梗菜', 'ちんげん菜' ],
							[ 'とうがらし', '唐辛子' ],
							[ 'とうがん', '冬瓜' ],
							[ 'なす', '茄子' ],
							[ 'にんじん', '人参' ],
							[ 'ねぎ', '葱' ],
							[ 'はくさい', '白菜' ],
							[ 'ほうれんそう', 'ほうれん草' ],
							[ 'みょうが', '茗荷' ],
							[ 'みずな', '水菜' ],
							[ 'むらさききゃべつ', '紫きゃべつ' ],
							[ 'むらさきたまねぎ', '紫たまねぎ', '紫玉葱' ],
							[ 'やまいも', '山芋' ],
							[ 'れんこん', '蓮根' ],
							[ 'なのはな', '菜の花' ],
							[ 'みつば', '三つ葉' ],
							[ 'めきゃべつ', '芽きゃべつ' ],
							[ 'ながいも', '長芋', '長いも' ],
							[ 'わさび', '山葵' ],
							[ 'ゆりね', '百合根', 'ゆり根' ],
							[ 'きぬさや', '絹さや' ],
							[ 'ひよこまめ', 'ひよこ豆' ],
							[ 'たけのこ', '筍', '竹の子' ],
							[ 'きのこ', '木の子' ],
							[ 'きくらげ', '木耳' ],
							[ 'みそ', '味噌' ],
							[ 'さとう', '砂糖' ],
							[ 'しょうゆ', '醤油' ],
							[ 'しお', '塩' ],
							[ 'こしょう', '胡椒' ],
							[ 'おりーぶおいる', 'おりーぶ油' ],
							[ 'こうじ', '麹' ],
							[ 'まいたけ', '舞茸' ],
						];

						$_keyword = $_GET[ 's_name' ];

						$_keyword_array		 = [];
						$_keyword			 = preg_replace( "/( |　)/", ",", $_keyword );
						$_l_keyword_array	 = explode( ",", $_keyword );

						foreach ( $lists as $k => &$v ) {
							$l = [];
							foreach ( $v as $k2 => $v2 ) {
								$l[] = $v2;
								$lv	 = mb_convert_kana( $v2, "KVC" );
								if ( $v2 !== $lv ) {
									$l[] = $lv;
								}
							}
							$v = $l;
						}
						unset( $v );


						foreach ( $_l_keyword_array as $k => $v ) {
							$lv	 = trim( $v );
							$lv2 = [];
							if ( $lv != '' ) {
								$lv2[]		 = $lv;
								$llv_kana	 = mb_convert_kana( $v, "cH" );
								if ( $lv !== $llv_kana ) {
									$lv2[] = $llv_kana;
								}
								$llv = mb_convert_kana( $v, "KVC" );
								if ( $lv !== $llv ) {
									$lv2[] = $llv;
								}

								foreach ( $lists as $k2 => $v2 ) {
									if ( in_array( $llv_kana, $v2, true ) ) {
										foreach ( $v2 as $k3 => $v3 ) {
											if ( in_array( $v3, $lv2, true ) ) {
												continue;
											}
											$lv2[] = $v3;
										}
										break;
									}
								}

								$_keyword_array[] = $lv2;
							}
						}

						if ( count( $_keyword_array ) > 1 ) {
							$operator_str = '(' . $operator_str . ')';
						} else {
							$operator_str = '';
						}

						//20200116start
						$base_sql = "SELECT distinct object_id FROM $wpdb->terms as a INNER JOIN $wpdb->term_taxonomy as b ON `a`.`term_id` = `b`.`term_id` INNER JOIN $wpdb->term_relationships as c ON `a`.`term_id` = `c`.`term_taxonomy_id` INNER JOIN $wpdb->posts as p ON `p`.`ID` = `c`.`object_id` WHERE ( `b`.taxonomy='recipe_category' OR `b`.taxonomy='wprm_course' OR `b`.taxonomy='wprm_cuisine' OR `b`.taxonomy='wprm_ingredient' )";

						$base_sql .= " AND ";

						foreach ( $_keyword_array as $k => $v ) {

							if ( count( $v ) === 0 ) {
								continue;
							}

							$sql = $base_sql;

							$sql .= ' ( ';

							foreach ( $v as $k2 => $v2 ) {
								if ( $k2 != 0 ) {
									$sql .= ' OR';
								}

								$sql .= " a.name LIKE '%" . $wpdb->esc_like( $v2 ) . "%' ";
							}

							$sql .= ' ) ';

							$results = $wpdb->get_results( $sql );

							foreach ( $results as $r ) {
								$l_id = (int) $r->object_id;
								if ( !in_array( $l_id, $lists_post_ids[ $k ] ?: [], true ) ) {
									$lists_post_ids[ $k ][] = $l_id;
								}
							}
						}

						//20200116start
						$base_sql = "SELECT post_id FROM $wpdb->postmeta INNER JOIN $wpdb->posts ON `$wpdb->posts`.`ID` = `$wpdb->postmeta`.`post_id` WHERE (meta_key = 'recipe_desc' OR meta_key = 'wprm_ingredients' OR meta_key = 'wprm_author_name' )  AND ";
						foreach ( $_keyword_array as $k => $v ) {
							if ( count( $v ) === 0 ) {
								continue;
							}

							$sql = $base_sql;
							$sql .= ' ( ';

							foreach ( $v as $k2 => $v2 ) {
								if ( $k2 != 0 ) {
									$sql .= ' OR';
								}
								$sql .= " meta_value LIKE '%" . $wpdb->esc_like( $v2 ) . "%' ";
							}

							$sql .= ' ) ';

							$results = $wpdb->get_results( $sql );

							foreach ( $results as $r ) {
								$l_id = (int) $r->post_id;
								if ( !in_array( $l_id, $lists_post_ids[ $k ] ?: [], true ) ) {
									$lists_post_ids[ $k ][] = $l_id;
								}
							}
						}

						//20200116start
						$base_sql = "SELECT ID FROM $wpdb->posts WHERE `$wpdb->posts`.`post_type` = 'wprm_recipe' AND ";
						foreach ( $_keyword_array as $k => $v ) {
							if ( count( $v ) === 0 ) {
								continue;
							}

							$sql = $base_sql;
							$sql .= ' ( ';

							foreach ( $v as $k2 => $v2 ) {
								if ( $k2 != 0 ) {
									$sql .= ' OR';
								}
								$sql .= " post_title LIKE '%" . $wpdb->esc_like( $v2 ) . "%' ";
								$sql .= " OR post_content LIKE '%" . $wpdb->esc_like( $v2 ) . "%' ";
							}

							$sql .= ' ) ';


							$results = $wpdb->get_results( $sql );

							foreach ( $results as $r ) {
								$l_id = (int) $r->ID;
								if ( !in_array( $l_id, $lists_post_ids[ $k ] ?: [], true ) ) {
									$lists_post_ids[ $k ][] = $l_id;
								}
							}
						}

						foreach ( $_keyword_array as $k => $v ) {
							if ( count( $v ) === 0 ) {
								continue;
							}
							$l = $lists_post_ids[ $k ] ?: [];
							if ( count( $l ) > 0 ) {
								$results = $wpdb->get_results( "SELECT meta_value FROM $wpdb->postmeta INNER JOIN $wpdb->posts ON `$wpdb->posts`.`ID` = `$wpdb->postmeta`.`meta_value` WHERE post_id IN (" . join( ',', $l ) . ") AND meta_key='wprm_parent_post_id'" );
								foreach ( $results as $r ) {
									$l_id = (int) $r->meta_value;
									if ( !in_array( $l_id, $lists_post_ids[ $k ] ?: [], true ) ) {
										$lists_post_ids[ $k ][] = $l_id;
									}
								}
							}
						}

						if ( $operator == 2 ) {
							foreach ( $lists_post_ids as $k => $v ) {
								foreach ( $v as $k2 => $v2 ) {
									if ( !in_array( $v2, $post_ids, true ) ) {
										$post_ids[] = $v2;
									}
								}
							}
						} else {
							$post_ids	 = $lists_post_ids[ 0 ] ?: [];
							$l_num		 = count( $lists_post_ids );
							if ( $l_num > 1 ) {
								for ( $i = 1; $i < $l_num; $i++ ) {
									$post_ids = array_intersect( $post_ids, $lists_post_ids[ $i ] );
								}
							}
						}
						//キーワードで検索こもまで

						$recipe_menu		 = func_recipe_conditions( 'menu', $menu_param );
						$recipe_ingredient	 = func_recipe_conditions( 'ingredient', $ingredient_param );
                                                
                                                //調理時間から探す
                                                $recipe_time = [];
                                                $recipe_time2 = [];
                                                if($cooking_time_param > 0) {
                                                    $recipe_cond =  $cooking_time_param <= 60 ? " <= {$cooking_time_param}" : " >= {$cooking_time_param}";
                                                    $results2 = $wpdb->get_results( "SELECT $wpdb->posts.ID AS post_id FROM $wpdb->posts LEFT JOIN $wpdb->postmeta AS pm1 ON ($wpdb->posts.ID = pm1.post_id AND pm1.meta_key='wprm_cook_time') LEFT JOIN $wpdb->postmeta AS pm2 ON ($wpdb->posts.ID = pm2.post_id AND pm2.meta_key='wprm_prep_time') WHERE $wpdb->posts.post_status = 'publish' AND (pm1.meta_value + pm2.meta_value) {$recipe_cond}" );
                                                    foreach ($results2 as $t_v) {
                                                        $t_id = (int) $t_v->post_id;
                                                        $recipe_time[] = $t_id;
                                                    }
                                                    if(count($recipe_time) > 0) {
                                                        $results3 = $wpdb->get_results( "SELECT meta_value FROM $wpdb->postmeta INNER JOIN $wpdb->posts ON `$wpdb->posts`.`ID` = `$wpdb->postmeta`.`meta_value` WHERE post_id IN (" . join( ',', $recipe_time ) . ") AND meta_key='wprm_parent_post_id'" );
                                                        foreach ($results3 as $r2_v) {
                                                            $p_id = (int) $r2_v->meta_value;
                                                            $recipe_time2[] = $p_id;
                                                            
                                                        }
                                                    }
                                                }
                                                
						# メニューから探す
						$l_menus2 = nk_type_menus();
						foreach ( $l_menus2 as $k => $v ) {
							if ( $k == 'all' ) {
								continue;
							}
							if ( $v[ 'n' ] != '' ) {
								$s_lists[ 's' . ($k) ] = [
									'n'	 => $v[ 'n' ],
									's'	 => $v[ 's' ]
								];
							}
						}

						# 材料から探す
						$l_ingredients = nk_type_ingredients();
						foreach ( $l_ingredients as $k => $v ) {
							if ( $k == 'all' ) {
								continue;
							}

							$l_s_all = [];

							$ll_no_s_all = [];

							foreach ( $v[ 'lists' ] as $k2 => $v2 ) {
								$l_s = isset( $v2[ 's' ] ) ? $v2[ 's' ] : [ $v2[ 'n' ] ];
								foreach ( $l_s as $k3 => $v3 ) {
									$l_s_all[] = $v3;
								}
								$l_s_no = isset( $v2[ 's_no' ] ) ? $v2[ 's_no' ] : [];
								foreach ( $l_s_no as $k3 => $v3 ) {
									$ll_no_s_all[] = $v3;
								}
								$s_lists2[ 's' . ($k + $k2) ] = [
									's'		 => $l_s,
									's_no'	 => $l_s_no,
									'n'		 => $v2[ 'n' ],
								];
							}

							if ( count( $l_s_all ) > 0 ) {

								$l_no_s_all = [];
								foreach ( $ll_no_s_all as $k2 => $v2 ) {

									if ( in_array( $v2, $l_s_all, true ) ) {
										continue;
									}

									$llv_kana = mb_convert_kana( $v2, "cH" );
									if ( in_array( $llv_kana, $l_s_all, true ) ) {
										continue;
									}

									$llv = mb_convert_kana( $v2, "KVC" );
									if ( in_array( $llv, $l_s_all, true ) ) {
										continue;
									}
									$l_no_s_all[] = $v2;
								}
								$s_lists2[ 's' . ($k) ] = [
									'n'		 => $v[ 'n' ],
									's_no'	 => $l_no_s_all,
									's'		 => $l_s_all
								];
							}
						}

						if ( isset( $s_lists[ 's' . $menu_param ] ) ) {
							$my_type = $s_lists[ 's' . $menu_param ];

							$_keyword_array = [];

							foreach ( $my_type[ 's' ] as $k => $v ) {
								$lv	 = trim( $v );
								$lv2 = [];
								if ( $lv != '' ) {
									$lv2[]		 = $lv;
									$llv_kana	 = mb_convert_kana( $v, "cH" );
									if ( $lv !== $llv_kana ) {
										$lv2[] = $llv_kana;
									}
									$llv = mb_convert_kana( $v, "KVC" );
									if ( $lv !== $llv ) {
										$lv2[] = $llv;
									}
									$_keyword_array[] = $lv2;
								}
							}

							$_no_keyword_array = [];
							if ( isset( $my_type[ 's_no' ] ) ) {
								foreach ( $my_type[ 's_no' ] as $k => $v ) {
									$lv = trim( $v );
									if ( $lv != '' ) {
										$_no_keyword_array[] = $lv;
										$llv_kana			 = mb_convert_kana( $v, "cH" );
										if ( $lv !== $llv_kana ) {
											$_no_keyword_array[] = $llv_kana;
										}
										$llv = mb_convert_kana( $v, "KVC" );
										if ( $lv !== $llv ) {
											$_no_keyword_array[] = $llv;
										}
									}
								}
							}


							$base_sql = "SELECT distinct object_id FROM $wpdb->terms as a INNER JOIN $wpdb->term_taxonomy as b ON `a`.`term_id` = `b`.`term_id` INNER JOIN $wpdb->term_relationships as c ON `a`.`term_id` = `c`.`term_taxonomy_id` INNER JOIN $wpdb->posts as p ON `p`.`ID` = `c`.`object_id` WHERE ( `b`.taxonomy='wprm_course' )";

							$base_sql .= " AND ";

							foreach ( $_keyword_array as $k => $v ) {

								if ( count( $v ) === 0 ) {
									continue;
								}

								$sql = $base_sql;

								$sql .= ' ( ';

								$i = 0;
								foreach ( $v as $k2 => $v2 ) {
									$lv		 = explode( '&&', $v2 );
									$l_terms = get_terms( [
										'taxonomy'	 => 'wprm_course',
										'fields'	 => 'ids',
										'name'		 => $lv,
									] );
									if ( count( $lv ) == count( $l_terms ) ) {
										if ( $i != 0 ) {
											$sql .= ' OR';
										}
										$sql .= "( ( SELECT COUNT(1) FROM $wpdb->term_relationships WHERE term_taxonomy_id IN (" . implode( ',', $l_terms ) . ") AND object_id = p.ID ) = " . count( $l_terms ) . " ) ";
										$i++;
									}
								}

								$sql .= ' ) ';

								$l_no_lists = [];
								if ( count( $_no_keyword_array ) > 0 && $type == 'menu' ) {
									$no_lists_sql	 = $base_sql;
									$no_lists_sql	 .= ' AND ';
									$no_lists_sql	 .= ' ( ';
									foreach ( $_no_keyword_array as $k2 => $v2 ) {
										if ( $k2 != 0 ) {
											$no_lists_sql .= ' OR';
										}
										if ( $s_type == 'menu' ) {
											$no_lists_sql .= " a.name = '" . $v2 . "' ";
										} else {
											$no_lists_sql .= " meta_value LIKE '%" . $wpdb->esc_like( $v2 ) . "%' ";
										}
									}
									$no_lists_sql	 .= ' ) ';
									$l_results		 = $wpdb->get_results( $no_lists_sql );
									foreach ( $l_results as $r ) {
										if ( $s_type == 'menu' ) {
											$l_id = (int) $r->object_id;
										} else {
											$l_id = (int) $r->post_id;
										}
										$l_no_lists[] = $l_id;
									}
								}

								$results = $wpdb->get_results( $sql );

								foreach ( $results as $r ) {
									$l_id = (int) $r->object_id;
									if ( in_array( $l_id, $l_no_lists, true ) ) {
										continue;
									}
									if ( !in_array( $l_id, $lists_post_ids[ $k ] ?: [], true ) ) {
										$lists_post_ids[ $k ][] = $l_id;
									}
								}
							}


							foreach ( $_keyword_array as $k => $v ) {
								if ( count( $v ) === 0 ) {
									continue;
								}
								$l = $lists_post_ids[ $k ] ?: [];
								if ( count( $l ) > 0 ) {
									$results = $wpdb->get_results( "SELECT meta_value FROM $wpdb->postmeta INNER JOIN $wpdb->posts ON `$wpdb->posts`.`ID` = `$wpdb->postmeta`.`meta_value` WHERE post_id IN (" . join( ',', $l ) . ") AND meta_key='wprm_parent_post_id'" );
									foreach ( $results as $r ) {
										$l_id = (int) $r->meta_value;
										if ( !in_array( $l_id, $lists_post_ids[ $k ] ?: [], true ) ) {
											$lists_post_ids[ $k ][] = $l_id;
										}
									}
								}
							}

							foreach ( $lists_post_ids as $k => $v ) {
								foreach ( $v as $k2 => $v2 ) {
									if ( !in_array( $v2, $post_ids, true ) ) {
										$post_ids[] = $v2;
									}
								}
							}
						}

						$post_ids2 = [];
						//キーワードで検索の場合
						if ($_keyword != "" && count($post_ids) > 0) {
                                                    $post_ids2 = $post_ids;
                                                } else {
                                                    //絞り込む検索の場合
                                                    if (count($recipe_menu) > 0 && count($recipe_time2) > 0)
                                                        $post_ids2 = array_intersect($recipe_menu, $recipe_time2);
                                                    elseif (count($recipe_menu) == 0)
                                                        $post_ids2 = $recipe_time2;
                                                    elseif (count($recipe_time2) == 0)
                                                        $post_ids2 = $recipe_menu;
                                                }

                                                if ( isset( $_GET[ 'view' ] ) && $_GET[ 'view' ] === 'popular' ) {
							$wpp_option = array( // 表示オプションの設定
								'range'			 => 'all',
								'post_type'		 => 'recipe',
								'orderby'		 => 'views',
								'posts_per_page' => -1,
								'limit'			 => '9999',
							);
							if ( count( $post_ids2 ) > 0 ) {
                                                            $wpp_option[ 'post__in' ] = $post_ids2;//聞かない条件
							}
							$wpp_query = new WPP_Query( $wpp_option );
							$post_ids3 = array_map(
                                                            function( $wppost ) {
                                                                return (int) $wppost->id;
                                                            }, $wpp_query->get_posts()
							);

                                                        //So sánh với giá trị của mảng recipe_id ở ranking
                                                        $post_ids2 = array_intersect($post_ids2, $post_ids3);
							$args[ 'post__in' ]	 = $post_ids2;
							$args[ 'orderby' ]	 = 'post__in';
							$args[ 'order' ]	 = 'ASC';
						} else {
							$args[ 'orderby' ]	 = 'date';
							$args[ 'order' ]	 = 'DESC';
						}
                                                
                                                $recipe_genre	 = "";
						if ( $g_param != "" )
							$recipe_genre	 = array(
								'taxonomy'	 => 'recipe_category',
								'field'		 => 'name',
								'terms'		 => sanitize_text_field( $g_param ),
								'operator'	 => 'IN'
							);

						$conditions		 = array(
							'relation' => 'AND',
							$recipe_genre,
						);
						if ( !empty( $season_param ) )
							$season_cond	 = array(
								'key'		 => 'recipe_season',
								'value'		 => sanitize_text_field( $season_param ),
								'compare'	 => 'LIKE'
							);
						$conditions_meta = array(
							$season_cond,
						);

						if ( $_keyword != "" && count( $post_ids2 ) === 0 ) {
							$post_ids2[] = 0;
						}
						$args[ 'post__in' ]			 = $post_ids2;
						$args[ 'post_type' ]		 = 'recipe';
						$args[ 'post_status' ]		 = 'publish';
						$args[ 'posts_per_page' ]	 = 16;
						$paged						 = (get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
						$args[ 'paged' ]			 = $paged;
						$args[ 'tax_query' ]		 = $conditions;
						$args[ 'meta_query' ]		 = $conditions_meta;

						$recipe_query	 = null;
						$recipe_query	 = new WP_Query( $args );

						if ( $recipe_query->have_posts() ):
							while ( $recipe_query->have_posts() ): $recipe_query->the_post();
								$recipe_cat = get_the_terms( get_the_ID(), 'recipe_category' );

								$recipes	 = WPRM_Recipe_Manager::get_recipe_ids_from_post();
								if ( !empty( $recipes ) )
									$recipe_id	 = $recipes[ 0 ];
								else {
									$recipe_id = preg_replace( '/[^0-9]/', '', get_field( 'recipe_desc' ) );
								}

								$recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );
                                                                $recipe_pos = get_field('recipe_display');
								?>
								<div class="section-recipe--pickup">
									<div class="section-recipe--pickup-inner">
										<div class="section-recipe--pickup-thumb">
                                                                                    <?php if(!is_user_logged_in() && $recipe_pos === 'is_not_member') {?>
                                                                                        <a href="javascript:void:(0)" data-remodal-target="modal-clip" class="link">
                                                                                            <?php
                                                                                            if ( has_post_thumbnail() ) :
                                                                                                    the_post_thumbnail( 'medium' );
                                                                                            else:
                                                                                                    echo do_shortcode( '[wprm-recipe-image size=\'medium\']' );
                                                                                            endif;
                                                                                            ?>
                                                                                        </a>
                                                                                    <?php } else {?>                                                                                        
											<a href="<?php the_permalink(); ?>" class="link">
                                                                                            <?php
                                                                                            if ( has_post_thumbnail() ) :
                                                                                                    the_post_thumbnail( 'medium' );
                                                                                            else:
                                                                                                    echo do_shortcode( '[wprm-recipe-image size=\'medium\']' );
                                                                                            endif;
                                                                                            ?>
											</a>
                                                                                    <?php }?>
										</div>
										<div class="section-recipe--pickup-cnt">
                                                                                    <?php if (!is_user_logged_in() && $recipe_pos === 'is_not_member') { ?>
                                                                                        <div class="restrict-box">
                                                                                            <span class="label-orange">会員限定</span><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-lock-orange.svg" style="width: auto;">                                                                                                        
                                                                                        </div>
                                                                                    <?php } ?>
											<p class="date big"><?php the_time( 'Y.m.d' ); ?> [<?php echo strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) ?>]</p>
											<h3 class="pickup-title big link">
                                                                                            <?php if(!is_user_logged_in() && $recipe_pos === 'is_not_member') {?>
                                                                                                <a href="javascript:void:(0)" data-remodal-target="modal-clip"><?php the_title() ?></a>
                                                                                            <?php } else {?>
                                                                                                <a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
                                                                                            <?php }?>
                                                                                        </h3>
											<div class="pc-only">
												<?php
												if ( $recipe || $recipe->tags( $key ) ) {
													$terms_course = $recipe->tags( 'course' );
													foreach ( $terms_course as $term_course ) {
														printf( "<span class=\"tag\">#%s</span>", $term_course->name );
													}
												}
												?>
											</div>
											<div class="pickup-infor pc-only">
												<span><?php echo esc_html( $recipe_cat[ 0 ]->name ); ?></span>
												<span class="time"><?php echo $recipe->prep_time() + $recipe->cook_time(); ?>分<?php if ( $recipe->custom_time_label() ) printf( "（%sを除く）", $recipe->custom_time_label() ) ?></span>
												<span class="kcal"><?php echo $recipe->calories(); ?>kcal</span>
											</div>
										</div>
									</div>
									<div class="sp-only">
										<?php
										if ( $recipe || $recipe->tags( $key ) ) {
											$terms_course = $recipe->tags( 'course' );
											foreach ( $terms_course as $term_course ) {
												printf( "<span class=\"tag\">#%s</span>", $term_course->name );
											}
										}
										?>
									</div>
									<div class="pickup-infor sp-only">
										<span><?php echo esc_html( $recipe_cat[ 0 ]->name ); ?></span>
										<span class="time"><?php echo $recipe->prep_time() + $recipe->cook_time(); ?>分<?php if ( $recipe->custom_time_label() ) printf( "（%sを除く）", $recipe->custom_time_label() ) ?></span>
										<span class="kcal"><?php echo $recipe->calories(); ?>kcal</span>
									</div>
								</div><!-- ./section-recipe--pickup -->

								<?php
							endwhile;
							wp_reset_postdata();
						else:
							?>
							<div class="search-msg">
								<p class="titleLv4">該当のレシピがありません。</p>
								<p class="titleLv5">キーワードを変更してもう一度検索してください。</p>
							</div>
							<div class="pickup-suggest">
								<h3 class="titleLv2">おすすめ新着レシピ</h3>
								<?php latest_recipe(); ?>
							</div><!--End .pickup-suggest-->
						<?php endif; ?>
                    </div><!-- ./section-recipe--pickupList -->
					<?php if ( $recipe_query->found_posts > 0 ) pagination( $recipe_query, 4, 16 ); ?>
                </div><!-- ./section-recipe--row -->
            </div><!-- ./section-recipe--left -->
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
					<?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section><!--End .recipe-->

	<?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>

<?php get_footer(); ?>

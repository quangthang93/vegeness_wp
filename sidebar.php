<div class="section-recipe--search pc-only">
    <p class="label-msg"><span>レシピを探す</span></p>
    <div class="form-oneWrap">
        <p class="section-recipe--search-label">キーワードで探す</p>
        <form class="form-one" action="/recipe">
            <input class="input" type="text" name="s_name" value="<?php echo isset($_GET['s_name']) ? $_GET['s_name']:'';?>" placeholder="料理名、レシピ名など">
            <input class="search" type="submit" value="">
        </form>
    </div>
    <?php
    $menus = nk_type_menus();
    $ingredients = nk_type_ingredients();
    if(get_field('recipe_keys', 'option')):?>
        <div class="form-suggest">
            <p class="form-suggest--label">よく検索されるキーワード</p>
            <div>
            <?php
            while( have_rows('recipe_keys', 'option') ) : the_row();
                printf("<a href=\"/recipe?s_name=%s\" class=\"tag\">%s</a>", get_sub_field('kname', 'option'), get_sub_field('kname', 'option'));
            endwhile;
            ?>
            </div>
        </div>
    <?php endif;?>

    <div class="form-searchWrap">
        <p class="section-recipe--search-label">条件を選択して探す</p>
        <form class="form-search" action="/recipe" id="frmRecipe">
            <input type="hidden" name="keyclear" />
            <input type="hidden" name="view" value="<?php echo isset($_GET['view']) ? $_GET['view'] : ''?>" />
            <select class="input" id="select01" name="genre">
                <option value="">ジャンルから探す</option>
                <?php
                $recipe_cats = get_terms('recipe_category', 'hide_empty=0');
                if ( ! empty( $recipe_cats ) && ! is_wp_error( $recipe_cats ) ){
                    foreach ($recipe_cats as $recipe_cat) {
                        printf("<option value=\"%s\" %s>%s</option>", $recipe_cat->name, $_GET['genre'] == $recipe_cat->name && $_REQUEST['keyclear'] != $recipe_cat->name ? 'selected="true"': '', $recipe_cat->name);
                    }
                }
                ?>
            </select>                
            <select class="input" id="select02" name="course">
                <option value="-1">メニューから探す</option>
                <?php
                foreach ($menus as $k => $v) {
                    if ((($v['skip'] ?? false) === true) || $k === 'all')
                        continue;
                    if ($v['n'] != '') {
                        printf("<option value=\"%s\" %s>%s</option>", $k, $_GET['course'] == $k && $_REQUEST['keyclear'] != $v['n'] ? 'selected="true"' : '', $v['n']);
                    }
                }?>                    
            </select>
            <?php
            global $cooking_times;
            ?>
            <select class="input" id="select05" name="time">
                <option value="-1">調理時間から探す</option>
                <?php
                foreach($cooking_times as $time_k => $cooking_time)
                    printf("<option value=\"%s\" %s>%s</option>", $time_k, $_GET['time'] == $time_k && $_REQUEST['keyclear'] != $time_k ? 'selected="true"': '', $cooking_time);
                ?>                    
            </select>
            <select class="input active" id="select04" name="season">
                <option value="">季節から探す</option>
                <?php
                $seasions = ['春', '夏', '秋', '冬'];
                foreach($seasions as $seasion)
                    printf("<option value=\"%s\" %s>%s</option>", $seasion, $_GET['season'] == $seasion && !in_array($_REQUEST['keyclear'], $seasions) ? 'selected="true"': '', $seasion);
                ?>                    
            </select>
            <div class="form-action">
                <input class="btn-gray" type="submit" value="検索">
                <input class="btn-gray sliver" type="reset" value="リセット">
            </div>
        </form>
    </div>
</div>
<?php
$args = array('post_type' => 'recipe', 'posts_per_page' => 5, 'ignore_sticky_posts' => 1);
$popular_recipe_list = get_posts_views_ranking( $range = 'daily', $args, 'WP_Query' );
if ($popular_recipe_list->have_posts()):
?>
<div class="section-recipe--ranking">
    <p class="label-msg ranking"><span>人気レシピランキング</span></p>
    <ul class="ranking-list">
        <?php
        while ($popular_recipe_list->have_posts()) : $popular_recipe_list->the_post();
            $recipe_pos = get_field('recipe_display');
            ?>

        <li class="ranking-item">
            <?php if(!is_user_logged_in() && $recipe_pos === 'is_not_member') {?>
                <a href="javascript:void:(0)" data-remodal-target="modal-clip" class="link">
                    <div class="ranking-item--thumb">
                        <?php if (has_post_thumbnail()) : 
                            the_post_thumbnail('thumbnail', array('class' => 'cover'));
                        else: 
                            echo do_shortcode('[wprm-recipe-image size=\'thumbnail\' class=\'cover\']');
                        endif;?>
                    </div>
                    <div class="ranking-item--cnt">
                        <?php the_title()?><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-lock-orange.svg" style="width: 12px;margin-left: 2px;">
                    </div>
                </a>
            <?php } else {?>
                <a class="link" href="<?php the_permalink(); ?>">
                    <div class="ranking-item--thumb">
                        <?php if (has_post_thumbnail()) : 
                            the_post_thumbnail('thumbnail', array('class' => 'cover'));
                        else: 
                            echo do_shortcode('[wprm-recipe-image size=\'thumbnail\' class=\'cover\']');
                        endif;?>
                    </div>
                    <div class="ranking-item--cnt">
                        <?php the_title()?>
                    </div>
                </a>
            <?php }?>            
        </li>
        <?php
            endwhile; 
            wp_reset_query();
        ?>            
    </ul>
    <div class="section-recipe--ranking-viewmore">
        <a class="view-more link" href="/recipe/ranking/">レシピランキング一覧へ</a>
    </div>
</div>
<?php endif;?>

<?php if (is_active_sidebar('widget-main')) : ?>
    <div class="section-recipe--advers">
        <?php dynamic_sidebar('widget-main'); ?>
    </div>
<?php endif; ?>
    
<?php

function nk_type_menus() {
    return [
        '100' => [
            'n' => 'ごはんもの',
            's' => [
                'ごはんもの',
                'ご飯もの',
            ],
        ],
        '200' => [
            'n' => '麺類',
            's' => ['麺類'],
        ],
        '300' => [
            'n' => 'パン',
            's' => ['パン', '米粉パン', 'サンドイッチ'],
        ],
        '600' => [
            'n' => 'サラダ',
            's' => ['サラダ'],
        ],
        '400' => [
            'n' => 'メインのおかず',
            's' => ['主菜'],
        ],
        '500' => [
            'n' => '副菜',
            's' => ['副菜', '一品料理'],
        ],
        '700' => [
            'n' => 'スープ・汁物',
            's' => ['スープ', '汁物'],
        ],
        '800' => [
            'n' => '常備菜',
            's' => ['常備菜', '作り置き'],
        ],
        '900' => [
            'n' => 'もどき料理',
            's' => ['もどき料理'],
        ],
        '1000' => [
            'n' => 'スイーツ',
            's' => ['洋菓子', '和菓子', 'ドリンク'],
        ],
        '1100' => [
            'n' => '',
        ],
        'all' => [
            'n' => 'すべて'
        ],
    ];
}

function nk_type_ingredients() {

    return [
        '100' => [
            'n' => '根菜・芋類',
            'lists' => [
                '1' => [
                    'n' => 'じゃがいも',
                    's' => ['じゃがいも', 'じゃが芋'],
                    's_no' => [
                        '新じゃがいも',
                        '新じゃが芋',
                    ]
                ],
                '2' => [
                    'n' => '玉ねぎ',
                    's' => ['たまねぎ', '玉ねぎ', '玉葱'],
                    's_no' => [
                        '新たまねぎ',
                        '新玉ねぎ',
                        '新玉葱',
                    ]
                ],
                '3' => [
                    'n' => '人参',
                    's' => ['にんじん', '人参']
                ],
                '4' => [
                    'n' => '大根',
                    's' => ['だいこん', '大根']
                ],
                '5' => [
                    'n' => 'さつまいも',
                    's' => ['さつまいも', 'さつま芋', '薩摩芋']
                ],
                '6' => [
                    'n' => '里芋',
                    's' => ['さといも', 'さと芋', '里芋']
                ],
                '7' => [
                    'n' => '長いも',
                    's' => ['ながいも', '長芋', '長いも']
                ],
                '8' => [
                    'n' => 'ごぼう',
                    's' => ['ごぼう', '牛蒡']
                ],
                '9' => [
                    'n' => 'れんこん',
                    's' => ['れんこん', '蓮根']
                ],
                '10' => [
                    'n' => 'かぶ',
                ],
                '11' => [
                    'n' => 'にんにく',
                ],
                '12' => [
                    'n' => 'ビーツ',
                ],
                '13' => [
                    'n' => 'ラディッシュ',
                ],
                '14' => [
                    'n' => '菊芋',
                ],
                '15' => [
                    'n' => '山芋',
                    's' => ['やまいも', '山芋']
                ],
            ]
        ],
        '200' => [
            'n' => '葉物',
            'lists' => [
                '1' => [
                    'n' => 'ほうれん草',
                    's' => ['ほうれんそう', 'ほうれん草']
                ],
                '2' => [
                    'n' => '小松菜',
                    's' => ['こまつな', '小松菜']
                ],
                '3' => [
                    'n' => 'きゃべつ',
                    's_no' => [
                        '芽きゃべつ',
                        '春きゃべつ'
                    ]
                ],
                '4' => [
                    'n' => '白菜',
                    's' => ['はくさい', '白菜']
                ],
                '5' => [
                    'n' => 'ねぎ',
                    's' => ['ねぎ', '葱'],
                    's_no' => [
                        'たまねぎ',
                        '玉ねぎ',
                        '玉葱'
                    ]
                ],
                '6' => [
                    'n' => 'ニラ',
                    's_no' => ['バニラ']
                ],
                '7' => [
                    'n' => 'モロヘイヤ',
                ],
                '8' => [
                    'n' => '水菜',
                    's' => ['みずな', '水菜']
                ],
                '9' => [
                    'n' => '春菊',
                    's' => ['しゅんぎく', '春菊']
                ],
                '10' => [
                    'n' => 'チンゲン菜',
                ],
                '11' => [
                    'n' => '豆苗',
                    's' => ['そらまめ', 'そら豆', '空豆']
                ],
                '12' => [
                    'n' => '空芯菜',
                ],
                '13' => [
                    'n' => 'レタス',
                ],
                '14' => [
                    'n' => 'サンチュ',
                ],
                '15' => [
                    'n' => '大葉',
                ],
                '16' => [
                    'n' => 'ケール',
                ],
                '17' => [
                    'n' => 'からし菜',
                ],
                '18' => [
                    'n' => 'バジル',
                ],
                '19' => [
                    'n' => 'みつば',
                    's' => ['みつば', '三つ葉']
                ],
                '20' => [
                    'n' => '菜の花',
                    's' => ['なのはな', '菜の花']
                ],
                '21' => [
                    'n' => 'わさび菜',
                ],
            ]
        ],
        '300' => [
            'n' => '果菜',
            'lists' => [
                '1' => [
                    'n' => 'トマト',
                ],
                '2' => [
                    'n' => 'なす',
                    's' => ['なす', '茄子']
                ],
                '3' => [
                    'n' => 'きゅうり',
                    's' => ['きゅうり', '胡瓜']
                ],
                '4' => [
                    'n' => 'かぼちゃ',
                    's' => ['かぼちゃ', '南瓜']
                ],
                '5' => [
                    'n' => 'おくら',
                ],
                '6' => [
                    'n' => 'ブロッコリー',
                ],
                '7' => [
                    'n' => 'ピーマン',
                ],
                '8' => [
                    'n' => 'ズッキーニ',
                ],
                '9' => [
                    'n' => 'アボカド',
                ],
                '10' => [
                    'n' => '枝豆',
                    's' => ['えだまめ', '枝豆']
                ],
                '11' => [
                    'n' => 'ゴーヤ',
                ],
                '12' => [
                    'n' => 'とうもろこし',
                ],
                '13' => [
                    'n' => 'パプリカ',
                ],
                '14' => [
                    'n' => 'カリフラワー',
                ],
                '15' => [
                    'n' => 'みょうが',
                    's' => ['みょうが', '茗荷']
                ],
                '16' => [
                    'n' => 'アスパラガス',
                ],
            ]
        ],
        '400' => [
            'n' => 'きのこ・豆類',
            'lists' => [
                '1' => [
                    'n' => 'しいたけ',
                    's' => ['しいたけ', '椎茸']
                ],
                '2' => [
                    'n' => 'えりんぎ',
                ],
                '3' => [
                    'n' => '舞茸',
                    's' => ['まいたけ', '舞茸']
                ],
                '4' => [
                    'n' => 'えのき',
                ],
                '5' => [
                    'n' => 'しめじ',
                ],
                '6' => [
                    'n' => 'なめこ',
                ],
                '7' => [
                    'n' => 'ひよこ豆',
                    's' => ['ひよこまめ', 'ひよこ豆']
                ],
                '8' => [
                    'n' => 'レンズ豆',
                ],
                '9' => [
                    'n' => '大豆',
                ],
                '10' => [
                    'n' => 'もやし',
                ],
                '11' => [
                    'n' => 'えんどう',
                    's_no' => [
                        'スナップエンドウ',
                        'さやえんどう'
                    ]
                ],
                '12' => [
                    'n' => 'いんげん',
                ],
                '13' => [
                    'n' => 'スナップエンドウ',
                ],
                '14' => [
                    'n' => '空豆',
                    's' => ['そらまめ', 'そら豆', '空豆']
                ],
            ]
        ],
        '900' => [
            'n' => '大豆製品',
            'skip' => true,
            'lists' => [
                '1' => [
                    'n' => '豆腐'
                ],
                '2' => [
                    'n' => '厚揚げ'
                ],
                '3' => [
                    'n' => '油揚げ'
                ],
                '4' => [
                    'n' => '納豆'
                ],
                '5' => [
                    'n' => 'おから'
                ],
            ]
        ],
        '500' => [
            'n' => '春野菜',
            'lists' => [
                '1' => [
                    'n' => 'たけのこ',
                    's' => ['たけのこ', '筍', '竹の子']
                ],
                '2' => [
                    'n' => 'ふき',
                ],
                '3' => [
                    'n' => '菜の花',
                ],
                '4' => [
                    'n' => '春きゃべつ',
                ],
                '5' => [
                    'n' => '新じゃがいも',
                    's' => ['新じゃがいも', '新じゃが芋'],
                ],
                '6' => [
                    'n' => 'アスパラ',
                ],
                '7' => [
                    'n' => '枝豆',
                    's' => ['えだまめ', '枝豆']
                ],
                '8' => [
                    'n' => 'からし菜',
                ],
                '9' => [
                    'n' => 'グリーンピース',
                ],
                '10' => [
                    'n' => 'クレソン',
                ],
                '11' => [
                    'n' => 'さやえんどう',
                ],
                '12' => [
                    'n' => 'セロリ',
                ],
                '13' => [
                    'n' => 'そらまめ',
                    's' => ['そらまめ', 'そら豆', '空豆']
                ],
                '14' => [
                    'n' => '新たまねぎ',
                    's' => [
                        '新たまねぎ',
                        '新玉ねぎ',
                        '新玉葱',
                    ]
                ],
                '15' => [
                    'n' => 'にら',
                    's_no' => ['バニラ']
                ],
                '16' => [
                    'n' => '水菜',
                    's' => ['みずな', '水菜']
                ],
                '17' => [
                    'n' => 'みつば',
                    's' => ['みつば', '三つ葉']
                ],
                '18' => [
                    'n' => 'ルッコラ',
                ],
                '19' => [
                    'n' => 'トマト',
                ],
            ]
        ],
        '600' => [
            'n' => '夏野菜',
            'lists' => [
                '1' => [
                    'n' => 'うり',
                    's_no' => [
                        'きゅうり'
                    ]
                ],
                '2' => [
                    'n' => '枝豆',
                    's' => ['えだまめ', '枝豆']
                ],
                '3' => [
                    'n' => 'オクラ',
                ],
                '4' => [
                    'n' => 'きゃべつ',
                    's_no' => [
                        '芽きゃべつ',
                        '春きゃべつ'
                    ]
                ],
                '5' => [
                    'n' => 'きゅうり',
                    's' => ['きゅうり', '胡瓜']
                ],
                '6' => [
                    'n' => '空芯菜',
                ],
                '7' => [
                    'n' => 'ゴーヤ',
                ],
                '8' => [
                    'n' => 'さやいんげん',
                ],
                '9' => [
                    'n' => 'ししとう',
                ],
                '10' => [
                    'n' => 'ズッキーニ',
                ],
                '11' => [
                    'n' => 'とうもろこし',
                ],
                '12' => [
                    'n' => 'トマト',
                ],
                '13' => [
                    'n' => 'なす',
                    's' => ['なす', '茄子']
                ],
                '14' => [
                    'n' => 'ピーマン',
                ],
                '15' => [
                    'n' => 'みょうが',
                    's' => ['みょうが', '茗荷']
                ],
                '16' => [
                    'n' => 'モロヘイヤ',
                ],
                '17' => [
                    'n' => 'レタス',
                ],
            ]
        ],
        '700' => [
            'n' => '秋野菜',
            'lists' => [
                '1' => [
                    'n' => 'かぼちゃ',
                    's' => ['かぼちゃ', '南瓜']
                ],
                '2' => [
                    'n' => 'ごぼう',
                    's' => ['ごぼう', '牛蒡']
                ],
                '3' => [
                    'n' => 'さつまいも',
                    's' => ['さつまいも', 'さつま芋', '薩摩芋']
                ],
                '4' => [
                    'n' => '里芋',
                    's' => ['さといも', 'さと芋', '里芋']
                ],
                '5' => [
                    'n' => 'じゃがいも',
                    's' => ['じゃがいも', 'じゃが芋'],
                    's_no' => [
                        '新じゃがいも',
                        '新じゃが芋',
                    ]
                ],
                '6' => [
                    'n' => '玉ねぎ',
                    's' => ['たまねぎ', '玉ねぎ', '玉葱'],
                    's_no' => [
                        '新たまねぎ',
                        '新玉ねぎ',
                        '新玉葱',
                    ]
                ],
                '7' => [
                    'n' => 'にんじん',
                    's' => ['にんじん', '人参']
                ],
                '8' => [
                    'n' => 'ビーツ',
                ],
                '9' => [
                    'n' => '山芋',
                    's' => ['やまいも', '山芋']
                ],
                '10' => [
                    'n' => 'ルッコラ',
                ],
                '11' => [
                    'n' => 'れんこん',
                    's' => ['れんこん', '蓮根']
                ],
                '12' => [
                    'n' => 'しいたけ',
                    's' => ['しいたけ', '椎茸']
                ],
                '13' => [
                    'n' => 'しめじ',
                ],
                '14' => [
                    'n' => 'まいたけ',
                    's' => ['まいたけ', '舞茸']
                ],
                '15' => [
                    'n' => 'なめこ',
                ],
                '16' => [
                    'n' => '松茸',
                ],
                '17' => [
                    'n' => 'きくらげ',
                    's' => ['きくらげ', '木耳']
                ],
            ]
        ],
        '800' => [
            'n' => '冬野菜',
            'lists' => [
                '1' => [
                    'n' => 'かぶ',
                ],
                '2' => [
                    'n' => 'からし菜',
                ],
                '3' => [
                    'n' => 'カリフラワー',
                ],
                '4' => [
                    'n' => 'きゃべつ',
                    's_no' => [
                        '芽きゃべつ',
                        '春きゃべつ'
                    ]
                ],
                '5' => [
                    'n' => 'ごぼう',
                    's' => ['ごぼう', '牛蒡']
                ],
                '6' => [
                    'n' => '小松菜',
                    's' => ['こまつな', '小松菜']
                ],
                '7' => [
                    'n' => '里芋',
                    's' => ['さといも', 'さと芋', '里芋']
                ],
                '8' => [
                    'n' => '春菊',
                    's' => ['しゅんぎく', '春菊']
                ],
                '9' => [
                    'n' => '大根',
                    's' => ['だいこん', '大根']
                ],
                '10' => [
                    'n' => 'チンゲン菜',
                ],
                '11' => [
                    'n' => 'にんじん',
                    's' => ['にんじん', '人参']
                ],
                '12' => [
                    'n' => 'ねぎ',
                    's' => ['ねぎ', '葱'],
                    's_no' => [
                        'たまねぎ',
                        '玉ねぎ',
                        '玉葱'
                    ]
                ],
                '13' => [
                    'n' => '白菜',
                    's' => ['はくさい', '白菜']
                ],
                '14' => [
                    'n' => 'ブロッコリー',
                ],
                '15' => [
                    'n' => 'ほうれん草',
                    's' => ['ほうれんそう', 'ほうれん草']
                ],
                '16' => [
                    'n' => '水菜',
                    's' => ['みずな', '水菜']
                ],
                '17' => [
                    'n' => 'みつば',
                    's' => ['みつば', '三つ葉']
                ],
                '18' => [
                    'n' => 'れんこん',
                    's' => ['れんこん', '蓮根']
                ],
                '19' => [
                    'n' => '芽キャベツ',
                ],
            ]
        ],
        'all' => [
            'n' => 'すべて'
        ],
    ];
}

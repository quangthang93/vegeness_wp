<?php /* Template Name: レストラン検索 */ get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
                <?php wp_breadcrumb()?>
            </div>
        </div>
    </div><!--End .breadcrumbWrap-->
    <section class="section recipe rest">
        <div class="container">
            <div class="section-recipe--left fadeup2">
                <div class="p-restaurant">
                    <div class="sectionEP-head">
                        <div class="sectionEP-titleWrap">
                            <h1 class="sectionEP-title fork"><?php the_title()?></h1>
                        </div>
                    </div><!--End .sectionEP-head-->
                    <div class="section-recipe--row">
                        <div class="section-restaurant--head">
                            <p class="titleLv4 pc-only">キーワードから探す</p>
                            <form class="form-one" action="/shop" method="GET">
                                <input class="input big" type="text" name="keywork" placeholder="食材、店舗名、所在地など" required="required" />
                                <input class="search" type="submit" value="">
                            </form>
                        </div><!-- ./section-restaurant--head -->
                        <form class="section-restaurant--form" action="/shop" method="GET">
                            <div class="section-restaurant--form-cnt">
                                <div class="section-restaurant--form-col">
                                    <p class="section-recipe--search-label">エリアから探す</p>
                                    <div class="form-search">
                                        <?php
                                        $shop_area = get_terms('shop_area', 'hide_empty=0&parent=0');
                                        ?>
                                        <script type="text/javascript">
                                        /* <![CDATA[ */
                                            jQuery(document).ready(function($) {     
                                                $('#restaurant01').change(function(){
                                                    var prefID = $(this).val();
                                                    // call ajax
                                                    $.ajax({
                                                        url: "<?php echo admin_url('admin-ajax.php'); ?>",
                                                        type:'POST',
                                                        data:'action=pref__action&pref_ID=' + prefID,
                                                        success:function(results)
                                                        {
                                                            $("#restaurant02").html(results);
                                                        }
                                                     });
                                                });         
                                            });     
                                        /* ]]> */
                                        </script>
                                        <select class="input" id="restaurant01" name="pref[]">
                                            <option>都道府県</option>
                                            <?php
                                            if(!empty($shop_area) && !is_wp_error($shop_area)):
                                                foreach($shop_area as $shop_area_pref):
                                                    foreach (get_terms('shop_area', "hide_empty=0&parent={$shop_area_pref->term_id}") as $area_pref):
                                                        echo "<option value='{$area_pref->term_id}'>{$area_pref->name}</option>";
                                                    endforeach;
                                                endforeach;
                                            endif;
                                            ?>                                            
                                        </select>
                                        <select name="pref[]" class="input" id="restaurant02">
                                            <option>エリア</option>
                                        </select>                                        
                                    </div>
                                </div><!--End .section-restaurant--form-col-->
                                <div class="section-restaurant--form-col">
                                    <p class="section-recipe--search-label ctrl">ジャンルから探す<span class="js-ctrlToggle sp-only">開く</span></p>
                                    <ul class="section-restaurant--cboxList ctrl js-cboxList">
                                        <?php $j=0;
                                        $shop_genres = get_terms('shop_genre', 'hide_empty=0');
                                        foreach ($shop_genres as $shop_genre) { $j++;?>
                                            <li>
                                                <input name="genre[]" class="checkbox" id="genre<?php echo printf('%02d', $j)?>" type="checkbox" value="<?php echo $shop_genre->term_id?>">
                                                <label for="genre<?php echo printf('%02d', $j)?>"><?php echo $shop_genre->name?></label>
                                            </li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </div><!--End .section-restaurant--form-col-->
                                <div class="section-restaurant--form-col">
                                    <p class="section-recipe--search-label ctrl">シーンから探す<span class="js-ctrlToggle sp-only">開く</span></p>
                                    <ul class="section-restaurant--cboxList ctrl js-cboxList">
                                        <?php $j=0;
                                        $shop_icons = get_terms('shop_icon', 'hide_empty=0');
                                        foreach ($shop_icons as $shop_icon) { $j++;?>
                                            <li>
                                                <input name="icon[]" class="checkbox" id="icon<?php echo printf('%02d', $j)?>" type="checkbox" value="<?php echo $shop_icon->term_id?>">
                                                <label for="icon<?php echo printf('%02d', $j)?>"><?php echo $shop_icon->name?></label>
                                            </li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </div><!--End .section-restaurant--form-col-->
                            </div>
                            <div class="form-action">
                                <input class="btn-gray" type="submit" value="検索">
                                <button type="reset" class="btn-gray sliver">リセット</button>
                            </div>
                        </form>
                    </div>
                </div><!--End .p-restaurant-->
                <div class="p-restaurant--advers pc-only">
                    <p class="p-restaurant--advers-feeling">オーナー様必見！</p>
                    <p class="p-restaurant--advers-question">あなたのレストランを<br>vegenessに掲載しませんか？</p>
                    <a href="/registration" class="btn-view-more2" target="_blank">詳しくはこちら</a>
                </div>
            </div><!--End .section-recipe--left-->
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
                    <?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section>
    <?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>

<?php get_footer(); ?>

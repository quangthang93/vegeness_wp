<?php if ( have_rows( 'advs_code', 'option' ) ): ?>
	<section class="section advers">
		<div class="container">
			<div class="section-news--advers fadeup ">
				<?php while ( have_rows( 'advs_code', 'option' ) ): the_row();
					?>
					<?php the_sub_field( 'content', 'option' ) ?>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
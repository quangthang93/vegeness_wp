<?php if (get_field('btn_type') === 'type1'): ?>
    <div class="view-more-wrap">
        <a href="<?php the_field('btn_link') ?>" class="btn-view-more"><?php the_field('btn_title') ?></a>
    </div>
<?php elseif (get_field('btn_type') === 'type2') : ?>
    <div class="view-more-wrap">
        <a href="<?php the_field('btn_link') ?>" class="btn-view-more2"><?php the_field('btn_title') ?></a>
    </div>
<?php endif; ?>
<?php if (get_field('arrow_type') === 'send'): ?>
    <a href="<?php the_field('arrow_uri') ?>" class="link-go"></a>    
<?php elseif (get_field('arrow_type') === 'close') : ?>
    <a href="<?php the_field('arrow_uri') ?>" class="btn-close"></a>
<?php endif; ?>
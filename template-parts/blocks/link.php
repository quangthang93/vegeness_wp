<?php if (get_field('link_type') === 'out_link'): ?>
    <a class="link-icon blank" href="<?php the_field('link_name') ?>" target="_blank"><?php the_field('link_title') ?></a>
<?php elseif (get_field('link_type') === 'on_link') : ?>
    <a class="link-icon" href="<?php the_field('link_name') ?>"><?php the_field('link_title') ?></a>
<?php elseif (get_field('link_type') === 'file_link') : ?>
    <a class="link-icon pdf" href="<?php the_field('link_file') ?>" target="_blank"><?php the_field('link_title') ?></a>
<?php endif; ?>
<?php

/**
 * カスタム投稿タイプ登録
 */
function tcd_membership_member_news_init() {
	global $dp_options;

	$account_news_label = $dp_options['account_news_label'] ? $dp_options['account_news_label'] : __( 'News for member', 'tcd-w' );

	// メンバーニュース
	register_post_type( 'member_news', array(
		'label' => $account_news_label,
		'labels' => array(
			'name' => $account_news_label,
			'singular_name' => $account_news_label,
			'name' => $account_news_label,
			'singular_name' => $account_news_label,
			'add_new' => __( 'Add New Item', 'tcd-w' ),
			'add_new_item' => __( 'Add New Item', 'tcd-w' ),
			'edit_item' => __( 'Edit', 'tcd-w' ),
			'new_item' => __( 'New item', 'tcd-w' ),
			'view_item' => __( 'View Item', 'tcd-w' ),
			'search_items' => __( 'Search Items', 'tcd-w' ),
			'not_found' => __( 'Not Found', 'tcd-w' ),
			'not_found_in_trash' => __( 'Not found in trash', 'tcd-w' )
		),
		'public' => false,
		'publicly_queryable' => false,
		'menu_position' => 5,
		'show_ui' => true,
		'query_var' => false,
		'rewrite' => false,
		'capabilities' => array(
			'read_post' => 'read',
			'create_posts' => 'edit_theme_options',
			'edit_post' => 'edit_theme_options',
			'edit_posts' => 'edit_theme_options',
			'edit_others_posts' => 'edit_theme_options',
			'delete_post' => 'edit_theme_options',
			'delete_posts' => 'edit_theme_options',
			'publish_posts' => 'edit_theme_options',
			'read_private_posts' => 'edit_theme_options',
		),
		'has_archive' => false,
		'hierarchical' => false,
		'show_in_rest' => false,
		'supports' => array( 'editor' )
	) );
}
add_action( 'init', 'tcd_membership_member_news_init' );

/**
 * Member News カスタムカラム追加
 */
function tcd_membership_member_news_manage_posts_columns( $columns ) {
	$new_columns = array();
	foreach ( $columns as $column_name => $column_display_name ) {
		// title カラムの前に post_id カラムを追加
		if ( isset( $columns['title'] ) && $column_name == 'title' ) {
			$new_columns['post_id'] = 'ID';
		}
		if ( $column_name != 'new_post_thumb' ) {
			$new_columns[$column_name] = $column_display_name;
		}
	}
	return $new_columns;
}
add_filter( 'manage_member_news_posts_columns', 'tcd_membership_member_news_manage_posts_columns' );

/**
 * Member News 保存前にタイトル生成
 */
function tcd_membership_member_news_wp_insert_post_data( $data, $postarr ) {

	if ( 'member_news' === $data['post_type'] ) {
		if ( isset( $postarr['ID'] ) ) {
			$data['post_name'] = 'member_news-' . $postarr['ID'];
		}
		if ( ! empty( $data['post_content'] ) ) {
			$content = $data['post_content'];
			$content = preg_replace( '!<style.*?>.*?</style.*?>!is', '', $content );
			$content = preg_replace( '!<script.*?>.*?</script.*?>!is', '', $content );
			$content = strip_shortcodes( $content );
			$content = strip_tags( $content );
			$content = str_replace( ']]>', ']]&gt;', $content );
			$content = str_replace( array( "\r\n", "\r", "\n", "&nbsp;" ), " ", $content );
			$content = htmlspecialchars( $content, ENT_QUOTES );
			$content = wp_trim_words( $content, 100, '...' );
			$data['post_title'] = $content;
		} else {
			$data['post_title'] = '';
		}
	}

	return $data;
}
add_filter( 'wp_insert_post_data', 'tcd_membership_member_news_wp_insert_post_data', 10, 2 );

<?php

/**
 * TCD Membership scripts
 */
function tcd_membership_wp_enqueue_scripts() {
	global $dp_options, $tcd_membership_vars;

	wp_enqueue_script( 'tcd-membership', get_template_directory_uri() . '/js/membership.js', array( 'jquery', 'jquery-form' ), version_num(), true );

	$localize = array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'ajax_error_message' => __( 'Error was occurred. Please retry again.', 'tcd-w' ),
		'not_image_file' => __( 'Please choose the image file.', 'tcd-w' )
	);

	// メッセージ自動表示
	if ( isset( $_GET['message'] ) ) {
		// アカウントアクティベーションメール送信
		if (  'sent_registration_email_activation' === $_GET['message'] ) {
			$localize['auto_modal_message'] = str_replace( array( "\r\n", "\r", "\n"  ), '<br>', $dp_options['notice_sent_registration_email_activate'] );

		// 会員登録完了
		} elseif (  'registration_completed' === $_GET['message'] ) {
			$localize['auto_modal_message'] = __( 'Registration completed.', 'tcd-w' );
			if ( ! current_user_can( 'read' ) ) {
				$localize['auto_modal_message'] .= '<br>' . __( 'Please login.', 'tcd-w' );
			}
		// アカウント削除
		} elseif ( 'account_deleted' === $_GET['message'] && ! current_user_can( 'read' ) ) {
			$localize['auto_modal_message'] = __( 'Account deleted.', 'tcd-w' );
		}
	}

	wp_localize_script( 'tcd-membership', 'TCD_MEMBERSHIP', $localize );
}
add_action( 'wp_enqueue_scripts', 'tcd_membership_wp_enqueue_scripts' );

/**
 * モーダル出力
 */
function render_tcd_membership_modal() {
	global $dp_options;

	if ( ! current_user_can( 'read' ) ) :
		if ( ! empty( $_REQUEST['redirect_to'] ) ) :
			$redirect = $_REQUEST['redirect_to'];
		else :
			$redirect = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		endif;
		
?>
<div id="modal_overlay">
<?php
		// ログインモーダル
		tcd_membership_login_form( array(
			'form_id' => 'js-modal-login-form',
			'modal' => true,
			'redirect' => $redirect,
			'show_registration' => true
		) );

		// パスワード再発行モーダル
			tcd_membership_reset_password_form( array(
				'form_id' => 'js-modal-reset-password-form',
				'modal' => true
			) );

		// 会員登録モーダル
		if ( tcd_membership_users_can_register() ) :
			tcd_membership_registration_form( array(
				'form_id' => 'js-modal-registration-form',
				'modal' => true,
				'redirect' => $redirect
			) );
		endif;
?>
</div>
<?php
	endif;
}
add_action( 'wp_footer', 'render_tcd_membership_modal' );

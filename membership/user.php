<?php

/**
 * 購読者はアドミンバー非表示
 */
if ( ! current_user_can( 'edit_posts' ) ) {
	add_filter( 'show_admin_bar', '__return_false' );
}

/**
 * 管理画面 ユーザー一覧カスタムカラム
 */
function tcd_membership_manage_users_columns( $posts_columns ) {
	global $dp_options;

	if ( isset( $posts_columns['display_name'] ) ) {
		return $posts_columns;
	}

	$new_columns = array();

	foreach ( $posts_columns as $key => $value ) {
		if ( $key == 'name' ) {
			// 氏名欄の代わりにdisplay_nameを表示
			$new_columns['display_name'] = __( 'Display name', 'tcd-w' );
		} else {
			$new_columns[$key] = $value;
		}
	}

	return $new_columns;
}
add_filter( 'manage_users_columns', 'tcd_membership_manage_users_columns', 10 );

function tcd_membership_manage_users_custom_column( $output, $column_name, $user_id ) {
	global $dp_options, $wpdb;

	switch( $column_name ) {
		case 'display_name':
			$user = get_user_by( 'id', $user_id );
			if ( $user ) {
				return $user->display_name;
			}
			break;
	}
	return $output;
}
add_filter( 'manage_users_custom_column', 'tcd_membership_manage_users_custom_column', 10, 3 );

/**
 * 管理画面 ユーザー一覧の検索対象カラムフィルター
 */
function tcd_membership_user_search_columns( $search_columns, $search, $WP_User_Query ) {
	global $pagenow;
	$search_columns_default = array( 'user_login', 'user_nicename' );
	if ( $pagenow == 'users.php' && $search && $search_columns === $search_columns_default ) {
		$search_columns = array( 'user_login', 'user_nicename', 'user_email', 'display_name' );
	}
	return $search_columns;
}
add_filter( 'user_search_columns', 'tcd_membership_user_search_columns', 10, 3 );

/**
 * 会員登録可能か
 */
function tcd_membership_users_can_register() {
	global $dp_options;

	// マルチサイトの場合、マルチサイト側でユーザー登録可でも会員制オプションで不可の場合も考慮
	if ( is_multisite() ) {
		return get_option( 'users_can_register' ) && $dp_options['users_can_register'];

	// シングルサイト
	} else {
		return get_option( 'users_can_register' );
	}
}

/**
 * ログインURLフィルター
 */
function tcd_membership_login_url( $login_url, $redirect, $force_reauth ) {
	// 管理画面操作中の場合は変更なし
	if ( is_admin() ) {
		return $login_url;
	}

	$login_url = get_tcd_membership_memberpage_url( 'login' );

	if ( $redirect ) {
		$login_url = add_query_arg( 'redirect_to', rawurlencode( rawurldecode( $redirect ) ), $login_url );
	}

	if ( $force_reauth ) {
		$login_url = add_query_arg( 'reauth', '1', $login_url );
	}

	return $login_url;
}
add_filter( 'login_url', 'tcd_membership_login_url', 10, 3 );

/**
 * ログアウトURLフィルター
 */
function tcd_membership_logout_url( $logout_url, $redirect ) {
	if ( ! is_admin() ) {
		$logout_url = get_tcd_membership_memberpage_url( 'logout' );
		if ( $redirect ) {
			$logout_url = add_query_arg( 'redirect_to', rawurlencode( rawurldecode( $redirect ) ), $logout_url );
		}
	}
	return $logout_url;
}
add_filter( 'logout_url', 'tcd_membership_logout_url', 10, 2 );

/**
 * ユーザー登録URLフィルター
 */
function tcd_membership_register_url( $register_url ) {
	return get_tcd_membership_memberpage_url( 'registration' );
}
add_filter( 'register_url', 'tcd_membership_register_url', 10, 1 );

/**
 * パスワード再設定URLフィルター
 */
function tcd_membership_lostpassword_url( $lostpassword_url, $redirect ) {
	$lostpassword_url = get_tcd_membership_memberpage_url( 'reset_password' );
	if ( $redirect ) {
		$lostpassword_url = add_query_arg( 'redirect_to', rawurlencode( rawurldecode( $redirect ) ), $lostpassword_url );
	}
	return $lostpassword_url;
}
add_filter( 'lostpassword_url', 'tcd_membership_lostpassword_url', 10, 2 );

/**
 * ログインアクション
 */
function tcd_membership_action_login() {
	global $dp_options, $tcd_membership_vars;

	nocache_headers();

	// POST
	if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
		$user = wp_signon();
		$error_messages = array();

		if ( is_wp_error( $user ) ) {
			foreach ( $user->get_error_codes() as $code ) {
				switch ( $code ) {
					case 'empty_username' :
						$error_messages[] = __( 'Please enter an email address.', 'tcd-w' );
						break;

					case 'empty_password' :
						$error_messages[] = __( 'Please enter a password.', 'tcd-w' );
						break;

					case 'invalid_email' :
					case 'invalid_username' :
					case 'incorrect_password' :
						$error_messages[] = __( 'Email address or password is incorrect.', 'tcd-w' );
						break;

					case 'spammer_account' :
						$error_messages[] = __( 'Your account has been marked as a spammer.', 'tcd-w' );
						break;
				}
			}

			$tcd_membership_vars['login']['errors'] = $user;
			$tcd_membership_vars['login']['error_message'] = implode( '<br>', $error_messages );
			$user = false;

		} else {
			// ログイン成功時ログイン記憶があればメールアドレスをクッキー保存
			if ( ! empty( $_POST['log'] ) && ! empty( $_POST['rememberme'] ) ) {
				$tcd_login_email = wp_unslash( $_POST['log'] );
				// 可能なら暗号化
				if ( function_exists( 'openssl_decrypt' ) && function_exists( 'openssl_decrypt' ) && defined( 'NONCE_KEY' ) && NONCE_KEY ) {
					$tcd_login_email = openssl_encrypt( $tcd_login_email, 'AES-128-ECB', NONCE_KEY );
				}
				setcookie( 'tcd_login_email', $tcd_login_email, time() + YEAR_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN, is_ssl(), true );

			// ログイン記憶がなければクッキー削除
			} else {
				setcookie( 'tcd_login_email', '', time() - YEAR_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN, is_ssl(), true );
			}
		}

		// ログイン成功時はリダイレクト
		if ( $user ) {
			if ( ! empty( $_REQUEST['redirect_to'] ) ) {
				$redirect_to = $_REQUEST['redirect_to'];
			} else {
				$redirect_to = get_tcd_membership_memberpage_url( 'mypage' );
			}
			wp_safe_redirect( $redirect_to );
			exit;
		}

	// ログイン済みの場合はリダイレクト
	} elseif ( current_user_can( 'read' ) ) {
		if ( ! empty( $_REQUEST['redirect_to'] ) ) {
			$redirect_to = $_REQUEST['redirect_to'];
		} else {
			$redirect_to = user_trailingslashit( home_url() );
		}
		wp_safe_redirect( $redirect_to );
		exit;
	}

	if ( ! empty( $_REQUEST['message'] ) ) {
		switch ( $_REQUEST['message'] ) {
			case 'require_login' :
				$tcd_membership_vars['login']['message'] = __( 'Please login.', 'tcd-w' );
				break;
			case 'registration_completed' :
				$tcd_membership_vars['login']['message'] = __( 'Registration completed.', 'tcd-w' );
				break;
			case 'password_changed' :
				$tcd_membership_vars['login']['message'] = __( 'Password changed.', 'tcd-w' );
				break;
		}
	}
}
add_action( 'tcd_membership_action-login', 'tcd_membership_action_login' );

/**
 * ログアウトアクション
 */
function tcd_membership_action_logout() {
	if ( current_user_can( 'read' ) ) {
		$user = wp_get_current_user();
		wp_logout();
	}
	wp_safe_redirect( user_trailingslashit( home_url() ) );
	exit;
}
add_action( 'tcd_membership_action-logout', 'tcd_membership_action_logout' );

/**
 * 会員登録アクション
 */
function tcd_membership_action_registration() {
	global $dp_options, $tcd_membership_vars, $wpdb;

	nocache_headers();

	if ( ! tcd_membership_users_can_register() ) {
		$tcd_membership_vars['template'] = 'error';
		$tcd_membership_vars['error_message'] = __( 'Disable registration.', 'tcd-w' );
	}

	if ( current_user_can( 'read' ) ) {
		wp_safe_redirect( user_trailingslashit( home_url() ) );
		exit;
	}

	// POST
	if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
		$formdata = wp_unslash( $_POST );
		$error_messages = array();

		// validation
		if ( empty( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'tcd-membership-registration' ) ) {
			$error_messages[] = __( 'Invalid nonce token.', 'tcd-w' );
		} else {
			if ( empty( $formdata['display_name'] ) ) {
				$error_messages[] = __( 'Please enter a nickname.', 'tcd-w' );
			} elseif ( 2 > mb_strlen( $formdata['display_name'] ) || 50 < mb_strlen( $formdata['display_name'] ) ) {
				$error_messages[] = sprintf( __( 'Username must be between %d and %d characters length.', 'tcd-w' ), 2, 50 );
			} else {
				// 全角スペースありのtrim
				$formdata['display_name'] = preg_replace('/\A[\x00\s]++|[\x00\s]++\z/u', '', $formdata['display_name'] );

				// サニタイズ
				$formdata['display_name'] = tcd_membership_sanitize_content( $formdata['display_name'] );

				if ( 2 > mb_strlen( $formdata['display_name'] ) || 50 < mb_strlen( $formdata['display_name'] ) ) {
					$error_messages[] = sprintf( __( 'Username must be between %d and %d characters length.', 'tcd-w' ), 2, 50 );
				}
			}

			if ( empty( $formdata['email'] ) ) {
				$error_messages[] = __( 'Please enter an email address.', 'tcd-w' );
			} elseif ( ! is_email( $formdata['email'] ) ) {
				$error_messages[] = __( 'E-mail address format is incorrect.', 'tcd-w' );
			} elseif ( 100 < strlen( $formdata['email'] ) ) {
				$error_messages[] = __( 'E-mail address must be 100 characters or less.', 'tcd-w' );
			} elseif ( email_exists( $formdata['email'] ) ) {
				$error_messages[] = __( 'This email is already registered, please choose another one.', 'tcd-w' );
			}

			if ( empty( $formdata['password'] ) ) {
				$error_messages[] = __( 'Please enter a password.', 'tcd-w' );
			} elseif ( 8 > strlen( $formdata['password'] ) ) {
				$error_messages[] = sprintf( __( 'Password must be at least %d characters.', 'tcd-w' ), 8 );
			}
		}

		// エラーなし
		if ( ! $error_messages ) {
			// アカウントアクティベーションあり
			if ( $dp_options['use_registration_email_activation'] ) {
				$ts = current_time( 'timestamp', true );

				// オプションから仮会員情報取得
				$tcd_every_provisional_users = get_option( 'tcd_every_provisional_users', array() );

				// 以前のものがあれば削除
				if ( isset( $tcd_every_provisional_users[$formdata['email']] ) ) {
					unset( $tcd_every_provisional_users[$formdata['email']] );
				}

				$tcd_every_provisional_users[$formdata['email']]['email'] = $formdata['email'];
				$tcd_every_provisional_users[$formdata['email']]['display_name'] = $formdata['display_name'];

				if ( $dp_options['use_receive_email'] && ! empty( $formdata['receive_email'] ) && 'yes' === $formdata['receive_email'] ) {
					$tcd_every_provisional_users[$formdata['email']]['receive_email'] = 'yes';
				} else {
					$tcd_every_provisional_users[$formdata['email']]['receive_email'] = '';
				}

				// パスワードは可能なら暗号化
				if ( function_exists( 'openssl_encrypt' ) && function_exists( 'openssl_decrypt' ) && defined( 'NONCE_KEY' ) && NONCE_KEY ) {
					$tcd_login_email = 
					$tcd_every_provisional_users[$formdata['email']]['password'] = openssl_encrypt( $formdata['password'], 'AES-128-ECB', NONCE_KEY );
					$tcd_every_provisional_users[$formdata['email']]['password_encrypt'] = true;
				} else {
					$tcd_every_provisional_users[$formdata['email']]['password'] = $formdata['password'];
				}

				$tcd_every_provisional_users[$formdata['email']]['timestamp'] = $ts;

				// 重複しないトークン生成
				$activate_tokens = array();
				if ( $tcd_every_provisional_users ) {
					foreach ( $tcd_every_provisional_users as $value ) {
						if ( isset( $value['token_expire'] ) ) {
							$activate_tokens[] = $value['token_expire'];
						}
					}
				}
				do {
					$activate_token = wp_generate_password( 20, false, false );
				} while ( in_array( $activate_token, $activate_tokens, true ) );

				$tcd_every_provisional_users[$formdata['email']]['token'] = $activate_token;

				// トークン有効期限24時間
				$tcd_every_provisional_users[$formdata['email']]['token_expire'] = $ts + DAY_IN_SECONDS;

				// メール送信
				$replaces = array(
					'[user_email]' => $formdata['email'],
					'[user_display_name]' => $formdata['display_name'],
					'[user_name]' => $formdata['display_name'],
					'[password]' => $formdata['password'],
					'[account_activation_url]' => add_query_arg( 'activate_token', $activate_token, get_tcd_membership_memberpage_url( 'registration' ) )
				);

				if ( tcd_membership_mail( 'registration_activate', $formdata['email'], $replaces ) ) {
					// オプションに仮会員情報保存
					update_option( 'tcd_every_provisional_users', $tcd_every_provisional_users );

					// リダイレクト
					$redirect = add_query_arg( 'message', 'sent_registration_email_activation', user_trailingslashit( home_url() ) );
					wp_safe_redirect( $redirect );
					exit;
				} else {
					$error_messages[] = __( 'Failed to send mail.', 'tcd-w' );
				}

			// アカウントアクティベーションなし
			} else {
				// ユーザー追加
				$user_id = wp_insert_user( array(
					'display_name' => $formdata['display_name'],
					'nickname' => $formdata['display_name'],
					'user_login' => $formdata['email'],	// user_loginにはメールアドレスを使用
					'user_pass' => $formdata['password'],
					'user_email' => $formdata['email'],
					'user_nicename' => wp_generate_password( 20, false, false ),	// URLには使用しないので乱数
					'role' => apply_filters( 'tcd_membership_account_create_user_role', 'subscriber' )
				) );

				if ( is_wp_error( $user_id ) ) {
					$error_messages[] = __( 'Failed to create account.', 'tcd-w' );
					$error_messages = array_merge( $error_messages, $user_id->get_error_messages() );
				} elseif ( ! $user_id ) {
					$error_messages[] = __( 'Failed to create account.', 'tcd-w' );
				} else {
					$tcd_membership_vars['registration']['complete'] = true;

					// メタ保存
					// receive_email
					if ( $dp_options['use_receive_email'] && ! empty( $formdata['receive_email'] ) && 'yes' === $formdata['receive_email'] ) {
						update_user_meta( $user_id, 'receive_email', 'yes' );
					} else {
						update_user_meta( $user_id, 'receive_email', '' );
					}

					// メール送信 メール必須ではないので送信エラー処理は無し
					$replaces = array(
						'[user_email]' => $formdata['email'],
						'[user_display_name]' => $formdata['display_name'],
						'[user_name]' => $formdata['display_name'],
						'[password]' => $formdata['password'],
						'[login_url]' => get_tcd_membership_memberpage_url( 'login' ),
						'[mypage_url]' => get_tcd_membership_memberpage_url( 'mypage' ),
						'[reset_password_url]' => get_tcd_membership_memberpage_url( 'reset_password' )
					);
					tcd_membership_mail( 'registration', $formdata['email'], $replaces );
					tcd_membership_mail( 'registration', $dp_options['mail_registration_admin_to'], $replaces );

					// action hook
					do_action( 'tcd_membership_account_created', $user_id, $formdata );

					// ログインさせる
					$user = get_user_by( 'id', $user_id );
					if ( $user ) {
						wp_clear_auth_cookie();
						wp_set_current_user( $user->ID, $user->user_login );
						wp_set_auth_cookie( $user->ID );
						do_action( 'wp_login', $user->user_login );
					}
				}
			}

			// 成功時、再送を防ぐためリダイレクト
			if ( ! empty( $tcd_membership_vars['registration']['complete'] ) ) {
				if ( ! empty( $_REQUEST['redirect_to'] ) ) {
					$redirect = $_REQUEST['redirect_to'];
				} else {
					$redirect = get_tcd_membership_memberpage_url( 'mypage' );
				}
				$redirect = add_query_arg( 'message', 'registration_completed', $redirect );
				wp_safe_redirect( $redirect );
				exit;
			}
		}

		// エラーメッセージ
		if ( $error_messages ) {
			$tcd_membership_vars['registration']['error_message'] = implode( '<br>', $error_messages );
		} else {
			$tcd_membership_vars['registration']['complete'] = true;
			$tcd_membership_vars['registration']['complete_email'] = $formdata['email'];
		}

	// アカウントアクティベーション
	} elseif ( isset( $_GET['activate_token'] ) ) {
		$error_messages = array();

		if ( $dp_options['use_registration_email_activation'] ) {

			// オプションから仮会員情報取得
			$tcd_every_provisional_users = get_option( 'tcd_every_provisional_users', array() );
			$provisional_user = null;
			if ( $tcd_every_provisional_users ) {
				foreach ( $tcd_every_provisional_users as $value ) {
					if ( isset( $value['token'] ) && $value['token'] === $_GET['activate_token'] ) {
						$provisional_user = $value;
						break;
					}
				}
			}

			// 仮会員情報あり
			if ( $provisional_user ) {
				// 有効期限切れ
				if ( empty( $provisional_user['token_expire'] ) || current_time( 'timestamp', true ) > $provisional_user['token_expire'] ) {
					$error_messages[] = __( 'Expired token.', 'tcd-w' );
					$error_messages[] = __( 'Please re-registration.', 'tcd-w' );
				} else {
					// パスワード暗号化していれば復号化
					if ( ! empty( $provisional_user['password_encrypt'] ) && function_exists( 'openssl_decrypt' ) && defined( 'NONCE_KEY' ) && NONCE_KEY ) {
						$provisional_user['password'] = openssl_decrypt( $provisional_user['password'], 'AES-128-ECB', NONCE_KEY );
					}

					// ユーザー追加
					$user_id = wp_insert_user( array(
						'display_name' => $provisional_user['display_name'],
						'nickname' => $provisional_user['display_name'],
						'user_login' => $provisional_user['email'],	// user_loginにはメールアドレスを使用
						'user_pass' => $provisional_user['password'],
						'user_email' => $provisional_user['email'],
						'user_nicename' => wp_generate_password( 20, false, false ),	// URLには使用しないので乱数
						'role' => apply_filters( 'tcd_membership_account_create_user_role', 'subscriber' )
					) );

					if ( is_wp_error( $user_id ) ) {
					$error_messages[] = __( 'Failed to create account.', 'tcd-w' );
					$error_messages = array_merge( $error_messages, $user_id->get_error_messages() );
					} elseif ( ! $user_id ) {
						$error_messages[] = __( 'Failed to create account.', 'tcd-w' );
					} else {
						$tcd_membership_vars['registration']['complete'] = true;

						// メタ保存
						// receive_email
						if ( $dp_options['use_receive_email'] && ! empty( $provisional_user['receive_email'] ) && 'yes' === $provisional_user['receive_email'] ) {
							update_user_meta( $user_id, 'receive_email', 'yes' );
						} else {
							update_user_meta( $user_id, 'receive_email', '' );
						}

						// メール送信 メール必須ではないので送信エラー処理は無し
						$replaces = array(
							'[user_email]' => $provisional_user['email'],
							'[user_display_name]' => $provisional_user['display_name'],
							'[user_name]' => $provisional_user['display_name'],
							'[password]' => $provisional_user['password'],
							'[login_url]' => get_tcd_membership_memberpage_url( 'login' ),
							'[mypage_url]' => get_tcd_membership_memberpage_url( 'mypage' ),
							'[reset_password_url]' => get_tcd_membership_memberpage_url( 'reset_password' )
						);
						tcd_membership_mail( 'registration', $provisional_user['email'], $replaces );
						tcd_membership_mail( 'registration', $dp_options['mail_registration_admin_to'], $replaces );

						// action hook
						do_action( 'tcd_membership_account_created', $user_id, $provisional_user );

						// ログインさせる
						$user = get_user_by( 'id', $user_id );
						if ( $user ) {
							wp_clear_auth_cookie();
							wp_set_current_user( $user->ID, $user->user_login );
							wp_set_auth_cookie( $user->ID );
							do_action( 'wp_login', $user->user_login );
						}

						// 以前のものがあれば削除
						// 仮会員情報削除してオプション保存
						if ( isset( $tcd_every_provisional_users[$provisional_user['email']] ) ) {
							unset( $tcd_every_provisional_users[$provisional_user['email']] );
						}
						update_option( 'tcd_every_provisional_users', $tcd_every_provisional_users );


						// リダイレクト
						if ( ! empty( $_REQUEST['redirect_to'] ) ) {
							$redirect = $_REQUEST['redirect_to'];
						} else {
							$redirect = get_tcd_membership_memberpage_url( 'mypage' );
						}
						$redirect = add_query_arg( 'message', 'registration_completed', $redirect );
						wp_safe_redirect( $redirect );
						exit;
					}
				}

			// 仮会員情報なし
			} else {
				$error_messages[] = __( 'Invalid token.', 'tcd-w' );
			}

		} else {
			$error_messages[] = __( 'Invalid token.', 'tcd-w' );
		}

		// エラーメッセージ
		if ( $error_messages ) {
			$tcd_membership_vars['registration']['error_message'] = implode( '<br>', $error_messages );
		}
	}
}
add_action( 'tcd_membership_action-registration', 'tcd_membership_action_registration' );

/**
 * アカウント編集アクション
 */
function tcd_membership_action_edit_account() {
	global $dp_options, $tcd_membership_vars, $wpdb, $gender_options, $receive_options, $notify_options;

	nocache_headers();

	$error_messages = array();
	$user = wp_get_current_user();

	if ( ! $user ) {
		wp_safe_redirect( user_trailingslashit( home_url() ) );
		exit;
	}

	// POST
	if ( ! empty( $_POST['edit_account'] ) ) {
		$formdata = wp_unslash( $_POST );

		// validation
		if ( empty( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'tcd-membership-edit_account' ) ) {
			$error_messages[] = __( 'Invalid nonce token.', 'tcd-w' );
		} else {
			if ( empty( $formdata['display_name'] ) ) {
				$error_messages[] = __( 'Please enter a nickname.', 'tcd-w' );
			} elseif ( 2 > mb_strlen( $formdata['display_name'] ) || 50 < mb_strlen( $formdata['display_name'] ) ) {
				$error_messages[] = sprintf( __( 'Username must be between %d and %d characters length.', 'tcd-w' ), 2, 50 );
			} else {
				// 全角スペースありのtrim
				$formdata['display_name'] = preg_replace('/\A[\x00\s]++|[\x00\s]++\z/u', '', $formdata['display_name'] );

				// サニタイズ
				$formdata['display_name'] = tcd_membership_sanitize_content( $formdata['display_name'] );

				if ( 2 > mb_strlen( $formdata['display_name'] ) || 50 < mb_strlen( $formdata['display_name'] ) ) {
					$error_messages[] = sprintf( __( 'Username must be between %d and %d characters length.', 'tcd-w' ), 2, 50 );
				}
			}

			if ( empty( $formdata['email'] ) ) {
				$error_messages[] = __( 'Please enter an email address.', 'tcd-w' );
			} elseif ( $user->user_email !== $formdata['email'] ) {
				if ( ! is_email( $formdata['email'] ) ) {
					$error_messages[] = __( 'E-mail address format is incorrect.', 'tcd-w' );
				} elseif ( 100 < strlen( $formdata['email'] ) ) {
					$error_messages[] = __( 'E-mail address must be 100 characters or less.', 'tcd-w' );
				} elseif ( tcd_membership_user_field_exists( 'user_email', $formdata['email'], $user->ID ) ) {
					$error_messages[] = __( 'This email is already registered, please choose another one.', 'tcd-w' );
				}
			}

			// 前回アップロード削除 profile_image
			if ( isset( $formdata['uploaded_profile_image_url'] ) && ! $formdata['uploaded_profile_image_url'] ) {
				tcd_membership_delete_uploaded_image( $user->uploaded_profile_image, $user->ID );
			}

			// ファイルアップロード profile_image
			if ( ! empty( $_FILES['profile_image_file']['name'] ) ) {
				// 必要ファイルインクルード
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				require_once( ABSPATH . 'wp-admin/includes/media.php' );
				require_once( ABSPATH . 'wp-admin/includes/image.php' );

				// メディアにアップロード
				$upload = media_handle_upload( 'profile_image_file', 0 );

				// 成功
				if ( is_int( $upload ) ) {
					// 前回の一時アップロードがあれば削除
					if ( $user->uploaded_profile_image ) {
						tcd_membership_delete_uploaded_image( $user->uploaded_profile_image, $user->ID );
					}

					// 一時アップロードとしてユーザーメタに保存
					update_user_meta( $user->ID, 'uploaded_profile_image', $upload );

				// エラー
				} elseif ( is_wp_error( $upload ) ) {
					$error_messages[] = sprintf( __( 'Failed to upload %s. (%s)', 'tcd-w' ), __( 'Profile image', 'tcd-w' ), $upload->get_error_message() );
				} else {
					$error_messages[] = sprintf( __( 'Failed to upload %s.', 'tcd-w' ), __( 'Profile image', 'tcd-w' ) );
				}
			}

			if ( ! empty( $formdata['new_pass1'] ) || ! empty( $formdata['new_pass2'] ) ) {
				if ( $formdata['new_pass1'] !== $formdata['new_pass2'] ) {
					$error_messages[] = __( 'Please enter the same password in both new password fields.', 'tcd-w' );
				} elseif ( 8 > strlen( $formdata['new_pass1'] ) ) {
					$error_messages[] = sprintf( __( 'Password must be at least %d characters.', 'tcd-w' ), 8 );
				}
			}
		}

		// エラーがなければ更新
		if ( ! $error_messages ) {
			//パスワード変更時・メールアドレス変更時のメール送信はしないように
			add_filter( 'send_password_change_email', '__return_false', 100 );
			add_filter( 'send_email_change_email', '__return_false', 100 );

			$user_id = wp_update_user( array(
				'ID' => $user->ID,
				'display_name' => $formdata['display_name'],
				'nickname' => $formdata['display_name'],
				'user_email' => $formdata['email']
			) );

			if ( is_wp_error( $user_id ) ) {
				$error_messages[] = __( 'Failed to update account.', 'tcd-w' );
				$error_messages = array_merge( $error_messages, $user_id->get_error_messages() );
			} elseif ( ! $user_id ) {
				$error_messages[] = __( 'Failed to update account.', 'tcd-w' );
			} else {
				$tcd_membership_vars['edit_account']['complete'] = true;
				$re_login = false;

				// メーアドレス変更時にはuser_loginも更新
				if ( $user->user_email !== $formdata['email'] ) {
					$result = $wpdb->update(
						$wpdb->users,
						array(
							'user_login' => $formdata['email']
						),
						array(
							'ID' => $user->ID
						),
						array(
							'%s'
						),
						array(
							'%d',
						)
					);

					$re_login = true;
				}

				// 新しいパスワードセット
				if ( $formdata['new_pass1'] ) {
					reset_password( $user, $formdata['new_pass1'] );
					$re_login = true;
				}

				// メタ保存
				// receive_email
				if ( $dp_options['use_receive_email'] && ! empty( $formdata['receive_email'] ) && 'yes' === $formdata['receive_email'] ) {
					update_user_meta( $user_id, 'receive_email', 'yes' );
				} else {
					update_user_meta( $user_id, 'receive_email', '' );
				}

				// 画像削除フラグあり
				if ( ! empty( $formdata['delete-image-profile_image'] ) ) {
					tcd_membership_delete_uploaded_image( $user->profile_image, $user_id );
					tcd_membership_delete_uploaded_image( $user->uploaded_profile_image, $user_id );
					update_user_meta( $user_id, 'profile_image', '' );
					update_user_meta( $user_id, '_profile_image', '' );
					update_user_meta( $user_id, 'uploaded_profile_image', '' );

				// 一時アップロードがあれば反映
				} elseif ( $user->uploaded_profile_image ) {
					tcd_membership_delete_uploaded_image( $user->profile_image, $user_id );
					update_user_meta( $user_id, 'profile_image', $user->uploaded_profile_image );
					update_user_meta( $user_id, '_profile_image', '' );
					update_user_meta( $user_id, 'uploaded_profile_image', '' );
				}

				// 再ログインさせる
				if ( $re_login ) {
					clean_user_cache( $user );
					$user = get_user_by( 'id', $user->ID );
					if ( $user ) {
						wp_clear_auth_cookie();
						wp_set_current_user( $user->ID, $user->user_login );
						wp_set_auth_cookie( $user->ID );
						do_action( 'wp_login', $user->user_login );
					}
				}

				// action hook
				do_action( 'tcd_membership_account_updated', $user_id, $formdata );
			}
		}

		// 成功時、再送を防ぐためリダイレクト
		if ( ! empty( $tcd_membership_vars['edit_account']['complete'] ) ) {
			$redirect = get_tcd_membership_memberpage_url( 'mypage' );
			$redirect = add_query_arg( 'account_message', 'account_updated', $redirect );
			wp_safe_redirect( $redirect );
			exit;
		}

	// post以外
	} else {
		// 一時アップロードがあれば削除
		if ( $user->uploaded_profile_image ) {
			tcd_membership_delete_uploaded_image( $user->uploaded_profile_image, $user->ID );
			update_user_meta( $user->ID, 'uploaded_profile_image', '' );
		}

		// 成功時のリダイレクトでメッセージ表示
		if ( ! empty( $_REQUEST['account_message'] ) ) {
			switch ( $_REQUEST['account_message'] ) {
				case 'account_updated' :
					$tcd_membership_vars['edit_account']['message'] = __( 'Account Updated.', 'tcd-w' );
					break;
			}
		}
	}

	// エラーメッセージ
	if ( $error_messages ) {
		$tcd_membership_vars['edit_account']['error_message'] = implode( '<br>', $error_messages );
	}
}
// mypageでのアクションになるので注意
add_action( 'tcd_membership_action-mypage', 'tcd_membership_action_edit_account' );

/**
 * パスワード再発行アクション
 */
function tcd_membership_action_reset_password() {
	global $dp_options, $tcd_membership_vars, $wpdb;

	nocache_headers();

	$error_messages = array();

	// check token
	if ( isset( $_REQUEST['token'] ) ) {
		// tokenから該当ユーザーIDを取得
		$reset_password_token = wp_unslash( $_REQUEST['token'] );
		$sql = "SELECT user_id FROM {$wpdb->usermeta} WHERE meta_key = %s AND meta_value = %s LIMIT 1";
		$user_id = $wpdb->get_var( $wpdb->prepare( $sql, 'reset_password_token', $reset_password_token ) );
		if ( $user_id ) {
			$user = get_user_by( 'id', $user_id );
			$reset_password_token_expire = get_user_meta( $user_id, 'reset_password_token_expire', true );

			// 有効期限切れ
			if ( ! $reset_password_token_expire || current_time( 'timestamp', true ) > $reset_password_token_expire ) {
				$tcd_membership_vars['reset_password']['error_message'] = __( 'Expired token.', 'tcd-w' );
			// 該当ユーザー無し
			} elseif ( ! $user ) {
				$tcd_membership_vars['reset_password']['error_message'] = __( 'Not foud user.', 'tcd-w' );
			} else {
				$tcd_membership_vars['reset_password']['token'] = $reset_password_token;
				$tcd_membership_vars['reset_password']['user'] = $user;
			}
		} else {
			$tcd_membership_vars['reset_password']['error_message'] = __( 'Invalid token.', 'tcd-w' );
		}
	}

	// トークンエラーがある場合はここで終了
	if ( ! empty( $tcd_membership_vars['reset_password']['error_message'] ) ) {
		return;
	}

	// POST
	if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
		$formdata = wp_unslash( $_POST );

		// 正常なトークンがある場合は新しいパスワード入力
		if ( ! empty( $tcd_membership_vars['reset_password']['token'] ) && ! empty( $user ) ) {
			// validation
			if ( empty( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'tcd-membership-reset_password' ) ) {
				$error_messages[] = __( 'Invalid nonce token.', 'tcd-w' );
			} elseif ( empty( $formdata['new_pass1'] ) ) {
				$error_messages[] = __( 'Please enter a new password.', 'tcd-w' );
			} elseif ( 8 > strlen( $formdata['new_pass1'] ) ) {
				$error_messages[] = sprintf( __( 'Password must be at least %d characters.', 'tcd-w' ), 8 );
			} elseif ( empty( $formdata['new_pass2'] ) || $formdata['new_pass1'] !== $formdata['new_pass2'] ) {
				$error_messages[] = __( 'Please enter the same password in both new password fields.', 'tcd-w' );
			} else {
				// 新しいパスワードセット
				reset_password( $user, $formdata['new_pass1'] );

				// ユーザーメタ削除
				delete_user_meta( $user->ID, 'reset_password_token' );
				delete_user_meta( $user->ID, 'reset_password_token_expire' );

				// ログイン中なら再ログインさせる
				if ( current_user_can( 'read' ) ) {
					clean_user_cache( $user );
					$user = get_user_by( 'id', $user->ID );
					if ( $user ) {
						wp_clear_auth_cookie();
						wp_set_current_user( $user->ID, $user->user_login );
						wp_set_auth_cookie( $user->ID );
						do_action( 'wp_login', $user->user_login );
					}
				}

				// 再送を防ぐためリダイレクト
				if ( current_user_can( 'read' ) ) {
					$redirect = get_tcd_membership_memberpage_url( 'reset_password' );
				} else {
					$redirect = get_tcd_membership_memberpage_url( 'login' );
				}
				$redirect = add_query_arg( 'message', 'password_changed', $redirect );
				wp_safe_redirect( $redirect );
				exit;
			}

		// メールアドレス入力
		} else {
			// validation
			if ( empty( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'tcd-membership-reset_password' ) ) {
				$error_messages[] = __( 'Invalid nonce token.', 'tcd-w' );
			} elseif ( empty( $formdata['email'] ) ) {
				$error_messages[] = __( 'Please enter an email address.', 'tcd-w' );
			} elseif ( ! is_email( $formdata['email'] ) ) {
				$error_messages[] = __( 'E-mail address format is incorrect.', 'tcd-w' );
			} elseif ( 100 < strlen( $formdata['email'] ) ) {
				$error_messages[] = __( 'E-mail address must be 100 characters or less.', 'tcd-w' );
			} else {
				$user = get_user_by( 'email', $formdata['email'] );
				if ( ! $user ) {
					$error_messages[] = __( 'This email is not registered.', 'tcd-w' );
				} else {
					// 重複しないトークン生成
					$sql = "SELECT user_id FROM {$wpdb->usermeta} WHERE meta_key = %s AND meta_value = %s LIMIT 1";
					do {
						$reset_password_token = wp_generate_password( 20, false, false );
						$wpdb->flush();
					} while ( $wpdb->get_var( $wpdb->prepare( $sql, 'reset_password_token', $reset_password_token ) ) );

					// トークン・有効期限（24時間）をユーザーメタに保存
					update_user_meta( $user->ID, 'reset_password_token', $reset_password_token );
					update_user_meta( $user->ID, 'reset_password_token_expire', current_time( 'timestamp', true ) + DAY_IN_SECONDS );

					// メール送信
					$set_new_password_url = add_query_arg( 'token', $reset_password_token, get_tcd_membership_memberpage_url( 'reset_password' ) );
					$replaces = array(
						'[user_email]' => $formdata['email'],
						'[user_display_name]' => $user->display_name,
						'[user_name]' => $user->display_name,
						'[set_new_password_url]' => $set_new_password_url,
						'[reset_password_url]' => $set_new_password_url
					);
					if ( tcd_membership_mail( 'reset_password', $formdata['email'], $replaces ) ) {
						$tcd_membership_vars['reset_password']['sent_email'] = true;
					} else {
						$error_messages[] = __( 'Failed to send mail.', 'tcd-w' );
						delete_user_meta( $user->ID, 'reset_password_token' );
						delete_user_meta( $user->ID, 'reset_password_token_expire' );
					}
				}
			}

			// ajax
			if ( ! empty( $formdata['ajax_reset_password'] ) ) {
				$json = array(
					'success' => false
				);

				if ( $error_messages ) {
					$json['error_message'] = implode( '<br>', $error_messages );
				} else {
					$json['success'] = true;
					$json['message'] = __( 'Sent email to entered email address.<br>Please read email and set a new password.', 'tcd-w' );
				}

				// JSON出力
				wp_send_json( $json );
				exit;
			}

			// 成功時、再送を防ぐためリダイレクト
			if ( ! empty( $tcd_membership_vars['reset_password']['sent_email'] ) ) {
				$redirect = get_tcd_membership_memberpage_url( 'reset_password' );
				$redirect = add_query_arg( 'message', 'reset_password_sent_email', $redirect );
				wp_safe_redirect( $redirect );
				exit;
			}
		}

	} else {
		// メール送信成功時のリダイレクトでメッセージ表示
		if ( ! empty( $_REQUEST['message'] ) && 'reset_password_sent_email' === $_REQUEST['message'] ) {
			$tcd_membership_vars['reset_password']['message'] = __( 'Sent email to entered email address.<br>Please read email and set a new password.', 'tcd-w' );
		}

		// 新しいパスワード設定成功時のリダイレクトでメッセージ表示
		if ( ! empty( $_REQUEST['message'] ) && 'password_changed' === $_REQUEST['message'] ) {
			$tcd_membership_vars['reset_password']['complete'] = true;
			$tcd_membership_vars['reset_password']['message'] = __( 'Password changed.', 'tcd-w' );
		}
	}

	// エラーメッセージ
	if ( $error_messages ) {
		$tcd_membership_vars['reset_password']['error_message'] = implode( '<br>', $error_messages );
	}
}
add_action( 'tcd_membership_action-reset_password', 'tcd_membership_action_reset_password' );

/**
 * 退会アクション
 */
function tcd_membership_action_delete_account() {
	global $dp_options, $tcd_membership_vars;

	nocache_headers();

	$error_messages = array();
	$user = wp_get_current_user();

	if ( ! $user ) {
		wp_safe_redirect( user_trailingslashit( home_url() ) );
		exit;
	}

	// POST
	if ( ! empty( $_POST['delete_account'] ) ) {
		// validation
		if ( empty( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'tcd-membership-delete_account' ) ) {
			$error_messages[] = __( 'Invalid nonce token.', 'tcd-w' );
		} else {
			// メール送信 メール必須ではないので送信エラー処理は無し
			$replaces = array(
				'[user_email]' => $user->user_email,
				'[user_display_name]' => $user->display_name,
				'[user_name]' => $user->display_name
			);
			tcd_membership_mail( 'withdraw', $user->user_email, $replaces );
			tcd_membership_mail( 'withdraw_admin', $dp_options['mail_withdraw_admin_to'], $replaces );

			// プロフィール画像削除
			tcd_membership_delete_uploaded_image( $user->profile_image, $user->ID );

			// 削除実行
			require_once( ABSPATH . 'wp-admin/includes/user.php' );
			wp_delete_user( $user->ID );
			wp_logout();

			// action hook
			do_action( 'tcd_membership_account_deleted', $user->ID );

			// リダイレクト
			$redirect = user_trailingslashit( home_url() );
			$redirect = add_query_arg( 'message', 'account_deleted', $redirect );
			wp_safe_redirect( $redirect );
			exit;
		}
	}

	// エラーメッセージ
	if ( $error_messages ) {
		$tcd_membership_vars['delete_account']['error_message'] = implode( '<br>', $error_messages );
	}
}
// mypageでのアクションになるので注意
add_action( 'tcd_membership_action-mypage', 'tcd_membership_action_delete_account' );

/**
 * 重複するユーザーフィールドがあるか
 */
function tcd_membership_user_field_exists( $field, $value, $exclude_user_id = null ) {
	global $wpdb;

	if ( ! $field || ! $value ) {
		return true;
	}

	$sql = "SELECT ID FROM {$wpdb->users} WHERE {$field} = %s";
	$prepares = array( $value );

	if ( $exclude_user_id && is_numeric( $exclude_user_id ) ) {
		$sql .= " AND ID != %d";
		$prepares[] = $exclude_user_id;
	}

	$sql .= " LIMIT 1";

	return $wpdb->get_var( $wpdb->prepare( $sql, $prepares ) );
}

/**
 * マルチサイトの他サイトにログイン中でこのサイトのアクセス権がない場合にメッセージを返す
 */
function tcd_membership_multisite_other_site_logged_in_message() {
	if ( is_multisite() && is_user_logged_in() && ! current_user_can( 'read' ) ) {
		$ms_message = __( 'We detect you logged in other site on this network.<br>If you request login to this site, contact to administrator.', 'tcd-w' );
		return apply_filters( 'tcd_membership_multisite_other_site_logged_in_message', $ms_message );
	}
	return false;
}

/**
 * テキストの無害化
 */
function tcd_membership_sanitize_content( $content, $allowable_tags = '' ) {
	$content = preg_replace( '!<style.*?>.*?</style.*?>!is', '', $content );
	$content = preg_replace( '!<script.*?>.*?</script.*?>!is', '', $content );
	$content = strip_shortcodes( $content );
	$content = strip_tags( $content, $allowable_tags );
	$content = str_replace( ']]>', ']]&gt;', $content );
	return $content;
}

/**
 * プロフィール画像の削除
 */
function tcd_membership_delete_uploaded_image( $media_id, $user_id ) {
	if ( ! $media_id || ! $user_id ) {
		return false;
	}

	$post = get_post( $media_id );
	if ( ! $post || empty( $post->post_author ) ) {
		return false;
	}

	if ( intval( $post->post_author ) === intval( $user_id ) ) {
		return wp_delete_post( $post->ID, false );
	}

	return false;
}

<?php

/**
 * 購読者は管理画面にアクセスさせない
 */
function tcd_membership_admin_restrict() {
	// ajaxは除外
	if ( wp_doing_ajax() ) {
		return;
	}

	if ( ! current_user_can( 'edit_posts' ) ) {
		wp_safe_redirect( home_url( '/' ) );
		exit;
	}
}
add_action( 'admin_init', 'tcd_membership_admin_restrict' );

/**
 * wp-login.phpにアクセスさせない
 */
function tcd_membership_disable_wp_login_php() {
	global $dp_options, $pagenow;

	if ( 'wp-login.php' == $pagenow && $dp_options['disable_wp_login_php'] ) {
		// action指定の場合や管理画面操作中の再ログインの場合は何もしない
		if ( isset( $_REQUEST['action'] ) || isset( $_REQUEST['key'] ) || isset( $_REQUEST['interim-login'] ) ) {
			return;
		}

		// リダイレクト先
		if ( current_user_can( 'read' ) ) {
			$redirect = get_tcd_membership_memberpage_url( 'mypage' );
		} else {
			$redirect = get_tcd_membership_memberpage_url( 'login' );
		}

		if ( ! empty( $_REQUEST['redirect_to'] ) ) {
			$redirect = add_query_arg( 'redirect_to', rawurlencode( $_REQUEST['redirect_to'] ), $redirect );
		}

		wp_safe_redirect( $redirect );
		exit;
	}
}
add_action( 'init', 'tcd_membership_disable_wp_login_php' );

/**
 * メール送信
 */
function tcd_membership_mail( $mail_type, $mailto = null, $replaces = array() ) {
	global $dp_options;

	if ( ! $mailto ) {
		return false;
	}

	$subject = '';
	$body = '';

	switch ( $mail_type ) {
		case 'registration_activate' :
			$subject = $dp_options['mail_registration_activate_subject'];
			$body = $dp_options['mail_registration_activate_body'];
			break;

		case 'registration' :
			$subject = $dp_options['mail_registration_subject'];
			$body = $dp_options['mail_registration_body'];
			break;

		case 'registration_admin' :
			$subject = $dp_options['mail_registration_admin_subject'];
			$body = $dp_options['mail_registration_admin_body'];
			break;

		case 'reset_password' :
			$subject = $dp_options['mail_reset_password_subject'];
			$body = $dp_options['mail_reset_password_body'];
			break;

		case 'withdraw' :
			$subject = $dp_options['mail_withdraw_subject'];
			$body = $dp_options['mail_withdraw_body'];
			break;

		case 'withdraw_admin' :
			$subject = $dp_options['mail_withdraw_admin_subject'];
			$body = $dp_options['mail_withdraw_admin_body'];
			break;

		case 'member_news_notify' :
			$subject = $dp_options['mail_member_news_notify_subject'];
			$body = $dp_options['mail_member_news_notify_body'];
			break;

		default :
			return false;
			break;
	}

	if ( ! $subject && ! $body ) {
		return false;
	}

	// 置換デフォルト
	$replaces = (array) $replaces;
	if ( ! isset( $replaces['[blog_name]'] ) ) {
		$replaces['[blog_name]'] = get_bloginfo( 'name' );
	}
	if ( ! isset( $replaces['[blog_url]'] ) ) {
		$replaces['[blog_url]'] = get_bloginfo( 'url' );
	}

	// 置換
	foreach ( $replaces as $search => $replace ) {
		if ( is_int( $search ) || ! is_string( $search ) ) continue;
		$subject = str_replace( $search, $replace, $subject );
		$body = str_replace( $search, $replace, $body );
	}

	// 件名・本文フィルター
	$subject = apply_filters( 'tcd_membership_mail_subject-' . $mail_type, $subject, $replaces, $mailto );
	$body = apply_filters( 'tcd_membership_mail_body-' . $mail_type, $body, $replaces, $mailto );

	if ( ! $subject && ! $body ) {
		return false;
	}

	// 送信元フィルター追加
	add_filter( 'wp_mail_from', 'tcd_membership_wp_mail_from', 20 );
	add_filter( 'wp_mail_from_name', 'tcd_membership_wp_mail_from_name', 20 );

	// メール送信
	$result = wp_mail( $mailto, $subject, $body );

	// 送信元フィルター削除
	remove_filter( 'wp_mail_from', 'tcd_membership_wp_mail_from', 20 );
	remove_filter( 'wp_mail_from_name', 'tcd_membership_wp_mail_from_name', 20 );

	return $result;
}

/**
 * メール送信元メールアドレスフィルター
 */
function tcd_membership_wp_mail_from( $from_email ) {
	global $dp_options;
	if ( $dp_options['mail_from_email'] && is_email( $dp_options['mail_from_email'] ) ) {
		return $dp_options['mail_from_email'];
	}
	return $from_email;
}

/**
 * メール送信元名フィルター
 */
function tcd_membership_wp_mail_from_name( $from_name ) {
	global $dp_options;
	return $dp_options['mail_from_name'];
}

<?php

/**
 * ログインフォーム
 */
function tcd_membership_login_form( $args = array() ) {
	global $dp_options, $tcd_membership_vars;

	$default_args = array(
		'echo' => true,
		'form_id' => 'login-form',
		'label_username' => __( 'E-mail', 'tcd-w' ),
		'label_password' => __( 'Password', 'tcd-w' ),
		'label_remember' => __( 'Remember Me', 'tcd-w' ),
		'label_log_in' => __( 'Login', 'tcd-w' ),
		'modal' => false,
		'redirect' => ! empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '',
		'remember' => true,
		'value_username' => '',
		'value_remember' => false,
		'show_registration' => false
	);
	$args = wp_parse_args( $args, apply_filters( 'login_form_default_args', $default_args ) );
	$args = apply_filters( 'tcd_membership_login_form_args', $args );

	if ( ! $args['value_username'] && ! empty( $_COOKIE['tcd_login_email'] ) ) :
		$tcd_login_email = $_COOKIE['tcd_login_email'];
		// メールアドレスでなければ復号化
		if ( ! is_email( $tcd_login_email ) && function_exists( 'openssl_decrypt' ) && defined( 'NONCE_KEY' ) && NONCE_KEY ) :
			$tcd_login_email = openssl_decrypt( $tcd_login_email, 'AES-128-ECB', NONCE_KEY );
		endif;
		if ( $tcd_login_email && is_email( $tcd_login_email ) ) :
			$args['value_username'] = $tcd_login_email;
		endif;
	endif;

	if ( ! $args['echo'] ) :
		ob_start();
	endif;

	if ( $args['modal'] ) :
?>
	<div class="login_form_wrap form_wrap modal_wrap" id="login_modal_wrap">
<?php
	else :
?>
	<div class="login_form_wrap form_wrap">
<?php
	endif;
?>
<?php
	// マルチサイトの他サイトにログイン中でこのサイトのアクセス権がない場合はメッセージ表示して終了
	$ms_message = tcd_membership_multisite_other_site_logged_in_message();
	if ( $ms_message ) :
		echo '<div class="modal_contents">' . $ms_message . '</div>' . "\n";
	else :
?>
		<div class="login_form_area">
			<form id="<?php echo esc_attr( $args['form_id'] ); ?>" class="membership-form" action="<?php echo esc_attr( get_tcd_membership_memberpage_url( 'login' ) ); ?>" method="post">
				<h2 class="headline"><?php _e( 'Login', 'tcd-w' ); ?></h2>
<?php
		if ( ! empty( $tcd_membership_vars['login']['message'] ) ) :
?>
				<div class="form-message"><?php echo wpautop( $tcd_membership_vars['login']['message'] ); ?></div>
<?php
		endif;
		if ( ! empty( $tcd_membership_vars['login']['error_message'] ) ) :
?>
				<div class="form-error"><?php echo wpautop( $tcd_membership_vars['login']['error_message'] ); ?></div>
<?php
		endif;

		echo apply_filters( 'login_form_top', '', $args );
?>
				<div class="email">
					<input class="input_field" type="email" name="log" value="<?php echo esc_attr( isset( $_REQUEST['log'] ) ? $_REQUEST['log'] : $args['value_username'] ); ?>" placeholder="<?php echo esc_attr( $args['label_username'] ); ?>" required>
				</div>
				<div class="password">
					<input class="input_field" type="password" name="pwd" value="" placeholder="<?php echo esc_attr( $args['label_password'] ); ?>" required>
				</div>
<?php
		echo apply_filters( 'login_form_middle', '', $args );

		if ( $args['remember'] ) :
?>
				<div class="remember"><label><input name="rememberme" type="checkbox" value="forever"<?php if ( $args['value_remember'] ) echo ' checked'; ?>><?php echo esc_html( $args['label_remember'] ); ?></label></div>
<?php
		endif;
?>
				<div class="submit">
					<input type="submit" value="<?php echo esc_attr( $args['label_log_in'] ); ?>">
<?php
		if ( $args['redirect'] ) :
?>
					<input type="hidden" name="redirect_to" value="<?php echo esc_attr( $args['redirect'] ); ?>">
<?php
		endif;
?>
				</div>
				<a id="lost_password" href="<?php echo esc_attr( get_tcd_membership_memberpage_url( 'reset_password' ) ); ?>"><?php _e( 'Lost your password?', 'tcd-w' ); ?></a>
<?php
		echo apply_filters( 'login_form_bottom', '', $args );
?>
			</form>
		</div>
<?php
		if ( $args['show_registration'] && tcd_membership_users_can_register() ) :
?>
		<div class="register_button_area">
			<p><?php _e( 'Please create your account if you are not the member', 'tcd-w' ); ?></p>
			<a id="create_account" href="<?php echo esc_attr( get_tcd_membership_memberpage_url( 'registration' ) ); ?>"><?php _e( 'Create new account', 'tcd-w' ); ?></a>
		</div>
<?php
		endif;
	endif;

	if ( $args['modal'] ) :
?>
		<a class="close_modal_button" href="#">CLOSE</a>
<?php
	endif;
?>
	</div>
<?php
	if ( ! $args['echo'] ) :
		return ob_get_clean();
	endif;
}

/**
 * 会員登録フォーム
 */
function tcd_membership_registration_form( $args = array() ) {
	global $dp_options, $tcd_membership_vars;

	$default_args = array(
		'echo' => true,
		'form_id' => 'registration-form',
		'label_display_name' => __( 'Nickname', 'tcd-w' ),
		'label_email' => __( 'E-mail', 'tcd-w' ),
		'label_password' => __( 'Password', 'tcd-w' ),
		'label_receive_email' => $dp_options['label_receive_email'],
		'label_submit' => $dp_options['register_button_label'],
		'receive_email_default_value' => 'yes',
		'modal' => false,
		'redirect' => ! empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : ''
	);
	$args = wp_parse_args( $args, apply_filters( 'tcd_membership_registration_form_default_args', $default_args ) );
	$args = apply_filters( 'tcd_membership_registration_form_args', $args );

	if ( ! $args['echo'] ) :
		ob_start();
	endif;

	if ( $args['modal'] ) :
?>
	<div class="register_form_wrap form_wrap modal_wrap" id="register_modal_wrap">
<?php
	else :
?>
	<div class="register_form_wrap form_wrap">
<?php
	endif;
?>
<?php
	// マルチサイトの他サイトにログイン中でこのサイトのアクセス権がない場合はメッセージ表示して終了
	$ms_message = tcd_membership_multisite_other_site_logged_in_message();
	if ( $ms_message ) :
		echo '<div class="modal_contents">' . $ms_message . '</div>' . "\n";
	else :
		$image = wp_get_attachment_image_src( $dp_options['register_image'], 'full' );
?>
		<form id="<?php echo esc_attr( $args['form_id'] ); ?>" class="membership-form" action="<?php echo esc_attr( get_tcd_membership_memberpage_url( 'registration' ) ); ?>" method="post">
			<div class="register_form_header"<?php if ( ! empty( $image[0] ) ) : ?> style="background: url(<?php echo esc_attr( $image[0] ); ?>) no-repeat center center; background-size: cover;"<?php endif; ?>>
				<h2 class="headline" style="font-size: <?php echo esc_attr( $dp_options['register_headline_font_size'] ); ?>px;"><?php echo esc_html( $dp_options['register_headline'] ); ?></h2>
			</div>
			<div class="register_form_area">
<?php
		if ( $dp_options['register_catch'] ) :
?>
				<h3 class="catch" style="font-size:<?php echo esc_attr( $dp_options['register_catch_font_size'] ); ?>px;"><?php echo nl2br( $dp_options['register_catch'] ); ?></h3>
<?php
		endif;
		if ( ! empty( $tcd_membership_vars['registration']['error_message'] ) ) :
?>
				<div class="form-error"><?php echo wpautop( $tcd_membership_vars['registration']['error_message'] ); ?></div>
<?php
		endif;
?>
				<div class="name">
					<input class="input_field" type="text" name="display_name" value="<?php echo esc_attr( isset( $_REQUEST['display_name'] ) ? $_REQUEST['display_name'] : '' ); ?>" placeholder="<?php echo esc_attr( $args['label_display_name'] ); ?>" minlength="2" maxlength="50" required>
				</div>
				<div class="email">
					<input class="input_field" type="email" name="email" value="<?php echo esc_attr( isset( $_REQUEST['email'] ) ? $_REQUEST['email'] : '' ); ?>" placeholder="<?php echo esc_attr( $args['label_email'] ); ?>" maxlength="100" required>
				</div>
				<div class="password">
					<input class="input_field" type="password" name="password" value="<?php echo esc_attr( isset( $_POST['password'] ) ? $_POST['password'] : '' ); ?>" placeholder="<?php echo esc_attr( $args['label_password'] ); ?>" minlength="8" required>
				</div>
<?php
		if ( $dp_options['use_receive_email'] ) :
?>
				<div class="receive_email">
					<input type="hidden" name="receive_email" value="">
					<label><input type="checkbox" name="receive_email" value="yes"<?php if ( ( isset( $_POST['receive_email'] ) && 'yes' === $_POST['receive_email'] ) || ( ! isset( $_POST['receive_email'] ) && 'yes' === $args['receive_email_default_value'] ) ) echo ' checked'; ?>><?php echo esc_html( $args['label_receive_email'] ); ?></label>
				</div>
<?php
		endif;

		if ( $dp_options['show_register_policy'] && $dp_options['register_policy_url'] ) :
?>
				<p class="privacy_policy"><?php printf( __( 'When you register as a member, you agree to the <a href="%s" target="_blank">term of use</a>.', 'tcd-w' ), esc_url( $dp_options['register_policy_url'] ) ); ?></p>
<?php
		endif;
?>
				<div class="submit">
					<input type="submit" value="<?php echo esc_attr( $args['label_submit'] ); ?>">
					<input type="hidden" name="nonce" value="<?php echo esc_attr( wp_create_nonce( 'tcd-membership-registration' ) ); ?>">
<?php
		if ( $args['redirect'] ) :
?>
					<input type="hidden" name="redirect_to" value="<?php echo esc_attr( $args['redirect'] ); ?>">
<?php
		endif;
?>
				</div>
			</div>
		</form>
<?php
	endif;

	if ( $args['modal'] ) :
?>
		<a class="close_modal_button" href="#">CLOSE</a>
<?php
	endif;
?>
	</div>
<?php
	if ( ! $args['echo'] ) :
		return ob_get_clean();
	endif;
}

/**
 * パスワード再発行（メールアドレス入力）フォーム
 */
function tcd_membership_reset_password_form( $args = array() ) {
	global $dp_options, $tcd_membership_vars;

	$default_args = array(
		'echo' => true,
		'form_id' => 'reset-password-form',
		'label_email' => __( 'E-mail', 'tcd-w' ),
		'label_submit' => __( 'Send E-mail', 'tcd-w' ),
		'modal' => false
	);
	$args = wp_parse_args( $args, apply_filters( 'tcd_membership_reset_password_form_default_args', $default_args ) );
	$args = apply_filters( 'tcd_membership_reset_password_form_args', $args );

	if ( ! $args['echo'] ) :
		ob_start();
	endif;

	if ( $args['modal'] ) :
?>
	<div class="password_form_wrap form_wrap modal_wrap" id="password_modal_wrap">
<?php
	else :
?>
	<div class="password_form_wrap form_wrap">
<?php
	endif;
?>
<?php
	// マルチサイトの他サイトにログイン中でこのサイトのアクセス権がない場合はメッセージ表示して終了
	$ms_message = tcd_membership_multisite_other_site_logged_in_message();
	if ( $ms_message ) :
		echo '<div class="modal_contents">' . $ms_message . '</div>' . "\n";
	else :
?>
		<div class="password_form_area">
			<form id="<?php echo esc_attr( $args['form_id'] ); ?>" class="membership-form" action="<?php echo esc_attr( get_tcd_membership_memberpage_url( 'reset_password' ) ); ?>" method="post">
				<h2 class="headline"><?php _e( 'Rest password', 'tcd-w' ); ?></h2>
<?php
		if ( ! empty( $tcd_membership_vars['reset_password']['message'] ) ) :
?>
				<div class="form-message"><?php echo wpautop( $tcd_membership_vars['reset_password']['message'] ); ?></div>
<?php
		endif;
		if ( ! empty( $tcd_membership_vars['reset_password']['error_message'] ) ) :
?>
				<div class="form-error"><?php echo wpautop( $tcd_membership_vars['reset_password']['error_message'] ); ?></div>
<?php
		endif;
?>
				<p><?php _e( 'Please enter a registerd email address.<br>Will send you a reset password email to the entered email address.', 'tcd-w' ); ?></p>
				<div class="email">
					<input class="input_field" type="email" name="email" value="<?php echo esc_attr( isset( $_REQUEST['email'] ) ? $_REQUEST['email'] : '' ); ?>" placeholder="<?php echo esc_attr( $args['label_email'] ); ?>" required>
				</div>
				<div class="submit">
					<input type="submit" value="<?php echo esc_attr( $args['label_submit'] ); ?>">
					<input type="hidden" name="nonce" value="<?php echo esc_attr( wp_create_nonce( 'tcd-membership-reset_password' ) ); ?>">
				</div>
			</form>
		</div>
<?php
	endif;

	if ( $args['modal'] ) :
?>
		<a class="close_modal_button" href="#">CLOSE</a>
<?php
	endif;
?>
	</div>
<?php
	if ( ! $args['echo'] ) :
		return ob_get_clean();
	endif;
}

/**
 * アカウント編集フォーム
 */
function tcd_membership_edit_account_form( $args = array() ) {
	global $dp_options, $tcd_membership_vars, $gender_options, $receive_options, $notify_options;

	$default_args = array(
		'echo' => true,
		'form_id' => 'edit-account-form',
		'label_display_name' => __( 'Nickname', 'tcd-w' ),
		'label_email' => __( 'E-mail', 'tcd-w' ),
		'label_receive_email' => $dp_options['label_receive_email'],
		'label_profile_image' => __( 'Profile image', 'tcd-w' ),
		'label_change_password' => __( 'Change password', 'tcd-w' ),
		'placeholder_new_password' => __( 'New Password', 'tcd-w' ),
		'placeholder_new_password_confirmation' => __( 'New Password Confirmation', 'tcd-w' ),
		'label_submit' => __( 'Update', 'tcd-w' ),
		'show_delete_account' => true,
		'delete_account_confirm_message' => __( 'Are you sure you want to withdraw from this site?', 'tcd-w' ),
		'delete_account_form_id' => 'delete-account-form',
		'label_delete_account' => __( 'Withdrawal', 'tcd-w' ),
	);
	$args = wp_parse_args( $args, $default_args );
	$args = apply_filters( 'tcd_membership_edit_account_form_args', $args );

	// ユーザー変数
	$user = wp_get_current_user();
	$user_id = $user->ID;
	$user_email = $user->user_email;

	if ( $dp_options['use_receive_email'] ) {
		$receive_email = $user->receive_email;
	}

	if ( ! empty( $user->display_name ) ) {
		$display_name = $user->display_name;
	} else {
		$display_name = $user->user_nicename;
	}

	if ( $user->profile_image ) {
		$profile_image_url = get_avatar_url( $user->ID, array( 'size' => 300 ) );
	} else {
		$profile_image_url = null;
	}
	$old_profile_image_url = $profile_image_url;

	$uploaded_profile_image = $user->uploaded_profile_image;
	$uploaded_profile_image_url = null;

	if ( $uploaded_profile_image ) {
		$uploaded_profile_image_url = wp_get_attachment_url( $uploaded_profile_image, 'size2' );
		if ( $uploaded_profile_image_url ) {
			$profile_image_url = $uploaded_profile_image_url;
		}
	}

	// POSTの場合、ユーザー変数上書き
	if ( isset( $_POST['edit_account'] ) ) {
		if ( isset( $_POST['display_name'] ) ) {
			$display_name = $_POST['display_name'];
		}
		if ( isset( $_POST['email'] ) ) {
			$user_email = $_POST['email'];
		}
		if ( $dp_options['use_receive_email'] && isset( $_POST['receive_email'] ) ) {
			$receive_email = $_POST['receive_email'];
		}
	}

	if ( ! $args['echo'] ) :
		ob_start();
	endif;
?>
	<div class="edit_account_form_wrap form_wrap">
		<form id="<?php echo esc_attr( $args['form_id'] ); ?>" class="membership-form" action="<?php echo esc_attr( get_tcd_membership_memberpage_url( 'mypage' ) ); ?>" enctype="multipart/form-data" method="post">
			<h3 class="headline"><?php _e( 'Edit account', 'tcd-w' ); ?></h3>
<?php
	if ( ! empty( $tcd_membership_vars['edit_account']['message'] ) ) :
?>
			<div class="form-message"><?php echo wpautop( $tcd_membership_vars['edit_account']['message'] ); ?></div>
<?php
	endif;
	if ( ! empty( $tcd_membership_vars['edit_account']['error_message'] ) ) :
?>
			<div class="form-error"><?php echo wpautop( $tcd_membership_vars['edit_account']['error_message'] ); ?></div>
<?php
	endif;
?>
			<dl>
				<dt><?php echo esc_html( $args['label_display_name'] ); ?></dt>
				<dd><input class="input_field" type="text" name="display_name" placeholder="<?php echo esc_attr( $args['label_display_name'] ); ?>" value="<?php echo esc_attr( $display_name ); ?>" minlength="2" maxlength="50" required></dd>
				<dt><?php echo esc_html( $args['label_email'] ); ?></dt>
				<dd>
					<input class="input_field" type="email" name="email" placeholder="<?php echo esc_attr( $args['label_email'] ); ?>" value="<?php echo esc_attr( $user_email ); ?>" maxlength="100" required>
<?php
		if ( $dp_options['use_receive_email'] ) :
?>
					<input type="hidden" name="receive_email" value="">
					<label class="receive_email"><input type="checkbox" name="receive_email" value="yes"<?php if ( 'yes' === $receive_email ) echo ' checked'; ?>><?php echo esc_html( $args['label_receive_email'] ); ?></label>
<?php
		endif;
?>
				</dd>
				<dt><?php echo esc_html( $args['label_change_password'] ); ?></dt>
				<dd class="change_password">
					<input class="input_field" type="password" name="new_pass1" value="<?php if ( isset( $_POST['new_pass1'] ) ) echo esc_attr( $_POST['new_pass1'] ); ?>" placeholder="<?php echo esc_attr( $args['placeholder_new_password'] ); ?>" minlength="8"><br>
					<input class="input_field" type="password" name="new_pass2" value="<?php if ( isset( $_POST['new_pass2'] ) ) echo esc_attr( $_POST['new_pass2'] ); ?>" placeholder="<?php echo esc_attr( $args['placeholder_new_password_confirmation'] ); ?>">
				</dd>
				<dt class="image_area"><?php echo esc_html( $args['label_profile_image'] ); ?></dt>
				<dd class="image_area">
					<div class="image">
						<div class="image_current"<?php if ( $profile_image_url ) echo ' style="background-image: url(' . esc_attr( $profile_image_url ) . ');"'; ?>></div>
						<div class="image_bg"<?php if ( $profile_image_url ) echo ' style="display: none;"'; ?>></div>
						<a class="delete_button" href="#"></a>
					</div>
					<a class="upload_button" href="#"><?php _e( 'Select image', 'tcd-w' ); ?></a>
					<input type="file" name="profile_image_file">
					<input type="hidden" name="profile_image_url" value="<?php echo esc_attr( $old_profile_image_url ); ?>">
<?php
	if ( $uploaded_profile_image_url ) :
?>
					<input type="hidden" name="uploaded_profile_image_url" value="<?php echo esc_attr( $uploaded_profile_image_url ); ?>">
<?php
endif;
?>
				</dd>
			</dl>
			<div class="submit">
				<input type="submit" name="edit_account" value="<?php echo esc_attr( $args['label_submit'] ); ?>">
				<input type="hidden" name="nonce" value="<?php echo esc_attr( wp_create_nonce( 'tcd-membership-edit_account' ) ); ?>">
			</div>
		</form>
<?php
	if ( $args['show_delete_account'] && $args['label_delete_account'] ) :
?>
		<form id="<?php echo esc_attr( $args['delete_account_form_id'] ); ?>"  class="membership-form" action="<?php echo esc_attr( get_tcd_membership_memberpage_url( 'mypage' ) ); ?>" method="post" data-confirm="<?php echo esc_attr( $args['delete_account_confirm_message'] ); ?>">
<?php
if ( ! empty( $tcd_membership_vars['delete_account']['error_message'] ) ) :
?>
			<div class="form-error"><?php echo wpautop( $tcd_membership_vars['delete_account']['error_message'] ); ?></div>
<?php
endif;
?>
			<div class="submit">
				<input id="delete_account" type="submit" name="delete_account" value="<?php echo esc_attr( $args['label_delete_account'] ); ?>">
				<input type="hidden" name="nonce" value="<?php echo esc_attr( wp_create_nonce( 'tcd-membership-delete_account' ) ); ?>">
			</div>
		</form>
<?php
	endif;
?>
	</div>
<?php
	if ( ! $args['echo'] ) :
		return ob_get_clean();
	endif;
}

<?php get_header(); ?>
<?php
    $queries_obj = get_queried_object();
?>
<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
                <?php wp_breadcrumb() ?>
            </div>
        </div>
    </div>

    <section class="section recipe blog">
        <div class="container">
            <div class="section-recipe--left fadeup2">
                <div class="p-restaurant">
                    <div class="sectionEP-head">
                        <div class="sectionEP-titleWrap">
                            <h1 class="sectionEP-title pen"><?php echo $queries_obj->name?></h1>
                        </div>
                    </div>
                    <div class="section-recipe--row">
                        <div class="blog-cnt">
                            <div class="blog-list">
                                <?php
                                $conditions = array(
                                    array(
                                        'taxonomy' => $queries_obj->taxonomy,
                                        'field' => 'term_id',
                                        'terms' => $queries_obj->term_id,
                                    )
                                );
                                
                                $args['post_type'] = 'blog';
                                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                $args['paged'] = $paged;
                                $args['post_status'] = 'publish';
                                $args['orderby'] = 'date';
                                $args['order'] = 'DESC';
                                $args['posts_per_page'] = 8;
                                $args['tax_query'] = $conditions;

                                $blog_query = null;
                                $blog_query = new WP_Query($args);
                                if ($blog_query->have_posts()): while ($blog_query->have_posts()) : $blog_query->the_post();
                                        $blog_terms = get_the_terms(get_the_ID(), 'blog_category');                                        
                                        ?>
                                
                                        <div class="blog-item">
                                            <div class="blog-item--thumb">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php if(in_array('pr_status', get_field('pr_icon'))):?>
                                                        <span class="blog-item--thumb-label">PR</span>
                                                    <?php endif;?>
                                                    <?php if ( has_post_thumbnail() ) : ?>
                                                        <?php the_post_thumbnail('medium_large'); ?>
                                                    <?php else:?>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-img.jpg" />
                                                    <?php endif; ?>
                                                </a>
                                            </div>
                                            <div class="blog-item--infor">
                                                <div class="blog-item--dateWrap">
                                                    <span class="date big"><?php the_time('Y.m.d'); ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d')))?>]</span>
                                                    <span class="label"><?php echo esc_html( $blog_terms[0]->name );?></span>
                                                </div>
                                                <h3 class="titleLv3 type2"><a href="<?php the_permalink(); ?>"><?php the_title()?></a></h3>
                                                <p class="desc2"><?php echo mb_substr(get_the_excerpt(), 0, 54); ?><a href="<?php the_permalink(); ?>" class="link-blog2">… [続きを読む]</a></p>
                                            </div>
                                        </div>
                                        
                                    <?php 
                                    endwhile;
                                    wp_reset_postdata();
                                    ?>
                                <?php endif; ?>
                            </div><!--End .blog-list-->
                            <?php simple_navigation()?>
                        </div>
                    </div>
                </div><!--End .p-restaurant-->                
            </div><!-- ./section-recipe--left -->
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
                    <?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section>
    <?php get_footer('blog'); ?>
    <?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>

<?php get_footer(); ?>

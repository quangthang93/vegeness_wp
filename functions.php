<?php
define( 'THEME_NAME', 'DEMO_THEME' );

load_textdomain( 'DEMO_THEME', dirname( __FILE__ ) . '/languages/' . get_locale() . '.mo' );

// アクセス数
require_once ( dirname( __FILE__ ) . '/views.php' );
require_once ( 'function_recipe.php' );

if ( function_exists( 'add_theme_support' ) ) {
	// Add Menu Support
	add_theme_support( 'menus' );

	// Add Thumbnail Theme Support
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
}

$cooking_times = [
    5 => '5分以内',
    10 => '10分以内',
    15 => '15分以内',
    20 => '20分以内',
    30 => '30分以内',
    60 => '60分以内',
    61 => '61分以上',
];

/* ------------------------------------*\
  Functions
  \*------------------------------------ */

function footer_nav() {
	$menu_name	 = 'footer-menu';
	if ( ( $locations	 = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
		$menu		 = wp_get_nav_menu_object( $locations[ $menu_name ] );
		$menu_items	 = wp_get_nav_menu_items( $menu->term_id );

		$menu_list = '<ul class="footer-nav01">';
		foreach ( (array) $menu_items as $key => $menu_item ) {
			if ( $menu_item->menu_item_parent == 0 ) {
				if ( $menu_item->target != '' ) {
					$l_target = "target='{$menu_item->target}'";
				}
				$menu_list .= '<li class="footer-nav01--item"><a href="' . $menu_item->url . '" ' . $l_target . ' class="link">' . $menu_item->title . '</a></li>';
			}
		}
		$menu_list .= '</ul>';
	}
	return $menu_list;
}

function footer_nav2() {
	$menu_name	 = 'footer-menu2';
	if ( ( $locations	 = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
		$menu		 = wp_get_nav_menu_object( $locations[ $menu_name ] );
		$menu_items	 = wp_get_nav_menu_items( $menu->term_id );

		$menu_list = '<ul class="footer-nav02">';
		foreach ( (array) $menu_items as $key => $menu_item ) {
			if ( $menu_item->menu_item_parent == 0 ) {
				if ( $menu_item->target != '' ) {
					$l_target = "target='{$menu_item->target}'";
				}
				$menu_list .= '<li class="footer-nav02--item"><a href="' . $menu_item->url . '" ' . $l_target . ' class="link">' . $menu_item->title . '</a></li>';
			}
		}
		$menu_list .= '</ul>';
	}
	return $menu_list;
}

//Remove JQuery migrate
function remove_jquery_migrate( $scripts ) {
	if ( !is_admin() && isset( $scripts->registered[ 'jquery' ] ) ) {
		$script = $scripts->registered[ 'jquery' ];

		if ( $script->deps ) { // Check whether the script has any dependencies
			$script->deps = array_diff( $script->deps, array(
				'jquery-migrate'
			) );
		}
	}
}

add_action( 'wp_default_scripts', 'remove_jquery_migrate' );

//Upgrade jQuery version
function sof_load_scripts() {
	if ( !is_admin() ) {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', get_template_directory_uri() . '/assets/js/lib/jquery-3.5.1.min.js', '3.5.1', false );
		wp_enqueue_script( 'jquery' );
	}
}

add_action( 'wp_enqueue_scripts', 'sof_load_scripts' );

function load_styles() {
	global $post;
	wp_enqueue_style( 'style', get_stylesheet_uri() );


	if ( ('blog' == $post->post_type && is_singular( 'blog' ) ) ) {
		wp_enqueue_style( 'css_reset', get_stylesheet_directory_uri() . "/assets/css/reset.css" );
		wp_enqueue_style( 'css_custom', get_template_directory_uri() . '/css/custom.css' );
		wp_enqueue_style( 'css_blog', get_stylesheet_directory_uri() . "/assets/css/reset-blog.css" );
		wp_enqueue_style( 'css_index', get_template_directory_uri() . "/assets/css/index.css" );
	} else {
		wp_enqueue_style( 'css_reset', get_stylesheet_directory_uri() . "/assets/css/reset.css" );
		wp_enqueue_style( 'css_custom', get_template_directory_uri() . '/css/custom.css' );
		wp_enqueue_style( 'css_man', get_template_directory_uri() . '/css/man.css' );
		wp_enqueue_style( 'css_index', get_template_directory_uri() . "/assets/css/index.css" );
	}
        if ( ('recipe' == $post->post_type && is_singular( 'recipe' ) ) ) {
            wp_enqueue_style('css_print', get_template_directory_uri() . '/assets/css/print.css', array(),'', 'print');
        }
}

function load_scripts() {
	global $post;
	if ( $GLOBALS[ 'pagenow' ] != 'wp-login.php' && !is_admin() ) {

		wp_enqueue_script( 'js_slick', get_template_directory_uri() . '/assets/js/lib/slick.min.js' );
		wp_enqueue_script( 'js_fitie', get_template_directory_uri() . '/assets/js/lib/fitie.js' );
		wp_enqueue_script( 'js_remodal', get_template_directory_uri() . '/assets/js/lib/remodal.min.js' );
		wp_enqueue_script( 'js_cookie', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js' );

		wp_enqueue_script( 'js_common', get_template_directory_uri() . '/assets/js/common.js' );
		if ( is_front_page() || is_page( 'mypage' ) ) {
			wp_enqueue_script( 'js_campaign', get_template_directory_uri() . '/assets/js/campaign.js', array( 'js_common' ) );
		}

		wp_register_script( 'ajaxzip3', 'https://ajaxzip3.github.io/ajaxzip3.js', array( 'jquery' ), '', true );
		wp_register_script( 'wprm_print', WPRM_URL . 'dist/print.js', array(), '', true );
		wp_register_script( 'wprmp_print', WPRMP_URL . 'dist/print.js', array(), '', true );

		wp_enqueue_script( 'ajax-global', get_template_directory_uri() . '/js/custom.js', array( 'jquery' ), '', true );
		wp_enqueue_script( 'js_man', get_template_directory_uri() . '/js/man.js' );
		wp_localize_script( 'ajax-global', 'urlPage', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

		if ( is_page( array( 55561 ) ) ):
			wp_enqueue_script( 'ajaxzip3' );
		endif;

		if ( is_front_page() || 'recipe' == $post->post_type || is_page( 'mypage' ) ) {
			wp_enqueue_script( 'ajax-global' );
		}
		if ( 'recipe' == $post->post_type ) {
			wp_enqueue_script( 'wprm_print' );
			wp_enqueue_script( 'wprmp_print' );
		}
                if (wp_verify_nonce( $_REQUEST['nonce'], 'recipe_member_nonce' ) && isset( $_REQUEST['nonce'] ) ) {
                    wp_enqueue_script( 'js_popup', get_template_directory_uri() . '/js/popup_execute.js' , array( 'jquery' ), '', true );
                }
	}
}

// Add js admin
add_action( 'admin_enqueue_scripts', 'func_select_parent' );

function func_select_parent( $hook ) {
	wp_enqueue_script( 'custom_admin', get_template_directory_uri() . '/js/admin.js', '', '', true );
}

function register_site_menu() {
	register_nav_menus( array(
		'footer-menu'	 => __( 'Footer Menu', THEME_NAME ),
		'footer-menu2'	 => __( 'Footer Menu2', THEME_NAME )
	) );
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args( $args = '' ) {
	$args[ 'container' ] = false;
	return $args;
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list( $thelist ) {
	return str_replace( 'rel="category tag"', 'rel="tag"', $thelist );
}

// Add page slug to body class
function add_slug_to_body_class( $classes ) {
	global $post;
	if ( is_home() ) {
		$key = array_search( 'blog', $classes );
		if ( $key > -1 ) {
			unset( $classes[ $key ] );
		}
	} elseif ( is_page() ) {
		$classes[] = sanitize_html_class( $post->post_name );
	} elseif ( is_singular() ) {
		$classes[] = sanitize_html_class( $post->post_name );
	}

	return $classes;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove( $tag ) {
	return preg_replace( '~\s+type=["\'][^"\']++["\']~', '', $tag );
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html ) {
	$html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
	return $html;
}

/* ------------------------------------*\
  Actions + Filters + ShortCodes
  \*------------------------------------ */

// Add Actions
add_action( 'wp_enqueue_scripts', 'load_styles' );
add_action( 'wp_enqueue_scripts', 'load_scripts' );
add_action( 'init', 'register_site_menu' );
// Remove Actions
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // Index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // Prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // Start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'rel_canonical' );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

// Remove jQuery Migrate Script from header and Load jQuery from Google API
function remove_wp_embed_and_jquery() {
	if ( !is_admin() ) {
		wp_deregister_script( 'wp-embed' );
	}
}

//add_action('init', 'remove_wp_embed_and_jquery');
// Add Filters
add_filter( 'body_class', 'add_slug_to_body_class' ); // Add slug to body class (Starkers build)
add_filter( 'widget_text', 'do_shortcode' ); // Allow shortcodes in Dynamic Sidebar
add_filter( 'widget_text', 'shortcode_unautop' ); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' ); // Remove surrounding <div> from WP Navigation
add_filter( 'the_category', 'remove_category_rel_from_category_list' ); // Remove invalid rel attribute
add_filter( 'the_excerpt', 'shortcode_unautop' ); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter( 'the_excerpt', 'do_shortcode' ); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter( 'style_loader_tag', 'html5_style_remove' ); // Remove 'text/css' from enqueued stylesheet
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 ); // Remove width and height dynamic attributes to thumbnails
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 ); // Remove width and height dynamic attributes to post images
// Remove Filters
remove_filter( 'the_excerpt', 'wpautop' ); // Remove <p> tags from Excerpt altogether

function remove_api() {
	remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
}

//add_action('after_setup_theme', 'remove_api');
//Remove <link rel='dns-prefetch' href='//s.w.org'> on header
remove_action( 'wp_head', 'wp_resource_hints', 2 );

if ( !function_exists( 'wp_breadcrumb' ) ) {

	function wp_breadcrumb( $list_style = 'ul', $list_id = '', $list_class = '', $active_class = 'active', $echo = true ) {
		// Open list
		$breadcrumb = '<' . $list_style . ' id="' . $list_id . '" class="' . $list_class . '">';

		$breadcrumb .= '<li><a href="' . home_url() . '">トップページ</a></li>';

		// Blog archive
		if ( 'page' == get_option( 'show_on_front' ) && get_option( 'page_for_posts' ) ) {
			$blog_page_id = get_option( 'page_for_posts' );
			if ( is_home() ) {
				$breadcrumb .= '<li class="' . $active_class . '">' . get_the_title( $blog_page_id ) . '</li>';
			} else if ( is_category() || is_tag() || is_author() || is_date() || is_singular( 'post' ) ) {
				$breadcrumb .= '<li><a href="' . get_permalink( $blog_page_id ) . '">' . get_the_title( $blog_page_id ) . '</a></li>';
			}
		}
		// Category, tag, author and date archives
		if ( is_archive() && !is_tax() && !is_post_type_archive() ) {
			$breadcrumb .= '<li class="' . $active_class . '">';
			// Title of archive
			if ( is_category() ) {
				$breadcrumb .= single_cat_title( '', false );
			} else if ( is_tag() ) {
				$breadcrumb .= single_tag_title( '', false );
			} else if ( is_author() ) {
				$breadcrumb .= get_the_author();
			} else if ( is_date() ) {
				if ( is_day() ) {
					$breadcrumb .= get_the_time( 'F j, Y' );
				} else if ( is_month() ) {
					$breadcrumb .= get_the_time( 'F, Y' );
				} else if ( is_year() ) {
					$breadcrumb .= get_the_time( 'Y' );
				}
			}
			$breadcrumb .= '</li>';
		}
		// Posts
		if ( is_singular( 'post' ) ) {
			// Post categories
			$post_cats = get_the_category();
			if ( $post_cats[ 0 ] ) {
				$breadcrumb .= '<li><a href="' . get_category_link( $post_cats[ 0 ]->term_id ) . '">' . $post_cats[ 0 ]->name . '</a></li>';
			}
			// Post title
			$breadcrumb .= '<li class="' . $active_class . '">' . get_the_title() . '</li>';
		}
		// Pages
		if ( is_page() && !is_front_page() ) {
			$post = get_post( get_the_ID() );
			// Page parents
			if ( $post->post_parent ) {
				$parent_id	 = $post->post_parent;
				$crumbs		 = array();
				while ( $parent_id ) {
					$page		 = get_page( $parent_id );
					$crumbs[]	 = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
					$parent_id	 = $page->post_parent;
				}
				$crumbs = array_reverse( $crumbs );
				foreach ( $crumbs as $crumb ) {
					$breadcrumb .= '<li>' . $crumb . '</li>';
				}
			}

			// Page title
			$breadcrumb .= '<li class="' . $active_class . '">' . get_the_title() . '</li>';
		}
		// Attachments
		if ( is_attachment() ) {
			// Attachment parent
			$post = get_post( get_the_ID() );
			if ( $post->post_parent ) {
				$breadcrumb .= '<li><a href="' . get_permalink( $post->post_parent ) . '">' . get_the_title( $post->post_parent ) . '</a></li>';
			}
			// Attachment title
			$breadcrumb .= '<li class="' . $active_class . '">' . get_the_title() . '</li>';
		}
		// Search
		if ( is_search() ) {
			$breadcrumb .= '';
		}
		// 404
		if ( is_404() ) {
			$breadcrumb .= '<li class="' . $active_class . '">' . __( '404', THEME_NAME ) . '</li>';
		}
		// Custom Post Type Archive
		if ( is_post_type_archive() ) {
			if ( !is_search() ) {
				$breadcrumb .= '<li class="' . $active_class . '">' . post_type_archive_title( '', false ) . '</li>';
			}
		}
		// Custom Taxonomies
		if ( is_tax() ) {
			// Get the post types the taxonomy is attached to
			$current_term	 = get_queried_object();
			$attached_to	 = array();
			$post_types		 = get_post_types();
			foreach ( $post_types as $post_type ) {
				$taxonomies = get_object_taxonomies( $post_type );
				if ( in_array( $current_term->taxonomy, $taxonomies ) ) {
					$attached_to[] = $post_type;
				}
			}
			// Post type archive link
			$output = false;
			foreach ( $attached_to as $post_type ) {
				$cpt_obj = get_post_type_object( $post_type );
				if ( !$output && get_post_type_archive_link( $cpt_obj->name ) ) {
					$breadcrumb	 .= '<li><a href="' . get_post_type_archive_link( $cpt_obj->name ) . '">' . $cpt_obj->labels->name . '</a></li>';
					$output		 = true;
				}
			}
			// Term title
			$breadcrumb .= '<li class="' . $active_class . '">' . single_term_title( '', false ) . '</li>';
		}
		// Custom Post Types
		if ( is_single() && get_post_type() != 'post' && get_post_type() != 'attachment' ) {
			$cpt_obj = get_post_type_object( get_post_type() );
			// Is cpt hierarchical like pages or posts?
			if ( is_post_type_hierarchical( $cpt_obj->name ) ) {
				// Like pages
				// Cpt archive
				if ( get_post_type_archive_link( $cpt_obj->name ) ) {
					$breadcrumb .= '<li><a href="' . get_post_type_archive_link( $cpt_obj->name ) . '">' . $cpt_obj->labels->name . '</a></li>';
				}
				// Cpt parents
				$post = get_post( get_the_ID() );
				if ( $post->post_parent ) {
					$parent_id	 = $post->post_parent;
					$crumbs		 = array();
					while ( $parent_id ) {
						$page		 = get_page( $parent_id );
						$crumbs[]	 = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
						$parent_id	 = $page->post_parent;
					}
					$crumbs = array_reverse( $crumbs );
					foreach ( $crumbs as $crumb ) {
						$breadcrumb .= '<li>' . $crumb . '</li>';
					}
				}
			} else {
				// Like posts
				// Cpt archive
				if ( get_post_type_archive_link( $cpt_obj->name ) ) {
					$breadcrumb .= '<li><a href="' . get_post_type_archive_link( $cpt_obj->name ) . '">' . $cpt_obj->labels->name . '</a></li>';
				}
				// Get cpt taxonomies
				$cpt_taxes = get_object_taxonomies( $cpt_obj->name );
				if ( $cpt_taxes && is_taxonomy_hierarchical( $cpt_taxes[ 0 ] ) ) {
					// Other taxes attached to the cpt could be hierachical, so need to look into that.
					$cpt_terms = get_the_terms( get_the_ID(), $cpt_taxes[ 0 ] );
					if ( is_array( $cpt_terms ) ) {
						$output = false;
						foreach ( $cpt_terms as $cpt_term ) {
							if ( !$output ) {
//                                $breadcrumb .= '<li><a href="' . get_term_link($cpt_term->name, $cpt_taxes[0]) . '">' . $cpt_term->name . '</a></li>';
								$output = true;
							}
						}
					}
				}
			}
			// Cpt title
			$breadcrumb .= '<li class="' . $active_class . '">' . get_the_title() . '</li>';
		}
		// Close list
		$breadcrumb .= '</' . $list_style . '>';
		// Ouput
		if ( $echo ) {
			echo $breadcrumb;
		} else {
			return $breadcrumb;
		}
	}

}

function cut_title( $text, $len = 30 ) { //Hàm cắt tiêu đề Unicode
	mb_internal_encoding( 'UTF-8' );
	if ( (mb_strlen( $text, 'UTF-8' ) > $len ) )
		$text = mb_substr( $text, 0, $len, 'UTF-8' ) . "...";
	return $text;
}

function cut_str( $text, $limit = 25 ) {
	$more	 = (mb_strlen( $text ) > $limit) ? TRUE : FALSE;
	$text	 = mb_substr( $text, 0, $limit, 'UTF-8' );
	return array( $text, $more );
}

function pagination( $query_data = '', $range = 4, $num_stt = 12 ) {
	$showitems = ($range * 2) + 1;

	global $paged;
	if ( empty( $paged ) )
		$paged = 1;

	if ( empty( $query_data ) || !is_object( $query_data ) ) {
		global $wp_query;
		$query_data = $wp_query;
	}
	$pages = $query_data->max_num_pages;

	if ( 1 != $pages ) {
		$my_posts_per_page	 = $num_stt;  //1ページに表示する最大投稿数
		$my_post_range		 = ($paged - 1) * $my_posts_per_page + 1;
		switch ( $paged ) {
			case 1:
				$pos_count	 = $query_data->post_count;
				break;
			case $paged == $query_data->max_num_pages:
				$pos_count	 = $query_data->found_posts;
				break;
			default:
				$pos_count	 = $my_post_range + $num_stt;
				break;
		}

		echo "<div class='pagination'>";
		echo "<p class=\"pagination-label\">" . $query_data->found_posts . "件中｜{$my_post_range}〜" . $pos_count . "件 表示</p>";
		echo "<div class=\"pagination-list\">";
		if ( $paged > 2 && $paged > $range + 1 && $showitems < $pages )
			echo "<a href='" . get_pagenum_link( 1 ) . "'>&laquo;</a>";
		if ( $paged > 1 && $showitems < $pages )
			echo "<a href='" . get_pagenum_link( $paged - 1 ) . "'>&lsaquo;</a>";

		for ( $i = 1; $i <= $pages; $i++ ) {
			if ( 1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems ) ) {
				echo ($paged == $i) ? "<a class=\"active\">" . $i . "</a>" : "<a href='" . get_pagenum_link( $i ) . "'>" . $i . "</a>";
			}
		}

		if ( $paged < $pages && $showitems < $pages )
			echo "<a href=\"" . get_pagenum_link( $paged + 1 ) . "\">&rsaquo;</a>";
		if ( $paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages )
			echo "<a href='" . get_pagenum_link( $pages ) . "'>&raquo;</a>";
		echo "</div>";
		echo "</div>";
	}
}

// Set path relative of image
add_filter( 'image_send_to_editor', 'image_to_relative', 5, 8 );

function image_to_relative( $html, $id, $caption, $title, $align, $url, $size, $alt ) {
	$sp	 = strpos( $html, "src=" ) + 5;
	$ep	 = strpos( $html, "\"", $sp );

	$imageurl = substr( $html, $sp, $ep - $sp );

	$relativeurl = str_replace( "http://", "", $imageurl );
	$sp			 = strpos( $relativeurl, "/" );
	$relativeurl = substr( $relativeurl, $sp );

	$html = str_replace( $imageurl, $relativeurl, $html );

	return $html;
}

//Xoa width shortcode mac dinh caption
add_shortcode( 'wp_caption', 'fixed_img_caption_shortcode' );
add_shortcode( 'caption', 'fixed_img_caption_shortcode' );

function fixed_img_caption_shortcode( $attr, $content = null ) {
	// New-style shortcode with the caption inside the shortcode with the link and image tags.
	if ( !isset( $attr[ 'caption' ] ) ) {
		if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
			$content			 = $matches[ 1 ];
			$attr[ 'caption' ]	 = trim( $matches[ 2 ] );
		}
	}
	// Allow plugins/themes to override the default caption template.
	$output	 = apply_filters( 'img_caption_shortcode', '', $attr, $content );
	if ( $output != '' )
		return $output;
	extract( shortcode_atts( array(
		'id'		 => '',
		'align'		 => 'alignnone',
		'width'		 => '',
		'caption'	 => ''
	), $attr ) );
	if ( 1 > (int) $width || empty( $caption ) )
		return $content;
	if ( $id )
		$id		 = 'id="' . esc_attr( $id ) . '" ';
	return '<div ' . $id . 'class="wp-caption ' . esc_attr( $align ) . '">'
	. do_shortcode( $content ) . '<p class="wp-caption-text">' . $caption . '</p></div>';
}

//Change show default of tag
add_filter( 'get_terms_args', 'editor_show_tags' );

function editor_show_tags( $args ) {
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX && isset( $_POST[ 'action' ] ) && $_POST[ 'action' ] === 'get-tagcloud' ) {
		unset( $args[ 'number' ] );
		$args[ 'hide_empty' ] = 0;
	}
	return $args;
}

//Disble srcset
add_filter( 'wp_calculate_image_srcset', '__return_false' );

//Keep classic editor
add_filter( 'use_block_editor_for_post', '__return_false' );

//Disable Gutenberg stylesheet
function wps_deregister_styles() {
	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
}

add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

// Disable update emails
add_filter( 'auto_core_update_send_email', '__return_true' );

/**
 * Gutenbergのブロッグの作成
 */
//add_action('acf/init', 'demo_acf_init');

function demo_acf_init() {

	// check function exists
	if ( function_exists( 'acf_register_block' ) ) {

		// register a button block
		acf_register_block( array(
			'name'				 => 'demo_button',
			'title'				 => __( 'ボタン' ),
			'description'		 => __( '' ),
			'render_template'	 => 'template-parts/blocks/buttons.php',
			'category'			 => 'common',
			'icon'				 => 'button',
			'keywords'			 => array( 'ボタン', 'button' ), //検索文字列
		) );
		// register a button block
		acf_register_block( array(
			'name'				 => 'demo_arrow',
			'title'				 => __( '矢印' ),
			'description'		 => __( '' ),
			'render_template'	 => 'template-parts/blocks/arrow.php',
			'category'			 => 'common',
			'icon'				 => 'arrowRight',
			'keywords'			 => array( '矢印', 'arrow' ), //検索文字列
		) );
		// register a link block
		acf_register_block( array(
			'name'				 => 'demo_link',
			'title'				 => __( 'リンク' ),
			'description'		 => __( '' ),
			'render_template'	 => 'template-parts/blocks/link.php',
			'category'			 => 'common',
			'icon'				 => 'link',
			'keywords'			 => array( 'リンク', 'link' ), //検索文字列
		) );
	}
}

if ( function_exists( 'register_sidebar' ) ) {
	register_sidebar( array(
		'name'			 => __( 'Widget Main', THEME_NAME ),
		'description'	 => __( 'Widget of site', THEME_NAME ),
		'id'			 => 'widget-main',
		'before_widget'	 => '<div id="%1$s" class="%2$s side">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<p class="ttl_subnavi">',
		'after_title'	 => '</p>'
	) );
	register_sidebar( array(
		'name'			 => __( 'Widget User', THEME_NAME ),
		'description'	 => __( 'Display User', THEME_NAME ),
		'id'			 => 'widget-user',
		'before_widget'	 => '<div id="%1$s" class="%2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<p class="ttl_subnavi">',
		'after_title'	 => '</p>'
	) );
}

function get_day_txt( $d = '' ) {
	if ( $d == '' )
		$d		 = date( 'Y-m-d' );
	$date	 = date_create( $d );
	return date_format( $date, "D" );
}

function simple_navigation( $query_data = '' ) {
	if ( empty( $query_data ) || !is_object( $query_data ) ) {
		global $wp_query;
		$query_data = $wp_query;
	}

	if ( $query_data->max_num_pages < 2 ) {
		return false;
	}
	?>
	<div class="blog-ctrl">
		<?php previous_posts_link( 'Back', $query_data->max_num_pages ); ?>
		<?php next_posts_link( 'Next', $query_data->max_num_pages ); ?>
	</div>
	<?php
}

add_filter( 'previous_posts_link_attributes', 'add_prev_posts_link_class' );

function add_prev_posts_link_class() {
	return 'class="view-more link back"';
}

add_filter( 'next_posts_link_attributes', 'add_next_posts_link_class' );

function add_next_posts_link_class() {
	return 'class="view-more link next"';
}

function func__latest_blog() {
	$args = array(
		'post_type'		 => 'blog',
		'posts_per_page' => 3,
		'post_status'	 => 'publish',
		'orderby'		 => 'date',
		'order'			 => 'DESC',
	);

	$query	 = null;
	$query	 = new WP_Query( $args );

	if ( $query->have_posts() ) :
		$result = '<ul>';
		while ( $query->have_posts() ) : $query->the_post();
			$blog_cat = get_the_terms( get_the_ID(), 'blog_category' );

			$result	 .= '<li>';
			$result	 .= '<a class="link" href="' . get_the_permalink() . '">';
			$result	 .= '<div class="blog-more--cate-thumb">' . get_the_post_thumbnail( get_the_ID(), 'medium' ) . '</div>';
			$result	 .= '<div class="blog-more--cate-infor">';
			$result	 .= '<div class="blog-more--cate-dateWrap">';
			$result	 .= '<span class="date">' . get_the_time( 'Y.m.d' ) . '[' . strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) . ']</span>';
			$result	 .= '<span class="label">' . $blog_cat[ 0 ]->name . '</span>';
			$result	 .= '</div>';
			$result	 .= '<h3 class="titleLv3 type2">' . get_the_title() . '</h3>';
			$result	 .= '</div>';
			$result	 .= '</a>';
			$result	 .= '</li>';

		endwhile;

		wp_reset_postdata();

		$result .= '</ul>';
	endif;

	return $result;
}

add_shortcode( 'blog-list', 'func__latest_blog' );

function func__single_popular_post( $post_html, $p, $instance ) {
	//サムネイル取得
	$thumbnail_id	 = get_post_thumbnail_id( $p->id ); //サムネイル表示サイズ
	$thumbnail_img	 = wp_get_attachment_image_src( $thumbnail_id, 'medium' );
	$custom_id		 = $p->id; //記事日付
	$custom_date	 = get_the_time( 'Y.m.d', $p->id );
	$monday			 = get_day_txt( get_the_time( 'Y-m-d', $p->id ) );
	$blog_cat		 = get_the_terms( $p->id, 'blog_category' );
	$output			 = ' <li><a href="' . get_the_permalink( $p->id ) . '" class="link"><div class="blog-more--cate-thumb"><img src="' . $thumbnail_img[ 0 ] . '" title="' . esc_attr( $p->title ) . '" /></div><div class="blog-more--cate-infor"><div class="blog-more--cate-dateWrap"><span class="date">' . $custom_date . '[' . strtolower( $monday ) . ']</span><span class="label">' . $blog_cat[ 0 ]->name . '</span></div><h3 class="titleLv3 type2">' . $p->title . '</h3></div></a></li> ';
	return $output;
}

add_filter( 'wpp_post', 'func__single_popular_post', 10, 3 );

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_sub_page( array(
		'page_title'	 => '人気のタグ',
		'menu_title'	 => '人気のタグ',
		'menu_slug'		 => 'tag-ranking',
		'parent_slug'	 => 'edit.php?post_type=blog',
	) );
	acf_add_options_page( array(
		'page_title' => 'テーマ設定',
		'menu_title' => 'テーマ設定',
		'menu_slug'	 => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect'	 => false
	) );

	acf_add_options_sub_page( array(
		'page_title'	 => '広告',
		'menu_title'	 => '広告',
		'menu_slug'		 => 'adv-option',
		'parent_slug'	 => 'theme-general-settings',
	) );

	acf_add_options_sub_page( array(
		'page_title'	 => 'よく検索されるキーワード',
		'menu_title'	 => 'キーワード',
		'menu_slug'		 => 'tag-recipe',
		'parent_slug'	 => 'edit.php?post_type=recipe',
	) );

	acf_add_options_sub_page( array(
		'page_title'	 => 'キャンペーン',
		'menu_title'	 => 'キャンペーン',
		'menu_slug'		 => 'campaign-option',
		'parent_slug'	 => 'theme-general-settings',
	) );
}

function title_filter( $where, $wp_query ) {
	global $wpdb;
	if ( $search_title = $wp_query->get( 'title_filter' ) ) :
		$search_title			 = $wpdb->esc_like( $search_title );
		$search_title			 = ' \'%' . $search_title . '%\'';
		$title_filter_relation	 = ( strtoupper( $wp_query->get( 'title_filter_relation' ) ) == 'OR' ? 'OR' : 'AND' );
		$where					 .= ' ' . $title_filter_relation . ' ' . $wpdb->posts . '.post_title LIKE ' . $search_title;
	endif;
	return $where;
}

function add_query_vars_filter( $vars ) {
	$vars[] = "key";
	return $vars;
}

add_filter( 'query_vars', 'add_query_vars_filter' );

//remove_action('embed_head', 'print_embed_styles');

function blog_embed_styles() {
	wp_enqueue_style( 'wp-oembed-embed', get_template_directory_uri() . "/css/wp-oembed-embed.css" );
}

add_filter( 'embed_head', 'blog_embed_styles' );

function getGoogleEmbed( string $address ): string {
	return '<iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q=' . $address . '&output=embed"></iframe>';
}

function ajaxzip() {
	if ( is_page( array( 55561 ) ) ):
		?>
		<script>
		jQuery( function ( $ ) {
		$( "button.btn-gray" ).click( function () {
			AjaxZip3.zip2addr( 'zip', null, 'pref', 'address' );
			return false;
		} );
		} );
		</script>
		<?php
	endif;
}

add_action( 'wp_footer', 'ajaxzip', 100 );

add_shortcode( 'path_theme', 'shortcode_tp' );

function shortcode_tp() {
	return get_template_directory_uri();
}

function shop_area_ajax() {
	$pref_ID = intval( $_POST[ 'pref_ID' ] );
	if ( isset( $pref_ID ) && $pref_ID > 0 ) {
		$regions = get_terms( 'shop_area', "hide_empty=0&parent=$pref_ID" );
		echo '<option>エリア</option>';
		if ( $regions && !is_wp_error( $regions ) ) {
			foreach ( $regions as $region ) {
				echo "<option value='{$region->term_id}'>{$region->name}</option>";
			}
		}
		die();
	} else {
		echo '<option>エリア</option>';
	}
}

add_action( 'wp_ajax_pref__action', 'shop_area_ajax' );
add_action( 'wp_ajax_nopriv_pref__action', 'shop_area_ajax' );

add_action( 'template_redirect', 'shop_search' );

function shop_search() {
	global $post;
	if ( $post->post_type != "shop" ) {
		return;
	}
	$data = $_POST;
}

//ひらがなでカタカナも検索できる
function change_search_char( $where, $obj ) {
	if ( $obj->is_search ) {
		$where	 = str_replace( ".post_title", ".post_title COLLATE utf8_unicode_ci", $where );
		$where	 = str_replace( ".post_content", ".post_content COLLATE utf8_unicode_ci", $where );
	}
	return $where;
}

//add_filter('posts_where', 'change_search_char', 10, 2);

/*
 * ファイルタイプの取得
 */
function get_file_type( $file_url = "" ) {
	if ( empty( $file_url ) )
		return;

	$fileInfo = pathinfo( $file_url );
	switch ( $fileInfo[ 'extension' ] ) {
		case 'pdf':
			$class_name = 'pdf';
			break;

		case 'docx':
		case 'doc':
			$class_name = 'word';
			break;

		case 'xlsx':
		case 'xls':
			$class_name = 'excel';
			break;
	}
	return $class_name;
}

add_action( 'wp_ajax_nopriv_blog_ajax', 'blog_by_taxonomy' );
add_action( 'wp_ajax_blog_ajax', 'blog_by_taxonomy' );

function blog_by_taxonomy() {
	$term_ID = intval( $_POST[ 'id' ] );
	if ( $term_ID > 0 ) {
		$condition = array(
			'taxonomy'	 => 'blog_category',
			'field'		 => 'term_id',
			'terms'		 => $term_ID
		);
	}

	$args = array(
		'post_type'		 => 'blog',
		'post_status'	 => 'publish',
		'posts_per_page' => 8,
		'orderby'		 => 'date',
		'order'			 => 'DESC',
		'tax_query'		 => array(
			$condition
		)
	);

	$blog_query	 = null;
	$blog_query	 = new WP_Query( $args );
	if ( $blog_query->have_posts() ) :
		while ( $blog_query->have_posts() ) : $blog_query->the_post();
			$terms_blog = get_the_terms( get_the_ID(), 'blog_category' );
			?>
			<li class="section-column--item">
				<a class="link" href="<?php the_permalink() ?>">
					<div class="section-column--item-thumb">
						<?php if ( in_array( 'pr_status', get_field( 'pr_icon' ) ) ): ?>
							<span class="blog-item--thumb-label">PR</span>
						<?php endif; ?>
						<?php if ( has_post_thumbnail() ) : ?>
							<?php the_post_thumbnail( 'medium' ); ?>
						<?php else: ?>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-img.jpg" />
						<?php endif; ?>
					</div>
					<div class="section-column--item-cnt">
						<div>
							<span class="date big"><?php the_time( 'Y.m.d' ); ?> [<?php echo strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) ?>]</span><br class="sp-only">
							<span class="tag">#<?php echo esc_html( $terms_blog[ 0 ]->name ); ?></span>
						</div>
						<div class="section-column--item-des">
							<?php the_title() ?>
						</div>
					</div>
				</a>
			</li>
			<?php
		endwhile;

	endif;
	wp_reset_postdata();

	die();
}

function cptui_register_my_taxes_blog_category() {

	/**
	 * Taxonomy: カテゴリー.
	 */
	$labels = [
		"name"			 => __( "カテゴリー", "demotheme" ),
		"singular_name"	 => __( "カテゴリー", "demotheme" ),
	];

	$args = [
		"label"					 => __( "カテゴリー", "demotheme" ),
		"labels"				 => $labels,
		"public"				 => true,
		"publicly_queryable"	 => true,
		"hierarchical"			 => true,
		"show_ui"				 => true,
		"show_in_menu"			 => true,
		"show_in_nav_menus"		 => true,
		"query_var"				 => true,
		"rewrite"				 => [ 'slug' => 'blog', 'with_front' => true, ],
		"show_admin_column"		 => true,
		"show_in_rest"			 => true,
		"rest_base"				 => "blog_category",
		"rest_controller_class"	 => "WP_REST_Terms_Controller",
		"show_in_quick_edit"	 => true,
	];
	register_taxonomy( "blog_category", [ "blog" ], $args );
}

add_action( 'init', 'cptui_register_my_taxes_blog_category' );

//Remove [...]
function new_excerpt_more( $more ) {
	return '';
}

add_filter( 'excerpt_more', 'new_excerpt_more' );

function really_simple_csv_importer_save_meta_filter( $meta, $post, $is_update ) {
	$meta_arr = json_decode( $meta[ 'meta_fields' ] );

	$meta_array[ 'store_content' ]	 = $meta[ 'store_content' ];
	// serialize metadata
	$meta_array						 = array();
	if ( $meta_arr->addr != "" )
		$store_address					 = $meta_arr->addr;
	if ( $meta_arr->zip != "" )
		$meta_array[ 'store_address' ]	 = "〒" . $meta_arr->zip . " " . $store_address;
	else
		$meta_array[ 'store_address' ]	 = $store_address;

	if ( $meta_arr->hp != "" )
		$meta_array[ 'store_url' ]		 = $meta_arr->hp;
	if ( $meta_arr->business_h != "" )
		$meta_array[ 'store_marketing' ] = $meta_arr->business_h;
	if ( $meta_arr->holiday != "" )
		$meta_array[ 'store_holiday' ]	 = $meta_arr->holiday;
	if ( $meta_arr->seats != "" )
		$meta_array[ 'store_seat' ]		 = $meta_arr->seats;
	if ( $meta_arr->credit != "" )
		$meta_array[ 'store_paid' ]		 = $meta_arr->credit;
	if ( $meta_arr->price != "" )
		$meta_array[ 'store_average' ]	 = $meta_arr->price;
	if ( $meta_arr->tel != "" )
		$meta_array[ 'store_tel' ]		 = $meta_arr->tel;
	if ( $meta_arr->acc != "" )
		$meta_array[ 'store_access' ]	 = $meta_arr->acc;
	if ( $meta_arr->parking != "" )
		$meta_array[ 'store_parking' ]	 = $meta_arr->parking;
	if ( $meta_arr->price_d != "" )
		$meta_array[ 'price_d' ]		 = $meta_arr->price_d;
	if ( $meta_arr->price_n != "" )
		$meta_array[ 'price_n' ]		 = $meta_arr->price_n;


	if ( count( $meta_arr->img_url ) > 0 ) {
		// $meta_array['store_gallery'] = implode(',', $meta_arr->img_url);
		foreach ( $meta_arr->img_url as $gal_img ) {
			// $meta_array['store_gallery'][] = $gal_img;
			// Create a instance of helper class
			$h = RSCSV_Import_Post_Helper::getByID( $post->ID );

			// Get the remote image data
			$file = $h->remoteGet( $gal_img );

			// Then, attach it
			$attachment_id					 = $h->setAttachment( $file );
			$meta_array[ 'store_gallery' ][] = $attachment_id;

			// Finally, replace the original data with the attachment id
			$h->setMeta( array( 'store_gallery' => $attachment_id ) );
		}
	}

	$meta = $meta_array;

	return $meta;
}

add_filter( 'really_simple_csv_importer_save_meta', 'really_simple_csv_importer_save_meta_filter', 10, 3 );

function mwform_validation_rule( $Validation, $data ) {
	$validation_select_msg	 = '未選択です。';
	$validation_input_msg	 = '未入力です。';
	$validation_chk_msg		 = 'チェックがされていません。';

	if ( empty( $data[ 'product' ] ) ) {
		$Validation->set_rule( 'product', 'noempty', array( 'message' => $validation_select_msg ) );
	}

	$Validation->set_rule( 'privacy', 'required', array( 'message' => $validation_chk_msg ) );

	return $Validation;
}

add_filter( 'mwform_validation_mw-wp-form-215', 'mwform_validation_rule', 10, 2 );

add_action( 'wp_ajax_recipe_like', 'func_recipe_like' );

function func_recipe_like() {
	$json = array(
		'result' => false
	);
	if ( !isset( $_POST[ 'post_id' ] ) ) {
		$json[ 'message' ] = __( 'Invalid request.' );
	} elseif ( !current_user_can( 'read' ) ) {
		$json[ 'message' ] = __( 'Require login.' );
	} else {
		$user_id	 = get_current_user_id();
		$post_id	 = (int) $_POST[ 'post_id' ];
		$posts_liked = get_user_meta( $user_id, 'tcd_likes', true );
		if ( $posts_liked ) {
			$posts_liked = array_map( 'intval', explode( ',', $posts_liked ) );
		}
		if ( 0 < $post_id ) {
			$target_post = get_post( $post_id );
		}
		if ( empty( $target_post->post_status ) ) {
			$json[ 'message' ] = __( 'Invalid request.' );
		} elseif ( 'publish' !== $target_post->post_status ) {
			$json[ 'message' ] = sprintf( __( 'Disable like in %s.' ), __( 'Not publish article' ) );
		} elseif ( $target_post->post_type !== 'recipe' ) {
			$json[ 'message' ] = sprintf( __( 'Disable like in %s.' ), $target_post->post_type );
		} elseif ( count( $posts_liked ) > 100 ) {
			$json[ 'result' ]		 = 'maximum';
			$json[ 'like_label' ]	 = 'クリップする';
			$json[ 'like_message' ]	 = __( 'クリップできるのは<br>最大100件です' );
		} else {
			// お気に入り済みの場合、お気に入り削除
			if ( is_liked( $post_id, $user_id ) ) {
				$result = remove_like( $post_id, $user_id );
				if ( true === $result ) {
					$json[ 'result' ]		 = 'removed';
					$json[ 'like_label' ]	 = 'クリップする';
					$json[ 'like_message' ]	 = __( 'クリップから<br>削除しました' );
				} elseif ( is_string( $result ) ) {
					$json[ 'message' ] = $result;
				} else {
					$json[ 'message' ] = __( 'Remove like error: ' ) . __( 'Failed to save the database.' );
				}

				// お気に入りしていない場合、お気に入り追加
			} else {
				$result = add_like( $post_id, $user_id );
				if ( true === $result ) {
					$json[ 'result' ]		 = 'added';
					$json[ 'like_label' ]	 = 'クリップ済み';
					$json[ 'like_message' ]	 = __( 'クリップに<br>追加されました' );
				} elseif ( is_string( $result ) ) {
					$json[ 'message' ] = $result;
				} else {
					$json[ 'message' ] = __( 'Add like error: ' ) . __( 'Failed to save the database.' );
				}
			}
		}
	}

	// JSON出力
	wp_send_json( $json );
	exit;
}

/**
 * お気に入り済みかを判別
 */
function is_liked( $post_id = null, $user_id = 0 ) {
	if ( !$user_id ) {
		$user_id = get_current_user_id();
	}
	if ( !$user_id ) {
		return null;
	}

	if ( null === $post_id ) {
		$post_id = get_the_ID();
	}
	$post_id = (int) $post_id;
	if ( 0 >= $post_id ) {
		return null;
	}

	$target_post = get_post( $post_id );
	if ( empty( $target_post->post_status ) || 'publish' !== $target_post->post_status ) {
		return null;
	}

	$user_likes = get_user_meta( $user_id, 'tcd_likes', true );
	if ( $user_likes ) {
		$user_likes = array_map( 'intval', explode( ',', $user_likes ) );
		return in_array( $post_id, $user_likes, true );
	} else {
		return false;
	}
}

/**
 * お気に入り削除
 */
function remove_like( $post_id, $user_id = 0 ) {
	if ( !$user_id ) {
		$user_id = get_current_user_id();
	}
	if ( !$user_id ) {
		return null;
	}

	$post_id = (int) $post_id;
	if ( 0 >= $post_id ) {
		return null;
	}

	$target_post = get_post( $post_id );
	if ( empty( $target_post->post_status ) || 'publish' !== $target_post->post_status ) {
		return null;
	}

	// ユーザーメタからお気に入りデータ取得
	$user_likes = get_user_meta( $user_id, 'tcd_likes', true );
	if ( $user_likes ) {
		$user_likes = array_map( 'intval', explode( ',', $user_likes ) );

		// お気に入り配列キー取得
		$key = array_search( $post_id, $user_likes, true );

		// お気に入り済み場合
		if ( false !== $key ) {
			unset( $user_likes[ $key ] );

			// ユーザーメタ更新
			if ( update_user_meta( $user_id, 'tcd_likes', implode( ',', $user_likes ) ) ) {
				// ユーザーメタ更新後、ポストメタのお気に入り数を1減らす
				$post_likes_number = intval( get_post_meta( $post_id, 'tcd_likes', true ) ) - 1;
				if ( 0 > $post_likes_number ) {
					$post_likes_number = 0;
				}

				update_post_meta( $post_id, 'tcd_likes', $post_likes_number );

				return true;
			} else {
				return __( 'Remove like error: ' ) . __( 'Failed to update user meta.' );
			}

			// お気に入りしていない場合
		} else {
			return false;
		}

		// お気に入りしていない場合
	} else {
		return false;
	}
}

/**
 * お気に入り追加
 */
function add_like( $post_id, $user_id = 0 ) {
	// お気に入り済みの場合
	if ( is_liked( $post_id, $user_id ) ) {
		return 0;
	}

	if ( !$user_id ) {
		$user_id = get_current_user_id();
	}
	if ( !$user_id ) {
		return null;
	}

	$post_id = (int) $post_id;
	if ( 0 >= $post_id ) {
		return null;
	}

	$target_post = get_post( $post_id );
	if ( empty( $target_post->post_status ) || 'publish' !== $target_post->post_status ) {
		return null;
	}

	// ユーザーメタからお気に入りデータ取得
	$user_likes = get_user_meta( $user_id, 'tcd_likes', true );
	if ( $user_likes ) {
		$user_likes = array_map( 'intval', explode( ',', $user_likes ) );

		// お気に入り配列キー取得
		$key = array_search( $post_id, $user_likes, true );

		// お気に入り済み場合
		if ( false !== $key ) {
			return false;
		}
	} else {
		$user_likes = array();
	}

	// 記事ID追加
	$user_likes[]	 = $post_id;
	$user_likes		 = array_unique( $user_likes );

	// ユーザーメタ更新
	if ( update_user_meta( $user_id, 'tcd_likes', implode( ',', $user_likes ) ) ) {
		// ユーザーメタ更新後、ポストメタのお気に入り数を1増やす
		$post_likes_number = intval( get_post_meta( $post_id, 'tcd_likes', true ) );

		if ( 0 > $post_likes_number ) {
			$post_likes_number = 0;
		}

		update_post_meta( $post_id, 'tcd_likes', $post_likes_number + 1 );

		return true;
	} else {
		return __( 'Add like error: ' ) . __( 'Failed to update user meta.' );
	}
}

add_action( 'wp_ajax_remove_like', 'func_remove_like' );

function func_remove_like() {
	$json = array(
		'result' => false
	);

	if ( !isset( $_POST[ 'post_id' ] ) ) {
		$json[ 'message' ] = __( 'Invalid request.' );
	} elseif ( !current_user_can( 'read' ) ) {
		$json[ 'message' ] = __( 'Require login.' );
	} else {
		$user_id = get_current_user_id();
		$post_id = (int) $_POST[ 'post_id' ];

		if ( 0 < $post_id ) {
			$target_post = get_post( $post_id );
		}
		if ( empty( $target_post->post_status ) ) {
			$json[ 'message' ] = __( 'Invalid request.' );
		} elseif ( 'publish' !== $target_post->post_status ) {
			$json[ 'message' ] = sprintf( __( 'Disable like in %s.' ), __( 'Not publish article' ) );
		} elseif ( $target_post->post_type !== 'recipe' ) {
			$json[ 'message' ] = sprintf( __( 'Disable like in %s.' ), $target_post->post_type );
		} else {

			// お気に入り済みの場合、お気に入り削除
			if ( is_liked( $post_id, $user_id ) ) {
				$result = remove_like( $post_id, $user_id );
				if ( true === $result ) {
					$json[ 'result' ]		 = 'removed';
					$json[ 'like_message' ]	 = __( 'クリップから<br>削除しました' );
				} elseif ( is_string( $result ) ) {
					$json[ 'message' ] = $result;
				} else {
					$json[ 'message' ] = __( 'Remove like error: ' ) . __( 'Failed to save the database.' );
				}

				// お気に入りしていない場合
			} else {
				$recipe_label		 = __( 'Recipe' );
				$json[ 'message' ]	 = sprintf( __( "You don't like this %s." ), $recipe_label );
			}
		}
	}

	// JSON出力
	wp_send_json( $json );
	exit;
}

if ( !current_user_can( 'manage_options' ) ) {
//    add_filter('show_admin_bar', '__return_false');
}

function shop_area( $area_id ) {
	if ( empty( $area_id ) )
		return;
	$area = get_term( $area_id, 'shop_area' );
	return $area->name;
}

/**
 * Redirection wp-login to page login
 * @param type $user
 * @param type $username
 * @param type $password
 */
function verify_user_pass( $user, $username, $password ) {
	$login_page = get_permalink( 283 );
	if ( $username == "" || $password == "" ) {
		wp_redirect( $login_page );
		exit;
	}
}

//add_filter('authenticate', 'verify_user_pass', 1, 3);

function custom_login_redirect( $redirect_to, $request, $user ) {
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		if ( in_array( 'administrator', $user->roles ) ) {
			return admin_url();
		} else {
			return '/my-account/';
		}
	} else {
		return home_url();
	}
}

//add_filter('login_redirect', 'custom_login_redirect', 1, 3);

/**
 * 栄養項目を取得する
 * @param type $field
 * @return type
 */
function acf_load_nutrition_fields( $field ) {
	$field[ 'choices' ] = array();

	$nutrition_fields = WPRM_Nutrition::get_fields();

	foreach ( $nutrition_fields as $key => $data ) {
		$field[ 'choices' ][ $key ] = $data[ 'label' ];
	}

	return $field;
}

add_filter( 'acf/load_field/name=items_focus', 'acf_load_nutrition_fields' );

/**
 * アクセス数ランキング記事一覧取得関数
 * @global type $wpdb
 * @param type $time
 */
function recipe_ranking( $time, $limit = 5 ) {
	global $wpdb;
}

/**
 * Fix order recipe post by date DESC
 * @param type $wp_query
 */
function func_post_types_admin_order( $wp_query ) {
	if ( is_admin() ) {

		$post_type = $wp_query->query[ 'post_type' ];

		if ( $post_type == 'recipe' ) {
			$wp_query->set( 'orderby', 'date' );
			$wp_query->set( 'order', 'DESC' );
		}
	}
}

add_filter( 'pre_get_posts', 'func_post_types_admin_order' );

function nk_admin_enqueue_scripts() {
	if ( wp_style_is( 'wprm-admin-modal' ) ) {
		$css_text = <<<EOM
.wprm-admin-modal-field-ingredient-header-container .wprm-admin-modal-field-ingredient-header:nth-child(3) {
    order: -2;
}
.wprm-admin-modal-field-ingredient .wprm-admin-modal-field-ingredient-text-container .wprm-admin-modal-field-richtext:nth-child(3){
    order: -2;
}
.wprm-admin-modal-field-ingredient-header-container .wprm-admin-modal-field-ingredient-header:nth-child(2){
    order:-1;
}
.wprm-admin-modal-field-ingredient .wprm-admin-modal-field-ingredient-text-container .wprm-admin-modal-field-richtext:nth-child(2) {
    order:-1;
}
EOM;
		wp_register_style( 'wprm-admin-modal-nk-add', false, [ 'wprm-admin-modal' ] );
		wp_enqueue_style( 'wprm-admin-modal-nk-add' );
		wp_add_inline_style( 'wprm-admin-modal-nk-add', $css_text );
	}
}

add_action( 'admin_enqueue_scripts', 'nk_admin_enqueue_scripts', 11 );

function recipe_query( $where ) {
	global $wpdb;
	if ( 'recipe' == get_post_type() ) {
		isset( $_GET[ 'season' ] ) ? $season	 = $_GET[ 'season' ] : $season	 = '';

		if ( $season ) {
			$where .= <<<EOM
            AND ( $wpdb->postmeta.meta_key = 'recipe_season' AND $wpdb->postmeta.meta_value LIKE '%{$season}%' )
EOM;
		}
	}
	return $where;
}

add_filter( 'posts_where', 'wpse18703_posts_where', 10, 2 );

function wpse18703_posts_where( $where, &$wp_query ) {
	global $wpdb;
	if ( $wpse18703_title = $wp_query->get( 'wpse18703_title' ) ) {
		$where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'' . esc_sql( $wpdb->esc_like( $wpse18703_title ) ) . '%\'';
	}
	return $where;
}

//add_filter('posts_join', 'add_my_join');

function add_my_join( $join ) {
	global $wpdb;
	if ( 'recipe' == get_post_type() ) {
		$join .= "INNER JOIN $wpdb->postmeta as $wpdb->postmeta ON ( $wpdb->posts.ID = $wpdb->postmeta.post_id )";
	}

	return $join;
}

/**
 * Google Analyticsタグを挿入。
 */
function GoogleaAnalytics() {
	?>
	<!-- Global site tag (gtag.js) - Google Analytics -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145780659-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [ ];
		function gtag() {
		dataLayer.push( arguments );
		}
		gtag( 'js', new Date() );

		gtag( 'config', 'UA-145780659-2' );
	</script>

	<?php
}

add_action( 'wp_head', 'GoogleaAnalytics' );

function func_recipe_conditions( $s_type = 'menu', $type = '' ) {
	global $wpdb;
	$lists_post_ids	 = [];
	$post_ids		 = [];
	$s_lists		 = [];
	if ( $s_type == 'menu' ) {
		$l_menus2 = nk_type_menus();
		foreach ( $l_menus2 as $k => $v ) {
			if ( $k == 'all' ) {
				continue;
			}
			if ( $v[ 'n' ] != '' ) {
				$s_lists[ 's' . ($k) ] = [
					'n'	 => $v[ 'n' ],
					's'	 => $v[ 's' ]
				];
			}
		}
	} else {
		$l_ingredients = nk_type_ingredients();
		foreach ( $l_ingredients as $k => $v ) {
			if ( $k == 'all' ) {
				continue;
			}

			$l_s_all = [];

			$ll_no_s_all = [];

			foreach ( $v[ 'lists' ] as $k2 => $v2 ) {
				$l_s = isset( $v2[ 's' ] ) ? $v2[ 's' ] : [ $v2[ 'n' ] ];
				foreach ( $l_s as $k3 => $v3 ) {
					$l_s_all[] = $v3;
				}
				$l_s_no = isset( $v2[ 's_no' ] ) ? $v2[ 's_no' ] : [];
				foreach ( $l_s_no as $k3 => $v3 ) {
					$ll_no_s_all[] = $v3;
				}
				$s_lists[ 's' . ($k + $k2) ] = [
					's'		 => $l_s,
					's_no'	 => $l_s_no,
					'n'		 => $v2[ 'n' ],
				];
			}

			if ( count( $l_s_all ) > 0 ) {

				$l_no_s_all = [];
				foreach ( $ll_no_s_all as $k2 => $v2 ) {

					if ( in_array( $v2, $l_s_all, true ) ) {
						continue;
					}

					$llv_kana = mb_convert_kana( $v2, "cH" );
					if ( in_array( $llv_kana, $l_s_all, true ) ) {
						continue;
					}

					$llv = mb_convert_kana( $v2, "KVC" );
					if ( in_array( $llv, $l_s_all, true ) ) {
						continue;
					}
					$l_no_s_all[] = $v2;
				}
				$s_lists[ 's' . ($k) ] = [
					'n'		 => $v[ 'n' ],
					's_no'	 => $l_no_s_all,
					's'		 => $l_s_all
				];
			}
		}
	}

	if ( isset( $s_lists[ 's' . $type ] ) ) {
		$my_type = $s_lists[ 's' . $type ];

		$_keyword_array = [];

		foreach ( $my_type[ 's' ] as $k => $v ) {
			$lv	 = trim( $v );
			$lv2 = [];
			if ( $lv != '' ) {
				$lv2[]		 = $lv;
				$llv_kana	 = mb_convert_kana( $v, "cH" );
				if ( $lv !== $llv_kana ) {
					$lv2[] = $llv_kana;
				}
				$llv = mb_convert_kana( $v, "KVC" );
				if ( $lv !== $llv ) {
					$lv2[] = $llv;
				}
				$_keyword_array[] = $lv2;
			}
		}

		$_no_keyword_array = [];
		if ( isset( $my_type[ 's_no' ] ) ) {
			foreach ( $my_type[ 's_no' ] as $k => $v ) {
				$lv = trim( $v );
				if ( $lv != '' ) {
					$_no_keyword_array[] = $lv;
					$llv_kana			 = mb_convert_kana( $v, "cH" );
					if ( $lv !== $llv_kana ) {
						$_no_keyword_array[] = $llv_kana;
					}
					$llv = mb_convert_kana( $v, "KVC" );
					if ( $lv !== $llv ) {
						$_no_keyword_array[] = $llv;
					}
				}
			}
		}



		if ( $s_type == 'menu' ) {
			$base_sql = "SELECT distinct object_id FROM $wpdb->terms as a INNER JOIN $wpdb->term_taxonomy as b ON `a`.`term_id` = `b`.`term_id` INNER JOIN $wpdb->term_relationships as c ON `a`.`term_id` = `c`.`term_taxonomy_id` INNER JOIN $wpdb->posts as p ON `p`.`ID` = `c`.`object_id` WHERE ( `b`.taxonomy='wprm_course' )";
		} else {
			$base_sql = "SELECT post_id FROM $wpdb->postmeta INNER JOIN $wpdb->posts ON `$wpdb->posts`.`ID` = `$wpdb->postmeta`.`post_id` WHERE ( meta_key = 'wprm_ingredients' )";
		}


		$base_sql .= " AND ";

		foreach ( $_keyword_array as $k => $v ) {

			if ( count( $v ) === 0 ) {
				continue;
			}

			$sql = $base_sql;

			$sql .= ' ( ';

			if ( $s_type == 'menu' ) {
				$i = 0;
				foreach ( $v as $k2 => $v2 ) {
					$lv		 = explode( '&&', $v2 );
					$l_terms = get_terms( [
						'taxonomy'	 => 'wprm_course',
						'fields'	 => 'ids',
						'name'		 => $lv,
					] );
					if ( count( $lv ) == count( $l_terms ) ) {
						if ( $i != 0 ) {
							$sql .= ' OR';
						}
						$sql .= "( ( SELECT COUNT(1) FROM $wpdb->term_relationships WHERE term_taxonomy_id IN (" . implode( ',', $l_terms ) . ") AND object_id = p.ID ) = " . count( $l_terms ) . " ) ";
						$i++;
					}
				}
			} else {

				foreach ( $v as $k2 => $v2 ) {
					if ( $k2 != 0 ) {
						$sql .= ' OR';
					}
					$sql .= "( meta_value LIKE '%\"" . $wpdb->esc_like( $v2 ) . "%' )";
				}
			}

			$sql .= ' ) ';

			$l_no_lists = [];
			if ( count( $_no_keyword_array ) > 0 && $type == 'menu' ) {
				$no_lists_sql	 = $base_sql;
				$no_lists_sql	 .= ' AND ';
				$no_lists_sql	 .= ' ( ';
				foreach ( $_no_keyword_array as $k2 => $v2 ) {
					if ( $k2 != 0 ) {
						$no_lists_sql .= ' OR';
					}
					if ( $s_type == 'menu' ) {
						$no_lists_sql .= " a.name = '" . $v2 . "' ";
					} else {
						$no_lists_sql .= " meta_value LIKE '%" . $wpdb->esc_like( $v2 ) . "%' ";
					}
				}
				$no_lists_sql	 .= ' ) ';
				$l_results		 = $wpdb->get_results( $no_lists_sql );
				foreach ( $l_results as $r ) {
					if ( $s_type == 'menu' ) {
						$l_id = (int) $r->object_id;
					} else {
						$l_id = (int) $r->post_id;
					}
					$l_no_lists[] = $l_id;
				}
			}

			$results = $wpdb->get_results( $sql );

			foreach ( $results as $r ) {
				if ( $s_type == 'menu' ) {
					$l_id = (int) $r->object_id;
				} else {
					$l_id = (int) $r->post_id;
				}
				if ( in_array( $l_id, $l_no_lists, true ) ) {
					continue;
				}
				if ( !in_array( $l_id, $lists_post_ids[ $k ] ?: [], true ) ) {
					$lists_post_ids[ $k ][] = $l_id;
				}
			}
		}


		foreach ( $_keyword_array as $k => $v ) {
			if ( count( $v ) === 0 ) {
				continue;
			}
			$l = $lists_post_ids[ $k ] ?: [];
			if ( count( $l ) > 0 ) {
				$results = $wpdb->get_results( "SELECT meta_value FROM $wpdb->postmeta INNER JOIN $wpdb->posts ON `$wpdb->posts`.`ID` = `$wpdb->postmeta`.`meta_value` WHERE post_id IN (" . join( ',', $l ) . ") AND meta_key='wprm_parent_post_id'" );
				foreach ( $results as $r ) {
					$l_id = (int) $r->meta_value;
					if ( !in_array( $l_id, $lists_post_ids[ $k ] ?: [], true ) ) {
						$lists_post_ids[ $k ][] = $l_id;
					}
				}
			}
		}

		foreach ( $lists_post_ids as $k => $v ) {
			foreach ( $v as $k2 => $v2 ) {
				if ( !in_array( $v2, $post_ids, true ) ) {
					$post_ids[] = $v2;
				}
			}
		}
	}
	return $post_ids;
}

function latest_recipe( $num = 20 ) {
	$args						 = [];
	$args[ 'post_type' ]		 = 'recipe';
	$args[ 'post_status' ]		 = 'publish';
	$args[ 'orderby' ]			 = 'date';
	$args[ 'order' ]			 = 'DESC';
	$args[ 'posts_per_page' ]	 = $num;

	$recipe_query	 = null;
	$recipe_query	 = new WP_Query( $args );

	if ( $recipe_query->have_posts() ):
		echo '<ul class="pickup-suggest--list">';
		while ( $recipe_query->have_posts() ): $recipe_query->the_post();
			$recipe_cat = get_the_terms( get_the_ID(), 'recipe_category' );

			$recipes	 = WPRM_Recipe_Manager::get_recipe_ids_from_post();
			if ( !empty( $recipes ) )
				$recipe_id	 = $recipes[ 0 ];
			else {
				$recipe_id = preg_replace( '/[^0-9]/', '', get_field( 'recipe_desc' ) );
			}
			$recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );
                        $recipe_pos = get_field('recipe_display');
			?>
			<li>
                            <?php if (!is_user_logged_in() && $recipe_pos === 'is_not_member') { ?>
                                <a href="javascript:void:(0)" data-remodal-target="modal-clip" class="link">
                                    <div class="pickup-suggest--thumb">
                                        <?php
                                        if (has_post_thumbnail()) :
                                            the_post_thumbnail('medium');
                                        else:
                                            echo do_shortcode('[wprm-recipe-image size=\'medium\']');
                                        endif;
                                        ?>
                                    </div>
                                    <div class="restrict-box">
                                        <span class="label-orange">会員限定</span><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-lock-orange.svg" style="width: auto;">
                                    </div>
                                    <p class="date big"><?php the_time('Y.m.d'); ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d'))) ?>]</p>
                                    <p class="pickup-title"><?php the_title() ?></p>
                                </a>
                            <?php } else { ?>                                                                                        
                                <a href="<?php the_permalink(); ?>" class="link">
                                    <div class="pickup-suggest--thumb">
                                        <?php
                                        if (has_post_thumbnail()) :
                                            the_post_thumbnail('medium');
                                        else:
                                            echo do_shortcode('[wprm-recipe-image size=\'medium\']');
                                        endif;
                                        ?>
                                    </div>
                                    <div class="restrict-box"></div>
                                    <p class="date big"><?php the_time('Y.m.d'); ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d'))) ?>]</p>
                                    <p class="pickup-title"><?php the_title() ?></p>
                                </a>
                            <?php } ?>				
			</li>
			<?php
		endwhile;
		wp_reset_postdata();
		echo '</ul>';
	endif;
}

function user_is_liked( $p ) {
	if ( !is_user_logged_in() )
		return false;

	$user_id	 = get_current_user_id();
	$posts_liked = get_user_meta( $user_id, 'tcd_likes', true );
	if ( $posts_liked ) {
		$posts_liked = array_map( 'intval', explode( ',', $posts_liked ) );
		return in_array( $p, $posts_liked );
	}
	return false;
}

/**
 * 自動ログアウトされる
 * @param type $expire
 * @return int
 * MINUTE_IN_SECONDS  = 60 (秒)
 * HOUR_IN_SECONDS    = 60 * MINUTE_IN_SECONDS  (1時間)
 * DAY_IN_SECONDS     = 24 * HOUR_IN_SECONDS    (1日)
 * WEEK_IN_SECONDS    = 7 * DAY_IN_SECONDS      (1週間)
 * YEAR_IN_SECONDS    = 365 * DAY_IN_SECONDS    (1年)
 */
function auto_logout_time_change( $expire ) {
	return 72 * HOUR_IN_SECONDS;
}

add_filter( 'auth_cookie_expiration', 'auto_logout_time_change' );

if ( !current_user_can( 'manage_options' ) ) {
	add_filter( 'show_admin_bar', '__return_false' );
}

/* --- Man add */
add_action( 'pre_get_posts', function( $q ) {
	if ( $title = $q->get( '_meta_or_title' ) ) {
		add_filter( 'get_meta_sql', function( $sql ) use ( $title ) {
			global $wpdb;

			// Only run once:
			static $nr = 0;
			if ( 0 != $nr++ )
				return $sql;

			// Modified WHERE
			$sql[ 'where' ] = sprintf(
			" AND ( %s OR %s OR %s ) ",
   $wpdb->prepare( "{$wpdb->posts}.post_title like '%%%s%%'", $title ),
				   $wpdb->prepare( "{$wpdb->posts}.post_content like '%%%s%%'", $title ),
					   mb_substr( $sql[ 'where' ], 5, mb_strlen( $sql[ 'where' ] ) )
			);

			return $sql;
		} );
	}
} );
/* Man add --- */


if ( !has_action( 'admin_footer', 'alert_category' ) ) {
	add_action( 'admin_footer', 'alert_category' );
}

function alert_category() {
	echo <<< EOF
<script type="text/javascript">

  jQuery("#post").attr("onsubmit", "return check_category();");

  function check_category(){
    var check_num = jQuery("#taxonomy-recipe_category input:checked").val();
    if(check_num == 0){
      alert("カテゴリが選択されていません。");
      jQuery("#ajax-loading").css("visibility","hidden");
      jQuery("#publish").removeClass("button-primary-disabled");
      return false;
    }else{
      return true;
    }
  }

</script>';
EOF;
}

/**
 * レシピの詳細ページに開くまえに、会員情報確認
 */
add_action('template_redirect', function() {
    global $post;        
    $recipe_pos = get_field('recipe_display');
    if (!is_user_logged_in() && is_singular('recipe') && $recipe_pos === 'is_not_member') {
        wp_redirect(home_url().'?nonce='.wp_create_nonce( 'recipe_member_nonce' ));
        exit;
    }
});

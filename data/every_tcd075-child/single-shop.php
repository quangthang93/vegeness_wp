<?php
  $status = $_GET['st_status']?:1;
  $c = filter_var($status, FILTER_VALIDATE_INT);
  if ($c===false) {
    $status = 1;
  }

  add_action( 'wp_enqueue_scripts', 'nk_theme_enqueue_styles_shop' );
  get_header();
?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?><?php
  $shop_meta = nk_shop_meta($post->ID);
  $shop_icons = nk_shop_icons($post->ID);
  $shop_genres = nk_shop_genres($post->ID);
  $shop_areas = nk_shop_areas($post->ID);

  $todo = $shop_areas[1]?:null;
  $area = $shop_areas[2]?:null;

  $swipe_num = $shop_meta['swipe_num']??1;

?><article id="article">

<div class="shop_name">
  <h1><?php echo esc_html($shop_meta['name']??'') ?></h1>
</div>
<div class="shop_meta_tag">
  <?php if ($area) { ?>
    <div><a href="/?map=y&sarea=<?php echo $area->term_id ?>"><?php echo esc_html($area->name) ?></a></div>
  <?php } ?>
  <?php if (count($shop_genres)>0) { ?>
  <div>
  <?php foreach ($shop_genres as $k => $v) { ?>
    <span><a href="/?map=y&genre=<?php echo $v->term_id ?>"><?php echo esc_html($v->name) ?></a></span>
  <?php } ?>
  </div>
  <?php } ?>
</div>



<div class="shop_tabs">
  <input id="shoptab_top" type="radio" name="shop_tab_item" value="1" <?php echo (($status==1)?'checked':''); ?>>
  <label class="shop_tab_item" for="shoptab_top">TOP</label>
  <input id="shoptab_photo" type="radio" name="shop_tab_item" value="2" <?php echo (($status==2)?'checked':''); ?>>
  <label class="shop_tab_item" for="shoptab_photo">写真</label>
  <input id="shoptab_repo" type="radio" name="shop_tab_item" value="3" <?php echo (($status==3)?'checked':''); ?>>
  <label class="shop_tab_item" for="shoptab_repo">詳細</label>

  <div class="shop_tab_content" id="shoptab_top_content">
    <div class="shop_tab_content_description">

<?php if (isset($shop_meta['swipe_imgs'])&&is_array($shop_meta['swipe_imgs'])) { ?>
  <!-- Swiper -->
  <div class="swiper-container gallery-top">
    <div class="swiper-wrapper">
      <?php $i = 0; foreach ($shop_meta['swipe_imgs'] as $k => $v) { ?>
      <?php if ($swipe_num <= $i) { break; } ?>
      <div class="swiper-slide"><?php echo wp_get_attachment_image($v,'large'); ?></div>
      <?php $i++; } ?>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>
  <div class="swiper-container gallery-thumbs">
    <div class="swiper-wrapper">
      <?php $i = 0; foreach ($shop_meta['swipe_imgs'] as $k => $v) { ?>
      <?php if ($swipe_num <= $i) { break; } ?>
      <div class="swiper-slide"><?php echo wp_get_attachment_image($v,'large'); ?></div>
      <?php $i++; } ?>
    </div>
  </div>
<?php }else if(has_post_thumbnail()) { ?>
<div class="shop_img">
<img src="<?php echo (wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' )[0]) ?>">
</div>
<?php } ?>

<?php if ($shop_meta['title']??'' != '') { ?>
<div class="shop_title"><p><?php echo esc_html($shop_meta['title']??'') ?></p></div>
<?php } ?>

<?php if ($shop_meta['desc']??'' != '') { ?>
<div class="shop_desc"><p><?php echo esc_html($shop_meta['desc']??'') ?></p></div>
<?php } ?>

  <div class="shop-databox">
    <dl>
      <dt>都道府県</dt>
      <dd>
        <?php if ($todo) { ?>
        <a href="/?map=y&area=<?php echo $todo->slug ?>"><?php echo esc_html($todo->name) ?></a>
        <?php } ?>
      </dd>
    </dl>
    <dl>
      <dt>エリア</dt>
      <dd>
        <?php if ($area) { ?>
        <a href="/?map=y&sarea=<?php echo $area->term_id ?>"><?php echo esc_html($area->name) ?></a>
        <?php } ?>
      </dd>
    </dl>
    <dl>
      <dt>ジャンル</dt>
      <dd>
        <?php foreach ($shop_genres as $k => $v) { ?>
          <span><a href="/?map=y&genre=<?php echo $v->term_id ?>"><?php echo esc_html($v->name) ?></a></span>
        <?php } ?>
      </dd>
    </dl>
    <dl>
      <dt>平均予算</dt>
      <dd>
        <?php if ($shop_meta['price']??'') { ?>
          <div><?php echo esc_html($shop_meta['price']??'') ?></div>
        <?php } ?>
        <div class="shop_dn">
        <?php if ($shop_meta['price_d']??'') { ?>
          <span>ランチ : <?php echo esc_html($shop_meta['price_d']??'') ?></span>
        <?php } ?>
        <?php if ($shop_meta['price_n']??'') { ?>
          <span>ディナー : <?php echo esc_html($shop_meta['price_n']??'') ?></span>
        <?php } ?>
      </div>
      </dd>
    </dl>
    <dl>
      <dt>営業時間</dt>
      <dd>
        <?php if ($shop_meta['business_h']??'') { ?>
          <div><?php echo esc_html($shop_meta['business_h']??'') ?></div>
        <?php } ?>
        <div class="shop_dn">
        <?php if ($shop_meta['business_hd']??'') { ?>
          <span>ランチ : <?php echo esc_html($shop_meta['business_hd']??'') ?></span>
        <?php } ?>
        <?php if ($shop_meta['business_hn']??'') { ?>
          <span>ディナー : <?php echo esc_html($shop_meta['business_hn']??'') ?></span>
        <?php } ?>
      </div>
      </dd>
    </dl>
    <dl>
      <dt>定休日</dt>
      <dd>
      <?php echo esc_html($shop_meta['holiday']??'') ?>
      </dd>
    </dl>
    <dl>
      <dt>電話番号</dt>
      <dd>
      <?php echo esc_html($shop_meta['tel']??'') ?>
      </dd>
    </dl>
    <dl>
      <dt>住所</dt>
      <dd>
        <div>〒<?php echo esc_html($shop_meta['zip']??'') ?> <?php echo esc_html($shop_meta['addr']??'') ?></div>
        <div><?php echo esc_html($shop_meta['acc']??'') ?></div>
        <?php if ($shop_meta['map']??''!='') { ?>
          <div class="shop_map"><?php echo ($shop_meta['map']??'') ?></div>
        <?php } ?>
      </dd>
    </dl>
    <dl>
      <dt>支払い方法</dt>
      <dd>
      <?php echo esc_html($shop_meta['credit']??'') ?>
      </dd>
    </dl>
    <dl>
      <dt>座席数</dt>
      <dd>
      <?php echo esc_html($shop_meta['seats']??'') ?>
      </dd>
    </dl>
    <dl>
      <dt>駐車場</dt>
      <dd>
      <?php echo esc_html($shop_meta['parking']??'') ?>
      </dd>
    </dl>
    <dl>
      <dt>URL</dt>
      <dd>
      <a href="<?php echo esc_url($shop_meta['hp']??'') ?>" target="_blank" rel="noopener"><?php echo esc_url($shop_meta['hp']??'') ?></a>
      </dd>
    </dl>
  </div>

  <?php if (count($shop_icons) > 0) { ?>
  <ul class="shop_tag">
    <?php foreach ($shop_icons as $k => $v) { ?>
    <li>
      <div class="shop_tag_img"><img src="<?php echo esc_url($v['m']['img_src']??'') ?>"></div>
      <div class="shop_tag_desc"><?php echo esc_html($v['i']->name) ?></div>
    </li>
    <?php } ?>
  </ul>
  <?php } ?>
  </div>
  </div>

  <div class="shop_tab_content" id="shoptab_photo_content">
    <div class="shop_tab_content_description">
      <div class="shop_photo_content">
      <?php if (isset($shop_meta['swipe_imgs'])&&is_array($shop_meta['swipe_imgs'])) { ?>
      <div class="shop_imgs">
      <?php foreach ($shop_meta['swipe_imgs'] as $k => $v) { ?>
      <div class="shop_imgs_one">
      <a href="<?php echo wp_get_attachment_image_src($v,'large')[0] ?>" class="gallery-luminous">
      <?php echo wp_get_attachment_image($v,'medium'); ?>
      </a>
      </div>
      <?php } ?>
      </div>
      <?php } ?>
      </div>
    </div>
  </div>

  <div class="shop_tab_content" id="shoptab_repo_content">
    <div class="shop_tab_content_description">
      <div class="shop_content">
      <?php the_content() ?>
      </div>
    </div>
  </div>
</div>

  </article>
    <?php endwhile; wp_reset_query(); ?>
  <?php
    endif;
  ?>

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->
<script>
!function(t){"use strict";function n(t){t=t||"",(t instanceof URLSearchParams||t instanceof n)&&(t=t.toString()),this[l]=i(t)}function r(t){var n={"!":"%21","'":"%27","(":"%28",")":"%29","~":"%7E","%20":"+","%00":"\x00"};return encodeURIComponent(t).replace(/[!'\(\)~]|%20|%00/g,function(t){return n[t]})}function e(t){return t.replace(/[ +]/g,"%20").replace(/(%[a-f0-9]{2})+/gi,function(t){return decodeURIComponent(t)})}function o(n){var r={next:function(){var t=n.shift();return{done:void 0===t,value:t}}};return v&&(r[t.Symbol.iterator]=function(){return r}),r}function i(t){var n={};if("object"==typeof t)if(c(t))for(var r=0;r<t.length;r++){var o=t[r];if(!c(o)||2!==o.length)throw new TypeError("Failed to construct 'URLSearchParams': Sequence initializer must only contain pair elements");a(n,o[0],o[1])}else for(var i in t)t.hasOwnProperty(i)&&a(n,i,t[i]);else{0===t.indexOf("?")&&(t=t.slice(1));for(var u=t.split("&"),s=0;s<u.length;s++){var f=u[s],h=f.indexOf("=");h>-1?a(n,e(f.slice(0,h)),e(f.slice(h+1))):f&&a(n,e(f),"")}}return n}function a(t,n,r){var e="string"==typeof r?r:null!==r&&void 0!==r&&"function"==typeof r.toString?r.toString():JSON.stringify(r);u(t,n)?t[n].push(e):t[n]=[e]}function c(t){return!!t&&"[object Array]"===Object.prototype.toString.call(t)}function u(t,n){return Object.prototype.hasOwnProperty.call(t,n)}var s=function(){try{if(t.URLSearchParams&&"bar"===new t.URLSearchParams("foo=bar").get("foo"))return t.URLSearchParams}catch(n){}return null}(),f=s&&"a=1"===new s({a:1}).toString(),h=s&&"+"===new s("s=%2B").get("s"),l="__URLSearchParams__",p=s?function(){var t=new s;return t.append("s"," &"),"s=+%26"===t.toString()}():!0,g=n.prototype,v=!(!t.Symbol||!t.Symbol.iterator);if(!(s&&f&&h&&p)){g.append=function(t,n){a(this[l],t,n)},g["delete"]=function(t){delete this[l][t]},g.get=function(t){var n=this[l];return this.has(t)?n[t][0]:null},g.getAll=function(t){var n=this[l];return this.has(t)?n[t].slice(0):[]},g.has=function(t){return u(this[l],t)},g.set=function(t,n){this[l][t]=[""+n]},g.toString=function(){var t,n,e,o,i=this[l],a=[];for(n in i)for(e=r(n),t=0,o=i[n];t<o.length;t++)a.push(e+"="+r(o[t]));return a.join("&")};var y=!h,S=!y&&s&&!f&&t.Proxy;Object.defineProperty(t,"URLSearchParams",{value:S?new Proxy(s,{construct:function(t,r){return new t(new n(r[0]).toString())}}):n});var d=t.URLSearchParams.prototype;d.polyfill=!0,d.forEach=d.forEach||function(t,n){var r=i(this.toString());Object.getOwnPropertyNames(r).forEach(function(e){r[e].forEach(function(r){t.call(n,r,e,this)},this)},this)},d.sort=d.sort||function(){var t,n,r,e=i(this.toString()),o=[];for(t in e)o.push(t);for(o.sort(),n=0;n<o.length;n++)this["delete"](o[n]);for(n=0;n<o.length;n++){var a=o[n],c=e[a];for(r=0;r<c.length;r++)this.append(a,c[r])}},d.keys=d.keys||function(){var t=[];return this.forEach(function(n,r){t.push(r)}),o(t)},d.values=d.values||function(){var t=[];return this.forEach(function(n){t.push(n)}),o(t)},d.entries=d.entries||function(){var t=[];return this.forEach(function(n,r){t.push([r,n])}),o(t)},v&&(d[t.Symbol.iterator]=d[t.Symbol.iterator]||d.entries)}}("undefined"!=typeof global?global:"undefined"!=typeof window?window:this);
(function( $ ) {
  $(function(){
    if( window.history && window.history.pushState ){
      $('input[name="shop_tab_item"]').on('change',function(e){
        window.history.pushState(null,null,'?st_status='+$(this).val());
      });
      $(window).on('popstate', function(e) {
        var searchParams = new URLSearchParams(window.location.search);
        var st_status = 1;
        if (searchParams.has('st_status')) {
          st_status = searchParams.get('st_status');
        }
        $('.shop_tabs input[value="'+st_status+'"]')
          .prop('checked',true);
      });
    }
  });
})(jQuery);
</script>

<?php get_footer(); ?>
<?php $options = get_design_plus_option(); ?>
<?php if ($_GET['s_type']=='menu') { ?>
<?php 
	$menus = nk_type_menus_all();
?>
<div class="s_type_ttlwb"><span></span><a href="/allrecipes/">＞＞全てののレシピを見る</a></div>
<?php foreach ($menus as $k => $v) { ?>
<div class="s_type_items_w">
<div class="s_type_ttlb"><?php echo $v['n'] ?></div>
<?php foreach ($v['lists'] as $k2 => $v2) { ?>
<?php 
	$image_id = isset($options['stype_menu_s'.$k2.'_img']) ?$options['stype_menu_s'.$k2.'_img']:'';
	$image = '';
	if(!empty($image_id)) {
	    $image = wp_get_attachment_image_src($image_id, 'full');
	}
	$desc = isset($options['stype_menu_s'.$k2.'_desc']) ?$options['stype_menu_s'.$k2.'_desc']:'';
?>
<div class="s_type_items">
	<div class="s_type_item_main">
		<div class="s_type_item_img_w">
			<?php if ($image!='') { ?>
			<div class="s_type_item_img">
				<div>
					<img src="<?php echo esc_attr($image[0]); ?>">
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="s_type_item_desc">
			<div class="s_type_ttl"><a href="/?s_type=menu&type=<?php echo $k2 ?>"><?php echo $v2['n'] ?></a></div>
			<div><?php echo $desc ?></div>
		</div>
	</div>
	<div class="s_type_item_links">
		<ul>
			<?php foreach ($v2['lists'] as $k3 => $v3) { ?>
			<li><a href="/?s_type=menu&type=<?php echo ($k2 + $k3) ?>"><?php echo $v3['n'] ?><span>&gt;</span></a></li>
			<?php } ?>
		</ul>
	</div>
</div>
<?php } ?>
</div>
<?php } ?>
<?php }else{ ?>
<?php $ingredients = nk_type_ingredients(); ?>
<div class="s_type_ttlwb"><span>材料で探す</span><a href="/allrecipes/">＞＞全てののレシピを見る</a></div>
<?php foreach ($ingredients as $k => $v) { ?>
<?php if ( !isset($v['lists']) ) { continue; } ?>
<?php 
	$image_id = isset($options['stype_ingredients_s'.$k.'_img']) ?$options['stype_ingredients_s'.$k.'_img']:'';
	$image = '';
	if(!empty($image_id)) {
	    $image = wp_get_attachment_image_src($image_id, 'full');
	}
	$desc = isset($options['stype_ingredients_s'.$k.'_desc']) ?$options['stype_ingredients_s'.$k.'_desc']:'';
?>
<div class="s_type_items_w">
<div class="s_type_items">
	<div class="s_type_item_main">
		<div class="s_type_item_img_w">
			<?php if ($image!='') { ?>
			<div class="s_type_item_img">
				<div>
					<img src="<?php echo esc_attr($image[0]); ?>">
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="s_type_item_desc">
			<div class="s_type_ttl"><a href="/?s_type=ingredients&type=<?php echo $k ?>"><?php echo $v['n'] ?></a></div>
			<div><?php echo $desc ?></div>
		</div>
	</div>
	<div class="s_type_item_links">
		<ul>
			<?php foreach ($v['lists'] as $k2 => $v2) { ?>
			<li><a href="/?s_type=ingredients&type=<?php echo ($k + $k2) ?>"><?php echo $v2['n'] ?><span>&gt;</span></a></li>
			<?php } ?>
		</ul>
	</div>
</div>
</div>
<?php } ?>
<?php } ?>
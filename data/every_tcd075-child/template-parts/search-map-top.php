<?php  
      $options = get_design_plus_option();
      $image_id = $options['shop_image'];
      if(!empty($image_id)) {
            $image = wp_get_attachment_image_src($image_id, 'full');
      }
?><?php if ($image_id) { ?>
<div id="map_areas">
      <img src="<?php echo esc_attr($image[0]); ?>">
</div>
<?php } ?>


<div id="main_contents" class="clearfix asr-mappage">
<div id="main_col" class="clearfix">



<div class="map-top-search">
<form role="search" method="get" class="searchform shopsearchform" action="/">
  <input type="hidden" name="map" value="y">
  <input type="hidden" name="map_s" value="1">
  <div>
    <label class="shop-screen-reader-text" for="shop_s_map">検索 …</label>
    <input type="text" value="" name="shop_s" id="shop_s_map" class="shop_s" placeholder="お店を検索" required="">
    <div class="shop-submit_button"><div class="submit_button"><input type="submit" value="検索" class="searchshopsubmit"></div></div>
  </div>
</form>
</div>

<div class="maps" align="center">
	<div class="map-box">
  <img src="/wp-content/uploads/map_pc.png" usemap="#Map" width="789" height="596" alt="都道府県から店舗を探す">
    <map name="Map">
  <area shape="rect" coords="61,18,181,55" href="/?map=y&area=kyushu_okinawa" alt="沖縄">
  <area shape="rect" coords="98,63,138,86" href="/?map=y&area=okinawa" alt="沖縄">
  <area shape="rect" coords="233,19,378,54" href="/?map=y&area=koshinetsu" alt="甲信越・北陸">
  <area shape="rect" coords="255,63,299,85" href="/?map=y&area=yamanashi" alt="山梨">
  <area shape="rect" coords="316,64,351,86" href="/?map=y&area=niigata" alt="新潟">
  <area shape="rect" coords="259,98,297,120" href="/?map=y&area=nagano" alt="長野">
  <area shape="rect" coords="311,96,351,124" href="/?map=y&area=toyama" alt="富山">
  <area shape="rect" coords="259,129,297,159" href="?map=y&area=ishikawa" alt="石川">
  <area shape="rect" coords="312,130,350,159" href="/?map=y&area=fukui" alt="福井">
  <area shape="rect" coords="575,19,703,53" href="/?map=y&area=hokkaido_tohoku" alt="北海道・東北">
  <area shape="rect" coords="582,63,638,88" href="/?map=y&area=hokkaido" alt="北海道">
  <area shape="rect" coords="655,62,691,86" href="/?map=y&area=aomori" alt="青森">
  <area shape="rect" coords="583,97,621,120" href="/?map=y&area=iwate" alt="岩手">
  <area shape="rect" coords="648,92,698,124" href="/?map=y&area=miyagi" alt="宮城">
  <area shape="rect" coords="582,131,622,157" href="/?map=y&area=akita" alt="秋田">
  <area shape="rect" coords="651,136,699,160" href="/?map=y&area=yamagata" alt="山形">
  <area shape="rect" coords="579,169,623,197" href="/?map=y&area=fukushima" alt="福島">
  <area shape="rect" coords="61,195,185,235" href="/?map=y&area=chugoku" alt="中国">
  <area shape="rect" coords="73,241,110,263" href="/?map=y&area=tottori" alt="鳥取">
  <area shape="rect" coords="123,239,165,265" href="/?map=y&area=shimane" alt="島根">
  <area shape="rect" coords="70,275,111,303" href="/?map=y&area=okayama" alt="岡山">
  <area shape="rect" coords="123,276,166,300" href="/?map=y&area=hiroshima" alt="広島">
  <area shape="rect" coords="70,310,112,338" href="/?map=y&area=yamaguchi" alt="山口">
  <area shape="rect" coords="579,209,712,251" href="/?map=y&area=kanto" alt="関東">
  <area shape="rect" coords="589,255,631,284" href="/?map=y&area=tokyo" alt="東京">
  <area shape="rect" coords="642,254,703,286" href="/?map=y&area=kanagawa" alt="神奈川">
  <area shape="rect" coords="586,292,630,321" href="/?map=y&area=saitama" alt="埼玉">
  <area shape="rect" coords="642,292,689,321" href="/?map=y&area=chiba" alt="千葉">
  <area shape="rect" coords="588,330,631,357" href="/?map=y&area=ibaraki" alt="茨城">
  <area shape="rect" coords="641,328,686,358" href="/?map=y&area=gunma" alt="群馬">
  <area shape="rect" coords="588,368,635,393" href="/?map=y&area=tochigi" alt="栃木">
  <area shape="rect" coords="56,408,187,442" href="/?map=y&area=kyushu_okinawa" alt="九州・沖縄">
  <area shape="rect" coords="73,454,114,479" href="/?map=y&area=fukuoka" alt="福岡">
  <area shape="rect" coords="128,454,169,482" href="/?map=y&area=saga" alt="佐賀">
  <area shape="rect" coords="71,487,117,515" href="/?map=y&area=nagasaki" alt="長崎">
  <area shape="rect" coords="125,489,166,515" href="/?map=y&area=kumamoto" alt="熊本">
  <area shape="rect" coords="72,525,115,552" href="/?map=y&area=oita" alt="大分">
  <area shape="rect" coords="121,525,171,553" href="/?map=y&area=miyazaki" alt="宮崎">
  <area shape="rect" coords="65,558,140,587" href="/?map=y&area=kagoshima" alt="鹿児島">
  <area shape="rect" coords="236,410,363,446" href="/?map=y&area=shikoku" alt="四国">
  <area shape="rect" coords="252,452,292,481" href="/?map=y&area=tokushima" alt="徳島">
  <area shape="rect" coords="304,451,347,480" href="/?map=y&area=kagawa" alt="香川">
  <area shape="rect" coords="250,486,295,518" href="/?map=y&area=ehime" alt="愛媛">
  <area shape="rect" coords="306,487,346,515" href="/?map=y&area=kochi" alt="高知">
  <area shape="rect" coords="413,403,542,449" href="/?map=y&area=kansai" alt="関西">
  <area shape="rect" coords="423,453,467,480" href="/?map=y&area=osaka" alt="大阪">
  <area shape="rect" coords="479,451,522,480" href="/?map=y&area=hyogo" alt="兵庫">
  <area shape="rect" coords="420,486,467,512" href="/?map=y&area=kyoto" alt="京都">
  <area shape="rect" coords="478,489,526,509" href="/?map=y&area=shiga" alt="滋賀">
  <area shape="rect" coords="421,524,467,550" href="/?map=y&area=nara" alt="奈良">
  <area shape="rect" coords="478,520,539,553" href="/?map=y&area=wakayama" alt="和歌山">
  <area shape="rect" coords="586,406,719,446" href="/?map=y&area=tokai" alt="東海">
  <area shape="rect" coords="603,454,647,480" href="/?map=y&area=aichi" alt="愛知">
  <area shape="rect" coords="658,452,698,480" href="/?map=y&area=gifu" alt="岐阜">
  <area shape="rect" coords="602,491,647,518" href="/?map=y&area=shizuoka" alt="静岡">
  <area shape="rect" coords="659,489,700,513" href="/?map=y&area=mie" alt="三重">
   </map>

 </div>
</div>

		
<div class="maps2" align="center">
      <div class="maps2_ttl">都道府県を選択</div>
	<div class="maps2_inner" align="left">
<?php 
	$l_areas = get_terms( 'shop_area', array( 'hide_empty' => false, 'orderby' => 'id', 'parent' => 0 ) ); 

  $areas = [];
  foreach ($l_areas as $k => $v) {
    $areas[$v->slug] = $v;
  }
  $lists = [
    'hokkaido_tohoku',
    'kanto',
    'koshinetsu',
    'tokai',
    'kansai',
    'chugoku',
    'shikoku',
    'kyushu_okinawa'
  ];
?>
	<ul class="sel_area">
		<?php foreach ($lists as $k => $area_key) { ?>
			<?php 
        if (is_null($areas[$area_key])) {
          continue;
        }
        $v = $areas[$area_key];
				$local_areas = get_terms( 'shop_area', array( 'hide_empty' => false, 'orderby' => 'id', 'parent' => $v->term_id ) ); 
			?>
			<li>
				<div><a href="?map=y&area=<?php echo $v->slug ?>"><?php echo esc_html($v->name) ?></a></div>
				<ul>
				<?php if (count($local_areas) > 0) { ?>
					<?php foreach ($local_areas as $k2 => $v2) { ?>
					<li><a href="?map=y&area=<?php echo $v2->slug ?>"><?php echo esc_html($v2->name) ?></a></li>
					<?php } ?>
				<?php } ?>
				</ul>
			</li>
		<?php } ?>
		</ul>
	</div>
</div>




 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->
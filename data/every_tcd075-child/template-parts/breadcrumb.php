<?php
     $options = get_design_plus_option();
?>
<div id="bread_crumb">
<?php if (isset($_GET['map'])) { ?>
<?php global $area_obj,$genre_obj,$sarea_obj; ?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <?php if ($_GET['map_s']==1) { ?>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>?map=y"><span itemprop="name">お店を探す</span></a><meta itemprop="position" content="2"></li>
<li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">詳細検索</span><meta itemprop="position" content="3"></li>
 <?php }else if (!is_null($sarea_obj)) { ?>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>?map=y"><span itemprop="name">お店を探す</span></a><meta itemprop="position" content="2"></li>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>?map=y&area=<?php echo $area_obj->slug ?>"><span itemprop="name"><?php echo esc_html($area_obj->name) ?>エリア</span></a><meta itemprop="position" content="3"></li> 
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html($sarea_obj->name) ?>エリア</span><meta itemprop="position" content="4"></li>
<?php }else if(!is_null($area_obj)){?>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>?map=y"><span itemprop="name">お店を探す</span></a><meta itemprop="position" content="2"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html($area_obj->name) ?>エリア</span><meta itemprop="position" content="3"></li>
<?php }else if (!is_null($genre_obj)) { ?>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>?map=y"><span itemprop="name">お店を探す</span></a><meta itemprop="position" content="2"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html($genre_obj->name) ?></span><meta itemprop="position" content="3"></li>
<?php }else{ ?>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">お店を探す</span><meta itemprop="position" content="2"></li>
<?php } ?>
</ul>
<?php }else if (is_singular('shop')) { ?><?php
  $shop_areas = nk_shop_areas($post->ID);
  $shop_meta = nk_shop_meta($post->ID);
  $todo = $shop_areas[1]?:null;
  $area = $shop_areas[2]?:null;
  $shop_pos = 2;
?><ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
  <?php if ($todo) { ?>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>?map=y&area=<?php echo $todo->slug ?>"><span itemprop="name"><?php echo esc_html($todo->name) ?></span></a><meta itemprop="position" content="<?php echo $shop_pos ?>"></li>
  <?php $shop_pos++; } ?>
  <?php if ($area) { ?>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>?map=y&sarea=<?php echo $area->term_id ?>"><span itemprop="name"><?php echo esc_html($area->name) ?></span></a><meta itemprop="position" content="<?php echo $shop_pos ?>"></li>
  <?php $shop_pos++; } ?>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html($shop_meta['name']??'') ?></span><meta itemprop="position" content="<?php echo $shop_pos ?>"></li>
</ul>
<?php
     // member page -----------------------
  }else if(get_tcd_membership_memberpage_type()) {
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html(get_tcd_membership_memberpage_title()); ?></span><meta itemprop="position" content="2"></li>
</ul>
<?php
     // recipe archive -----------------------
     }elseif(is_post_type_archive('recipe')) {
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html($options['recipe_label']); ?></span><meta itemprop="position" content="2"></li>
</ul>
<?php
     // recipe category -----------------------
     } elseif(is_tax('recipe_category')) {
       $query_obj = get_queried_object();
       $parent_cat_id = $query_obj->parent;
       if($parent_cat_id == 0){ // parent category page
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(get_post_type_archive_link('recipe')); ?>"><span itemprop="name"><?php echo esc_html($options['recipe_label']); ?></span></a><meta itemprop="position" content="2"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo single_cat_title("", false); ?></span><meta itemprop="position" content="3"></li>
</ul>
<?php
       } else { // child category page
         $parent_category_data = get_term_by('id', $parent_cat_id, 'recipe_category');
         $parent_category_name = $parent_category_data->name;
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(get_post_type_archive_link('recipe')); ?>"><span itemprop="name"><?php echo esc_html($options['recipe_label']); ?></span></a><meta itemprop="position" content="2"></li>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(get_term_link($parent_cat_id,'recipe_category')); ?>"><span itemprop="name"><?php echo esc_html($parent_category_name); ?></span></a><meta itemprop="position" content="3"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html(single_cat_title("", false)); ?></span><meta itemprop="position" content="4"></li>
</ul>
<?php
       }
     // recipe single page
     } elseif(is_singular('recipe')) {
       $args = array( 'orderby' => 'term_order' );
       $recipe_category = wp_get_post_terms( $post->ID, 'recipe_category' ,$args);
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(get_post_type_archive_link('recipe')); ?>"><span itemprop="name"><?php echo esc_html($options['recipe_label']); ?></span></a><meta itemprop="position" content="2"></li>
 <?php
      if ( $recipe_category && ! is_wp_error($recipe_category) ) {
        foreach ( $recipe_category as $cat ) :
          $parent_cat_id = $cat->parent;
          if($parent_cat_id != 0) {
            $child_cat_name = $cat->name;
            $child_cat_id = $cat->term_id;
            break;
          } else {
            $parent_cat_id = $cat->term_id;
          }
        endforeach;
        $parent_category_data = get_term_by('id', $parent_cat_id, 'recipe_category');
 ?>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="category">
  <a itemprop="item" href="<?php echo esc_url(get_category_link($parent_cat_id)); ?>"><span itemprop="name"><?php echo esc_html($parent_category_data->name); ?></span></a>
  <meta itemprop="position" content="3">
 </li>
 <?php if($parent_cat_id != 0) { ?>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="category">
  <a itemprop="item" href="<?php echo esc_url(get_category_link($child_cat_id)); ?>"><span itemprop="name"><?php echo esc_html($child_cat_name); ?></span></a>
  <meta itemprop="position" content="4">
 </li>
 <?php }; ?>
 <?php }; ?>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php the_title_attribute(); ?></span><meta itemprop="position" content="5"></li>
</ul>
<?php
     // news archive -----------------------
     } elseif(is_post_type_archive('news')) {
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html($options['news_label']); ?></span><meta itemprop="position" content="2"></li>
</ul>
<?php
     // news single -----------------------
     } elseif(is_singular('news')) {
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(get_post_type_archive_link('news')); ?>"><span itemprop="name"><?php echo esc_html($options['news_label']); ?></span></a><meta itemprop="position" content="2"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php the_title_attribute(); ?></span><meta itemprop="position" content="3"></li>
</ul>
<?php
     // Search -----------------------
     } elseif(is_search()) {
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php _e('Search result','tcd-w'); ?></span><meta itemprop="position" content="2"></li>
</ul>
<?php
     // Blog page -----------------------
     } elseif(is_home()) {
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html($options['blog_label']); ?></span><meta itemprop="position" content="2"></li>
</ul>
<?php
     // Author page -----------------------
     } elseif(is_page_template('page-author-list.php')) {
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html($options['author_parent_page_name']); ?></span><meta itemprop="position" content="2"></li>
</ul>
<?php
     } elseif(is_author()) {
       $query_obj = get_queried_object();
       $author_id = $query_obj->ID;
       $user_data = get_userdata($author_id);
       $author_name = $user_data->display_name;
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <?php if($options['author_parent_page_url']) { ?><li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url($options['author_parent_page_url']); ?>"><span itemprop="name"><?php echo esc_html($options['author_parent_page_name']); ?></span></a><meta itemprop="position" content="2"></li><?php }; ?>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html($author_name); ?></span><meta itemprop="position" content="3"></li>
</ul>
<?php
     // Category, Tag , Archive page -----------------------
     } elseif(is_category() || is_tag() || is_day() || is_month() || is_year()) {
       if (is_category()) {
         $title = single_cat_title('', false);
       } elseif( is_tag() ) {
         $title = single_tag_title('', false);
       } elseif (is_day()) {
         $title = sprintf(__('Archive for %s', 'tcd-w'), get_the_time(__('F jS, Y', 'tcd-w')) );
       } elseif (is_month()) {
         $title = sprintf(__('Archive for %s', 'tcd-w'), get_the_time(__('F, Y', 'tcd-w')) );
       } elseif (is_year()) {
         $title = sprintf(__('Archive for %s', 'tcd-w'), get_the_time(__('Y', 'tcd-w')) );
       };
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(get_permalink(get_option('page_for_posts'))); ?>"><span itemprop="name"><?php echo esc_html($options['blog_label']); ?></span></a><meta itemprop="position" content="2"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html($title); ?></span><meta itemprop="position" content="3"></li>
</ul>
<?php
     //  Page -----------------------
     } elseif(is_page()) {
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php the_title_attribute(); ?></span><meta itemprop="position" content="3"></li>
</ul>
<?php
     // Other page -----------------------
     } else {
     $category = get_the_category();
?>
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(get_permalink(get_option('page_for_posts'))); ?>"><span itemprop="name"><?php echo esc_html($options['blog_label']); ?></span></a><meta itemprop="position" content="2"></li>
 <?php if($category) { ?>
 <li class="category" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
  <?php
       $count=1;
       foreach ($category as $cat) {
  ?>
  <a itemprop="item" href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><span itemprop="name"><?php echo esc_html($cat->name); ?></span></a>
  <?php $count++; } ?>
  <meta itemprop="position" content="3">
 </li>
 <?php }; ?>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php the_title_attribute(); ?></span><meta itemprop="position" content="4"></li>
</ul>
<?php }; ?>

</div>

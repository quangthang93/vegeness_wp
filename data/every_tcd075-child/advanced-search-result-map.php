<?php
if ( !defined( 'ABSPATH' ) ) exit;
if (empty($_GET['map'])){
  header("Location: "."http://{$_SERVER['HTTP_HOST']}");
  exit();
}

add_action( 'wp_enqueue_scripts', 'nk_theme_enqueue_styles_shop' );

global $area_obj,$sarea_obj,$genre_obj,$genres_obj,$areas_obj;

$page = 1;
//パラメータの取得
if(isset($_GET['paged'])){
	$page = $_GET['paged'];
} else {
	$elms = explode('/', $_SERVER['REQUEST_URI']);
	if($elms[1] == 'page'){
		$page = (int)$elms[2];
	}
}
//ページの状態
if(filter_var($page, FILTER_VALIDATE_INT) === false){
	$page  = 1;
}

$query = [];

if(isset($_GET['area'])&&$_GET['area']!=''){

	$area = $_GET['area'];

	$c = filter_var($area, FILTER_VALIDATE_INT);
	if ($c !== false) {
		if($area=='1'){
			$area = 'hokkaido_tohoku';
		}elseif($area=="2"){
			$area = 'hokkaido_tohoku';
		}elseif($area=="3"){
			$area = 'kanto';
		}elseif($area=="4"){
			$area = 'koshinetsu_hokuriku';
		}elseif($area=="5"){
			$area = 'tokai';
		}elseif($area=="6"){
			$area = 'kansai';
		}elseif($area=="7"){
			$area = 'chugoku';
		}elseif($area=="8"){
			$area = 'shikoku';
		}elseif($area=="9"){
			$area = 'kyushu_okinawa';
		}
	}

	$areas = get_terms( 'shop_area', [ 'slug' => $area , 'hide_empty' => false] ); 
	if (isset($areas[0])) {
		$area_obj = $areas[0];
		$query['area'] = $area;
	}
}else if(isset($_GET['sarea'])&&$_GET['sarea']!=''){
	$sarea = $_GET['sarea'];

	$c = filter_var($sarea, FILTER_VALIDATE_INT);
	if ($c !== false) {
		$sareas = get_terms( 'shop_area', [ 'include' => $sarea , 'hide_empty' => false] ); 
		if (isset($sareas[0])) {
			$sarea_obj = $sareas[0];

			$ancestors = get_ancestors($sarea_obj->term_id,'shop_area');
			if (count($ancestors) >= 2) {
				$areas = get_terms( 'shop_area', [ 'include' => $ancestors[0] , 'hide_empty' => false] ); 
				if (isset($areas[0])) {
					$area_obj = $areas[0];
					$query['sarea'] = $sarea;
					$query['area'] = $areas[0]->slug;
				}
			}else{
				$area_obj = $sarea_obj;
				$sarea_obj = null;
				$query['area'] = $area_obj->slug;
			}
		}
	}
}

if(isset($_GET['genre'])&&$_GET['genre']!=''){
	$genre = $_GET['genre'];

	$c = filter_var($genre, FILTER_VALIDATE_INT);
	if ($c !== false) {
		$genres = get_terms( 'shop_genre', [ 'include' => $genre , 'hide_empty' => false] ); 
		if (isset($genres[0])) {
			$query['genre'] = $genre;
			$genre_obj = $genres[0];
		}
	}

}

if ($_GET['map_s']==1) {
	if(isset($_GET['genres'])&&is_array($_GET['genres'])&&count($_GET['genres'])>0){
		$l_genre_ids = [];
		foreach ($_GET['genres'] as $k => $v) {
			$c = filter_var($v, FILTER_VALIDATE_INT);
			if ($c !== false) {
				$l_genre_ids[] = $v;
			}
		}

		$l_genres = get_terms( 'shop_genre', [ 'include' => $l_genre_ids , 'hide_empty' => false] ); 
		if (count($l_genres) > 0) {
			$l_genre_ids = [];
			foreach ($l_genres as $k => $v) {
				$l_genre_ids[] = $v->term_id;
			}
			$query['genres'] = $l_genre_ids;
			$genres_obj = $l_genres;
		}
	}

	if(isset($_GET['areas'])&&is_array($_GET['areas'])&&count($_GET['areas'])>0){
		$l_area_ids = [];
		foreach ($_GET['areas'] as $k => $v) {
			$c = filter_var($v, FILTER_VALIDATE_INT);
			if ($c !== false) {
				$l_area_ids[] = $v;
			}
		}


		$l_areas = get_terms( 'shop_area', [ 'include' => $l_area_ids , 'hide_empty' => false] ); 
		if (count($l_areas) > 0) {
			$l_area_ids = [];
			foreach ($l_areas as $k => $v) {
				$l_area_ids[] = $v->term_id;
			}
			$query['areas'] = $l_area_ids;
			$areas_obj = $l_areas;
		}
	}
}

if (isset($_GET['shop_s'])&&$_GET['shop_s']!='') {
	$query['shop_s'] = $_GET['shop_s'];
}


if (count($query) > 0 || $_GET['map_s']==1) {

	$v_args = array(
	    'post_type' => array( 'shop' ),
	    'orderby' => 'ASC',
	    'posts_per_page' => 10,
	    'paged' => $page
	);

	$tax_query = [];

	if (isset($query['area'])) {
		$tax_query[] = array(
			'taxonomy' => 'shop_area',
			'field' => 'slug',
			'terms' => $query['area']
      	);
	}

	if (isset($query['sarea'])) {
		$tax_query[] = array(
			'taxonomy' => 'shop_area',
			'field' => 'id',
			'terms' => $query['sarea']
      	);
	}

	if (isset($query['areas'])) {
		$tax_query[] = array(
			'taxonomy' => 'shop_area',
			'field' => 'id',
			'terms' => $query['areas']
      	);
	}

	if (isset($query['genre'])) {
		$tax_query[] = array(
			'taxonomy' => 'shop_genre',
			'field' => 'id',
			'terms' => $query['genre']
      	);
	}

	if (isset($query['genres'])) {
		$tax_query[] = array(
			'taxonomy' => 'shop_genre',
			'field' => 'id',
			'terms' => $query['genres']
      	);
	}

	if (count($tax_query) > 0) {
		$v_args['tax_query'] = $tax_query;
	}
	
	if ($query['shop_s']!='') {

		function nk_like_filter( $where, $wp_query ){
		    global $wpdb;
		    if ( $search_term = $wp_query->get( 'nk_search_like' ) ) {
		    	$lists = [];
		    	$lists[] = $search_term;
			    $lv = mb_convert_kana($search_term, "KVC");
			    if ($search_term !== $lv) {
			      $lists[] = $lv;
			    }
			    $lv = mb_convert_kana($search_term, "HVc");
			    if ($search_term !== $lv) {
			      $lists[] = $lv;
			    }

			    $sql_str = '';

			    foreach ($lists as $k => $v) {
			    	if ($sql_str!='') {
			    		$sql_str .= 'OR';
			    	}
			    	$sql_str .= ' '.$wpdb->posts . '.post_title LIKE \'%' . esc_sql( like_escape( $v ) ) . '%\' OR ' . $wpdb->posts . '.post_content LIKE \'%' . esc_sql( like_escape( $v ) ) . '%\' OR ' . $wpdb->postmeta . '.meta_value LIKE \'%' . esc_sql( like_escape( $v ) ) . '%\' ';
			    }

		        $where .= ' AND (' .$sql_str. ') ';
		    }
		    return $where;
		}

		function nk_join_like_filter( $where, $wp_query ){
		    global $wpdb;

		    if ( $search_term = $wp_query->get( 'nk_search_like' ) ) {
		        $join .= "LEFT JOIN $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id and $wpdb->postmeta.meta_key = 'shop_meta' ";
		    }

		    return $join;
		};


		$v_args['nk_search_like'] = $query['shop_s'];

		add_filter( 'posts_where', 'nk_like_filter', 10, 2 );
		add_filter( 'posts_join', 'nk_join_like_filter', 10, 2 );
		$custom_query = new WP_Query($v_args);
		remove_filter( 'posts_join', 'nk_join_like_filter', 10, 2 );
		remove_filter( 'posts_where', 'nk_like_filter', 10, 2 );

	}else{
		$custom_query = new WP_Query($v_args);
	}

	$is_search = true;

} ?><?php get_header(); ?>
<?php get_template_part('template-parts/breadcrumb'); ?>


<?php if ($is_search) { ?>

<div id="main_contents" class="clearfix asr-mappage">
<div id="main_col" class="clearfix">

<div class="search_shop_lists_w">
<div class="search_shop_lists">
<button id="myAreaBtn" type="button">エリアを<span>選ぶ</span></button>
<button id="myGenreBtn" type="button">ジャンルを<span>選ぶ</span></button>
<form id="search_shop">
	<input type="hidden" name="map" value="y">
	<input type="hidden" name="map_s" value="1">
	<button type="submit">検索</button>
</form>
</div>
<div class="gcse-search-w">
<form role="search" method="get" id="shopsearchform" class="searchform shopsearchform" action="">
	<input type="hidden" name="map" value="y">
	<input type="hidden" name="map_s" value="1">
	<div>
		<label class="shop-screen-reader-text" for="shop_s">検索 …</label>
		<input type="text" value="" name="shop_s" id="shop_s" class="shop_s" placeholder="お店を検索" required="">
		<div class="shop-submit_button"><div class="submit_button"><input type="submit" id="searchshopsubmit" class="searchshopsubmit" value="検索"></div></div>
	</div>
</form>
</div>
</div>

<?php 
	$genres = get_terms( 'shop_genre', array( 'hide_empty' => false, 'orderby' => 'id', 'parent' => 0 ) ); 
?>
<div id="myGenreModal" class="shop-modal">
  <div class="shop-modal-content-w">
  <span class="shop-close shop-ok_btn">決定</span>
  <div class="shop-modal-content">
	<ul class="ssel_genre ssel_lists">
	<li>
	  <div class="acd-content-w" data-ban="genres">
	  <div class="acd-content acd-content-genres">
	  <ul>
	    <li class="gbtn"><button type="button" class="gclear" data-url="genres">全ジャンル</button></li>
	    <?php foreach ($genres as $k => $v) { ?>
	    <li><input type="checkbox" name="genres[]" id="fgs_<?php echo $v->term_id ?>" value="<?php echo $v->term_id ?>" data-ban="genres" class="s_check"><label for="fgs_<?php echo $v->term_id ?>"><?php echo esc_html($v->name) ?></label></li>
		<?php } ?>
	  </ul>
	  </div>
	  </div>
	</li>
	</ul>
  </div>
  </div>
</div>

<div id="myAreaModal" class="shop-modal">

  <div class="shop-modal-content-w">
  <span class="shop-close shop-ok_btn">決定</span>

  <div class="shop-modal-content">

<?php 
	$l_areas = get_terms( 'shop_area', array( 'hide_empty' => false, 'orderby' => 'id', 'parent' => 0 ) ); 
	$areas = [];
	foreach ($l_areas as $k => $v) {
		$areas[$v->slug] = $v;
	}
	$lists = [
		'hokkaido_tohoku',
		'kanto',
		'koshinetsu',
		'tokai',
		'kansai',
		'chugoku',
		'shikoku',
		'kyushu_okinawa'
	];
?>
<?php foreach ($lists as $k => $area_key) { ?>
<?php 
    if (is_null($areas[$area_key])) {
      continue;
    }
    $v = $areas[$area_key]; 
	$local_areas = get_terms( 'shop_area', array( 'hide_empty' => false, 'orderby' => 'id', 'parent' => $v->term_id ) );
	$local_areas2_lists = []; 
 	foreach ($local_areas as $k2 => $v2) {
 		$l_areas = get_terms( 'shop_area', array( 'hide_empty' => false, 'orderby' => 'id', 'parent' => $v2->term_id ) );
 		if (count($l_areas) > 0) {
			$local_areas2_lists[$v2->term_id] = $l_areas;
 		}
 	}
 
?><?php if (count($local_areas2_lists) > 0) { ?>
<ul class="ssel_area ssel_lists">
<li>
<div><?php echo esc_html($v->name) ?></div>
<?php foreach ($local_areas as $k2 => $v2) { ?>	
<?php if (isset($local_areas2_lists[$v2->term_id])) { ?>
<?php $local_areas2 = $local_areas2_lists[$v2->term_id]; ?>
<div class="acd-content-w" data-ban="<?php echo esc_html($v2->slug) ?>">
<input id="acd-c<?php echo $v2->term_id ?>" class="acd-check" type="radio" name="a_s">
<label class="acd-label" for="acd-c<?php echo $v2->term_id ?>"><?php echo esc_html($v2->name) ?></label>
<div class="acd-content acd-content-area acd-dcontent acd-content-<?php echo esc_html($v2->slug) ?>">
  <ul>
    <li class="gbtn"><button type="button" class="gclear" data-url="<?php echo esc_html($v2->slug) ?>" data-id="<?php echo $v2->term_id ?>"><?php echo esc_html($v2->name) ?>全体</button></li>
    <?php foreach ($local_areas2 as $k3 => $v3) { ?>
    <li><input type="checkbox" name="areas[]" id="fas_<?php echo $v3->term_id ?>" value="<?php echo $v3->term_id ?>" data-ban="<?php echo esc_html($v2->slug) ?>" class="s_check"><label for="fas_<?php echo $v3->term_id ?>"><?php echo esc_html($v3->name) ?></label></li>
	<?php } ?>
  </ul>
</div>
</div>
<?php } ?>
<?php } ?>
</li>
</ul>
<?php } ?>
<?php } ?>
</div>
</div>
</div>

<?php if ( $custom_query && $custom_query->have_posts() ){ ?>
<ul class="area-info">
    <?php
         global $post;
         while ( $custom_query->have_posts() ) :
          $custom_query->the_post(); 

  $shop_meta = nk_shop_meta($post->ID);
  $shop_genres = nk_shop_genres($post->ID);
  $shop_icons = nk_shop_icons($post->ID);
?>
	<li class="shop_list">
		<div class="shop_list_wrap">
			<div class="shop_list_ttl"><?php echo esc_html($shop_meta['name']??'') ?></div>
			<div class="shop_list_descs">
				<div class="shop_list_img">					
				<?php if(has_post_thumbnail()) { ?>
				<img src="<?php echo (wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' )[0]) ?>">
				<?php } ?>

				<div class="shop_list_links spc">
					<div><a class="shop_list_wrap_target" href="<?php the_permalink() ?>">店舗詳細</a></div>
					<div><a href="<?php echo esc_url($shop_meta['hp']??'') ?>" target="_blank" rel="noopener">公式サイトを見る</a></div>
				</div>

				</div>
				<div class="shop_list_desc">
					<div>〒<?php echo esc_html($shop_meta['zip']??'') ?> <?php echo esc_html($shop_meta['addr']??'') ?></div>
					<div><?php echo esc_html($shop_meta['acc']??'') ?></div>
  <div class="shop_list_box">
    <dl>
      <dt>営業時間</dt>
      <dd>
        <?php if ($shop_meta['business_h']??'') { ?>
          <div><?php echo esc_html($shop_meta['business_h']??'') ?></div>
        <?php } ?>
        <div class="shop_dn">
        <?php if ($shop_meta['business_hd']??'') { ?>
          <span>ランチ : <?php echo esc_html($shop_meta['business_hd']??'') ?></span>
        <?php } ?>
        <?php if ($shop_meta['business_hn']??'') { ?>
          <span>ディナー : <?php echo esc_html($shop_meta['business_hn']??'') ?></span>
        <?php } ?>
	    </div>
      </dd>
    </dl>
    <dl>
      <dt>定休日</dt>
      <dd>
      <?php echo esc_html($shop_meta['holiday']??'') ?>
      </dd>
    </dl>
    <dl>
      <dt>ジャンル</dt>
      <dd>
        <?php foreach ($shop_genres as $k => $v) { ?>
          <span><a href="/?map=y&genre=<?php echo $v->term_id ?>"><?php echo esc_html($v->name) ?></a></span>
        <?php } ?>
      </dd>
    </dl>
    <dl>
      <dt>平均予算</dt>
      <dd>
        <?php if ($shop_meta['price']??'') { ?>
          <div><?php echo esc_html($shop_meta['price']??'') ?></div>
        <?php } ?>
        <div class="shop_dn">
        <?php if ($shop_meta['price_d']??'') { ?>
          <span>ランチ : <?php echo esc_html($shop_meta['price_d']??'') ?></span>
        <?php } ?>
        <?php if ($shop_meta['price_n']??'') { ?>
          <span>ディナー : <?php echo esc_html($shop_meta['price_n']??'') ?></span>
        <?php } ?>
	    </div>
      </dd>
    </dl>
    <dl>
      <dt>駐車場</dt>
      <dd>
      <?php echo esc_html($shop_meta['parking']??'') ?>
      </dd>
    </dl>
  </div>

  <?php if (count($shop_icons) > 0) { ?>
  <ul class="shop_list_tag">
    <?php foreach ($shop_icons as $k => $v) { ?>
    <li>
      <div class="shop_list_tag_img"><img src="<?php echo esc_url($v['m']['img_src']??'') ?>"></div>
      <div class="shop_list_tag_desc"><?php echo esc_html($v['i']->name) ?></div>
    </li>
    <?php } ?>
  </ul>
  <?php } ?>

</div>
			</div>
			<div class="shop_list_links ssp"><a class="shop_list_wrap_target" href="<?php the_permalink() ?>">店舗詳細</a><a href="<?php echo esc_url($shop_meta['hp']??'') ?>" target="_blank" rel="noopener">公式サイトを見る</a></div>
		</div>
	</li>
    <?php endwhile; ?>
</ul>

<?php 
global $wp_query;
$old_wp_query = $wp_query;
$wp_query = $custom_query;
?>
<?php get_template_part('template-parts/navigation'); ?>
<?php 
$wp_query = $old_wp_query;
?>

<?php }else{ ?>

<p id="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></p>

<?php } ?>


 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->


<script>
(function($){
$(function(){

	var modal = document.getElementById("myAreaModal");
	var btn = document.getElementById("myAreaBtn");

	var modal2 = document.getElementById("myGenreModal");
	var btn2 = document.getElementById("myGenreBtn");

	btn.onclick = function() {
	  modal.style.display = "block";
	}
	btn2.onclick = function() {
	  modal2.style.display = "block";
	}
	var span = document.getElementsByClassName("shop-close");
	(Array.prototype.slice.call(span,0)).forEach(function(v,i){
		(function(elem){
			elem.onclick = function(e) {
		    	modal.style.display = "none";
		    	modal2.style.display = "none";
			}
		})(v);
	});
	window.onclick = function(e) {
	  if (e.target == modal) {
	    modal.style.display = "none";
	  }
	  if (e.target == modal2) {
	    modal2.style.display = "none";
	  }
	}

  var sCheck = function(lname){

    var elem1 = $('.acd-content-'+lname+' input[data-ban=\"'+lname+'\"]').length;
    var elem2 = $('.acd-content-'+lname+' input[data-ban=\"'+lname+'\"]:checked').length;

    var elem3 = $('.acd-content-'+lname+' .gclear[data-url="'+lname+'"]');
    if (elem1 != 0) {
      if (elem1 == elem2) {
        if (!elem3.hasClass('gclear_on')) {
          elem3.addClass('gclear_on');
        }
      }else{
        if (elem3.hasClass('gclear_on')) {
          elem3.removeClass('gclear_on');
        }
      }
    }

  };

  setTimeout(function(){
	  $('.acd-content-w').each(function(e,v){
	    var lname = $(v).data("ban");
	    sCheck(lname);
	  });
  });

  $('.s_check').on("change",function(e){
    var lname = $(this).data("ban");
    sCheck(lname);
  });
  
  $('.gclear').on("click",function(e){
    var self = $(this);
    var url = self.data("url");

    var elem = $('.acd-content-'+url+' input[data-ban=\"'+url+'\"]');

    if(self.hasClass('gclear_on')){
      self.removeClass('gclear_on');
      elem.prop("checked",false);
    }else{
      self.addClass('gclear_on');
      elem.prop("checked",true);
    }
  });
  $('#search_shop').on('submit',function(e){

  	$('#search_shop')
  		.find('input[name=\"genres[]\"]')
  		.remove();

  	$('#search_shop')
  		.find('input[name=\"areas[]\"]')
  		.remove();

    var elem1 = $('input[name=\"genres[]\"]');
    var elem1_num = elem1.length;
    var elem2 = $('input[name=\"genres[]\"]:checked');
    var elem2_num = elem2.length;

    if (elem2_num!=0&&elem1_num!=elem2_num) {
	  	elem2.each(function(e,v){
	  		$('#search_shop').append(
	  			$('<input>')
	  				.attr('type','hidden')
	  				.attr('name','genres[]')
	  				.attr('value',$(v).val())
	  		);
	  	});    	
    }

    $('.acd-content-area').each(function(i,v){
    	var elem = $(v);
    	var item = elem.find('.gclear_on');
    	if (item[0]) {
	  		$('#search_shop').append(
	  			$('<input>')
	  				.attr('type','hidden')
	  				.attr('name','areas[]')
	  				.attr('value',$(item[0]).data('id'))
	  		);
    	}else{
    		elem.find('input[name=\"areas[]\"]:checked')
    			.each(function(i2,v2){
		  		$('#search_shop').append(
		  			$('<input>')
		  				.attr('type','hidden')
		  				.attr('name','areas[]')
		  				.attr('value',$(v2).val())
		  		);			
    		});
    	}
    });


  	return true;
  });
});
})(jQuery);
</script>

<?php }else{ ?>
<?php get_template_part('template-parts/search-map-top'); ?>
<script src="/js/jquery.rwdImageMaps.min.js"></script>
<script type="text/javascript">
(function( $ ) {
	$(function(){
		$('img[usemap]').rwdImageMaps();
	});
})(jQuery);
</script>
<?php } ?>

<?php get_footer(); ?>
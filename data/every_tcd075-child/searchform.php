<?php $options = get_design_plus_option(); ?>
<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div>
		<label class="screen-reader-text" for="s"><?php echo esc_attr_x( 'Search &hellip;', 'placeholder' ) ?></label>
		<input type="text" value="<?php echo esc_attr( $_GET['s']?:'' ); ?>" name="s" id="s" placeholder="<?php echo esc_html($options['header_search_label']?:''); ?>" required >
		<div class="submit_button"><input type="submit" id="searchsubmit" value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>"></div>
	</div>
	<div id="s_radio_ope">
		<input id="radio_s_and" type="radio" name="s_ope" value="1" <?php if( ($_GET['s_ope']?:'1') == 1 ){ ?>checked="checked"<?php } ?>><label for="radio_s_and">すべて</label>
		<input id="radio_s_or" type="radio" name="s_ope" value="2" <?php if( ($_GET['s_ope']?:'') == 2 ){ ?>checked="checked"<?php } ?>><label for="radio_s_or">いずれかのキーワードを含む</label>
	</div>
</form>
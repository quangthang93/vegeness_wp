<?php $options = get_design_plus_option(); ?>
<!DOCTYPE html>
<html class="pc" <?php language_attributes(); ?>>
<?php if($options['use_ogp']) { ?>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<?php } else { ?>
<head>
	<script data-ad-client="ca-pub-5303046129997885" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<?php }; ?>
<meta charset="<?php bloginfo('charset'); ?>">
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
<meta name="viewport" content="width=device-width">
<meta name="format-detection" content="telephone=no">
<title><?php wp_title('|', true, 'right'); ?></title>
<meta name="description" content="<?php seo_description(); ?>">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<?php
     if ( $options['favicon'] ) {
       $favicon_image = wp_get_attachment_image_src( $options['favicon'], 'full');
       if(!empty($favicon_image)) {
?>
<link rel="shortcut icon" href="<?php echo esc_url($favicon_image[0]); ?>">
<?php }; }; ?>
<?php wp_enqueue_style('style', get_stylesheet_uri(), false, version_num(), 'all'); wp_enqueue_script( 'jquery' ); if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145780659-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145780659-2');
</script>
</head>
<body id="body" <?php body_class(); ?>>

<?php
     if ($options['show_load_screen'] == 'type2') {
       if(is_front_page() && !get_tcd_membership_memberpage_type()){
         load_icon();
       }
     } elseif ($options['show_load_screen'] == 'type3') {
       if(((is_front_page() || is_home()) && !get_tcd_membership_memberpage_type()) || is_post_type_archive('work') ){
         load_icon();
       }
     };
?>

<div id="container">

 <header id="header"<?php if (!has_nav_menu('global-menu')) { echo ' class="no_menu"'; }; ?>>

  <div id="header_inner">

   <div id="header_logo">
    <?php header_logo(); ?>
    <?php if ($options['header_logo_show_desc'] == 'type2' && is_front_page() && !get_tcd_membership_memberpage_type()) { ?>
    <h3 class="desc"><?php echo esc_html(get_bloginfo('description')); ?></h3>
    <?php } elseif ($options['header_logo_show_desc'] == 'type3') { ?>
    <h3 class="desc"><?php echo esc_html(get_bloginfo('description')); ?></h3>
    <?php }; ?>
   </div>

   <?php
        // ログインボタン等 ------------------------------
        if ($options['show_header_search'] || $options['show_header_login'] || (tcd_membership_users_can_register() && $options['show_header_register'])) {
   ?>
   <div id="header_button_area" class="clearfix">
    <?php
         if( current_user_can( 'read' ) ) {
           $user = wp_get_current_user();
           if(!empty($user->display_name)){
             $user_name = $user->display_name;
           } else {
             $user_name = $user->user_nicename;
           }
    ?>
    <p id="header_user_name"><?php printf(__('<span class="user_name">%s</span><span class="etc"></span>', 'tcd-w'),esc_html($user_name)); ?></p>
    <?php if ($options['show_header_login']) { ?><a id="header_logout" href="<?php echo get_tcd_membership_memberpage_url('mypage'); ?>"><?php _e( 'MY PAGE', 'tcd-w' ); ?></a><?php }; ?>
    <?php } else { ?>
    <?php if ($options['show_header_login']) { ?><a id="header_login" href="<?php echo get_tcd_membership_memberpage_url('login'); ?>"><?php echo esc_html($options['header_login_label']); ?></a><?php }; ?>
    <?php if (tcd_membership_users_can_register() && $options['show_header_register']) { ?><a id="header_register" href="<?php echo get_tcd_membership_memberpage_url('ragistration'); ?>"><?php echo esc_html($options['header_register_label']); ?></a><?php }; ?>
    <?php }; ?>
    <?php if ($options['show_header_search']) { ?><a id="header_search_button" href="#">SEARCH</a><?php }; ?>
   </div>
   <?php }; ?>

  </div>

  <?php if (has_nav_menu('global-menu')) { ?>
  <a href="#" id="menu_button"><span><?php _e('menu', 'tcd-w'); ?></span></a>
  <nav id="global_menu">
   <?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'theme_location' => 'global-menu' , 'container' => '' ) ); ?>
  </nav>
  <?php }; ?>

  <?php if ($options['show_header_search']) { ?>
  <div id="header_search">
   <form method="get" action="<?php echo esc_url(home_url('/')); ?>">
    <div class="search_input">
      <input type="text" value="" name="s" placeholder="<?php echo esc_html($options['header_search_label']); ?>" required />
      <div id="sh_radio_ope">
        <input id="radio_sh_and" type="radio" name="s_ope" value="1" checked="checked"><label for="radio_sh_and">すべて</label>
        <input id="radio_sh_or" type="radio" name="s_ope" value="2"><label for="radio_sh_or">いずれかのキーワードを含む</label>
      </div>
    </div>
    <div class="search_button"><label for="header_search_submit_button"><?php _e('Search','tcd-w'); ?></label><input id="header_search_submit_button" type="submit" value="<?php _e('Search','tcd-w'); ?>" /></div>
   </form>
  </div>
  <?php }; ?>

  <?php get_template_part( 'template-parts/megamenu' ); ?>

  <?php
       // 総レシピ数 ----------------------------
       if(is_front_page() && !get_tcd_membership_memberpage_type() && $options['show_index_total_recipe']) {
         $args = array( 'post_type' => 'recipe', 'posts_per_page' => 1);
         $total_recipe_query = new wp_query($args);
         $total_post = $total_recipe_query->found_posts;
  ?>
  <div id="index_total_recipe" style="background:<?php echo esc_html($options['index_total_recipe_color']); ?>;">
   <p><?php echo esc_html($options['index_total_recipe_headline']); ?><br /><?php if($total_post) { echo number_format($total_post) . esc_html($options['index_total_recipe_label']); }; ?></p>
  </div>
  <?php }; ?>

 </header>

 <?php if(is_front_page() && !get_tcd_membership_memberpage_type()) { ?>
 <div id="index_header_content_wrap">
  <div id="index_header_content">
  <?php
       // 画像スライダー ***********************************************************************************************************************
       if($options['index_header_content_type'] == 'type1') {
         $index_slider_mobile_content_type = $options['index_slider_mobile_content_type'];
  ?>
  <div id="index_slider_wrap"<?php if($index_slider_mobile_content_type != 'type1') { echo ' class="use_mobile_caption"'; }; ?>>
   <div id="index_slider" class="animation_<?php echo esc_attr($options['index_slider_animation_type']); ?>">
    <?php
         // mobile caption
         if($index_slider_mobile_content_type != 'type1') {
    ?>
    <div class="caption mobile">
     <div class="caption_inner">
      <h2 class="catch rich_font_<?php echo esc_attr($options['index_slider_mobile_catch_font_type']); ?>"><span><?php echo nl2br(esc_html($options['index_slider_mobile_catch'])); ?></span></h2>
     </div>
    </div>
    <?php }; ?>
    <?php
         // item loop start
         for($i=1; $i<= 3; $i++):
           $image_id = $options['index_slider_image'.$i];
           if(!empty($image_id)) {
             // image
             $image = wp_get_attachment_image_src($image_id, 'full');
             $image_url = $image[0];
    ?>
    <div class="item item<?php echo $i; if($i == 1){ echo ' first_item'; }; ?>">
     <?php
          // caption
          $num = 1;
          $catch = $options['index_slider_catch'.$i];
          $desc = $options['index_slider_desc'.$i];
          $font_type = $options['index_slider_catch_font_type'.$i];
          $show_button = $options['index_slider_show_button'.$i];
          $url = $options['index_slider_url'.$i];
          $target = $options['index_slider_target'.$i];
          $button_label = $options['index_slider_button_label'.$i];
          if($catch || $desc || $show_button){
     ?>
     <div class="caption direction_<?php echo esc_attr($options['index_slider_content_direction'.$i]); ?> <?php if($index_slider_mobile_content_type != 'type1') { echo 'pc'; }; ?>">
      <div class="caption_inner">
       <?php if($catch) { ?>
       <h2 class="catch rich_font_<?php echo esc_attr($font_type); ?> animate<?php echo $num; $num++; ?>"><span><?php echo nl2br(esc_html($catch)); ?></span></h2>
       <?php }; ?>
       <?php if($desc) { ?>
       <p class="desc animate<?php echo $num; $num++; ?>"><span><?php echo nl2br(esc_html($desc)); ?></span></p>
       <?php }; ?>
       <?php if( $show_button ) { ?>
       <a class="button animate<?php echo $num; $num++; ?>" href="<?php echo esc_url($url); ?>"<?php if($target == 1) { echo ' target="_blank"'; }; ?>><span><?php echo esc_html($button_label); ?></span></a>
       <?php }; ?>
      </div>
     </div><!-- END .caption -->
     <?php
          };
          // overlay
          if($options['index_slider_use_overlay'.$i] || $options['index_slider_use_overlay_mobile'.$i]) {
     ?>
     <div class="overlay"></div>
     <?php
          };
          // image
          $image_mobile_id = $options['index_slider_image_mobile'.$i];
     ?>
     <div class="slice_image_list clearfix <?php if(!empty($image_mobile_id)) { echo 'pc'; }; ?>">
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url); ?>);"></div></div></div>
     </div>
     <?php
          // image mobile
          if(!empty($image_mobile_id)) {
             $image_mobile = wp_get_attachment_image_src($image_mobile_id, 'full');
             $image_url_mobile = $image_mobile[0];
     ?>
     <div class="slice_image_list clearfix mobile">
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url_mobile); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url_mobile); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url_mobile); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url_mobile); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url_mobile); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url_mobile); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url_mobile); ?>);"></div></div></div>
      <div class="slice_image"><div class="image_wrap"><div class="image" style="background-image:url(<?php echo esc_attr($image_url_mobile); ?>);"></div></div></div>
     </div>
     <?php }; ?>
    </div><!-- END .item -->
    <?php
           };// END if image
         endfor; // END item loop
    ?>
   </div><!-- END #index_slider -->
  </div><!-- END #index_slider_wrap -->
  <?php
       // 動画, YouTube ***********************************************************************************************************************
       } elseif($options['index_header_content_type'] == 'type2' || $options['index_header_content_type'] == 'type3') {
  ?>
  <div id="index_slider_wrap">
   <div id="index_slider" class="video">

    <?php
         // pc catchphrase
         $num = 1;
         $catch = $options['index_movie_catch'];
         $desc = $options['index_movie_desc'];
         $font_type = $options['index_movie_catch_font_type'];
         $show_button = $options['index_movie_show_button'];
         $url = $options['index_movie_url'];
         $target = $options['index_movie_target'];
         $button_label = $options['index_movie_button_label'];
         if($catch || $desc || $show_button){
    ?>
    <div class="caption <?php if($options['index_movie_mobile_content_type'] == 'type2') { echo 'pc'; }; ?>">
     <div class="caption_inner">
      <?php if($catch) { ?>
      <h2 class="catch rich_font_<?php echo esc_attr($font_type); ?> animate<?php echo $num; $num++; ?>"><span><?php echo nl2br(esc_html($catch)); ?></span></h2>
      <?php }; ?>
      <?php if($desc) { ?>
      <p class="desc animate<?php echo $num; $num++; ?>"><span><?php echo nl2br(esc_html($desc)); ?></span></p>
      <?php }; ?>
      <?php if($show_button) { ?>
      <a class="button animate<?php echo $num; $num++; ?>" href="<?php echo esc_url($url); ?>"<?php if($target == 1) { echo ' target="_blank"'; }; ?>><span><?php echo esc_html($button_label); ?></span></a>
      <?php }; ?>
     </div>
    </div><!-- END .caption -->
    <?php }; ?>

    <?php
         // mobile catchphrase
         if($options['index_movie_mobile_content_type'] == 'type2') {
           $catch = $options['index_movie_catch_mobile'];
           $font_type = $options['index_movie_catch_font_type_mobile'];
           if($catch){
    ?>
    <div class="caption mobile">
     <div class="caption_inner">
      <?php if($catch) { ?>
      <h2 class="catch rich_font_<?php echo esc_attr($font_type); ?> animate1"><span><?php echo nl2br(esc_html($catch)); ?></span></h2>
      <?php }; ?>
     </div>
    </div><!-- END .caption -->
    <?php }; }; ?>

    <?php
         // overlay
         if($options['index_movie_use_overlay']) {
    ?>
    <div class="overlay"></div>
    <?php }; ?>

    <?php
         // 動画 ---------------------------------------------------------
         if($options['index_header_content_type'] == 'type2') {
           $video = $options['index_video'];
           if(!empty($video)) {
             $video_image_id = $options['index_movie_image'];
             if($video_image_id) {
               $video_image = wp_get_attachment_image_src($video_image_id, 'full');
             }
    ?>
    <div id="index_video">
     <?php if (auto_play_movie()) { ?>
     <video src="<?php echo esc_url(wp_get_attachment_url($options['index_video'])); ?>" id="index_video_mp4" playsinline autoplay loop muted></video>
     <?php
          } else {
            if($video_image_id) {
     ?>
     <div id="video_poster" style="background:url(<?php echo esc_attr($video_image[0]); ?>) no-repeat center center; background-size:cover;"></div>
     <?php }; }; ?>
    </div>
    <?php
           };
           // YouTube ---------------------------------------------------------
         } elseif($options['index_header_content_type'] == 'type3') {
           $youtube_url = $options['index_youtube_url'];
           if(!empty($youtube_url)) {
             $video_image_id = $options['index_movie_image'];
             if($video_image_id) {
               $video_image = wp_get_attachment_image_src($video_image_id, 'full');
             } 
             if(auto_play_movie()){
    ?>
    <div id="index_video">
     <div id="youtube_video_wrap">
      <div id="youtube_video_wrap_inner">
       <div id="youtube_video_player"></div>
      </div>
     </div>
    </div>
    <?php
         } else {
         if($video_image_id) {
    ?>
    <div id="video_poster" style="background:url(<?php echo esc_attr($video_image[0]); ?>) no-repeat center center; background-size:cover;"></div>
    <?php }; }; ?>
    <?php
           };
         };
    ?>
   </div><!-- END #index_slider -->
  </div><!-- END #index_slider_wrap -->
  <?php }; // END header content type ?>

 </div><!-- END #index_header_content -->

<div class="h_btns_w">

<div class="h_search_w">
   <form id="h_search" method="get" action="<?php echo esc_url(home_url('/')); ?>">
    <div class="search_input">
      <input type="text" value="" name="s" placeholder="<?php echo esc_html($options['header_search_label']); ?>" required />
      <div id="sh_radio_ope2">
        <input id="radio_sh_and2" type="radio" name="s_ope" value="1" checked="checked"><label for="radio_sh_and2">すべて</label>
        <input id="radio_sh_or2" type="radio" name="s_ope" value="2"><label for="radio_sh_or2">いずれかのキーワードを含む</label>
      </div>
    </div>
    <div class="search_button"><label for="h_search_submit_button2"><?php _e('Search','tcd-w'); ?></label><input id="h_search_submit_button2" type="submit" value="<?php _e('Search','tcd-w'); ?>" /></div>
   </form>
</div>

<div class="h_sp h_btns">
  <div><a href="/recipe/">カテゴリ一覧</a></div>
  <div><a href="/category_rank/">ランキング</a></div>
  <div><a href="/?map=y">お店を探す</a></div>
</div>

<?php
  $menus = nk_type_menus();
  $ingredients = nk_type_ingredients();
?>
<div class="h_btns2">
  <input id="h_btns2_menu1" class="h_btns2_menu_r" type="radio" name="h_btns2" checked="checked">
  <input id="h_btns2_menu2" class="h_btns2_menu_r" type="radio" name="h_btns2">
  <div class="h_btns2_tab">
  <div class="h_btns2_menu1_s"><label for="h_btns2_menu1">材料から探す</label></div>
  <div class="h_btns2_menu2_s"><label for="h_btns2_menu2">メニューから探す</label></div>
  </div>
  <div class="h_btns2_menu1 h_btns2_menu">
    <ul>
      <?php foreach ($ingredients as $k => $v) { ?>
        <?php if ( ($v['skip']??false) === true ) { continue; } ?>
        <?php if ($v['n']!='') { ?>
        <li><a href="/?s_type=ingredients&type=<?php echo $k ?>"><?php echo $v['n'] ?></a></li>
        <?php }else{ ?>
        <li></li>
        <?php } ?>
      <?php } ?>
    </ul>
  </div>
  <div class="h_btns2_menu2 h_btns2_menu">
    <ul>
      <?php foreach ($menus as $k => $v) { ?>
        <?php if ( ($v['skip']??false) === true ) { continue; } ?>
        <?php if ($v['n']!='') { ?>
        <li><a href="/?s_type=menu&type=<?php echo $k ?>"><?php echo $v['n'] ?></a></li>
        <?php }else{ ?>
        <li></li>
        <?php } ?>
      <?php } ?>
    </ul>
  </div>
</div>
</div>

 <?php
      // レシピスライダー ---------------------------------------------------------------
      if(false&&$options['show_index_recipe_slider']) {
        $show_category = $options['index_recipe_slider_show_category'];
        $show_post_view = $options['index_recipe_slider_show_count'];
        $show_premium_icon = $options['index_recipe_slider_show_premium_icon'];
        $post_type = $options['index_recipe_slider_post_type'];
        $post_num = $options['index_recipe_slider_num'];
        if($post_type == 'all') {
          $args = array( 'post_type' => 'recipe', 'posts_per_page' => $post_num);
        } else {
          $args = array( 'post_type' => 'recipe', 'meta_key' => $post_type, 'meta_value' => '1', 'posts_per_page' => $post_num );
        }
        $recipe_slider_query = new wp_query($args);
        if($recipe_slider_query->have_posts()):
 ?>
 <div id="index_recipe_slider_wrap">
  <div id="index_recipe_slider" class="owl-carousel">
   <?php
        while($recipe_slider_query->have_posts()): $recipe_slider_query->the_post();
          $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
          $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
          if(has_post_thumbnail()) {
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
          } elseif($options['no_image2']) {
            $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
          } else {
            $image = array();
            $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
          }
          $recipe_category = wp_get_post_terms( $post->ID, 'recipe_category' ,array( 'orderby' => 'term_order' ));
          if ( $recipe_category && ! is_wp_error($recipe_category) ) {
            foreach ( $recipe_category as $cat ) :
              if($cat->parent == 0) {
                $cat_name = $cat->name;
                $cat_id = $cat->term_id;
                $term_meta = get_option( 'taxonomy_' . $cat_id, array() );
                $category_color = "#009fe1";
                if (!empty($term_meta['main_color'])){
                  $category_color = $term_meta['main_color'];
                }
                break;
              }
            endforeach;
          };
   ?>
   <article class="item<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>">
    <a class="link animate_background" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
     <div class="image_wrap">
      <?php if($recipe_category && $show_category) { ?><p class="category" style="background:<?php echo esc_attr($category_color); ?>;"><?php echo esc_html($cat_name); ?></p><?php }; ?>
      <?php if($premium_recipe && $show_premium_icon) { ?><div class="premium_icon"></div><?php }; ?>
      <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
     </div>
    </a>
    <div class="title_area">
     <h3 class="title"><a href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>"><span><?php the_title(); ?></span></a></h3>
     <?php if ($show_post_view){ ?><p class="post_meta"><?php if($recipe_type != 'type2') { _e('Hits:', 'tcd-w'); } else { _e('Views:', 'tcd-w'); }; ?><?php the_post_views(); ?></p><?php }; ?>
    </div>
   </article>
   <?php endwhile; ?>
  </div><!-- END #index_recipe_slider -->
 </div><!-- END #index_recipe_slider_wrap -->
 <?php endif; ?>
 <?php }; ?>

 <?php
      // 会員登録メッセージ ---------------------------------------------------------------
      if( ($options['show_index_welcome'] == 'type1') || (!is_user_logged_in() && $options['show_index_welcome'] == 'type2') ) {
          $catch = $options['index_welcome_catch'];
          $desc = $options['index_welcome_desc'];
          $font_type = $options['index_welcome_catch_font_type'];
          $show_button = $options['index_welcome_show_button'];
          $button_label = $options['index_welcome_button_label'];
 ?>
 <div id="index_welcome_message">
  <?php if($catch) { ?>
  <h2 class="catch rich_font_<?php echo esc_attr($font_type); ?>"><span><?php echo nl2br(esc_html($catch)); ?></span></h2>
  <?php }; ?>
  <?php if($desc) { ?>
  <p class="desc"><span><?php echo nl2br(esc_html($desc)); ?></span></p>
  <?php }; ?>
  <?php if( $show_button ) { ?>
  <a class="button register_link" href="#"><span><?php echo esc_html($button_label); ?></span></a>
  <?php }; ?>
 </div>
 <?php }; ?>

 </div><!-- END #index_header_content_wrap -->
 <?php }; // END if is front page ?>



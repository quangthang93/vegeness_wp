<?php
/*
 * 実績の設定
 */


function nk_shop_meta($id){
  $shop_meta = get_post_meta( $id, 'shop_meta', true );
  if (!is_array($shop_meta)) {
    $shop_meta = [];
  }
  return $shop_meta;
}

function nk_shop_icon_meta($term_id,$is_img = false){
  $term_meta = get_term_meta($term_id, 'shop_icon',true);
  if (!is_array($term_meta)) {
    $term_meta = [];
  }
  if ($is_img&&isset($term_meta['image'])) {
    $image = wp_get_attachment_image_src( $term_meta['image'], 'full' );
    $term_meta['img_src'] = $image[0];
  }
  return $term_meta;
}

// Add default values
add_filter( 'before_getting_design_plus_option', 'add_shop_dp_default_options',11 );


//  Add label of shop tab
add_action( 'tcd_tab_labels', 'add_shop_tab_label',11 );


// Add HTML of shop tab
add_action( 'tcd_tab_panel', 'add_shop_tab_panel' ,11);


// Register sanitize function
add_filter( 'theme_options_validate', 'add_shop_theme_options_validate',11 );


// タブの名前
function add_shop_tab_label( $tab_labels ) {
  $options = get_design_plus_option();
  $tab_label = 'お店';
  $tab_labels['shop'] = $tab_label;
	return $tab_labels;
}


// 初期値
function add_shop_dp_default_options( $dp_default_options ) {

	$dp_default_options['shop_image'] = false;

	return $dp_default_options;

}


// 入力欄の出力
function add_shop_tab_panel( $options ) {

  global $dp_default_options, $pagenation_type_options;

?>

<div id="tab-content-shop" class="tab-content">

   <div class="theme_option_field cf theme_option_field_ac open active">
    <h3 class="theme_option_headline">エリア設定</h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Background image', 'tcd-w'); ?></h4>
     <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '810', '455'); ?></p>
     <div class="image_box cf">
      <div class="cf cf_media_field hide-if-no-js shop_bg_image">
       <input type="hidden" value="<?php echo esc_attr( $options['shop_image'] ); ?>" id="shop_image" name="dp_options[shop_image]" class="cf_media_id">
       <div class="preview_field"><?php if($options['shop_image']){ echo wp_get_attachment_image($options['shop_image'], 'medium'); }; ?></div>
       <div class="buttton_area">
        <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
        <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['shop_image']){ echo 'hidden'; }; ?>">
       </div>
      </div>
     </div>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


</div><!-- END .tab-content -->

<?php
} // END add_shop_tab_panel()


// バリデーション
function add_shop_theme_options_validate( $input ) {

  global $dp_default_options, $pagenation_type_options, $shop_list_animation_type_options;

  $input['shop_image'] = wp_filter_nohtml_kses( $input['shop_image'] );

	return $input;

};


add_action( 'add_meta_boxes', function(){
  add_meta_box( 'shop_meta','お店情報', function($post){
    $name = 'shop';
    wp_nonce_field( 'nk_'.$name.'_meta_box', 'nk_create_form_'.$name.'_nonce' );  

    $shop_meta = nk_shop_meta($post->ID);
?>
<div>
  <div>お店名</div>
  <input type="text" name="shop_meta[name]" value="<?php echo esc_attr($shop_meta['name']??'') ?>" style="width: 100%;">
  <div>タイトル</div>
  <input type="text" name="shop_meta[title]" value="<?php echo esc_attr($shop_meta['title']??'') ?>" style="width: 100%;">
  <div>説明</div>
  <textarea name="shop_meta[desc]" style="width: 100%;" rows="5"><?php echo esc_textarea($shop_meta['desc']??'') ?></textarea>
  <div>郵便番号</div>
  <input type="text" name="shop_meta[zip]" value="<?php echo esc_attr($shop_meta['zip']??'') ?>" style="width: 100%;">
  <div>住所</div>
  <input type="text" name="shop_meta[addr]" value="<?php echo esc_attr($shop_meta['addr']??'') ?>" style="width: 100%;">
  <div>アクセス</div>
  <input type="text" name="shop_meta[acc]" value="<?php echo esc_attr($shop_meta['acc']??'') ?>" style="width: 100%;">
  <div>map(iframe)</div>
  <input type="text" name="shop_meta[map]" value="<?php echo esc_attr($shop_meta['map']??'') ?>" style="width: 100%;">
  <div>電話番号</div>
  <input type="text" name="shop_meta[tel]" value="<?php echo esc_attr($shop_meta['tel']??'') ?>" style="width: 100%;">
  <div>HP</div>
  <input type="text" name="shop_meta[hp]" value="<?php echo esc_attr($shop_meta['hp']??'') ?>" style="width: 100%;">
  <div>平均予算</div>
  <input type="text" name="shop_meta[price]" value="<?php echo esc_attr($shop_meta['price']??'') ?>" style="width: 100%;">
  <div>平均予算(ランチ)</div>
  <input type="text" name="shop_meta[price_d]" value="<?php echo esc_attr($shop_meta['price_d']??'') ?>" style="width: 100%;">
  <div>平均予算(ディナー)</div>
  <input type="text" name="shop_meta[price_n]" value="<?php echo esc_attr($shop_meta['price_n']??'') ?>" style="width: 100%;">
  <div>営業時間</div>
  <input type="text" name="shop_meta[business_h]" value="<?php echo esc_attr($shop_meta['business_h']??'') ?>" style="width: 100%;">
  <div>営業時間(ランチ)</div>
  <input type="text" name="shop_meta[business_hd]" value="<?php echo esc_attr($shop_meta['business_hd']??'') ?>" style="width: 100%;">
  <div>営業時間(ディナー)</div>
  <input type="text" name="shop_meta[business_hn]" value="<?php echo esc_attr($shop_meta['business_hn']??'') ?>" style="width: 100%;">
  <div>定休日</div>
  <input type="text" name="shop_meta[holiday]" value="<?php echo esc_attr($shop_meta['holiday']??'') ?>" style="width: 100%;">
  <div>座席数</div>
  <input type="text" name="shop_meta[seats]" value="<?php echo esc_attr($shop_meta['seats']??'') ?>" style="width: 100%;">
  <div>支払い方法</div>
  <input type="text" name="shop_meta[credit]" value="<?php echo esc_attr($shop_meta['credit']??'') ?>" style="width: 100%;">
  <div>駐車場</div>
  <input type="text" name="shop_meta[parking]" value="<?php echo esc_attr($shop_meta['parking']??'') ?>" style="width: 100%;">
</div>
<div style="margin-top: 10px">
  <div>画像一覧設定</div>
  <div>画像一覧</div>
  <div><button id="swipe_btn" type="button">画像追加</button></div>
  <div id="swipe_imgs">
    <?php if (isset($shop_meta['swipe_imgs']) && is_array($shop_meta['swipe_imgs'])) { ?>
    <?php foreach ($shop_meta['swipe_imgs'] as $k => $v) { ?>
      <div class="swipe_selects">
        <button class="swipe_del" type="button">✖︎</button>
        <input type="hidden" name="shop_meta[swipe_imgs][]" value="<?php echo $v ?>">
        <?php echo wp_get_attachment_image($v); ?>
      </div>
    <?php } ?>
    <?php } ?>
  </div>
  <div>スライド画像枚数</div>
  <input type="number" name="shop_meta[swipe_num]" value="<?php echo esc_attr($shop_meta['swipe_num']??'') ?>" style="width: 100%;">
</div>
<style type="text/css">
#swipe_imgs {
    display: flex;
    flex-wrap: wrap;
}

.swipe_selects {
      position: relative;
    width: 20%;
    padding: 5px;
    box-sizing: border-box;
}
button.swipe_del {
    position: absolute;
    right: 10px;
    top: 10px;
    font-size: 20px;
}
</style>
<script src="<?php echo (get_stylesheet_directory_uri().'/assets/js/sortable/Sortable.min.js') ?>"></script>
<script>
 jQuery(document).ready(function($){
    var el = document.getElementById('swipe_imgs');
    var sortable = Sortable.create(el);

    $('.swipe_del').on('click',function(){
      $(this).parent().remove();
    });

    // Instantiates the variable that holds the media library frame.
    var meta_image_frame;

    // Runs when the image button is clicked. You need to insert ID of your button
    $('#swipe_btn').on('click',function(e){

        // Prevents the default action from occuring.
        e.preventDefault();

        // If the frame already exists, re-open it.
        if ( meta_image_frame ) {
            meta_image_frame.open();
            return;
        }

        // Sets up the media library frame
        meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
            title: '画像を追加',
            multiple: 'add',
            button: { text:  '画像を追加' },
            library: { type: 'image' }
        });

        // Runs when an image is selected. You need to insert ID of input field
        meta_image_frame.on('select', function(){
            var selection = meta_image_frame.state().get('selection');
            var size = 'thumbnail';
            selection.map(function(attachment) {
              attachment = attachment.toJSON();
              $('#swipe_imgs').append(
                $('<div>')
                .addClass('swipe_selects')
                .append(
                  $('<img>')
                  .attr('src',attachment.sizes[size].url)
                )
                .append(
                  $('<input>')
                  .attr('type','hidden')
                  .attr('name','shop_meta[swipe_imgs][]')
                  .val(attachment.id)
                )
              );
            });

        });

        // Opens the media library frame.
        meta_image_frame.open();
    });
});
</script>

<?php
  }, 'shop', 'advanced' );
});

add_action('save_post', function ($post_id){

  $name = 'shop';

  if ( ! isset( $_POST['nk_create_form_'.$name.'_nonce'] ) ) {
    return;
  }

  // nonceが正しいかどうか検証
  if ( ! wp_verify_nonce( $_POST['nk_create_form_'.$name.'_nonce'], 'nk_'.$name.'_meta_box' ) ) {
    return;
  }

  // 自動保存の場合はなにもしない
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    return;
  }

  // ユーザー権限の確認
  if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

    if ( ! current_user_can( 'edit_page', $post_id ) ) {
      return;
    }

  } else {

    if ( ! current_user_can( 'edit_post', $post_id ) ) {
      return;
    }
  }

  // データがセットされているか確認する
  if ( ! ( isset( $_POST['shop_meta'] ) && is_array($_POST['shop_meta']) ) ) {
    return;
  }

  $shop_meta = nk_shop_meta($post_id);

  $p_shop_meta = $_POST['shop_meta']??[];

  $texts = [
    'name',
    'title',
    'zip',
    'addr',
    'acc',
    'tel',
    'hp',
    'price',
    'price_d',
    'price_n',
    'business_h',
    'business_hd',
    'business_hn',
    'holiday',
    'seats',
    'credit',
    'parking'
  ];
  foreach ($texts as $k => $v) {
    $shop_meta[$v] = sanitize_text_field( $p_shop_meta[$v]??'' );
  }

  $texts = [
    'swipe_num',
  ];
  foreach ($texts as $k => $v) {
    $lv = sanitize_text_field( $p_shop_meta[$v]??'');
    $c = filter_var($lv, FILTER_VALIDATE_INT);
    if ($c === false) {
      $lv = 1;
    }
    $shop_meta[$v] = $lv;
  }

  $swipe_ids = [];
  if (isset($p_shop_meta['swipe_imgs'])&&is_array($p_shop_meta['swipe_imgs'])) {
    foreach ($p_shop_meta['swipe_imgs'] as $k => $v) {
      $c = filter_var($v, FILTER_VALIDATE_INT);
      if ($c !== false) {
        $swipe_ids[] = $v;
      }
    }
  }
  $shop_meta['swipe_imgs'] = $swipe_ids;

  $shop_meta['map'] = wp_kses($p_shop_meta['map']??'',[ 'iframe'=> [
      'src'             => true,
      'height'          => true,
      'width'           => true,
      'frameborder'     => true,
      'allowfullscreen' => true,
      'aria-hidden' => true,
      'tabindex' => true,
      'style' => true,
      'class' => true,
      'id' => true
  ]]);

  $tareas = ['desc'];
  foreach ($tareas as $k => $v) {
    $shop_meta[$v] = sanitize_textarea_field( $p_shop_meta[$v]??'' );
  }

  update_post_meta($post_id, 'shop_meta', $shop_meta);
});


// カテゴリー編集用入力欄を出力 -------------------------------------------------------
function nk_shop_icon_edit_extra_fields( $term ) {

  $name = 'shop_icon';
  wp_nonce_field( 'nk_'.$name.'_meta_box', 'nk_create_form_'.$name.'_nonce' ); 


  $term_meta = nk_shop_icon_meta($term->term_id);

  $term_meta = array_merge( [
    'image' => null,
    'desc' => '',
  ], $term_meta );
?>
<tr class="form-field">
  <th colspan="2">

<div class="custom_category_meta<?php if(0 < $term->parent){ echo ' has_parent'; } ?>">
 <h3 class="ccm_headline"><?php _e( 'Additional data', 'tcd-w' ); ?></h3>

 <div class="ccm_content clearfix hide_if_child">
  <h4 class="headline"><?php _e( 'Image', 'tcd-w' ); ?></h4>
  <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '810', '455'); ?></p>
  <div class="input_field">
   <div class="image_box cf">
    <div class="cf cf_media_field hide-if-no-js image">
     <input type="hidden" value="<?php if ( $term_meta['image'] ) echo esc_attr( $term_meta['image'] ); ?>" id="image" name="term_meta[image]" class="cf_media_id">
     <div class="preview_field"><?php if ( $term_meta['image'] ) echo wp_get_attachment_image( $term_meta['image'], 'medium'); ?></div>
     <div class="button_area">
      <input type="button" value="<?php _e( 'Select Image', 'tcd-w' ); ?>" class="cfmf-select-img button">
      <input type="button" value="<?php _e( 'Remove Image', 'tcd-w' ); ?>" class="cfmf-delete-img button <?php if ( ! $term_meta['image'] ) echo 'hidden'; ?>">
     </div>
    </div>
   </div>
  </div><!-- END input_field -->
 </div><!-- END ccm_content -->

 <div class="ccm_content clearfix">
  <h4 class="headline"><?php _e( 'Description', 'tcd-w' ); ?></h4>
  <div class="input_field">
   <p><textarea class="large-text" cols="50" rows="3" name="term_meta[desc]"><?php echo esc_textarea( $term_meta['desc'] ); ?></textarea></p>
  </div><!-- END input_field -->
 </div><!-- END ccm_content -->


</div><!-- END .custom_category_meta -->

 </th>
</tr><!-- END .form-field -->
<?php
}
add_action( 'shop_icon_edit_form_fields', 'nk_shop_icon_edit_extra_fields' );


// データを保存 -------------------------------------------------------
function nk_shop_icon_save_extra_fileds( $term_id ) {

  $name = 'shop_icon';

  if ( ! isset( $_POST['nk_create_form_'.$name.'_nonce'] ) ) {
    return;
  }

  // nonceが正しいかどうか検証
  if ( ! wp_verify_nonce( $_POST['nk_create_form_'.$name.'_nonce'], 'nk_'.$name.'_meta_box' ) ) {
    return;
  }

  if ( ! current_user_can( 'manage_categories' ) ) {
    return;
  }

  if ( ! ( isset( $_POST['term_meta'] ) && is_array($_POST['term_meta']) ) ) {
    return;
  }

  $term_meta = nk_shop_icon_meta($term_id);
  $meta_keys = array(
    'image',
    'desc',
  );
  foreach ( $meta_keys as $key ) {
    $term_meta[$key] = wp_filter_nohtml_kses($_POST['term_meta'][$key]??'');
  }

  update_term_meta( $term_id, 'shop_icon' , $term_meta );

}
add_action( 'edited_shop_icon', 'nk_shop_icon_save_extra_fileds' );



?>
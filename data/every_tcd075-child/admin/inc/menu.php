<?php
add_filter( 'before_getting_design_plus_option', 'add_stype_menu_dp_default_options',11 );

//  Add label of stype_menu tab
add_action( 'tcd_tab_labels', 'add_stype_menu_tab_label',11 );


// Add HTML of stype_menu tab
add_action( 'tcd_tab_panel', 'add_stype_menu_tab_panel' ,11);


// Register sanitize function
add_filter( 'theme_options_validate', 'add_stype_menu_theme_options_validate',11 );

// 初期値
function add_stype_menu_dp_default_options( $dp_default_options ) {
	$menus = nk_type_menus_all();
	foreach ($menus as $k => $v) {
		foreach ($v['lists'] as $k2 => $v2) {
	  		$dp_default_options['stype_menu_s'.$k2.'_img'] = false;
	  		$dp_default_options['stype_menu_s'.$k2.'_desc'] = '';
		}
	}

	return $dp_default_options;
}

// タブの名前
function add_stype_menu_tab_label( $tab_labels ) {
  $options = get_design_plus_option();
  $tab_label = 'メニューから探す';
  $tab_labels['stype_menu'] = $tab_label;
	return $tab_labels;
}

function add_stype_menu_tab_panel( $options ) {

  global $dp_default_options, $pagenation_type_options;

  $menus = nk_type_menus_all();
?>

<div id="tab-content-shop" class="tab-content">

	<?php foreach ($menus as $k => $v) { ?>
	<?php foreach ($v['lists'] as $k2 => $v2) { ?>
   <div class="theme_option_field cf theme_option_field_ac open active">
    <h3 class="theme_option_headline"><?php echo $v2['n'] ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2">画像</h4>
     <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '500', '500'); ?></p>
     <div class="image_box cf">
      <div class="cf cf_media_field hide-if-no-js shop_bg_image">
       <input type="hidden" value="<?php echo esc_attr( $options['stype_menu_s'.$k2.'_img'] ); ?>" id="stype_menu_s<?php echo $k2 ?>_img" name="dp_options[stype_menu_s<?php echo $k2 ?>_img]" class="cf_media_id">
       <div class="preview_field"><?php if($options['stype_menu']['s'.$k2]['img']){ echo wp_get_attachment_image($options['stype_menu_s'.$k2.'_img'], 'medium'); }; ?></div>
       <div class="buttton_area">
        <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
        <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['stype_menu_s'.$k2.'_img']){ echo 'hidden'; }; ?>">
       </div>
      </div>
     </div>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2">説明</h4>
     	<textarea class="large-text" cols="50" rows="2" name="dp_options[stype_menu_s<?php echo $k2 ?>_desc]"><?php echo esc_textarea( $options['stype_menu_s'.$k2.'_desc'] ); ?></textarea>
     </div>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->
   <?php } ?>
   <?php } ?>

</div>
<?php }


// バリデーション
function add_stype_menu_theme_options_validate( $input ) {

  global $dp_default_options, $pagenation_type_options, $shop_list_animation_type_options;

  $menus = nk_type_menus_all();
  foreach ($menus as $k => $v) {
	foreach ($v['lists'] as $k2 => $v2) {
  		$input['stype_menu_s'.$k2.'_img'] = wp_filter_nohtml_kses( $input['stype_menu_s'.$k2.'_img'] );
  		$input['stype_menu_s'.$k2.'_desc'] = wp_filter_nohtml_kses( $input['stype_menu_s'.$k2.'_desc'] );
	}
  }

  return $input;

};
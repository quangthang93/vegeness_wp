<?php
add_filter( 'before_getting_design_plus_option', 'add_stype_ingredients_dp_default_options',11 );

//  Add label of stype_ingredients tab
add_action( 'tcd_tab_labels', 'add_stype_ingredients_tab_label',11 );


// Add HTML of stype_ingredients tab
add_action( 'tcd_tab_panel', 'add_stype_ingredients_tab_panel' ,11);


// Register sanitize function
add_filter( 'theme_options_validate', 'add_stype_ingredients_theme_options_validate',11 );

// 初期値
function add_stype_ingredients_dp_default_options( $dp_default_options ) {
	$ingredients = nk_type_ingredients();
	foreach ($ingredients as $k => $v) {
		if ( !isset($v['lists']) ) { continue; }
  		$dp_default_options['stype_ingredients_s'.$k.'_img'] = false;
  		$dp_default_options['stype_ingredients_s'.$k.'_desc'] = '';
	}

	return $dp_default_options;
}

// タブの名前
function add_stype_ingredients_tab_label( $tab_labels ) {
  $options = get_design_plus_option();
  $tab_label = '材料から探す';
  $tab_labels['stype_ingredients'] = $tab_label;
	return $tab_labels;
}

function add_stype_ingredients_tab_panel( $options ) {

  global $dp_default_options, $pagenation_type_options;

  $ingredients = nk_type_ingredients();
?>

<div id="tab-content-shop" class="tab-content">

	<?php foreach ($ingredients as $k => $v) { ?>
		<?php if ( !isset($v['lists']) ) { continue; } ?>
   <div class="theme_option_field cf theme_option_field_ac open active">
    <h3 class="theme_option_headline"><?php echo $v['n'] ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2">画像</h4>
     <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '500', '500'); ?></p>
     <div class="image_box cf">
      <div class="cf cf_media_field hide-if-no-js shop_bg_image">
       <input type="hidden" value="<?php echo esc_attr( $options['stype_ingredients_s'.$k.'_img'] ); ?>" id="stype_ingredients_s<?php echo $k ?>_img" name="dp_options[stype_ingredients_s<?php echo $k ?>_img]" class="cf_media_id">
       <div class="preview_field"><?php if($options['stype_ingredients']['s'.$k]['img']){ echo wp_get_attachment_image($options['stype_ingredients_s'.$k.'_img'], 'medium'); }; ?></div>
       <div class="buttton_area">
        <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
        <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['stype_ingredients_s'.$k.'_img']){ echo 'hidden'; }; ?>">
       </div>
      </div>
     </div>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2">説明</h4>
     	<textarea class="large-text" cols="50" rows="2" name="dp_options[stype_ingredients_s<?php echo $k ?>_desc]"><?php echo esc_textarea( $options['stype_ingredients_s'.$k.'_desc'] ); ?></textarea>
     </div>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->
   <?php } ?>

</div>
<?php }


// バリデーション
function add_stype_ingredients_theme_options_validate( $input ) {

  global $dp_default_options, $pagenation_type_options, $shop_list_animation_type_options;

  $ingredients = nk_type_ingredients();
  foreach ($ingredients as $k => $v) {
	if ( !isset($v['lists']) ) { continue; }
	$input['stype_ingredients_s'.$k.'_img'] = wp_filter_nohtml_kses( $input['stype_ingredients_s'.$k.'_img'] );
	$input['stype_ingredients_s'.$k.'_desc'] = wp_filter_nohtml_kses( $input['stype_ingredients_s'.$k.'_desc'] );
  }

  return $input;

};
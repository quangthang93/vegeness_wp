<?php //インデックス一覧
/**
 * Cocoon WordPress Theme
 * @author: yhira
 * @link: https://wp-cocoon.com/
 * @license: http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 */
if ( !defined( 'ABSPATH' ) ) exit;

$lists_post_ids = [];
$post_ids = [];
$lists = [
  ['えだまめ','枝豆'],
  ['かぼちゃ','南瓜'],
  ['きゅうり','胡瓜'],
  ['こまつな','小松菜'],
  ['ごぼう','牛蒡'],
  ['さつまいも','さつま芋','薩摩芋'],
  ['さといも','さと芋','里芋'],
  ['しいたけ','椎茸'],
  ['じゃがいも','じゃが芋'],
  ['しゅんぎく','春菊'],
  ['しょうが','生姜'],
  ['そらまめ','そら豆','空豆'],
  ['たまねぎ','玉ねぎ','玉葱'],
  ['だいこん','大根'],
  ['ちんげんさい','青梗菜','ちんげん菜'],
  ['とうがらし','唐辛子'],
  ['とうがん','冬瓜'],
  ['なす','茄子'],
  ['にんじん','人参'],
  ['ねぎ','葱'],
  ['はくさい','白菜'],
  ['ほうれんそう','ほうれん草'],
  ['みょうが','茗荷'],
  ['みずな','水菜'],
  ['むらさききゃべつ','紫きゃべつ'],
  ['むらさきたまねぎ','紫たまねぎ','紫玉葱'],
  ['やまいも','山芋'],
  ['れんこん','蓮根'],
  ['なのはな','菜の花'],
  ['みつば','三つ葉'],
  ['めきゃべつ','芽きゃべつ'],
  ['ながいも','長芋','長いも'],
  ['わさび','山葵'],
  ['ゆりね','百合根','ゆり根'],
  ['きぬさや','絹さや'],
  ['ひよこまめ','ひよこ豆'],
  ['たけのこ','筍','竹の子'],
  ['きのこ','木の子'],
  ['きくらげ','木耳'],
  ['みそ','味噌'],
  ['さとう','砂糖'],
  ['しょうゆ','醤油'],
  ['しお','塩'],
  ['こしょう','胡椒'],
  ['おりーぶおいる','おりーぶ油'],
  ['こうじ','麹'],
  ['まいたけ','舞茸'],
];




$_keyword = $_GET['s'];

$_keyword_array = [];
$_keyword  = preg_replace("/( |　)/", ",", $_keyword);
$_l_keyword_array = explode(",", $_keyword);


foreach ($lists as $k => &$v) {
  $l = [];
  foreach ($v as $k2 => $v2) {
    $l[] = $v2;
    $lv = mb_convert_kana($v2, "KVC");
    if ($v2 !== $lv) {
      $l[] = $lv;
    }
  }
  $v = $l;

}
unset($v);


foreach ($_l_keyword_array as $k => $v) {
  $lv = trim($v);
  $lv2 = [];
  if ($lv != '') {
    $lv2[] = $lv;
    $llv_kana = mb_convert_kana($v,"cH");
    if ($lv !== $llv_kana) {
      $lv2[] = $llv_kana;
    }
    $llv = mb_convert_kana($v,"KVC");
    if ($lv !== $llv) {
      $lv2[] = $llv;
    }

    foreach ($lists as $k2 => $v2) {
      if (in_array($llv_kana,$v2,true)) {
        foreach ($v2 as $k3 => $v3) {
          if (in_array($v3, $lv2 ,true)) {
            continue;
          }
          $lv2[] = $v3;
        }
        break;
      }
    }


    $_keyword_array[] = $lv2;
  }
}

$operator = 1;
$operator_str = 'すべて';
if ( ($_GET['s_ope']??'1')=='2' ) {
  $operator = 2;
  $operator_str = 'いずれかのキーワードを含む';
}

if (count($_keyword_array) > 1) {
  $operator_str = '('.$operator_str.')';
}else{
  $operator_str = '';
}

$headline = '「'.esc_html(implode( ' ' , $_l_keyword_array)).$operator_str.'」の検索結果';


global $wpdb;

//20200116start
$base_sql = "SELECT distinct object_id FROM wp_terms as a INNER JOIN wp_term_taxonomy as b ON `a`.`term_id` = `b`.`term_id` INNER JOIN wp_term_relationships as c ON `a`.`term_id` = `c`.`term_taxonomy_id` INNER JOIN wp_posts as p ON `p`.`ID` = `c`.`object_id` WHERE ( `b`.taxonomy='recipe_category' OR `b`.taxonomy='wprm_course' OR `b`.taxonomy='wprm_cuisine' OR `b`.taxonomy='wprm_ingredient' )";

$base_sql .= " AND ";

foreach($_keyword_array as $k => $v){

  if (count($v)===0) {
    continue;
  }

  $sql = $base_sql;

  $sql .= ' ( ';

  foreach ($v as $k2 => $v2) {
    if($k2!=0){
      $sql .=  ' OR';
    }

    $sql .= " a.name LIKE '%".$wpdb->esc_like( $v2 )."%' ";
  }

  $sql .= ' ) ';

  $results = $wpdb->get_results($sql);

  foreach($results as $r){
    $l_id = (int)$r->object_id;
    if(!in_array($l_id, $lists_post_ids[$k]?:[] ,true)){
      $lists_post_ids[$k][] = $l_id;
    }
  }
}

//20200116start
//$results = $wpdb->get_results($wpdb->prepare("SELECT post_id FROM wp_postmeta WHERE meta_value LIKE %s AND meta_key = 'wprm_ingredients'", '%'.$_keyword.'%'));
$base_sql = "SELECT post_id FROM wp_postmeta INNER JOIN wp_posts ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE (meta_key = 'recipe_desc' OR meta_key = 'wprm_ingredients' OR meta_key = 'wprm_author_name' )  AND ";
foreach($_keyword_array as $k => $v){
  if (count($v)===0) {
    continue;
  }

  $sql = $base_sql;
  $sql .= ' ( ';

  foreach ($v as $k2 => $v2) {
    if($k2!=0){
      $sql .=  ' OR';
    }
    $sql .= " meta_value LIKE '%".$wpdb->esc_like( $v2 )."%' ";
  }

  $sql .= ' ) ';

  $results = $wpdb->get_results($sql);

  foreach($results as $r){
    $l_id = (int)$r->post_id;
    if(!in_array($l_id, $lists_post_ids[$k]?:[] ,true)){
      $lists_post_ids[$k][] = $l_id;
    }
  }
}

//20200116start
//$results = $wpdb->get_results($wpdb->prepare("SELECT ID FROM wp_posts WHERE post_title LIKE %s", '%'.$_keyword.'%'));
$base_sql = "SELECT ID FROM wp_posts WHERE `wp_posts`.`post_type` = 'wprm_recipe' AND ";
foreach($_keyword_array as $k => $v){
  if (count($v)===0) {
    continue;
  }

  $sql = $base_sql;
  $sql .= ' ( ';

  foreach ($v as $k2 => $v2) {
    if($k2!=0){
      $sql .=  ' OR';
    }
    $sql .= " post_title LIKE '%".$wpdb->esc_like( $v2 )."%' ";
    $sql .= " OR post_content LIKE '%".$wpdb->esc_like( $v2 )."%' ";
  }

  $sql .= ' ) ';


  $results = $wpdb->get_results($sql);

  foreach($results as $r){
    $l_id = (int)$r->ID;
    if(!in_array($l_id, $lists_post_ids[$k]?:[] ,true)){
      $lists_post_ids[$k][] = $l_id;
    }
  }

}

foreach($_keyword_array as $k => $v){
  if (count($v)===0) {
    continue;
  }
  $l = $lists_post_ids[$k]?:[];
  if (count($l) > 0) {
    $results = $wpdb->get_results("SELECT meta_value FROM wp_postmeta INNER JOIN wp_posts ON `wp_posts`.`ID` = `wp_postmeta`.`meta_value` WHERE post_id IN (". join(',', $l) .") AND meta_key='wprm_parent_post_id'");
    foreach($results as $r){
      $l_id = (int)$r->meta_value;
      if(!in_array($l_id, $lists_post_ids[$k]?:[] ,true)){
        $lists_post_ids[$k][] = $l_id;
      }
    }
  }
}

if ($operator == 2) {
  foreach ($lists_post_ids as $k => $v) {
    foreach ($v as $k2 => $v2) {
      if (!in_array($v2, $post_ids , true)) {
        $post_ids[] = $v2;
      }
    }
  }
}else{
  $post_ids = $lists_post_ids[0]?:[];
  $l_num = count($lists_post_ids);
  if ($l_num > 1) {
    for ($i=1; $i < $l_num; $i++) { 
      $post_ids = array_intersect($post_ids,$lists_post_ids[$i]);
    }
  }
}

$page = 1;
if(isset($_GET['paged'])){
  $page = $_GET['paged'];
} else {
  $elms = explode('/', $_SERVER['REQUEST_URI']);
  if($elms[1] == 'page'){
    $page = (int)$elms[2];
  }
}
if(filter_var($page, FILTER_VALIDATE_INT) === false){
  $page  = 1;
}

$custom_query = null;
if (count($post_ids) > 0 || empty(trim($_GET['s'])?:'')) {
  $v_args = array(
    'post_type' => array( 'recipe' ),
    'orderby' => 'ASC',
    'posts_per_page' => 10,
    'paged' => $page
  );
  if (count($post_ids) > 0) {
    $v_args['post__in'] = $post_ids;
  }
  $custom_query = new WP_Query($v_args);
}



?><?php get_header(); ?>

<div id="bread_crumb">
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php _e('Search result','tcd-w'); ?></span><meta itemprop="position" content="2"></li>
</ul>
</div>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <h3 class="design_headline design_headline_nks clearfix rich_font"><span class="search_keyword"><?php echo esc_html($headline); ?></span></h3>

  <div id="blog_archive">

   <?php if ( $custom_query && $custom_query->have_posts() ) : ?>

   <div id="post_list" class="clearfix">
    <?php
         global $post;
         while ( $custom_query->have_posts() ) :
          $custom_query->the_post();
           $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
           if( get_post_meta($post->ID,'premium_post',true) ){
             $premium_recipe = get_post_meta($post->ID,'premium_post',true);
           }
           $post_type = $post->post_type;
           if($post_type == 'post' && $options['all_premium_post']){
             $premium_recipe = '1';
           }
           if(has_post_thumbnail()) {
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
           } elseif($options['no_image2']) {
             $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
           } else {
             $image = array();
             $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
           }

        /*
        $time_str = '';
        if (class_exists('WPRM_Recipe_Manager')) {
          $results = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_value='".$post->ID."' AND meta_key='wprm_parent_post_id'");
          if (is_array($results) && isset($results[0])) { 
            $recipe = WPRM_Recipe_Manager::get_recipe( $results[0]->post_id );
            $time_str = $recipe->cook_time_formatted();
          }
        }*/

        $recipe_time_label = get_post_meta($post->ID, 'recipe_time_label', true);
        if(empty($recipe_time_label)){
          $recipe_time_label = __('Cooking time', 'tcd-w');
        }
        $recipe_time = get_post_meta($post->ID, 'recipe_time', true);
        $recipe_price_label = get_post_meta($post->ID, 'recipe_price_label', true);
        if(empty($recipe_price_label)){
          $recipe_price_label = __('Estimated cost', 'tcd-w');
        }
        $recipe_price = get_post_meta($post->ID, 'recipe_price', true);
        
    ?>
    <article class="item clearfix<?php if( $post_type == 'post' ){ echo ' premium_post'; }; ?>">
     <a class="link animate_background<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
      <div class="image_wrap">
       <?php if($premium_recipe) { ?><div class="premium_icon"></div><?php }; ?>
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
      <div class="title_area">
       <h3 class="title rich_font"><?php the_title(); ?></h3>
       <?php if($recipe_time || $recipe_price){ ?>
       <ul class="nk_meta_recipe clearfix">
        <?php if($recipe_time){ ?>
        <li><?php echo esc_html($recipe_time_label); ?> <?php echo esc_html($recipe_time); ?></li>
        <?php }; ?>
        <?php if($recipe_price){ ?>
        <li><?php echo esc_html($recipe_price_label); ?> <?php echo esc_html($recipe_price); ?></li>
        <?php }; ?>
       </ul>
       <?php } ?>
       <?php if ( $options['show_archive_blog_date'] || $options['show_archive_blog_category'] ){ ?>
       <ul class="post_meta clearfix">
        <?php if ( $options['show_archive_blog_date'] ){ ?><li class="date"><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time></li><?php }; ?>
       </ul>
       <?php }; ?>
      </div>
     </a>
    </article>
    <?php endwhile; ?>
   </div><!-- END .post_list2 -->


   <?php 
    global $wp_query;
    $old_wp_query = $wp_query;
    $wp_query = $custom_query;
   ?>
   <?php get_template_part('template-parts/navigation'); ?>
   <?php 
    $wp_query = $old_wp_query;
   ?>

   <?php else: ?>

   <p id="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></p>

   <?php endif; ?>

  </div><!-- END #blog_archive -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
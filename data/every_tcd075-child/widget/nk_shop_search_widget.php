<?php

class nk_shop_search_widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      'nk_shop_search_widget',// ID
      __('お店検索', 'tcd-w'),
      array(
        'classname' => 'nk_shop_search_widget',
        'description' => __('お店検索表示', 'tcd-w')
      )
    );
  }

  function form( $instance ) {
  
    $title = isset( $instance['title'] ) ? $instance['title'] : ''; 
?>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Title', 'tcd-w'); ?></h3>
  <input class="widefat" name="<?php echo $this->get_field_name( 'title' ); ?>'" type="text" value="<?php echo esc_attr( $title ); ?>">
</div>
<?php
  }

    public function widget( $args, $instance ) {
      $title = apply_filters( 'widget_title', $instance['title'] );
      echo $args['before_widget'];
      if ( ! empty( $title ) ){
          echo $args['before_title'] . $title . $args['after_title'];
      }
?>
<div class="gcse-search-w">
<form role="search" method="get" class="searchform shopsearchform" action="/">
	<input type="hidden" name="map" value="y">
	<input type="hidden" name="map_s" value="1">
	<div>
		<label class="shop-screen-reader-text" for="shop_s_<?php echo $this->id ?>">検索 …</label>
		<input type="text" value="" name="shop_s" id="shop_s_<?php echo $this->id ?>" class="shop_s" placeholder="お店を検索" required="">
		<div class="shop-submit_button"><div class="submit_button"><input type="submit" value="検索" class="searchshopsubmit"></div></div>
	</div>
</form>
</div>       
<?php
        echo $args['after_widget'];
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }

}

function register_nk_shop_search_widget() {
  register_widget( 'nk_shop_search_widget' );
}
add_action( 'widgets_init', 'register_nk_shop_search_widget' );
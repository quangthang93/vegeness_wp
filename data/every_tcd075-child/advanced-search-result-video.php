<?php
if ( !defined( 'ABSPATH' ) ) exit;
if (empty($_GET['video'])){
  header("Location: "."http://{$_SERVER['HTTP_HOST']}");
  exit();
}


global $wpdb;
$post_ids = array();
$results = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_value <> '' AND meta_key = 'wprm_video_embed'");
foreach($results as $r){
  array_push($post_ids, (int)$r->post_id);
}

$results = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id IN (". join(',', $post_ids) .") AND meta_key='wprm_parent_post_id'");
foreach($results as $r){
  if(!in_array($r->meta_value, $post_ids)){
    array_push($post_ids, (int)$r->meta_value);
  }
}
$results = $wpdb->get_results("SELECT ID FROM wp_posts WHERE ID IN (" . join(',', $post_ids) . ")");
$final_posts = array();
foreach($results as $r){
  array_push($final_posts, (int)$r->ID);
}
$page = 1;
if(isset($_GET['paged'])){
  $page = $_GET['paged'];
} else {
  $elms = explode('/', $_SERVER['REQUEST_URI']);
  if($elms[1] == 'page'){
    $page = (int)$elms[2];
  }
}
if(filter_var($page, FILTER_VALIDATE_INT) === false){
  $page  = 1;
}
$v_args = array(
  'post_type' => array( 'recipe' ),
  'orderby' => 'ASC',
  'post__in' => $final_posts,
  'posts_per_page' => 9,
  'paged' => $page
);
$custom_query = new WP_Query($v_args);

?><?php get_header(); ?>
<style type="text/css">
#archive-title {
    margin: 10px 0;
    font-size: 1.5rem;
}
</style>

<div id="bread_crumb">
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>?video=list"><span itemprop="name">動画一覧</span></a><meta itemprop="position" content="2"></li>
</ul>
</div>

<div id="main_contents" class="clearfix asr-mappage">

 <div id="main_col" class="clearfix">
<?php if(!empty($_GET['video'])) : ?>
<h1 id="archive-title" class="archive-title">
    <span class="fa fa-folder-open"></span>動画一覧
</h1>
<?php endif?>

   <?php if ( $custom_query->have_posts() ) : ?>

   <div class="recipe_list type2 clearfix">
    <?php
         while ( $custom_query->have_posts() ) :
          $custom_query->the_post();

           $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
           $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
           if(has_post_thumbnail()) {
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
           } elseif($options['no_image2']) {
             $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
           } else {
             $image = array();
             $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
           }
    ?>
    <article class="item<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>">
     <a class="link animate_background" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
      <div class="image_wrap">
       <?php if($premium_recipe && $options['category_recipe_list_show_premium_icon']) { ?><div class="premium_icon"></div><?php }; ?>
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
     </a>
     <div class="title_area">
      <h3 class="title"><a href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>"><span><?php the_title(); ?></span></a></h3>
      <?php
           if($parent_cat_id == 0){  // parent category page
             $args = array( 'orderby' => 'term_order' );
             $recipe_child_category = wp_get_post_terms( $post->ID, 'recipe_category' ,$args);
             if ($recipe_child_category && $options['category_recipe_list_show_category']) {
      ?>
      <p class="post_meta">
       <?php
            foreach ( $recipe_child_category as $child_cat ) :
              if($child_cat->parent != 0) {
                 $child_cat_name = $child_cat->name;
                 $child_cat_id = $child_cat->term_id;
       ?>
       <a href="<?php echo esc_url(get_term_link($child_cat_id,'recipe_category')); ?>"><?php echo esc_html($child_cat_name); ?></a>
       <?php
              break;
              }
             endforeach;
       ?>
      </p>
      <?php
             };
           } else { // child category page
             if($options['category_recipe_list_show_post_view']) {
      ?>
      <p class="post_meta"><?php if($recipe_type != 'type2') { _e('Hits:', 'tcd-w'); } else { _e('Views:', 'tcd-w'); }; ?><?php the_post_views(); ?></p>
      <?php
             };
           };
      ?>
     </div>
    </article>
    <?php endwhile; ?>
   </div><!-- END .recipe_list1 -->

   <?php 
    global $wp_query;
    $old_wp_query = $wp_query;
    $wp_query = $custom_query;
   ?>
   <?php get_template_part('template-parts/navigation'); ?>
   <?php 
    $wp_query = $old_wp_query;
   ?>

   <?php else: ?>

   <p id="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></p>

   <?php endif; ?>

   <?php wp_reset_postdata(); ?>

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
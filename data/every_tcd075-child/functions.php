<?php

// テーマオプション --------------------------------------------------------------------------------
require_once ( dirname(__FILE__) . '/admin/theme-options.php' );


// ウィジェット ------------------------------------------------------------------------
require_once ( dirname(__FILE__) . '/widget/nk_shop_search_widget.php' );


define('NK_VER','20200822');

add_action( 'wp_enqueue_scripts', 'nk_theme_enqueue_styles' );
function nk_theme_enqueue_styles() {
	wp_deregister_style('style');
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' , [] , NK_VER );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css',['parent-style'] , NK_VER);
}

function nk_theme_enqueue_styles_shop() {
  wp_enqueue_style( 'swipe-style', get_stylesheet_directory_uri() . '/assets/js/swiper/css/swiper.min.css',['child-style'] ,NK_VER);
  wp_enqueue_style( 'luminous-style', get_stylesheet_directory_uri() . '/assets/js/luminous/luminous-basic.min.css',['child-style'] ,NK_VER);
  wp_enqueue_style( 'shop-style', get_stylesheet_directory_uri() . '/assets/css/shop.css',['child-style'] , NK_VER);
  
  wp_enqueue_script( 'nk-swipe', get_stylesheet_directory_uri() . '/assets/js/swiper/js/swiper.min.js', [], NK_VER  ,true);

  $js = <<<EOT
var galleryThumbs = new Swiper('.gallery-thumbs', {
  spaceBetween: 10,
  slidesPerView: 4,
  loop: true,
  freeMode: true,
  loopedSlides: 5, //looped slides should be the same
  watchSlidesVisibility: true,
  watchSlidesProgress: true,
});
var galleryTop = new Swiper('.gallery-top', {
  spaceBetween: 10,
  loop:true,
  loopedSlides: 5, //looped slides should be the same
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  thumbs: {
    swiper: galleryThumbs,
  },
});
EOT;
  wp_add_inline_script('nk-swipe',$js);

  wp_enqueue_script( 'nk-luminous', get_stylesheet_directory_uri() . '/assets/js/luminous/Luminous.min.js', [], NK_VER  ,true);

  $js = <<<EOT
new LuminousGallery(document.querySelectorAll(".gallery-luminous"));
EOT;
  wp_add_inline_script('nk-luminous',$js);

}

function nk_theme_enqueue_styles_s_type() {
  wp_enqueue_style( 's_type-style', get_stylesheet_directory_uri() . '/assets/css/s_type.css',['child-style'] , NK_VER);
}



function wpse_load_custom_search_template(){
	if( !empty($_REQUEST['s'])) {
	    require('advanced-search-result.php');
	    die();
	}else{
		if( !empty($_REQUEST['video'])) {
			require('advanced-search-result-video.php');
			die();
		}
		if( !empty($_REQUEST['map'])) {
			require('advanced-search-result-map.php');
			die();
		}
    if( !empty($_REQUEST['s_type'])) {
      require('advanced-search-result-type.php');
      die();
    }
	}
}
add_action('init','wpse_load_custom_search_template');

function nk_register_post_type_args($args,$name){
	if ($name === 'recipe'||$name === 'news') {
		$args['rewrite']['with_front']= false;
	}
	return $args;
}

add_filter('register_post_type_args','nk_register_post_type_args',10,2);

function nk_admin_enqueue_scripts(){
	if (wp_style_is('wprm-admin-modal')) {
$css_text = <<<EOM
.wprm-admin-modal-field-ingredient-header-container .wprm-admin-modal-field-ingredient-header:nth-child(3) {
    order: -2;
}
.wprm-admin-modal-field-ingredient .wprm-admin-modal-field-ingredient-text-container .wprm-admin-modal-field-richtext:nth-child(3){
    order: -2;   
}
.wprm-admin-modal-field-ingredient-header-container .wprm-admin-modal-field-ingredient-header:nth-child(2){
    order:-1;
}
.wprm-admin-modal-field-ingredient .wprm-admin-modal-field-ingredient-text-container .wprm-admin-modal-field-richtext:nth-child(2) {
    order:-1;
}
EOM;
		wp_register_style( 'wprm-admin-modal-nk-add', false ,['wprm-admin-modal']);
		wp_enqueue_style( 'wprm-admin-modal-nk-add' );
		wp_add_inline_style('wprm-admin-modal-nk-add',$css_text);
	}
}
add_action('admin_enqueue_scripts','nk_admin_enqueue_scripts',11);



register_post_type( 'shop', array(
  'label' => 'お店',
  'labels' => array(
	  'name' => 'お店',
	  'add_new' => __( 'Add New', 'tcd-w' ),
	  'add_new_item' => __( 'Add New Item', 'tcd-w' ),
	  'edit_item' => __( 'Edit', 'tcd-w' ),
	  'new_item' => __( 'New item', 'tcd-w' ),
	  'view_item' => __( 'View Item', 'tcd-w' ),
	  'search_items' => __( 'Search Items', 'tcd-w' ),
	  'not_found' => __( 'Not Found', 'tcd-w' ),
	  'not_found_in_trash' => __( 'Not found in trash', 'tcd-w' ),
	  'parent_item_colon' => ''
	),
  'public' => true,
  'publicly_queryable' => true,
  'menu_position' => 5,
  'show_ui' => true,
  'query_var' => true,
  'rewrite' => array( 'slug' => 'shop' ),
  'capability_type' => 'post',
  'has_archive' => false,
  'hierarchical' => false,
  'supports' => array( 'title', 'editor', 'thumbnail' )
));

register_taxonomy( 'shop_area', 'shop', array(
  'labels' => array(
	  'name' => 'お店(エリア)',
	  'singular_name' => 'お店(エリア)'
	),
  'public' => false,
  'show_ui' => true, 
  'show_admin_column' => true, 
  'show_in_nav_menus' => true, 
  'show_tagcloud' => true, 
  'hierarchical' => true,
  'rewrite' => array( 'slug' => 'area' ),
  'show_in_rest' => false
));

register_taxonomy( 'shop_genre', 'shop', array(
  'labels' => array(
	  'name' => 'お店(ジャンル)',
	  'singular_name' => 'お店(ジャンル)'
	),
  'public' => false,
  'show_ui' => true, 
  'show_admin_column' => true, 
  'show_in_nav_menus' => true, 
  'show_tagcloud' => true, 
  'hierarchical' => true,
  'rewrite' => array( 'slug' => 'genre' ),
  'show_in_rest' => false	// REST APIで表示しない
));

register_taxonomy( 'shop_icon', 'shop', array(
  'labels' => array(
	  'name' => 'お店(アイコン)',
	  'singular_name' => 'お店(アイコン)'
	),
  'public' => false,
  'show_ui' => true, 
  'show_admin_column' => true, 
  'show_in_nav_menus' => true, 
  'show_tagcloud' => true, 
  'hierarchical' => true,
  'rewrite' => array( 'slug' => 'icon' ),
  'show_in_rest' => false	// REST APIで表示しない
));

function shop_post_type_link( $link, $post ){
  if ( 'shop' === $post->post_type ) {
    return home_url( '/shop/' . $post->ID );
  } else {
    return $link;
  }
}
add_filter( 'post_type_link', 'shop_post_type_link', 1, 2 );

function shop_rewrite_rules_array( $rules ) {
  $new_rules = [
    'shop/([0-9]+)/?$' => 'index.php?post_type=shop&p=$matches[1]',
  ];

  return $new_rules + $rules;
}
add_filter( 'rewrite_rules_array', 'shop_rewrite_rules_array' );

function nk_shop_icons_data(){
  static $icons;
  if (is_array($icons)) {
    return $icons;
  }

  $icons_data = get_terms('shop_icon',
    [
      'hide_empty' => true, 
      'orderby' => 'term_order',
      'parent' => 0
    ]
  );

  $icons = [];
  if ( $icons_data && ! is_wp_error($icons_data) ) {
    foreach ($icons_data as $k => $v) {
      $icons[$v->term_id] = [
        'i' => $v,
        'm' => nk_shop_icon_meta($v->term_id,true)
      ];
    }
  }

  return $icons;
}

function nk_shop_areas($id){
  $args = array( 'orderby' => 'term_order' );
  $l_areas = wp_get_post_terms( $id, 'shop_area' ,$args);

  $areas = [];

  foreach ($l_areas as $k => $v) {
    if ($v->parent == 0) {
      $areas[] = $v;
      unset($l_areas[$k]);
      break;
    }
  }

  if (isset($areas[0])) {
    foreach ($l_areas as $k => $v) {
      if ($areas[0]->term_id==$v->parent) {
        $areas[] = $v;
        unset($l_areas[$k]);
        break;
      }
    }
  }

  if (isset($areas[1])) {
    foreach ($l_areas as $k => $v) {
      if ($areas[1]->term_id==$v->parent) {
        $areas[] = $v;
        unset($l_areas[$k]);
        break;
      }
    }
  }

  return $areas;
}

function nk_shop_genres($id){
  $args = array( 'orderby' => 'term_order' );
  $genres = wp_get_post_terms( $id, 'shop_genre' ,$args);
  return $genres;
}

function nk_shop_icons($id){
  $args = array( 'orderby' => 'term_order' );
  $shop_icons = wp_get_post_terms( $id, 'shop_icon' ,$args);
  $icon_data = nk_shop_icons_data();

  $icons = [];

  if ( $shop_icons && ! is_wp_error($shop_icons) ) {
   foreach ( $shop_icons as $icon ){
      $icons[] = $icon_data[$icon->term_id];
   }
  };

  return $icons;
}

function nk_type_menus_all(){
  return [
  [
    'n' => '主食',
    'lists' => [
      '100' => [
        'n' => 'ごはんもの',
        'lists' => [
          '1' => [
            'n' => 'ごはんもの',
            's' => [
              'ごはんもの',
              'ご飯もの',
            ],
            's_no' => [
              'その他'
            ]
          ],
          '2' => [
            'n' => '丼',
            's' => [
              '丼'
            ],
          ],
          '3' => [
            'n' => 'カレー',
            's' => [
              'カレー'
            ],
          ],
          '4' => [
            'n' => 'おにぎり',
            's' => [
              'おにぎり'
            ],
          ],
          '5' => [
            'n' => '雑炊・お粥',
            's' => [
              '雑炊',
              'お粥',
              'リゾット'
            ],
          ],
          '6' => [
            'n' => 'その他',
            's' => [
              'ごはんもの&&その他'
            ],
          ],
        ]
      ],
      '200' => [
        'n' => '麺類', 
        'lists' => [
          '1' => [
            'n' => 'パスタ',
            's' => ['パスタ','スパゲッティ']
          ],
          '2' => [
            'n' => 'うどん',
            's' => ['うどん']
          ],
          '3' => [
            'n' => 'そば',
            's' => ['そば','蕎麦']
          ],
          '4' => [
            'n' => '素麺',
            's' => ['素麺','そうめん']
          ],
          '5' => [
            'n' => 'その他',
            's' => ['麺類&&その他']
          ],
        ]
      ],
      '300' => [
        'n' => 'パン',
        'lists' => [
          '1' => [
            'n' => 'パン',
            's' => ['パン']
          ],
          '2' => [
            'n' => '米粉パン',
            's' => ['米粉パン']
          ],
          '3' => [
            'n' => 'サンドイッチ',
            's' => ['サンドイッチ']
          ],
        ]
      ]
    ]
  ],[
    'n' => 'おかず',
    'lists' => [
      '400' => [
        'n' => 'メインのおかず',
        'lists' => [
          '1' => [
            'n' => '焼き物',
            's' => ['主菜&&焼き物','主菜&&焼きもの']
          ],
          '2' => [
            'n' => '揚げ物',
            's' => ['主菜&&揚げ物','主菜&&揚げもの']
          ],
          '3' => [
            'n' => '炒め物',
            's' => ['主菜&&炒め物','主菜&&炒めもの']
          ],
          '4' => [
            'n' => '煮物',
            's' => ['主菜&&煮物']
          ],
        ]
      ],
      '500' => [
        'n' => '副菜',
        'lists' => [
          '1' => [
            'n' => '焼き物',
            's' => ['副菜&&焼き物','副菜&&焼きもの']
          ],
          '2' => [
            'n' => '揚げ物',
            's' => ['副菜&&揚げ物','副菜&&揚げもの']
          ],
          '3' => [
            'n' => '炒め物',
            's' => ['副菜&&炒め物','副菜&&炒めもの']
          ],
          '4' => [
            'n' => '煮物',
            's' => ['副菜&&煮物']
          ],
          '5' => [
            'n' => '和え物',
            's' => ['副菜&&和え物']
          ],
          '6' => [
            'n' => '漬物',
            's' => ['副菜&&漬物']
          ],
        ]
      ],
      '600' => [
        'n' => 'サラダ',
        'lists' => [
          '1' => [
            'n' => 'サラダ',
            's' => ['サラダ']
          ],
        ]
      ],
      '700' => [
        'n' => 'スープ・汁物',
        'lists' => [
          '1' => [
            'n' => '汁物',
            's' => ['汁物']
          ],
          '2' => [
            'n' => 'スープ',
            's' => ['スープ']
          ],
          '3' => [
            'n' => 'シチュー',
            's' => ['シチュー']
          ],
        ]
      ],
      '800' => [
        'n' => '常備菜',
        'lists' => [
          '1' => [
            'n' => '常備菜',
            's' => ['常備菜','作り置き']
          ],
        ]
      ],
      '900' => [
        'n' => 'もどき料理',
        'lists' => [
          '1' => [
            'n' => 'もどき料理',
            's' => ['もどき料理']
          ],
        ]
      ]
    ]
  ],[
    'n' => 'スイーツ',
    'lists' => [
      '2000' => [
        'n' => '洋菓子',
        's' => ['洋菓子'],
        'lists' => [
          '1' => [
            'n' => 'クッキー',
            's' => ['クッキー','米粉クッキー']
          ],
          '2' => [
            'n' => 'ケーキ類',
            's' => ['ケーキ','パウンドケーキ','カップケーキ']
          ],
          '8' => [
            'n' => 'スコーン',
            's' => ['スコーン']
          ],
          '3' => [
            'n' => 'プリン・ゼリー',
            's' => ['プリン','ゼリー']
          ],
          '4' => [
            'n' => 'パンケーキ',
            's' => ['パンケーキ']
          ],
          '5' => [
            'n' => 'アイス',
            's' => ['アイス']
          ],
          '6' => [
            'n' => '米粉スイーツ',
            's' => ['米粉クッキー','米粉パン','米粉スイーツ']
          ],
          '7' => [
            'n' => 'その他',
            's' => ['洋菓子&&その他']
          ],
        ]
      ],
      '2100' =>[
        'n'=>'和菓子',
        's' => ['和菓子'],
        'lists' => [
          '1' => [
            'n' => '和菓子',
            's' => ['和菓子']
          ]
        ]
      ]
    ]
  ]


  ];
}

function nk_type_menus(){
  return [
    '100' => [
      'n' => 'ごはんもの',
      's' => [
        'ごはんもの',
        'ご飯もの',
      ],
    ],
    '200' => [
      'n' => '麺類',
      's' => ['麺類'],
    ],
    '300' => [
      'n' => 'パン',
      's' => ['パン','米粉パン','サンドイッチ'],
    ],
    '600' => [
      'n' => 'サラダ',
      's' => ['サラダ'],
    ],
    '400' => [
      'n' => 'メインのおかず',
      's' => ['主菜'],
    ],
    '500' => [
      'n' => '副菜',
      's' => ['副菜','一品料理'],
    ],
    '700' => [
      'n' => 'スープ・汁物',
      's' => ['スープ','汁物'],
    ],
    '800' => [
      'n' => '常備菜',
      's' => ['常備菜','作り置き'],
    ],
    '900' => [
      'n' => 'もどき料理',
      's' => ['もどき料理'],
    ],
    '1000' => [
      'n' => 'スイーツ',
      's' => ['洋菓子','和菓子','ドリンク'],
    ],
    '1100' => [
      'n' => '',
    ],
    'all' => [
      'n' => 'すべて'
    ],
  ];
}

function nk_type_ingredients(){

  return [
    '100' => [
      'n' => '根菜・芋類',
      'lists' => [
        '1' => [
          'n' => 'じゃがいも',
          's' => ['じゃがいも','じゃが芋'],
          's_no'=>[
            '新じゃがいも',
            '新じゃが芋',
          ]
        ],
        '2' => [
          'n' => '玉ねぎ',
          's' => ['たまねぎ','玉ねぎ','玉葱'],
          's_no' => [
            '新たまねぎ',
            '新玉ねぎ',
            '新玉葱',
          ]
        ],
        '3' => [
          'n' => '人参',
          's' => ['にんじん','人参']
        ],
        '4' => [
          'n' => '大根',
          's' => ['だいこん','大根']
        ],
        '5' => [
          'n' => 'さつまいも',
          's' => ['さつまいも','さつま芋','薩摩芋']
        ],
        '6' => [
          'n' => '里芋',
          's' => ['さといも','さと芋','里芋']
        ],
        '7' => [
          'n' => '長いも',
          's' => ['ながいも','長芋','長いも']
        ],
        '8' => [
          'n' => 'ごぼう',
          's' => ['ごぼう','牛蒡']
        ],
        '9' => [
          'n' => 'れんこん',
          's' => ['れんこん','蓮根']
        ],
        '10' => [
          'n' => 'かぶ',
        ],
        '11' => [
          'n' => 'にんにく',
        ],
        '12' => [
          'n' => 'ビーツ',
        ],
        '13' => [
          'n' => 'ラディッシュ',
        ],
        '14' => [
          'n' => '菊芋',
        ],
        '15' => [
          'n' => '山芋',
          's' => ['やまいも','山芋']
        ],
      ]
    ],
    '200' => [
      'n' => '葉物',
      'lists' => [
        '1' => [
          'n' => 'ほうれん草',
          's' => ['ほうれんそう','ほうれん草']
        ],
        '2' => [
          'n' => '小松菜',
          's' => ['こまつな','小松菜']
        ],
        '3' => [
          'n' => 'きゃべつ',
          's_no'=>[
            '芽きゃべつ',
            '春きゃべつ'
          ]
        ],
        '4' => [
          'n' => '白菜',
          's' => ['はくさい','白菜']
        ],
        '5' => [
          'n' => 'ねぎ',
          's' => ['ねぎ','葱'],
          's_no' => [
            'たまねぎ',
            '玉ねぎ',
            '玉葱'
          ]
        ],
        '6' => [
          'n' => 'ニラ',
          's_no' => ['バニラ']
        ],
        '7' => [
          'n' => 'モロヘイヤ',
        ],
        '8' => [
          'n' => '水菜',
          's' => ['みずな','水菜']
        ],
        '9' => [
          'n' => '春菊',
          's' => ['しゅんぎく','春菊']
        ],
        '10' => [
          'n' => 'チンゲン菜',
        ],
        '11' => [
          'n' => '豆苗',
          's' => ['そらまめ','そら豆','空豆']
        ],
        '12' => [
          'n' => '空芯菜',
        ],
        '13' => [
          'n' => 'レタス',
        ],
        '14' => [
          'n' => 'サンチュ',
        ],
        '15' => [
          'n' => '大葉',
        ],
        '16' => [
          'n' => 'ケール',
        ],
        '17' => [
          'n' => 'からし菜',
        ],
        '18' => [
          'n' => 'バジル',
        ],
        '19' => [
          'n' => 'みつば',
          's' => ['みつば','三つ葉']
        ],
        '20' => [
          'n' => '菜の花',
          's' => ['なのはな','菜の花']
        ],
        '21' => [
          'n' => 'わさび菜',
        ],    
      ]
    ],
    '300' => [
      'n' => '果菜',
      'lists' => [
        '1' => [
          'n' => 'トマト',
        ], 
        '2' => [
          'n' => 'なす',
          's' => ['なす','茄子']
        ], 
        '3' => [
          'n' => 'きゅうり',
          's' => ['きゅうり','胡瓜']
        ], 
        '4' => [
          'n' => 'かぼちゃ',
          's' => ['かぼちゃ','南瓜']
        ], 
        '5' => [
          'n' => 'おくら',
        ], 
        '6' => [
          'n' => 'ブロッコリー',
        ], 
        '7' => [
          'n' => 'ピーマン',
        ], 
        '8' => [
          'n' => 'ズッキーニ',
        ], 
        '9' => [
          'n' => 'アボカド',
        ], 
        '10' => [
          'n' => '枝豆',
          's' => ['えだまめ','枝豆']
        ], 
        '11' => [
          'n' => 'ゴーヤ',
        ], 
        '12' => [
          'n' => 'とうもろこし',
        ], 
        '13' => [
          'n' => 'パプリカ',
        ], 
        '14' => [
          'n' => 'カリフラワー',
        ], 
        '15' => [
          'n' => 'みょうが',
          's' => ['みょうが','茗荷']
        ], 
        '16' => [
          'n' => 'アスパラガス',
        ], 
      ]
    ],
    '400' => [
      'n' => 'きのこ・豆類',
      'lists' => [ 
        '1' => [
          'n' => 'しいたけ',
          's' => ['しいたけ','椎茸']
        ], 
        '2' => [
          'n' => 'えりんぎ',
        ], 
        '3' => [
          'n' => '舞茸',
          's' => ['まいたけ','舞茸']
        ],
        '4' => [
          'n' => 'えのき',
        ],
        '5' => [
          'n' => 'しめじ',
        ],
        '6' => [
          'n' => 'なめこ',
        ],
        '7' => [
          'n' => 'ひよこ豆',
          's' => ['ひよこまめ','ひよこ豆']
        ],
        '8' => [
          'n' => 'レンズ豆',
        ],
        '9' => [
          'n' => '大豆',
        ],
        '10' => [
          'n' => 'もやし',
        ],
        '11' => [
          'n' => 'えんどう',
          's_no' => [
            'スナップエンドウ',
            'さやえんどう'
          ]
        ],
        '12' => [
          'n' => 'いんげん',
        ],
        '13' => [
          'n' => 'スナップエンドウ',
        ],
        '14' => [
          'n' => '空豆',
          's' => ['そらまめ','そら豆','空豆']
        ],
      ]
    ],
    '900' => [
      'n' => '大豆製品',
      'skip' => true,
      'lists' => [
        '1' => [
          'n' => '豆腐'
        ],
        '2' => [
          'n' => '厚揚げ'
        ],
        '3' => [
          'n' => '油揚げ'
        ],
        '4' => [
          'n' => '納豆'
        ],
        '5' => [
          'n' => 'おから'
        ],
      ]
    ],
    '500' => [
      'n' => '春野菜',
      'lists'=> [
        '1' => [
          'n' => 'たけのこ',
          's' => ['たけのこ','筍','竹の子']
        ],
        '2' => [
          'n' => 'ふき',
        ],
        '3' => [
          'n' => '菜の花',
        ],
        '4' => [
          'n' => '春きゃべつ',
        ],
        '5' => [
          'n' => '新じゃがいも',
          's' => ['新じゃがいも','新じゃが芋'],
        ],
        '6' => [
          'n' => 'アスパラ',
        ],
        '7' => [
          'n' => '枝豆',
          's' => ['えだまめ','枝豆']
        ],
        '8' => [
          'n' => 'からし菜',
        ],
        '9' => [
          'n' => 'グリーンピース',
        ],
        '10' => [
          'n' => 'クレソン',
        ],
        '11' => [
          'n' => 'さやえんどう',
        ],
        '12' => [
          'n' => 'セロリ',
        ],
        '13' => [
          'n' => 'そらまめ',
          's' => ['そらまめ','そら豆','空豆']
        ],
        '14' => [
          'n' => '新たまねぎ',
          's' =>[
            '新たまねぎ',
            '新玉ねぎ',
            '新玉葱',
          ]
        ],
        '15' => [
          'n' => 'にら',
          's_no' => ['バニラ']
        ],
        '16' => [
          'n' => '水菜',
          's' => ['みずな','水菜']
        ],
        '17' => [
          'n' => 'みつば',
          's' => ['みつば','三つ葉']
        ],
        '18' => [
          'n' => 'ルッコラ',
        ],
        '19' => [
          'n' => 'トマト',
        ],
      ]
    ],
    '600' => [
      'n' => '夏野菜',
      'lists' => [
        '1' => [
          'n' => 'うり',
          's_no' => [
            'きゅうり'
          ]
        ],
        '2' => [
          'n' => '枝豆',
          's' => ['えだまめ','枝豆']
        ],
        '3' => [
          'n' => 'オクラ',
        ],
        '4' => [
          'n' => 'きゃべつ',
          's_no'=>[
            '芽きゃべつ',
            '春きゃべつ'
          ]
        ],
        '5' => [
          'n' => 'きゅうり',
          's' => ['きゅうり','胡瓜']
        ],
        '6' => [
          'n' => '空芯菜',
        ],
        '7' => [
          'n' => 'ゴーヤ',
        ],
        '8' => [
          'n' => 'さやいんげん',
        ],
        '9' => [
          'n' => 'ししとう',
        ],
        '10' => [
          'n' => 'ズッキーニ',
        ],
        '11' => [
          'n' => 'とうもろこし',
        ],
        '12' => [
          'n' => 'トマト',
        ],
        '13' => [
          'n' => 'なす',
          's' => ['なす','茄子']
        ],
        '14' => [
          'n' => 'ピーマン',
        ],
        '15' => [
          'n' => 'みょうが',
          's' => ['みょうが','茗荷']
        ],
        '16' => [
          'n' => 'モロヘイヤ',
        ],
        '17' => [
          'n' => 'レタス',
        ],
      ]
    ],
    '700' => [
      'n' => '秋野菜',
      'lists' => [
        '1' => [
          'n' => 'かぼちゃ',
          's' => ['かぼちゃ','南瓜']
        ],
        '2' => [
          'n' => 'ごぼう',
          's' => ['ごぼう','牛蒡']
        ],
        '3' => [
          'n' => 'さつまいも',
          's' => ['さつまいも','さつま芋','薩摩芋']
        ],
        '4' => [
          'n' => '里芋',
          's' => ['さといも','さと芋','里芋']
        ],
        '5' => [
          'n' => 'じゃがいも',
          's' => ['じゃがいも','じゃが芋'],
          's_no'=>[
            '新じゃがいも',
            '新じゃが芋',
          ]
        ],
        '6' => [
          'n' => '玉ねぎ',
          's' => ['たまねぎ','玉ねぎ','玉葱'],
          's_no' => [
            '新たまねぎ',
            '新玉ねぎ',
            '新玉葱',
          ]
        ],
        '7' => [
          'n' => 'にんじん',
          's' => ['にんじん','人参']
        ],
        '8' => [
          'n' => 'ビーツ',
        ],
        '9' => [
          'n' => '山芋',
          's' => ['やまいも','山芋']
        ],
        '10' => [
          'n' => 'ルッコラ',
        ],
        '11' => [
          'n' => 'れんこん',
          's' => ['れんこん','蓮根']
        ],
        '12' => [
          'n' => 'しいたけ',
          's' => ['しいたけ','椎茸']
        ],
        '13' => [
          'n' => 'しめじ',
        ],
        '14' => [
          'n' => 'まいたけ',
          's' => ['まいたけ','舞茸']
        ],
        '15' => [
          'n' => 'なめこ',
        ],
        '16' => [
          'n' => '松茸',
        ],
        '17' => [
          'n' => 'きくらげ',
          's' => ['きくらげ','木耳']
        ],
      ]
    ],
    '800' => [
      'n' => '冬野菜',
      'lists' => [
        '1' => [
          'n' => 'かぶ',
        ],
        '2' => [
          'n' => 'からし菜',
        ],
        '3' => [
          'n' => 'カリフラワー',
        ],
        '4' => [
          'n' => 'きゃべつ',
          's_no'=>[
            '芽きゃべつ',
            '春きゃべつ'
          ]
        ],
        '5' => [
          'n' => 'ごぼう',
          's' => ['ごぼう','牛蒡']
        ],
        '6' => [
          'n' => '小松菜',
          's' => ['こまつな','小松菜']
        ],
        '7' => [
          'n' => '里芋',
          's' => ['さといも','さと芋','里芋']
        ],
        '8' => [
          'n' => '春菊',
          's' => ['しゅんぎく','春菊']
        ],
        '9' => [
          'n' => '大根',
          's' => ['だいこん','大根']
        ],
        '10' => [
          'n' => 'チンゲン菜',
        ],
        '11' => [
          'n' => 'にんじん',
          's' => ['にんじん','人参']
        ],
        '12' => [
          'n' => 'ねぎ',
          's' => ['ねぎ','葱'],
          's_no' => [
            'たまねぎ',
            '玉ねぎ',
            '玉葱'
          ]
        ],
        '13' => [
          'n' => '白菜',
          's' => ['はくさい','白菜']
        ],
        '14' => [
          'n' => 'ブロッコリー',
        ],
        '15' => [
          'n' => 'ほうれん草',
          's' =>['ほうれんそう','ほうれん草']
        ],
        '16' => [
          'n' => '水菜',
          's' => ['みずな','水菜']
        ],
        '17' => [
          'n' => 'みつば',
          's' => ['みつば','三つ葉']
        ],
        '18' => [
          'n' => 'れんこん',
          's' => ['れんこん','蓮根']
        ],
        '19' => [
          'n' => '芽キャベツ',
        ],
      ]
    ],
    'all' => [
      'n' => 'すべて'
    ],
  ];
}

?>
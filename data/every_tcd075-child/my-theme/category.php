<?php
/**
* Template Name: おすすめ記事テンプレート
*
*/
add_action('wp_head',function(){ ?>
<script type="text/javascript">
jQuery(document).ready(function($){
  var base_image = $('#recipe_archive .item');
  var target_image = $('#recipe_archive .blur_image');
  var image_width = base_image.width();
  var image_height = base_image.height();
  target_image.css({'width': image_width, 'height': image_height });
  $(window).on('resize',function(){
    image_width = base_image.width();
    image_height = base_image.height();
    target_image.css({'width': image_width, 'height': image_height });
  });
});
</script>
<?php });

     get_header();
     $options = get_design_plus_option();

     $headline = $options['blog_label'];
     $headline_color = $options['blog_headline_color'];
     $desc = $options['blog_desc'];
     $image_id = $options['blog_bg_image'];
     $use_overlay = $options['blog_use_overlay'];
     if($use_overlay) {
       $overlay_color = hex2rgb($options['blog_overlay_color']);
       $overlay_color = implode(",",$overlay_color);
       $overlay_opacity = $options['blog_overlay_opacity'];
     }
     if($image_id) {
       $b_image = wp_get_attachment_image_src( $image_id, 'full' );
     }
?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <?php if(!is_paged()) { ?>
  <div id="page_header" <?php if($image_id) { ?>style="background:url(<?php echo esc_attr($b_image[0]); ?>) no-repeat center center; background-size:cover;"<?php }; ?>>
   <?php if($headline){ ?><h2 class="headline rich_font" style="background:<?php echo esc_attr($headline_color); ?>;"><span><?php echo wp_kses_post(nl2br($headline)); ?></span></h2><?php }; ?>
   <?php if($desc){ ?><p class="desc"><?php echo wp_kses_post(nl2br($desc)); ?></p><?php }; ?>
   <?php if($use_overlay) { ?><div class="overlay" style="background: -webkit-linear-gradient(top, transparent 50%, rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>) 100%); background: linear-gradient(to bottom, transparent 50%, rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>) 100%);"></div><?php }; ?>
  </div>
  <?php }; ?>

  <div id="recipe_archive">

   <?php
        $recipe_category = get_terms( 'category', array( 'hide_empty' => true, 'orderby' => 'id', 'parent' => 0 ) );
        if ( $recipe_category && ! is_wp_error( $recipe_category ) ) :
          foreach ( $recipe_category as $cat ):
            $cat_id = $cat->term_id;
            $custom_fields = get_option( 'taxonomy_' . $cat_id, array() );
            $category_color = "#009fe1";
            if (!empty($custom_fields['main_color'])){
              $category_color = $custom_fields['main_color'];
            }
            if (!empty($custom_fields['image'])){
              $image = wp_get_attachment_image_src( $custom_fields['image'], 'full' );
            }else{
              $image = $b_image;
            }
   ?>
   <article class="item clearfix cat_id_<?php echo esc_html($cat_id); ?>">
    <a class="link animate_background" href="<?php echo esc_url(get_term_link($cat_id,'category')); ?>">
     <p class="category rich_font" style="background:<?php echo esc_attr($category_color); ?>;"><?php echo esc_html($cat->name); ?></p>
     <?php if( !empty($image[0]) ){ ?>
     <div class="image_wrap">
      <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
     </div>
     <?php }; ?>
     <?php if (!empty($cat->description)){ ?>
     <div class="desc">
      <p><span><?php echo wp_kses_post(nl2br($cat->description)); ?></span></p>
      <div class="blur_image" <?php if( !empty($custom_fields['image']) ) { ?>style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"<?php }; ?>></div>
     </div>
     <?php }; ?>
    </a>
   </article>
   <?php
          endforeach;
        endif; // has term
   ?>

  </div><!-- END #blog_archive -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
<?php
/**
* Template Name: カテゴリ別人気記事
*
*/

add_action('wp_head',function(){ ?>
<style type="text/css">
.design_headline_cate-rank:before {
    background: #ff7f00;
    font-family: 'design_plus';
    content: '\e926';
    font-size: 23px;
    line-height: 62px;
}
.design_headline_cate-rank:after {
    border-color: #ff7f00 transparent transparent transparent;
}
.nk-category-rnak-num {
    position: absolute;
    left: 10px;
    top: 10px;
    background: #ff7f00;
    color: #fff;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    border-radius: 50%;
    width: 1.8em;
    height: 1.8em;
    z-index: 9;
    font-size: 17px;
}
.nk_category_all_link{
    text-align: right;
    padding: 20px 10px;
    font-size: 1.2em;
}
</style>
<?php });

get_header();

?>

<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">
<?php
$recipe_category = get_terms( 'recipe_category', array( 'hide_empty' => true, 'orderby' => 'term_order', 'parent' => 0 ,'exclude' => 3257) );
if ( $recipe_category && ! is_wp_error( $recipe_category ) ) :
  foreach ( $recipe_category as $r_cat ):
    $cat_id = $r_cat->term_id;
    $rank = 1;
?>
<h3 class="design_headline design_headline_cate-rank clearfix rich_font">人気の<?php echo esc_html($r_cat->name) ?>レシピTOP5</h3>
<div class="index_popular_recipe cb_contents">
 <?php
      $range = 'day';
      $args = array( 'post_type' => 'recipe', 'posts_per_page' => 5 , 'tax_query' => [
        [
          'taxonomy' => 'recipe_category',
          'field'    => 'term_id',
          'terms'    => $cat_id,
        ]
      ]);
      $popular_post_list = get_posts_views_ranking( $range, $args, 'WP_Query' );
      if($popular_post_list->have_posts()):
 ?>
 <div class="recipe_list clearfix">
  <?php
       while($popular_post_list->have_posts()): $popular_post_list->the_post();
         $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
         $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
         if(has_post_thumbnail()) {
           $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
         } elseif($options['no_image2']) {
           $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
         } else {
           $image = array();
           $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
         }
         $recipe_category = wp_get_post_terms( $post->ID, 'recipe_category' ,array( 'orderby' => 'term_order' ));
         if ( $recipe_category && ! is_wp_error($recipe_category) ) {
           foreach ( $recipe_category as $cat ) :
             if($cat->parent == 0) {
               $cat_name = $cat->name;
               $cat_id = $cat->term_id;
               $term_meta = get_option( 'taxonomy_' . $cat_id, array() );
               $category_color = "#009fe1";
               if (!empty($term_meta['main_color'])){
                 $category_color = $term_meta['main_color'];
               }
               break;
             }
           endforeach;
         };
  ?>
  <article class="item<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>">
    <span class="nk-category-rnak-num"><?php echo $rank ?></span>
   <a class="link animate_background" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
    <div class="image_wrap">
     <?php if($recipe_category && $show_category) { ?><p class="category" style="background:<?php echo esc_attr($category_color); ?>;"><?php echo esc_html($cat_name); ?></p><?php }; ?>
     <?php if($premium_recipe && $show_premium) { ?><div class="premium_icon"></div><?php }; ?>
     <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
    </div>
   </a>
   <div class="title_area">
    <h3 class="title"><a href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>"><span><?php the_title(); ?></span></a></h3>
    <?php if ($show_post_view){ ?><p class="post_meta"><?php if($recipe_type != 'type2') { _e('Hits:', 'tcd-w'); } else { _e('Views:', 'tcd-w'); }; ?><?php the_post_views(); ?></p><?php }; ?>
   </div>
  </article>
  <?php $rank++; endwhile; ?>
 </div><!-- END .recipe_list -->
 <?php endif; wp_reset_query(); ?>
 <div class="nk_category_all_link"><a href="<?php echo get_term_link( $cat_id, 'recipe_category' ) ?>">＞＞すべての<?php echo esc_html($r_cat->name) ?>レシピはこちら</a></div>
</div><!-- END .index_popular_recipe -->
<?php endforeach; ?>
<?php endif; ?>

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>

<?php //インデックス一覧
/**
 * Cocoon WordPress Theme
 * @author: yhira
 * @link: https://wp-cocoon.com/
 * @license: http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 */
if ( !defined( 'ABSPATH' ) ) exit;
if (empty($_GET['s_type'])){
  header("Location: "."http://{$_SERVER['HTTP_HOST']}");
  exit();
}

add_action( 'wp_enqueue_scripts', 'nk_theme_enqueue_styles_s_type' );


$lists_post_ids = [];
$post_ids = [];

$type = 'all';
if (isset($_GET['type'])) {
	$l_type = $_GET['type'];
	if ($l_type != 'all') {
		if(filter_var($l_type, FILTER_VALIDATE_INT) !== false){
			$type = $l_type;
		}
	}
}

$s_type = 'menu';
$s_type_str = 'メニューから探す';
if ($_GET['s_type']=='ingredients') {
	$s_type = 'ingredients';
	$s_type_str = '材料から探す';
}
$headline = 'NO DATA';

if ( $type != 'all') {

	$page = 1;
	if(isset($_GET['paged'])){
	  $page = $_GET['paged'];
	} else {
	  $elms = explode('/', $_SERVER['REQUEST_URI']);
	  if($elms[1] == 'page'){
	    $page = (int)$elms[2];
	  }
	}
	if(filter_var($page, FILTER_VALIDATE_INT) === false){
		$page  = 1;
	}

	$s_lists = [];
	if ($s_type=='menu') {
		$l_menus = nk_type_menus_all();
		foreach ($l_menus as $k => $v) {
			foreach ($v['lists'] as $k2 => $v2) {
				if (isset($v2['s'])) {
					$s_lists['s'.($k2)] = [
						'n' => $v2['n'],
						's' => $v2['s'],
						's_no' => isset($v2['s_no'])?$v2['s_no']:[],
					];
				}
				foreach ($v2['lists'] as $k3 => $v3) {
					$s_lists['s'.($k2+$k3)] = [
						'n' => ($v3['n']=='その他')?$v2['n'].' '.$v3['n']:$v3['n'],
						's' => $v3['s'],
						's_no' => isset($v3['s_no'])?$v3['s_no']:[],
					];
				}
			}
		}
		$l_menus2 = nk_type_menus();
		foreach ($l_menus2 as $k => $v) {
			if ($k == 'all') {
				continue;
			}
			if ($v['n']!='') {
				$s_lists['s'.($k)] = [
					'n' => $v['n'],
					's' => $v['s']
				];
			}
		}
	}else{
		$l_ingredients = nk_type_ingredients();
		foreach ($l_ingredients as $k => $v) {
			if ($k == 'all') {
				continue;
			}

			$l_s_all = [];

			$ll_no_s_all = [];

			foreach ($v['lists'] as $k2 => $v2) {
				$l_s = isset($v2['s']) ? $v2['s'] : [$v2['n']];
				foreach ($l_s as $k3 => $v3) {
					$l_s_all[] = $v3;
				}
				$l_s_no = isset($v2['s_no']) ? $v2['s_no'] :[];
				foreach ($l_s_no as $k3 => $v3) {
					$ll_no_s_all[] = $v3;
				}
				$s_lists['s'.($k+$k2)] = [
					's' => $l_s,
					's_no' => $l_s_no,
					'n' => $v2['n'],
				];
			}

			if (count($l_s_all)>0) {

				$l_no_s_all = [];
				foreach ($ll_no_s_all as $k2 => $v2) {

					if (in_array($v2, $l_s_all , true)) {
						continue;
					}

				    $llv_kana = mb_convert_kana($v2,"cH");
					if (in_array($llv_kana, $l_s_all , true)) {
						continue;
					}

				    $llv = mb_convert_kana($v2,"KVC");
					if (in_array($llv, $l_s_all , true)) {
						continue;
					}
					$l_no_s_all[] = $v2;
				}
				$s_lists['s'.($k)] = [
					'n'=> $v['n'],
					's_no' => $l_no_s_all,
					's' => $l_s_all
				];
			}
		}
	}

	if (isset($s_lists['s'.$type])){
		$my_type = $s_lists['s'.$type];

		$headline = '「'.$my_type['n'].'」';

		global $wpdb;


		$_keyword_array = [];

		foreach ($my_type['s'] as $k => $v) {
		 	$lv = trim($v);
			$lv2 = [];
		  	if ($lv != '') {
			    $lv2[] = $lv;
			    $llv_kana = mb_convert_kana($v,"cH");
			    if ($lv !== $llv_kana) {
			      $lv2[] = $llv_kana;
			    }
			    $llv = mb_convert_kana($v,"KVC");
			    if ($lv !== $llv) {
			      $lv2[] = $llv;
			    }
				$_keyword_array[] = $lv2;
			}
		}

		$_no_keyword_array = [];
		if (isset($my_type['s_no'])) {
			foreach ($my_type['s_no'] as $k => $v) {
			 	$lv = trim($v);
			  	if ($lv != '') {
				    $_no_keyword_array[] = $lv;
				    $llv_kana = mb_convert_kana($v,"cH");
				    if ($lv !== $llv_kana) {
				      $_no_keyword_array[] = $llv_kana;
				    }
				    $llv = mb_convert_kana($v,"KVC");
				    if ($lv !== $llv) {
				      $_no_keyword_array[] = $llv;
				    }
				}
			}
		}



		if ($s_type=='menu') {
			$base_sql = "SELECT distinct object_id FROM wp_terms as a INNER JOIN wp_term_taxonomy as b ON `a`.`term_id` = `b`.`term_id` INNER JOIN wp_term_relationships as c ON `a`.`term_id` = `c`.`term_taxonomy_id` INNER JOIN wp_posts as p ON `p`.`ID` = `c`.`object_id` WHERE ( `b`.taxonomy='wprm_course' )";
		}else{
			$base_sql = "SELECT post_id FROM wp_postmeta INNER JOIN wp_posts ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` WHERE ( meta_key = 'wprm_ingredients' )";	
		}


		$base_sql .= " AND ";

		foreach($_keyword_array as $k => $v){

			if (count($v)===0) {
		    	continue;
		  	}

			$sql = $base_sql;

			$sql .= ' ( ';

			if ($s_type == 'menu') {
				$i = 0;
				foreach ($v as $k2 => $v2) {
			    	$lv = explode('&&',$v2);
			    	$l_terms = get_terms([
			    		'taxonomy' => 'wprm_course',
			    		'fields' => 'ids',
			    		'name' => $lv,
			    	]);
			    	if (count($lv)==count($l_terms)) {
					    if($i!=0){
					      $sql .=  ' OR';
					    }
			    		$sql .= "( ( SELECT COUNT(1) FROM wp_term_relationships WHERE term_taxonomy_id IN (".implode(',', $l_terms).") AND object_id = p.ID ) = ".count($l_terms)." ) ";
			    		$i++;
			    	}
			    }
			}else{

				foreach ($v as $k2 => $v2) {
				    if($k2!=0){
				      $sql .=  ' OR';
				    }
					$sql .= "( meta_value LIKE '%\"".$wpdb->esc_like( $v2 )."%' )";
				}
			}

		  	$sql .= ' ) ';

			$l_no_lists = [];
			if (count($_no_keyword_array) > 0 && $type == 'menu') {
			  	$no_lists_sql = $base_sql;
			  	$no_lists_sql .= ' AND ';
				$no_lists_sql .= ' ( ';
			    foreach ($_no_keyword_array as $k2 => $v2) {
				    if($k2!=0){
				      $no_lists_sql .=  ' OR';
				    }
				    if ($s_type=='menu') {
					    $no_lists_sql .= " a.name = '".$v2."' ";
					}else{
	    				$no_lists_sql .= " meta_value LIKE '%".$wpdb->esc_like( $v2 )."%' ";
					}
			    }
			 	$no_lists_sql .= ' ) ';
			  	$l_results = $wpdb->get_results($no_lists_sql);
			  	foreach($l_results as $r){
				  	if ($s_type=='menu') {
					    $l_id = (int)$r->object_id;
					}else{
		    			$l_id = (int)$r->post_id;
					}
					$l_no_lists[] = $l_id;
				}
			}

		  	$results = $wpdb->get_results($sql);

		  	foreach($results as $r){
		  		if ($s_type=='menu') {
			    	$l_id = (int)$r->object_id;
				}else{
    				$l_id = (int)$r->post_id;
				}
				if (in_array($l_id, $l_no_lists , true)) {
					continue;
				}
		    	if(!in_array($l_id, $lists_post_ids[$k]?:[] ,true)){
		      		$lists_post_ids[$k][] = $l_id;
		    	}
		  	}
		}
		

		foreach($_keyword_array as $k => $v){
		  if (count($v)===0) {
		    continue;
		  }
		  $l = $lists_post_ids[$k]?:[];
		  if (count($l) > 0) {
		    $results = $wpdb->get_results("SELECT meta_value FROM wp_postmeta INNER JOIN wp_posts ON `wp_posts`.`ID` = `wp_postmeta`.`meta_value` WHERE post_id IN (". join(',', $l) .") AND meta_key='wprm_parent_post_id'");
		    foreach($results as $r){
		      $l_id = (int)$r->meta_value;
		      if(!in_array($l_id, $lists_post_ids[$k]?:[] ,true)){
		        $lists_post_ids[$k][] = $l_id;
		      }
		    }
		  }
		}

		foreach ($lists_post_ids as $k => $v) {
		    foreach ($v as $k2 => $v2) {
		      	if (!in_array($v2, $post_ids , true)) {
		       		$post_ids[] = $v2;
	      		}
	    	}
	  	}

		$custom_query = null;
		if (count($post_ids) > 0) {
		  $v_args = array(
		    'post_type' => array( 'recipe' ),
		    'orderby' => 'ASC',
		    'posts_per_page' => 10,
		    'paged' => $page
		  );
		  if (count($post_ids) > 0) {
		    $v_args['post__in'] = $post_ids;
		  }
		  $custom_query = new WP_Query($v_args);
		}
	}

}



?><?php get_header(); ?>

<div id="bread_crumb">
<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="home"><a itemprop="item" href="<?php echo esc_url(home_url('/')); ?>"><span itemprop="name"><?php _e('Home', 'tcd-w'); ?></span></a><meta itemprop="position" content="1"></li>
 <?php if ($type != 'all') { ?>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><a href="/?s_type=<?php echo esc_html($s_type) ?>&type=all"><?php echo $s_type_str ?></a></span><meta itemprop="position" content="2"></li>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo esc_html($headline); ?></span><meta itemprop="position" content="3"></li>
<?php }else{ ?>
 <li class="last" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo $s_type_str ?></span><meta itemprop="position" content="2"></li>
<?php } ?>
</ul>
</div>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">


<?php if ($type == 'all') { ?>
<?php get_template_part('template-parts/search-s_type-all'); ?>
<?php }else{ ?>


  <h3 class="design_headline design_headline_nks clearfix rich_font"><span class="search_keyword"><?php echo esc_html($headline); ?></span></h3>

  <div id="blog_archive">
   <?php if ( $custom_query && $custom_query->have_posts() ) : ?>

   <div id="post_list" class="clearfix">
    <?php
         global $post;
         while ( $custom_query->have_posts() ) :
          $custom_query->the_post();
           $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
           if( get_post_meta($post->ID,'premium_post',true) ){
             $premium_recipe = get_post_meta($post->ID,'premium_post',true);
           }
           $post_type = $post->post_type;
           if($post_type == 'post' && $options['all_premium_post']){
             $premium_recipe = '1';
           }
           if(has_post_thumbnail()) {
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
           } elseif($options['no_image2']) {
             $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
           } else {
             $image = array();
             $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
           }

        /*
        $time_str = '';
        if (class_exists('WPRM_Recipe_Manager')) {
          $results = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_value='".$post->ID."' AND meta_key='wprm_parent_post_id'");
          if (is_array($results) && isset($results[0])) { 
            $recipe = WPRM_Recipe_Manager::get_recipe( $results[0]->post_id );
            $time_str = $recipe->cook_time_formatted();
          }
        }*/

        $recipe_time_label = get_post_meta($post->ID, 'recipe_time_label', true);
        if(empty($recipe_time_label)){
          $recipe_time_label = __('Cooking time', 'tcd-w');
        }
        $recipe_time = get_post_meta($post->ID, 'recipe_time', true);
        $recipe_price_label = get_post_meta($post->ID, 'recipe_price_label', true);
        if(empty($recipe_price_label)){
          $recipe_price_label = __('Estimated cost', 'tcd-w');
        }
        $recipe_price = get_post_meta($post->ID, 'recipe_price', true);
        
    ?>
    <article class="item clearfix<?php if( $post_type == 'post' ){ echo ' premium_post'; }; ?>">
     <a class="link animate_background<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
      <div class="image_wrap">
       <?php if($premium_recipe) { ?><div class="premium_icon"></div><?php }; ?>
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
      <div class="title_area">
       <h3 class="title rich_font"><?php the_title(); ?></h3>
       <?php if($recipe_time || $recipe_price){ ?>
       <ul class="nk_meta_recipe clearfix">
        <?php if($recipe_time){ ?>
        <li><?php echo esc_html($recipe_time_label); ?> <?php echo esc_html($recipe_time); ?></li>
        <?php }; ?>
        <?php if($recipe_price){ ?>
        <li><?php echo esc_html($recipe_price_label); ?> <?php echo esc_html($recipe_price); ?></li>
        <?php }; ?>
       </ul>
       <?php } ?>
       <?php if ( $options['show_archive_blog_date'] || $options['show_archive_blog_category'] ){ ?>
       <ul class="post_meta clearfix">
        <?php if ( $options['show_archive_blog_date'] ){ ?><li class="date"><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time></li><?php }; ?>
       </ul>
       <?php }; ?>
      </div>
     </a>
    </article>
    <?php endwhile; ?>
   </div><!-- END .post_list2 -->


   <?php 
    global $wp_query;
    $old_wp_query = $wp_query;
    $wp_query = $custom_query;
   ?>
   <?php get_template_part('template-parts/navigation'); ?>
   <?php 
    $wp_query = $old_wp_query;
   ?>

   <?php else: ?>

   <p id="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></p>

   <?php endif; ?>
  </div><!-- END #blog_archive -->
<?php } ?>


 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
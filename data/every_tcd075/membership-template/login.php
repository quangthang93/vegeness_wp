<?php
global $dp_options, $tcd_membership_vars;

$has_sidebar = tcd_membership_memberpage_has_sidebar();

get_header();
get_template_part( 'template-parts/breadcrumb' );
?>
<div id="main_contents" class="clearfix<?php if ( ! $has_sidebar ) echo ' noside'; ?>">
 <div id="main_col" class="clearfix">
  <article id="article" class="page memberpage">
<?php
tcd_membership_login_form();
?>
  </article>
 </div>
<?php if ( $has_sidebar ) get_sidebar(); ?>
</div>
<?php get_footer(); ?>

<?php
if ( ! current_user_can( 'read' ) ) {
  wp_safe_redirect( home_url() );
  exit;
}

// マイページ用
function mypage_paginate_links( $paged_string, $current_page, $max_num_pages, $echo = true ) {
	$current_page = absint( $current_page );
	$max_num_pages = absint( $max_num_pages );
	$output = null;

	if ( ! $paged_string || 2 > $max_num_pages ) return;

	$paginate_links = paginate_links( array(
		'base' => add_query_arg( $paged_string, '%#%', get_tcd_membership_memberpage_url( 'mypage' ) ),
		'format' => '',
		'total' => $max_num_pages,
		'show_all' => true,
		'prev_next' => true,
		'current' => $current_page,
		'type' => 'list',
		'prev_text' => '<span>&laquo;</span>',
		'next_text' => '<span>&raquo;</span>'
	));

	if ( $paginate_links ) {
		$output = '<div class="page_navi clearfix">'. "\n" . $paginate_links . "\n</div>\n";
	}

	if ( $echo ) {
		echo $output;
	} else {
		return $output;
	}
}

get_header();
get_template_part( 'template-parts/breadcrumb' );

$options = get_design_plus_option();
$user = wp_get_current_user();
$user_email = $user->user_email;
$user_id = $user->ID;
if ( ! empty( $user->display_name ) ) {
	$user_name = $user->display_name;
} else {
	$user_name = $user->user_nicename;
}

if ( ! empty( $_POST['edit_account'] ) || isset( $_GET['account_message'] ) || ! empty( $_POST['delete_account'] )  ) {
	$active_tab = 'edit_account';
} elseif ( isset( $_GET['like_page'] ) ) {
	$active_tab = 'like';
} else {
	$active_tab = 'news';
}
?>
<div id="main_contents" class="clearfix<?php if ( ! tcd_membership_memberpage_has_sidebar() ) echo ' noside'; ?>">

 <div id="main_col" class="clearfix">

  <?php // ヘッダー -------------------------- ?>
  <div id="my_account_header">
   <div class="user_info">
    <div class="avatar"><?php echo wp_kses_post( get_avatar( $user_id, 300 ) ); ?></div>
    <div class="name"><?php echo esc_html( $user_name ); ?></div>
   </div>
   <a class="logout" href="<?php echo get_tcd_membership_memberpage_url( 'logout' ); ?>"><?php _e( 'Logout', 'tcd-w' ); ?></a>
  </div>

  <?php // コンテンツ -------------------------- ?>
  <div id="my_account_content">

   <?php // タブ -------------------------- ?>
   <ul id="my_account_content_tab" class="clearfix">
    <li<?php if ( 'news' === $active_tab ) echo ' class="active"'; ?>><a href="#my_account_news"><?php echo esc_html( $options['account_news_label'] ); ?></a></li>
    <li<?php if ( 'like' === $active_tab ) echo ' class="active"'; ?>><a href="#my_account_like"><?php echo esc_html( $options['account_like_label'] ) ?></a></li>
    <li<?php if ( 'edit_account' === $active_tab ) echo ' class="active"'; ?>><a href="#my_account_edit"><?php _e( 'Account', 'tcd-w' ); ?></a></li>
   </ul>

   <?php // お知らせ -------------------------- ?>
   <div id="my_account_news" class="my_account_content" <?php if ( 'news' === $active_tab ) echo 'style="display:block;"'; ?>>
    <?php // バナー---------- ?>
    <?php if ( $options['account_news_ad_code1'] || $options['account_news_ad_image1'] || $options['account_news_ad_code2'] || $options['account_news_ad_image2'] ) { ?>
    <div class="banner_area clearfix">
     <?php if ( $options['account_news_ad_code1' ]) { ?>
     <div class="banner banner_left">
      <?php echo $options['account_news_ad_code1']; ?>
     </div>
     <?php } else { ?>
     <?php $single_image1 = wp_get_attachment_image_src( $options['account_news_ad_image1'], 'full' ); ?>
     <div class="banner banner_left">
      <a href="<?php echo esc_url( $options['account_news_ad_url1'] ); ?>" target="_blank"><img src="<?php echo esc_attr( $single_image1[0] ); ?>" alt="" title="" /></a>
     </div>
     <?php }; ?>
     <?php if ( $options['account_news_ad_code2'] ) { ?>
     <div class="banner banner_right">
      <?php echo $options['account_news_ad_code2']; ?>
     </div>
     <?php } else { ?>
     <?php $single_image2 = wp_get_attachment_image_src( $options['account_news_ad_image2'], 'full' ); ?>
     <div class="banner banner_right">
      <a href="<?php echo esc_url( $options['account_news_ad_url2'] ); ?>" target="_blank"><img src="<?php echo esc_attr( $single_image2[0] ); ?>" alt="" title="" /></a>
     </div>
     <?php }; ?>
    </div><!-- END #banner_area -->
    <?php }; ?>
    <?php // お知らせ一覧 ---------- ?>
    <?php
         $member_news_paged = isset( $_GET['news_page'] ) ? absint( $_GET['news_page'] ) : 1;
         $args = array( 'post_type' => 'member_news', 'posts_per_page' => $options['account_news_list_num'], 'paged' => $member_news_paged );
         $member_news_query = new WP_Query( $args );
         if ( $member_news_query->have_posts() ) :
    ?>
    <ol class="news">
     <?php
          while ( $member_news_query->have_posts() ) :
            $member_news_query->the_post();
     ?>
     <li><p class="date"><?php the_time( 'Y.m.d' ); ?></p><div class="title post_content clearfix"><?php the_content( null, false ); ?></div></li>
     <?php
          endwhile;
          wp_reset_postdata();
     ?>
    </ol>
    <?php
           $paginate_links = mypage_paginate_links( 'news_page', $member_news_paged, $member_news_query->max_num_pages, false );
           if ( $paginate_links ) :
             // 1ページ目の?news_page=1を削除
             $paginate_links = str_replace( array( '?news_page=1', '&#038;news_page=1' ), '', $paginate_links );
	         echo $paginate_links;
           endif;

         else :
    ?>
    <p class="no_post"><?php _e( 'There are no post registered as news.', 'tcd-w' ); ?></p>
    <?php
         endif;
    ?>
   </div>

   <?php // お気に入り -------------------------- ?>
   <div id="my_account_like" class="my_account_content" <?php if ( 'like' === $active_tab ) echo 'style="display:block;"'; ?>>
    <?php
         $like_paged = isset( $_GET['like_page'] ) ? absint( $_GET['like_page'] ) : 1;
         $args = array( 'post_type' => 'recipe', 'posts_per_page' => $options['account_like_list_num'], 'paged' => $like_paged );
         $recipe_query = get_user_liked_posts( $user_id, $args, 'WP_Query' );
         if ( $recipe_query->have_posts() ) :
    ?>
    <div class="recipe_list2 clearfix">
     <?php
          while( $recipe_query->have_posts() ) :
            $recipe_query->the_post();
            if ( has_post_thumbnail() ) {
              $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
            } elseif( $options['no_image2'] ) {
              $image = wp_get_attachment_image_src( $options['no_image1'], 'full' );
            } else {
              $image = array();
              $image[0] = get_bloginfo('template_url') . "/img/common/no_image1.gif";
            }
     ?>
     <article class="item">
      <a class="link animate_background" href="<?php the_permalink(); ?>">
       <div class="image_wrap">
        <div class="image" style="background:url(<?php echo esc_attr( $image[0] ); ?>) no-repeat center center; background-size:cover;"></div>
       </div>
       <h3 class="title"><span><?php the_title(); ?></span></h3>
      </a>
      <a class="delete remove-like" href="#" data-post-id="<?php the_ID(); ?>"><?php _e('Delete', 'tcd-w'); ?></a>
     </article>
     <?php endwhile; ?>
    </div><!-- END .recipe_list2 -->
    <?php mypage_paginate_links( 'like_page', $like_paged, $recipe_query->max_num_pages, true ); ?>
    <?php else: ?>
    <p class="no_post"><?php _e( 'There are no post registered as favorites.', 'tcd-w' ); ?></p>
    <?php endif; wp_reset_postdata(); ?>
   </div>

   <?php // アカウントの編集 -------------------------- ?>
   <div id="my_account_edit" class="my_account_content" <?php if ( 'edit_account' === $active_tab ) echo 'style="display:block;"'; ?>>
<?php tcd_membership_edit_account_form(); ?>
   </div>

  </div><!-- END #my_account_content -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>

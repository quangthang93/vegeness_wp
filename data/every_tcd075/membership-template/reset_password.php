<?php
global $dp_options, $tcd_membership_vars;

$has_sidebar = tcd_membership_memberpage_has_sidebar();

get_header();
get_template_part( 'template-parts/breadcrumb' );
?>
<div id="main_contents" class="clearfix<?php if ( ! $has_sidebar ) echo ' noside'; ?>">
 <div id="main_col" class="clearfix">
  <article id="article" class="page memberpage">
<?php
// 完了画面
if ( ! empty( $tcd_membership_vars['reset_password']['complete'] ) && ! empty( $tcd_membership_vars['reset_password']['message'] ) ) :
?>
   <div class="post_content clearfix">
    <div class="form-message"><?php echo wpautop( $tcd_membership_vars['reset_password']['message'] ); ?></div>
   </div>
<?php
// 新しいパスワード入力フォーム表示
elseif ( ! empty( $tcd_membership_vars['reset_password']['token'] ) ) :
?>

    <div class="password_form_wrap form_wrap">
     <div class="password_form_area">
      <form id="reset-password-form" class="membership-form" action="<?php echo esc_attr( get_tcd_membership_memberpage_url( 'reset_password' ) ); ?>" method="post">
       <h2 class="headline"><?php _e( 'Rest password', 'tcd-w' ); ?></h2>
<?php
	if ( ! empty( $tcd_membership_vars['reset_password']['message'] ) ) :
?>
       <div class="form-message"><?php echo wpautop( $tcd_membership_vars['reset_password']['message'] ); ?></div>
<?php
	endif;
	if ( ! empty( $tcd_membership_vars['reset_password']['error_message'] ) ) :
?>
       <div class="form-error"><?php echo wpautop( $tcd_membership_vars['reset_password']['error_message'] ); ?></div>
<?php
	endif;
?>
       <p><?php _e( 'Please enter new password.', 'tcd-w' ); ?></p>
       <div class="password new_pass1">
        <input class="input_field" type="password" name="new_pass1" value="" placeholder="<?php _e( 'New Password', 'tcd-w' ); ?>" minlength="8" required>
       </div>
       <div class="password new_pass2">
        <input class="input_field" type="password" name="new_pass2" value="" placeholder="<?php _e( 'New Password Confirmation', 'tcd-w' ); ?>" required>
       </div>
       <div class="submit">
        <input type="submit" value="<?php _e( 'Set new password', 'tcd-w' ); ?>">
        <input type="hidden" name="nonce" value="<?php echo esc_attr( wp_create_nonce( 'tcd-membership-reset_password' ) ); ?>">
        <input type="hidden" name="token" value="<?php echo esc_attr( $tcd_membership_vars['reset_password']['token'] ); ?>">
       </div>
      </form>
     </div>
    </div>
<?php
// メールアドレス入力フォーム表示
else :
	tcd_membership_reset_password_form();
endif;
?>
  </article>
 </div>
<?php if ( $has_sidebar ) get_sidebar(); ?>
</div>
<?php get_footer(); ?>

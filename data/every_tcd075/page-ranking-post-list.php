<?php
/*
Template Name:Ranking page
*/
__('Ranking page', 'tcd-w');
?>
<?php
     get_header();
     $options = get_design_plus_option();
?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <div id="ranking_recipe">

   <?php
        $headline = get_post_meta($post->ID, 'ranking_headline', true);
        $desc = get_post_meta($post->ID, 'ranking_desc', true);
   ?>
   <?php if($headline) { ?><h3 class="design_headline clearfix<?php if(get_post_meta($post->ID, 'ranking_headline_hide_icon', true)) { echo ' hide_icon'; }; ?>"><span class="title rich_font"><?php echo esc_html($headline); ?></span></h3><?php }; ?>
   <?php if($desc){ ?><p class="recipe_list_desc"><?php echo wp_kses_post(nl2br($desc)); ?></p><?php }; ?>

   <?php
        $show_category = get_post_meta($post->ID, 'ranking_list_show_category', true);
        $show_premium_icon = get_post_meta($post->ID, 'ranking_list_show_premium_icon', true);
        $range = get_post_meta($post->ID, 'ranking_list_range', true);
        $post_num = get_post_meta($post->ID, 'ranking_list_num', true);
        if(empty($post_num)){
          $post_num = '10';
        }
        $ranking_color = get_post_meta($post->ID, 'ranking_list_color', true);
        if(empty($ranking_color)){
          $ranking_color = '#ff7f00';
        }
        $args = array( 'post_type' => 'recipe', 'posts_per_page' => $post_num );
        $ranges = array( 1 => 'daily', 2 => 'weekly', 3 => 'monthly', 4 => 'yearly' );
   ?>
   <ul id="ranking_list_tab" class="clearfix">
    <li class="active"><a href="#ranking_list-daily"><?php echo esc_html($post->ranking_tab_label1 ? $post->ranking_tab_label1 : __('Daily', 'tcd-w')); ?></a></li>
    <li><a href="#ranking_list-weekly"><?php echo esc_html($post->ranking_tab_label2 ? $post->ranking_tab_label2 : __('Weekly', 'tcd-w')); ?></a></li>
    <li><a href="#ranking_list-monthly"><?php echo esc_html($post->ranking_tab_label3 ? $post->ranking_tab_label3 : __('Monthly', 'tcd-w')); ?></a></li>
    <li><a href="#ranking_list-yearly"><?php echo esc_html($post->ranking_tab_label4 ? $post->ranking_tab_label4 : __('Yearly', 'tcd-w')); ?></a></li>
   </ul>

   <?php
        foreach ( $ranges as $key => $range ) :
          $popular_post_list = get_posts_views_ranking( $range, $args, 'WP_Query' );
   ?>
   <div id="ranking_list-<?php echo $range; ?>" class="ranking_list ranking_list_tab_content clearfix"<?php if ($key==1) echo ' style="display:block;"'; ?>>

    <?php
         if($popular_post_list->have_posts()):
           $i = 1;
           while($popular_post_list->have_posts()): $popular_post_list->the_post();
             $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
             $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
             if(has_post_thumbnail()) {
               $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
             } elseif($options['no_image2']) {
               $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
             } else {
               $image = array();
               $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
             }
             $recipe_category = wp_get_post_terms( $post->ID, 'recipe_category' ,array( 'orderby' => 'term_order' ));
             if ( $recipe_category && ! is_wp_error($recipe_category) ) {
               foreach ( $recipe_category as $cat ) :
                 if($cat->parent == 0) {
                   $cat_name = $cat->name;
                   $cat_id = $cat->term_id;
                   $term_meta = get_option( 'taxonomy_' . $cat_id, array() );
                   $category_color = "#009fe1";
                   if (!empty($term_meta['main_color'])){
                     $category_color = $term_meta['main_color'];
                   }
                   break;
                 }
               endforeach;
             };
    ?>
    <article class="item">
     <a class="link animate_background<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
      <?php if($recipe_category && $show_category) { ?><p class="category" style="background:<?php echo esc_attr($category_color); ?>;"><?php echo esc_html($cat_name); ?></p><?php }; ?>
      <div class="image_wrap">
       <?php if($premium_recipe && $show_premium_icon) { ?><div class="premium_icon"></div><?php }; ?>
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
      <div class="title_area">
       <p class="rank" style="color:<?php echo esc_attr($ranking_color); ?>;"><?php printf(__('Rank %s', 'tcd-w'),$i); ?></p>
       <h3 class="title"><span><?php the_title(); ?></span></h3>
      </div>
     </a>
    </article>
    <?php $i++; endwhile; ?>
   <?php else: ?>

     <p id="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></p>

    <?php endif; ?>

   </div><!-- END .ranking_list -->
   <?php endforeach; wp_reset_postdata(); ?>

  </div><!-- END #ranking_recipe -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
<?php
/*
Template Name:No sidebar page
*/
__('No sidebar page', 'tcd-w');
?>
<?php
     get_header();
     $options = get_design_plus_option();
?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <article id="article" class="page">

   <h2 id="page_title"><?php the_title(); ?></h2>

   <?php // post content ------------------------------------------------------------------------------------------------------------------------ ?>
   <div class="post_content clearfix">
    <?php
         the_content();
         if ( ! post_password_required() ) {
           $pagenation_type = get_post_meta($post->ID, 'pagenation_type', true);
           if($pagenation_type == 'type3') {
             $pagenation_type = $options['pagenation_type'];
           };
           if ( $pagenation_type == 'type2' ) {
             if ( $page < $numpages && preg_match( '/href="(.*?)"/', _wp_link_page( $page + 1 ), $matches ) ) :
    ?>
    <div id="p_readmore">
     <a class="button" href="<?php echo esc_url( $matches[1] ); ?>"><?php _e( 'Read more', 'tcd-w' ); ?></a>
     <p class="num"><?php echo $page . ' / ' . $numpages; ?></p>
    </div>
    <?php
             endif;
           } else {
             custom_wp_link_pages();
           }
         }
    ?>
   </div>

  </article><!-- END #article -->

  <?php endwhile; endif; ?>

 </div><!-- END #main_col -->

</div><!-- END #main_contents -->

<?php get_footer(); ?>
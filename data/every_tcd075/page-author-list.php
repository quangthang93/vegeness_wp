<?php
/*
Template Name:Author list page
*/
__('Author list page', 'tcd-w');
?>
<?php
     $options = get_design_plus_option();
     get_header();
     $author_page_name = $options['author_parent_page_name'];
     $headline_color = get_post_meta($post->ID, 'author_list_headline_color', true);
     if(empty($headline_color)) {
       $headline_color = '#ff7f00';
     }
     $desc_color = get_post_meta($post->ID, 'author_list_desc_color', true);
     if(empty($headline_color)) {
       $desc_color = '#FFFFFF';
     }
     $use_overlay = get_post_meta($post->ID, 'author_list_use_overlay', true);
     if($use_overlay) {
       $overlay_color = '#000000';
       $overlay_color = get_post_meta($post->ID, 'author_list_overlay_color', true);
       $overlay_opacity = '0.5';
       $overlay_opacity = get_post_meta($post->ID, 'author_list_overlay_opacity', true);
       $overlay_color = hex2rgb($overlay_color);
       $overlay_color = implode(",",$overlay_color);
     }
     $image_id = get_post_meta($post->ID, 'author_list_image', true);
     if(!empty($image_id)) {
       $image = wp_get_attachment_image_src($image_id, 'full');
     }
     $desc = get_post_meta($post->ID, 'author_list_desc', true);
?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <div id="page_header" <?php if($image_id) { ?>style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"<?php }; ?>>
   <h2 class="headline rich_font" style="background:<?php echo esc_attr($headline_color); ?>;"><span><?php echo esc_html($author_page_name); ?></span></h2>
   <?php if($desc){ ?><p class="desc" style="color:<?php echo esc_attr($desc_color); ?>;"><?php echo wp_kses_post(nl2br($desc)); ?></p><?php }; ?>
   <?php if($use_overlay) { ?><div class="overlay" style="background: -webkit-linear-gradient(top, transparent 50%, rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>) 100%); background: linear-gradient(to bottom, transparent 50%, rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>) 100%);"></div><?php }; ?>
  </div>

  <?php
       $author_list_order = get_post_meta($post->ID, 'author_list_order', true);
       if (empty($author_list_order) || !is_array($author_list_order)) {
         $author_list_order = array();
       }

       $users = get_users(array(
         'fields' => array('ID'),
         'role__not_in' => array('subscriber','contributor'),
         'orderby' => 'ID',
         'order' => 'ASC'
       ));

       if ($users) {
         $user_ids = array();
         foreach ($users as $user) {
           $user_ids[] = $user->ID;
         }

         if ($author_list_order) {
           foreach ($author_list_order as $key => $author_id) {
             if (!in_array($author_id, $user_ids)) {
               unset($author_list_order[$key]);
             }
           }
         }

         foreach ($user_ids as $user_id) {
           if (!in_array($user_id, $author_list_order)) {
             $author_list_order[] = $user_id;
           }
         }

         unset($user_ids, $user_id);
       } else {
         $author_list_order = array();
       }
       unset($users);
       if ($author_list_order) {
         $show_category = get_post_meta($post->ID, 'author_list_show_category', true);
  ?>
  <div id="author_list" class="clearfix">
   <?php
        foreach((array) $author_list_order as $author_id) :
          $user_data = get_userdata($author_id);
          $user_name = $user_data->display_name;
          $sub_title = $user_data->user_sub_title;
          $user_image_id = $user_data->user_image;
          if($user_image_id) {
            $image = wp_get_attachment_image_src( $user_image_id, 'full' );
          }
          $user_desc = $user_data->user_short_desc;
          $author_url = get_author_posts_url($author_id);
          $show_author_list = $user_data->show_author_list;
          if($show_author_list) {
   ?>
   <article class="item">
    <a class="link animate_background" href="<?php if($author_url) { echo esc_url($author_url); }; ?>">
     <?php if($user_image_id) { ?>
     <div class="image_wrap">
      <p class="category rich_font">
       <?php
            $category_ids = $user_data->user_category;
            if($category_ids) {
              $count = array();
              foreach ( $category_ids as $category_id ):
                $category_data = get_term_by('id', $category_id, 'recipe_category');
                if($category_data){
                  $count[] = $category_data->count;
                }
              endforeach;
              $category_ids_new = array_combine($category_ids, $count);
              arsort($category_ids_new);
              foreach ( $category_ids_new as $category_id => $category_post_count):
                $category_data = get_term_by('id', $category_id, 'recipe_category');
                if($category_data){
                  $term_meta = get_option( 'taxonomy_' . $category_id, array() );
                  $category_color = "#009fe1";
                  if (!empty($term_meta['main_color'])){
                    $category_color = $term_meta['main_color'];
                  };
       ?>
       <span style="background:<?php echo esc_attr($category_color); ?>;"><?php echo esc_html($category_data->name); ?></span>
       <?php
                };
              endforeach;
            };
       ?>
      </p>
      <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
     </div>
     <?php }; ?>
     <div class="title_area">
      <?php if($user_name) { ?>
      <h3 class="title rich_font"><?php echo esc_html($user_name); ?><?php if($sub_title) { ?><span><?php echo esc_html($sub_title); ?></span><?php }; ?></h3>
      <?php }; ?>
      <?php if($user_desc) { ?>
      <p class="desc"><span><?php echo wp_kses_post(nl2br($user_desc)); ?></span></p>
      <?php }; ?>
     </div>
    </a>
   </article>
   <?php }; endforeach; ?>
  </div>
  <?php }; ?>

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
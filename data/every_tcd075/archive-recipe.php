<?php
     get_header();
     $options = get_design_plus_option();
     $headline = $options['recipe_headline'];
     $headline_color = $options['recipe_headline_color'];
     $desc = $options['recipe_desc'];
     $desc_color = $options['recipe_desc_color'];
     $image_id = $options['recipe_bg_image'];
     if(!empty($image_id)) {
       $image = wp_get_attachment_image_src($image_id, 'full');
     }
     $use_overlay = $options['recipe_use_overlay'];
     if($use_overlay) {
       $overlay_color = hex2rgb($options['recipe_overlay_color']);
       $overlay_color = implode(",",$overlay_color);
       $overlay_opacity = $options['recipe_overlay_opacity'];
     }
?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <?php if(!is_paged()) { ?>
  <div id="page_header" <?php if($image_id) { ?>style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"<?php }; ?>>
   <?php if($headline){ ?><h2 class="headline rich_font" style="background:<?php echo esc_attr($headline_color); ?>;"><span><?php echo wp_kses_post(nl2br($headline)); ?></span></h2><?php }; ?>
   <?php if($desc){ ?><p class="desc" style="color:<?php echo esc_attr($desc_color); ?>;"><span><?php echo wp_kses_post(nl2br($desc)); ?></span></p><?php }; ?>
   <?php if($use_overlay) { ?><div class="overlay" style="background: -webkit-linear-gradient(top, transparent 50%, rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>) 100%); background: linear-gradient(to bottom, transparent 50%, rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>) 100%);"></div><?php }; ?>
  </div>
  <?php }; ?>

  <div id="recipe_archive">

   <?php
        $recipe_category = get_terms( 'recipe_category', array( 'hide_empty' => true, 'orderby' => 'id', 'parent' => 0 ) );
        if ( $recipe_category && ! is_wp_error( $recipe_category ) ) :
          foreach ( $recipe_category as $cat ):
            $cat_id = $cat->term_id;
            $custom_fields = get_option( 'taxonomy_' . $cat_id, array() );
            $category_color = "#009fe1";
            if (!empty($custom_fields['main_color'])){
              $category_color = $custom_fields['main_color'];
            }
            if (!empty($custom_fields['image'])){
              $image = wp_get_attachment_image_src( $custom_fields['image'], 'full' );
            } elseif($options['no_image2']) {
              $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
            } else {
              $image = array();
              $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
            }
   ?>
   <article class="item clearfix cat_id_<?php echo esc_html($cat_id); ?>">
    <a class="link animate_background" href="<?php echo esc_url(get_term_link($cat_id,'recipe_category')); ?>">
     <p class="category rich_font" style="background:<?php echo esc_attr($category_color); ?>;"><?php echo esc_html($cat->name); ?></p>
     <?php if( !empty($custom_fields['image']) ){ ?>
     <div class="image_wrap">
      <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
     </div>
     <?php }; ?>
     <?php if (!empty($custom_fields['desc'])){ ?>
     <div class="desc">
      <p><span><?php echo wp_kses_post(nl2br($custom_fields['desc'])); ?></span></p>
      <div class="blur_image" <?php if( !empty($custom_fields['image']) ) { ?>style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"<?php }; ?>></div>
     </div>
     <?php }; ?>
    </a>
   </article>
   <?php
          endforeach;
        endif; // has term
   ?>

  </div><!-- END recipe_archive -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
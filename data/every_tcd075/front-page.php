<?php
     $options = get_design_plus_option();
     get_header();
?>
<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

<?php
     // コンテンツビルダー
     if (!empty($options['contents_builder'])) :
       $content_count = 1;
       $is_user_logged_in = current_user_can( 'read' );
       foreach($options['contents_builder'] as $content) :
         if (!isset($content['cb_display'])) $content['cb_display'] = 'type1';

         // ログイン中のみ表示
         if ( 'type2' == $content['cb_display'] && ! $is_user_logged_in ) {
           continue;
         // 未ログインのみ
         } elseif ( 'type3' == $content['cb_display'] && $is_user_logged_in ) {
           continue;
         // 非表示
         } elseif ( 'type4' == $content['cb_display'] ) {
           continue;
         }

         // 新着レシピ --------------------------------------------------------------------------------
         if ($content['cb_content_select'] == 'recent_recipe_list') {
           $headline = $content['recent_headline'];
           $desc = $content['recent_desc'];
?>
<div class="index_recent_recipe cb_contents num<?php echo $content_count; ?>">
 <?php if($headline) { ?>
 <h3 class="design_headline clearfix rich_font<?php if($content['hide_recent_headline_icon']){ echo ' hide_icon'; }; ?>"><?php echo esc_html($headline); ?></h3>
 <?php }; ?>
 <?php if($desc) { ?>
 <p class="desc"><span><?php echo nl2br(esc_html($desc)); ?></span></p>
 <?php }; ?>
 <?php
      $post_num = $content['recent_num'];
      $show_category = $content['recent_show_category'];
      $show_date = $content['recent_show_date'];
      $show_premium = $content['recent_show_premium'];
      $args = array( 'post_type' => 'recipe', 'posts_per_page' => $post_num );
      $recipe_query = new wp_query($args);
      if($recipe_query->have_posts()):
 ?>
 <div class="recipe_list type2 clearfix">
  <?php
       while($recipe_query->have_posts()): $recipe_query->the_post();
         $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
         $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
         if(has_post_thumbnail()) {
           $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
         } elseif($options['no_image2']) {
           $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
         } else {
           $image = array();
           $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
         }
         $recipe_category = wp_get_post_terms( $post->ID, 'recipe_category' ,array( 'orderby' => 'term_order' ));
         if ( $recipe_category && ! is_wp_error($recipe_category) ) {
           foreach ( $recipe_category as $cat ) :
             if($cat->parent == 0) {
               $cat_name = $cat->name;
               $cat_id = $cat->term_id;
               $term_meta = get_option( 'taxonomy_' . $cat_id, array() );
               $category_color = "#009fe1";
               if (!empty($term_meta['main_color'])){
                 $category_color = $term_meta['main_color'];
               }
               break;
             }
           endforeach;
         };
  ?>
  <article class="item<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>">
   <a class="link animate_background" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
    <div class="image_wrap">
     <?php if($recipe_category && $show_category) { ?><p class="category" style="background:<?php echo esc_attr($category_color); ?>;"><?php echo esc_html($cat_name); ?></p><?php }; ?>
     <?php if($premium_recipe && $show_premium) { ?><div class="premium_icon"></div><?php }; ?>
     <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
    </div>
   </a>
   <div class="title_area">
    <h3 class="title"><a href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>"><span><?php the_title(); ?></span></a></h3>
    <?php if ($show_date){ ?><p class="post_meta"><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time></p><?php }; ?>
   </div>
  </article>
  <?php endwhile; ?>
 </div><!-- END .recipe_list -->
 <?php endif; wp_reset_query(); ?>
</div><!-- END .index_recent_recipe -->
<?php
         // 人気のレシピ --------------------------------------------------------------------------------
         } elseif ($content['cb_content_select'] == 'popular_recipe_list') {
           $headline = $content['popular_headline'];
           $desc = $content['popular_desc'];
?>
<div class="index_popular_recipe cb_contents num<?php echo $content_count; ?>">
 <?php if($headline) { ?>
 <h3 class="design_headline clearfix rich_font<?php if($content['hide_popular_headline_icon']){ echo ' hide_icon'; }; ?>"><?php echo esc_html($headline); ?></h3>
 <?php }; ?>
 <?php if($desc) { ?>
 <p class="desc"><span><?php echo nl2br(esc_html($desc)); ?></span></p>
 <?php }; ?>
 <?php
      $post_num = $content['popular_num'];
      $range = $content['popular_range'];
      $show_category = $content['popular_show_category'];
      $show_post_view = $content['popular_show_post_view'];
      $show_premium = $content['popular_show_premium'];
      $args = array( 'post_type' => 'recipe', 'posts_per_page' => $post_num );
      $popular_post_list = get_posts_views_ranking( $range, $args, 'WP_Query' );
      if($popular_post_list->have_posts()):
 ?>
 <div class="recipe_list clearfix">
  <?php
       while($popular_post_list->have_posts()): $popular_post_list->the_post();
         $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
         $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
         if(has_post_thumbnail()) {
           $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
         } elseif($options['no_image2']) {
           $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
         } else {
           $image = array();
           $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
         }
         $recipe_category = wp_get_post_terms( $post->ID, 'recipe_category' ,array( 'orderby' => 'term_order' ));
         if ( $recipe_category && ! is_wp_error($recipe_category) ) {
           foreach ( $recipe_category as $cat ) :
             if($cat->parent == 0) {
               $cat_name = $cat->name;
               $cat_id = $cat->term_id;
               $term_meta = get_option( 'taxonomy_' . $cat_id, array() );
               $category_color = "#009fe1";
               if (!empty($term_meta['main_color'])){
                 $category_color = $term_meta['main_color'];
               }
               break;
             }
           endforeach;
         };
  ?>
  <article class="item<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>">
   <a class="link animate_background" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
    <div class="image_wrap">
     <?php if($recipe_category && $show_category) { ?><p class="category" style="background:<?php echo esc_attr($category_color); ?>;"><?php echo esc_html($cat_name); ?></p><?php }; ?>
     <?php if($premium_recipe && $show_premium) { ?><div class="premium_icon"></div><?php }; ?>
     <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
    </div>
   </a>
   <div class="title_area">
    <h3 class="title"><a href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>"><span><?php the_title(); ?></span></a></h3>
    <?php if ($show_post_view){ ?><p class="post_meta"><?php if($recipe_type != 'type2') { _e('Hits:', 'tcd-w'); } else { _e('Views:', 'tcd-w'); }; ?><?php the_post_views(); ?></p><?php }; ?>
   </div>
  </article>
  <?php endwhile; ?>
 </div><!-- END .recipe_list -->
 <?php endif; wp_reset_query(); ?>
</div><!-- END .index_popular_recipe -->
<?php
         // 特集レシピ --------------------------------------------------------------------------------
         } elseif ($content['cb_content_select'] == 'featured_recipe_list') {
           $headline = $content['featured_headline'];
           $desc = $content['featured_desc'];
?>
<div class="index_featured_recipe cb_contents num<?php echo $content_count; ?>">
 <?php if($headline) { ?>
 <h3 class="design_headline clearfix rich_font<?php if($content['hide_featured_headline_icon']){ echo ' hide_icon'; }; ?>"><?php echo esc_html($headline); ?></h3>
 <?php }; ?>
 <?php if($desc) { ?>
 <p class="desc"><span><?php echo nl2br(esc_html($desc)); ?></span></p>
 <?php }; ?>
 <div class="banner_list">
  <?php
       for($i=1; $i<= 4; $i++):
         $image_id = $content['featured_image'.$i];
         if(!empty($image_id)) {
           $banner_type = $content['featured_banner_type'.$i];
           $image = wp_get_attachment_image_src($image_id, 'full');
           $url = $content['featured_url'.$i];
           if ($content['featured_page'.$i]) {
             $featured_page = get_post($content['featured_page'.$i]);
             if (isset($featured_page->post_status) && 'publish' == $featured_page->post_status) {
               $url = get_permalink($featured_page);
             }
           }
           $target = $content['featured_target'.$i];
           $title = $content['featured_title'.$i];
           $sub_title = $content['featured_sub_title'.$i];
           $font_type = $content['featured_title_font_type'.$i];
           $use_overlay = $content['featured_use_overlay'.$i];
           if($banner_type == 'type1') {
             $color = $content['featured_type1_font_color'.$i];
           } else {
             $color = $content['featured_type2_font_color'.$i];
           }
           $border_color = $content['featured_type1_border_color'.$i];
  ?>
  <a class="link animate_background <?php echo esc_attr($banner_type); ?> num<?php echo $i; ?>" style="border-color:<?php echo esc_attr($border_color); ?>;" href="<?php echo esc_url($url); ?>" <?php if($target == 1) { echo 'target="_blank"'; }; ?>>
   <?php if($title || $sub_title) { ?>
   <div class="title_area" style="color:<?php echo esc_attr($color); ?>;">
    <?php if($sub_title) { ?>
    <p class="sub_title"><span><?php echo nl2br(esc_html($sub_title)); ?></span></p>
    <?php }; ?>
    <?php if($title) { ?>
    <h3 class="title rich_font_<?php echo esc_attr($font_type); ?>"><span><?php echo nl2br(esc_html($title)); ?></span></h3>
    <?php }; ?>
   </div>
   <?php }; ?>
   <?php if($banner_type == 'type2' && $use_overlay == '1') { ?>
   <div class="overlay"></div>
   <?php }; ?>
   <div class="image_wrap">
    <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
   </div>
  </a>
  <?php
         };
       endfor;
  ?>
 </div>
</div><!-- END .index_featured_recipe -->
<?php
         // 新着ブログ --------------------------------------------------------------------------------
         } elseif ($content['cb_content_select'] == 'recent_blog_list') {
           $headline = $content['recent_blog_headline'];
           $desc = $content['recent_blog_desc'];
?>
<div class="index_blog cb_contents num<?php echo $content_count; ?>">
 <?php if($headline) { ?>
 <h3 class="design_headline clearfix rich_font<?php if($content['hide_recent_blog_headline_icon']){ echo ' hide_icon'; }; ?>"><?php echo esc_html($headline); ?></h3>
 <?php }; ?>
 <?php if($desc) { ?>
 <p class="desc"><span><?php echo nl2br(esc_html($desc)); ?></span></p>
 <?php }; ?>
 <?php
      $post_num = $content['recent_blog_num'];
      $show_date = $content['recent_blog_show_date'];
      $show_premium = $content['recent_blog_show_premium'];
      $args = array( 'post_type' => 'post', 'posts_per_page' => $post_num );
      $blog_query = new wp_query($args);
      if($blog_query->have_posts()):
 ?>
 <div class="recipe_list type2 clearfix">
  <?php
       while($blog_query->have_posts()): $blog_query->the_post();
         $premium_post = get_post_meta($post->ID,'premium_post',true);
         if($options['all_premium_post']) {
           $premium_post = '1';
         }
         if(has_post_thumbnail()) {
           $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
         } elseif($options['no_image2']) {
           $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
         } else {
           $image = array();
           $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
         }
  ?>
  <article class="item<?php if(!is_user_logged_in() && $premium_post) { echo ' register_link'; }; ?>">
   <a class="link animate_background" href="<?php if(!is_user_logged_in() && $premium_post) { echo '#'; } else { the_permalink(); }; ?>">
    <div class="image_wrap">
     <?php if($premium_post && $show_premium) { ?><div class="premium_icon"></div><?php }; ?>
     <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
    </div>
   </a>
   <div class="title_area">
    <h3 class="title"><a href="<?php if(!is_user_logged_in() && $premium_post) { echo '#'; } else { the_permalink(); }; ?>"><span><?php the_title(); ?></span></a></h3>
    <?php if ($show_date){ ?><p class="post_meta"><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time></p><?php }; ?>
   </div>
  </article>
  <?php endwhile; ?>
 </div><!-- END .recipe_list -->
 <?php endif; wp_reset_query(); ?>
</div><!-- END .index_blog -->
<?php
         // お知らせ --------------------------------------------------------------------------------
         } elseif ($content['cb_content_select'] == 'news_list') {
           $headline = $content['news_headline'];
           $desc = $content['news_desc'];
?>
<div class="index_news cb_contents num<?php echo $content_count; ?>">
 <?php if($headline) { ?>
 <h3 class="design_headline clearfix rich_font<?php if($content['hide_news_headline_icon']){ echo ' hide_icon'; }; ?>"><?php echo esc_html($headline); ?></h3>
 <?php }; ?>
 <?php if($desc) { ?>
 <p class="desc"><span><?php echo nl2br(esc_html($desc)); ?></span></p>
 <?php }; ?>
 <?php
      $post_num = $content['news_num'];
      $args = array( 'post_type' => 'news', 'posts_per_page' => $post_num );
      $news_query = new wp_query($args);
      if($news_query->have_posts()):
 ?>
 <div class="index_news_list clearfix">
  <?php
       while($news_query->have_posts()): $news_query->the_post();
  ?>
  <article class="item">
   <a class="link clearfix" href="<?php the_permalink(); ?>">
    <p class="date"><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time></p>
    <h3 class="title"><span><?php the_title(); ?></span></h3>
   </a>
  </article>
  <?php endwhile; ?>
 </div><!-- END .index_news_list -->
 <?php endif; wp_reset_query(); ?>
</div><!-- END .index_news -->
<?php
         // バナー一覧 --------------------------------------------------------------------------------
         } elseif ($content['cb_content_select'] == 'banner') {
?>
<div class="index_banner cb_contents num<?php echo $content_count; ?>">
 <div class="banner_list clearfix">
  <?php
       for($i=1; $i<= 4; $i++):
         $image_id = $content['banner_image'.$i];
         if(!empty($image_id)) {
           $image = wp_get_attachment_image_src($image_id, 'full');
           $url = $content['banner_url'.$i];
           $target = $content['banner_target'.$i];
           $title = $content['banner_title'.$i];
           $sub_title = $content['banner_sub_title'.$i];
           $font_type = $content['banner_title_font_type'.$i];
           $use_overlay = $content['banner_use_overlay'.$i];
           $color = $content['banner_font_color'.$i];
  ?>
  <a class="link animate_background num<?php echo $i; ?>" href="<?php echo esc_url($url); ?>" <?php if($target == 1) { echo 'target="_blank"'; }; ?>>
   <?php if($title || $sub_title) { ?>
   <div class="title_area" style="color:<?php echo esc_attr($color); ?>;">
    <?php if($sub_title) { ?>
    <p class="sub_title"><span><?php echo nl2br(esc_html($sub_title)); ?></span></p>
    <?php }; ?>
    <?php if($title) { ?>
    <h3 class="title rich_font_<?php echo esc_attr($font_type); ?>"><span><?php echo nl2br(esc_html($title)); ?></span></h3>
    <?php }; ?>
   </div>
   <?php }; ?>
   <?php if($use_overlay) { ?>
   <div class="overlay"></div>
   <?php }; ?>
   <div class="image_wrap">
    <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
   </div>
  </a>
  <?php
         };
       endfor;
  ?>
 </div>
</div><!-- END .index_banner -->
<?php
         // free space -----------------------------------------------------
         } elseif ($content['cb_content_select'] == 'free_space') {
           if (!empty($content['free_space'])) {
?>
<div class="index_free_space cb_contents num<?php echo $content_count; ?>">
 <div class="post_content clearfix">
  <?php echo $content['free_space']; ?>
 </div>
</div><!-- END .cb_contents -->
<?php
           };
         };
       $content_count++;
       endforeach;
     endif;
// コンテンツビルダーここまで
?>

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
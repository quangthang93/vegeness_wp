<?php
     get_header();
     $options = get_design_plus_option();

     $headline = $options['blog_label'];
     $headline_color = $options['blog_headline_color'];
     $desc = $options['blog_desc'];
     $image_id = $options['blog_bg_image'];
     $use_overlay = $options['blog_use_overlay'];
     if($use_overlay) {
       $overlay_color = hex2rgb($options['blog_overlay_color']);
       $overlay_color = implode(",",$overlay_color);
       $overlay_opacity = $options['blog_overlay_opacity'];
     }

     if (is_category()) {
       $headline = single_cat_title('', false);
       $query_obj = get_queried_object();
       $cat_id = $query_obj->term_id;
       if (!empty($query_obj->description)){
         $desc = $query_obj->description;
       }
       $term_meta = get_option( 'taxonomy_' . $cat_id, array() );
       if (!empty($term_meta['image'])){
         $image_id = $term_meta['image'];
       }
       if (!empty($term_meta['main_color'])){
         $headline_color = $term_meta['main_color'];
       }
       if (!empty($term_meta['blog_use_overlay'])){
         if (!empty($term_meta['blog_overlay_color'])){
           $overlay_color = hex2rgb($term_meta['blog_overlay_color']);
           $overlay_color = implode(",",$overlay_color);
           if (!empty($term_meta['blog_overlay_opacity'])){
             $overlay_opacity = $term_meta['blog_overlay_opacity'];
           } else {
             $overlay_opacity = '0.3';
           }
         }
       }
     } elseif( is_tag() ) {
       $headline = single_tag_title('', false);
       $query_obj = get_queried_object();
       $cat_id = $query_obj->term_id;
       if (!empty($query_obj->description)){
         $desc = $query_obj->description;
       }
     } elseif (is_day()) {
       $headline = sprintf(__('Archive for %s', 'tcd-w'), get_the_time(__('F jS, Y', 'tcd-w')) );
       $desc = '';
     } elseif (is_month()) {
       $headline = sprintf(__('Archive for %s', 'tcd-w'), get_the_time(__('F, Y', 'tcd-w')) );
       $desc = '';
     } elseif (is_year()) {
       $headline = sprintf(__('Archive for %s', 'tcd-w'), get_the_time(__('Y', 'tcd-w')) );
       $desc = '';
     } elseif (is_author()) {
       global $wp_query;
       $curauth = $wp_query->get_queried_object();
       $author_id = $curauth->ID;
       $user_data = get_userdata($author_id);
       $author_name = $user_data->display_name;
       $headline = sprintf(__('Archive for %s', 'tcd-w'), $author_name);
       $desc = '';
     } else {
     };
     if($image_id) {
       $image = wp_get_attachment_image_src( $image_id, 'full' );
     }
?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <?php if(!is_paged()) { ?>
  <div id="page_header" <?php if($image_id) { ?>style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"<?php }; ?>>
   <?php if($headline){ ?><h2 class="headline rich_font" style="background:<?php echo esc_attr($headline_color); ?>;"><span><?php echo wp_kses_post(nl2br($headline)); ?></span></h2><?php }; ?>
   <?php if($desc){ ?><p class="desc"><?php echo wp_kses_post(nl2br($desc)); ?></p><?php }; ?>
   <?php if($use_overlay) { ?><div class="overlay" style="background: -webkit-linear-gradient(top, transparent 50%, rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>) 100%); background: linear-gradient(to bottom, transparent 50%, rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>) 100%);"></div><?php }; ?>
  </div>
  <?php }; ?>

  <div id="blog_archive">

   <?php if ( have_posts() ) : ?>

   <div id="post_list" class="clearfix">
    <?php
         while ( have_posts() ) : the_post();
           $premium_post = get_post_meta($post->ID,'premium_post',true);
           if($options['all_premium_post']){
             $premium_post = '1';
           }
           if(has_post_thumbnail()) {
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
           } elseif($options['no_image2']) {
             $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
           } else {
             $image = array();
             $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
           }
           $args = array( 'orderby' => 'term_order' );
           $category = wp_get_post_terms( $post->ID, 'category' ,$args);
    ?>
    <article class="item clearfix<?php if( $premium_post ){ echo ' premium_post'; }; ?>">
     <a class="link animate_background<?php if(!is_user_logged_in() && $premium_post) { echo ' register_link'; }; ?>" href="<?php if(!is_user_logged_in() && $premium_post) { echo '#'; } else { the_permalink(); }; ?>">
     <div class="image_wrap">
       <?php if($premium_post && $options['archive_blog_list_show_premium_icon']) { ?><div class="premium_icon"></div><?php }; ?>
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
      <div class="title_area">
       <h3 class="title rich_font"><span><?php the_title(); ?></span></h3>
       <?php if ( $options['show_archive_blog_date'] || $options['show_archive_blog_category'] ){ ?>
       <ul class="post_meta clearfix">
        <?php if ( $options['show_archive_blog_date'] ){ ?><li class="date"><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time></li><?php }; ?>
        <?php if ( $options['show_archive_blog_category'] && $category ) { ?>
        <li class="category">
         <?php
              foreach ( $category as $cat ) :
                echo '<span>' . esc_html($cat->name) . '</span>';
              endforeach;
         ?>
        </li>
        <?php }; ?>
       </ul>
       <?php }; ?>
      </div>
     </a>
    </article>
    <?php endwhile; ?>
   </div><!-- END .post_list2 -->

   <?php get_template_part('template-parts/navigation'); ?>

   <?php else: ?>

   <p id="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></p>

   <?php endif; ?>

  </div><!-- END #blog_archive -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
<?php $options = get_design_plus_option(); ?>

 <footer id="footer">

  <?php
       // Footer menu widget area -------------------------------------------------------
       $image = wp_get_attachment_image_src($options['footer_widget_bg_image'], 'full');
       if($options['footer_bg_type'] == 'type2') {
         $image = '';
         $video = $options['footer_widget_video'];
         if(!empty($video)) {
           if (!auto_play_movie()) {
             $video_image_id = $options['footer_widget_video_image'];
             if($video_image_id) {
               $image = wp_get_attachment_image_src($video_image_id, 'full');
             }
           }
         }
       }
  ?>
  <div id="footer_top"<?php if(!empty($image)) { ?> style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"<?php }; ?>>

   <?php
        // video -----------------------------------------------------
        if($options['footer_bg_type'] == 'type2') {
          $video = $options['footer_widget_video'];
          if(!empty($video)) {
            if (auto_play_movie()) {
   ?>
   <video id="footer_video" src="<?php echo esc_url(wp_get_attachment_url($options['footer_widget_video'])); ?>" playsinline autoplay loop muted></video>
   <?php }; }; }; ?>

   <div id="footer_top_inner" class="clearfix<?php if(!$options['show_footer_category_menu']) { echo ' no_menu'; }; ?>">

    <?php // custom menu -------------------------------------------- ?>
    <?php if (has_nav_menu('footer-menu') && $options['show_footer_menu']) { ?>
    <div id="footer_menu" class="footer_menu">
     <h3 class="footer_headline rich_font"><?php echo esc_html($options['footer_menu_headline']); ?></h3>
     <?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'theme_location' => 'footer-menu' , 'container' => '' , 'depth' => '1') ); ?>
    </div>
    <?php }; ?>

    <?php
         // Category menu --------------------------------------------------------------------
         if( $options['show_footer_category_menu']) {
    ?>
    <div id="footer_category_menu" class="footer_menu">
     <h3 class="footer_headline rich_font"><?php echo esc_html($options['footer_category_menu_headline']); ?></h3>
     <?php
          $recipe_category = get_terms( 'recipe_category', array( 'hide_empty' => true, 'orderby' => 'id', 'parent' => 0 ) );
          if ( $recipe_category && ! is_wp_error( $recipe_category ) ) :
     ?>
     <ul>
      <?php
           foreach ( $recipe_category as $cat ):
             $cat_id = $cat->term_id;
      ?>
      <li><a href="<?php echo esc_url(get_term_link($cat,'recipe_category')); ?>"><?php echo esc_html($cat->name); ?></a></li>
      <?php endforeach; ?>
     </ul>
     <?php endif; ?>
    </div>
    <?php }; ?>

    <?php
         // footer widget ------------------------------------------
         if( !wp_is_mobile() ) {
           if(is_active_sidebar('footer_widget')) {
    ?>
    <div id="footer_widget" class="clearfix">
     <?php dynamic_sidebar('footer_widget'); ?>
    </div>
    <?php
           };
         } else {
           if(is_active_sidebar('footer_mobile_widget')) {
    ?>
    <div id="footer_widget">
     <?php dynamic_sidebar('footer_mobile_widget'); ?>
    </div>
    <?php
           };
         };
    ?>

   </div><!-- END #footer_top_inner -->
   <?php
        $use_overlay = $options['footer_widget_use_overlay'];
        if($use_overlay) {
          $overlay_color = hex2rgb($options['footer_widget_overlay_color']);
          $overlay_color = implode(",",$overlay_color);
          $overlay_opacity = $options['footer_widget_overlay_opacity'];
   ?>
   <div id="widget_area_overlay" style="background:rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>);"></div>
   <?php }; ?>
  </div><!-- END #footer_top -->

  <div id="footer_bottom">

   <?php
        // Logo --------------------------------------------------------------------
   ?>
   <div id="footer_logo">
    <?php footer_logo(); ?>
    <?php if ($options['footer_logo_show_desc'] == 'type2' && is_front_page()) { ?>
    <h3 class="desc"><?php echo esc_html(get_bloginfo('description')); ?></h3>
    <?php } elseif ($options['footer_logo_show_desc'] == 'type3') { ?>
    <h3 class="desc"><?php echo esc_html(get_bloginfo('description')); ?></h3>
    <?php }; ?>
   </div>

   <?php
        // footer sns ------------------------------------
        $facebook = $options['footer_facebook_url'];
        $twitter = $options['footer_twitter_url'];
        $insta = $options['footer_instagram_url'];
        $pinterest = $options['footer_pinterest_url'];
        $youtube = $options['footer_youtube_url'];
        $contact = $options['footer_contact_url'];
        $show_rss = $options['footer_show_rss'];
   ?>
   <?php if($facebook || $twitter || $insta || $pinterest || $youtube || $contact || $show_rss) { ?>
   <ul id="footer_social_link" class="clearfix">
    <?php if($facebook) { ?><li class="facebook"><a href="<?php echo esc_url($facebook); ?>" rel="nofollow" target="_blank" title="Facebook"><span>Facebook</span></a></li><?php }; ?>
    <?php if($twitter) { ?><li class="twitter"><a href="<?php echo esc_url($twitter); ?>" rel="nofollow" target="_blank" title="Twitter"><span>Twitter</span></a></li><?php }; ?>
    <?php if($insta) { ?><li class="insta"><a href="<?php echo esc_url($insta); ?>" rel="nofollow" target="_blank" title="Instagram"><span>Instagram</span></a></li><?php }; ?>
    <?php if($pinterest) { ?><li class="pinterest"><a href="<?php echo esc_url($pinterest); ?>" rel="nofollow" target="_blank" title="Pinterest"><span>Pinterest</span></a></li><?php }; ?>
    <?php if($youtube) { ?><li class="youtube"><a href="<?php echo esc_url($youtube); ?>" rel="nofollow" target="_blank" title="Youtube"><span>Youtube</span></a></li><?php }; ?>
    <?php if($contact) { ?><li class="contact"><a href="<?php echo esc_url($contact); ?>" rel="nofollow" target="_blank" title="Contact"><span>Contact</span></a></li><?php }; ?>
    <?php if($show_rss) { ?><li class="rss"><a href="<?php esc_url(bloginfo('rss2_url')); ?>" rel="nofollow" target="_blank" title="RSS"><span>RSS</span></a></li><?php }; ?>
   </ul>
   <?php }; ?>

  </div><!-- END #footer_bottom -->

  <div id="return_top">
   <a href="#body"><span><?php _e('PAGE TOP', 'tcd-w'); ?></span></a>
  </div>

  <p id="copyright"><?php echo wp_kses_post($options['copyright']); ?></p>

 </footer>

 <?php
      // footer bar for mobile device -------------------
      if( is_mobile() ) {
        if($options['footer_bar_display'] != 'type3') {
          get_template_part('template-parts/footer-bar');
        }
      };
 ?>

</div><!-- #container -->

<?php // drawer menu -------------------------------------------- ?>
<div id="drawer_menu">
 <?php if (has_nav_menu('global-menu')) { ?>
 <nav>
  <?php wp_nav_menu( array( 'menu_id' => 'mobile_menu', 'sort_column' => 'menu_order', 'theme_location' => 'global-menu' , 'container' => '' ) ); ?>
 </nav>
 <?php }; ?>
 <div id="mobile_banner">
  <?php
       for($i=1; $i<= 3; $i++):
         if( $options['mobile_menu_ad_code'.$i] || $options['mobile_menu_ad_image'.$i] ) {
           if ($options['mobile_menu_ad_code'.$i]) {
  ?>
  <div class="banner">
   <?php echo $options['mobile_menu_ad_code'.$i]; ?>
  </div>
  <?php
       } else {
         $mobile_menu_image = wp_get_attachment_image_src( $options['mobile_menu_ad_image'.$i], 'full' );
  ?>
  <div class="banner">
   <a href="<?php echo esc_url( $options['mobile_menu_ad_url'.$i] ); ?>"<?php if($options['mobile_menu_ad_target'.$i] == 1) { ?> target="_blank"<?php }; ?>><img src="<?php echo esc_attr($mobile_menu_image[0]); ?>" alt="" title="" /></a>
  </div>
  <?php }; }; endfor; ?>
 </div><!-- END #header_mobile_banner -->
</div>

<?php
     // load script -----------------------------------------------------------
     if ($options['show_load_screen'] == 'type2') {
       if(is_front_page()){
         has_loading_screen();
       } else {
         no_loading_screen();
       }
     } elseif ($options['show_load_screen'] == 'type3') {
       if(is_front_page() || is_home() || is_post_type_archive('work') ){
         has_loading_screen();
       } else {
         no_loading_screen();
       }
     } else {
       no_loading_screen();
     };
?>

<?php
     // share button ----------------------------------------------------------------------
     if ( is_single() && ( $options['show_sns_top'] || $options['show_sns_btm'] || $options['show_sns_top_news'] || $options['show_sns_btm_news']) ) :
       if ( 'type5' == $options['sns_type_top'] || 'type5' == $options['sns_type_btm'] ) :
         if ( $options['show_twitter_top'] || $options['show_twitter_btm'] ) :
?>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<?php
         endif;
         if ( $options['show_fblike_top'] || $options['show_fbshare_top'] || $options['show_fblike_btm'] || $options['show_fbshare_btm'] ) :
?>
<!-- facebook share button code -->
<div id="fb-root"></div>
<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<?php
         endif;
         if ( $options['show_hatena_top'] || $options['show_hatena_btm'] ) :
?>
<script type="text/javascript" src="http://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script>
<?php
         endif;
         if ( $options['show_pocket_top'] || $options['show_pocket_btm'] ) :
?>
<script type="text/javascript">!function(d,i){if(!d.getElementById(i)){var j=d.createElement("script");j.id=i;j.src="https://widgets.getpocket.com/v1/j/btn.js?v=1";var w=d.getElementById(i);d.body.appendChild(j);}}(document,"pocket-btn-js");</script>
<?php
         endif;
         if ( $options['show_pinterest_top'] || $options['show_pinterest_btm'] ) :
?>
<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
<?php
         endif;
       endif;
     endif;
?>

<?php wp_footer(); ?>
</body>
</html>
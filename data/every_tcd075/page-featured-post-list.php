<?php
/*
Template Name:Featured page
*/
__('Featured page', 'tcd-w');
?>
<?php
     get_header();
     $options = get_design_plus_option();

     $catch = get_post_meta($post->ID, 'featured_header_catch', true);
     $desc = get_post_meta($post->ID, 'featured_header_desc', true);

     $font_color = get_post_meta($post->ID, 'featured_header_font_color', true);
     if(empty($font_color)){
       $font_color = '#FFFFFF';
     }

     $font_type = get_post_meta($post->ID, 'featured_header_catch_font_type', true);
     if(empty($font_type)){
       $font_type = 'type3';
     }

     $bg_color = get_post_meta($post->ID, 'featured_header_bg_color', true);
     if(empty($bg_color)){
       $bg_color = '#666666';
     }

     $image_id = get_post_meta($post->ID, 'featured_header_bg_image', true);
     if($image_id){
       $image = wp_get_attachment_image_src( $image_id, 'full' );
       if(is_mobile()) {
         $image_mobile_id = get_post_meta($post->ID, 'featured_header_bg_image_mobile', true);
         if($image_mobile_id){
           $image = wp_get_attachment_image_src( $image_mobile_id, 'full');
         }
       }
     };
     $use_overlay = get_post_meta($post->ID, 'featured_header_use_overlay', true);
     if($use_overlay) {
       $overlay_color = get_post_meta($post->ID, 'featured_header_overlay_color', true);
       if(empty($overlay_color)){
         $overlay_color = '#000000';
       }
       $overlay_color = hex2rgb($overlay_color);
       $overlay_color = implode(",",$overlay_color);
       $overlay_opacity = get_post_meta($post->ID, 'featured_header_overlay_opacity', true);
       if(empty($overlay_opacity)){
         $overlay_opacity = '0.5';
       }
     }
?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="wide_page_header" style="<?php if($image_id) { ?>background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;<?php } else { ?>background:<?php echo esc_attr($bg_color); ?><?php }; ?>">
 <div id="wide_page_header_inner" style="color:<?php echo esc_html($font_color); ?>;">
  <?php if($catch){ ?><h2 class="catch rich_font_<?php echo esc_attr($font_type); ?>"><?php echo nl2br(esc_html($catch)); ?></h2><?php }; ?>
  <?php if($desc){ ?><p class="desc"><?php echo nl2br(esc_html($desc)); ?></p><?php }; ?>
 </div>
 <?php if($use_overlay) { ?><div class="overlay" style="background:rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>);"></div><?php }; ?>
</div>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <div id="featured_recipe">

   <?php
        $show_post_view = get_post_meta($post->ID, 'featured_list_show_post_view', true);
        $show_premium_icon = get_post_meta($post->ID, 'featured_list_show_premium_icon', true);
        $post_type = get_post_meta($post->ID, 'featured_list_post_type', true);
        if(empty($post_type)){
          $post_type = 'all';
        }
        $post_num = get_post_meta($post->ID, 'featured_list_num', true);
        if(empty($post_num)){
          $post_num = '9';
        }
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        if($post_type == 'all') {
          $args = array( 'post_type' => 'recipe', 'posts_per_page' => $post_num, 'paged' => $paged );
        } else {
          $args = array( 'post_type' => 'recipe', 'meta_key' => $post_type, 'meta_value' => '1', 'posts_per_page' => $post_num, 'paged' => $paged );
        }
        $recipe_query = new wp_query($args);

        $headline = get_post_meta($post->ID, 'featured_headline', true);
        $desc = get_post_meta($post->ID, 'featured_desc', true);
        $post_count = $recipe_query->found_posts;
        $show_total_post = get_post_meta($post->ID, 'show_featured_total_post', true);
        $total_post_label = get_post_meta($post->ID, 'featured_total_post_label', true);
        if(empty($total_post_label)){
          $total_post_label = __( 'recipe', 'tcd-w' );
        }
   ?>
   <?php if($headline) { ?><h3 class="design_headline clearfix<?php if(get_post_meta($post->ID, 'featured_headline_hide_icon', true)) { echo ' hide_icon'; }; ?>"><span class="title rich_font"><?php echo esc_html($headline); ?></span><?php if($show_total_post) { ?><span class="total_post"><?php echo number_format($post_count) . esc_html($total_post_label); ?></span><?php }; ?></h3><?php }; ?>
   <?php if($desc){ ?><p class="recipe_list_desc"><?php echo wp_kses_post(nl2br($desc)); ?></p><?php }; ?>

   <?php if($recipe_query->have_posts()): ?>
   <div class="recipe_list type2 clearfix">
    <?php
         while($recipe_query->have_posts()): $recipe_query->the_post();
           $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
           $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
           if(has_post_thumbnail()) {
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
           } elseif($options['no_image2']) {
             $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
           } else {
             $image = array();
             $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
           }
    ?>
    <article class="item<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>">
     <a class="link animate_background" style="background:none;" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
      <div class="image_wrap">
       <?php if($premium_recipe && $show_premium_icon) { ?><div class="premium_icon"></div><?php }; ?>
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
     </a>
     <div class="title_area">
      <h3 class="title"><a href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>"><span><?php the_title(); ?></span></a></h3>
      <?php if ($show_post_view){ ?><p class="post_meta"><?php if($recipe_type != 'type2') { _e('Hits:', 'tcd-w'); } else { _e('Views:', 'tcd-w'); }; ?><?php the_post_views(); ?></p><?php }; ?>
     </div>
    </article>
    <?php endwhile; ?>
   </div><!-- END .recipe_list1 -->
   <?php get_template_part('template-parts/navigation2'); ?>
   <?php else: ?>

   <p id="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></p>

   <?php endif; ?>

  </div><!-- END #featured_recipe -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
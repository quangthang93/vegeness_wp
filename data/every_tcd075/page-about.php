<?php
/*
Template Name:ABOUT page
*/
__('ABOUT page', 'tcd-w');
?>
<?php
     get_header();
     $options = get_design_plus_option();
?>
<?php get_template_part('template-parts/breadcrumb'); ?>
<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <div id="about_page">

   <?php
        $header_image_id = get_post_meta($post->ID, 'about_header_image', true);
        if(!empty($header_image_id)) {
          $header_image = wp_get_attachment_image_src($header_image_id, 'full');
          $header_headline_color = get_post_meta($post->ID, 'about_header_headline_color', true);
          if(empty($header_headline_color)){
            $header_headline_color = '#ff8000';
          }
   ?>
   <div id="about_header" style="background:url(<?php echo esc_attr($header_image[0]); ?>) no-repeat center center; background-size:cover;">
    <h2 class="headline rich_font" style="background:<?php echo esc_attr($header_headline_color); ?>;"><span><?php the_title(); ?></span></h2>
   </div>
   <?php }; ?>

   <?php
        $font_type = get_post_meta($post->ID, 'about_content_catch_font_type', true);
        if(empty($font_type)){
          $font_type = 'type3';
        }
        $page_content_order = get_post_meta($post->ID, 'page_content_order', true);
        if(empty($page_content_order)) {
          $page_content_order = array('content1','content2','content3','content4');
        }
        foreach((array) $page_content_order as $page_content) :

        // content1 --------------------------------------------------------------------
        if( ($page_content == 'content1') && get_post_meta($post->ID, 'show_about_content1', true) ) {
          $about_content1_catch = get_post_meta($post->ID, 'about_content1_catch', true);
          $about_content1_desc = get_post_meta($post->ID, 'about_content1_desc', true);
          $about_content1_image1 = get_post_meta($post->ID, 'about_content1_image1', true);
          if(!empty($about_content1_image1)) {
            $content1_image1 = wp_get_attachment_image_src($about_content1_image1, 'full');
          }
          $about_content1_image2 = get_post_meta($post->ID, 'about_content1_image2', true);
          if(!empty($about_content1_image2)) {
            $content1_image2 = wp_get_attachment_image_src($about_content1_image2, 'full');
          }
   ?>
   <div class="about_content" id="about_content1">
    <div class="content_area">
     <?php if($about_content1_catch){ ?><h2 class="headline rich_font_<?php echo esc_attr($font_type); ?>"><?php echo wp_kses_post(nl2br($about_content1_catch)); ?></h2><?php }; ?>
     <?php if($about_content1_desc){ ?>
     <div class="post_content clearfix">
      <?php echo do_shortcode( wpautop(wp_kses_post($about_content1_desc)) ); ?>
     </div>
     <?php }; ?>
    </div>
    <?php if($about_content1_image1 || $about_content1_image2){ ?>
    <div class="image_area clearfix">
     <?php if($about_content1_image1){ ?><img src="<?php echo esc_attr($content1_image1[0]); ?>" alt="" title="" /><?php }; ?>
     <?php if($about_content1_image2){ ?><img src="<?php echo esc_attr($content1_image2[0]); ?>" alt="" title="" /><?php }; ?>
    </div>
    <?php }; ?>
   </div>
   <?php }; ?>

   <?php
        // content2 --------------------------------------------------------------------
        if( ($page_content == 'content2') && get_post_meta($post->ID, 'show_about_content2', true) ) {
          $about_content2_catch = get_post_meta($post->ID, 'about_content2_catch', true);
          $about_content2_desc = get_post_meta($post->ID, 'about_content2_desc', true);
          $about_content2_image1 = get_post_meta($post->ID, 'about_content2_image1', true);
          if(!empty($about_content2_image1)) {
            $content2_image1 = wp_get_attachment_image_src($about_content2_image1, 'full');
          }
          $about_content2_image2 = get_post_meta($post->ID, 'about_content2_image2', true);
          if(!empty($about_content2_image2)) {
            $content2_image2 = wp_get_attachment_image_src($about_content2_image2, 'full');
          }
          $about_content2_image3 = get_post_meta($post->ID, 'about_content2_image3', true);
          if(!empty($about_content2_image3)) {
            $content2_image3 = wp_get_attachment_image_src($about_content2_image3, 'full');
          }
   ?>
   <div class="about_content" id="about_content2">
    <div class="content_area">
     <?php if($about_content2_catch){ ?><h2 class="headline rich_font_<?php echo esc_attr($font_type); ?>"><?php echo wp_kses_post(nl2br($about_content2_catch)); ?></h2><?php }; ?>
     <?php if($about_content2_desc){ ?>
     <div class="post_content clearfix">
      <?php echo do_shortcode( wpautop(wp_kses_post($about_content2_desc)) ); ?>
     </div>
     <?php }; ?>
    </div>
    <?php if($about_content2_image1 || $about_content2_image2 || $about_content2_image3){ ?>
    <div class="image_area clearfix">
     <?php if($about_content2_image1){ ?><img src="<?php echo esc_attr($content2_image1[0]); ?>" alt="" title="" /><?php }; ?>
     <?php if($about_content2_image2){ ?><img src="<?php echo esc_attr($content2_image2[0]); ?>" alt="" title="" /><?php }; ?>
     <?php if($about_content2_image3){ ?><img src="<?php echo esc_attr($content2_image3[0]); ?>" alt="" title="" /><?php }; ?>
    </div>
    <?php }; ?>
   </div>
   <?php }; ?>

   <?php
        // content3 --------------------------------------------------------------------
        if( ($page_content == 'content3') && get_post_meta($post->ID, 'show_about_content3', true) ) {
          $about_content3_catch = get_post_meta($post->ID, 'about_content3_catch', true);
          $about_content3_desc = get_post_meta($post->ID, 'about_content3_desc', true);
          $about_content3_image1 = get_post_meta($post->ID, 'about_content3_image1', true);
          if(!empty($about_content3_image1)) {
            $content3_image1 = wp_get_attachment_image_src($about_content3_image1, 'full');
          }
   ?>
   <div class="about_content" id="about_content3">
    <div class="content_area">
     <?php if($about_content3_catch){ ?><h2 class="headline rich_font_<?php echo esc_attr($font_type); ?>"><?php echo wp_kses_post(nl2br($about_content3_catch)); ?></h2><?php }; ?>
     <?php if($about_content3_desc){ ?>
     <div class="post_content clearfix">
      <?php echo do_shortcode( wpautop(wp_kses_post($about_content3_desc)) ); ?>
     </div>
     <?php }; ?>
    </div>
    <?php if($about_content3_image1){ ?>
    <div class="image_area clearfix">
     <?php if($about_content3_image1){ ?><img src="<?php echo esc_attr($content3_image1[0]); ?>" alt="" title="" /><?php }; ?>
    </div>
    <?php }; ?>
   </div>
   <?php }; ?>

   <?php
        // content4 --------------------------------------------------------------------
        if( ($page_content == 'content4') && get_post_meta($post->ID, 'show_about_content4', true) ) {
          $about_content4_catch = get_post_meta($post->ID, 'about_content4_catch', true);
          $about_content4_desc = get_post_meta($post->ID, 'about_content4_desc', true);
          $about_content4_data_list = get_post_meta($post->ID, 'about_content4_data_list', true);
   ?>
   <div class="about_content" id="about_content4">
    <div class="content_area">
     <?php if($about_content4_catch){ ?><h2 class="headline rich_font_<?php echo esc_attr($font_type); ?>"><?php echo wp_kses_post(nl2br($about_content4_catch)); ?></h2><?php }; ?>
     <?php if($about_content4_desc){ ?>
     <div class="post_content clearfix">
      <?php echo do_shortcode( wpautop(wp_kses_post($about_content4_desc)) ); ?>
     </div>
     <?php }; ?>
    </div>
    <?php if($about_content4_data_list){ ?>
    <dl id="about_faq_list">
     <?php foreach ( $about_content4_data_list as $key => $value ) : ?>
     <?php if(!empty($value['title'])) { ?>
     <dt><p><?php echo wp_kses_post(nl2br($value['title'])); ?></p></dt>
     <?php }; ?>
     <?php if(!empty($value['desc'])) { ?>
     <dd><div class="post_content clearfix"><p><?php echo wp_kses_post(nl2br($value['desc'])); ?></p></div></dd>
     <?php }; ?>
     <?php endforeach; ?>
    </dl>
    <?php }; ?>
   </div>
   <?php }; ?>

   <?php endforeach; // END contet order ?>

  </div><!-- #about_page -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
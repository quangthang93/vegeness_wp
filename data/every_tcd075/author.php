<?php
     $query_obj = get_queried_object();
     $user_roles = $query_obj->roles[0];
     if($user_roles == 'subscriber' || $user_roles == 'contributor') {
       wp_safe_redirect( home_url() );
       exit;
     }

     get_header();
     $options = get_design_plus_option();

     $post_count = $wp_query->found_posts;
     $author_id = $query_obj->ID;
     $user_data = get_userdata($author_id);

     $user_name = $user_data->display_name;
     $sub_title = $user_data->user_sub_title;
     $desc = $user_data->user_desc;
     $facebook = $user_data->facebook_url;
     $twitter = $user_data->twitter_url;
     $insta = $user_data->instagram_url;
     $pinterest = $user_data->pinterest_url;
     $youtube = $user_data->youtube_url;
     $contact = $user_data->contact_url;

     $user_image_id = $user_data->user_image;
     if($user_image_id) {
       $image = wp_get_attachment_image_src( $user_image_id, 'full' );
     }
?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <?php if(!is_paged()) { ?>
  <div id="author_page_header">
   <div class="image" <?php if($user_image_id) { ?>style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"<?php }; ?>>
    <div class="info_wrap">
     <div class="info">
      <?php
           $category_ids = $user_data->user_category;
           if($category_ids) {
             foreach ( $category_ids as $category_id ):
               $category_data = get_term_by('id', $category_id, 'recipe_category');
               if($category_data){
                 $count[] = $category_data->count;
               }
             endforeach;
             $category_ids_new = array_combine($category_ids, $count);
             arsort($category_ids_new);
             foreach ( $category_ids_new as $category_id => $category_post_count):
               $category_data = get_term_by('id', $category_id, 'recipe_category');
               if($category_data){
                 $term_meta = get_option( 'taxonomy_' . $category_id, array() );
                 if (!empty($term_meta['main_color'])){
                   $category_color = $term_meta['main_color'];
                 } else {
                   $category_color = "#009fe1";
                 }
      ?>
      <p class="category rich_font" style="background:<?php echo esc_attr($category_color); ?>;"><?php echo esc_html($category_data->name); ?></p>
      <?php
               };
               break;
             endforeach;
           };
      ?>
      <h2 class="name rich_font"><?php echo esc_html($user_name); ?><?php if($sub_title) { ?><span><?php echo esc_html($sub_title); ?></span><?php }; ?></h2>
      <?php if($facebook || $twitter || $insta || $pinterest || $youtube || $contact ) { ?>
      <ul class="author_link clearfix">
       <?php if($facebook) { ?><li class="facebook"><a href="<?php echo esc_url($facebook); ?>" rel="nofollow" target="_blank" title="Facebook"><span>Facebook</span></a></li><?php }; ?>
       <?php if($twitter) { ?><li class="twitter"><a href="<?php echo esc_url($twitter); ?>" rel="nofollow" target="_blank" title="Twitter"><span>Twitter</span></a></li><?php }; ?>
       <?php if($insta) { ?><li class="insta"><a href="<?php echo esc_url($insta); ?>" rel="nofollow" target="_blank" title="Instagram"><span>Instagram</span></a></li><?php }; ?>
       <?php if($pinterest) { ?><li class="pinterest"><a href="<?php echo esc_url($pinterest); ?>" rel="nofollow" target="_blank" title="Pinterest"><span>Pinterest</span></a></li><?php }; ?>
       <?php if($youtube) { ?><li class="youtube"><a href="<?php echo esc_url($youtube); ?>" rel="nofollow" target="_blank" title="Youtube"><span>Youtube</span></a></li><?php }; ?>
       <?php if($contact) { ?><li class="contact"><a href="<?php echo esc_url($contact); ?>" rel="nofollow" target="_blank" title="Contact"><span>Contact</span></a></li><?php }; ?>
      </ul>
      <?php }; ?>
     </div>
     <div class="blur_image" <?php if($user_image_id) { ?>style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"<?php }; ?>></div>
    </div>
   </div><!-- END .image -->
   <?php if($desc) { ?>
   <div class="desc post_content clearfix">
    <?php echo do_shortcode( wpautop(wp_kses_post($desc)) ); ?>
   </div>
   <?php }; ?>
  </div>
  <?php }; ?>

  <div id="author_archive">

   <h3 class="design_headline clearfix rich_font<?php if($options['hide_author_recipe_headline_icon']){ echo ' hide_icon'; }; ?>"><?php printf(__('<span class="author_name">%s %s</span>', 'tcd-w'),$user_name,$options['author_recipe_headline_label']); ?><?php if($options['show_author_recipe_post_count']) { ?><span class="total_post"><?php echo number_format($post_count) . esc_html($options['author_recipe_post_count_label']); ?></span><?php }; ?></h3>

   <?php if ( have_posts() ) : ?>

   <div class="recipe_list type2 clearfix">
    <?php
         while ( have_posts() ) : the_post();
           $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
           $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
           if(has_post_thumbnail()) {
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
           } elseif($options['no_image2']) {
             $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
           } else {
             $image = array();
             $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
           }
    ?>
    <article class="item<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>">
     <a class="link animate_background" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
      <div class="image_wrap">
       <?php if($premium_recipe) { ?><div class="premium_icon"></div><?php }; ?>
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
     </a>
     <div class="title_area">
      <h3 class="title"><a href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>"><span><?php the_title(); ?></span></a></h3>
      <?php if($options['author_recipe_list_show_post_view']) { ?>
      <p class="post_meta"><?php if($recipe_type != 'type2') { _e('Hits:', 'tcd-w'); } else { _e('Views:', 'tcd-w'); }; ?><?php the_post_views(); ?></p>
      <?php }; ?>
     </div>
    </article>
    <?php endwhile; ?>
   </div><!-- END .recipe_list1 -->

   <?php get_template_part('template-parts/navigation'); ?>

   <?php else: ?>

   <p id="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></p>

   <?php endif; ?>

  </div><!-- END recipe_archive -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
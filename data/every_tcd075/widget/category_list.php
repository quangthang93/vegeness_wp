<?php

class tcd_category_list_widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      'tcd_category_list_widget',// ID
      __( 'Category list (tcd ver)', 'tcd-w' ),
      array(
        'classname' => 'tcd_category_list_widget',
        'description' => __('Displays designed category list.', 'tcd-w')
      )
    );
  }

  function widget($args, $instance) {

    extract( $args );
    $title = apply_filters('widget_title', $instance['title']);
    $exclude_cat_num = $instance['exclude_cat_num'];
    $only_parent = $instance['only_parent'];

    // Before widget //
    echo $before_widget;

    // Title of widget //
    if ( $title ) { echo $before_title . $title . $after_title; }

    // Widget output //
?>
<ul class="tcd_category_list clearfix">
 <?php
      if($only_parent) {
        $string = wp_list_categories(array('title_li' =>'','show_count' => 0, 'echo' => 0, 'depth' => 1, 'exclude' => $exclude_cat_num));
      } else {
        $string = wp_list_categories(array('title_li' =>'','show_count' => 0, 'echo' => 0, 'exclude' => $exclude_cat_num));
      }
      echo $string;
 ?>
</ul>
<?php

    // After widget //
    echo $after_widget;

  } // end function widget


  // Update Settings //
  function update($new_instance, $old_instance) {
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['exclude_cat_num'] = $new_instance['exclude_cat_num'];
    $instance['only_parent'] = $new_instance['only_parent'];
    $instance['headline_font_color'] = $new_instance['headline_font_color'];
    $instance['headline_bg_color'] = $new_instance['headline_bg_color'];
    $instance['headline_border_color'] = $new_instance['headline_border_color'];
    $instance['icon_color'] = $new_instance['icon_color'];
    $instance['hide_icon'] = $new_instance['hide_icon'];
    $instance['icon_image'] = $new_instance['icon_image'];
    return $instance;
  }

  // Widget Control Panel //
  function form($instance) {
    $defaults = array( 'title' => __('Category list', 'tcd-w'), 'exclude_cat_num' => '', 'only_parent' => '', 'headline_font_color' => '#000000', 'headline_bg_color' => '#ffffff', 'headline_border_color' => '#dddddd', 'icon_color' => '#000000', 'hide_icon' => '', 'icon_image' => false);
    $instance = wp_parse_args( (array) $instance, $defaults );
?>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Title', 'tcd-w'); ?></h3>
 <input class="widefat" name="<?php echo $this->get_field_name('title'); ?>'" type="text" value="<?php echo $instance['title']; ?>" />
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Categories to exclude', 'tcd-w'); ?></h3>
 <input class="widefat" name="<?php echo $this->get_field_name('exclude_cat_num'); ?>'" type="text" value="<?php echo $instance['exclude_cat_num']; ?>" />
 <p><?php _e('Enter a comma-seperated list of category ID numbers, example 2,4,10<br />(Don\'t enter comma for last number).', 'tcd-w'); ?></p>
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Display setting', 'tcd-w'); ?></h3>
 <input id="<?php echo $this->get_field_id('only_parent'); ?>" name="<?php echo $this->get_field_name('only_parent'); ?>" type="checkbox" value="1" <?php checked( '1', $instance['only_parent'] ); ?> />
 <label for="<?php echo $this->get_field_id('only_parent'); ?>"><?php _e('Display only parent category', 'tcd-w'); ?></label>
</div>

<div class="tcd_ad_widget_box_wrap">
 <h3 class="tcd_ad_widget_headline"><?php _e('Headline setting', 'tcd-w'); ?></h3>
 <div class="tcd_ad_widget_box">

<div class="theme_option_message" style="box-shadow:none; margin:0 0 20px;">
 <p><?php _e('Headline options will not be applied to footer widget area.', 'tcd-w'); ?></p>
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Font color of headline', 'tcd-w'); ?></h3>
 <input name="<?php echo $this->get_field_name('headline_font_color'); ?>'" type="text" value="<?php echo $instance['headline_font_color']; ?>" data-default-color="#000000" class="color-picker">
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Background color of headline', 'tcd-w'); ?></h3>
 <input name="<?php echo $this->get_field_name('headline_bg_color'); ?>'" type="text" value="<?php echo $instance['headline_bg_color']; ?>" data-default-color="#ffffff" class="color-picker">
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Border color of headline', 'tcd-w'); ?></h3>
 <input name="<?php echo $this->get_field_name('headline_border_color'); ?>'" type="text" value="<?php echo $instance['headline_border_color']; ?>" data-default-color="#dddddd" class="color-picker">
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Headline icon display setting', 'tcd-w'); ?></h3>
 <input class="widget_headline_hide_icon" id="<?php echo $this->get_field_id('hide_icon'); ?>" name="<?php echo $this->get_field_name('hide_icon'); ?>" type="checkbox" value="1" <?php checked( '1', $instance['hide_icon'] ); ?> />
 <label for="<?php echo $this->get_field_id('hide_icon'); ?>"><?php _e('Hide headline icon', 'tcd-w'); ?></label>
</div>
<div class="widget_headline_icon_area"<?php if($instance['hide_icon']) { echo ' style="display:none;"'; }; ?>>
 <div class="tcd_widget_content">
  <h3 class="tcd_widget_headline"><?php _e('Icon background color', 'tcd-w'); ?></h3>
  <input name="<?php echo $this->get_field_name('icon_color'); ?>'" type="text" value="<?php echo $instance['icon_color']; ?>" data-default-color="#000000" class="color-picker">
 </div>
 <div class="tcd_widget_content">
  <h3 class="tcd_widget_headline"><?php _e('Icon image', 'tcd-w'); ?></h3>
  <div class="widget_media_upload cf cf_media_field hide-if-no-js <?php echo $this->get_field_id('icon_image'); ?>">
   <input type="hidden" value="<?php echo $instance['icon_image']; ?>" id="<?php echo $this->get_field_id('icon_image'); ?>" name="<?php echo $this->get_field_name('icon_image'); ?>" class="cf_media_id">
   <div class="preview_field"><?php if($instance['icon_image']){ echo wp_get_attachment_image($instance['icon_image'], 'full'); }; ?></div>
   <div class="buttton_area">
    <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
    <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$instance['icon_image']){ echo 'hidden'; }; ?>">
   </div>
  </div>
 </div>
</div>

 </div>
</div>
<?php
  } // end function form

} // end class


function register_tcd_category_list_widget() {
	register_widget( 'tcd_category_list_widget' );
}
add_action( 'widgets_init', 'register_tcd_category_list_widget' );


?>
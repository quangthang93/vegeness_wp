<?php

class ranking_list_widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      'ranking_list_widget',// ID
      __( 'Popular post list (tcd ver)', 'tcd-w' ),
      array(
        'classname' => 'ranking_list_widget',
        'description' => __('Displays post list based on post view.', 'tcd-w')
      )
    );
  }

  // Extract Args //
  function widget($args, $instance) {

    global $post;

    extract( $args );
    $title = apply_filters('widget_title', $instance['title']);
    $post_num = $instance['post_num'];
    $range = $instance['post_range'];

    // Before widget //
    echo $before_widget;

    // Title of widget //
    if ( $title ) { echo $before_title . $title . $after_title; }

    // Widget output //
    $args = array('post_type' => 'post', 'posts_per_page' => $post_num, 'ignore_sticky_posts' => 1);
    $popular_post_list = get_posts_views_ranking( $range, $args, 'WP_Query' );

    $options = get_design_plus_option();
?>
<ol class="styled_post_list1 clearfix">
<?php
     if ($popular_post_list->have_posts()) {
       $i = 1;
       while ($popular_post_list->have_posts()) : $popular_post_list->the_post();
         $premium_post = get_post_meta($post->ID,'premium_post',true);
         if($options['all_premium_post']) {
           $premium_post = '1';
         }
         if(has_post_thumbnail()) {
           $image = wp_get_attachment_image_src( get_post_thumbnail_id( $popular_post_list->ID ), 'size1' );
         } elseif($options['no_image1']) {
           $image = wp_get_attachment_image_src( $options['no_image1'], 'full' );
         } else {
           $image = array();
           $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image1.gif";
         }
?>
 <li class="clearfix rank<?php echo $i; ?>">
  <a class="clearfix animate_background<?php if(!is_user_logged_in() && $premium_post) { echo ' register_link'; }; ?>" href="<?php if(!is_user_logged_in() && $premium_post) { echo '#'; } else { the_permalink(); }; ?>">
   <div class="image_wrap">
    <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
   </div>
   <div class="title_area">
    <div class="title_area_inner">
     <p class="rank"><?php printf(__('Rank %s', 'tcd-w'),$i); ?></p>
     <h4 class="title"><span><?php the_title_attribute(); ?></span></h4>
    </div>
   </div>
  </a>
 </li>
<?php $i++; endwhile; wp_reset_query(); } else { ?>
 <li class="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></li>
<?php }; ?>
</ol>
<?php

    // After widget //
    echo $after_widget;

  } // end function widget


  // Update Settings //
  function update($new_instance, $old_instance) {
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['post_num'] = $new_instance['post_num'];
    $instance['post_range'] = $new_instance['post_range'];
    $instance['headline_font_color'] = $new_instance['headline_font_color'];
    $instance['headline_bg_color'] = $new_instance['headline_bg_color'];
    $instance['headline_border_color'] = $new_instance['headline_border_color'];
    $instance['icon_color'] = $new_instance['icon_color'];
    $instance['hide_icon'] = $new_instance['hide_icon'];
    $instance['icon_image'] = $new_instance['icon_image'];
    return $instance;
  }

  // Widget Control Panel //
  function form($instance) {
    $defaults = array( 'title' => __('Ranking', 'tcd-w'), 'post_num' => 3, 'post_range' => '', 'headline_font_color' => '#000000', 'headline_bg_color' => '#ffffff', 'headline_border_color' => '#dddddd', 'icon_color' => '#000000', 'hide_icon' => '', 'icon_image' => false);
    $instance = wp_parse_args( (array) $instance, $defaults );
?>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Title', 'tcd-w'); ?></h3>
 <input class="widefat" name="<?php echo $this->get_field_name('title'); ?>'" type="text" value="<?php echo $instance['title']; ?>" />
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Scope of popular post', 'tcd-w'); ?></h3>
 <select name="<?php echo $this->get_field_name('post_range'); ?>" class="widefat" style="width:100%;">
  <option value="day" <?php selected('day', $instance['post_range']); ?>><?php _e('Daily', 'tcd-w'); ?></option>
  <option value="week" <?php selected('week', $instance['post_range']); ?>><?php _e('Weekly', 'tcd-w'); ?></option>
  <option value="month" <?php selected('month', $instance['post_range']); ?>><?php _e('Monthly', 'tcd-w'); ?></option>
  <option value="year" <?php selected('year', $instance['post_range']); ?>><?php _e('Yearly', 'tcd-w'); ?></option>
  <option value="" <?php selected('', $instance['post_range']); ?>><?php _e('All time', 'tcd-w'); ?></option>
 </select>
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Number of post', 'tcd-w'); ?></h3>
 <select name="<?php echo $this->get_field_name('post_num'); ?>" class="widefat" style="width:100%;">
  <option value="3" <?php selected('3', $instance['post_num']); ?>>3</option>
  <option value="4" <?php selected('4', $instance['post_num']); ?>>4</option>
  <option value="5" <?php selected('5', $instance['post_num']); ?>>5</option>
  <option value="6" <?php selected('6', $instance['post_num']); ?>>6</option>
  <option value="7" <?php selected('7', $instance['post_num']); ?>>7</option>
  <option value="8" <?php selected('8', $instance['post_num']); ?>>8</option>
  <option value="9" <?php selected('9', $instance['post_num']); ?>>9</option>
  <option value="10" <?php selected('10', $instance['post_num']); ?>>10</option>
 </select>
</div>

<div class="tcd_ad_widget_box_wrap">
 <h3 class="tcd_ad_widget_headline"><?php _e('Headline setting', 'tcd-w'); ?></h3>
 <div class="tcd_ad_widget_box">

<div class="theme_option_message" style="box-shadow:none; margin:0 0 20px;">
 <p><?php _e('Headline options will not be applied to footer widget area.', 'tcd-w'); ?></p>
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Font color of headline', 'tcd-w'); ?></h3>
 <input name="<?php echo $this->get_field_name('headline_font_color'); ?>'" type="text" value="<?php echo $instance['headline_font_color']; ?>" data-default-color="#000000" class="color-picker">
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Background color of headline', 'tcd-w'); ?></h3>
 <input name="<?php echo $this->get_field_name('headline_bg_color'); ?>'" type="text" value="<?php echo $instance['headline_bg_color']; ?>" data-default-color="#ffffff" class="color-picker">
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Border color of headline', 'tcd-w'); ?></h3>
 <input name="<?php echo $this->get_field_name('headline_border_color'); ?>'" type="text" value="<?php echo $instance['headline_border_color']; ?>" data-default-color="#dddddd" class="color-picker">
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Headline icon display setting', 'tcd-w'); ?></h3>
 <input class="widget_headline_hide_icon" id="<?php echo $this->get_field_id('hide_icon'); ?>" name="<?php echo $this->get_field_name('hide_icon'); ?>" type="checkbox" value="1" <?php checked( '1', $instance['hide_icon'] ); ?> />
 <label for="<?php echo $this->get_field_id('hide_icon'); ?>"><?php _e('Hide headline icon', 'tcd-w'); ?></label>
</div>
<div class="widget_headline_icon_area"<?php if($instance['hide_icon']) { echo ' style="display:none;"'; }; ?>>
 <div class="tcd_widget_content">
  <h3 class="tcd_widget_headline"><?php _e('Icon background color', 'tcd-w'); ?></h3>
  <input name="<?php echo $this->get_field_name('icon_color'); ?>'" type="text" value="<?php echo $instance['icon_color']; ?>" data-default-color="#000000" class="color-picker">
 </div>
 <div class="tcd_widget_content">
  <h3 class="tcd_widget_headline"><?php _e('Icon image', 'tcd-w'); ?></h3>
  <div class="widget_media_upload cf cf_media_field hide-if-no-js <?php echo $this->get_field_id('icon_image'); ?>">
   <input type="hidden" value="<?php echo $instance['icon_image']; ?>" id="<?php echo $this->get_field_id('icon_image'); ?>" name="<?php echo $this->get_field_name('icon_image'); ?>" class="cf_media_id">
   <div class="preview_field"><?php if($instance['icon_image']){ echo wp_get_attachment_image($instance['icon_image'], 'full'); }; ?></div>
   <div class="buttton_area">
    <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
    <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$instance['icon_image']){ echo 'hidden'; }; ?>">
   </div>
  </div>
 </div>
</div>

 </div>
</div>
<?php

  } // end function form

} // end class


function register_ranking_list_widget() {
	register_widget( 'ranking_list_widget' );
}
add_action( 'widgets_init', 'register_ranking_list_widget' );


?>

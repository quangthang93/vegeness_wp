<?php

class premium_recipe_list_widget extends WP_Widget {

  function __construct() {
    $options = get_design_plus_option();
    $recipe_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );
    parent::__construct(
      'premium_recipe_list_widget',// ID
      sprintf(__('Premium %s list (tcd ver)', 'tcd-w'), $recipe_label),
      array(
        'classname' => 'premium_recipe_list_widget',
        'description' => sprintf(__('Display premium %s list for login user.', 'tcd-w'), $recipe_label)
      )
    );
  }

  // Extract Args //
  function widget($args, $instance) {

    extract( $args );
    $title = apply_filters('widget_title', $instance['title']);
    $post_num = $instance['post_num'];
    $show_date = $instance['show_date'];

    $post_order = $instance['post_order'];
    if($post_order=='date2'){ $order = 'ASC'; } else { $order = 'DESC'; };
    if($post_order=='date1'||$post_order=='date2'){ $post_order = 'date'; };

    // Before widget //
    echo $before_widget;

    // Title of widget //
    if ( $title ) { echo $before_title . $title . $after_title; }

    // Widget output //
    $args = array('post_type' => 'recipe', 'posts_per_page' => $post_num, 'ignore_sticky_posts' => 1, 'orderby' => $post_order, 'order' => $order, 'meta_key' => 'premium_recipe', 'meta_value' => '1');


    $options = get_design_plus_option();
    $styled_post_list = new WP_Query($args);

?>
<ol class="styled_post_list1 clearfix">
<?php
     if ($styled_post_list->have_posts()) {
       while ($styled_post_list->have_posts()) : $styled_post_list->the_post();
         if(has_post_thumbnail()) {
           $image = wp_get_attachment_image_src( get_post_thumbnail_id( $styled_post_list->ID ), 'size1' );
         } elseif($options['no_image1']) {
           $image = wp_get_attachment_image_src( $options['no_image1'], 'full' );
         } else {
           $image = array();
           $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image1.gif";
         }
?>
 <li class="clearfix<?php if($show_date) { echo ' has_date'; }; ?>">
  <a class="clearfix animate_background<?php if(!is_user_logged_in()) { echo ' register_link'; }; ?>" href="<?php if(is_user_logged_in()) { the_permalink(); } else { echo '#'; }; ?>">
   <div class="image_wrap">
    <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
   </div>
   <div class="title_area">
    <div class="title_area_inner">
     <h4 class="title"><span><?php the_title_attribute(); ?></span></h4>
     <?php if($show_date) { ?><p class="date"><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time></p><?php }; ?>
    </div>
   </div>
  </a>
 </li>
<?php endwhile; wp_reset_query(); } else { ?>
 <li class="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></li>
<?php }; ?>
</ol>
<?php

    // After widget //
    echo $after_widget;

  } // end function widget


  // Update Settings //
  function update($new_instance, $old_instance) {
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['post_num'] = $new_instance['post_num'];
    $instance['post_order'] = $new_instance['post_order'];
    $instance['show_date'] = $new_instance['show_date'];
    $instance['headline_font_color'] = $new_instance['headline_font_color'];
    $instance['headline_bg_color'] = $new_instance['headline_bg_color'];
    $instance['headline_border_color'] = $new_instance['headline_border_color'];
    $instance['icon_color'] = $new_instance['icon_color'];
    $instance['hide_icon'] = $new_instance['hide_icon'];
    $instance['icon_image'] = $new_instance['icon_image'];
    return $instance;
  }

  // Widget Control Panel //
  function form($instance) {
    $options = get_design_plus_option();
    $recipe_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );
    $defaults = array( 'title' => sprintf(__('Premium %s', 'tcd-w'), $recipe_label), 'post_num' => 3, 'post_order' => 'date1', 'show_date' => '', 'headline_font_color' => '#000000', 'headline_bg_color' => '#ffffff', 'headline_border_color' => '#dddddd', 'icon_color' => '#bcab4a', 'hide_icon' => '', 'icon_image' => false);
    $instance = wp_parse_args( (array) $instance, $defaults );
?>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Title', 'tcd-w'); ?></h3>
 <input class="widefat" name="<?php echo $this->get_field_name('title'); ?>'" type="text" value="<?php echo $instance['title']; ?>" />
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Number of post', 'tcd-w'); ?></h3>
 <select name="<?php echo $this->get_field_name('post_num'); ?>" class="widefat" style="width:100%;">
  <option value="3" <?php selected('3', $instance['post_num']); ?>>3</option>
  <option value="4" <?php selected('4', $instance['post_num']); ?>>4</option>
  <option value="5" <?php selected('5', $instance['post_num']); ?>>5</option>
  <option value="6" <?php selected('6', $instance['post_num']); ?>>6</option>
  <option value="7" <?php selected('7', $instance['post_num']); ?>>7</option>
  <option value="8" <?php selected('8', $instance['post_num']); ?>>8</option>
  <option value="9" <?php selected('9', $instance['post_num']); ?>>9</option>
  <option value="10" <?php selected('10', $instance['post_num']); ?>>10</option>
 </select>
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Post order', 'tcd-w'); ?></h3>
 <select name="<?php echo $this->get_field_name('post_order'); ?>" class="widefat" style="width:100%;">
  <option value="date1" <?php selected('date1', $instance['post_order']); ?>><?php _e('Date (DESC)', 'tcd-w'); ?></option>
  <option value="date2" <?php selected('date2', $instance['post_order']); ?>><?php _e('Date (ASC)', 'tcd-w'); ?></option>
  <option value="rand" <?php selected('rand', $instance['post_order']); ?>><?php _e('Random', 'tcd-w'); ?></option>
 </select>
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Display setting', 'tcd-w'); ?></h3>
 <input id="<?php echo $this->get_field_id('show_date'); ?>" name="<?php echo $this->get_field_name('show_date'); ?>" type="checkbox" value="1" <?php checked( '1', $instance['show_date'] ); ?> />
 <label for="<?php echo $this->get_field_id('show_date'); ?>"><?php _e('Display date', 'tcd-w'); ?></label>
</div>


<div class="tcd_ad_widget_box_wrap">
 <h3 class="tcd_ad_widget_headline"><?php _e('Headline setting', 'tcd-w'); ?></h3>
 <div class="tcd_ad_widget_box">

<div class="theme_option_message" style="box-shadow:none; margin:0 0 20px;">
 <p><?php _e('Headline options will not be applied to footer widget area.', 'tcd-w'); ?></p>
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Font color of headline', 'tcd-w'); ?></h3>
 <input name="<?php echo $this->get_field_name('headline_font_color'); ?>'" type="text" value="<?php echo $instance['headline_font_color']; ?>" data-default-color="#000000" class="color-picker">
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Background color of headline', 'tcd-w'); ?></h3>
 <input name="<?php echo $this->get_field_name('headline_bg_color'); ?>'" type="text" value="<?php echo $instance['headline_bg_color']; ?>" data-default-color="#ffffff" class="color-picker">
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Border color of headline', 'tcd-w'); ?></h3>
 <input name="<?php echo $this->get_field_name('headline_border_color'); ?>'" type="text" value="<?php echo $instance['headline_border_color']; ?>" data-default-color="#dddddd" class="color-picker">
</div>
<div class="tcd_widget_content">
 <h3 class="tcd_widget_headline"><?php _e('Headline icon display setting', 'tcd-w'); ?></h3>
 <input class="widget_headline_hide_icon" id="<?php echo $this->get_field_id('hide_icon'); ?>" name="<?php echo $this->get_field_name('hide_icon'); ?>" type="checkbox" value="1" <?php checked( '1', $instance['hide_icon'] ); ?> />
 <label for="<?php echo $this->get_field_id('hide_icon'); ?>"><?php _e('Hide headline icon', 'tcd-w'); ?></label>
</div>
<div class="widget_headline_icon_area"<?php if($instance['hide_icon']) { echo ' style="display:none;"'; }; ?>>
 <div class="tcd_widget_content">
  <h3 class="tcd_widget_headline"><?php _e('Icon background color', 'tcd-w'); ?></h3>
  <input name="<?php echo $this->get_field_name('icon_color'); ?>'" type="text" value="<?php echo $instance['icon_color']; ?>" data-default-color="#bcab4a" class="color-picker">
 </div>
 <div class="tcd_widget_content">
  <h3 class="tcd_widget_headline"><?php _e('Icon image', 'tcd-w'); ?></h3>
  <div class="widget_media_upload cf cf_media_field hide-if-no-js <?php echo $this->get_field_id('icon_image'); ?>">
   <input type="hidden" value="<?php echo $instance['icon_image']; ?>" id="<?php echo $this->get_field_id('icon_image'); ?>" name="<?php echo $this->get_field_name('icon_image'); ?>" class="cf_media_id">
   <div class="preview_field"><?php if($instance['icon_image']){ echo wp_get_attachment_image($instance['icon_image'], 'full'); }; ?></div>
   <div class="buttton_area">
    <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
    <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$instance['icon_image']){ echo 'hidden'; }; ?>">
   </div>
  </div>
 </div>
</div>

 </div>
</div>
<?php

  } // end function form

} // end class


function register_premium_recipe_list_widget() {
	register_widget( 'premium_recipe_list_widget' );
}
add_action( 'widgets_init', 'register_premium_recipe_list_widget' );


?>

<?php
     get_header();
     $options = get_design_plus_option();

     if (is_search()) {
       if ( have_posts() ) {
         $headline = sprintf(__('Search results for - %s', 'tcd-w'), get_search_query());
       } else {
         $headline = __('Search result','tcd-w');
       };
     };
?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <h3 class="design_headline clearfix rich_font"><span class="search_keyword"><?php echo esc_html($headline); ?></span></h3>

  <div id="blog_archive">

   <?php if ( have_posts() ) : ?>

   <div id="post_list" class="clearfix">
    <?php
         while ( have_posts() ) : the_post();
           $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
           if( get_post_meta($post->ID,'premium_post',true) ){
             $premium_recipe = get_post_meta($post->ID,'premium_post',true);
           }
           $post_type = $post->post_type;
           if($post_type == 'post' && $options['all_premium_post']){
             $premium_recipe = '1';
           }
           if(has_post_thumbnail()) {
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
           } elseif($options['no_image2']) {
             $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
           } else {
             $image = array();
             $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
           }
    ?>
    <article class="item clearfix<?php if( $post_type == 'post' ){ echo ' premium_post'; }; ?>">
     <a class="link animate_background<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
      <div class="image_wrap">
       <?php if($premium_recipe) { ?><div class="premium_icon"></div><?php }; ?>
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
      <div class="title_area">
       <h3 class="title rich_font"><?php the_title(); ?></h3>
       <?php if ( $options['show_archive_blog_date'] || $options['show_archive_blog_category'] ){ ?>
       <ul class="post_meta clearfix">
        <?php if ( $options['show_archive_blog_date'] ){ ?><li class="date"><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time></li><?php }; ?>
       </ul>
       <?php }; ?>
      </div>
     </a>
    </article>
    <?php endwhile; ?>
   </div><!-- END .post_list2 -->

   <?php get_template_part('template-parts/navigation'); ?>

   <?php else: ?>

   <p id="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></p>

   <?php endif; ?>

  </div><!-- END #blog_archive -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
<?php
     $options = get_design_plus_option();
     $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
     if( $premium_recipe && ! current_user_can( 'read' ) ) {
       $current_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
       $redirect = add_query_arg( 'redirect_to', rawurlencode( rawurldecode( $current_url ) ), get_tcd_membership_memberpage_url( 'login' ) );
       wp_safe_redirect( $redirect );
       exit;
     }
     get_header();
     $args = array( 'orderby' => 'term_order' );
     $recipe_category = wp_get_post_terms( $post->ID, 'recipe_category' ,$args);
     if ( $recipe_category && ! is_wp_error($recipe_category) ) {
       foreach ( $recipe_category as $cat ) :
         if($cat->parent == 0) {
           $cat_name = $cat->name;
           $cat_id = $cat->term_id;
         } else {
           $child_cat_name = $cat->name;
           $child_cat_id = $cat->term_id;
         }
       endforeach;
     };
?>

<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <?php
       // メインコンテンツ -----------------------------------------------------------------
       $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
       $recipe_desc = get_post_meta($post->ID, 'recipe_desc', true);
       $query_obj = get_queried_object();
       $author_id = $query_obj->post_author;
       $author_url = get_author_posts_url($author_id);
       $user_data = get_userdata($author_id);
  ?>
  <div id="recipe_main_content">
   <div id="recipe_title_area" class="clearfix">
    <?php if ($recipe_category) { ?>
    <a href="<?php echo esc_url(get_term_link($cat_id,'recipe_category')); ?>" class="rich_font cat_id_<?php echo esc_attr($cat_id); ?> parent_category"><span><?php echo esc_html($cat_name); ?></span></a>
    <?php }; ?>
    <div class="title_area">
     <h1 class="title rich_font entry-title"><?php the_title(); ?></h1>
     <ul class="meta clearfix">
      <?php if ($options['single_recipe_show_author']){ ?><li class="post_author"><?php printf('<span class="author_label">%s : </span>',$options['single_recipe_author_label']); ?><a href="<?php echo esc_url($author_url); ?>"><?php echo esc_html($user_data->display_name); ?></a></li><?php }; ?>
      <?php if ($recipe_category && $child_cat_name && $options['single_recipe_show_category']) { ?>
      <li class="child_category">
       <?php
            if ( $recipe_category && ! is_wp_error($recipe_category) ) {
              foreach ( $recipe_category as $cat ) :
                if($cat->parent != 0) {
       ?>
       <a href="<?php echo esc_url(get_term_link($cat->term_id,'recipe_category')); ?>"><?php echo esc_html($cat->name); ?></a>
       <?php
                }
              endforeach;
            };
       ?>
      </li>
      <?php }; ?>
      <?php if ($options['single_recipe_show_post_view']){ ?><li class="post_view"><?php if($recipe_type != 'type2') { _e('Hits:', 'tcd-w'); } else { _e('Views:', 'tcd-w'); }; ?><?php the_post_views(); ?></li><?php }; ?>
     </ul>
     <?php
          // お気に入りボタン -------------------------------------
          if (is_user_logged_in() && $options['show_recipe_like_button']) {
            if ( !is_liked( $post->ID ) ) {
     ?>
     <a class="like_button" id="like_button" href="#" data-post-id="<?php the_ID(); ?>"><span class="like_label"><?php echo esc_html($options['recipe_like_button_label']); ?></span><span class="like_message"><?php _e('Added to favorite list', 'tcd-w'); ?></span></a>
     <?php } else { ?>
     <a class="like_button" id="delete_like_button" href="#" data-post-id="<?php the_ID(); ?>"><span class="like_label"><?php echo esc_html($options['recipe_delete_like_button_label']); ?></span><span class="like_message"><?php _e('Deleted from favorite list', 'tcd-w'); ?></span></a>
     <?php
            };
          };
     ?>
    </div>
   </div><!-- END #recipe_title_area -->
   <?php
        // 動画 -----------------------------------------------------
        if($recipe_type == 'type1') {
          $recipe_video = get_post_meta($post->ID, 'recipe_video', true);
          if(!empty($recipe_video)){
   ?>
   <div id="recipe_video_wrap">
    <div id="recipe_video">
     <video src="<?php echo esc_url(wp_get_attachment_url($recipe_video)); ?>" <?php if(has_post_thumbnail()) { ?>poster="<?php echo esc_attr($image[0]); ?>"<?php }; ?> id="recipe_video_mp4" controls></video>
    </div>
   </div>
   <?php
          };
        // 画像スライダー -----------------------------------------------------
        } elseif($recipe_type == 'type2') {
          $recipe_image_list = get_post_meta($post->ID, 'recipe_image_list', true);
          if(!empty($recipe_image_list)){
   ?>
   <div id="recipe_image_slider_wrap">
    <div id="recipe_image_slider">
     <?php
          if(has_post_thumbnail()) {
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
     ?>
     <div class="item" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
     <?php }; ?>
     <?php
          foreach ( $recipe_image_list as $key => $value ) :
            $image = wp_get_attachment_image_src($value['image'], 'full');
            if(!empty($image)) {
     ?>
     <div class="item" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
     <?php
            };
          endforeach;
     ?>
    </div><!-- END #recipe_image_slider -->
    <div id="recipe_image_slider_nav">
     <?php
          foreach ( $recipe_image_list as $key => $value ) :
            $image = wp_get_attachment_image_src($value['image'], 'size1');
            if(!empty($image)) {
     ?>
     <div class="item" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
     <?php
            };
          endforeach;
     ?>
    </div><!-- END #recipe_image_slider_nav -->
   </div><!-- END #recipe_image_slider_wrap -->
   <?php
          };
        // YouTube -----------------------------------------------------
        } elseif($recipe_type == 'type3') {
   ?>
   <div id="youtube_player_wrap">
    <div id="youtube_player_wrap_inner">
     <div id="youtube_player"></div>
    </div>
   </div>
   <?php }; ?>
   <?php
        $recipe_time_label = get_post_meta($post->ID, 'recipe_time_label', true);
        if(empty($recipe_time_label)){
          $recipe_time_label = __('Cooking time', 'tcd-w');
        }
        $recipe_time = get_post_meta($post->ID, 'recipe_time', true);
        $recipe_price_label = get_post_meta($post->ID, 'recipe_price_label', true);
        if(empty($recipe_price_label)){
          $recipe_price_label = __('Estimated cost', 'tcd-w');
        }
        $recipe_price = get_post_meta($post->ID, 'recipe_price', true);
        if(has_post_thumbnail()) {
          $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size4' );
        }
   ?>
   <?php if($recipe_time || $recipe_price){ ?>
   <dl id="recipe_video_data" class="clearfix">
    <?php if($recipe_time){ ?>
    <dt><?php echo esc_html($recipe_time_label); ?></dt>
    <dd><?php echo esc_html($recipe_time); ?></dd>
    <?php }; ?>
    <?php if($recipe_price){ ?>
    <dt><?php echo esc_html($recipe_price_label); ?></dt>
    <dd><?php echo esc_html($recipe_price); ?></dd>
    <?php }; ?>
   </dl>
   <?php }; ?>
   <?php
        // copy title&url button ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        if($options['single_recipe_show_copy_button']) {
   ?>
   <div class="single_copy_title_url" id="single_copy_title_url_top">
    <button class="single_copy_title_url_btn" data-clipboard-text="<?php echo esc_attr( strip_tags( get_the_title() ) . ' ' . get_permalink() ); ?>" data-clipboard-copied="<?php echo esc_attr( __( 'COPIED Title&amp;URL', 'tcd-w' ) ); ?>"><?php _e( 'COPY Title&amp;URL', 'tcd-w' ); ?></button>
   </div>
   <?php }; ?>
   <?php if(!empty($recipe_desc)) { ?>
   <div class="post_content clearfix">
    <?php echo do_shortcode( wpautop(wp_kses_post($recipe_desc)) ); ?>
   </div>
   <?php }; ?>
  </div>

  <?php
       // コンテンツビルダーここから ----------------------------------------------------------
       $recipe_contents_builder = get_post_meta( $post->ID, 'recipe_contents_builder', true );
       if ( $recipe_contents_builder && is_array( $recipe_contents_builder ) ) :
         foreach( $recipe_contents_builder as $key => $content ) :

            // 材料 ------------------------------------------------------
            if ( 'recipe_material' === $content['content_select'] ) {
  ?>
  <div class="recipe_material num<?php echo esc_attr($key); ?>">
   <?php if (!empty($content['headline'])) { ?>
   <h3 class="design_headline clearfix rich_font<?php if (!empty($content['hide_icon'])) { echo ' hide_icon'; }; ?>"><?php echo esc_html($content['headline']); ?></h3>
   <?php }; ?>
   <?php if (!empty($content['material_list']) && is_array( $content['material_list'] ) ) : ?>
   <dl class="clearfix">
    <dt style="background:<?php if (!empty($content['list_bg_color'])) { echo esc_attr($content['list_bg_color']); }; ?>; color:<?php if (!empty($content['list_font_color'])) { echo esc_attr($content['list_font_color']); }; ?>;"><?php if (!empty($content['list_label1'])) { echo esc_html($content['list_label1']); }; ?></dt>
    <dd style="background:<?php if (!empty($content['list_bg_color'])) { echo esc_attr($content['list_bg_color']); }; ?>; color:<?php if (!empty($content['list_font_color'])) { echo esc_attr($content['list_font_color']); }; ?>;"><?php if (!empty($content['list_label2'])) { echo esc_html($content['list_label2']); }; ?></dd>
    <?php foreach ( $content['material_list'] as $key => $value ) : ?>
    <?php if(!empty($value['name'])){ ?><dt><?php echo esc_html($value['name']); ?></dt><?php }; ?>
    <?php if(!empty($value['amount'])){ ?><dd><?php echo esc_html($value['amount']); ?></dd><?php }; ?>
    <?php endforeach; ?>
   </dl>
   <?php endif; ?>
  </div><!-- END .recipe_material -->

  <?php
       // 作り方 ------------------------------------------------------
       } elseif ( 'recipe_howto' === $content['content_select'] ) {
  ?>
  <div class="recipe_howto num<?php echo esc_attr($key); ?>">
   <?php if (!empty($content['headline'])) { ?>
   <h3 class="design_headline clearfix rich_font<?php if (!empty($content['hide_icon'])) { echo ' hide_icon'; }; ?>"><?php echo esc_html($content['headline']); ?></h3>
   <?php }; ?>
   <?php if (!empty($content['howto_list']) && is_array( $content['howto_list'] ) ) : ?>
   <dl class="clearfix">
    <?php
         $i = 1;
         foreach ( $content['howto_list'] as $key => $value ) :
    ?>
    <dt style="background:<?php if (!empty($content['list_bg_color'])) { echo esc_attr($content['list_bg_color']); }; ?>; color:<?php if (!empty($content['list_font_color'])) { echo esc_attr($content['list_font_color']); }; ?>;"><?php echo esc_html($i); ?></dt>
    <?php if(!empty($value['content'])){ ?><dd><p><?php echo nl2br(wp_kses_post($value['content'])); ?></p></dd><?php }; ?>
    <?php $i++; endforeach; ?>
   </dl>
   <?php endif; ?>
  </div><!-- END .recipe_howto -->

  <?php
       // ポイント ------------------------------------------------------
       } elseif ( 'recipe_point' === $content['content_select'] ) {
  ?>
  <div class="recipe_point num<?php echo esc_attr($key); ?>">
   <?php if (!empty($content['headline'])) { ?>
   <h3 class="design_headline clearfix rich_font<?php if (!empty($content['hide_icon'])) { echo ' hide_icon'; }; ?>"><?php echo esc_html($content['headline']); ?></h3>
   <?php }; ?>
   <?php if (!empty($content['desc'])) { ?>
   <div class="post_content clearfix">
    <?php echo do_shortcode( wpautop(wp_kses_post($content['desc'])) ); ?>
   </div>
   <?php }; ?>
  </div><!-- END .recipe_howto -->

  <?php
       // フリースペース ------------------------------------------------------
       } elseif ( 'recipe_free' === $content['content_select'] ) {
  ?>
  <div class="recipe_free num<?php echo esc_attr($key); ?>">
   <?php if (!empty($content['desc'])) { ?>
   <div class="post_content clearfix">
    <?php echo do_shortcode( wpautop(wp_kses_post($content['desc'])) ); ?>
   </div>
   <?php }; ?>
  </div><!-- END .recipe_free -->

  <?php
           };
         endforeach;
       endif;
       // コンテンツビルダーここまで
  ?>


  <?php
       // ページナビ ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
       if ($options['show_recipe_nav']) :
  ?>
  <div id="next_prev_post" class="clearfix">
   <?php next_prev_post_link(); ?>
  </div>
  <?php endif; ?>


  <?php
       // 関連レシピ ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
       if ($options['show_related_recipe'] && $recipe_category){
         $post_num = $options['related_recipe_post_num'];
         $args = array( 'post_type' => 'recipe', 'posts_per_page' => $post_num, 'tax_query' => array( array( 'taxonomy' => 'recipe_category', 'field' => 'term_id', 'terms' => $cat_id ) ) );
         $recipe_query = new wp_query($args);
         if($recipe_query->have_posts()):
  ?>
  <div id="related_recipe">
   <h3 class="design_headline clearfix rich_font<?php if($options['hide_related_recipe_headline_icon']){ echo ' hide_icon'; }; ?>"><?php echo esc_html($options['related_recipe_headline']); ?></h3>
   <div class="recipe_list clearfix">
    <?php
         while($recipe_query->have_posts()): $recipe_query->the_post();
           $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
           $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
           if(has_post_thumbnail()) {
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
           } elseif($options['no_image2']) {
             $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
           } else {
             $image = array();
             $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
           }
    ?>
    <article class="item<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>">
     <a class="link animate_background" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
      <div class="image_wrap">
       <?php if($premium_recipe && $options['related_recipe_list_show_premium_icon']) { ?><div class="premium_icon"></div><?php }; ?>
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
     </a>
     <div class="title_area">
      <h3 class="title"><a href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>"><span><?php the_title(); ?></span></a></h3>
      <?php if ($options['related_recipe_list_show_post_view']){ ?><p class="post_meta"><?php if($recipe_type != 'type2') { _e('Hits:', 'tcd-w'); } else { _e('Views:', 'tcd-w'); }; ?><?php the_post_views(); ?></p><?php }; ?>
     </div>
    </article>
    <?php endwhile; ?>
   </div><!-- END .recipe_list1 -->
  <?php endif; wp_reset_query(); ?>
  </div><!-- END #related_recipe -->
  <?php }; ?>


 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
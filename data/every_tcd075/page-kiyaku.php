<?php
/*
Template Name:Privacy policy page
*/
__('Privacy policy page', 'tcd-w');
?>
<?php
     get_header();
     $options = get_design_plus_option();
?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <h3 class="design_headline clearfix<?php if(get_post_meta($post->ID, 'kiyaku_page_headline_hide_icon', true)) { echo ' hide_icon'; }; ?>" id="kiyaku_headline"><?php the_title(); ?></h3>

  <div id="kiyaku">

   <?php
        $desc = get_post_meta($post->ID, 'kiyaku_desc', true);
        if($desc){
   ?>
   <div class="post_content clearfix">
    <?php echo do_shortcode( wpautop(wp_kses_post($desc)) ); ?>
   </div>
   <?php }; ?>

   <?php
        $data_list = get_post_meta($post->ID, 'kiyaku_data_list', true);
        if($data_list){
   ?>
   <div id="kiyaku_list">
    <?php foreach ( $data_list as $key => $value ) : ?>
    <?php if(!empty($value['title'])) { ?>
    <h3 class="kiyaku_headline"><?php echo wp_kses_post(nl2br($value['title'])); ?></h3>
    <?php }; ?>
    <?php if(!empty($value['desc'])) { ?>
    <div class="post_content clearfix">
     <?php echo do_shortcode( wpautop(wp_kses_post($value['desc'])) ); ?>
    </div>
    <?php }; ?>
    <?php endforeach; ?>
   </div>
   <?php }; ?>

  </div><!-- END #kiyaku -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
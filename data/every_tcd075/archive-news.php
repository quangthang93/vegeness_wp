<?php
     get_header();
     $options = get_design_plus_option();
?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <div id="news_archive">

   <h3 class="design_headline clearfix rich_font<?php if($options['hide_archive_news_headline_icon']){ echo ' hide_icon'; }; ?>"><?php echo esc_html($options['news_label']); ?></h3>

   <?php if ( have_posts() ) : ?>

   <div id="news_list" class="clearfix">
    <?php
         while ( have_posts() ) : the_post();
           if(has_post_thumbnail()) {
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
           } elseif($options['no_image2']) {
             $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
           } else {
             $image = array();
             $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
           }
    ?>
    <article class="item clearfix <?php if( !has_post_thumbnail() && $options['news_no_image'] == 'hide' ) { echo 'no_image'; }; ?>">
     <a class="link animate_background" href="<?php the_permalink(); ?>">
      <?php if( has_post_thumbnail() && $options['news_no_image'] == 'hide') { ?>
      <div class="image_wrap">
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
      <?php } elseif($options['news_no_image'] != 'hide') { ?>
      <div class="image_wrap">
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
      <?php }; ?>
      <div class="title_area">
       <div class="title_area_inner">
        <?php if ( $options['show_archive_news_date'] ){ ?><p class="date"><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time></p><?php }; ?>
        <h3 class="title rich_font"><span><?php the_title(); ?></span></h3>
       </div>
      </div>
     </a>
    </article>
    <?php endwhile; ?>
   </div><!-- END .post_list2 -->

   <?php get_template_part('template-parts/navigation'); ?>

   <?php else: ?>

   <p id="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></p>

   <?php endif; ?>

  </div><!-- END #archive_news -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
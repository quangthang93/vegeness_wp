<?php
     function tcd_head() {
       $options = get_design_plus_option();
?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/design-plus.css?ver=<?php echo version_num(); ?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/sns-botton.css?ver=<?php echo version_num(); ?>">
<link rel="stylesheet" media="screen and (max-width:1210px)" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css?ver=<?php echo version_num(); ?>">
<link rel="stylesheet" media="screen and (max-width:1210px)" href="<?php echo get_template_directory_uri(); ?>/css/footer-bar.css?ver=<?php echo version_num(); ?>">

<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js?ver=<?php echo version_num(); ?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jscript.js?ver=<?php echo version_num(); ?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/comment.js?ver=<?php echo version_num(); ?>"></script>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/simplebar.css?ver=<?php echo version_num(); ?>">
<script src="<?php echo get_template_directory_uri(); ?>/js/simplebar.min.js?ver=<?php echo version_num(); ?>"></script>

<?php if(is_mobile()) { ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/footer-bar.js?ver=<?php echo version_num(); ?>"></script>
<?php }; ?>

<?php
     if($options['header_fix'] != 'type1') {
?>
<script src="<?php echo get_template_directory_uri(); ?>/js/header_fix.js?ver=<?php echo version_num(); ?>"></script>
<?php }; ?>
<?php
     if($options['mobile_header_fix'] == 'type2') {
?>
<script src="<?php echo get_template_directory_uri(); ?>/js/header_fix_mobile.js?ver=<?php echo version_num(); ?>"></script>
<?php };  ?>

<style type="text/css">
<?php
     // フォントタイプの設定　------------------------------------------------------------------
?>

<?php
     // 基本のフォントタイプ
     if($options['font_type'] == 'type1') {
?>
body, input, textarea { font-family: Arial, "ヒラギノ角ゴ ProN W3", "Hiragino Kaku Gothic ProN", "メイリオ", Meiryo, sans-serif; }
<?php } elseif($options['font_type'] == 'type2') { ?>
body, input, textarea { font-family: "Hiragino Sans", "ヒラギノ角ゴ ProN", "Hiragino Kaku Gothic ProN", "游ゴシック", YuGothic, "メイリオ", Meiryo, sans-serif; }
<?php } else { ?>
body, input, textarea { font-family: "Times New Roman" , "游明朝" , "Yu Mincho" , "游明朝体" , "YuMincho" , "ヒラギノ明朝 Pro W3" , "Hiragino Mincho Pro" , "HiraMinProN-W3" , "HGS明朝E" , "ＭＳ Ｐ明朝" , "MS PMincho" , serif; }
<?php }; ?>

<?php
     // 見出しのフォントタイプ
     if($options['headline_font_type'] == 'type1') {
?>
.rich_font, .p-vertical { font-family: Arial, "ヒラギノ角ゴ ProN W3", "Hiragino Kaku Gothic ProN", "メイリオ", Meiryo, sans-serif; }
<?php } elseif($options['headline_font_type'] == 'type2') { ?>
.rich_font, .p-vertical { font-family: "Hiragino Sans", "ヒラギノ角ゴ ProN", "Hiragino Kaku Gothic ProN", "游ゴシック", YuGothic, "メイリオ", Meiryo, sans-serif; font-weight:500; }
<?php } else { ?>
.rich_font, .p-vertical { font-family: "Times New Roman" , "游明朝" , "Yu Mincho" , "游明朝体" , "YuMincho" , "ヒラギノ明朝 Pro W3" , "Hiragino Mincho Pro" , "HiraMinProN-W3" , "HGS明朝E" , "ＭＳ Ｐ明朝" , "MS PMincho" , serif; font-weight:500; }
<?php }; ?>

.rich_font_type1 { font-family: Arial, "ヒラギノ角ゴ ProN W3", "Hiragino Kaku Gothic ProN", "メイリオ", Meiryo, sans-serif; }
.rich_font_type2 { font-family: "Hiragino Sans", "ヒラギノ角ゴ ProN", "Hiragino Kaku Gothic ProN", "游ゴシック", YuGothic, "メイリオ", Meiryo, sans-serif; font-weight:500; }
.rich_font_type3 { font-family: "Times New Roman" , "游明朝" , "Yu Mincho" , "游明朝体" , "YuMincho" , "ヒラギノ明朝 Pro W3" , "Hiragino Mincho Pro" , "HiraMinProN-W3" , "HGS明朝E" , "ＭＳ Ｐ明朝" , "MS PMincho" , serif; font-weight:500; }

<?php
     // 本文のフォントタイプ
     if(is_single()) {
       if($options['content_font_type'] == 'type1') {
?>
.post_content, #next_prev_post { font-family: Arial, "ヒラギノ角ゴ ProN W3", "Hiragino Kaku Gothic ProN", "メイリオ", Meiryo, sans-serif; }
<?php } elseif($options['content_font_type'] == 'type2') { ?>
.post_content, #next_prev_post { font-family: "Hiragino Sans", "ヒラギノ角ゴ ProN", "Hiragino Kaku Gothic ProN", "游ゴシック", YuGothic, "メイリオ", Meiryo, sans-serif; }
<?php } else { ?>
.post_content, #next_prev_post { font-family: "Times New Roman" , "游明朝" , "Yu Mincho" , "游明朝体" , "YuMincho" , "ヒラギノ明朝 Pro W3" , "Hiragino Mincho Pro" , "HiraMinProN-W3" , "HGS明朝E" , "ＭＳ Ｐ明朝" , "MS PMincho" , serif; }
<?php }; }; ?>

<?php
     // グローバルメニュー
?>
#global_menu > ul > li > a { color:<?php echo esc_html($options['global_menu_font_color']); ?>; }
#global_menu > ul > li > a:after { background:<?php echo esc_html($options['global_menu_border_color']); ?>; }
#global_menu ul ul a { color:<?php echo esc_html($options['global_menu_child_font_color']); ?>; background:<?php echo esc_html($options['global_menu_child_bg_color']); ?>; }
#global_menu ul ul a:hover { background:<?php echo esc_html($options['global_menu_child_bg_color_hover']); ?>; }
#global_menu ul ul li.menu-item-has-children > a:before { color:<?php echo esc_html($options['global_menu_child_font_color']); ?>; }
<?php
     // ドロワーメニュー
?>
#drawer_menu { background:<?php echo esc_html($options['mobile_menu_bg_color']); ?>; }
#mobile_menu a { color:<?php echo esc_html($options['mobile_menu_font_color']); ?>; background:<?php echo esc_html($options['mobile_menu_bg_color']); ?>; border-bottom:1px solid <?php echo esc_html($options['mobile_menu_border_color']); ?>; }
#mobile_menu li li a { color:<?php echo esc_html($options['mobile_menu_child_font_color']); ?>; background:<?php echo esc_html($options['mobile_menu_sub_menu_bg_color']); ?>; }
#mobile_menu a:hover, #drawer_menu .close_button:hover, #mobile_menu .child_menu_button:hover { color:<?php echo esc_html($options['mobile_menu_font_hover_color']); ?>; background:<?php echo esc_html($options['mobile_menu_bg_hover_color']); ?>; }
#mobile_menu li li a:hover { color:<?php echo esc_html($options['mobile_menu_child_font_hover_color']); ?>; }
<?php
     // メガメニュー
?>
.megamenu_recipe_category_list .headline { color:<?php echo esc_attr($options['mega_menu_a_headline_color']); ?>; font-size:<?php echo esc_attr($options['mega_menu_a_headline_font_size']); ?>px; }
.megamenu_recipe_category_list .headline a { color:<?php echo esc_attr($options['mega_menu_a_headline_color']); ?>; }
.megamenu_blog_list .menu_area a:hover, .megamenu_blog_list .menu_area li.active a, .megamenu_blog_list .post_list { background:<?php echo esc_html($options['mega_menu_b_bg_color2']); ?>; }
<?php
     // ログインボタン
     $header_search_bg_color = hex2rgb($options['header_search_bg_color']);
     $header_search_bg_color = implode(",",$header_search_bg_color);
?>
#header_search { background:rgba(<?php echo esc_html($header_search_bg_color); ?>,<?php echo esc_html($options['header_search_bg_opacity']); ?>); }
#header_login, #header_logout { color:<?php echo esc_html($options['header_login_font_color']); ?>; background:<?php echo esc_html($options['header_login_bg_color']); ?>; }
#header_login:hover, #header_logout:hover { color:<?php echo esc_html($options['header_login_font_color_hover']); ?>; background:<?php echo esc_html($options['header_login_bg_color_hover']); ?>; }
#header_register { color:<?php echo esc_html($options['header_register_font_color']); ?>; background:<?php echo esc_html($options['header_register_bg_color']); ?>; }
#header_register:hover { color:<?php echo esc_html($options['header_register_font_color_hover']); ?>; background:<?php echo esc_html($options['header_register_bg_color_hover']); ?>; }
<?php
     // フッター -----------------------------------------------------------------------------------
     $footer_widget_border_color = hex2rgb($options['footer_widget_border_color']);
     $footer_widget_border_color = implode(",",$footer_widget_border_color);
?>
.footer_headline { color:<?php echo esc_html($options['footer_widget_headline_color']); ?>; }
.footer_menu ul li a { color:<?php echo esc_html($options['footer_widget_font_color']); ?>; }
#footer_menu, #footer_category_menu, #footer_widget { border-color:rgba(<?php echo esc_html($footer_widget_border_color); ?>,<?php echo esc_html($options['footer_widget_border_opacity']); ?>); }
<?php
     // フッターボタン
     if( ( is_mobile() && $options['show_footer_button1']) || ( is_mobile() && $options['show_footer_button2']) ) {
?>
#footer_button .button1 a { color:<?php echo esc_html($options['footer_button_font_color1']); ?>; background:<?php echo esc_html($options['footer_button_bg_color1']); ?>; }
#footer_button .button1 a:hover { color:<?php echo esc_html($options['footer_button_font_color_hover1']); ?>; background:<?php echo esc_html($options['footer_button_bg_color_hover1']); ?>; }
#footer_button .button2 a { color:<?php echo esc_html($options['footer_button_font_color2']); ?>; background:<?php echo esc_html($options['footer_button_bg_color2']); ?>; }
#footer_button .button2 a:hover { color:<?php echo esc_html($options['footer_button_font_color_hover2']); ?>; background:<?php echo esc_html($options['footer_button_bg_color_hover2']); ?>; }
<?php }; ?>

<?php
     // ウィジェット -----------------------------------------------------------------------------
     widget_css();
?>

<?php
     // レシピ -----------------------------------------------------------------------------

     // カテゴリーの色設定
     $categories = get_terms( 'recipe_category', array( 'hide_empty' => true, 'parent' => 0) );
     if ( $categories && ! is_wp_error( $categories ) ) :
       foreach ( $categories as $cat ):
         $cat_id = $cat->term_id;
         $custom_fields = get_option( 'taxonomy_' . $cat_id, array() );
         $main_color = '#009fe1';
         if (!empty($custom_fields['main_color'])){ $main_color = $custom_fields['main_color']; }
         $sub_color = '#0079ab';
         if (!empty($custom_fields['sub_color'])){ $sub_color = $custom_fields['sub_color']; }
?>
.cat_id_<?php echo esc_attr($cat_id); ?> { background:<?php echo esc_html($main_color); ?>; }
.cat_id_<?php echo esc_attr($cat_id); ?>:hover { background:<?php echo esc_html($sub_color); ?>; }
<?php
      endforeach;
     endif;

     // プレミアムレシピアイコン
     $recipe_premium_icon_image_id = $options['recipe_premium_icon_image'];
     if($recipe_premium_icon_image_id) {
       $recipe_premium_icon_image = wp_get_attachment_image_src($recipe_premium_icon_image_id, 'full');
?>
.premium_icon { background:<?php echo esc_attr($options['recipe_premium_icon_color']); ?> url(<?php echo esc_attr($recipe_premium_icon_image[0]); ?>) no-repeat center !important; }
<?php } else { ?>
.premium_icon { background:<?php echo esc_attr($options['recipe_premium_icon_color']); ?> !important; }
<?php if (strtoupper(get_locale()) == 'JA') { ?>
.premium_icon:before { content:'\e911'; }
<?php } else { ?>
.premium_icon:before { content:'\e912'; font-size:7px !important; width:50px !important; }
@media screen and (max-width:650px) {
  .premium_icon:before { font-size:5px !important; width:30px !important; }
}
<?php }; ?>
<?php
     };
     // プレミアムブログアイコン
     $blog_premium_icon_image_id = $options['blog_premium_icon_image'];
     if($blog_premium_icon_image_id) {
       $blog_premium_icon_image = wp_get_attachment_image_src($blog_premium_icon_image_id, 'full');
?>
.index_blog .premium_icon, #post_list .premium_post .premium_icon, #related_post .premium_icon { background:<?php echo esc_attr($options['blog_premium_icon_color']); ?> url(<?php echo esc_attr($blog_premium_icon_image[0]); ?>) no-repeat center !important; }
@media screen and (max-width:650px) {
  .index_blog .premium_icon, #post_list .premium_post .premium_icon, #related_post .premium_icon { -webkit-background-size:20px !important; background-size:20px !important; }
}
<?php } else { ?>
.index_blog .premium_icon, #post_list .premium_post .premium_icon, #related_post .premium_icon { background:<?php echo esc_attr($options['blog_premium_icon_color']); ?> !important; }
<?php if (strtoupper(get_locale()) == 'JA') { ?>
.index_blog .premium_icon:before, #post_list .premium_post .premium_icon:before, #related_post .premium_icon:before { content:'\e911'; }
<?php } else { ?>
.index_blog .premium_icon:before, #post_list .premium_post .premium_icon:before, #related_post .premium_icon:before { content:'\e912'; font-size:7px !important; width:50px !important; }
@media screen and (max-width:650px) {
  .index_blog .premium_icon:before, #post_list .premium_post .premium_icon:before, #related_post .premium_icon:before { font-size:5px !important; width:30px !important; }
}
<?php }; ?>
<?php
     };
     // レシピ詳細ページ
     if(is_singular('recipe')) {
       global $post;
       $like_color = $options['recipe_like_button_color'];
       // 関連レシピ
       $related_headline_image_id = $options['related_recipe_headline_image'];
       if($related_headline_image_id) { $related_headline_image = wp_get_attachment_image_src($related_headline_image_id, 'full'); }
?>
#recipe_title_area .title { font-size:<?php echo esc_html($options['single_recipe_title_font_size']); ?>px; }
#recipe_main_content, .recipe_material, .recipe_howto, .recipe_point, recipe_free { font-size:<?php echo esc_html($options['single_recipe_content_font_size']); ?>px; }
#recipe_title_area .like_button { border-color:<?php echo esc_attr($like_color); ?>; color:<?php echo esc_attr($like_color); ?>; }
#recipe_title_area .like_button:before { color:<?php echo esc_attr($like_color); ?>; }
#recipe_title_area .like_button:hover, #recipe_title_area .like_button.active, #recipe_title_area .like_message { background-color:<?php echo esc_attr($like_color); ?>; }
#recipe_title_area .like_message:after { border-color:<?php echo esc_attr($like_color); ?> transparent transparent transparent; }
#related_recipe .design_headline { font-size:<?php echo esc_html($options['related_recipe_headline_font_size']); ?>px; color:<?php echo esc_attr($options['related_recipe_headline_font_color']); ?>; background:<?php echo esc_attr($options['related_recipe_headline_bg_color']); ?>; border-color:<?php echo esc_attr($options['related_recipe_headline_border_color']); ?>; }
<?php if($related_headline_image_id) { ?>
#related_recipe .design_headline:before { background:<?php echo esc_attr($options['related_recipe_headline_icon_color']); ?> url(<?php echo esc_attr($related_headline_image[0]); ?>) no-repeat center; }
<?php } else { ?>
#related_recipe .design_headline:before { background:<?php echo esc_attr($options['related_recipe_headline_icon_color']); ?>; font-family:'headline_icon'; content:'\e90d'; font-size:23px; line-height:62px; }
@media screen and (max-width:1210px) {
  #related_recipe .design_headline:before { font-size:20px; line-height:47px; }
}
<?php }; ?>
#related_recipe .design_headline:after { border-color:<?php echo esc_attr($options['related_recipe_headline_icon_color']); ?> transparent transparent transparent; }
#related_recipe .recipe_list .title_area .title { font-size:<?php echo esc_html($options['related_recipe_list_title_font_size']); ?>px; }
@media screen and (max-width:650px) {
  #recipe_title_area .title { font-size:<?php echo esc_html($options['single_recipe_title_font_size_mobile']); ?>px; }
  #recipe_main_content, .recipe_material, .recipe_howto, .recipe_point, .recipe_free { font-size:<?php echo esc_html($options['single_recipe_content_font_size_mobile']); ?>px; }
  #related_recipe .design_headline { font-size:<?php echo esc_html($options['related_recipe_headline_font_size_mobile']); ?>px; }
  #related_recipe .recipe_list .title_area .title { font-size:<?php echo esc_html($options['related_recipe_list_title_font_size_mobile']); ?>px; }
}
<?php
     $recipe_contents_builder = get_post_meta( $post->ID, 'recipe_contents_builder', true );
     if ( $recipe_contents_builder && is_array( $recipe_contents_builder ) ) :
       foreach( $recipe_contents_builder as $key => $content ) :
         // 材料
         if ( 'recipe_material' === $content['content_select'] ) {
           if(!empty($content['headline_font_size'])) { $headline_font_size = $content['headline_font_size']; } else { $headline_font_size = '20'; };
           if(!empty($content['headline_font_size_mobile'])) { $headline_font_size_mobile = $content['headline_font_size_mobile']; } else { $headline_font_size_mobile = '16'; };
           if(!empty($content['headline_font_color'])) { $headline_font_color = $content['headline_font_color']; } else { $headline_font_color = '#000000'; };
           if(!empty($content['headline_border_color'])) { $headline_border_color = $content['headline_border_color']; } else { $headline_border_color = '#dddddd'; };
           if(!empty($content['headline_bg_color'])) { $headline_bg_color = $content['headline_bg_color']; } else { $headline_bg_color = '#ffffff'; };
           if(!empty($content['headline_icon_color'])) { $headline_icon_color = $content['headline_icon_color']; } else { $headline_icon_color = '#000000'; };
           if(!empty($content['icon_image'])) { $icon_image = wp_get_attachment_image_src($content['icon_image'], 'full'); } else { $icon_image = ''; };
?>
.recipe_material.num<?php echo esc_attr($key); ?> .design_headline { font-size:<?php echo esc_html($headline_font_size); ?>px; color:<?php echo esc_attr($headline_font_color); ?>; background:<?php echo esc_attr($headline_bg_color); ?>; border-color:<?php echo esc_attr($headline_border_color); ?>; }
<?php if($icon_image) { ?>
.recipe_material.num<?php echo esc_attr($key); ?> .design_headline:before { background:<?php echo esc_attr($headline_icon_color); ?> url(<?php echo esc_attr($icon_image[0]); ?>) no-repeat center; }
<?php } else { ?>
.recipe_material.num<?php echo esc_attr($key); ?> .design_headline:before { background:<?php echo esc_attr($headline_icon_color); ?>; font-family:'design_plus'; content:'\f0c9'; font-size:18px; line-height:62px; }
@media screen and (max-width:1210px) {
  .recipe_material.num<?php echo esc_attr($key); ?> .design_headline:before { font-size:15px; line-height:47px; }
}
<?php }; ?>
.recipe_material.num<?php echo esc_attr($key); ?> .design_headline:after { border-color:<?php echo esc_attr($headline_icon_color); ?> transparent transparent transparent; }
@media screen and (max-width:650px) {
  .recipe_material.num<?php echo esc_attr($key); ?> .design_headline { font-size:<?php echo esc_html($headline_font_size_mobile); ?>px; }
}
<?php
     // 作り方
     } elseif ( 'recipe_howto' === $content['content_select'] ) {
       if(!empty($content['headline_font_size'])) { $headline_font_size = $content['headline_font_size']; } else { $headline_font_size = '20'; };
       if(!empty($content['headline_font_size_mobile'])) { $headline_font_size_mobile = $content['headline_font_size_mobile']; } else { $headline_font_size_mobile = '16'; };
       if(!empty($content['headline_font_color'])) { $headline_font_color = $content['headline_font_color']; } else { $headline_font_color = '#000000'; };
       if(!empty($content['headline_border_color'])) { $headline_border_color = $content['headline_border_color']; } else { $headline_border_color = '#dddddd'; };
       if(!empty($content['headline_bg_color'])) { $headline_bg_color = $content['headline_bg_color']; } else { $headline_bg_color = '#ffffff'; };
       if(!empty($content['headline_icon_color'])) { $headline_icon_color = $content['headline_icon_color']; } else { $headline_icon_color = '#000000'; };
       if(!empty($content['icon_image'])) { $icon_image = wp_get_attachment_image_src($content['icon_image'], 'full'); } else { $icon_image = ''; };
?>
.recipe_howto.num<?php echo esc_attr($key); ?> .design_headline { font-size:<?php echo esc_html($headline_font_size); ?>px; color:<?php echo esc_attr($headline_font_color); ?>; background:<?php echo esc_attr($headline_bg_color); ?>; border-color:<?php echo esc_attr($headline_border_color); ?>; }
<?php if($icon_image) { ?>
.recipe_howto.num<?php echo esc_attr($key); ?> .design_headline:before { background:<?php echo esc_attr($headline_icon_color); ?> url(<?php echo esc_attr($icon_image[0]); ?>) no-repeat center; }
<?php } else { ?>
.recipe_howto.num<?php echo esc_attr($key); ?> .design_headline:before { background:<?php echo esc_attr($headline_icon_color); ?>; font-family:'headline_icon'; content:'\e906'; font-size:23px; line-height:62px; }
@media screen and (max-width:1210px) {
  .recipe_howto.num<?php echo esc_attr($key); ?> .design_headline:before { font-size:20px; line-height:47px; }
}
<?php }; ?>
.recipe_howto.num<?php echo esc_attr($key); ?> .design_headline:after { border-color:<?php echo esc_attr($headline_icon_color); ?> transparent transparent transparent; }
@media screen and (max-width:650px) {
  .recipe_howto.num<?php echo esc_attr($key); ?> .design_headline { font-size:<?php echo esc_html($headline_font_size_mobile); ?>px; }
}
<?php
     // ポイント
     } elseif ( 'recipe_point' === $content['content_select'] ) {
       if(!empty($content['headline_font_size'])) { $headline_font_size = $content['headline_font_size']; } else { $headline_font_size = '20'; };
       if(!empty($content['headline_font_size_mobile'])) { $headline_font_size_mobile = $content['headline_font_size_mobile']; } else { $headline_font_size_mobile = '16'; };
       if(!empty($content['headline_font_color'])) { $headline_font_color = $content['headline_font_color']; } else { $headline_font_color = '#000000'; };
       if(!empty($content['headline_border_color'])) { $headline_border_color = $content['headline_border_color']; } else { $headline_border_color = '#dddddd'; };
       if(!empty($content['headline_bg_color'])) { $headline_bg_color = $content['headline_bg_color']; } else { $headline_bg_color = '#ffffff'; };
       if(!empty($content['headline_icon_color'])) { $headline_icon_color = $content['headline_icon_color']; } else { $headline_icon_color = '#000000'; };
       if(!empty($content['icon_image'])) { $icon_image = wp_get_attachment_image_src($content['icon_image'], 'full'); } else { $icon_image = ''; };
?>
.recipe_point.num<?php echo esc_attr($key); ?> .design_headline { font-size:<?php echo esc_html($headline_font_size); ?>px; color:<?php echo esc_attr($headline_font_color); ?>; background:<?php echo esc_attr($headline_bg_color); ?>; border-color:<?php echo esc_attr($headline_border_color); ?>; }
<?php if($icon_image) { ?>
.recipe_point.num<?php echo esc_attr($key); ?> .design_headline:before { background:<?php echo esc_attr($headline_icon_color); ?> url(<?php echo esc_attr($icon_image[0]); ?>) no-repeat center; }
<?php } else { ?>
.recipe_point.num<?php echo esc_attr($key); ?> .design_headline:before { background:<?php echo esc_attr($headline_icon_color); ?>; font-family:'headline_icon'; content:'\e90f'; font-size:23px; line-height:62px; }
@media screen and (max-width:1210px) {
  .recipe_point.num<?php echo esc_attr($key); ?> .design_headline:before { font-size:20px; line-height:47px; }
}
<?php }; ?>
.recipe_point.num<?php echo esc_attr($key); ?> .design_headline:after { border-color:<?php echo esc_attr($headline_icon_color); ?> transparent transparent transparent; }
@media screen and (max-width:650px) {
  .recipe_point.num<?php echo esc_attr($key); ?> .design_headline { font-size:<?php echo esc_html($headline_font_size_mobile); ?>px; }
}
<?php
         };
       endforeach;
     endif;

     // レシピアーカイブページ -----------------------------------------------------------------------------
     } elseif(is_post_type_archive('recipe')) {
?>
#page_header .headline { font-size:<?php echo esc_html($options['recipe_headline_font_size']); ?>px; }
#page_header .desc { font-size:<?php echo esc_html($options['recipe_desc_font_size']); ?>px; }
#recipe_archive .desc { font-size:<?php echo esc_html($options['archive_recipe_desc_font_size']); ?>px; }
@media screen and (max-width:650px) {
  #page_header .headline { font-size:<?php echo esc_html($options['recipe_headline_font_size_mobile']); ?>px; }
  #page_header .desc { font-size:<?php echo esc_html($options['recipe_desc_font_size_mobile']); ?>px; }
  #recipe_archive .desc { font-size:<?php echo esc_html($options['archive_recipe_desc_font_size_mobile']); ?>px; }
}
<?php
     // レシピカテゴリーページ -----------------------------------------------------------------------------
     } elseif(is_tax('recipe_category')) {
?>
#archive_headline .title { font-size:<?php echo esc_html($options['category_recipe_headline_font_size']); ?>px; }
#recipe_categry .child_category_desc { font-size:<?php echo esc_html($options['category_recipe_desc_font_size']); ?>px; }
#recipe_categry .recipe_list .title_area .title { font-size:<?php echo esc_html($options['category_recipe_list_title_font_size']); ?>px; }
@media screen and (max-width:650px) {
  #archive_headline .title { font-size:<?php echo esc_html($options['category_recipe_headline_font_size_mobile']); ?>px; }
  #recipe_categry .child_category_desc { font-size:<?php echo esc_html($options['category_recipe_desc_font_size_mobile']); ?>px; }
  #recipe_categry .recipe_list .title_area .title { font-size:<?php echo esc_html($options['category_recipe_list_title_font_size_mobile']); ?>px; }
}
<?php
     // お知らせアーカイブページ -----------------------------------------------------------------------------
     } elseif(is_post_type_archive('news')) {
       $archive_news_headline_image_id = $options['archive_news_headline_image'];
         if($archive_news_headline_image_id) { $archive_news_headline_image = wp_get_attachment_image_src($archive_news_headline_image_id, 'full'); }
?>
#news_archive .design_headline { font-size:<?php echo esc_html($options['archive_news_headline_font_size']); ?>px; color:<?php echo esc_attr($options['archive_news_headline_font_color']); ?>; background:<?php echo esc_attr($options['archive_news_headline_bg_color']); ?>; border-color:<?php echo esc_attr($options['archive_news_headline_border_color']); ?>; }
<?php if($archive_news_headline_image_id) { ?>
#news_archive .design_headline:before { background:<?php echo esc_attr($options['archive_news_headline_icon_color']); ?> url(<?php echo esc_attr($archive_news_headline_image[0]); ?>) no-repeat center; }
<?php } else { ?>
#news_archive .design_headline:before { background:<?php echo esc_attr($options['archive_news_headline_icon_color']); ?>; font-family:'headline_icon'; content:'\e90b'; font-size:19px; line-height:62px; }
@media screen and (max-width:1210px) {
  #news_archive .design_headline:before { font-size:16px; line-height:47px; }
}
<?php }; ?>
#news_archive .design_headline:after { border-color:<?php echo esc_attr($options['archive_news_headline_icon_color']); ?> transparent transparent transparent; }
#news_list .title { font-size:<?php echo esc_html($options['archive_news_title_font_size']); ?>px; }
@media screen and (max-width:650px) {
  #news_archive .design_headline { font-size:<?php echo esc_html($options['archive_news_headline_font_size_mobile']); ?>px; }
  #news_list .title { font-size:<?php echo esc_html($options['archive_news_title_font_size_mobile']); ?>px; }
}
<?php
     // お知らせ詳細ページ -----------------------------------------------------------------------------
     } elseif(is_singular('news')) {
       $recent_news_headline_image_id = $options['recent_news_headline_image'];
         if($recent_news_headline_image_id) { $recent_news_headline_image = wp_get_attachment_image_src($recent_news_headline_image_id, 'full'); }
?>
#post_title_area .title { font-size:<?php echo esc_html($options['single_news_title_font_size']); ?>px; }
#article .post_content { font-size:<?php echo esc_html($options['single_news_content_font_size']); ?>px; }
#recent_news .design_headline { font-size:<?php echo esc_html($options['recent_news_headline_font_size']); ?>px; color:<?php echo esc_attr($options['recent_news_headline_font_color']); ?>; background:<?php echo esc_attr($options['recent_news_headline_bg_color']); ?>; border-color:<?php echo esc_attr($options['recent_news_headline_border_color']); ?>; }
<?php if($recent_news_headline_image_id) { ?>
#recent_news .design_headline:before { background:<?php echo esc_attr($options['recent_news_headline_icon_color']); ?> url(<?php echo esc_attr($recent_news_headline_image[0]); ?>) no-repeat center; }
<?php } else { ?>
#recent_news .design_headline:before { background:<?php echo esc_attr($options['recent_news_headline_icon_color']); ?>; font-family:'headline_icon'; content:'\e90b'; font-size:19px; line-height:62px; }
@media screen and (max-width:1210px) {
  #recent_news .design_headline:before { font-size:16px; line-height:47px; }
}
<?php }; ?>
#recent_news .design_headline:after { border-color:<?php echo esc_attr($options['recent_news_headline_icon_color']); ?> transparent transparent transparent; }
#recent_news .design_headline .archive_link { color:<?php echo esc_attr($options['recent_news_headline_font_color']); ?>; }
@media screen and (max-width:650px) {
  #post_title_area .title { font-size:<?php echo esc_html($options['single_news_title_font_size_mobile']); ?>px; }
  #article .post_content { font-size:<?php echo esc_html($options['single_news_content_font_size_mobile']); ?>px; }
  #recent_news .design_headline { font-size:<?php echo esc_html($options['recent_news_headline_font_size_mobile']); ?>px; }
}
<?php
     // 投稿者ページ -----------------------------------------------------------------------------
     } elseif( is_author() ) {
       $author_recipe_overlay_color = hex2rgb($options['author_recipe_overlay_color']);
       $author_recipe_overlay_color = implode(",",$author_recipe_overlay_color);
       $author_recipe_headline_image_id = $options['author_recipe_headline_image'];
         if($author_recipe_headline_image_id) { $author_recipe_headline_image = wp_get_attachment_image_src($author_recipe_headline_image_id, 'full'); }
?>
#author_page_header .name { font-size:<?php echo esc_html($options['author_recipe_name_font_size']); ?>px; }
#author_page_header .name span { font-size:<?php echo esc_html($options['author_recipe_sub_title_font_size']); ?>px; }
#author_page_header .blur_image { filter:blur(<?php echo esc_html($options['author_recipe_blur']); ?>px); }
#author_page_header .blur_image:before { background:rgba(<?php echo esc_html($author_recipe_overlay_color); ?>,<?php echo esc_html($options['author_recipe_overlay_opacity']); ?>); }
#author_page_header .post_content { font-size:<?php echo esc_html($options['author_recipe_desc_font_size']); ?>px; }
#author_archive .design_headline { font-size:<?php echo esc_html($options['author_recipe_headline_font_size']); ?>px; color:<?php echo esc_attr($options['author_recipe_headline_font_color']); ?>; background:<?php echo esc_attr($options['author_recipe_headline_bg_color']); ?>; border-color:<?php echo esc_attr($options['author_recipe_headline_border_color']); ?>; }
<?php if($author_recipe_headline_image_id) { ?>
#author_archive .design_headline:before { background:<?php echo esc_attr($options['author_recipe_headline_icon_color']); ?> url(<?php echo esc_attr($author_recipe_headline_image[0]); ?>) no-repeat center; }
<?php } else { ?>
#author_archive .design_headline:before { background:<?php echo esc_attr($options['author_recipe_headline_icon_color']); ?>; font-family:'headline_icon'; content:'\e909'; font-size:17px; line-height:62px; }
@media screen and (max-width:1210px) {
  #author_archive .design_headline:before { font-size:14px; line-height:47px; }
}
<?php }; ?>
#author_archive .design_headline:after { border-color:<?php echo esc_attr($options['author_recipe_headline_icon_color']); ?> transparent transparent transparent; }
#author_archive .design_headline .total_post { color:<?php echo esc_attr($options['author_recipe_headline_font_color']); ?>; border-color:<?php echo esc_attr($options['author_recipe_headline_border_color']); ?>; }
#author_archive .recipe_list .title_area .title { font-size:<?php echo esc_html($options['author_recipe_list_title_font_size']); ?>px; }
@media screen and (max-width:650px) {
  #author_page_header .name { font-size:<?php echo esc_html($options['author_recipe_name_font_size_mobile']); ?>px; }
  #author_page_header .name span { font-size:<?php echo esc_html($options['author_recipe_sub_title_font_size_mobile']); ?>px; }
  #author_page_header .post_content { font-size:<?php echo esc_html($options['author_recipe_desc_font_size_mobile']); ?>px; }
  #author_archive .design_headline { font-size:<?php echo esc_html($options['author_recipe_headline_font_size_mobile']); ?>px; }
  #author_archive .recipe_list .title_area .title { font-size:<?php echo esc_html($options['author_recipe_list_title_font_size_mobile']); ?>px; }
}
<?php
     // トップページ -----------------------------------------------------------------------------
     } elseif(is_front_page()) {
       // 画像スライダー
       if($options['index_header_content_type'] == 'type1') {
         for($i=1; $i<= 3; $i++):
           $use_text_shadow = $options['index_slider_use_shadow'.$i];
           if($use_text_shadow) {
             $shadow1 = $options['index_slider_shadow_h'.$i];
             $shadow2 = $options['index_slider_shadow_v'.$i];
             $shadow3 = $options['index_slider_shadow_b'.$i];
             $shadow4 = $options['index_slider_shadow_c'.$i];
           };
           // キャッチフレーズ、説明文
?>
#index_slider .item<?php echo $i; ?> .catch { font-size:<?php echo esc_html($options['index_slider_catch_font_size'.$i]); ?>px; color:<?php echo esc_html($options['index_slider_catch_color'.$i]); ?>; <?php if($use_text_shadow) { ?>text-shadow:<?php echo esc_attr($shadow1); ?>px <?php echo esc_attr($shadow2); ?>px <?php echo esc_attr($shadow3); ?>px <?php echo esc_attr($shadow4); ?>;<?php }; ?> }
#index_slider .item<?php echo $i; ?> .desc { font-size:<?php echo esc_html($options['index_slider_desc_font_size'.$i]); ?>px; color:<?php echo esc_html($options['index_slider_desc_color'.$i]); ?>; <?php if($use_text_shadow) { ?>text-shadow:<?php echo esc_attr($shadow1); ?>px <?php echo esc_attr($shadow2); ?>px <?php echo esc_attr($shadow3); ?>px <?php echo esc_attr($shadow4); ?>;<?php }; ?> }
@media screen and (max-width:650px) {
  #index_slider .item<?php echo $i; ?> .catch { font-size:<?php echo esc_html($options['index_slider_catch_font_size_mobile'.$i]); ?>px; }
  #index_slider .item<?php echo $i; ?> .desc { font-size:<?php echo esc_html($options['index_slider_desc_font_size_mobile'.$i]); ?>px; }
}
<?php
     // ボタン
     if($options['index_slider_show_button'.$i]) {
?>
#index_slider .item<?php echo $i; ?> .button { color:<?php echo esc_html($options['index_slider_button_font_color'.$i]); ?>; background:<?php echo esc_html($options['index_slider_button_bg_color'.$i]); ?>; }
#index_slider .item<?php echo $i; ?> .button:hover { color:<?php echo esc_html($options['index_slider_button_font_color_hover'.$i]); ?>; background:<?php echo esc_html($options['index_slider_button_bg_color_hover'.$i]); ?>; }
<?php
     };
     // オーバーレイ
     if($options['index_slider_use_overlay'.$i] || $options['index_slider_use_overlay_mobile'.$i]) {
       $overlay_color = hex2rgb($options['index_slider_overlay_color'.$i]);
       $overlay_color = implode(",",$overlay_color);
       $overlay_opacity = $options['index_slider_overlay_opacity'.$i];
       if(is_mobile() && $options['index_slider_use_overlay_mobile'.$i]) {
         $overlay_color = hex2rgb($options['index_slider_overlay_color_mobile'.$i]);
         $overlay_color = implode(",",$overlay_color);
         $overlay_opacity = $options['index_slider_overlay_opacity_mobile'.$i];
       }
?>
#index_slider .item<?php echo $i; ?> .overlay { background:rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>); }
<?php
       };
     endfor;
       // 画像スライダー　スマホ用　テキストシャドウ
       if($options['index_slider_mobile_content_type'] != 'type1') {
         $use_text_shadow = $options['index_slider_mobile_use_shadow'];
         if($use_text_shadow) {
           $shadow1 = $options['index_slider_mobile_shadow_h'];
           $shadow2 = $options['index_slider_mobile_shadow_v'];
           $shadow3 = $options['index_slider_mobile_shadow_b'];
           $shadow4 = $options['index_slider_mobile_shadow_c'];
         }
?>
#index_slider .caption.mobile .catch { font-size:<?php echo esc_attr($options['index_slider_mobile_catch_font_size']); ?>px; color:<?php echo esc_attr($options['index_slider_mobile_catch_color']); ?>; <?php if($use_text_shadow) { ?>text-shadow:<?php echo esc_attr($shadow1); ?>px <?php echo esc_attr($shadow2); ?>px <?php echo esc_attr($shadow3); ?>px <?php echo esc_attr($shadow4); ?>;<?php }; ?> }
<?php
       };
     }; // END 画像スライダー

     // 動画 ---------------------------------------------------------------------------------------------------------------------------------------
     if($options['index_header_content_type'] == 'type2' || $options['index_header_content_type'] == 'type3') {
       $use_text_shadow = $options['index_movie_use_shadow'];
       if($use_text_shadow) {
         $shadow1 = $options['index_movie_shadow_h'];
         $shadow2 = $options['index_movie_shadow_v'];
         $shadow3 = $options['index_movie_shadow_b'];
         $shadow4 = $options['index_movie_shadow_c'];
       };
?>
#index_slider .catch { font-size:<?php echo esc_html($options['index_movie_catch_font_size']); ?>px; color:<?php echo esc_html($options['index_movie_catch_color']); ?>; <?php if($use_text_shadow) { ?>text-shadow:<?php echo esc_attr($shadow1); ?>px <?php echo esc_attr($shadow2); ?>px <?php echo esc_attr($shadow3); ?>px <?php echo esc_attr($shadow4); ?>;<?php }; ?> }
#index_slider .desc { font-size:<?php echo esc_html($options['index_movie_desc_font_size']); ?>px; color:<?php echo esc_html($options['index_movie_desc_color']); ?>; <?php if($use_text_shadow) { ?>text-shadow:<?php echo esc_attr($shadow1); ?>px <?php echo esc_attr($shadow2); ?>px <?php echo esc_attr($shadow3); ?>px <?php echo esc_attr($shadow4); ?>;<?php }; ?> }
<?php
     // ボタン
     if($options['index_movie_show_button']){
?>
#index_slider .button { color:<?php echo esc_html($options['index_movie_button_font_color']); ?>; background:<?php echo esc_html($options['index_movie_button_bg_color']); ?>; }
#index_slider .button:hover { color:<?php echo esc_html($options['index_movie_button_font_color_hover']); ?>; background:<?php echo esc_html($options['index_movie_button_bg_color_hover']); ?>; }
<?php
     };
     // オーバーレイ
     if($options['index_movie_use_overlay']) {
       $overlay_color = hex2rgb($options['index_movie_overlay_color']);
       $overlay_color = implode(",",$overlay_color);
       $overlay_opacity = $options['index_movie_overlay_opacity'];
?>
#index_slider .overlay { background:rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>); }
<?php }; ?>
@media screen and (max-width:650px) {
  #index_slider .catch { font-size:<?php echo esc_html($options['index_movie_catch_font_size_mobile']); ?>px; }
  #index_slider .desc { font-size:<?php echo esc_html($options['index_movie_desc_font_size_mobile']); ?>px; }
}
<?php
     // モバイルサイズ時に別のコンテンツを表示する場合 ---------------
     if($options['index_movie_mobile_content_type'] == 'type2') {
       $use_text_shadow = $options['index_movie_use_shadow_mobile'];
       if($use_text_shadow) {
         $shadow1 = $options['index_movie_shadow_h_mobile'];
         $shadow2 = $options['index_movie_shadow_v_mobile'];
         $shadow3 = $options['index_movie_shadow_b_mobile'];
         $shadow4 = $options['index_movie_shadow_c_mobile'];
       };
?>
@media screen and (max-width:650px) {
  #index_slider .catch { font-size:<?php echo esc_html($options['index_movie_mobile_catch_font_size']); ?>px; color:<?php echo esc_html($options['index_movie_catch_color_mobile']); ?>; <?php if($use_text_shadow) { ?>text-shadow:<?php echo esc_attr($shadow1); ?>px <?php echo esc_attr($shadow2); ?>px <?php echo esc_attr($shadow3); ?>px <?php echo esc_attr($shadow4); ?>;<?php }; ?> }
}
<?php
       }; // END content type2
     }; // END 動画

     // レシピスライダー -------------------------------------------------------------------------------------------------------------
     if ($options['show_index_recipe_slider']){
?>
#index_recipe_slider .title { font-size:<?php echo esc_html($options['index_recipe_slider_font_size']); ?>px; }
@media screen and (max-width:1100px) {
  #index_recipe_slider .title { font-size:<?php echo esc_html($options['index_recipe_slider_font_size_mobile']); ?>px; }
}
<?php
     }

     // 無料会員登録メッセージ -------------------------------------------------------------------------------------------------------------
     if ( !is_user_logged_in() && $options['show_index_welcome']){
?>
#index_welcome_message .catch { font-size:<?php echo esc_html($options['index_welcome_catch_font_size']); ?>px; }
#index_welcome_message .desc { font-size:<?php echo esc_html($options['index_welcome_desc_font_size']); ?>px; }
#index_welcome_message .button { color:<?php echo esc_html($options['index_welcome_button_font_color']); ?>; background:<?php echo esc_html($options['index_welcome_button_bg_color']); ?>; }
#index_welcome_message .button:hover { color:<?php echo esc_html($options['index_welcome_button_font_color_hover']); ?>; background:<?php echo esc_html($options['index_welcome_button_bg_color_hover']); ?>; }
@media screen and (max-width:650px) {
  #index_welcome_message .catch { font-size:<?php echo esc_html($options['index_welcome_catch_font_size_mobile']); ?>px; }
  #index_welcome_message .desc { font-size:<?php echo esc_html($options['index_welcome_desc_font_size_mobile']); ?>px; }
}
<?php
     };

     // コンテンツビルダー -------------------------------------------------------------------------------------------------------------
     if (!empty($options['contents_builder'])) :
       $content_count = 1;
       foreach($options['contents_builder'] as $content) :
         // 新着レシピ
         if ($content['cb_content_select'] == 'recent_recipe_list') {

           $headline_image_id = $content['recent_headline_image'];
           if($headline_image_id) {
             $headline_image = wp_get_attachment_image_src($headline_image_id, 'full');
           }
?>
.index_recent_recipe.num<?php echo $content_count; ?> .design_headline { font-size:<?php echo esc_html($content['recent_headline_font_size']); ?>px; color:<?php echo esc_attr($content['recent_headline_font_color']); ?>; background:<?php echo esc_attr($content['recent_headline_bg_color']); ?>; border-color:<?php echo esc_attr($content['recent_headline_border_color']); ?>; }
<?php if($headline_image_id) { ?>
.index_recent_recipe.num<?php echo $content_count; ?> .design_headline:before { background:<?php echo esc_attr($content['recent_headline_icon_color']); ?> url(<?php echo esc_attr($headline_image[0]); ?>) no-repeat center; }
<?php } else { ?>
.index_recent_recipe.num<?php echo $content_count; ?> .design_headline:before { background:<?php echo esc_attr($content['recent_headline_icon_color']); ?>; font-family:'headline_icon'; content:'\e90c'; font-size:20px; line-height:62px; }
@media screen and (max-width:1210px) {
  .index_recent_recipe.num<?php echo $content_count; ?> .design_headline:before { font-size:17px; line-height:47px; }
}
<?php }; ?>
.index_recent_recipe.num<?php echo $content_count; ?> .design_headline:after { border-color:<?php echo esc_attr($content['recent_headline_icon_color']); ?> transparent transparent transparent; }
.index_recent_recipe.num<?php echo $content_count; ?> .desc { font-size:<?php echo esc_attr($content['recent_desc_font_size']); ?>px; }
.index_recent_recipe.num<?php echo $content_count; ?> .title { font-size:<?php echo esc_attr($content['recent_title_font_size']); ?>px; }
@media screen and (max-width:650px) {
  .index_recent_recipe.num<?php echo $content_count; ?> .design_headline { font-size:<?php echo esc_html($content['recent_headline_font_size_mobile']); ?>px; }
  .index_recent_recipe.num<?php echo $content_count; ?> .desc { font-size:<?php echo esc_attr($content['recent_desc_font_size_mobile']); ?>px; }
  .index_recent_recipe.num<?php echo $content_count; ?> .title { font-size:<?php echo esc_attr($content['recent_title_font_size_mobile']); ?>px; }
}
<?php
     // 人気のレシピ
     } elseif ($content['cb_content_select'] == 'popular_recipe_list') {

       $headline_image_id = $content['popular_headline_image'];
       if($headline_image_id) {
         $headline_image = wp_get_attachment_image_src($headline_image_id, 'full');
       }
?>
.index_popular_recipe.num<?php echo $content_count; ?> .design_headline { font-size:<?php echo esc_html($content['popular_headline_font_size']); ?>px; color:<?php echo esc_attr($content['popular_headline_font_color']); ?>; background:<?php echo esc_attr($content['popular_headline_bg_color']); ?>; border-color:<?php echo esc_attr($content['popular_headline_border_color']); ?>; }
<?php if($headline_image_id) { ?>
.index_popular_recipe.num<?php echo $content_count; ?> .design_headline:before { background:<?php echo esc_attr($content['popular_headline_icon_color']); ?> url(<?php echo esc_attr($headline_image[0]); ?>) no-repeat center; }
<?php } else { ?>
.index_popular_recipe.num<?php echo $content_count; ?> .design_headline:before { background:<?php echo esc_attr($content['popular_headline_icon_color']); ?>; font-family:'headline_icon'; content:'\e900'; font-size:23px; line-height:62px; }
@media screen and (max-width:1210px) {
  .index_popular_recipe.num<?php echo $content_count; ?> .design_headline:before { font-size:20px; line-height:47px; }
}
<?php }; ?>
.index_popular_recipe.num<?php echo $content_count; ?> .design_headline:after { border-color:<?php echo esc_attr($content['popular_headline_icon_color']); ?> transparent transparent transparent; }
.index_popular_recipe.num<?php echo $content_count; ?> .desc { font-size:<?php echo esc_attr($content['popular_desc_font_size']); ?>px; }
.index_popular_recipe.num<?php echo $content_count; ?> .title { font-size:<?php echo esc_attr($content['popular_title_font_size']); ?>px; }
@media screen and (max-width:650px) {
  .index_popular_recipe.num<?php echo $content_count; ?> .design_headline { font-size:<?php echo esc_html($content['popular_headline_font_size_mobile']); ?>px; }
  .index_popular_recipe.num<?php echo $content_count; ?> .desc { font-size:<?php echo esc_attr($content['popular_desc_font_size_mobile']); ?>px; }
  .index_popular_recipe.num<?php echo $content_count; ?> .title { font-size:<?php echo esc_attr($content['popular_title_font_size_mobile']); ?>px; }
}
<?php
     // 特集レシピ
     } elseif ($content['cb_content_select'] == 'featured_recipe_list') {
       $headline_image_id = $content['featured_headline_image'];
       if($headline_image_id) {
         $headline_image = wp_get_attachment_image_src($headline_image_id, 'full');
       }
?>
.index_featured_recipe.num<?php echo $content_count; ?> .design_headline { font-size:<?php echo esc_html($content['featured_headline_font_size']); ?>px; color:<?php echo esc_attr($content['featured_headline_font_color']); ?>; background:<?php echo esc_attr($content['featured_headline_bg_color']); ?>; border-color:<?php echo esc_attr($content['featured_headline_border_color']); ?>; }
<?php if($headline_image_id) { ?>
.index_featured_recipe.num<?php echo $content_count; ?> .design_headline:before { background:<?php echo esc_attr($content['featured_headline_icon_color']); ?> url(<?php echo esc_attr($headline_image[0]); ?>) no-repeat center; }
<?php } else { ?>
.index_featured_recipe.num<?php echo $content_count; ?> .design_headline:before { background:<?php echo esc_attr($content['featured_headline_icon_color']); ?>; font-family:'headline_icon'; content:'\e90e'; font-size:23px; line-height:62px; }
@media screen and (max-width:1210px) {
  .index_featured_recipe.num<?php echo $content_count; ?> .design_headline:before { font-size:20px; line-height:47px; }
}
<?php }; ?>
.index_featured_recipe.num<?php echo $content_count; ?> .design_headline:after { border-color:<?php echo esc_attr($content['featured_headline_icon_color']); ?> transparent transparent transparent; }
.index_featured_recipe.num<?php echo $content_count; ?> .desc { font-size:<?php echo esc_attr($content['featured_desc_font_size']); ?>px; }
@media screen and (max-width:650px) {
  .index_featured_recipe.num<?php echo $content_count; ?> .design_headline { font-size:<?php echo esc_html($content['featured_headline_font_size_mobile']); ?>px; }
  .index_featured_recipe.num<?php echo $content_count; ?> .desc { font-size:<?php echo esc_attr($content['featured_desc_font_size_mobile']); ?>px; }
}
<?php
     for($i=1; $i<= 4; $i++):
       $title_font_size = $content['featured_title_font_size'.$i];
       $title_font_size_mobile = $content['featured_title_font_size_mobile'.$i];
       $sub_title_font_size = $content['featured_sub_title_font_size'.$i];
       $sub_title_font_size_mobile = $content['featured_sub_title_font_size_mobile'.$i];
       $overlay_color = hex2rgb($content['featured_overlay_color'.$i]);
       $overlay_color = implode(",",$overlay_color);
?>
.index_featured_recipe.num<?php echo $content_count; ?> .banner_list .num<?php echo $i; ?> .title { font-size:<?php echo esc_html($title_font_size); ?>px; }
.index_featured_recipe.num<?php echo $content_count; ?> .banner_list .num<?php echo $i; ?> .sub_title { font-size:<?php echo esc_html($sub_title_font_size); ?>px; }
.index_featured_recipe.num<?php echo $content_count; ?> .banner_list .num<?php echo $i; ?> .overlay {
  background: -moz-linear-gradient(left,  rgba(<?php echo esc_attr($overlay_color); ?>,1) 0%, rgba(<?php echo esc_attr($overlay_color); ?>,0) 100%);
  background: -webkit-linear-gradient(left,  rgba(<?php echo esc_attr($overlay_color); ?>,1) 0%,rgba(<?php echo esc_attr($overlay_color); ?>,0) 100%);
  background: linear-gradient(to right,  rgba(<?php echo esc_attr($overlay_color); ?>,1) 0%,rgba(<?php echo esc_attr($overlay_color); ?>,0) 100%);
}
@media screen and (max-width:650px) {
  .index_featured_recipe.num<?php echo $content_count; ?> .banner_list .num<?php echo $i; ?> .title { font-size:<?php echo esc_html($title_font_size_mobile); ?>px; }
  .index_featured_recipe.num<?php echo $content_count; ?> .banner_list .num<?php echo $i; ?> .sub_title { font-size:<?php echo esc_html($sub_title_font_size_mobile); ?>px; }
}
<?php endfor; ?>
<?php
     // 新着ブログ
     } elseif ($content['cb_content_select'] == 'recent_blog_list') {
       $headline_image_id = $content['recent_blog_headline_image'];
       if($headline_image_id) {
         $headline_image = wp_get_attachment_image_src($headline_image_id, 'full');
       }
?>
.index_blog.num<?php echo $content_count; ?> .design_headline { font-size:<?php echo esc_html($content['recent_blog_headline_font_size']); ?>px; color:<?php echo esc_attr($content['recent_blog_headline_font_color']); ?>; background:<?php echo esc_attr($content['recent_blog_headline_bg_color']); ?>; border-color:<?php echo esc_attr($content['recent_blog_headline_border_color']); ?>; }
<?php if($headline_image_id) { ?>
.index_blog.num<?php echo $content_count; ?> .design_headline:before { background:<?php echo esc_attr($content['recent_blog_headline_icon_color']); ?> url(<?php echo esc_attr($headline_image[0]); ?>) no-repeat center; }
<?php } else { ?>
.index_blog.num<?php echo $content_count; ?> .design_headline:before { background:<?php echo esc_attr($content['recent_blog_headline_icon_color']); ?>; font-family:'headline_icon'; content:'\e90a'; font-size:37px; line-height:65px; }
@media screen and (max-width:1210px) {
  .index_blog.num<?php echo $content_count; ?> .design_headline:before { font-size:32px; line-height:52px; }
}
<?php }; ?>
.index_blog.num<?php echo $content_count; ?> .design_headline:after { border-color:<?php echo esc_attr($content['recent_blog_headline_icon_color']); ?> transparent transparent transparent; }
.index_blog.num<?php echo $content_count; ?> .desc { font-size:<?php echo esc_attr($content['recent_blog_desc']); ?>px; }
.index_blog.num<?php echo $content_count; ?> .title { font-size:<?php echo esc_attr($content['recent_blog_title_font_size']); ?>px; }
@media screen and (max-width:650px) {
  .index_blog.num<?php echo $content_count; ?> .design_headline { font-size:<?php echo esc_html($content['recent_blog_headline_font_size_mobile']); ?>px; }
  .index_blog.num<?php echo $content_count; ?> .desc { font-size:<?php echo esc_attr($content['recent_blog_desc_font_size_mobile']); ?>px; }
  .index_blog.num<?php echo $content_count; ?> .title { font-size:<?php echo esc_attr($content['recent_blog_title_font_size_mobile']); ?>px; }
}
<?php
     // お知らせ
     } elseif ($content['cb_content_select'] == 'news_list') {
       $headline_image_id = $content['news_headline_image'];
       if($headline_image_id) {
         $headline_image = wp_get_attachment_image_src($headline_image_id, 'full');
       }
?>
.index_news.num<?php echo $content_count; ?> .design_headline { font-size:<?php echo esc_html($content['news_headline_font_size']); ?>px; color:<?php echo esc_attr($content['news_headline_font_color']); ?>; background:<?php echo esc_attr($content['news_headline_bg_color']); ?>; border-color:<?php echo esc_attr($content['news_headline_border_color']); ?>; }
<?php if($headline_image_id) { ?>
.index_news.num<?php echo $content_count; ?> .design_headline:before { background:<?php echo esc_attr($content['news_headline_icon_color']); ?> url(<?php echo esc_attr($headline_image[0]); ?>) no-repeat center; }
<?php } else { ?>
.index_news.num<?php echo $content_count; ?> .design_headline:before { background:<?php echo esc_attr($content['news_headline_icon_color']); ?>; font-family:'headline_icon'; content:'\e90b'; font-size:20px; line-height:62px; }
@media screen and (max-width:1210px) {
  .index_news.num<?php echo $content_count; ?> .design_headline:before { font-size:17px; line-height:47px; }
}
<?php }; ?>
.index_news.num<?php echo $content_count; ?> .design_headline:after { border-color:<?php echo esc_attr($content['news_headline_icon_color']); ?> transparent transparent transparent; }
.index_news.num<?php echo $content_count; ?> .desc { font-size:<?php echo esc_attr($content['news_desc_font_size']); ?>px; }
.index_news.num<?php echo $content_count; ?> a .date { color:<?php echo esc_attr($content['news_date_color']); ?>; }
@media screen and (max-width:650px) {
  .index_news.num<?php echo $content_count; ?> .design_headline { font-size:<?php echo esc_html($content['news_headline_font_size_mobile']); ?>px; }
  .index_news.num<?php echo $content_count; ?> .desc { font-size:<?php echo esc_attr($content['news_desc_font_size_mobile']); ?>px; }
}
<?php
     // バナー一覧
     } elseif ($content['cb_content_select'] == 'banner') {
       for($i=1; $i<= 4; $i++):
         $title_font_size = $content['banner_title_font_size'.$i];
         $sub_title_font_size = $content['banner_sub_title_font_size'.$i];
         $overlay_color = hex2rgb($content['banner_overlay_color'.$i]);
         $overlay_color = implode(",",$overlay_color);
?>
.index_banner.num<?php echo $content_count; ?> .banner_list .num<?php echo $i; ?> .title { font-size:<?php echo esc_html($title_font_size); ?>px; }
.index_banner.num<?php echo $content_count; ?> .banner_list .num<?php echo $i; ?> .sub_title { font-size:<?php echo esc_html($sub_title_font_size); ?>px; }
.index_banner.num<?php echo $content_count; ?> .banner_list .num<?php echo $i; ?> .overlay {
  background: -moz-linear-gradient(left,  rgba(<?php echo esc_attr($overlay_color); ?>,1) 0%, rgba(<?php echo esc_attr($overlay_color); ?>,0) 100%);
  background: -webkit-linear-gradient(left,  rgba(<?php echo esc_attr($overlay_color); ?>,1) 0%,rgba(<?php echo esc_attr($overlay_color); ?>,0) 100%);
  background: linear-gradient(to right,  rgba(<?php echo esc_attr($overlay_color); ?>,1) 0%,rgba(<?php echo esc_attr($overlay_color); ?>,0) 100%);
}
<?php endfor; ?>
<?php
       };
       $content_count++;
       endforeach;
     endif; // END コンテンツビルダーここまで

     // ブログ -----------------------------------------------------------------------------
     } elseif(is_archive() || is_home() || is_search() || is_single() ) {
       $related_post_headline_image_id = $options['related_post_headline_image'];
         if($related_post_headline_image_id) { $related_post_headline_image = wp_get_attachment_image_src($related_post_headline_image_id, 'full'); }
?>
#page_header .headline { font-size:<?php echo esc_html($options['blog_headline_font_size']); ?>px; }
#page_header .desc { font-size:<?php echo esc_html($options['blog_desc_font_size']); ?>px; color:<?php echo esc_html($options['blog_desc_color']); ?>; }
#post_list .title { font-size:<?php echo esc_html($options['archive_blog_title_font_size']); ?>px; }
#post_title_area .title { font-size:<?php echo esc_html($options['single_blog_title_font_size']); ?>px; }
#article .post_content { font-size:<?php echo esc_html($options['single_blog_content_font_size']); ?>px; }

#related_post .design_headline { font-size:<?php echo esc_html($options['related_post_headline_font_size']); ?>px; color:<?php echo esc_attr($options['related_post_headline_font_color']); ?>; background:<?php echo esc_attr($options['related_post_headline_bg_color']); ?>; border-color:<?php echo esc_attr($options['related_post_headline_border_color']); ?>; }
<?php if($related_post_headline_image_id) { ?>
#related_post .design_headline:before { background:<?php echo esc_attr($options['related_post_headline_icon_color']); ?> url(<?php echo esc_attr($related_post_headline_image[0]); ?>) no-repeat center; }
<?php } else { ?>
#related_post .design_headline:before { background:<?php echo esc_attr($options['related_post_headline_icon_color']); ?>; font-family:'headline_icon'; content:'\e90d'; font-size:23px; line-height:62px; }
@media screen and (max-width:1210px) {
  #related_post .design_headline:before { font-size:20px; line-height:47px; }
}
<?php }; ?>
#related_post .design_headline:after { border-color:<?php echo esc_attr($options['related_post_headline_icon_color']); ?> transparent transparent transparent; }
#related_post .recipe_list .title_area .title { font-size:<?php echo esc_html($options['related_post_list_title_font_size']); ?>px; }

@media screen and (max-width:650px) {
  #page_header .headline { font-size:<?php echo esc_html($options['blog_headline_font_size_mobile']); ?>px; }
  #page_header .desc { font-size:<?php echo esc_html($options['blog_desc_font_size_mobile']); ?>px; }
  #post_list .title { font-size:<?php echo esc_html($options['archive_blog_title_font_size_mobile']); ?>px; }
  #post_title_area .title { font-size:<?php echo esc_html($options['single_blog_title_font_size_mobile']); ?>px; }
  #article .post_content { font-size:<?php echo esc_html($options['single_blog_content_font_size_mobile']); ?>px; }
  #related_post .design_headline { font-size:<?php echo esc_html($options['related_post_headline_font_size_mobile']); ?>px; }
  #related_post .recipe_list .title_area .title { font-size:<?php echo esc_html($options['related_post_list_title_font_size_mobile']); ?>px; }
}
<?php
     // 投稿者一覧ページ --------------------------------------------------------------------
     } elseif(is_page_template('page-author-list.php')) {

       global $post;

       $author_list_headline_font_size = get_post_meta($post->ID, 'author_list_headline_font_size', true);
         if(empty($author_list_headline_font_size)){ $author_list_headline_font_size = '14'; }
       $author_list_headline_font_size_mobile = get_post_meta($post->ID, 'author_list_headline_font_size_mobile', true);
         if(empty($author_list_headline_font_size_mobile)){ $author_list_headline_font_size_mobile = '12'; }
       $author_list_desc_font_size = get_post_meta($post->ID, 'author_list_desc_font_size', true);
         if(empty($author_list_desc_font_size)){ $author_list_desc_font_size = '16'; }
       $author_list_desc_font_size_mobile = get_post_meta($post->ID, 'author_list_desc_font_size_mobile', true);
         if(empty($author_list_desc_font_size_mobile)){ $author_list_desc_font_size_mobile = '14'; }
       $author_list_title_font_size = get_post_meta($post->ID, 'author_list_title_font_size', true);
         if(empty($author_list_title_font_size)){ $author_list_title_font_size = '20'; }
       $author_list_title_font_size_mobile = get_post_meta($post->ID, 'author_list_title_font_size_mobile', true);
         if(empty($author_list_title_font_size_mobile)){ $author_list_title_font_size_mobile = '16'; }
       $author_list_sub_title_font_size = get_post_meta($post->ID, 'author_list_sub_title_font_size', true);
         if(empty($author_list_sub_title_font_size)){ $author_list_sub_title_font_size = '12'; }
       $author_list_sub_title_font_size_mobile = get_post_meta($post->ID, 'author_list_sub_title_font_size_mobile', true);
         if(empty($author_list_sub_title_font_size_mobile)){ $author_list_sub_title_font_size_mobile = '11'; }
       $author_list_author_desc_font_size = get_post_meta($post->ID, 'author_list_author_desc_font_size', true);
         if(empty($author_list_author_desc_font_size)){ $author_list_author_desc_font_size = '16'; }
       $author_list_author_desc_font_size_mobile = get_post_meta($post->ID, 'author_list_author_desc_font_size_mobile', true);
         if(empty($author_list_author_desc_font_size_mobile)){ $author_list_author_desc_font_size_mobile = '14'; }
?>
#page_header .headline { font-size:<?php echo esc_html($author_list_headline_font_size); ?>px; }
#page_header .desc { font-size:<?php echo esc_html($author_list_desc_font_size); ?>px; }
#author_list .title { font-size:<?php echo esc_html($author_list_title_font_size); ?>px; }
#author_list .title span { font-size:<?php echo esc_html($author_list_sub_title_font_size); ?>px; }
#author_list .desc { font-size:<?php echo esc_html($author_list_author_desc_font_size); ?>px; }
@media screen and (max-width:1210px) {
  #author_list .desc { font-size:<?php echo esc_html($author_list_author_desc_font_size_mobile); ?>px; }
}
@media screen and (max-width:650px) {
  #page_header .headline { font-size:<?php echo esc_html($author_list_headline_font_size_mobile); ?>px; }
  #page_header .desc { font-size:<?php echo esc_html($author_list_desc_font_size_mobile); ?>px; }
  #author_list .title { font-size:<?php echo esc_html($author_list_title_font_size_mobile); ?>px; }
  #author_list .title span { font-size:<?php echo esc_html($author_list_sub_title_font_size_mobile); ?>px; }
}
<?php
     // 特集ページ --------------------------------------------------------------------
     } elseif(is_page_template('page-featured-post-list.php')) {

       global $post;

       $featured_header_catch_font_size = get_post_meta($post->ID, 'featured_header_catch_font_size', true);
         if(empty($featured_header_catch_font_size)){ $featured_header_catch_font_size = '38'; }
       $featured_header_catch_font_size_mobile = get_post_meta($post->ID, 'featured_header_catch_font_size_mobile', true);
         if(empty($featured_header_catch_font_size_mobile)){ $featured_header_catch_font_size_mobile = '20'; }
       $featured_header_desc_font_size = get_post_meta($post->ID, 'featured_header_desc_font_size', true);
         if(empty($featured_header_desc_font_size)){ $featured_header_desc_font_size = '18'; }
       $featured_header_desc_font_size_mobile = get_post_meta($post->ID, 'featured_header_desc_font_size_mobile', true);
         if(empty($featured_header_desc_font_size_mobile)){ $featured_header_desc_font_size_mobile = '13'; }

       $featured_headline_font_size = get_post_meta($post->ID, 'featured_headline_font_size', true);
         if(empty($featured_headline_font_size)){ $featured_headline_font_size = '20'; }
       $featured_headline_font_size_mobile = get_post_meta($post->ID, 'featured_headline_font_size_mobile', true);
         if(empty($featured_headline_font_size_mobile)){ $featured_headline_font_size_mobile = '15'; }
       $featured_headline_font_color = get_post_meta($post->ID, 'featured_headline_font_color', true);
         if(empty($featured_headline_font_color)){ $featured_headline_font_color = '#000000'; }
       $featured_headline_border_color = get_post_meta($post->ID, 'featured_headline_border_color', true);
         if(empty($featured_headline_border_color)){ $featured_headline_border_color = '#dddddd'; }
       $featured_headline_bg_color = get_post_meta($post->ID, 'featured_headline_bg_color', true);
         if(empty($featured_headline_bg_color)){ $featured_headline_bg_color = '#ffffff'; }

       $featured_headline_hide_icon = get_post_meta($post->ID, 'featured_headline_hide_icon', true);
       $featured_headline_icon_color = get_post_meta($post->ID, 'featured_headline_icon_color', true);
         if(empty($featured_headline_icon_color)){ $featured_headline_icon_color = '#000000'; }

       $featured_headline_icon_image_id = get_post_meta($post->ID, 'featured_headline_icon_image', true);
         if($featured_headline_icon_image_id) {
           $featured_headline_icon_image = wp_get_attachment_image_src($featured_headline_icon_image_id, 'full');
         }

       $featured_desc_font_size = get_post_meta($post->ID, 'featured_desc_font_size', true);
         if(empty($featured_desc_font_size)){ $featured_desc_font_size = '16'; }
       $featured_desc_font_size_mobile = get_post_meta($post->ID, 'featured_desc_font_size_mobile', true);
         if(empty($featured_desc_font_size_mobile)){ $featured_desc_font_size_mobile = '14'; }

       $featured_list_title_font_size = get_post_meta($post->ID, 'featured_list_title_font_size', true);
         if(empty($featured_list_title_font_size)){ $featured_list_title_font_size = '16'; }
       $featured_list_title_font_size_mobile = get_post_meta($post->ID, 'featured_list_title_font_size_mobile', true);
         if(empty($featured_list_title_font_size_mobile)){ $featured_list_title_font_size_mobile = '14'; }
?>
#wide_page_header .catch { font-size:<?php echo esc_html($featured_header_catch_font_size); ?>px; }
#wide_page_header .desc { font-size:<?php echo esc_html($featured_header_desc_font_size); ?>px; }
#featured_recipe .design_headline { font-size:<?php echo esc_html($featured_headline_font_size); ?>px; color:<?php echo esc_attr($featured_headline_font_color); ?>; background:<?php echo esc_attr($featured_headline_bg_color); ?>; border-color:<?php echo esc_attr($featured_headline_border_color); ?>; }
<?php if($featured_headline_icon_image_id) { ?>
#featured_recipe .design_headline:before { background:<?php echo esc_attr($featured_headline_icon_color); ?> url(<?php echo esc_attr($featured_headline_icon_image[0]); ?>) no-repeat center; }
<?php } else { ?>
#featured_recipe .design_headline:before { background:<?php echo esc_attr($featured_headline_icon_color); ?>; font-family:'headline_icon'; content:'\e906'; font-size:25px; line-height:62px; }
@media screen and (max-width:1210px) {
  #featured_recipe .design_headline:before { font-size:20px; line-height:47px; }
}
<?php }; ?>
#featured_recipe .design_headline:after { border-color:<?php echo esc_attr($featured_headline_icon_color); ?> transparent transparent transparent; }
#featured_recipe .total_post { border-color:<?php echo esc_attr($featured_headline_border_color); ?>; }
#featured_recipe .recipe_list_desc { font-size:<?php echo esc_html($featured_desc_font_size); ?>px; }
#featured_recipe .recipe_list .title_area .title { font-size:<?php echo esc_html($featured_list_title_font_size); ?>px; }
@media screen and (max-width:650px) {
  #wide_page_header .catch { font-size:<?php echo esc_html($featured_header_catch_font_size_mobile); ?>px; }
  #wide_page_header .desc { font-size:<?php echo esc_html($featured_header_desc_font_size_mobile); ?>px; }
  #featured_recipe .design_headline { font-size:<?php echo esc_html($featured_headline_font_size_mobile); ?>px; }
  #featured_recipe .recipe_list_desc { font-size:<?php echo esc_html($featured_desc_font_size_mobile); ?>px; }
  #featured_recipe .recipe_list .title_area .title { font-size:<?php echo esc_html($featured_list_title_font_size_mobile); ?>px; }
}
<?php
     // ランキングページ --------------------------------------------------------------------
     } elseif(is_page_template('page-ranking-post-list.php')) {

       global $post;

       $ranking_headline_font_size = get_post_meta($post->ID, 'ranking_headline_font_size', true);
         if(empty($ranking_headline_font_size)){ $ranking_headline_font_size = '20'; }
       $ranking_headline_font_size_mobile = get_post_meta($post->ID, 'ranking_headline_font_size_mobile', true);
         if(empty($ranking_headline_font_size_mobile)){ $ranking_headline_font_size_mobile = '15'; }
       $ranking_headline_font_color = get_post_meta($post->ID, 'ranking_headline_font_color', true);
         if(empty($ranking_headline_font_color)){ $ranking_headline_font_color = '#000000'; }
       $ranking_headline_border_color = get_post_meta($post->ID, 'ranking_headline_border_color', true);
         if(empty($ranking_headline_border_color)){ $ranking_headline_border_color = '#dddddd'; }
       $ranking_headline_bg_color = get_post_meta($post->ID, 'ranking_headline_bg_color', true);
         if(empty($ranking_headline_bg_color)){ $ranking_headline_bg_color = '#ffffff'; }

       $ranking_headline_hide_icon = get_post_meta($post->ID, 'ranking_headline_hide_icon', true);
       $ranking_headline_icon_color = get_post_meta($post->ID, 'ranking_headline_icon_color', true);
         if(empty($ranking_headline_icon_color)){ $ranking_headline_icon_color = '#000000'; }

       $ranking_headline_icon_image_id = get_post_meta($post->ID, 'ranking_headline_icon_image', true);
         if($ranking_headline_icon_image_id) {
           $ranking_headline_icon_image = wp_get_attachment_image_src($ranking_headline_icon_image_id, 'full');
         }

       $ranking_desc_font_size = get_post_meta($post->ID, 'ranking_desc_font_size', true);
         if(empty($ranking_desc_font_size)){ $ranking_desc_font_size = '16'; }
       $ranking_desc_font_size_mobile = get_post_meta($post->ID, 'ranking_desc_font_size_mobile', true);
         if(empty($ranking_desc_font_size_mobile)){ $ranking_desc_font_size_mobile = '14'; }

       $ranking_list_title_font_size = get_post_meta($post->ID, 'ranking_list_title_font_size', true);
         if(empty($ranking_list_title_font_size)){ $ranking_list_title_font_size = '20'; }
       $ranking_list_title_font_size_mobile = get_post_meta($post->ID, 'ranking_list_title_font_size_mobile', true);
         if(empty($ranking_list_title_font_size_mobile)){ $ranking_list_title_font_size_mobile = '15'; }
?>
#ranking_recipe .design_headline { font-size:<?php echo esc_html($ranking_headline_font_size); ?>px; color:<?php echo esc_attr($ranking_headline_font_color); ?>; background:<?php echo esc_attr($ranking_headline_bg_color); ?>; border-color:<?php echo esc_attr($ranking_headline_border_color); ?>; }
<?php if($ranking_headline_icon_image_id) { ?>
#ranking_recipe .design_headline:before { background:<?php echo esc_attr($ranking_headline_icon_color); ?> url(<?php echo esc_attr($ranking_headline_icon_image[0]); ?>) no-repeat center; }
<?php } else { ?>
#ranking_recipe .design_headline:before { background:<?php echo esc_attr($ranking_headline_icon_color); ?>; font-family:'headline_icon'; content:'\e902'; font-size:24px; line-height:65px; }
@media screen and (max-width:1210px) {
  #ranking_recipe .design_headline:before { font-size:18px; line-height:52px; }
}
<?php }; ?>
#ranking_recipe .design_headline:after { border-color:<?php echo esc_attr($ranking_headline_icon_color); ?> transparent transparent transparent; }
#ranking_recipe .recipe_list_desc { font-size:<?php echo esc_html($ranking_desc_font_size); ?>px; }
.ranking_list .title, .ranking_list .rank { font-size:<?php echo esc_html($ranking_list_title_font_size); ?>px; }
@media screen and (max-width:650px) {
  #ranking_recipe .design_headline { font-size:<?php echo esc_html($ranking_headline_font_size_mobile); ?>px; }
  #ranking_recipe .recipe_list_desc { font-size:<?php echo esc_html($ranking_desc_font_size_mobile); ?>px; }
  .ranking_list .title, .ranking_list .rank { font-size:<?php echo esc_html($ranking_list_title_font_size_mobile); ?>px; }
}
<?php
     // 利用規約ページ --------------------------------------------------------------------
     } elseif(is_page_template('page-kiyaku.php')) {

       global $post;

       $kiyaku_page_headline_font_size = get_post_meta($post->ID, 'kiyaku_page_headline_font_size', true);
         if(empty($kiyaku_page_headline_font_size)){ $kiyaku_page_headline_font_size = '20'; }
       $kiyaku_page_headline_font_size_mobile = get_post_meta($post->ID, 'kiyaku_page_headline_font_size_mobile', true);
         if(empty($kiyaku_page_headline_font_size_mobile)){ $kiyaku_page_headline_font_size_mobile = '15'; }

       $kiyaku_page_headline_font_color = get_post_meta($post->ID, 'kiyaku_page_headline_font_color', true);
         if(empty($kiyaku_page_headline_font_color)){ $kiyaku_page_headline_font_color = '#000000'; }
       $kiyaku_page_headline_border_color = get_post_meta($post->ID, 'kiyaku_page_headline_border_color', true);
         if(empty($kiyaku_page_headline_border_color)){ $kiyaku_page_headline_border_color = '#dddddd'; }
       $kiyaku_page_headline_bg_color = get_post_meta($post->ID, 'kiyaku_page_headline_bg_color', true);
         if(empty($kiyaku_page_headline_bg_color)){ $kiyaku_page_headline_bg_color = '#ffffff'; }
       $kiyaku_page_headline_icon_color = get_post_meta($post->ID, 'kiyaku_page_headline_icon_color', true);
         if(empty($kiyaku_page_headline_icon_color)){ $kiyaku_page_headline_icon_color = '#000000'; }

       $kiyaku_page_headline_icon_image_id = get_post_meta($post->ID, 'kiyaku_page_headline_icon_image', true);
         if($kiyaku_page_headline_icon_image_id) {
           $kiyaku_page_headline_icon_image = wp_get_attachment_image_src($kiyaku_page_headline_icon_image_id, 'full');
         }

       $kiyaku_list_headline_color = get_post_meta($post->ID, 'kiyaku_list_headline_color', true);
         if(empty($kiyaku_list_headline_color)){ $kiyaku_list_headline_color = '#ff7f00'; }
       $kiyaku_list_headline_font_size = get_post_meta($post->ID, 'kiyaku_list_headline_font_size', true);
         if(empty($kiyaku_list_headline_font_size)){ $kiyaku_list_headline_font_size = '18'; }
       $kiyaku_list_headline_font_size_mobile = get_post_meta($post->ID, 'kiyaku_list_headline_font_size_mobile', true);
         if(empty($kiyaku_list_headline_font_size_mobile)){ $kiyaku_list_headline_font_size_mobile = '14'; }
       $kiyaku_list_desc_font_size = get_post_meta($post->ID, 'kiyaku_list_desc_font_size', true);
         if(empty($kiyaku_list_desc_font_size)){ $kiyaku_list_desc_font_size = '16'; }
       $kiyaku_list_desc_font_size_mobile = get_post_meta($post->ID, 'kiyaku_list_desc_font_size_mobile', true);
         if(empty($kiyaku_list_desc_font_size_mobile)){ $kiyaku_list_desc_font_size_mobile = '14'; }
?>
#kiyaku_headline { font-size:<?php echo esc_html($kiyaku_page_headline_font_size); ?>px; color:<?php echo esc_attr($kiyaku_page_headline_font_color); ?>; background:<?php echo esc_attr($kiyaku_page_headline_bg_color); ?>; border-color:<?php echo esc_attr($kiyaku_page_headline_border_color); ?>; }
<?php if($kiyaku_page_headline_icon_image_id) { ?>
#kiyaku_headline:before { background:<?php echo esc_attr($kiyaku_page_headline_icon_color); ?> url(<?php echo esc_attr($kiyaku_page_headline_icon_image[0]); ?>) no-repeat center; }
<?php } else { ?>
#kiyaku_headline:before { background:<?php echo esc_attr($kiyaku_page_headline_icon_color); ?>; font-family:'headline_icon'; content:'\e901'; font-size:23px; line-height:62px; }
@media screen and (max-width:1210px) {
  #kiyaku_headline:before { font-size:20px; line-height:47px; }
}
<?php }; ?>
#kiyaku_headline:after { border-color:<?php echo esc_attr($kiyaku_page_headline_icon_color); ?> transparent transparent transparent; }
#kiyaku .kiyaku_headline { font-size:<?php echo esc_html($kiyaku_list_headline_font_size); ?>px; color:<?php echo esc_attr($kiyaku_list_headline_color); ?>; }
#kiyaku .post_content { font-size:<?php echo esc_html($kiyaku_list_desc_font_size); ?>px; }
@media screen and (max-width:650px) {
  #kiyaku_headline { font-size:<?php echo esc_html($kiyaku_page_headline_font_size_mobile); ?>px; }
  #kiyaku .kiyaku_headline { font-size:<?php echo esc_html($kiyaku_list_headline_font_size_mobile); ?>px; }
  #kiyaku .post_content { font-size:<?php echo esc_html($kiyaku_list_desc_font_size_mobile); ?>px; }
}
<?php
     // ABOUTページ --------------------------------------------------------------------
     } elseif(is_page_template('page-about.php')) {

       global $post;

       $about_header_headline_font_size = get_post_meta($post->ID, 'about_header_headline_font_size', true);
         if(empty($about_header_headline_font_size)){ $about_header_headline_font_size = '14'; }
       $about_header_headline_font_size_mobile = get_post_meta($post->ID, 'about_header_headline_font_size_mobile', true);
         if(empty($about_header_headline_font_size_mobile)){ $about_header_headline_font_size_mobile = '11'; }

       $about_content_catch_font_size = get_post_meta($post->ID, 'about_content_catch_font_size', true);
         if(empty($about_content_catch_font_size)){ $about_content_catch_font_size = '26'; }
       $about_content_catch_font_size_mobile = get_post_meta($post->ID, 'about_content_catch_font_size_mobile', true);
         if(empty($about_content_catch_font_size_mobile)){ $about_content_catch_font_size_mobile = '20'; }
       $about_content_desc_font_size = get_post_meta($post->ID, 'about_content_desc_font_size', true);
         if(empty($about_content_desc_font_size)){ $about_content_desc_font_size = '16'; }
       $about_content_desc_font_size_mobile = get_post_meta($post->ID, 'about_content_desc_font_size_mobile', true);
         if(empty($about_content_desc_font_size_mobile)){ $about_content_desc_font_size_mobile = '14'; }
       $about_content_catch_color = get_post_meta($post->ID, 'about_content_catch_color', true);
         if(empty($about_content_catch_color)){ $about_content_catch_color = '#ff8000'; }
?>
#about_header .headline { font-size:<?php echo esc_html($about_header_headline_font_size); ?>px; }
.about_content .headline { font-size:<?php echo esc_html($about_content_catch_font_size); ?>px; color:<?php echo esc_html($about_content_catch_color); ?>; }
.about_content .post_content, #about_faq_list { font-size:<?php echo esc_html($about_content_desc_font_size); ?>px; }
@media screen and (max-width:650px) {
  #about_header .headline { font-size:<?php echo esc_html($about_header_headline_font_size_mobile); ?>px; }
  .about_content .headline { font-size:<?php echo esc_html($about_content_catch_font_size_mobile); ?>px; }
  .about_content .post_content, #about_faq_list { font-size:<?php echo esc_html($about_content_desc_font_size_mobile); ?>px; }
}
<?php
     // 固定ページ --------------------------------------------------------------------
     } elseif(is_page()) {

       global $post;

       $page_header_type = get_post_meta($post->ID, 'page_header_type', true);
       if(empty($page_header_type)){
         $page_header_type = 'type1';
       }

       if($page_header_type == 'type1') {

         $page_title_font_size = get_post_meta($post->ID, 'page_title_font_size', true);
           if(empty($page_title_font_size)){ $page_title_font_size = '38'; }
         $page_title_font_size_mobile = get_post_meta($post->ID, 'page_title_font_size_mobile', true);
           if(empty($page_title_font_size_mobile)){ $page_title_font_size_mobile = '20'; }
         $page_sub_title_font_size = get_post_meta($post->ID, 'page_sub_title_font_size', true);
           if(empty($page_sub_title_font_size)){ $page_sub_title_font_size = '18'; }
         $page_sub_title_font_size_mobile = get_post_meta($post->ID, 'page_sub_title_font_size_mobile', true);
           if(empty($page_sub_title_font_size_mobile)){ $page_sub_title_font_size_mobile = '13'; }
?>
#wide_page_header .catch { font-size:<?php echo esc_html($page_title_font_size); ?>px; }
#wide_page_header .desc { font-size:<?php echo esc_html($page_sub_title_font_size); ?>px; }
@media screen and (max-width:650px) {
  #wide_page_header .catch { font-size:<?php echo esc_html($page_title_font_size_mobile); ?>px; }
  #wide_page_header .desc { font-size:<?php echo esc_html($page_sub_title_font_size_mobile); ?>px; }
}
<?php
       } elseif($page_header_type == 'type2') {

         $page_headline_font_size = get_post_meta($post->ID, 'page_headline_font_size', true);
           if(empty($page_headline_font_size)){ $page_headline_font_size = '20'; }
         $page_headline_font_size_mobile = get_post_meta($post->ID, 'page_headline_font_size_mobile', true);
           if(empty($page_headline_font_size_mobile)){ $page_headline_font_size_mobile = '15'; }

         $page_headline_font_color = get_post_meta($post->ID, 'page_headline_font_color', true);
           if(empty($page_headline_font_color)){ $page_headline_font_color = '#000000'; }
         $page_headline_border_color = get_post_meta($post->ID, 'page_headline_border_color', true);
           if(empty($page_headline_border_color)){ $page_headline_border_color = '#dddddd'; }
         $page_headline_bg_color = get_post_meta($post->ID, 'page_headline_bg_color', true);
           if(empty($page_headline_bg_color)){ $page_headline_bg_color = '#ffffff'; }
         $page_headline_icon_color = get_post_meta($post->ID, 'page_headline_icon_color', true);
           if(empty($page_headline_icon_color)){ $page_headline_icon_color = '#000000'; }

         $page_headline_icon_image_id = get_post_meta($post->ID, 'page_headline_icon_image', true);
           if($page_headline_icon_image_id) {
             $page_headline_icon_image = wp_get_attachment_image_src($page_headline_icon_image_id, 'full');
           }
?>
#page_header_design_headline { font-size:<?php echo esc_html($page_headline_font_size); ?>px; color:<?php echo esc_attr($page_headline_font_color); ?>; background:<?php echo esc_attr($page_headline_bg_color); ?>; border-color:<?php echo esc_attr($page_headline_border_color); ?>; }
<?php if($page_headline_icon_image_id) { ?>
#page_header_design_headline:before { background:<?php echo esc_attr($page_headline_icon_color); ?> url(<?php echo esc_attr($page_headline_icon_image[0]); ?>) no-repeat center; }
<?php } else { ?>
#page_header_design_headline:before { background:<?php echo esc_attr($page_headline_icon_color); ?>; font-family:'headline_icon'; content:'\e90a'; font-size:37px; line-height:65px; }
@media screen and (max-width:1210px) {
  #page_header_design_headline:before { font-size:32px; line-height:52px; }
}
<?php }; ?>
#page_header_design_headline:after { border-color:<?php echo esc_attr($page_headline_icon_color); ?> transparent transparent transparent; }
@media screen and (max-width:650px) {
  #page_header_design_headline { font-size:<?php echo esc_html($page_headline_font_size_mobile); ?>px; }
}
<?php
       }
     // 404ページ -----------------------------------------------------------------------------
     } elseif( is_404()) {
       $title_font_size_pc = ( ! empty( $options['header_txt_size_404'] ) ) ? $options['header_txt_size_404'] : 38;
       $sub_title_font_size_pc = ( ! empty( $options['header_sub_txt_size_404'] ) ) ? $options['header_sub_txt_size_404'] : 16;
       $title_font_size_mobile = ( ! empty( $options['header_txt_size_404_mobile'] ) ) ? $options['header_txt_size_404_mobile'] : 28;
       $sub_title_font_size_mobile = ( ! empty( $options['header_sub_txt_size_404_mobile'] ) ) ? $options['header_sub_txt_size_404_mobile'] : 14;
?>
#page_404_header .catch { font-size:<?php echo esc_html($title_font_size_pc); ?>px; }
#page_404_header .desc { font-size:<?php echo esc_html($sub_title_font_size_pc); ?>px; }
@media screen and (max-width:650px) {
  #page_404_header .catch { font-size:<?php echo esc_html($title_font_size_mobile); ?>px; }
  #page_404_header .desc { font-size:<?php echo esc_html($sub_title_font_size_mobile); ?>px; }
}
<?php
     }; //END page setting
?>

<?php
     // サムネイルのアニメーション設定　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
     if($options['hover_type']!="type4"){

       // ズーム ------------------------------------------------------------------------------
       if($options['hover_type']=="type1"){
?>
.author_profile a.avatar img, .animate_image img, .animate_background .image, #recipe_archive .blur_image {
  width:100%; height:auto;
  -webkit-transition: transform  0.75s ease;
  transition: transform  0.75s ease;
}
.author_profile a.avatar:hover img, .animate_image:hover img, .animate_background:hover .image, #recipe_archive a:hover .blur_image {
  -webkit-transform: scale(<?php echo $options['hover1_zoom']; ?>);
  transform: scale(<?php echo $options['hover1_zoom']; ?>);
}


<?php
     // スライド ------------------------------------------------------------------------------
     } elseif($options['hover_type']=="type2"){
?>
.animate_image img, .animate_background .image {
  -webkit-width:calc(100% + 30px) !important; width:calc(100% + 30px) !important; height:auto; max-width:inherit !important; position:relative;
  <?php if($options['hover2_direct']=='type1'): ?>
  -webkit-transform: translate(-15px, 0px); -webkit-transition-property: opacity, translateX; -webkit-transition: 0.5s;
  transform: translate(-15px, 0px); transition-property: opacity, translateX; transition: 0.5s;
  <?php else: ?>
  -webkit-transform: translate(-15px, 0px); -webkit-transition-property: opacity, translateX; -webkit-transition: 0.5s;
  transform: translate(-15px, 0px); transition-property: opacity, translateX; transition: 0.5s;
  <?php endif; ?>
}
.animate_image:hover img, .animate_background:hover .image {
  opacity:<?php echo $options['hover2_opacity']; ?>;
  <?php if($options['hover2_direct']=='type1'): ?>
  -webkit-transform: translate(0px, 0px);
  transform: translate(0px, 0px);
  <?php else: ?>
  -webkit-transform: translate(-30px, 0px);
  transform: translate(-30px, 0px);
  <?php endif; ?>
}
.animate_image.square img {
  -webkit-width:calc(100% + 30px) !important; width:calc(100% + 30px) !important; height:auto; max-width:inherit !important; position:relative;
  <?php if($options['hover2_direct']=='type1'): ?>
  -webkit-transform: translate(-15px, -15px); -webkit-transition-property: opacity, translateX; -webkit-transition: 0.5s;
  transform: translate(-15px, -15px); transition-property: opacity, translateX; transition: 0.5s;
  <?php else: ?>
  -webkit-transform: translate(-15px, -15px); -webkit-transition-property: opacity, translateX; -webkit-transition: 0.5s;
  transform: translate(-15px, -15px); transition-property: opacity, translateX; transition: 0.5s;
  <?php endif; ?>
}
.animate_image.square:hover img {
  opacity:<?php echo $options['hover2_opacity']; ?>;
  <?php if($options['hover2_direct']=='type1'): ?>
  -webkit-transform: translate(0px, -15px);
  transform: translate(0px, -15px);
  <?php else: ?>
  -webkit-transform: translate(-30px, -15px);
  transform: translate(-30px, -15px);
  <?php endif; ?>
}
<?php
     // フェードアウト ------------------------------------------------------------------------------
     } elseif($options['hover_type']=="type3"){
?>
.author_profile a.avatar, .animate_image, .animate_background, .animate_background .image_wrap {
  background: <?php echo $options['hover3_bgcolor']; ?>;
}
.author_profile a.avatar img, .animate_image img, .animate_background .image {
  -webkit-transition-property: opacity; -webkit-transition: 0.5s;
  transition-property: opacity; transition: 0.5s;
}
.author_profile a.avatar:hover img, .animate_image:hover img, .animate_background:hover .image {
  opacity: <?php echo $options['hover3_opacity']; ?>;
}
<?php }; }; // アニメーションここまで ?>


<?php
     // 色関連のスタイル　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

     // メインカラー ----------------------------------
     $main_color = esc_html($options['main_color']);
?>
a { color:#000; }

a:hover, #header_logo a, #footer_logo a, #header_user_name .user_name, #comment_headline, .tcd_category_list a:hover, .tcd_category_list .child_menu_button:hover, .register_form_wrap .privacy_policy a, #my_account_edit #delete_account:hover, .widget_tab_post_list a:hover .date, #post_title_area .post_meta a:hover, #news_list a:hover .date,
  .recipe_list .title_area .title a:hover, .recipe_list .link:hover .title, .recipe_list .link:hover .post_meta, #post_list a:hover .title, #post_list a:hover .post_meta li, #recipe_archive a:hover .desc, .cf_data_list li a:hover, #footer_social_link li a:hover:before, #recipe_title_area .meta li a:hover, #recipe_image_slider .slick-arrow:hover:after, .recipe_slider_widget .slick-arrow:hover:before,
    #footer a:hover, .cardlink_title a:hover, #related_post .item a:hover, .comment a:hover, .comment_form_wrapper a:hover, #bread_crumb, #bread_crumb .last, #bread_crumb a:hover, #bread_crumb li.home a:hover:after, .author_profile a:hover, .author_profile .author_link li a:hover:before, #post_meta_bottom a:hover, .recipe_list .title_area .post_meta a:hover,
      #author_page_header .author_link li a:hover:before, #ranking_list_tab li.active a, .ranking_list a:hover .title, #author_list a:hover .title span, #searchform .submit_button:hover:before, .styled_post_list1 a:hover .title_area, .styled_post_list1 a:hover .date, .p-dropdown__title:hover:after, .p-dropdown__list li a:hover,
        #index_recipe_slider a:hover .title, #index_recipe_slider a:hover .post_meta, #index_recipe_slider .owl-nav button:hover span:after, .recipe_slider_widget .slick-arrow:hover:after, #about_faq_list dt:hover,#about_faq_list dt.active, #about_faq_list dt:hover:after,
          #menu_button:hover:before, .mobile #header_login:hover, .mobile #header_logout:hover, .mobile #header_search_button:hover:before
  { color: <?php echo $main_color; ?>; }

.pc #header_search_button:hover, #index_slider .search_button:hover input, #return_top a, #comment_tab li a:hover, #comment_tab li.active a, #comment_header #comment_closed p, #submit_comment:hover, #cancel_comment_reply a:hover,
  #recipe_image_slider .slick-dots button:hover::before, #recipe_image_slider .slick-dots .slick-active button::before, .form_wrap .submit input, .login_form_wrap #create_account, .register_form_wrap .register_form_header, .recipe_list2 .delete:hover,
    #wp-calendar #prev a:hover, #wp-calendar #next a:hover, #wp-calendar td a:hover, #p_readmore .button, .page_navi span.current, .page_navi a:hover, #post_pagination p, #post_pagination a:hover, .c-pw__btn:hover, #post_pagination a:hover
  { background-color: <?php echo $main_color; ?>; }

.form_wrap .input_field:focus, #guest_info input:focus, #comment_textarea textarea:focus, .c-pw__box-input:focus, .page_navi span.current, .page_navi a:hover, #post_pagination p, #post_pagination a:hover
  { border-color: <?php echo $main_color; ?>; }

#comment_tab li.active a:after, #comment_header #comment_closed p:after
  { border-color:<?php echo $main_color; ?> transparent transparent transparent; }

.modal_wrap .close_modal_button:hover:before
  { color: <?php echo $main_color; ?> !important; }

<?php
     // サブカラー ----------------------------------
     $sub_color = esc_html($options['sub_color']);
?>
#header_logo a:hover, #footer_logo a:hover, .register_form_wrap .privacy_policy a:hover, .megamenu_recipe_category_list .headline a:hover
  { color: <?php echo $sub_color; ?>; }
#header_register:hover, #return_top a:hover, .form_wrap .submit input:hover, .login_form_wrap #create_account:hover, #p_readmore .button:hover
  { background-color: <?php echo $sub_color; ?>; }
<?php
     // その他のカラー ----------------------------------
?>
.post_content a { color: <?php echo esc_html($options['content_link_color']); ?>; }
.post_content a:hover { color:<?php echo esc_html($options['content_link_hover_color']); ?>; }
<?php
     // その他のスタイル ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

     // ロード画面 -----------------------------------------
     get_template_part('functions/loader');
     if($options['load_icon'] == 'type4'){
?>
#site_loader_logo_inner p { font-size:<?php echo esc_html($options['load_type4_catch_font_size']); ?>px; color:<?php echo esc_html($options['load_type4_catch_color']); ?>; }
@media screen and (max-width:750px) {
  #site_loader_logo_inner p { font-size:<?php echo esc_html($options['load_type4_catch_font_size_sp']); ?>px; }
}
<?php
     };

     //フッターバー --------------------------------------------
     if(is_mobile()) {
       if($options['footer_bar_display'] == 'type1' || $options['footer_bar_display'] == 'type2') {
?>
.dp-footer-bar { background: <?php echo 'rgba('.implode(',', hex2rgb($options['footer_bar_bg'])).', '.esc_html($options['footer_bar_tp']).');'; ?> border-top: solid 1px <?php echo esc_html($options['footer_bar_border']); ?>; color: <?php echo esc_html($options['footer_bar_color']); ?>; display: flex; flex-wrap: wrap; }
.dp-footer-bar a { color: <?php echo esc_html($options['footer_bar_color']); ?>; }
.dp-footer-bar-item + .dp-footer-bar-item { border-left: solid 1px <?php echo esc_html($options['footer_bar_border']); ?>; }
<?php
       };
     };
?>

<?php
     // カスタムCSS --------------------------------------------
     if($options['css_code']) {
       echo wp_kses_post($options['css_code']);
     };
     if(is_single() || is_page()) {
       global $post;
       $custom_css = get_post_meta($post->ID, 'custom_css', true);
       if($custom_css) {
         echo wp_kses_post($custom_css);
       };
     }
?>

</style>

<?php
     // JavaScriptの設定はここから　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

     // トップページ
     if(is_front_page()) {
       // 画像スライダー --------------------------------------------------
       if($options['index_header_content_type'] == 'type1'){
         wp_enqueue_style('slick-style', apply_filters('page_builder_slider_slick_style_url', get_template_directory_uri().'/js/slick.css'), '', '1.0.0');
         wp_enqueue_script('slick-script', apply_filters('page_builder_slider_slick_script_url', get_template_directory_uri().'/js/slick.min.js'), '', '1.0.0', true);
?>
<script type="text/javascript">
jQuery(document).ready(function($){

  $('#index_slider').slick({
    infinite: true,
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: false,
    pauseOnFocus: true,
    pauseOnHover: false,
    autoplay: true,
    fade: true,
    slide: '.item',
    easing: 'easeOutExpo',
    speed: 1500,
    autoplaySpeed: <?php echo esc_html($options['index_slider_time']); ?>
  });
  $('#index_slider').on("beforeChange", function(event, slick, currentSlide, nextSlide) {
    $('#index_slider .item').removeClass('first_item');
  });

  var mqls = [
    window.matchMedia("(min-width: 951px)"),
    window.matchMedia("(max-width: 651px)")
  ]
  function mediaqueryresponse(mql){
    if (mqls[0].matches){ // 951px
      var base_image = $('#index_slider');
      var image_width = base_image.width();
      var target_image = $('#index_slider .item .slice_image');
      target_image.css({'width': Math.ceil(image_width / 8)});
      $(window).on('resize',function(){
        base_image = $('#index_slider');
        image_width = base_image.width();
        target_image.css({'width': Math.ceil(image_width / 8)});
      });
    }
    if (mqls[1].matches){ // 651px
      var base_image = $('#index_slider');
      var image_width = base_image.width();
      var target_image = $('#index_slider .item .slice_image');
      target_image.css({'width': Math.ceil(image_width / 4)});
      $(window).on('resize',function(){
        base_image = $('#index_slider');
        image_width = base_image.width();
        target_image.css({'width': Math.ceil(image_width / 4)});
      });
    }
    if (!mqls[0].matches && !mqls[1].matches){ // 652 ~ 950px
      var base_image = $('#index_slider');
      var image_width = base_image.width();
      var target_image = $('#index_slider .item .slice_image');
      target_image.css({'width': Math.ceil(image_width / 6)});
      $(window).on('resize',function(){
        base_image = $('#index_slider');
        image_width = base_image.width();
        target_image.css({'width': Math.ceil(image_width / 6)});
      });
    }
  }
  for (var i=0; i<mqls.length; i++){
    mediaqueryresponse(mqls[i])
    mqls[i].addListener(mediaqueryresponse)
  }

});
</script>
<?php
     };
     // Youtube ------------------------------------------------------------
     if($options['index_header_content_type'] == 'type3') {
       $youtube_url = $options['index_youtube_url'];
       if($youtube_url && auto_play_movie()) {
         $matches = array();
         if(preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[\w\-?&!#=,;]+/[\w\-?&!#=/,;]+/|(?:v|e(?:mbed)?)/|[\w\-?&!#=,;]*[?&]v=)|youtu\.be/)([\w-]{11})(?:[^\w-]|\Z)%i', $youtube_url, $matches)) {
           $video_id = $matches[1];
         }
         if($video_id) {
?>
<script src="https://www.youtube.com/iframe_api"></script>
<script type="text/javascript">
function onYouTubeIframeAPIReady() {
  ytPlayer = new YT.Player(
    'youtube_video_player',{
      videoId: '<?php echo esc_html($video_id); ?>',
      playerVars: {
        loop: 1,
        playlist: '<?php echo esc_html($video_id); ?>',
        controls: 0,
        autoplay: 1,
        showinfo: 0
      },
      events: {
       'onReady': onPlayerReady
      }
    }
  );
}
function onPlayerReady(event) {
  event.target.playVideo();
  event.target.mute();
}
jQuery(document).ready(function($) {

   $(window).on('load',function(){
     set_youtube_width_height($('#youtube_video_wrap'));
   });

   $(window).on('resize',function(){
     set_youtube_width_height($('#youtube_video_wrap'));
   });

   function set_youtube_width_height(object){
     var slider_height = $('#index_video').innerHeight();
     var slider_width = slider_height*(16/9);
     var win_width = $(window).width();
     var win_height = win_width*(9/16);
     if(win_width > slider_width) {
       object.addClass('type1');
       object.removeClass('type2');
       object.css({'width': '100%', 'height': win_height});
     } else {
       object.removeClass('type1');
       object.addClass('type2');
       object.css({'width':slider_width, 'height':slider_height });
     }
   }

});
</script>
<?php
         };
       };
     };

     // レシピスライダー --------------------------------------------------
     if($options['show_index_recipe_slider']){
       wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.css','','1.0.0');
       wp_enqueue_style('owl-theme-default', get_template_directory_uri() . '/js/owl.theme.default.min.css','','1.0.0');
       wp_enqueue_script('owl-carousel-js', get_template_directory_uri().'/js/owl.carousel.min.js', '', '1.0.0', true);
?>
<script type="text/javascript">
jQuery(document).ready(function($){

  $('#index_recipe_slider').owlCarousel({
    loop: true,
    center: true,
    items: 3,
    autoWidth: true,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplaySpeed: 1000,
    autoplayHoverPause: true,
    dots: false,
    nav: true,
    navSpeed: 1000,
    responsive : {
      0 : {
        margin: 10,
        nav: false,
      },
      1100 : {
        margin: 15,
        nav: true,
      }
    }
  });

});
</script>
<?php
         }; // END recipe slider

       }; // END front page

       // レシピアーカイブページ ---------------------------------
       if(is_post_type_archive('recipe')) {
?>
<script type="text/javascript">
jQuery(document).ready(function($){
  var base_image = $('#recipe_archive .item');
  var target_image = $('#recipe_archive .blur_image');
  var image_width = base_image.width();
  var image_height = base_image.height();
  target_image.css({'width': image_width, 'height': image_height });
  $(window).on('resize',function(){
    image_width = base_image.width();
    image_height = base_image.height();
    target_image.css({'width': image_width, 'height': image_height });
  });
});
</script>
<?php
       };

       // レシピカテゴリーページ ---------------------------------
       if(is_tax('recipe_category')) {
?>
<script type="text/javascript">
jQuery(document).ready(function($){
 var menu_num = $('#recipe_child_category_list li').length;
 var menu_mql = window.matchMedia('screen and (min-width: 651px)');
 function checkBreakPointMenu(menu_mql) {
  if(menu_mql.matches){
    $('#recipe_child_category_list').css("width","auto");
  } else {
    $('#recipe_child_category_list').css("width",137 * menu_num);
    if ($('#recipe_child_category_list_wrap').length) {
      if(! $(body).hasClass('mobile_device') ) {
        new SimpleBar($('#recipe_child_category_list_wrap')[0]);
      };
    };
  }
}
menu_mql.addListener(checkBreakPointMenu);
checkBreakPointMenu(menu_mql);

});
</script>
<?php
       };

       // レシピ詳細ページ ---------------------------------
       if(is_singular('recipe')) {
         $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
         // MP4動画の場合
         if($recipe_type == 'type2') {
           wp_enqueue_style('slick-style', apply_filters('page_builder_slider_slick_style_url', get_template_directory_uri().'/js/slick.css'), '', '1.0.0');
           wp_enqueue_script('slick-script', apply_filters('page_builder_slider_slick_script_url', get_template_directory_uri().'/js/slick.min.js'), '', '1.0.0', true);
?>
<script type="text/javascript">
jQuery(document).ready(function($){

  $('#recipe_image_slider').slick({
    infinite: true,
    dots: true,
    arrows: false,
    slidesToShow: 1,
    adaptiveHeight: false,
    pauseOnFocus: false,
    pauseOnHover: false,
    autoplay: true,
    easing: 'easeOutExpo',
    speed: 700,
    autoplaySpeed: 10000,
  });

  var base_slider = $('#recipe_image_slider');
  var base_slider_item = $('#recipe_image_slider .item');
  var slider_width = base_slider.width();
  var slider_height = slider_width*(9/16);
  base_slider.css('height',slider_height);
  base_slider_item.css('height',slider_height);
  $(window).on('resize',function(){
    slider_width = base_slider.width();
    slider_height = slider_width*(9/16);
    base_slider.css('height',slider_height);
    base_slider_item.css('height',slider_height);
  });

});
</script>
<?php
     // Youtubeの場合
     } elseif($recipe_type == 'type3') {
       global $post;
       $recipe_youtube_url = get_post_meta($post->ID, 'recipe_youtube_url', true);
       if($recipe_youtube_url){
         $matches = array();
         if(preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[\w\-?&!#=,;]+/[\w\-?&!#=/,;]+/|(?:v|e(?:mbed)?)/|[\w\-?&!#=,;]*[?&]v=)|youtu\.be/)([\w-]{11})(?:[^\w-]|\Z)%i', $recipe_youtube_url, $matches)) {
           $video_id = $matches[1];
         }
         if($video_id) {
?>
<script src="https://www.youtube.com/iframe_api"></script>
<script type="text/javascript">
function onYouTubeIframeAPIReady() {
  ytPlayer = new YT.Player(
    'youtube_player',{
      videoId: '<?php echo esc_html($video_id); ?>',
      playerVars: {
        loop: 0,
        playlist: '<?php echo esc_html($video_id); ?>',
        controls: 0,
        autoplay: 0,
        showinfo: 0
      },
      events: {
       'onReady': onPlayerReady
      }
    }
  );
}
function onPlayerReady(event) {
  event.target.playVideo();
}
</script>
<?php
           };
         };
       };
     }; // END recipe single page

     // 講師ページ ---------------------------------
     if(is_author()) {
?>
<script type="text/javascript">
jQuery(document).ready(function($){
  var base_image = $('#author_page_header .image');
  var target_image = $('#author_page_header .blur_image');
  var image_width = base_image.width();
  var image_height = base_image.height();
  target_image.css({'width': image_width, 'height': image_height });
  $(window).on('resize',function(){
    image_width = base_image.width();
    image_height = base_image.height();
    target_image.css({'width': image_width, 'height': image_height });
  });
});
</script>
<?php
       };

       // ABOUTページ ---------------------------------
       if(is_page_template('page-about.php')) {
?>
<script type="text/javascript">
jQuery(document).ready(function($){
  $('#about_faq_list dt').on('click', function() {
    $('#about_faq_list dt').not($(this)).removeClass('active');
    if( $(this).hasClass('active') ){
      $(this).removeClass('active');
    } else {
      $(this).addClass('active');
    }
    $(this).next('dd').slideToggle();
    $('#about_faq_list dd').not($(this).next('dd')).slideUp();
  });
});
</script>
<?php
       };

       // スライダーウィジェット --------------------
       if ( is_active_widget(false, false, 'recipe_slider_widget', true) ) {
         wp_enqueue_style('slick-style', apply_filters('page_builder_slider_slick_style_url', get_template_directory_uri().'/js/slick.css'), '', '1.0.0');
         wp_enqueue_script('slick-script', apply_filters('page_builder_slider_slick_script_url', get_template_directory_uri().'/js/slick.min.js'), '', '1.0.0', true);
?>
<script type="text/javascript">
jQuery(document).ready(function($){

  if( $('.recipe_slider').length ){
    $('.recipe_slider').slick({
      infinite: true,
      dots: false,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: false,
      pauseOnHover: false,
      autoplay: true,
      fade: false,
      easing: 'easeOutExpo',
      speed: 700,
      autoplaySpeed: 7000,
      responsive: [
        {
          breakpoint: 950,
          settings: { slidesToShow: 2 }
        },
        {
          breakpoint: 550,
          settings: { slidesToShow: 1 }
        }
      ]
    });
  }

});
</script>
<?php
       } // スライダーウィジェット

     }; // END function tcd_head()

     add_action("wp_head", "tcd_head");
?>
<?php

function featured_meta_box() {
  add_meta_box(
    'featured_meta_box',//ID of meta box
    __('Featured page setting', 'tcd-w'),
    'show_featured_meta_box',//callback function
    'page',// post type
    'normal',// context
    'high'// priority
  );
}
add_action('add_meta_boxes', 'featured_meta_box');

function show_featured_meta_box() {
  global $post, $font_type_options;

  $options = get_design_plus_option();
  $recipe_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );

  // ヘッダー
  $featured_header_catch = get_post_meta($post->ID, 'featured_header_catch', true);
  $featured_header_catch_font_size = get_post_meta($post->ID, 'featured_header_catch_font_size', true);
  if(empty($featured_header_catch_font_size)){
    $featured_header_catch_font_size = '38';
  }
  $featured_header_catch_font_size_mobile = get_post_meta($post->ID, 'featured_header_catch_font_size_mobile', true);
  if(empty($featured_header_catch_font_size_mobile)){
    $featured_header_catch_font_size_mobile = '20';
  }
  $featured_header_catch_font_type = get_post_meta($post->ID, 'featured_header_catch_font_type', true);
  if(empty($featured_header_catch_font_type)){
    $featured_header_catch_font_type = 'type3';
  }

  $featured_header_desc = get_post_meta($post->ID, 'featured_header_desc', true);
  $featured_header_desc_font_size = get_post_meta($post->ID, 'featured_header_desc_font_size', true);
  if(empty($featured_header_desc_font_size)){
    $featured_header_desc_font_size = '18';
  }
  $featured_header_desc_font_size_mobile = get_post_meta($post->ID, 'featured_header_desc_font_size_mobile', true);
  if(empty($featured_header_desc_font_size_mobile)){
    $featured_header_desc_font_size_mobile = '13';
  }

  $featured_header_font_color = get_post_meta($post->ID, 'featured_header_font_color', true);
  if(empty($featured_header_font_color)){
    $featured_header_font_color = '#FFFFFF';
  }
  $featured_header_bg_color = get_post_meta($post->ID, 'featured_header_bg_color', true);
  if(empty($featured_header_bg_color)){
    $featured_header_bg_color = '#666666';
  }

  $featured_header_use_overlay = get_post_meta($post->ID, 'featured_header_use_overlay', true);
  $featured_header_overlay_color = get_post_meta($post->ID, 'featured_header_overlay_color', true);
  if(empty($featured_header_overlay_color)){
    $featured_header_overlay_color = '#000000';
  }
  $featured_header_overlay_opacity = get_post_meta($post->ID, 'featured_header_overlay_opacity', true);
  if(empty($featured_header_overlay_opacity)){
    $featured_header_overlay_opacity = '0.5';
  }


  // 見出し
  $featured_headline = get_post_meta($post->ID, 'featured_headline', true);
  if(empty($featured_headline)){
    $featured_headline = __('Featured','tcd-w');
  }
  $featured_headline_font_size = get_post_meta($post->ID, 'featured_headline_font_size', true);
  if(empty($featured_headline_font_size)){
    $featured_headline_font_size = '20';
  }
  $featured_headline_font_size_mobile = get_post_meta($post->ID, 'featured_headline_font_size_mobile', true);
  if(empty($featured_headline_font_size_mobile)){
    $featured_headline_font_size_mobile = '15';
  }
  $featured_headline_font_color = get_post_meta($post->ID, 'featured_headline_font_color', true);
  if(empty($featured_headline_font_color)){
    $featured_headline_font_color = '#000000';
  }
  $featured_headline_border_color = get_post_meta($post->ID, 'featured_headline_border_color', true);
  if(empty($featured_headline_border_color)){
    $featured_headline_border_color = '#dddddd';
  }
  $featured_headline_bg_color = get_post_meta($post->ID, 'featured_headline_bg_color', true);
  if(empty($featured_headline_bg_color)){
    $featured_headline_bg_color = '#ffffff';
  }

  $featured_headline_hide_icon = get_post_meta($post->ID, 'featured_headline_hide_icon', true);
  $featured_headline_icon_color = get_post_meta($post->ID, 'featured_headline_icon_color', true);
  if(empty($featured_headline_icon_color)){
    $featured_headline_icon_color = '#000000';
  }


  $show_featured_total_post = get_post_meta($post->ID, 'show_featured_total_post', true);
  $featured_total_post_label = get_post_meta($post->ID, 'featured_total_post_label', true);
  if(empty($featured_total_post_label)){
    $featured_total_post_label = __( 'recipe', 'tcd-w' );
  }

  // 説明文
  $featured_desc = get_post_meta($post->ID, 'featured_desc', true);
  $featured_desc_font_size = get_post_meta($post->ID, 'featured_desc_font_size', true);
  if(empty($featured_desc_font_size)){
    $featured_desc_font_size = '16';
  }
  $featured_desc_font_size_mobile = get_post_meta($post->ID, 'featured_desc_font_size_mobile', true);
  if(empty($featured_desc_font_size_mobile)){
    $featured_desc_font_size_mobile = '14';
  }

  // 記事一覧
  $featured_list_post_type = get_post_meta($post->ID, 'featured_list_post_type', true);
  if(empty($featured_list_post_type)){
    $featured_list_post_type = 'all';
  }

  $featured_list_num = get_post_meta($post->ID, 'featured_list_num', true);
  if(empty($featured_list_num)){
    $featured_list_num = '9';
  }
  $featured_list_title_font_size = get_post_meta($post->ID, 'featured_list_title_font_size', true);
  if(empty($featured_list_title_font_size)){
    $featured_list_title_font_size = '16';
  }
  $featured_list_title_font_size_mobile = get_post_meta($post->ID, 'featured_list_title_font_size_mobile', true);
  if(empty($featured_list_title_font_size_mobile)){
    $featured_list_title_font_size_mobile = '14';
  }
  $featured_list_show_post_view = get_post_meta($post->ID, 'featured_list_show_post_view', true);
  $featured_list_show_premium_icon = get_post_meta($post->ID, 'featured_list_show_premium_icon', true);

  echo '<input type="hidden" name="featured_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

  //入力欄 ***************************************************************************************************************************************************************************************
?>

<?php
     // WP5.0対策として隠しフィールドを用意　選択されているページテンプレートによってABOUT入力欄を表示・非表示する
     if ( count( get_page_templates( $post ) ) > 0 && get_option( 'page_for_posts' ) != $post->ID ) :
       $template = ! empty( $post->page_template ) ? $post->page_template : false;
?>
<select name="hidden_page_template" id="hidden_page_template" style="display:none;">
 <option value="default">Default Template</option>
 <?php page_template_dropdown( $template, 'page' ); ?>
</select>
<?php endif; ?>

<div class="tcd_custom_field_wrap">

  <?php // ヘッダー ----------------------------------------- ?>
  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php _e( 'Header setting', 'tcd-w' ); ?></h3>
   <div class="theme_option_field_ac_content">

    <h3 class="theme_option_headline2"><?php _e('Catchphrase', 'tcd-w'); ?></h3>
    <textarea class="large-text" cols="50" rows="2" name="featured_header_catch"><?php echo esc_textarea($featured_header_catch); ?></textarea>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="featured_header_catch_font_size" value="<?php echo esc_attr($featured_header_catch_font_size); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="featured_header_catch_font_size_mobile" value="<?php echo esc_attr($featured_header_catch_font_size_mobile); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font type', 'tcd-w'); ?></span>
      <select name="featured_header_catch_font_type">
       <?php
            foreach ( $font_type_options as $option ) {
            if(strtoupper(get_locale()) == 'JA'){
              $label = $option['label'];
            } else {
              $label = $option['label_en'];
            }
       ?>
       <option style="padding-right: 10px;" value="<?php esc_attr_e( $option['value'] ); ?>" <?php selected( $featured_header_catch_font_type, $option['value'] ); ?>><?php echo esc_html($label); ?></option>
       <?php }; ?>
      </select>
     </li>
    </ul>

    <h3 class="theme_option_headline2"><?php _e('Description', 'tcd-w'); ?></h3>
    <textarea class="large-text" cols="50" rows="4" name="featured_header_desc"><?php echo esc_textarea($featured_header_desc); ?></textarea>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="featured_header_desc_font_size" value="<?php echo esc_attr($featured_header_desc_font_size); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="featured_header_desc_font_size_mobile" value="<?php echo esc_attr($featured_header_desc_font_size_mobile); ?>" /><span>px</span></li>
    </ul>

    <h3 class="theme_option_headline2"><?php _e( 'Color setting', 'tcd-w' ); ?></h3>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="featured_header_font_color" value="<?php echo esc_attr($featured_header_font_color); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="featured_header_bg_color" value="<?php echo esc_attr($featured_header_bg_color); ?>" data-default-color="#666666" class="c-color-picker"></li>
    </ul>

    <h3 class="theme_option_headline2"><?php _e( 'Background image', 'tcd-w' ); ?></h3>
    <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '1450', '500'); ?></p>
    <?php mlcf_media_form('featured_header_bg_image', __('Background image', 'tcd-w')); ?>

    <h3 class="theme_option_headline2"><?php _e( 'Background image (mobile)', 'tcd-w' ); ?></h3>
    <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '750', '1334'); ?></p>
    <?php mlcf_media_form('featured_header_bg_image_mobile', __('Background image (mobile)', 'tcd-w')); ?>

    <h3 class="theme_option_headline2"><?php _e( 'Overlay setting', 'tcd-w' ); ?></h3>
    <p class="displayment_checkbox"><label for="featured_header_use_overlay"><input id="featured_header_use_overlay" type="checkbox" name="featured_header_use_overlay" value="1" <?php if( $featured_header_use_overlay == '1' ) { echo 'checked="checked"'; }; ?> /><?php _e( 'Use overlay', 'tcd-w' ); ?></label></p>
    <div class="blog_show_overlay" style="<?php if($featured_header_use_overlay == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
     <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
      <li class="cf"><span class="label"><?php _e('Color of overlay', 'tcd-w'); ?></span><input type="text" name="featured_header_overlay_color" value="<?php echo esc_attr($featured_header_overlay_color); ?>" data-default-color="#000000" class="c-color-picker" /></li>
      <li class="cf">
       <span class="label"><?php _e('Transparency of overlay', 'tcd-w'); ?></span><input class="hankaku" style="width:70px;" type="number" max="1" min="0" step="0.1" type="text" name="featured_header_overlay_opacity" value="<?php echo esc_attr($featured_header_overlay_opacity); ?>" />
       <div class="theme_option_message2" style="clear:both; margin:7px 0 0 0;">
        <p><?php _e('Please specify the number of 0.1 from 0.9. Overlay color will be more transparent as the number is small.', 'tcd-w');  ?></p>
       </div>
      </li>
     </ul>
    </div>

    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->

  <?php // 見出し ----------------------------------------- ?>
  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php _e( 'Headline and description setting', 'tcd-w' ); ?></h3>
   <div class="theme_option_field_ac_content">
    <h4 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w');  ?></h4>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w'); ?></span><input class="full_width" type="text" name="featured_headline" value="<?php esc_attr_e( $featured_headline ); ?>" /></li>
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="featured_headline_font_size" value="<?php esc_attr_e( $featured_headline_font_size ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="featured_headline_font_size_mobile" value="<?php esc_attr_e( $featured_headline_font_size_mobile ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="featured_headline_font_color" value="<?php echo esc_attr( $featured_headline_font_color ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="featured_headline_bg_color" value="<?php echo esc_attr( $featured_headline_bg_color ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Border color', 'tcd-w'); ?></span><input type="text" name="featured_headline_border_color" value="<?php echo esc_attr( $featured_headline_border_color ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Hide icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="featured_headline_hide_icon" value="1" <?php checked( $featured_headline_hide_icon, 1 ); ?>></li>
    </ul>
    <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($featured_headline_hide_icon) { echo ' display:none;'; }; ?>">
     <li class="cf"><span class="label"><?php _e('Icon background color', 'tcd-w'); ?></span><input type="text" name="featured_headline_icon_color" value="<?php echo esc_attr( $featured_headline_icon_color ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Icon image', 'tcd-w');  ?></span>
      <?php mlcf_media_form('featured_headline_icon_image', __('Image', 'tcd-w') ); ?>
      <div class="theme_option_message2">
       <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
       <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
      </div>
     </li>
    </ul>
    <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;">
     <li class="cf"><span class="label"><?php _e('Display total post', 'tcd-w'); ?></span><input name="show_featured_total_post" type="checkbox" value="1" <?php checked( $show_featured_total_post, 1 ); ?>></li>
     <li class="cf"><span class="label"><?php _e('Label of total count', 'tcd-w'); ?></span><input class="full_width" type="text" name="featured_total_post_label" value="<?php echo esc_attr( $featured_total_post_label ); ?>" /></li>
    </ul>
    <h4 class="theme_option_headline2"><?php _e('Description setting', 'tcd-w');  ?></h4>
    <textarea class="large-text" cols="50" rows="2" name="featured_desc"><?php echo esc_textarea($featured_desc); ?></textarea>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="featured_desc_font_size" value="<?php esc_attr_e( $featured_desc_font_size ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="featured_desc_font_size_mobile" value="<?php esc_attr_e( $featured_desc_font_size_mobile ); ?>" /><span>px</span></li>
    </ul>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->

  <?php // 記事一覧 ----------------------------------------- ?>
  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php printf(__('%s list setting', 'tcd-w'), $recipe_label); ?></h3>
   <div class="theme_option_field_ac_content">
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Post type', 'tcd-w');  ?></span>
      <select name="featured_list_post_type">
       <option style="padding-right: 10px;" value="all" <?php selected( $featured_list_post_type, 'all' ); ?>><?php printf(__('All %s', 'tcd-w'), $recipe_label); ?></option>
       <option style="padding-right: 10px;" value="premium_recipe" <?php selected( $featured_list_post_type, 'premium_recipe' ); ?>><?php printf(__('Premium %s', 'tcd-w'), $recipe_label); ?></option>
       <option style="padding-right: 10px;" value="featured_recipe1" <?php selected( $featured_list_post_type, 'featured_recipe1' ); ?>><?php printf(__('Featured %s', 'tcd-w'), $recipe_label); ?>1</option>
       <option style="padding-right: 10px;" value="featured_recipe2" <?php selected( $featured_list_post_type, 'featured_recipe2' ); ?>><?php printf(__('Featured %s', 'tcd-w'), $recipe_label); ?>2</option>
       <option style="padding-right: 10px;" value="featured_recipe3" <?php selected( $featured_list_post_type, 'featured_recipe3' ); ?>><?php printf(__('Featured %s', 'tcd-w'), $recipe_label); ?>3</option>
      </select>
     </li>
     <li class="cf"><span class="label"><?php _e('Number of post to display', 'tcd-w');  ?></span>
      <select name="featured_list_num">
       <?php for($i=3; $i<= 21; $i++): ?>
       <?php if( $i % 3 == 0 ){ ?>
       <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $featured_list_num, $i ); ?>><?php echo esc_html($i); ?></option>
       <?php }; ?>
       <?php endfor; ?>
      </select>
     </li>
     <li class="cf"><span class="label"><?php _e('Font size of title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="featured_list_title_font_size" value="<?php esc_attr_e( $featured_list_title_font_size ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size of title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="featured_list_title_font_size_mobile" value="<?php esc_attr_e( $featured_list_title_font_size_mobile ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php printf(__('Display premium %s icon', 'tcd-w'), $recipe_label); ?></span><input name="featured_list_show_premium_icon" type="checkbox" value="1" <?php checked( '1', $featured_list_show_premium_icon ); ?> /></li>
     <li class="cf"><span class="label"><?php _e('Display post view count', 'tcd-w');  ?></span><input name="featured_list_show_post_view" type="checkbox" value="1" <?php checked( '1', $featured_list_show_post_view ); ?> /></li>
    </ul>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->

</div><!-- END .tcd_custom_field_wrap -->

<?php
}

function save_featured_meta_box( $post_id ) {

  // verify nonce
  if (!isset($_POST['featured_meta_box_nonce']) || !wp_verify_nonce($_POST['featured_meta_box_nonce'], basename(__FILE__))) {
    return $post_id;
  }

  // check autosave
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  // check permissions
  if ('page' == $_POST['post_type']) {
    if (!current_user_can('edit_page', $post_id)) {
      return $post_id;
    }
  } elseif (!current_user_can('edit_post', $post_id)) {
      return $post_id;
  }

  // save or delete
  $cf_keys = array(
    'featured_header_bg_image','featured_header_bg_image_mobile','featured_header_bg_color','featured_header_use_overlay','featured_header_overlay_color','featured_header_overlay_opacity',
    'featured_header_catch', 'featured_header_catch_font_size','featured_header_catch_font_size_mobile','featured_header_desc_font_size','featured_header_desc_font_size_mobile','featured_header_font_color','featured_header_desc', 'featured_header_catch_font_type',
    'featured_headline','featured_headline_font_size','featured_headline_font_size_mobile','featured_headline_font_color','featured_headline_border_color','featured_headline_bg_color','featured_headline_icon_color','featured_headline_icon_image','featured_headline_hide_icon',
    'show_featured_total_post','featured_total_post_label',
    'featured_desc','featured_desc_font_size','featured_desc_font_size_mobile',
    'featured_list_post_type','featured_list_num','featured_list_title_font_size','featured_list_title_font_size_mobile','featured_list_show_premium_icon','featured_list_show_post_view'
  );
  foreach ($cf_keys as $cf_key) {
    $old = get_post_meta($post_id, $cf_key, true);

    if (isset($_POST[$cf_key])) {
      $new = $_POST[$cf_key];
    } else {
      $new = '';
    }

    if ($new && $new != $old) {
      update_post_meta($post_id, $cf_key, $new);
    } elseif ('' == $new && $old) {
      delete_post_meta($post_id, $cf_key, $old);
    }
  }


}
add_action('save_post', 'save_featured_meta_box');




?>

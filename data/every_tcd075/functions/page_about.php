<?php

function about_meta_box() {
  add_meta_box(
    'about_meta_box',//ID of meta box
    __('ABOUT page setting', 'tcd-w'),//label
    'show_about_meta_box',//callback function
    'page',// post type
    'normal',// context
    'high'// priority
  );
}
add_action('add_meta_boxes', 'about_meta_box');

function show_about_meta_box() {
  global $post, $font_type_options;;

  // ヘッダー -------------------------------------------------------
  $about_header_headline = get_post_meta($post->ID, 'about_header_headline', true);
  $about_header_headline_font_size = get_post_meta($post->ID, 'about_header_headline_font_size', true);
  if(empty($about_header_headline_font_size)){
    $about_header_headline_font_size = '14';
  }
  $about_header_headline_font_size_mobile = get_post_meta($post->ID, 'about_header_headline_font_size_mobile', true);
  if(empty($about_header_headline_font_size_mobile)){
    $about_header_headline_font_size_mobile = '11';
  }
  $about_header_headline_color = get_post_meta($post->ID, 'about_header_headline_color', true);
  if(empty($about_header_headline_color)){
    $about_header_headline_color = '#ff8000';
  }

  // コンテンツの共通設定 -----------------------------------------------
  $about_content_catch_font_size = get_post_meta($post->ID, 'about_content_catch_font_size', true);
  if(empty($about_content_catch_font_size)){
    $about_content_catch_font_size = '26';
  }
  $about_content_catch_font_size_mobile = get_post_meta($post->ID, 'about_content_catch_font_size_mobile', true);
  if(empty($about_content_catch_font_size_mobile)){
    $about_content_catch_font_size_mobile = '20';
  }
  $about_content_desc_font_size = get_post_meta($post->ID, 'about_content_desc_font_size', true);
  if(empty($about_content_desc_font_size)){
    $about_content_desc_font_size = '16';
  }
  $about_content_desc_font_size_mobile = get_post_meta($post->ID, 'about_content_desc_font_size_mobile', true);
  if(empty($about_content_desc_font_size_mobile)){
    $about_content_desc_font_size_mobile = '14';
  }
  $about_content_catch_color = get_post_meta($post->ID, 'about_content_catch_color', true);
  if(empty($about_content_catch_color)){
    $about_content_catch_color = '#ff8000';
  }
  $about_content_catch_font_type = get_post_meta($post->ID, 'about_content_catch_font_type', true);
  if(empty($about_content_catch_font_type)){
    $about_content_catch_font_type = 'type3';
  }

  // コンテンツ１ -------------------------------------------------------
  $show_about_content1 = get_post_meta($post->ID, 'show_about_content1', true);

  $about_content1_catch = get_post_meta($post->ID, 'about_content1_catch', true);
  $about_content1_desc = get_post_meta($post->ID, 'about_content1_desc', true);

  // コンテンツ２ -------------------------------------------------------
  $show_about_content2 = get_post_meta($post->ID, 'show_about_content2', true);

  $about_content2_catch = get_post_meta($post->ID, 'about_content2_catch', true);
  $about_content2_desc = get_post_meta($post->ID, 'about_content2_desc', true);

  // コンテンツ３ -------------------------------------------------------
  $show_about_content3 = get_post_meta($post->ID, 'show_about_content3', true);

  $about_content3_catch = get_post_meta($post->ID, 'about_content3_catch', true);
  $about_content3_desc = get_post_meta($post->ID, 'about_content3_desc', true);

  // コンテンツ４ -------------------------------------------------------
  $show_about_content4 = get_post_meta($post->ID, 'show_about_content4', true);

  $about_content4_catch = get_post_meta($post->ID, 'about_content4_catch', true);
  $about_content4_desc = get_post_meta($post->ID, 'about_content4_desc', true);
  $about_content4_data_list = get_post_meta($post->ID, 'about_content4_data_list', true);


  // 並び替え
  $page_content_order = get_post_meta($post->ID, 'page_content_order', true);
  if(empty($page_content_order)) {
    $page_content_order = array('content1','content2','content3','content4');
  }

  echo '<input type="hidden" name="design1_custom_fields_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

  //入力欄 ***************************************************************************************************************************************************************************************
?>

<?php
     // WP5.0対策として隠しフィールドを用意　選択されているページテンプレートによってABOUT入力欄を表示・非表示する
     if ( count( get_page_templates( $post ) ) > 0 && get_option( 'page_for_posts' ) != $post->ID ) :
       $template = ! empty( $post->page_template ) ? $post->page_template : false;
?>
<select name="hidden_page_template" id="hidden_page_template" style="display:none;">
 <option value="default">Default Template</option>
 <?php page_template_dropdown( $template, 'page' ); ?>
</select>
<?php endif; ?>

<div class="tcd_custom_field_wrap">

  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php _e( 'Header setting', 'tcd-w' ); ?></h3>
   <div class="theme_option_field_ac_content">
    <h3 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w'); ?></h3>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="about_header_headline_font_size" value="<?php esc_attr_e( $about_header_headline_font_size ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="about_header_headline_font_size_mobile" value="<?php esc_attr_e( $about_header_headline_font_size_mobile ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="about_header_headline_color" value="<?php echo esc_attr( $about_header_headline_color ); ?>" data-default-color="#ff8000" class="c-color-picker"></li>
    </ul>
    <h3 class="theme_option_headline2"><?php _e('Background image', 'tcd-w'); ?></h3>
    <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '810', '455'); ?></p>
    <?php mlcf_media_form('about_header_image', __('Image', 'tcd-w') ); ?>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->

  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php _e( 'Content basic setting', 'tcd-w' ); ?></h3>
   <div class="theme_option_field_ac_content">
    <h3 class="theme_option_headline2"><?php _e('Catchphrase setting', 'tcd-w'); ?></h3>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="about_content_catch_font_size" value="<?php echo esc_attr($about_content_catch_font_size); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="about_content_catch_font_size_mobile" value="<?php echo esc_attr($about_content_catch_font_size_mobile); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="about_content_catch_color" value="<?php echo esc_attr($about_content_catch_color); ?>" data-default-color="#ff8000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Font type', 'tcd-w'); ?></span>
      <select name="about_content_catch_font_type">
       <?php
            foreach ( $font_type_options as $option ) {
            if(strtoupper(get_locale()) == 'JA'){
              $label = $option['label'];
            } else {
              $label = $option['label_en'];
            }
       ?>
       <option style="padding-right: 10px;" value="<?php esc_attr_e( $option['value'] ); ?>" <?php selected( $about_content_catch_font_type, $option['value'] ); ?>><?php echo esc_html($label); ?></option>
       <?php }; ?>
      </select>
     </li>
    </ul>
    <h3 class="theme_option_headline2"><?php _e('Description setting', 'tcd-w'); ?></h3>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="about_content_desc_font_size" value="<?php echo esc_attr($about_content_desc_font_size); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="about_content_desc_font_size_mobile" value="<?php echo esc_attr($about_content_desc_font_size_mobile); ?>" /><span>px</span></li>
    </ul>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->

 <div class="theme_option_message">
  <p><?php _e('You can change order by dragging each headline of option field.', 'tcd-w');  ?></p>
 </div>

 <?php // 並び替え ----- ?>
 <div class="theme_option_field_order" style="margin-bottom:15px;">
 <?php foreach((array) $page_content_order as $page_content) : ?>

 <?php // コンテンツ１ ----------------------------------------------------- ?>
 <?php if ($page_content == 'content1') : ?>
  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php printf(__('Content%s setting', 'tcd-w'), 1); ?></h3>
   <div class="theme_option_field_ac_content">
    <p><label for="show_about_content1"><input id="show_about_content1" type="checkbox" name="show_about_content1" value="1" <?php checked( $show_about_content1, 1 ); ?>><?php printf(__('Display content%s', 'tcd-w'), 1); ?></label></p>
    <h3 class="theme_option_headline2"><?php _e('Catchphrase', 'tcd-w'); ?></h3>
    <textarea class="large-text" cols="50" rows="2" name="about_content1_catch"><?php echo esc_textarea($about_content1_catch); ?></textarea>
    <h3 class="theme_option_headline2"><?php _e('Description', 'tcd-w'); ?></h3>
    <?php wp_editor( $about_content1_desc, 'about_content1_desc', array( 'textarea_name' => 'about_content1_desc', 'textarea_rows' => 10 )); ?>
    <h3 class="theme_option_headline2"><?php _e('Image', 'tcd-w'); ?>1</h3>
    <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '405', '270'); ?></p>
    <?php mlcf_media_form('about_content1_image1', __('Image', 'tcd-w') ); ?>
    <h3 class="theme_option_headline2"><?php _e('Image', 'tcd-w'); ?>2</h3>
    <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '405', '270'); ?></p>
    <?php mlcf_media_form('about_content1_image2', __('Image', 'tcd-w') ); ?>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
    <input type="hidden" name="page_content_order[]" value="content1" />
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->
 <?php endif; ?>


 <?php // コンテンツ２ ----------------------------------------------------- ?>
 <?php if ($page_content == 'content2') : ?>
  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php printf(__('Content%s setting', 'tcd-w'), 2); ?></h3>
   <div class="theme_option_field_ac_content">
    <p><label for="show_about_content2"><input id="show_about_content2" type="checkbox" name="show_about_content2" value="1" <?php checked( $show_about_content2, 1 ); ?>><?php printf(__('Display content%s', 'tcd-w'), 2); ?></label></p>
    <h3 class="theme_option_headline2"><?php _e('Catchphrase', 'tcd-w'); ?></h3>
    <textarea class="large-text" cols="50" rows="2" name="about_content2_catch"><?php echo esc_textarea($about_content2_catch); ?></textarea>
    <h3 class="theme_option_headline2"><?php _e('Description', 'tcd-w'); ?></h3>
    <?php wp_editor( $about_content2_desc, 'about_content2_desc', array( 'textarea_name' => 'about_content2_desc', 'textarea_rows' => 10 )); ?>
    <h3 class="theme_option_headline2"><?php _e('Image', 'tcd-w'); ?>1</h3>
    <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '270', '270'); ?></p>
    <?php mlcf_media_form('about_content2_image1', __('Image', 'tcd-w') ); ?>
    <h3 class="theme_option_headline2"><?php _e('Image', 'tcd-w'); ?>2</h3>
    <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '270', '270'); ?></p>
    <?php mlcf_media_form('about_content2_image2', __('Image', 'tcd-w') ); ?>
    <h3 class="theme_option_headline2"><?php _e('Image', 'tcd-w'); ?>3</h3>
    <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '270', '270'); ?></p>
    <?php mlcf_media_form('about_content2_image3', __('Image', 'tcd-w') ); ?>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
    <input type="hidden" name="page_content_order[]" value="content2" />
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->
 <?php endif; ?>


 <?php // コンテンツ３ ----------------------------------------------------- ?>
 <?php if ($page_content == 'content3') : ?>
  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php printf(__('Content%s setting', 'tcd-w'), 3); ?></h3>
   <div class="theme_option_field_ac_content">
    <p><label for="show_about_content3"><input id="show_about_content3" type="checkbox" name="show_about_content3" value="1" <?php checked( $show_about_content3, 1 ); ?>><?php printf(__('Display content%s', 'tcd-w'), 3); ?></label></p>
    <h3 class="theme_option_headline2"><?php _e('Catchphrase', 'tcd-w'); ?></h3>
    <textarea class="large-text" cols="50" rows="2" name="about_content3_catch"><?php echo esc_textarea($about_content3_catch); ?></textarea>
    <h3 class="theme_option_headline2"><?php _e('Description', 'tcd-w'); ?></h3>
    <?php wp_editor( $about_content3_desc, 'about_content3_desc', array( 'textarea_name' => 'about_content3_desc', 'textarea_rows' => 10 )); ?>
    <h3 class="theme_option_headline2"><?php _e('Image', 'tcd-w'); ?></h3>
    <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '810', '270'); ?></p>
    <?php mlcf_media_form('about_content3_image1', __('Image', 'tcd-w') ); ?>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
    <input type="hidden" name="page_content_order[]" value="content3" />
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->
 <?php endif; ?>


 <?php // コンテンツ４ ----------------------------------------------------- ?>
 <?php if ($page_content == 'content4') : ?>
  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php printf(__('Content%s setting', 'tcd-w'), 4); ?></h3>
   <div class="theme_option_field_ac_content">
    <p><label for="show_about_content4"><input id="show_about_content4" type="checkbox" name="show_about_content4" value="1" <?php checked( $show_about_content4, 1 ); ?>><?php printf(__('Display content%s', 'tcd-w'), 4); ?></label></p>
    <h3 class="theme_option_headline2"><?php _e('Catchphrase', 'tcd-w'); ?></h3>
    <textarea class="large-text" cols="50" rows="2" name="about_content4_catch"><?php echo esc_textarea($about_content4_catch); ?></textarea>
    <h3 class="theme_option_headline2"><?php _e('Description', 'tcd-w'); ?></h3>
    <?php wp_editor( $about_content4_desc, 'about_content4_desc', array( 'textarea_name' => 'about_content4_desc', 'textarea_rows' => 10 )); ?>
    <?php // データ一覧 ---------- ?>
    <h3 class="theme_option_headline2"><?php _e( 'FAQ list', 'tcd-w' ); ?></h3>
    <div class="theme_option_message2">
     <p><?php _e('You can change order by dragging each item.', 'tcd-w'); ?></p>
    </div>
    <?php //繰り返しフィールド ----- ?>
    <div class="repeater-wrapper">
     <div class="repeater sortable" data-delete-confirm="<?php _e( 'Delete?', 'tcd-w' ); ?>">
      <?php
           if ( $about_content4_data_list ) :
             foreach ( $about_content4_data_list as $key => $value ) :
      ?>
      <div class="sub_box repeater-item repeater-item-<?php echo $key; ?>">
       <h4 class="theme_option_subbox_headline"><?php _e( 'New item', 'tcd-w' ); ?></h4>
       <div class="sub_box_content">
        <h4 class="theme_option_headline2"><?php _e( 'Question', 'tcd-w' ); ?></h4>
        <p><input class="repeater-label" type="text" name="about_content4_data_list[<?php echo esc_attr( $key ); ?>][title]" value="<?php echo esc_attr( isset( $about_content4_data_list[$key]['title'] ) ? $about_content4_data_list[$key]['title'] : '' ); ?>" style="width:100%" /></p>
        <h4 class="theme_option_headline2"><?php _e( 'Answer', 'tcd-w' ); ?></h4>
        <textarea class="large-text" cols="50" rows="2" name="about_content4_data_list[<?php echo esc_attr( $key ); ?>][desc]"><?php echo esc_textarea( isset( $about_content4_data_list[$key]['desc'] ) ? $about_content4_data_list[$key]['desc'] : '' ); ?></textarea>
        <p class="delete-row right-align"><a href="#" class="button button-secondary button-delete-row"><?php _e( 'Delete item', 'tcd-w' ); ?></a></p>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php
             endforeach;
           endif;
           $key = 'addindex';
           ob_start();
      ?>
      <div class="sub_box repeater-item repeater-item-<?php echo $key; ?>">
       <h4 class="theme_option_subbox_headline"><?php echo esc_html( ! empty( $about_content4_data_list[$key]['title'] ) ? $about_content4_data_list[$key]['title'] : __( 'New item', 'tcd-w' ) ); ?></h4>
       <div class="sub_box_content">
        <h4 class="theme_option_headline2"><?php _e( 'Question', 'tcd-w' );  ?></h4>
        <p><input class="repeater-label" type="text" name="about_content4_data_list[<?php echo esc_attr( $key ); ?>][title]" value="" style="width:100%" /></p>
        <h4 class="theme_option_headline2"><?php _e( 'Answer', 'tcd-w' );  ?></h4>
        <textarea class="large-text" cols="50" rows="2" name="about_content4_data_list[<?php echo esc_attr( $key ); ?>][desc]"></textarea>
        <p class="delete-row right-align"><a href="#" class="button button-secondary button-delete-row"><?php _e( 'Delete item', 'tcd-w' ); ?></a></p>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php
           $clone = ob_get_clean();
      ?>
     </div><!-- END .repeater -->
     <a href="#" class="button button-secondary button-add-row" data-clone="<?php echo esc_attr( $clone ); ?>"><?php _e( 'Add item', 'tcd-w' ); ?></a>
    </div><!-- END .repeater-wrapper -->
    <?php // 繰り返しフィールドここまで ----- ?>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
    <input type="hidden" name="page_content_order[]" value="content4" />
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->
 <?php endif; ?>

 <?php endforeach; // 並び替えここまで ?>
 </div><!-- END .theme_option_field_order -->

</div><!-- END .tcd_custom_field_wrap -->

<?php
}

function save_about_meta_box( $post_id ) {

  // verify nonce
  if (!isset($_POST['design1_custom_fields_meta_box_nonce']) || !wp_verify_nonce($_POST['design1_custom_fields_meta_box_nonce'], basename(__FILE__))) {
    return $post_id;
  }

  // check autosave
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  // check permissions
  if ('page' == $_POST['post_type']) {
    if (!current_user_can('edit_page', $post_id)) {
      return $post_id;
    }
  } elseif (!current_user_can('edit_post', $post_id)) {
      return $post_id;
  }

  // save or delete
  $cf_keys = array(
    'about_header_headline','about_header_headline_font_size','about_header_headline_font_size_mobile','about_header_headline_color','about_header_image',
    'about_content_catch_font_size','about_content_catch_font_size_mobile','about_content_desc_font_size','about_content_desc_font_size_mobile','about_content_catch_color','about_content_catch_font_type',
    'show_about_content1',
    'about_content1_catch','about_content1_desc','about_content1_image1','about_content1_image2',
    'show_about_content2',
    'about_content2_catch','about_content2_desc','about_content2_image1','about_content2_image2', 'about_content2_image3',
    'show_about_content3',
    'about_content3_catch','about_content3_desc','about_content3_image1',
    'show_about_content4',
    'about_content4_catch','about_content4_desc',
  );
  foreach ($cf_keys as $cf_key) {
    $old = get_post_meta($post_id, $cf_key, true);

    if (isset($_POST[$cf_key])) {
      $new = $_POST[$cf_key];
    } else {
      $new = '';
    }

    if ($new && $new != $old) {
      update_post_meta($post_id, $cf_key, $new);
    } elseif ('' == $new && $old) {
      delete_post_meta($post_id, $cf_key, $old);
    }
  }

  // repeater save or delete
  $cf_keys = array( 'about_content4_data_list', 'page_content_order' );
  foreach ( $cf_keys as $cf_key ) {
    $old = get_post_meta( $post_id, $cf_key, true );

    if ( isset( $_POST[$cf_key] ) && is_array( $_POST[$cf_key] ) ) {
      $new = array_values( $_POST[$cf_key] );
    } else {
      $new = false;
    }

    if ( $new && $new != $old ) {
      update_post_meta( $post_id, $cf_key, $new );
    } elseif ( ! $new && $old ) {
      delete_post_meta( $post_id, $cf_key, $old );
    }
  }

}
add_action('save_post', 'save_about_meta_box');




?>

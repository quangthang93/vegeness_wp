<?php

function ranking_meta_box() {
  add_meta_box(
    'ranking_meta_box',//ID of meta box
    __('Ranking page setting', 'tcd-w'),
    'show_ranking_meta_box',//callback function
    'page',// post type
    'normal',// context
    'high'// priority
  );
}
add_action('add_meta_boxes', 'ranking_meta_box');

function show_ranking_meta_box() {
  global $post;

  $options = get_design_plus_option();
  $recipe_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );

  // 見出し
  $ranking_headline = get_post_meta($post->ID, 'ranking_headline', true);
  if(empty($ranking_headline)){
    $ranking_headline = __('Ranking','tcd-w');
  }
  $ranking_headline_font_size = get_post_meta($post->ID, 'ranking_headline_font_size', true);
  if(empty($ranking_headline_font_size)){
    $ranking_headline_font_size = '20';
  }
  $ranking_headline_font_size_mobile = get_post_meta($post->ID, 'ranking_headline_font_size_mobile', true);
  if(empty($ranking_headline_font_size_mobile)){
    $ranking_headline_font_size_mobile = '15';
  }
  $ranking_headline_font_color = get_post_meta($post->ID, 'ranking_headline_font_color', true);
  if(empty($ranking_headline_font_color)){
    $ranking_headline_font_color = '#000000';
  }
  $ranking_headline_border_color = get_post_meta($post->ID, 'ranking_headline_border_color', true);
  if(empty($ranking_headline_border_color)){
    $ranking_headline_border_color = '#dddddd';
  }
  $ranking_headline_bg_color = get_post_meta($post->ID, 'ranking_headline_bg_color', true);
  if(empty($ranking_headline_bg_color)){
    $ranking_headline_bg_color = '#ffffff';
  }

  $ranking_headline_hide_icon = get_post_meta($post->ID, 'ranking_headline_hide_icon', true);
  $ranking_headline_icon_color = get_post_meta($post->ID, 'ranking_headline_icon_color', true);
  if(empty($ranking_headline_icon_color)){
    $ranking_headline_icon_color = '#000000';
  }

  // 説明文
  $ranking_desc = get_post_meta($post->ID, 'ranking_desc', true);
  $ranking_desc_font_size = get_post_meta($post->ID, 'ranking_desc_font_size', true);
  if(empty($ranking_desc_font_size)){
    $ranking_desc_font_size = '16';
  }
  $ranking_desc_font_size_mobile = get_post_meta($post->ID, 'ranking_desc_font_size_mobile', true);
  if(empty($ranking_desc_font_size_mobile)){
    $ranking_desc_font_size_mobile = '14';
  }

  // 記事一覧
  $ranking_list_range = get_post_meta($post->ID, 'ranking_list_range', true);

  $ranking_list_num = get_post_meta($post->ID, 'ranking_list_num', true);
  if(empty($ranking_list_num)){
    $ranking_list_num = '10';
  }

  $ranking_list_title_font_size = get_post_meta($post->ID, 'ranking_list_title_font_size', true);
  if(empty($ranking_list_title_font_size)){
    $ranking_list_title_font_size = '20';
  }
  $ranking_list_title_font_size_mobile = get_post_meta($post->ID, 'ranking_list_title_font_size_mobile', true);
  if(empty($ranking_list_title_font_size_mobile)){
    $ranking_list_title_font_size_mobile = '15';
  }
  $ranking_list_show_category = get_post_meta($post->ID, 'ranking_list_show_category', true);
  $ranking_list_show_premium_icon = get_post_meta($post->ID, 'ranking_list_show_premium_icon', true);

  $ranking_list_color = get_post_meta($post->ID, 'ranking_list_color', true);
  if(empty($ranking_list_color)){
    $ranking_list_color = '#ff7f00';
  }

  // タブ名
  $ranking_tab_label1 = get_post_meta($post->ID, 'ranking_tab_label1', true);
  if ('' === $ranking_tab_label1){
    $ranking_tab_label1 = __('Daily', 'tcd-w');
  }

  $ranking_tab_label2 = get_post_meta($post->ID, 'ranking_tab_label2', true);
  if ('' === $ranking_tab_label2){
    $ranking_tab_label2 = __('Weekly', 'tcd-w');
  }

  $ranking_tab_label3 = get_post_meta($post->ID, 'ranking_tab_label3', true);
  if ('' === $ranking_tab_label3){
    $ranking_tab_label3 = __('Monthly', 'tcd-w');
  }

  $ranking_tab_label4 = get_post_meta($post->ID, 'ranking_tab_label4', true);
  if ('' === $ranking_tab_label4){
    $ranking_tab_label4 = __('Yearly', 'tcd-w');
  }

  echo '<input type="hidden" name="ranking_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

  //入力欄 ***************************************************************************************************************************************************************************************
?>

<?php
     // WP5.0対策として隠しフィールドを用意　選択されているページテンプレートによってABOUT入力欄を表示・非表示する
     if ( count( get_page_templates( $post ) ) > 0 && get_option( 'page_for_posts' ) != $post->ID ) :
       $template = ! empty( $post->page_template ) ? $post->page_template : false;
?>
<select name="hidden_page_template" id="hidden_page_template" style="display:none;">
 <option value="default">Default Template</option>
 <?php page_template_dropdown( $template, 'page' ); ?>
</select>
<?php endif; ?>

<div class="tcd_custom_field_wrap">

  <?php // 見出し ----------------------------------------- ?>
  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php _e( 'Headline and description setting', 'tcd-w' ); ?></h3>
   <div class="theme_option_field_ac_content">
    <h4 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w');  ?></h4>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w'); ?></span><input class="full_width" type="text" name="ranking_headline" value="<?php esc_attr_e( $ranking_headline ); ?>" /></li>
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="ranking_headline_font_size" value="<?php esc_attr_e( $ranking_headline_font_size ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="ranking_headline_font_size_mobile" value="<?php esc_attr_e( $ranking_headline_font_size_mobile ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="ranking_headline_font_color" value="<?php echo esc_attr( $ranking_headline_font_color ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="ranking_headline_bg_color" value="<?php echo esc_attr( $ranking_headline_bg_color ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Border color', 'tcd-w'); ?></span><input type="text" name="ranking_headline_border_color" value="<?php echo esc_attr( $ranking_headline_border_color ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Hide icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="ranking_headline_hide_icon" value="1" <?php checked( $ranking_headline_hide_icon, 1 ); ?>></li>
    </ul>
    <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($ranking_headline_hide_icon) { echo ' display:none;'; }; ?>">
     <li class="cf"><span class="label"><?php _e('Icon background color', 'tcd-w'); ?></span><input type="text" name="ranking_headline_icon_color" value="<?php echo esc_attr( $ranking_headline_icon_color ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Icon image', 'tcd-w');  ?></span>
      <?php mlcf_media_form('ranking_headline_icon_image', __('Image', 'tcd-w') ); ?>
      <div class="theme_option_message2">
       <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
       <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
      </div>
     </li>
    </ul>
    <h4 class="theme_option_headline2"><?php _e('Description setting', 'tcd-w');  ?></h4>
    <textarea class="large-text" cols="50" rows="2" name="ranking_desc"><?php echo esc_textarea($ranking_desc); ?></textarea>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="ranking_desc_font_size" value="<?php esc_attr_e( $ranking_desc_font_size ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="ranking_desc_font_size_mobile" value="<?php esc_attr_e( $ranking_desc_font_size_mobile ); ?>" /><span>px</span></li>
    </ul>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->

  <?php // 記事一覧 ----------------------------------------- ?>
  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php printf(__('%s list setting', 'tcd-w'), $recipe_label); ?></h3>
   <div class="theme_option_field_ac_content">
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Number of post to display', 'tcd-w');  ?></span>
      <select name="ranking_list_num">
       <?php for($i=5; $i<= 20; $i++): ?>
       <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $ranking_list_num, $i ); ?>><?php echo esc_html($i); ?></option>
       <?php endfor; ?>
      </select>
     </li>
     <li class="cf"><span class="label"><?php _e('Font size of title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="ranking_list_title_font_size" value="<?php esc_attr_e( $ranking_list_title_font_size ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size of title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="ranking_list_title_font_size_mobile" value="<?php esc_attr_e( $ranking_list_title_font_size_mobile ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Rank color', 'tcd-w'); ?></span><input type="text" name="ranking_list_color" value="<?php echo esc_attr( $ranking_list_color ); ?>" data-default-color="#ff7f00" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php printf(__('Display premium %s icon', 'tcd-w'), $recipe_label); ?></span><input name="ranking_list_show_premium_icon" type="checkbox" value="1" <?php checked( '1', $ranking_list_show_premium_icon ); ?> /></li>
     <li class="cf"><span class="label"><?php _e('Display category', 'tcd-w');  ?></span><input name="ranking_list_show_category" type="checkbox" value="1" <?php checked( '1', $ranking_list_show_category ); ?> /></li>
    </ul>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->

  <?php // タブ ----------------------------------------- ?>
  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php _e('Tab setting', 'tcd-w'); ?></h3>
   <div class="theme_option_field_ac_content">
    <ul class="option_list">
     <li class="cf"><span class="label"><?php printf(__('%s tab label', 'tcd-w'), __('Daily', 'tcd-w')); ?></span><input class="full_width" type="text" name="ranking_tab_label1" value="<?php echo esc_attr( $ranking_tab_label1 ); ?>"></li>
     <li class="cf"><span class="label"><?php printf(__('%s tab label', 'tcd-w'), __('Weekly', 'tcd-w')); ?></span><input class="full_width" type="text" name="ranking_tab_label2" value="<?php echo esc_attr( $ranking_tab_label2 ); ?>"></li>
     <li class="cf"><span class="label"><?php printf(__('%s tab label', 'tcd-w'), __('Monthly', 'tcd-w')); ?></span><input class="full_width" type="text" name="ranking_tab_label3" value="<?php echo esc_attr( $ranking_tab_label3 ); ?>"></li>
     <li class="cf"><span class="label"><?php printf(__('%s tab label', 'tcd-w'), __('Yearly', 'tcd-w')); ?></span><input class="full_width" type="text" name="ranking_tab_label4" value="<?php echo esc_attr( $ranking_tab_label4 ); ?>"></li>
    </ul>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->

</div><!-- END .tcd_custom_field_wrap -->

<?php
}

function save_ranking_meta_box( $post_id ) {

  // verify nonce
  if (!isset($_POST['ranking_meta_box_nonce']) || !wp_verify_nonce($_POST['ranking_meta_box_nonce'], basename(__FILE__))) {
    return $post_id;
  }

  // check autosave
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  // check permissions
  if ('page' == $_POST['post_type']) {
    if (!current_user_can('edit_page', $post_id)) {
      return $post_id;
    }
  } elseif (!current_user_can('edit_post', $post_id)) {
      return $post_id;
  }

  // save or delete
  $cf_keys = array(
    'ranking_headline','ranking_headline_font_size','ranking_headline_font_size_mobile','ranking_headline_font_color','ranking_headline_border_color','ranking_headline_bg_color','ranking_headline_icon_color','ranking_headline_icon_image','ranking_headline_hide_icon',
    'ranking_desc','ranking_desc_font_size','ranking_desc_font_size_mobile',
    'ranking_list_num','ranking_list_title_font_size','ranking_list_title_font_size_mobile','ranking_list_show_premium_icon','ranking_list_show_category','ranking_list_color',
    'ranking_tab_label1', 'ranking_tab_label2', 'ranking_tab_label3', 'ranking_tab_label4'
  );
  foreach ($cf_keys as $cf_key) {
    $old = get_post_meta($post_id, $cf_key, true);

    if (isset($_POST[$cf_key])) {
      $new = $_POST[$cf_key];
    } else {
      $new = '';
    }

    if ($new && $new != $old) {
      update_post_meta($post_id, $cf_key, $new);
    } elseif ('' == $new && $old) {
      delete_post_meta($post_id, $cf_key, $old);
    }
  }


}
add_action('save_post', 'save_ranking_meta_box');




?>
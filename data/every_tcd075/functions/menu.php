<?php
/**
 * Add data-megamenu attributes to the global navigation
 */
function nano_walker_nav_menu_start_el( $item_output, $item, $depth, $args ) {

  $options = get_design_plus_option();

  if ( 'global-menu' !== $args->theme_location ) return $item_output;

  if ( ! isset( $options['megamenu'][$item->ID] ) ) return $item_output;

  if ( 'type1' === $options['megamenu'][$item->ID] ) return $item_output;

  return sprintf( '<a href="%s" class="megamenu_button" data-megamenu="js-megamenu%d">%s</a>', $item->url, $item->ID, $item->title );
}

add_filter( 'walker_nav_menu_start_el', 'nano_walker_nav_menu_start_el', 10, 4 );


// Mega menu A - Recipe category list ---------------------------------------------------------------
function render_megamenu_a( $id, $megamenus ) {
  global $post;
  $options = get_design_plus_option();
?>
<div class="megamenu_recipe_category_list" id="js-megamenu<?php echo esc_attr( $id ); ?>">
 <div class="megamenu_recipe_category_list_inner clearfix">

  <div class="item">
   <h2 class="headline"><?php echo esc_html($options['mega_menu_a_headline_label']); ?></h2>
   <a class="archive_link" href="<?php echo esc_url(get_post_type_archive_link('recipe')); ?>">TOP</a>
  </div>

  <?php
       $i = 1;
       foreach ( $megamenus[$id] as $menu ) :
         if($i == 6) { break; };
         if ( 'recipe_category' !== $menu->object ) continue;
         $cat_id = $menu->object_id;
         $cat_name = $menu->title;
         $url = $menu->url;
         $custom_fields = get_option( 'taxonomy_' . $cat_id, array() );
         if (!empty($custom_fields['image'])){
           $image = wp_get_attachment_image_src( $custom_fields['image'], 'size2' );
         } elseif($options['no_image2']) {
           $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
         } else {
           $image = array();
           $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
         }
  ?>
  <div class="item">
   <h3 class="headline"><a href="<?php echo esc_url($url); ?>"><?php echo esc_html($cat_name); ?></a></h3>
   <?php if( !empty($custom_fields['image']) ){ ?>
   <a class="link animate_background" style="background:none;" href="<?php echo esc_url($url); ?>">
    <div class="image_wrap">
     <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
    </div>
   </a>
   <?php }; ?>
   <?php
        $recipe_category = get_terms( 'recipe_category', array( 'hide_empty' => true, 'orderby' => 'id', 'parent' => $cat_id ) );
        if ( $recipe_category && ! is_wp_error( $recipe_category ) ) :
   ?>
   <ol class="child_category_list">
    <?php
         foreach ( $recipe_category as $child_cat ):
           $child_cat_id = $child_cat->term_id;
    ?>
    <li><a href="<?php echo esc_url(get_term_link($child_cat_id,'recipe_category')); ?>"><?php echo esc_html($child_cat->name); ?></a></li>
    <?php endforeach; ?>
   </ol>
   <?php endif; ?>
  </div><!-- END .item -->
  <?php $i++; endforeach; ?>

 </div>
</div>
<?php
}

// Mega menu B - Blog list ---------------------------------------------------------------
function render_megamenu_b( $id, $megamenus ) {
  global $post;
  $options = get_design_plus_option();
?>
<div class="megamenu_blog_list" id="js-megamenu<?php echo esc_attr( $id ); ?>">
 <div class="megamenu_blog_list_inner clearfix">
  <ul class="menu_area">
   <?php
        $i = 1;
        foreach ( $megamenus[$id] as $menu ) :
          if ( 'category' !== $menu->object ) continue;
          $cat_id = $menu->object_id;
          $cat_name = $menu->title;
          $url = $menu->url;
   ?>
   <li<?php if($i == 1) { echo ' class="active"'; }; ?>><a class="cat_id<?php echo esc_attr($cat_id); ?>" href="<?php echo esc_url($url); ?>"><?php echo esc_html($cat_name); ?></a></li>
   <?php $i++; endforeach; ?>
  </ul>
  <div class="post_list_area">
   <?php
       foreach ( $megamenus[$id] as $menu ) :
         if ( 'category' !== $menu->object ) continue;
         $cat_id = $menu->object_id;
           $args = array( 'post_type' => 'post', 'posts_per_page' => 9, 'tax_query' => array( array( 'taxonomy' => 'category', 'field' => 'term_id', 'terms' => $cat_id ) ) );
           $post_list = new wp_query($args);
           if($post_list->have_posts()):
   ?>
   <ol class="post_list clearfix cat_id<?php echo esc_attr($cat_id); ?>">
    <?php
         while( $post_list->have_posts() ) : $post_list->the_post();
           $premium_post = get_post_meta($post->ID,'premium_post',true);
           if($options['all_premium_post']) {
             $premium_post = '1';
           }
           if(has_post_thumbnail()) {
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
           } elseif($options['no_image1']) {
             $image = wp_get_attachment_image_src( $options['no_image1'], 'full' );
           } else {
             $image = array();
             $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image1.gif";
           }
    ?>
    <li>
     <a class="clearfix animate_background<?php if(!is_user_logged_in() && $premium_post) { echo ' register_link'; }; ?>" href="<?php if(!is_user_logged_in() && $premium_post) { echo '#'; } else { the_permalink(); }; ?>">
      <div class="image_wrap">
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
      <div class="title_area">
       <h4 class="title"><span><?php the_title_attribute(); ?></span></h4>
      </div>
     </a>
    </li>
    <?php endwhile; wp_reset_query(); ?>
   </ol>
   <?php endif; // END end post list ?>
   <?php endforeach; ?>
  </div><!-- END post_list_area -->
 </div>
</div>
<?php
}
?>
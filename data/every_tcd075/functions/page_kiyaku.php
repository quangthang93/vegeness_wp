<?php

function kiyaku_meta_box() {
  add_meta_box(
    'kiyaku_meta_box',//ID of meta box
    __('Privacy policy page setting', 'tcd-w'),
    'show_kiyaku_meta_box',//callback function
    'page',// post type
    'normal',// context
    'high'// priority
  );
}
add_action('add_meta_boxes', 'kiyaku_meta_box');

function show_kiyaku_meta_box() {
  global $post, $font_type_options;

  $options = get_design_plus_option();

  $kiyaku_page_headline_font_size = get_post_meta($post->ID, 'kiyaku_page_headline_font_size', true);
  if(empty($kiyaku_page_headline_font_size)){
    $kiyaku_page_headline_font_size = '20';
  }
  $kiyaku_page_headline_font_size_mobile = get_post_meta($post->ID, 'kiyaku_page_headline_font_size_mobile', true);
  if(empty($kiyaku_page_headline_font_size_mobile)){
    $kiyaku_page_headline_font_size_mobile = '15';
  }
  $kiyaku_page_headline_font_color = get_post_meta($post->ID, 'kiyaku_page_headline_font_color', true);
  if(empty($kiyaku_page_headline_font_color)){
    $kiyaku_page_headline_font_color = '#000000';
  }
  $kiyaku_page_headline_border_color = get_post_meta($post->ID, 'kiyaku_page_headline_border_color', true);
  if(empty($kiyaku_page_headline_border_color)){
    $kiyaku_page_headline_border_color = '#dddddd';
  }
  $kiyaku_page_headline_bg_color = get_post_meta($post->ID, 'kiyaku_page_headline_bg_color', true);
  if(empty($kiyaku_page_headline_bg_color)){
    $kiyaku_page_headline_bg_color = '#ffffff';
  }

  $kiyaku_page_headline_hide_icon = get_post_meta($post->ID, 'kiyaku_page_headline_hide_icon', true);
  $kiyaku_page_headline_icon_color = get_post_meta($post->ID, 'kiyaku_page_headline_icon_color', true);
  if(empty($kiyaku_page_headline_icon_color)){
    $kiyaku_page_headline_icon_color = '#000000';
  }

  $kiyaku_desc = get_post_meta($post->ID, 'kiyaku_desc', true);
  $kiyaku_list_headline_color = get_post_meta($post->ID, 'kiyaku_list_headline_color', true);
  if(empty($kiyaku_list_headline_color)){
    $kiyaku_list_headline_color = '#ff7f00';
  }
  $kiyaku_list_headline_font_size = get_post_meta($post->ID, 'kiyaku_list_headline_font_size', true);
  if(empty($kiyaku_list_headline_font_size)){
    $kiyaku_list_headline_font_size = '18';
  }
  $kiyaku_list_headline_font_size_mobile = get_post_meta($post->ID, 'kiyaku_list_headline_font_size_mobile', true);
  if(empty($kiyaku_list_headline_font_size_mobile)){
    $kiyaku_list_headline_font_size_mobile = '14';
  }
  $kiyaku_list_desc_font_size = get_post_meta($post->ID, 'kiyaku_list_desc_font_size', true);
  if(empty($kiyaku_list_desc_font_size)){
    $kiyaku_list_desc_font_size = '16';
  }
  $kiyaku_list_desc_font_size_mobile = get_post_meta($post->ID, 'kiyaku_list_desc_font_size_mobile', true);
  if(empty($kiyaku_list_desc_font_size_mobile)){
    $kiyaku_list_desc_font_size_mobile = '14';
  }
  $kiyaku_data_list = get_post_meta($post->ID, 'kiyaku_data_list', true);

  echo '<input type="hidden" name="kiyaku_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

  //入力欄 ***************************************************************************************************************************************************************************************
?>

<?php
     // WP5.0対策として隠しフィールドを用意　選択されているページテンプレートによってABOUT入力欄を表示・非表示する
     if ( count( get_page_templates( $post ) ) > 0 && get_option( 'page_for_posts' ) != $post->ID ) :
       $template = ! empty( $post->page_template ) ? $post->page_template : false;
?>
<select name="hidden_page_template" id="hidden_page_template" style="display:none;">
 <option value="default">Default Template</option>
 <?php page_template_dropdown( $template, 'page' ); ?>
</select>
<?php endif; ?>

<div class="tcd_custom_field_wrap">

  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php _e( 'Headline setting', 'tcd-w' ); ?></h3>
   <div class="theme_option_field_ac_content">
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="kiyaku_page_headline_font_size" value="<?php esc_attr_e( $kiyaku_page_headline_font_size ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="kiyaku_page_headline_font_size_mobile" value="<?php esc_attr_e( $kiyaku_page_headline_font_size_mobile ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="kiyaku_page_headline_font_color" value="<?php echo esc_attr( $kiyaku_page_headline_font_color ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="kiyaku_page_headline_bg_color" value="<?php echo esc_attr( $kiyaku_page_headline_bg_color ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Border color', 'tcd-w'); ?></span><input type="text" name="kiyaku_page_headline_border_color" value="<?php echo esc_attr( $kiyaku_page_headline_border_color ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Hide icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="kiyaku_page_headline_hide_icon" value="1" <?php checked( $kiyaku_page_headline_hide_icon, 1 ); ?>></li>
    </ul>
    <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($kiyaku_page_headline_hide_icon) { echo ' display:none;'; }; ?>">
     <li class="cf"><span class="label"><?php _e('Icon background color', 'tcd-w'); ?></span><input type="text" name="kiyaku_page_headline_icon_color" value="<?php echo esc_attr( $kiyaku_page_headline_icon_color ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Icon image', 'tcd-w');  ?></span>
      <?php mlcf_media_form('kiyaku_page_headline_icon_image', __('Image', 'tcd-w') ); ?>
      <div class="theme_option_message2">
       <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
       <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
      </div>
     </li>
    </ul>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->

  <div class="theme_option_field cf theme_option_field_ac">
   <h3 class="theme_option_headline"><?php _e( 'Privacy policy list setting', 'tcd-w' ); ?></h3>
   <div class="theme_option_field_ac_content">
    <h4 class="theme_option_headline2"><?php _e('Description before privacy policy list', 'tcd-w');  ?></h4>
    <?php wp_editor( $kiyaku_desc, 'kiyaku_desc', array( 'textarea_name' => 'kiyaku_desc', 'textarea_rows' => 10 )); ?>
    <?php // データ一覧 ---------- ?>
    <h3 class="theme_option_headline2"><?php _e( 'Privacy policy list', 'tcd-w' ); ?></h3>
    <div class="theme_option_message2">
     <p><?php _e('You can change order by dragging each item.', 'tcd-w'); ?></p>
    </div>
    <?php //繰り返しフィールド ----- ?>
    <div class="repeater-wrapper">
     <div class="repeater sortable" data-delete-confirm="<?php _e( 'Delete?', 'tcd-w' ); ?>">
      <?php
           if ( $kiyaku_data_list ) :
             foreach ( $kiyaku_data_list as $key => $value ) :
      ?>
      <div class="sub_box repeater-item repeater-item-<?php echo $key; ?>">
       <h4 class="theme_option_subbox_headline"><?php _e( 'New item', 'tcd-w' ); ?></h4>
       <div class="sub_box_content">
        <h4 class="theme_option_headline2"><?php _e( 'Headline', 'tcd-w' ); ?></h4>
        <p><input class="repeater-label" type="text" name="kiyaku_data_list[<?php echo esc_attr( $key ); ?>][title]" value="<?php echo esc_attr( isset( $kiyaku_data_list[$key]['title'] ) ? $kiyaku_data_list[$key]['title'] : '' ); ?>" style="width:100%" /></p>
        <h4 class="theme_option_headline2"><?php _e( 'Description', 'tcd-w' ); ?></h4>
        <?php wp_editor( isset( $kiyaku_data_list[$key]['desc'] ) ? $kiyaku_data_list[$key]['desc'] : '', 'kiyaku_data_list_' . $key . '_desc', array ( 'textarea_name' => 'kiyaku_data_list[' . $key . '][desc]' ) ); ?>
        <p class="delete-row right-align"><a href="#" class="button button-secondary button-delete-row"><?php _e( 'Delete item', 'tcd-w' ); ?></a></p>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php
             endforeach;
           endif;
           $key = 'addindex';
           ob_start();
      ?>
      <div class="sub_box repeater-item repeater-item-<?php echo $key; ?>">
       <h4 class="theme_option_subbox_headline"><?php echo esc_html( ! empty( $kiyaku_data_list[$key]['title'] ) ? $kiyaku_data_list[$key]['title'] : __( 'New item', 'tcd-w' ) ); ?></h4>
       <div class="sub_box_content">
        <h4 class="theme_option_headline2"><?php _e( 'Headline', 'tcd-w' );  ?></h4>
        <p><input class="repeater-label" type="text" name="kiyaku_data_list[<?php echo esc_attr( $key ); ?>][title]" value="" style="width:100%" /></p>
        <h4 class="theme_option_headline2"><?php _e( 'Description', 'tcd-w' );  ?></h4>
        <textarea class="large-text" cols="50" rows="10" name="kiyaku_data_list[<?php echo esc_attr( $key ); ?>][desc]"></textarea>
        <?php wp_editor( isset( $kiyaku_data_list[$key]['desc'] ) ? $kiyaku_data_list[$key]['desc'] : '', 'kiyaku_data_list_' . $key . '_desc', array ( 'textarea_name' => 'kiyaku_data_list[' . $key . '][desc]' ) ); ?>
        <p class="delete-row right-align"><a href="#" class="button button-secondary button-delete-row"><?php _e( 'Delete item', 'tcd-w' ); ?></a></p>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php
           $clone = ob_get_clean();
      ?>
     </div><!-- END .repeater -->
     <a href="#" class="button button-secondary button-add-row" data-clone="<?php echo esc_attr( $clone ); ?>"><?php _e( 'Add item', 'tcd-w' ); ?></a>
    </div><!-- END .repeater-wrapper -->
    <?php // 繰り返しフィールドここまで ----- ?>
    <h3 class="theme_option_headline2"><?php _e( 'Privacy policy list other setting', 'tcd-w' ); ?></h3>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Font size of headline', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="kiyaku_list_headline_font_size" value="<?php esc_attr_e( $kiyaku_list_headline_font_size ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size of headline (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="kiyaku_list_headline_font_size_mobile" value="<?php esc_attr_e( $kiyaku_list_headline_font_size_mobile ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Color of headline', 'tcd-w'); ?></span><input type="text" name="kiyaku_list_headline_color" value="<?php echo esc_attr( $kiyaku_list_headline_color ); ?>" data-default-color="#ff7f00" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Font size of description', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="kiyaku_list_desc_font_size" value="<?php esc_attr_e( $kiyaku_list_desc_font_size ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size of description (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="kiyaku_list_desc_font_size_mobile" value="<?php esc_attr_e( $kiyaku_list_desc_font_size_mobile ); ?>" /><span>px</span></li>
    </ul>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div><!-- END .theme_option_field_ac_content -->
  </div><!-- END .theme_option_field -->

</div><!-- END .tcd_custom_field_wrap -->

<?php
}

function save_kiyaku_meta_box( $post_id ) {

  // verify nonce
  if (!isset($_POST['kiyaku_meta_box_nonce']) || !wp_verify_nonce($_POST['kiyaku_meta_box_nonce'], basename(__FILE__))) {
    return $post_id;
  }

  // check autosave
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  // check permissions
  if ('page' == $_POST['post_type']) {
    if (!current_user_can('edit_page', $post_id)) {
      return $post_id;
    }
  } elseif (!current_user_can('edit_post', $post_id)) {
      return $post_id;
  }

  // save or delete
  $cf_keys = array(
    'kiyaku_page_headline_font_size','kiyaku_page_headline_font_size_mobile','kiyaku_page_headline_font_color','kiyaku_page_headline_border_color','kiyaku_page_headline_bg_color','kiyaku_page_headline_icon_color','kiyaku_page_headline_icon_image','kiyaku_page_headline_hide_icon',
    'kiyaku_desc','kiyaku_list_headline_font_size','kiyaku_list_headline_font_size_mobile','kiyaku_list_headline_color','kiyaku_list_desc_font_size','kiyaku_list_desc_font_size_mobile'
  );
  foreach ($cf_keys as $cf_key) {
    $old = get_post_meta($post_id, $cf_key, true);

    if (isset($_POST[$cf_key])) {
      $new = $_POST[$cf_key];
    } else {
      $new = '';
    }

    if ($new && $new != $old) {
      update_post_meta($post_id, $cf_key, $new);
    } elseif ('' == $new && $old) {
      delete_post_meta($post_id, $cf_key, $old);
    }
  }

  // repeater save or delete
  $cf_keys = array( 'kiyaku_data_list');
  foreach ( $cf_keys as $cf_key ) {
    $old = get_post_meta( $post_id, $cf_key, true );

    if ( isset( $_POST[$cf_key] ) && is_array( $_POST[$cf_key] ) ) {
      $new = array_values( $_POST[$cf_key] );
    } else {
      $new = false;
    }

    if ( $new && $new != $old ) {
      update_post_meta( $post_id, $cf_key, $new );
    } elseif ( ! $new && $old ) {
      delete_post_meta( $post_id, $cf_key, $old );
    }
  }

}
add_action('save_post', 'save_kiyaku_meta_box');




?>

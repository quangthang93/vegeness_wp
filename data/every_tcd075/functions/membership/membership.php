<?php

// TCD Membership backend
get_template_part( 'functions/membership/backend' );

// TCD Membership frontend
get_template_part( 'functions/membership/frontend' );

// TCD Membership member news
get_template_part( 'functions/membership/member_news' );

// TCD Membership like
get_template_part( 'functions/membership/like' );

// TCD Membership memberpage
get_template_part( 'functions/membership/memberpage' );

// TCD Membership user
get_template_part( 'functions/membership/user' );

// TCD Membership user form
get_template_part( 'functions/membership/user_form' );

// TCD Membership notify
get_template_part( 'functions/membership/notify' );

// load options
global $dp_options;
if ( ! $dp_options ) $dp_options = get_design_plus_option();

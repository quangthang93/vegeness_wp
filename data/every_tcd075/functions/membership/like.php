<?php

/**
 * ajaxでのお気に入り追加削除
 */
function ajax_toggle_like() {
	global $dp_options;
	if ( ! $dp_options ) $dp_options = get_design_plus_option();

	$json = array(
		'result' => false
	);

	if ( ! isset( $_POST['post_id'] ) ) {
		$json['message'] = __( 'Invalid request.', 'tcd-w' );
	} elseif ( ! current_user_can( 'read' ) ) {
		$json['message'] = __( 'Require login.', 'tcd-w' );
	} else {
		$user_id = get_current_user_id();
		$post_id = (int) $_POST['post_id'];

		if ( 0 < $post_id ) {
			$target_post = get_post( $post_id );
		}
		if ( empty( $target_post->post_status ) ) {
			$json['message'] = __( 'Invalid request.', 'tcd-w' );
		} elseif ( 'publish' !== $target_post->post_status ) {
			$json['message'] = sprintf( __( 'Disable like in %s.', 'tcd-w' ), __( 'Not publish article', 'tcd-w' ) );
		} elseif ( $target_post->post_type !== 'recipe' ) {
			$json['message'] = sprintf( __( 'Disable like in %s.', 'tcd-w' ), $target_post->post_type );
		} else {

			// お気に入り済みの場合、お気に入り削除
			if ( is_liked( $post_id, $user_id ) ) {
				$result = remove_like( $post_id, $user_id );
				if ( true === $result ) {
					$json['result'] = 'removed';
					$json['like_label'] = esc_html( $dp_options['recipe_like_button_label'] );
					$json['like_message'] = __( 'Added to favorite list', 'tcd-w');
				} elseif ( is_string( $result ) ) {
					$json['message'] = $result;
				} else {
					$json['message'] = __( 'Remove like error: ', 'tcd-w' ) . __( 'Failed to save the database.', 'tcd-w' );
				}

			// お気に入りしていない場合、お気に入り追加
			} else {
				$result = add_like( $post_id, $user_id );
				if ( true === $result ) {
					$json['result'] = 'added';
					$json['like_label'] = esc_html( $dp_options['recipe_delete_like_button_label'] );
					$json['like_message'] = __( 'Deleted from favorite list', 'tcd-w');
				} elseif ( is_string( $result ) ) {
					$json['message'] = $result;
				} else {
					$json['message'] = __( 'Add like error: ', 'tcd-w' ) . __( 'Failed to save the database.', 'tcd-w' );
				}
			}
		}
	}

	// JSON出力
	wp_send_json( $json );
	exit;
}
add_action( 'wp_ajax_toggle_like', 'ajax_toggle_like' );

/**
 * ajaxでのお気に入り追加
 */
function ajax_add_like() {
	global $dp_options;
	if ( ! $dp_options ) $dp_options = get_design_plus_option();

	$json = array(
		'result' => false
	);

	if ( ! isset( $_POST['post_id'] ) ) {
		$json['message'] = __( 'Invalid request.', 'tcd-w' );
	} elseif ( ! current_user_can( 'read' ) ) {
		$json['message'] = __( 'Require login.', 'tcd-w' );
	} else {
		$user_id = get_current_user_id();
		$post_id = (int) $_POST['post_id'];

		if ( 0 < $post_id ) {
			$target_post = get_post( $post_id );
		}
		if ( empty( $target_post->post_status ) ) {
			$json['message'] = __( 'Invalid request.', 'tcd-w' );
		} elseif ( 'publish' !== $target_post->post_status ) {
			$json['message'] = sprintf( __( 'Disable like in %s.', 'tcd-w' ), __( 'Not publish article', 'tcd-w' ) );
		} elseif ( $target_post->post_type !== 'recipe' ) {
			$json['message'] = sprintf( __( 'Disable like in %s.', 'tcd-w' ), $target_post->post_type );
		} else {

			// お気に入り済みの場合
			if ( is_liked( $post_id, $user_id ) ) {
				$json['result'] = 'added';

			// お気に入りしていない場合、お気に入り追加
			} else {
				$result = add_like( $post_id, $user_id );
				if ( true === $result ) {
					$json['result'] = 'added';
				} elseif ( is_string( $result ) ) {
					$json['message'] = $result;
				} else {
					$json['message'] = __( 'Add like error: ', 'tcd-w' ) . __( 'Failed to save the database.', 'tcd-w' );
				}
			}
		}
	}

	// JSON出力
	wp_send_json( $json );
	exit;
}
add_action( 'wp_ajax_add_like', 'ajax_add_like' );

/**
 * ajaxでのお気に入り削除
 */
function ajax_remove_like() {
	global $dp_options;
	if ( ! $dp_options ) $dp_options = get_design_plus_option();

	$json = array(
		'result' => false
	);

	if ( ! isset( $_POST['post_id'] ) ) {
		$json['message'] = __( 'Invalid request.', 'tcd-w' );
	} elseif ( ! current_user_can( 'read' ) ) {
		$json['message'] = __( 'Require login.', 'tcd-w' );
	} else {
		$user_id = get_current_user_id();
		$post_id = (int) $_POST['post_id'];

		if ( 0 < $post_id ) {
			$target_post = get_post( $post_id );
		}
		if ( empty( $target_post->post_status ) ) {
			$json['message'] = __( 'Invalid request.', 'tcd-w' );
		} elseif ( 'publish' !== $target_post->post_status ) {
			$json['message'] = sprintf( __( 'Disable like in %s.', 'tcd-w' ), __( 'Not publish article', 'tcd-w' ) );
		} elseif ( $target_post->post_type !== 'recipe' ) {
			$json['message'] = sprintf( __( 'Disable like in %s.', 'tcd-w' ), $target_post->post_type );
		} else {

			// お気に入り済みの場合、お気に入り削除
			if ( is_liked( $post_id, $user_id ) ) {
				$result = remove_like( $post_id, $user_id );
				if ( true === $result ) {
					$json['result'] = 'removed';
				} elseif ( is_string( $result ) ) {
					$json['message'] = $result;
				} else {
					$json['message'] = __( 'Remove like error: ', 'tcd-w' ) . __( 'Failed to save the database.', 'tcd-w' );
				}

			// お気に入りしていない場合
			} else {
				$recipe_label = $dp_options['recipe_label'] ? esc_html( $dp_options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );
				$json['message'] = sprintf( __( "You don't like this %s.", 'tcd-w' ), $recipe_label );
			}
		}
	}

	// JSON出力
	wp_send_json( $json );
	exit;
}
add_action( 'wp_ajax_remove_like', 'ajax_remove_like' );

/**
 * お気に入り追加
 */
function add_like( $post_id, $user_id = 0 ) {
	// お気に入り済みの場合
	if ( is_liked( $post_id, $user_id ) ) {
		return 0;
	}

	if ( ! $user_id ) {
		$user_id = get_current_user_id();
	}
	if ( ! $user_id ) {
		return null;
	}

	$post_id = (int) $post_id;
	if ( 0 >= $post_id ) {
		return null;
	}

	$target_post = get_post( $post_id );
	if ( empty( $target_post->post_status ) || 'publish' !== $target_post->post_status ) {
		return null;
	}

	// ユーザーメタからお気に入りデータ取得
	$user_likes = get_user_meta( $user_id, 'tcd_likes', true );
	if ( $user_likes ) {
		$user_likes = array_map( 'intval', explode( ',', $user_likes ) );

		// お気に入り配列キー取得
		$key = array_search( $post_id, $user_likes, true );

		// お気に入り済み場合
		if ( false !== $key ) {
			return false;
		}
	} else {
		$user_likes = array();
	}

	// 記事ID追加
	$user_likes[] = $post_id;
	$user_likes = array_unique( $user_likes );

	// ユーザーメタ更新
	if ( update_user_meta( $user_id, 'tcd_likes', implode( ',', $user_likes ) ) ) {
		// ユーザーメタ更新後、ポストメタのお気に入り数を1増やす
		$post_likes_number = intval( get_post_meta( $post_id, 'tcd_likes', true ) );

		if ( 0 > $post_likes_number)  {
			$post_likes_number = 0;
		}

		update_post_meta( $post_id, 'tcd_likes', $post_likes_number + 1 );

		return true;

	} else {
		return __( 'Add like error: ', 'tcd-w' ) . __( 'Failed to update user meta.', 'tcd-w' );
	}
}

/**
 * お気に入り削除
 */
function remove_like( $post_id, $user_id = 0 ) {
	if ( ! $user_id ) {
		$user_id = get_current_user_id();
	}
	if ( ! $user_id ) {
		return null;
	}

	$post_id = (int) $post_id;
	if ( 0 >= $post_id ) {
		return null;
	}

	$target_post = get_post( $post_id );
	if ( empty( $target_post->post_status ) || 'publish' !== $target_post->post_status ) {
		return null;
	}

	// ユーザーメタからお気に入りデータ取得
	$user_likes = get_user_meta( $user_id, 'tcd_likes', true );
	if ( $user_likes ) {
		$user_likes = array_map( 'intval', explode( ',', $user_likes ) );

		// お気に入り配列キー取得
		$key = array_search( $post_id, $user_likes, true );

		// お気に入り済み場合
		if ( false !== $key ) {
			unset( $user_likes[ $key ] );

			// ユーザーメタ更新
			if ( update_user_meta( $user_id, 'tcd_likes', implode( ',', $user_likes ) ) ) {
				// ユーザーメタ更新後、ポストメタのお気に入り数を1減らす
				$post_likes_number = intval( get_post_meta( $post_id, 'tcd_likes', true ) ) - 1;
				if ( 0 > $post_likes_number)  {
					$post_likes_number = 0;
				}

				update_post_meta( $post_id, 'tcd_likes', $post_likes_number );

				return true;

			} else {
				return __( 'Remove like error: ', 'tcd-w' ) . __( 'Failed to update user meta.', 'tcd-w' );
			}

		// お気に入りしていない場合
		} else {
			return false;
		}

	// お気に入りしていない場合
	} else {
		return false;
	}
}

/**
 * お気に入り済みかを判別
 */
function is_liked( $post_id = null, $user_id = 0 ) {
	if ( ! $user_id ) {
		$user_id = get_current_user_id();
	}
	if ( ! $user_id ) {
		return null;
	}

	if ( null === $post_id ) {
		$post_id = get_the_ID();
	}
	$post_id = (int) $post_id;
	if ( 0 >= $post_id ) {
		return null;
	}

	$target_post = get_post( $post_id );
	if ( empty( $target_post->post_status ) || 'publish' !== $target_post->post_status ) {
		return null;
	}

	$user_likes = get_user_meta( $user_id, 'tcd_likes', true );
	if ( $user_likes ) {
		$user_likes = array_map( 'intval', explode( ',', $user_likes ) );
		return in_array( $post_id, $user_likes, true );
	} else {
		return false;
	}
}

/**
 * ユーザーのお気に入り記事一覧配列を取得
 *
 * @param int    $user_id    ユーザーID（未指定時はログインユーザー）
 * @param string $query_args WP_Queryの引数指定
 * @param string $output     出力形式 [ ''（WP_Query以外） | WP_Query ]
 *
 * @return WP_Query|array
 */
function get_user_liked_posts( $user_id = 0, $query_args = array(), $output = 'WP_Query' ) {
	$query_args_defaults = array(
		'ignore_sticky_posts' => 1,
		'post_type' => 'recipe',
		'posts_per_page' => 9
	);
	$query_args = wp_parse_args( (array) $query_args, $query_args_defaults );

	$output = strtolower( $output );

	if ( ! $user_id ) {
		$user_id = get_current_user_id();
	}
	if ( ! $user_id ) {
		if ( 'wp_query' === $output ) {
			$_wp_query = new WP_Query();
			return $_wp_query;
		} else {
			return array();
		}
	}

	$user_likes = get_user_meta( $user_id, 'tcd_likes', true );
	$user_likes = array_map( 'intval', explode( ',', $user_likes ) );
	$user_likes = array_reverse( $user_likes );

	if ( ! $user_likes ) {
		if ( 'wp_query' === $output ) {
			$_wp_query = new WP_Query();
			return $_wp_query;
		} else {
			return array();
		}
	}

	// お気に入り追加した順の逆
	$query_args['post__in'] = $user_likes;
	$query_args['orderby'] = 'post__in';
	$_wp_query = new WP_Query( $query_args );

	if ( 'wp_query' === $output ) {
		return $_wp_query;
	} else {
		return (array) $_wp_query->posts;
	}
}

/**
 * 記事保存時にtcd_likesメタが空なら0をセット
 */
function save_post_likes_zero( $post_id, $post = null ) {
	global $dp_options;

	// check autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}

	if ( ! empty( $post->post_type ) && $post->post_type === 'recipe' && '' === get_post_meta( $post_id, 'tcd_likes', true ) ) {
		update_post_meta( $post_id, 'tcd_likes', 0 );
	}
}
add_action( 'save_post_recipe', 'save_post_likes_zero', 10, 2 );

/**
 * カスタムフィールドお気に入り数ランキング記事一覧取得関数
 *
 * @param string $query_args WP_Queryの引数指定
 * @param string $output     出力形式 [ ''（WP_Query以外） | WP_Query ]
 *
 * @return WP_Query|array
 */
function get_posts_likes_ranking( $query_args = array(), $output = null ) {
	$query_args_defaults = array(
		'ignore_sticky_posts' => 1,
	);
	$query_args = wp_parse_args( (array) $query_args, $query_args_defaults );

	$output = strtolower( $output );

	// カスタムフィールドアクセス数降順
	$query_args2 = $query_args;
	$query_args2['meta_key'] = 'tcd_likes';
	$query_args2['orderby'] = 'meta_value_num';
	$query_args2['order'] = 'DESC';
	if ( isset( $query_args2['meta_value'] ) ) {
		unset( $query_args2['meta_value'] );
	}

	$_wp_query = new WP_Query( $query_args2 );

	// ランダム対応
	if ( ! empty( $query_args['orderby'] ) && 'rand' === $query_args['orderby'] && $_wp_query->posts ) {
		mt_shuffle( $_wp_query->posts );
	}

	if ( 'wp_query' === $output ) {
		return $_wp_query;
	} else {
		return (array) $_wp_query->posts;
	}
}

/**
 * shuffle()の偏り対策
 */
if ( ! function_exists( 'mt_shuffle' ) ) :
	function mt_shuffle( array &$array ) {
		$array = array_values( $array );
		for ( $i = count( $array ) - 1; $i > 0; --$i ) {
			$j = mt_rand( 0, $i );
			if ( $i !== $j ) {
				list( $array[$i], $array[$j] ) = array( $array[$j], $array[$i] );
			}
		}
	}
endif;

<?php

/**
 * メンバーページ設定初期化
 */
function tcd_membership_memberpage_init() {
	global $dp_options, $tcd_membership_vars;
	if ( ! $dp_options ) $dp_options = get_design_plus_option();

	$tcd_membership_vars = array(
		'memberpage_page' => null,
		'memberpage_url' => null,
		'memberpage_type' => null,
		'memberpage_all_types' => array(
			'login',
			'logout',
			'reset_password',
			'mypage'
		),
		'memberpage_guest_types' => array(
			'login',
			'reset_password'
		)
	);

	if ( tcd_membership_users_can_register() ) {
		$tcd_membership_vars['memberpage_all_types'][] = 'registration';
		$tcd_membership_vars['memberpage_guest_types'][] = 'registration';
	}

	// メンバーページの固定ページ情報を取得
	if ( $dp_options['memberpage_page_id'] ) {
		if ( 'page' === get_option( 'show_on_front' ) && in_array( $dp_options['memberpage_page_id'], array( get_option( 'page_on_front' ), get_option( 'page_for_posts' ) ) ) ) {
			$dp_options['memberpage_page_id'] = null;
		}
	}
	if ( $dp_options['memberpage_page_id'] ) {
		$tcd_membership_vars['memberpage_page'] = get_post( $dp_options['memberpage_page_id'] );
		if ( empty( $tcd_membership_vars['memberpage_page']->post_status ) || 'publish' !== $tcd_membership_vars['memberpage_page']->post_status || 'page' !== $tcd_membership_vars['memberpage_page']->post_type ) {
			$tcd_membership_vars['memberpage_page'] = false;
		} else {
			$tcd_membership_vars['memberpage_url'] = get_permalink( $tcd_membership_vars['memberpage_page'] );
		}
	}
}
add_action( 'init', 'tcd_membership_memberpage_init', 8 );

/**
 * メンバーページの{page_slug}/{memberpage_type}形式リクエスト処理
 */
function tcd_membership_memberpage_parse_request( $wp ) {
	global $tcd_membership_vars;

	if ( is_admin() ) return;

	if ( ! empty( $wp->query_vars['pagename'] ) && $tcd_membership_vars['memberpage_page'] && get_option( 'permalink_structure' ) ) {
		// {page_slug}
		if ( $tcd_membership_vars['memberpage_page']->post_name === $wp->query_vars['pagename'] ) {
			$tcd_membership_vars['memberpage_type'] = 'mypage';

		// {page_slug}/{memberpage_type}/{ページ番号}
		} elseif ( preg_match( '!' . $tcd_membership_vars['memberpage_page']->post_name .'/([^/]+)/?!u', $wp->query_vars['pagename'], $matches ) ) {
			if ( in_array( $matches[1], $tcd_membership_vars['memberpage_all_types'] ) ) {

				$wp->query_vars['pagename'] = $tcd_membership_vars['memberpage_page']->post_name;
				$tcd_membership_vars['memberpage_type'] = $matches[1];
			}
		}
	}
}
add_action( 'parse_request', 'tcd_membership_memberpage_parse_request', 1 );

/**
 * メンバーページinit(wp)
 */
function tcd_membership_memberpage_wp() {
	global $tcd_membership_vars;

	if ( is_admin() ) return;

	// {page_slug}/{memberpage_type}以外のメンバーページリクエスト処理
	if ( ! $tcd_membership_vars['memberpage_type'] ) {
		// $_REQUEST['memberpage'] を受け取るフラグ
		$is_query_string_memberpage = false;

		if ( $tcd_membership_vars['memberpage_page'] && is_page( $tcd_membership_vars['memberpage_page']->ID ) ) {
			if ( ! empty( $_REQUEST['memberpage'] ) ) {
				$is_query_string_memberpage = true;
			} else {
				$tcd_membership_vars['memberpage_type'] = 'mypage';
			}
		} elseif ( ! $tcd_membership_vars['memberpage_page'] && is_front_page() ) {
			$is_query_string_memberpage = true;
		}

		if ( $is_query_string_memberpage ) {
			if ( ! empty( $_REQUEST['memberpage'] ) && in_array( $_REQUEST['memberpage'], $tcd_membership_vars['memberpage_all_types'] ) ) {
				$tcd_membership_vars['memberpage_type'] = $_REQUEST['memberpage'];
			}
		}
	}

	if ( $tcd_membership_vars['memberpage_type'] ) {
		// 未ログインの場合にアクセスできるmemberpage_typeでなければログインページへリダイレクト
		if ( ! current_user_can( 'read' ) && ! in_array( $tcd_membership_vars['memberpage_type'], $tcd_membership_vars['memberpage_guest_types'] ) ) {
			$current_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$redirect = add_query_arg( 'redirect_to', rawurlencode( rawurldecode( $current_url ) ), get_tcd_membership_memberpage_url( 'login' ) );
			wp_safe_redirect( $redirect );
			exit;
		}

		// action
		do_action( 'tcd_membership_action-' . $tcd_membership_vars['memberpage_type'] );
	}
}
add_action( 'wp', 'tcd_membership_memberpage_wp' );

/**
 * メンバーページのテンプレートファイルフィルター
 */
function tcd_membership_memberpage_template_include( $template ) {
	global $tcd_membership_vars;

	if ( !empty( $tcd_membership_vars['template'] ) ) {
		$template_name = $tcd_membership_vars['template'];
	} else {
		$template_name = $tcd_membership_vars['memberpage_type'];
	}

	if ( $template_name ) {
		$memberpage_template = locate_template( 'membership-template/' . $template_name . '.php' );
		if ( $memberpage_template ) {
			return $memberpage_template;
		} else {
			global $wp_query;
			$wp_query->is_404 = true;
			return get_404_template();
		}
	}

	return $template;
}
add_filter( 'template_include', 'tcd_membership_memberpage_template_include', 20 );

/**
 * メンバーページのwp_title用タイトルフィルター
 */
function tcd_membership_memberpage_wp_title_parts( $title_array ) {
	global $tcd_membership_vars;

	if ( ! is_admin() && $tcd_membership_vars['memberpage_type'] ) {
		if ( $_title = get_tcd_membership_memberpage_title( $tcd_membership_vars['memberpage_type'] ) ) {

			$title_array[] = $_title;
		}
	}

	return $title_array;
}
add_filter( 'wp_title_parts', 'tcd_membership_memberpage_wp_title_parts', 10 );

/**
 * メンバーページのタイトルを返す
 */
function get_tcd_membership_memberpage_title( $type = null ) {
	global $dp_options, $tcd_membership_vars;

	if ( ! $type ) {
		$type = get_tcd_membership_memberpage_type();
	}

	switch ( $type ) {
		case 'login' :
			$title = __( 'Login', 'tcd-w' );
			break;
		case 'logout' :
			$title = __( 'Logout', 'tcd-w' );
			break;
		case 'registration' :
			$title = $dp_options['register_headline'] ? $dp_options['register_headline'] : __( 'Member registration', 'tcd-w' );
			break;
		case 'reset_password' :
			$title = __( 'Reset Password', 'tcd-w' );
			break;
		case 'mypage' :
			if ( ! empty( $tcd_membership_vars['memberpage_page']->post_title ) ) {
				$title = $tcd_membership_vars['memberpage_page']->post_title;
			} else {
				$title = __( 'Mypage', 'tcd-w' );
			}
			break;
		default :
			$title = null;
			break;
	}

	return apply_filters( 'get_tcd_membership_memberpage_title', $title, $type );
}

/**
 * メンバーページのURLを返す
 */
function get_tcd_membership_memberpage_url( $type, $add_query_key = null, $add_query_value = null ) {
	global $tcd_membership_vars;

	if ( $tcd_membership_vars['memberpage_url'] ) {
		$baseurl = $tcd_membership_vars['memberpage_url'];
		if ( ! $type || 'mypage' === $type ) {
			return user_trailingslashit( $baseurl );
		}

		if ( get_option( 'permalink_structure' ) ) {
			$url = user_trailingslashit( untrailingslashit( $baseurl ) . '/' . $type );
		} else {
			$url = add_query_arg( 'memberpage', $type, $baseurl );
		}
	} else {
		$baseurl = home_url( '/' );
		if ( ! $type ) {
			return $baseurl;
		}

		$url = add_query_arg( 'memberpage', $type, $baseurl );
	}

	if ( $add_query_key && $add_query_value ) {
		$url = add_query_arg( $add_query_key, $add_query_value, $url );
	}

	return $url;
}

/**
 * 現ページのメンバーページ種別を返す
 */
function get_tcd_membership_memberpage_type() {
	global $tcd_membership_vars;
	return $tcd_membership_vars['memberpage_type'];
}

/**
 * body class
 */
function tcd_membership_body_classes( $classes ) {
	global $tcd_membership_vars;

	// メンバーページ表示の場合
	if ( $tcd_membership_vars['memberpage_type'] ) {
		$classes[] = 'membership-' . $tcd_membership_vars['memberpage_type'];

		// フロントページならhome削除
		if ( is_front_page() && false !== ( $key = array_search( 'home', $classes ) ) ) {
			unset( $classes[$key] );
		}
	}

	// ゲスト 未ログイン
	if ( ! current_user_can( 'read' ) ) {
		$classes[] = 'guest';
	}

	// マルチサイトの他サイトにログイン中でこのサイトのアクセス権がない場合はlogged-in削除
	if ( is_multisite() && is_user_logged_in() && ! current_user_can( 'read' ) && false !== ( $key = array_search( 'logged-in', $classes ) ) ) {
		unset( $classes[$key] );
	}

	return array_unique( $classes );
}
add_filter( 'body_class', 'tcd_membership_body_classes', 11 );

/**
 * 表示するサイドバーがあるか
 */
function tcd_membership_memberpage_has_sidebar() {
	$sidebar = 'page_widget';

	if ( is_mobile() ) {
		$sidebar .= '_mobile';
	}

	if ( is_active_sidebar( $sidebar ) ) {
		return true;
	} elseif ( is_active_sidebar( 'common_widget' ) ) {
		return true;
	}

	return false;
}

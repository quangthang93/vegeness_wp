<?php

function recipe_meta_box2() {
  $options = get_design_plus_option();
  $recipe_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );
  add_meta_box(
    'recipe_meta_box2',//ID of meta box
    sprintf(__('%s other setting', 'tcd-w'), $recipe_label),
    'show_recipe_meta_box2',//callback function
    'recipe',// post type
    'normal',// context
    'high'// priority
  );
}
add_action('add_meta_boxes', 'recipe_meta_box2');

function show_recipe_meta_box2() {

  global $post;
  $options = get_design_plus_option();
  $recipe_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );

  $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
  $featured_recipe1 = get_post_meta($post->ID,'featured_recipe1',true);
  $featured_recipe2 = get_post_meta($post->ID,'featured_recipe2',true);
  $featured_recipe3 = get_post_meta($post->ID,'featured_recipe3',true);
  $recommend_recipe = get_post_meta($post->ID,'recommend_recipe',true);

  echo '<input type="hidden" name="recipe_custom_fields_meta_box2_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

  //���͗� ***************************************************************************************************************************************************************************************
?>

<div class="tcd_custom_field_wrap">

 <div class="theme_option_field cf theme_option_field_ac">
  <h3 class="theme_option_headline"><?php _e('Post type setting', 'tcd-w'); ?></h3>
  <div class="theme_option_field_ac_content">
   <div class="theme_option_message2">
    <p><?php printf(__('Featured %s will be used in featured page template.', 'tcd-w'), $recipe_label); ?></p>
   </div>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php printf(__('Show this post as premium %s', 'tcd-w'), $recipe_label); ?></span><input type="checkbox" name="premium_recipe" value="1" <?php checked( '1', $premium_recipe ); ?>></li>
    <li class="cf"><span class="label"><?php printf(__('Show this post as featured %s%s', 'tcd-w'), $recipe_label, '1'); ?></span><input type="checkbox" name="featured_recipe1" value="1" <?php checked( '1', $featured_recipe1 ); ?>></li>
    <li class="cf"><span class="label"><?php printf(__('Show this post as featured %s%s', 'tcd-w'), $recipe_label, '2'); ?></span><input type="checkbox" name="featured_recipe2" value="1" <?php checked( '1', $featured_recipe2 ); ?>></li>
    <li class="cf"><span class="label"><?php printf(__('Show this post as featured %s%s', 'tcd-w'), $recipe_label, '3'); ?></span><input type="checkbox" name="featured_recipe3" value="1" <?php checked( '1', $featured_recipe3 ); ?>></li>
    <li class="cf"><span class="label"><?php printf(__('Show this post as recommend %s', 'tcd-w'), $recipe_label); ?></span><input type="checkbox" name="recommend_recipe" value="1" <?php checked( '1', $recommend_recipe ); ?>></li>
   </ul>
   <ul class="button_list cf">
    <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
   </ul>
  </div><!-- END .theme_option_field_ac_content -->
 </div><!-- END .theme_option_field -->

</div><!-- END .tcd_custom_field_wrap -->

<?php
}

function save_recipe_meta_box2( $post_id ) {

  // verify nonce
  if (!isset($_POST['recipe_custom_fields_meta_box2_nonce']) || !wp_verify_nonce($_POST['recipe_custom_fields_meta_box2_nonce'], basename(__FILE__))) {
    return $post_id;
  }

  // check autosave
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  // check permissions
  if ('page' == $_POST['post_type']) {
    if (!current_user_can('edit_page', $post_id)) {
      return $post_id;
    }
  } elseif (!current_user_can('edit_post', $post_id)) {
      return $post_id;
  }

  // save or delete
  $cf_keys = array(
    'premium_recipe','featured_recipe1','featured_recipe2','featured_recipe3','recommend_recipe'
  );
  foreach ($cf_keys as $cf_key) {
    $old = get_post_meta($post_id, $cf_key, true);

    if (isset($_POST[$cf_key])) {
      $new = $_POST[$cf_key];
    } else {
      $new = '';
    }

    if ($new && $new != $old) {
      update_post_meta($post_id, $cf_key, $new);
    } elseif ('' == $new && $old) {
      delete_post_meta($post_id, $cf_key, $old);
    }
  }

}
add_action('save_post', 'save_recipe_meta_box2');




?>

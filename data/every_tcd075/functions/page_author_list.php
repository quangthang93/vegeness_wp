<?php

function author_list_meta_box() {
  add_meta_box(
    'author_list_meta_box',//ID of meta box
    __('Author list page setting', 'tcd-w'),//label
    'show_author_list_meta_box',//callback function
    'page',// post type
    'normal',// context
    'high'// priority
  );
}
add_action('add_meta_boxes', 'author_list_meta_box');

function show_author_list_meta_box() {
  global $post;

  // ヘッダー
  $author_list_headline_font_size = get_post_meta($post->ID, 'author_list_headline_font_size', true);
  if(empty($author_list_headline_font_size)){
    $author_list_headline_font_size = '14';
  }
  $author_list_headline_font_size_mobile = get_post_meta($post->ID, 'author_list_headline_font_size_mobile', true);
  if(empty($author_list_headline_font_size_mobile)){
    $author_list_headline_font_size_mobile = '12';
  }
  $author_list_headline_color = get_post_meta($post->ID, 'author_list_headline_color', true);
  if(empty($author_list_headline_color)){
    $author_list_headline_color = '#ff7f00';
  };

  $author_list_desc = get_post_meta($post->ID, 'author_list_desc', true);
  $author_list_desc_font_size = get_post_meta($post->ID, 'author_list_desc_font_size', true);
  if(empty($author_list_desc_font_size)){
    $author_list_desc_font_size = '16';
  }
  $author_list_desc_font_size_mobile = get_post_meta($post->ID, 'author_list_desc_font_size_mobile', true);
  if(empty($author_list_desc_font_size_mobile)){
    $author_list_desc_font_size_mobile = '14';
  }
  $author_list_desc_color = get_post_meta($post->ID, 'author_list_desc_color', true);
  if(empty($author_list_desc_color)){
    $author_list_desc_color = '#FFFFFF';
  };

  $author_list_use_overlay = get_post_meta($post->ID, 'author_list_use_overlay', true);
  $author_list_overlay_color = get_post_meta($post->ID, 'author_list_overlay_color', true);
  if(empty($author_list_overlay_color)){
    $author_list_overlay_color = '#000000';
  };
  $author_list_overlay_opacity = get_post_meta($post->ID, 'author_list_overlay_opacity', true);
  if(empty($author_list_overlay_opacity)){
    $author_list_overlay_opacity = '0.5';
  };

  // 投稿者
  $author_list_title_font_size = get_post_meta($post->ID, 'author_list_title_font_size', true);
  if(empty($author_list_title_font_size)){
    $author_list_title_font_size = '20';
  }
  $author_list_title_font_size_mobile = get_post_meta($post->ID, 'author_list_title_font_size_mobile', true);
  if(empty($author_list_title_font_size_mobile)){
    $author_list_title_font_size_mobile = '16';
  }

  $author_list_sub_title_font_size = get_post_meta($post->ID, 'author_list_sub_title_font_size', true);
  if(empty($author_list_sub_title_font_size)){
    $author_list_sub_title_font_size = '12';
  }
  $author_list_sub_title_font_size_mobile = get_post_meta($post->ID, 'author_list_sub_title_font_size_mobile', true);
  if(empty($author_list_sub_title_font_size_mobile)){
    $author_list_sub_title_font_size_mobile = '11';
  }

  $author_list_author_desc_font_size = get_post_meta($post->ID, 'author_list_author_desc_font_size', true);
  if(empty($author_list_author_desc_font_size)){
    $author_list_author_desc_font_size = '16';
  }
  $author_list_author_desc_font_size_mobile = get_post_meta($post->ID, 'author_list_author_desc_font_size_mobile', true);
  if(empty($author_list_author_desc_font_size_mobile)){
    $author_list_author_desc_font_size_mobile = '14';
  }

  $author_list_show_category = get_post_meta($post->ID, 'author_list_show_category', true);

  $author_list_order = get_post_meta($post->ID, 'author_list_order', true);
  if (empty($author_list_order) || !is_array($author_list_order)) {
    $author_list_order = array();
  }

  $users = get_users(array(
    'fields' => array('ID'),
    'role__not_in' => array('subscriber','contributor'),
    'orderby' => 'ID',
    'order' => 'ASC'
  ));

  if ($users) {
    $user_ids = array();
    foreach ($users as $user) {
      $user_ids[] = $user->ID;
    }

    if ($author_list_order) {
      foreach ($author_list_order as $key => $author_id) {
        if (!in_array($author_id, $user_ids)) {
          unset($author_list_order[$key]);
        }
      }
    }

    foreach ($user_ids as $user_id) {
      if (!in_array($user_id, $author_list_order)) {
        $author_list_order[] = $user_id;
      }
    }

    unset($user_ids, $user_id);
  } else {
    $author_list_order = array();
  }
  unset($users);

  echo '<input type="hidden" name="author_list_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

  //入力欄 ***************************************************************************************************************************************************************************************
?>

<?php
     // WP5.0対策として隠しフィールドを用意　選択されているページテンプレートによってABOUT入力欄を表示・非表示する
     if ( count( get_page_templates( $post ) ) > 0 && get_option( 'page_for_posts' ) != $post->ID ) :
       $template = ! empty( $post->page_template ) ? $post->page_template : false;
?>
<select name="hidden_page_template" id="hidden_page_template" style="display:none;">
 <option value="default">Default Template</option>
 <?php page_template_dropdown( $template, 'page' ); ?>
</select>
<?php endif; ?>

<div class="tcd_custom_field_wrap">

 <div class="theme_option_field cf theme_option_field_ac">
  <h3 class="theme_option_headline"><?php _e('Header setting', 'tcd-w'); ?></h3>
  <div class="theme_option_field_ac_content">
   <h3 class="theme_option_headline2"><?php _e('Description', 'tcd-w'); ?></h3>
   <textarea class="large-text" cols="50" rows="2" name="author_list_desc"><?php echo esc_textarea($author_list_desc); ?></textarea>
   <h3 class="theme_option_headline2"><?php _e('Font size setting', 'tcd-w'); ?></h3>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="author_list_headline_font_size" value="<?php echo esc_attr($author_list_headline_font_size); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Description', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="author_list_desc_font_size" value="<?php echo esc_attr($author_list_desc_font_size); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Headline (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="author_list_headline_font_size_mobile" value="<?php echo esc_attr($author_list_headline_font_size_mobile); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Desctiption (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="author_list_desc_font_size_mobile" value="<?php echo esc_attr($author_list_desc_font_size_mobile); ?>" /><span>px</span></li>
   </ul>
   <h3 class="theme_option_headline2"><?php _e('Color setting', 'tcd-w'); ?></h3>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="author_list_headline_color" value="<?php echo esc_attr($author_list_headline_color); ?>" data-default-color="#ff7f00" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Font color of description', 'tcd-w'); ?></span><input type="text" name="author_list_desc_color" value="<?php echo esc_attr($author_list_desc_color); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
   </ul>
   <h3 class="theme_option_headline2"><?php _e('Background image', 'tcd-w'); ?></h3>
   <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '810', '456'); ?></p>
   <?php mlcf_media_form('author_list_image', __('Image', 'tcd-w') ); ?>
   <h3 class="theme_option_headline2"><?php _e('Overlay setting', 'tcd-w'); ?></h3>
   <p class="displayment_checkbox"><label><input name="author_list_use_overlay" type="checkbox" value="1" <?php checked( $author_list_use_overlay, 1 ); ?>><?php _e( 'Use overlay', 'tcd-w' ); ?></label></p>
   <div style="<?php if($author_list_use_overlay) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Color of overlay', 'tcd-w'); ?></span><input class="c-color-picker" type="text" name="author_list_overlay_color" value="<?php echo esc_attr( $author_list_overlay_color ); ?>" data-default-color="#000000"></li>
     <li class="cf">
      <span class="label"><?php _e('Transparency of overlay', 'tcd-w'); ?></span><input class="hankaku" style="width:70px;" type="number" max="1" min="0" step="0.1" name="author_list_overlay_opacity" value="<?php echo esc_attr( $author_list_overlay_opacity ); ?>" />
      <div class="theme_option_message2">
       <p><?php _e('Please specify the number of 0.1 from 0.9. Overlay color will be more transparent as the number is small.', 'tcd-w');  ?></p>
      </div>
     </li>
    </ul>
   </div>
   <ul class="button_list cf">
    <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
   </ul>
  </div><!-- END .theme_option_field_ac_content -->
 </div><!-- END .theme_option_field -->

 <div class="theme_option_field cf theme_option_field_ac">
  <h3 class="theme_option_headline"><?php _e('Author list setting', 'tcd-w'); ?></h3>
  <div class="theme_option_field_ac_content">
   <h3 class="theme_option_headline2"><?php _e('Font size setting', 'tcd-w'); ?></h3>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Name', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="author_list_title_font_size" value="<?php echo esc_attr($author_list_title_font_size); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Sub title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="author_list_sub_title_font_size" value="<?php echo esc_attr($author_list_sub_title_font_size); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Description', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="author_list_author_desc_font_size" value="<?php echo esc_attr($author_list_author_desc_font_size); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Name (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="author_list_title_font_size_mobile" value="<?php echo esc_attr($author_list_title_font_size_mobile); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Sub title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="author_list_sub_title_font_size_mobile" value="<?php echo esc_attr($author_list_sub_title_font_size_mobile); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Desctiption (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="author_list_author_desc_font_size_mobile" value="<?php echo esc_attr($author_list_author_desc_font_size_mobile); ?>" /><span>px</span></li>
   </ul>
   <h3 class="theme_option_headline2"><?php _e('Display setting', 'tcd-w'); ?></h3>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Display category', 'tcd-w'); ?></span><input name="author_list_show_category" type="checkbox" value="1" <?php checked( $author_list_show_category, 1 ); ?>></li>
   </ul>
   <ul class="button_list cf">
    <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
   </ul>
  </div><!-- END .theme_option_field_ac_content -->
 </div><!-- END .theme_option_field -->

 <div class="theme_option_message" style="margin-top:0;">
  <p><?php _e('You can change author order by dragging each headline of option field.', 'tcd-w');  ?></p>
 </div>

 <?php // 投稿者一覧の並び替え ----- ?>
 <div id="author_list_order">

  <?php
       foreach((array) $author_list_order as $author_id) :
           $user_data = get_userdata($author_id);
           $user_name = $user_data->display_name;
           $show_author_list = $user_data->show_author_list;
  ?>
  <div class="item"<?php if(empty($show_author_list)) { echo ' style="display:none;"'; }; ?>>
   <h3 class="name"><?php echo esc_html($user_name); ?></h3>
   <input type="hidden" name="author_list_order[]" value="<?php echo esc_attr($author_id); ?>" />
  </div>
  <?php endforeach; ?>

 </div><!-- END #author_list_order -->

</div><!-- END .tcd_custom_field_wrap -->

<?php
}

function save_author_list_meta_box( $post_id ) {

  // verify nonce
  if (!isset($_POST['author_list_meta_box_nonce']) || !wp_verify_nonce($_POST['author_list_meta_box_nonce'], basename(__FILE__))) {
    return $post_id;
  }

  // check autosave
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  // check permissions
  if ('page' == $_POST['post_type']) {
    if (!current_user_can('edit_page', $post_id)) {
      return $post_id;
    }
  } elseif (!current_user_can('edit_post', $post_id)) {
      return $post_id;
  }

  // save or delete
  $cf_keys = array(
    'author_list_headline_font_size','author_list_headline_font_size_mobile','author_list_headline_color',
    'author_list_desc','author_list_desc_font_size','author_list_desc_font_size_mobile','author_list_desc_color',
    'author_list_image','author_list_use_overlay','author_list_overlay_color','author_list_overlay_opacity',
    'author_list_title_font_size','author_list_title_font_size_mobile','author_list_sub_title_font_size','author_list_sub_title_font_size_mobile','author_list_author_desc_font_size','author_list_author_desc_font_size_mobile','author_list_show_category',
  );
  foreach ($cf_keys as $cf_key) {
    $old = get_post_meta($post_id, $cf_key, true);

    if (isset($_POST[$cf_key])) {
      $new = $_POST[$cf_key];
    } else {
      $new = '';
    }

    if ($new && $new != $old) {
      update_post_meta($post_id, $cf_key, $new);
    } elseif ('' == $new && $old) {
      delete_post_meta($post_id, $cf_key, $old);
    }
  }

  // repeater save or delete
  $cf_keys = array('author_list_order');
  foreach ( $cf_keys as $cf_key ) {
    $old = get_post_meta( $post_id, $cf_key, true );

    if ( isset( $_POST[$cf_key] ) && is_array( $_POST[$cf_key] ) ) {
      $new = array_values( $_POST[$cf_key] );
    } else {
      $new = false;
    }

    if ( $new && $new != $old ) {
      update_post_meta( $post_id, $cf_key, $new );
    } elseif ( ! $new && $old ) {
      delete_post_meta( $post_id, $cf_key, $old );
    }
  }

}
add_action('save_post', 'save_author_list_meta_box');




?>

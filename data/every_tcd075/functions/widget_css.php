<?php
     function widget_css() {

     // デザインされたブログ記事一覧 -------------------------------------------------------------
     $post_list_widget_data = get_option('widget_styled_post_list1_widget');
     if(!empty($post_list_widget_data)) {
       foreach($post_list_widget_data as $data => $value) :
         if(is_numeric($data)) {
           if(!empty($value['headline_font_color'])){
             $headline_font_color = $value['headline_font_color'];
           } else {
             $headline_font_color = '#000000';
           }
           if(!empty($value['headline_bg_color'])){
             $headline_bg_color = $value['headline_bg_color'];
           } else {
             $headline_bg_color = '#ffffff';
           }
           if(!empty($value['headline_border_color'])){
             $headline_border_color = $value['headline_border_color'];
           } else {
             $headline_border_color = '#dddddd';
           }
           if(!empty($value['hide_icon'])){
             $hide_icon = $value['hide_icon'];
           } else {
             $hide_icon = '';
           }
           if(!empty($value['icon_color'])){
             $icon_color = $value['icon_color'];
           } else {
             $icon_color = '#000000';
           }
           if(!empty($value['icon_image'])){
             $icon_image_id = $value['icon_image'];
             $icon_image = wp_get_attachment_image_src($icon_image_id, 'full');
             $icon_image = $icon_image[0];
           } else {
             $icon_image_id = '';
           }
?>
#styled_post_list1_widget-<?php echo esc_html($data); ?> .widget_headline { color:<?php echo esc_attr($headline_font_color); ?>; background:<?php echo esc_attr($headline_bg_color); ?>; border-color:<?php echo esc_attr($headline_border_color); ?>; }
<?php if($icon_image_id) { ?>
#styled_post_list1_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?> url(<?php echo esc_attr($icon_image); ?>) no-repeat center; }
<?php } else { ?>
#styled_post_list1_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?>; font-family:'headline_icon'; content:'\e90a'; font-size:37px; line-height:65px; }
@media screen and (max-width:650px) {
  #styled_post_list1_widget-<?php echo esc_html($data); ?> .widget_headline:before { font-size:32px; line-height:52px !important; }
}
<?php }; ?>
#styled_post_list1_widget-<?php echo esc_html($data); ?> .widget_headline:after { border-color:<?php echo esc_attr($icon_color); ?> transparent transparent transparent; }
<?php if($hide_icon) { ?>
#styled_post_list1_widget-<?php echo esc_html($data); ?> .widget_headline { padding-left:25px; }
#styled_post_list1_widget-<?php echo esc_html($data); ?> .widget_headline:before, #styled_post_list1_widget-<?php echo esc_html($data); ?> .widget_headline:after { display:none; }
<?php }; ?>
<?php
         };
       endforeach;
     };

     // デザインされたレシピ一覧 ----------------------------------------------------------------
     $recipe_list_widget_data = get_option('widget_recipe_post_list_widget');
     if(!empty($recipe_list_widget_data)) {
       foreach($recipe_list_widget_data as $data => $value) :
         if(is_numeric($data)) {
           if(!empty($value['headline_font_color'])){
             $headline_font_color = $value['headline_font_color'];
           } else {
             $headline_font_color = '#000000';
           }
           if(!empty($value['headline_bg_color'])){
             $headline_bg_color = $value['headline_bg_color'];
           } else {
             $headline_bg_color = '#ffffff';
           }
           if(!empty($value['headline_border_color'])){
             $headline_border_color = $value['headline_border_color'];
           } else {
             $headline_border_color = '#dddddd';
           }
           if(!empty($value['hide_icon'])){
             $hide_icon = $value['hide_icon'];
           } else {
             $hide_icon = '';
           }
           if(!empty($value['icon_color'])){
             $icon_color = $value['icon_color'];
           } else {
             $icon_color = '#000000';
           }
           if(!empty($value['icon_image'])){
             $icon_image_id = $value['icon_image'];
             $icon_image = wp_get_attachment_image_src($icon_image_id, 'full');
             $icon_image = $icon_image[0];
           } else {
             $icon_image_id = '';
           }
?>
#recipe_post_list_widget-<?php echo esc_html($data); ?> .widget_headline { color:<?php echo esc_attr($headline_font_color); ?>; background:<?php echo esc_attr($headline_bg_color); ?>; border-color:<?php echo esc_attr($headline_border_color); ?>; }
<?php if($icon_image_id) { ?>
#recipe_post_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?> url(<?php echo esc_attr($icon_image); ?>) no-repeat center; }
<?php } else { ?>
#recipe_post_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?>; font-family:'headline_icon'; content:'\e90a'; font-size:37px; line-height:65px; }
@media screen and (max-width:650px) {
  #recipe_post_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { font-size:32px; line-height:52px !important; }
}
<?php }; ?>
#recipe_post_list_widget-<?php echo esc_html($data); ?> .widget_headline:after { border-color:<?php echo esc_attr($icon_color); ?> transparent transparent transparent; }
<?php if($hide_icon) { ?>
#recipe_post_list_widget-<?php echo esc_html($data); ?> .widget_headline { padding-left:25px; }
#recipe_post_list_widget-<?php echo esc_html($data); ?> .widget_headline:before, #recipe_post_list_widget-<?php echo esc_html($data); ?> .widget_headline:after { display:none; }
<?php }; ?>
<?php
         };
       endforeach;
     };

     // プレミアムレシピ一覧 ----------------------------------------------------------------
     $premium_list_widget_data = get_option('widget_premium_recipe_list_widget');
     if(!empty($premium_list_widget_data)) {
       foreach($premium_list_widget_data as $data => $value) :
         if(is_numeric($data)) {
           if(!empty($value['headline_font_color'])){
             $headline_font_color = $value['headline_font_color'];
           } else {
             $headline_font_color = '#000000';
           }
           if(!empty($value['headline_bg_color'])){
             $headline_bg_color = $value['headline_bg_color'];
           } else {
             $headline_bg_color = '#ffffff';
           }
           if(!empty($value['headline_border_color'])){
             $headline_border_color = $value['headline_border_color'];
           } else {
             $headline_border_color = '#dddddd';
           }
           if(!empty($value['hide_icon'])){
             $hide_icon = $value['hide_icon'];
           } else {
             $hide_icon = '';
           }
           if(!empty($value['icon_color'])){
             $icon_color = $value['icon_color'];
           } else {
             $icon_color = '#bcab4a';
           }
           if(!empty($value['icon_image'])){
             $icon_image_id = $value['icon_image'];
             $icon_image = wp_get_attachment_image_src($icon_image_id, 'full');
             $icon_image = $icon_image[0];
           } else {
             $icon_image_id = '';
           }
?>
#premium_recipe_list_widget-<?php echo esc_html($data); ?> .widget_headline { color:<?php echo esc_attr($headline_font_color); ?>; background:<?php echo esc_attr($headline_bg_color); ?>; border-color:<?php echo esc_attr($headline_border_color); ?>; }
<?php if($icon_image_id) { ?>
#premium_recipe_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?> url(<?php echo esc_attr($icon_image); ?>) no-repeat center; }
<?php } else { ?>
<?php if (strtoupper(get_locale()) == 'JA') { ?>
#premium_recipe_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?>; font-family:'headline_icon'; content:'\e911'; font-size:24px; line-height:62px; }
@media screen and (max-width:650px) {
  #premium_recipe_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { font-size:21px; line-height:47px !important; }
}
<?php } else { ?>
#premium_recipe_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?>; font-family:'headline_icon'; content:'\e912'; font-size:7px; line-height:62px; }
@media screen and (max-width:650px) {
  #premium_recipe_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { font-size:5px; line-height:47px !important; }
}
<?php }; ?>
<?php }; ?>
#premium_recipe_list_widget-<?php echo esc_html($data); ?> .widget_headline:after { border-color:<?php echo esc_attr($icon_color); ?> transparent transparent transparent; }
<?php if($hide_icon) { ?>
#premium_recipe_list_widget-<?php echo esc_html($data); ?> .widget_headline { padding-left:25px; }
#premium_recipe_list_widget-<?php echo esc_html($data); ?> .widget_headline:before, #premium_recipe_list_widget-<?php echo esc_html($data); ?> .widget_headline:after { display:none; }
<?php }; ?>
<?php
         };
       endforeach;
     };

     // レシピカテゴリー ----------------------------------------------------------------
     $recipe_category_list_widget_data = get_option('widget_recipe_category_list_widget');
     if(!empty($recipe_category_list_widget_data)) {
       foreach($recipe_category_list_widget_data as $data => $value) :
         if(is_numeric($data)) {
           if(!empty($value['headline_font_color'])){
             $headline_font_color = $value['headline_font_color'];
           } else {
             $headline_font_color = '#000000';
           }
           if(!empty($value['headline_bg_color'])){
             $headline_bg_color = $value['headline_bg_color'];
           } else {
             $headline_bg_color = '#ffffff';
           }
           if(!empty($value['headline_border_color'])){
             $headline_border_color = $value['headline_border_color'];
           } else {
             $headline_border_color = '#dddddd';
           }
           if(!empty($value['hide_icon'])){
             $hide_icon = $value['hide_icon'];
           } else {
             $hide_icon = '';
           }
           if(!empty($value['icon_color'])){
             $icon_color = $value['icon_color'];
           } else {
             $icon_color = '#000000';
           }
           if(!empty($value['icon_image'])){
             $icon_image_id = $value['icon_image'];
             $icon_image = wp_get_attachment_image_src($icon_image_id, 'full');
             $icon_image = $icon_image[0];
           } else {
             $icon_image_id = '';
           }
?>
#recipe_category_list_widget-<?php echo esc_html($data); ?> .widget_headline { color:<?php echo esc_attr($headline_font_color); ?>; background:<?php echo esc_attr($headline_bg_color); ?>; border-color:<?php echo esc_attr($headline_border_color); ?>; }
<?php if($icon_image_id) { ?>
#recipe_category_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?> url(<?php echo esc_attr($icon_image); ?>) no-repeat center; }
<?php } else { ?>
#recipe_category_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?>; font-family:'headline_icon'; content:'\e907'; font-size:22px; line-height:62px; }
@media screen and (max-width:650px) {
  #recipe_category_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { font-size:17px; line-height:48px !important; }
}
<?php }; ?>
#recipe_category_list_widget-<?php echo esc_html($data); ?> .widget_headline:after { border-color:<?php echo esc_attr($icon_color); ?> transparent transparent transparent; }
<?php if($hide_icon) { ?>
#recipe_category_list_widget-<?php echo esc_html($data); ?> .widget_headline { padding-left:25px; }
#recipe_category_list_widget-<?php echo esc_html($data); ?> .widget_headline:before, #recipe_category_list_widget-<?php echo esc_html($data); ?> .widget_headline:after { display:none; }
<?php }; ?>
<?php
         };
       endforeach;
     };

     // タブ記事一覧 ----------------------------------------------------------------
     $tab_post_list_widget_data = get_option('widget_tab_post_list_widget');
     if(!empty($tab_post_list_widget_data)) {
       foreach($tab_post_list_widget_data as $data => $value) :
         if(is_numeric($data)) {
           if(!empty($value['headline_font_color'])){
             $headline_font_color = $value['headline_font_color'];
           } else {
             $headline_font_color = '#000000';
           }
           if(!empty($value['headline_bg_color'])){
             $headline_bg_color = $value['headline_bg_color'];
           } else {
             $headline_bg_color = '#ffffff';
           }
           if(!empty($value['headline_border_color'])){
             $headline_border_color = $value['headline_border_color'];
           } else {
             $headline_border_color = '#dddddd';
           }
           if(!empty($value['hide_icon'])){
             $hide_icon = $value['hide_icon'];
           } else {
             $hide_icon = '';
           }
           if(!empty($value['icon_color'])){
             $icon_color = $value['icon_color'];
           } else {
             $icon_color = '#000000';
           }
           if(!empty($value['icon_image'])){
             $icon_image_id = $value['icon_image'];
             $icon_image = wp_get_attachment_image_src($icon_image_id, 'full');
             $icon_image = $icon_image[0];
           } else {
             $icon_image_id = '';
           }
?>
#tab_post_list_widget-<?php echo esc_html($data); ?> .widget_headline { color:<?php echo esc_attr($headline_font_color); ?>; background:<?php echo esc_attr($headline_bg_color); ?>; border-color:<?php echo esc_attr($headline_border_color); ?>; }
<?php if($icon_image_id) { ?>
#tab_post_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?> url(<?php echo esc_attr($icon_image); ?>) no-repeat center; }
<?php } else { ?>
#tab_post_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?>; font-family:'headline_icon'; content:'\e90a'; font-size:37px; line-height:65px; }
@media screen and (max-width:650px) {
  #tab_post_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { font-size:32px; line-height:52px !important; }
}
<?php }; ?>
#tab_post_list_widget-<?php echo esc_html($data); ?> .widget_headline:after { border-color:<?php echo esc_attr($icon_color); ?> transparent transparent transparent; }
<?php if($hide_icon) { ?>
#tab_post_list_widget-<?php echo esc_html($data); ?> .widget_headline { padding-left:25px; }
#tab_post_list_widget-<?php echo esc_html($data); ?> .widget_headline:before, #tab_post_list_widget-<?php echo esc_html($data); ?> .widget_headline:after { display:none; }
<?php }; ?>
<?php
         };
       endforeach;
     };

     // レシピスライダー ----------------------------------------------------------------
     $recipe_slider_widget_data = get_option('widget_recipe_slider_widget');
     if(!empty($recipe_slider_widget_data)) {
       foreach($recipe_slider_widget_data as $data => $value) :
         if(is_numeric($data)) {
           if(!empty($value['headline_font_color'])){
             $headline_font_color = $value['headline_font_color'];
           } else {
             $headline_font_color = '#000000';
           }
           if(!empty($value['headline_bg_color'])){
             $headline_bg_color = $value['headline_bg_color'];
           } else {
             $headline_bg_color = '#ffffff';
           }
           if(!empty($value['headline_border_color'])){
             $headline_border_color = $value['headline_border_color'];
           } else {
             $headline_border_color = '#dddddd';
           }
           if(!empty($value['hide_icon'])){
             $hide_icon = $value['hide_icon'];
           } else {
             $hide_icon = '';
           }
           if(!empty($value['icon_color'])){
             $icon_color = $value['icon_color'];
           } else {
             $icon_color = '#bcab4a';
           }
           if(!empty($value['icon_image'])){
             $icon_image_id = $value['icon_image'];
             $icon_image = wp_get_attachment_image_src($icon_image_id, 'full');
             $icon_image = $icon_image[0];
           } else {
             $icon_image_id = '';
           }
?>
#recipe_slider_widget-<?php echo esc_html($data); ?> .widget_headline { color:<?php echo esc_attr($headline_font_color); ?>; background:<?php echo esc_attr($headline_bg_color); ?>; border-color:<?php echo esc_attr($headline_border_color); ?>; }
<?php if($icon_image_id) { ?>
#recipe_slider_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?> url(<?php echo esc_attr($icon_image); ?>) no-repeat center; }
<?php } else { ?>
<?php if (strtoupper(get_locale()) == 'JA') { ?>
#recipe_slider_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?>; font-family:'headline_icon'; content:'\e911'; font-size:24px; line-height:62px; }
@media screen and (max-width:650px) {
  #recipe_slider_widget-<?php echo esc_html($data); ?> .widget_headline:before { font-size:21px; line-height:47px !important; }
}
<?php } else { ?>
#recipe_slider_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?>; font-family:'headline_icon'; content:'\e912'; font-size:7px; line-height:62px; }
@media screen and (max-width:650px) {
  #recipe_slider_widget-<?php echo esc_html($data); ?> .widget_headline:before { font-size:5px; line-height:47px !important; }
}
<?php }; ?>
<?php }; ?>
#recipe_slider_widget-<?php echo esc_html($data); ?> .widget_headline:after { border-color:<?php echo esc_attr($icon_color); ?> transparent transparent transparent; }
<?php if($hide_icon) { ?>
#recipe_slider_widget-<?php echo esc_html($data); ?> .widget_headline { padding-left:25px; }
#recipe_slider_widget-<?php echo esc_html($data); ?> .widget_headline:before, #recipe_slider_widget-<?php echo esc_html($data); ?> .widget_headline:after { display:none; }
<?php }; ?>
<?php
         };
       endforeach;
     };

     // カテゴリー一覧 ----------------------------------------------------------------
     $tcd_category_list_widget_data = get_option('widget_tcd_category_list_widget');
     if(!empty($tcd_category_list_widget_data)) {
       foreach($tcd_category_list_widget_data as $data => $value) :
         if(is_numeric($data)) {
           if(!empty($value['headline_font_color'])){
             $headline_font_color = $value['headline_font_color'];
           } else {
             $headline_font_color = '#000000';
           }
           if(!empty($value['headline_bg_color'])){
             $headline_bg_color = $value['headline_bg_color'];
           } else {
             $headline_bg_color = '#ffffff';
           }
           if(!empty($value['headline_border_color'])){
             $headline_border_color = $value['headline_border_color'];
           } else {
             $headline_border_color = '#dddddd';
           }
           if(!empty($value['hide_icon'])){
             $hide_icon = $value['hide_icon'];
           } else {
             $hide_icon = '';
           }
           if(!empty($value['icon_color'])){
             $icon_color = $value['icon_color'];
           } else {
             $icon_color = '#000000';
           }
           if(!empty($value['icon_image'])){
             $icon_image_id = $value['icon_image'];
             $icon_image = wp_get_attachment_image_src($icon_image_id, 'full');
             $icon_image = $icon_image[0];
           } else {
             $icon_image_id = '';
           }
?>
#tcd_category_list_widget-<?php echo esc_html($data); ?> .widget_headline { color:<?php echo esc_attr($headline_font_color); ?>; background:<?php echo esc_attr($headline_bg_color); ?>; border-color:<?php echo esc_attr($headline_border_color); ?>; }
<?php if($icon_image_id) { ?>
#tcd_category_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?> url(<?php echo esc_attr($icon_image); ?>) no-repeat center; }
<?php } else { ?>
#tcd_category_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?>; font-family:'headline_icon'; content:'\e904'; font-size:23px; line-height:61px; }
@media screen and (max-width:650px) {
  #tcd_category_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { font-size:18px; line-height:47px !important; }
}
<?php }; ?>
#tcd_category_list_widget-<?php echo esc_html($data); ?> .widget_headline:after { border-color:<?php echo esc_attr($icon_color); ?> transparent transparent transparent; }
<?php if($hide_icon) { ?>
#tcd_category_list_widget-<?php echo esc_html($data); ?> .widget_headline { padding-left:25px; }
#tcd_category_list_widget-<?php echo esc_html($data); ?> .widget_headline:before, #tcd_category_list_widget-<?php echo esc_html($data); ?> .widget_headline:after { display:none; }
<?php }; ?>
<?php
         };
       endforeach;
     };

     // 人気の記事一覧 ----------------------------------------------------------------
     $ranking_list_widget_data = get_option('widget_ranking_list_widget');
     if(!empty($ranking_list_widget_data)) {
       foreach($ranking_list_widget_data as $data => $value) :
         if(is_numeric($data)) {
           if(!empty($value['headline_font_color'])){
             $headline_font_color = $value['headline_font_color'];
           } else {
             $headline_font_color = '#000000';
           }
           if(!empty($value['headline_bg_color'])){
             $headline_bg_color = $value['headline_bg_color'];
           } else {
             $headline_bg_color = '#ffffff';
           }
           if(!empty($value['headline_border_color'])){
             $headline_border_color = $value['headline_border_color'];
           } else {
             $headline_border_color = '#dddddd';
           }
           if(!empty($value['hide_icon'])){
             $hide_icon = $value['hide_icon'];
           } else {
             $hide_icon = '';
           }
           if(!empty($value['icon_color'])){
             $icon_color = $value['icon_color'];
           } else {
             $icon_color = '#000000';
           }
           if(!empty($value['icon_image'])){
             $icon_image_id = $value['icon_image'];
             $icon_image = wp_get_attachment_image_src($icon_image_id, 'full');
             $icon_image = $icon_image[0];
           } else {
             $icon_image_id = '';
           }
?>
#ranking_list_widget-<?php echo esc_html($data); ?> .widget_headline { color:<?php echo esc_attr($headline_font_color); ?>; background:<?php echo esc_attr($headline_bg_color); ?>; border-color:<?php echo esc_attr($headline_border_color); ?>; }
<?php if($icon_image_id) { ?>
#ranking_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?> url(<?php echo esc_attr($icon_image); ?>) no-repeat center; }
<?php } else { ?>
#ranking_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { background:<?php echo esc_attr($icon_color); ?>; font-family:'headline_icon'; content:'\e902'; font-size:24px; line-height:65px; }
@media screen and (max-width:650px) {
  #ranking_list_widget-<?php echo esc_html($data); ?> .widget_headline:before { font-size:18px; line-height:52px !important; }
}
<?php }; ?>
#ranking_list_widget-<?php echo esc_html($data); ?> .widget_headline:after { border-color:<?php echo esc_attr($icon_color); ?> transparent transparent transparent; }
<?php if($hide_icon) { ?>
#ranking_list_widget-<?php echo esc_html($data); ?> .widget_headline { padding-left:25px; }
#ranking_list_widget-<?php echo esc_html($data); ?> .widget_headline:before, #ranking_list_widget-<?php echo esc_html($data); ?> .widget_headline:after { display:none; }
<?php }; ?>
<?php
         };
       endforeach;
     };

?>

<?php
     } // end function
?>
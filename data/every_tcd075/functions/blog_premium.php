<?php
function blog_meta_box2() {
  add_meta_box(
    'blog_meta_box2',//ID of meta box
    __('Premium blog setting', 'tcd-w'),//label
    'show_blog_meta_box2',//callback function
    'post',// post type
    'normal',// context
    'high'// priority
  );
}
add_action('add_meta_boxes', 'blog_meta_box2');

function show_blog_meta_box2() {
  global $post;

  $premium_post = get_post_meta($post->ID,'premium_post',true);

  echo '<input type="hidden" name="blog_custom_fields_meta_box_nonce2" value="', wp_create_nonce(basename(__FILE__)), '" />';

  //���͗� ***************************************************************************************************************************************************************************************
?>
<div class="tcd_custom_fields">

  <div class="tcd_cf_content">
   <h3 class="tcd_cf_headline"><?php _e( 'Premium post settings', 'tcd-w' ); ?></h3>
   <p><?php _e('Check if you wan\'t to show this post for premium post.', 'tcd-w');  ?></p>
   <p><label><input type="checkbox" name="premium_post" value="1" <?php checked( '1', $premium_post ); ?> />  <?php _e('Show this post for premium post', 'tcd-w');  ?></label></p>
  </div><!-- END .content -->

</div><!-- END #tcd_custom_fields -->

<?php
}

function save_blog_meta_box2( $post_id ) {

  // verify nonce
  if (!isset($_POST['blog_custom_fields_meta_box_nonce2']) || !wp_verify_nonce($_POST['blog_custom_fields_meta_box_nonce2'], basename(__FILE__))) {
    return $post_id;
  }

  // check autosave
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  // check permissions
  if ('page' == $_POST['post_type']) {
    if (!current_user_can('edit_page', $post_id)) {
      return $post_id;
    }
  } elseif (!current_user_can('edit_post', $post_id)) {
      return $post_id;
  }

  // save or delete
  $cf_keys = array('premium_post');
  foreach ($cf_keys as $cf_key) {
    $old = get_post_meta($post_id, $cf_key, true);

    if (isset($_POST[$cf_key])) {
      $new = $_POST[$cf_key];
    } else {
      $new = '';
    }

    if ($new && $new != $old) {
      update_post_meta($post_id, $cf_key, $new);
    } elseif ('' == $new && $old) {
      delete_post_meta($post_id, $cf_key, $old);
    }
  }

}
add_action('save_post', 'save_blog_meta_box2');



?>
<?php

function recipe_meta_box() {
  $options = get_design_plus_option();
  $recipe_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );
  add_meta_box(
    'recipe_meta_box',//ID of meta box
    sprintf(__('%s basic setting', 'tcd-w'), $recipe_label),
    'show_recipe_meta_box',//callback function
    'recipe',// post type
    'normal',// context
    'high'// priority
  );
}
add_action('add_meta_boxes', 'recipe_meta_box');

function show_recipe_meta_box() {

  global $post;

  $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
  if(empty($recipe_type)){
    $recipe_type = 'type1';
  }

  // 動画
  $recipe_video = get_post_meta($post->ID, 'recipe_video', true);
  $recipe_time_label = get_post_meta($post->ID, 'recipe_time_label', true);
  if(empty($recipe_time_label)){
    $recipe_time_label = __('Cooking time', 'tcd-w');
  }
  $recipe_time = get_post_meta($post->ID, 'recipe_time', true);
  $recipe_price_label = get_post_meta($post->ID, 'recipe_price_label', true);
  if(empty($recipe_price_label)){
    $recipe_price_label = __('Estimated cost', 'tcd-w');
  }
  $recipe_price = get_post_meta($post->ID, 'recipe_price', true);

  // Youtube
  $recipe_youtube_url = get_post_meta($post->ID, 'recipe_youtube_url', true);

  // 画像
  $recipe_image_list = get_post_meta($post->ID, 'recipe_image_list', true);

  // 説明文
  $recipe_desc = get_post_meta($post->ID, 'recipe_desc', true);

  // 材料
  $show_material_content = get_post_meta($post->ID, 'show_material_content', true);

  // コンテンツビルダー
  $recipe_contents_builder = get_post_meta( $post->ID, 'recipe_contents_builder', true );

  echo '<input type="hidden" name="recipe_custom_fields_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

  //入力欄 ***************************************************************************************************************************************************************************************
?>

<div class="tcd_custom_field_wrap">

 <div class="theme_option_field cf theme_option_field_ac">
  <h3 class="theme_option_headline"><?php _e('Main content setting', 'tcd-w'); ?></h3>
  <div class="theme_option_field_ac_content">
   <h3 class="theme_option_headline2"><?php _e('Content type', 'tcd-w'); ?></h3>
   <ul class="design_radio_button">
    <li>
     <input type="radio" id="recipe_type_type1" name="recipe_type" value="type1" <?php checked( $recipe_type, 'type1' ); ?> />
     <label for="recipe_type_type1"><?php _e('Video', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="recipe_type_type3" name="recipe_type" value="type3" <?php checked( $recipe_type, 'type3' ); ?> />
     <label for="recipe_type_type3"><?php _e('Youtube', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="recipe_type_type2" name="recipe_type" value="type2" <?php checked( $recipe_type, 'type2' ); ?> />
     <label for="recipe_type_type2"><?php _e('Image', 'tcd-w'); ?></label>
    </li>
   </ul>
   <div id="recipe_video_area" style="display:<?php if($recipe_type == 'type1'){ echo 'block'; } else { echo 'none'; }; ?>;">
    <h3 class="theme_option_headline2"><?php _e('Video setting', 'tcd-w'); ?></h3>
    <div class="theme_option_message2">
     <p><?php _e('Please upload MP4 format file.', 'tcd-w');  ?></p>
    </div>
    <div class="image_box cf">
     <div class="cf cf_media_field hide-if-no-js recipe_video">
      <input type="hidden" value="<?php echo esc_attr($recipe_video); ?>" id="recipe_video" name="recipe_video" class="cf_media_id">
      <div class="preview_field preview_field_video">
       <?php if($recipe_video){ ?>
       <h4><?php _e( 'Uploaded MP4 file', 'tcd-w' ); ?></h4>
       <p><?php echo esc_url(wp_get_attachment_url($recipe_video)); ?></p>
       <?php }; ?>
      </div>
      <div class="buttton_area">
       <input type="button" value="<?php _e('Select MP4 file', 'tcd-w'); ?>" class="cfmf-select-video button">
       <input type="button" value="<?php _e('Remove MP4 file', 'tcd-w'); ?>" class="cfmf-delete-video button <?php if(!$recipe_video){ echo 'hidden'; }; ?>">
      </div>
     </div>
    </div>
   </div><!-- END #recipe_video_area -->
   <div id="recipe_image_area" style="display:<?php if($recipe_type == 'type2'){ echo 'block'; } else { echo 'none'; }; ?>;">
    <h3 class="theme_option_headline2"><?php _e('Image setting', 'tcd-w'); ?></h3>
    <?php //繰り返しフィールド ----- ?>
    <div class="repeater-wrapper">
     <div class="repeater sortable" data-delete-confirm="<?php _e( 'Delete?', 'tcd-w' ); ?>">
      <?php
           if ( $recipe_image_list ) :
             foreach ( $recipe_image_list as $key => $value ) :
      ?>
      <div class="sub_box repeater-item repeater-item-<?php echo $key; ?>">
       <h4 class="theme_option_subbox_headline"><?php echo sprintf( __( 'Image%s', 'tcd-w' ), $key+1 ); ?> <span></span></h4>
       <div class="sub_box_content">
        <h4 class="theme_option_headline2"><?php _e( 'Image', 'tcd-w' ); ?></h4>
        <div class="cf cf_media_field hide-if-no-js recipe_image_list-<?php echo esc_attr( $key ); ?>-image">
         <input type="hidden" class="cf_media_id" name="recipe_image_list[<?php echo esc_attr( $key ); ?>][image]" id="recipe_image_list-<?php echo esc_attr( $key ); ?>-image" value="<?php echo esc_attr( isset( $recipe_image_list[$key]['image'] ) ? $recipe_image_list[$key]['image'] : '' ); ?>" />
         <div class="preview_field"><?php if ( isset( $recipe_image_list[$key]['image'] ) ) echo wp_get_attachment_image( $recipe_image_list[$key]['image'], 'medium' ); ?></div>
         <div class="buttton_area">
          <input type="button" class="cfmf-select-img button" value="<?php _e( 'Select Image', 'tcd-w' ); ?>" />
          <input type="button" class="cfmf-delete-img button<?php if ( empty( $recipe_image_list[$key]['image'] ) ) echo ' hidden'; ?>" value="<?php _e( 'Remove Image', 'tcd-w' ); ?>" />
         </div>
        </div>
        <p class="delete-row right-align"><a href="#" class="button button-secondary button-delete-row"><?php _e( 'Delete item', 'tcd-w' ); ?></a></p>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php
             endforeach;
           endif;
           $key = 'addindex';
           ob_start();
      ?>
      <div class="sub_box repeater-item repeater-item-<?php echo $key; ?>">
       <h4 class="theme_option_subbox_headline"><?php _e( 'New image', 'tcd-w' ); ?></h4>
       <div class="sub_box_content">
        <h4 class="theme_option_headline2"><?php _e( 'Image', 'tcd-w' ); ?></h4>
        <div class="cf cf_media_field hide-if-no-js recipe_image_list-<?php echo esc_attr( $key ); ?>-image">
         <input type="hidden" class="cf_media_id" name="recipe_image_list[<?php echo esc_attr( $key ); ?>][image]" id="recipe_image_list-<?php echo esc_attr( $key ); ?>-image" value="" />
         <div class="preview_field"></div>
         <div class="buttton_area">
          <input type="button" class="cfmf-select-img button" value="<?php _e( 'Select Image', 'tcd-w' ); ?>" />
          <input type="button" class="cfmf-delete-img button hidden" value="<?php _e( 'Remove Image', 'tcd-w' ); ?>" />
         </div>
        </div>
        <p class="delete-row right-align"><a href="#" class="button button-secondary button-delete-row"><?php _e( 'Delete item', 'tcd-w' ); ?></a></p>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php
           $clone = ob_get_clean();
      ?>
     </div><!-- END .repeater -->
     <a href="#" class="button button-secondary button-add-row" data-clone="<?php echo esc_attr( $clone ); ?>"><?php _e( 'Add item', 'tcd-w' ); ?></a>
    </div><!-- END .repeater-wrapper -->
    <?php // 繰り返しフィールドここまで ----- ?>
   </div><!-- END #recipe_image_area -->
   <div id="recipe_youtube_url_area" style="display:<?php if($recipe_type == 'type3'){ echo 'block'; } else { echo 'none'; }; ?>;">
    <h3 class="theme_option_headline2"><?php _e('Youtube setting', 'tcd-w'); ?></h3>
    <div class="theme_option_message2">
     <p><?php _e('Please enter Youtube URL.', 'tcd-w');  ?></p>
    </div>
    <input class="full_width" type="text" name="recipe_youtube_url" value="<?php echo esc_attr($recipe_youtube_url); ?>" />
   </div><!-- END #recipe_youtube_url_area -->
   <h3 class="theme_option_headline2"><?php _e('First line data setting', 'tcd-w'); ?></h3>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w'); ?></span><input class="full_width" type="text" name="recipe_time_label" value="<?php echo esc_attr($recipe_time_label); ?>" /></li>
    <li class="cf"><span class="label"><?php _e('Contents', 'tcd-w'); ?></span><input class="full_width" type="text" name="recipe_time" value="<?php echo esc_attr($recipe_time); ?>" /></li>
   </ul>
   <h3 class="theme_option_headline2"><?php _e('Second line data setting', 'tcd-w'); ?></h3>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w'); ?></span><input class="full_width" type="text" name="recipe_price_label" value="<?php echo esc_attr($recipe_price_label); ?>" /></li>
    <li class="cf"><span class="label"><?php _e('Contents', 'tcd-w'); ?></span><input class="full_width" type="text" name="recipe_price" value="<?php echo esc_attr($recipe_price); ?>" /></li>
   </ul>
   <h3 class="theme_option_headline2"><?php _e('Description', 'tcd-w'); ?></h3>
   <?php wp_editor( $recipe_desc, 'recipe_desc', array( 'textarea_name' => 'recipe_desc', 'textarea_rows' => 10 )); ?>
   <ul class="button_list cf">
    <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
   </ul>
  </div><!-- END .theme_option_field_ac_content -->
 </div><!-- END .theme_option_field -->


 <?php
      // コンテンツビルダーはここから -----------------------------------------------------------------
 ?>
 <div class="theme_option_message" style="margin-top:0;">
  <?php _e( '<p>STEP1: Click Add content button.<br />STEP2: Select content from dropdown menu.<br />STEP3: Input data and save.</p><br /><p>You can change order by dragging MOVE button and you can delete content by clicking DELETE button.</p>', 'tcd-w' ); ?>
 </div>

 <div id="contents_builder_wrap">
  <div id="contents_builder">
   <p class="cb_message"><?php _e( 'Click Add content button to start content builder', 'tcd-w' ); ?></p>
<?php
if ( $recipe_contents_builder && is_array( $recipe_contents_builder ) ) :
	foreach( $recipe_contents_builder as $key => $content ) :
		$cb_index = 'cb_' . $key . '_' . mt_rand( 0, 999999 );
?>
   <div class="cb_row">
    <ul class="cb_button cf">
     <li><span class="cb_move"><?php _e( 'Move', 'tcd-w' ); ?></span></li>
     <li><span class="cb_delete"><?php _e( 'Delete', 'tcd-w' ); ?></span></li>
    </ul>
    <div class="cb_column_area cf">
     <div class="cb_column">
      <input type="hidden" class="cb_index" value="<?php echo $cb_index; ?>">
<?php
		the_recipe_cb_content_select( $cb_index, $content['content_select'] );
		if ( ! empty( $content['content_select'] ) ) :
			the_recipe_cb_content_setting( $cb_index, $content['content_select'], $content );
		endif;
?>
     </div><!-- END .cb_column -->
    </div><!-- END .cb_column_area -->
   </div><!-- END .cb_row -->
<?php
	endforeach;
endif;
?>
  </div><!-- END #contents_builder -->
  <ul class="button_list cf" id="cb_add_row_buttton_area">
   <li><input type="button" value="<?php _e( 'Add content', 'tcd-w' ); ?>" class="button-ml add_row"></li>
  </ul>
 </div><!-- END #contents_builder_wrap -->

 <?php // コンテンツビルダー追加用 非表示 ?>
 <div id="contents_builder-clone" class="hidden">
  <div class="cb_row">
   <ul class="cb_button cf">
    <li><span class="cb_move"><?php _e( 'Move', 'tcd-w' ); ?></span></li>
    <li><span class="cb_delete"><?php _e( 'Delete', 'tcd-w' ); ?></span></li>
   </ul>
   <div class="cb_column_area cf">
    <div class="cb_column">
     <input type="hidden" class="cb_index" value="cb_cloneindex">
       <?php the_recipe_cb_content_select( 'cb_cloneindex' ); ?>
    </div><!-- END .cb_column -->
   </div><!-- END .cb_column_area -->
  </div><!-- END .cb_row -->
<?php
foreach ( recipe_cb_get_contents() as $key => $value ) :
	the_recipe_cb_content_setting( 'cb_cloneindex', $key );
endforeach;
?>
 </div><!-- END #contents_builder-clone.hidden -->

</div><!-- END .tcd_custom_field_wrap -->
<?php
}

function save_recipe_meta_box( $post_id ) {

  // verify nonce
  if (!isset($_POST['recipe_custom_fields_meta_box_nonce']) || !wp_verify_nonce($_POST['recipe_custom_fields_meta_box_nonce'], basename(__FILE__))) {
    return $post_id;
  }

  // check autosave
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  // check permissions
  if ('page' == $_POST['post_type']) {
    if (!current_user_can('edit_page', $post_id)) {
      return $post_id;
    }
  } elseif (!current_user_can('edit_post', $post_id)) {
      return $post_id;
  }

  // save or delete
  $cf_keys = array('recipe_type','recipe_video','recipe_time_label','recipe_time','recipe_price_label','recipe_price','recipe_youtube_url','recipe_desc');
  foreach ($cf_keys as $cf_key) {
    $old = get_post_meta($post_id, $cf_key, true);

    if (isset($_POST[$cf_key])) {
      $new = $_POST[$cf_key];
    } else {
      $new = '';
    }

    if ($new && $new != $old) {
      update_post_meta($post_id, $cf_key, $new);
    } elseif ('' == $new && $old) {
      delete_post_meta($post_id, $cf_key, $old);
    }
  }

  // repeater save or delete
  $cf_keys = array( 'recipe_image_list' );
  foreach ( $cf_keys as $cf_key ) {
    $old = get_post_meta( $post_id, $cf_key, true );

    if ( isset( $_POST[$cf_key] ) && is_array( $_POST[$cf_key] ) ) {
      $new = array_values( $_POST[$cf_key] );
    } else {
      $new = false;
    }

    if ( $new && $new != $old ) {
      update_post_meta( $post_id, $cf_key, $new );
    } elseif ( ! $new && $old ) {
      delete_post_meta( $post_id, $cf_key, $old );
    }
  }

	// コンテンツビルダー 整形保存
	if ( ! empty( $_POST['recipe_contents_builder'] ) && is_array( $_POST['recipe_contents_builder'] ) ) {
		$cb_contents = recipe_cb_get_contents();
		$cb_data = array();

		foreach ( $_POST['recipe_contents_builder'] as $key => $value ) {
			// クローン用はスルー
			if ( 'cb_cloneindex' === $key ) continue;

			// コンテンツデフォルト値に入力値をマージ
			if ( ! empty( $value['content_select'] ) && isset( $cb_contents[$value['content_select']]['default'] ) ) {
				$value = array_merge( (array) $cb_contents[$value['content_select']]['default'], $value );
			}

			// 材料
			if ( 'recipe_material' === $value['content_select'] ) {
				$value['headline'] = sanitize_text_field( $value['headline'] );
				$value['headline_font_size'] = absint( $value['headline_font_size'] );
				$value['headline_font_size_mobile'] = absint( $value['headline_font_size_mobile'] );
				$value['headline_font_color'] = sanitize_hex_color( $value['headline_font_color'] );
				$value['headline_border_color'] = sanitize_hex_color( $value['headline_border_color'] );
				$value['headline_bg_color'] = sanitize_hex_color( $value['headline_bg_color'] );
				$value['headline_icon_color'] = sanitize_hex_color( $value['headline_icon_color'] );
				$value['icon_image'] = sanitize_text_field( $value['icon_image'] );
				$value['hide_icon'] = ! empty( $value['hide_icon'] ) ? 1 : 0;
				$material_list = array();
				if ( $value['material_list'] && is_array( $value['material_list'] ) ) {
					foreach( array_values( $value['material_list'] ) as $repeater_value ) {
						$material_list[] = array_merge( $cb_contents[$value['content_select']]['repeater_material_list_row_default'], $repeater_value );
					}
				}
				$value['material_list'] = $material_list;
				$value['list_label1'] = sanitize_text_field( $value['list_label1'] );
				$value['list_label2'] = sanitize_text_field( $value['list_label2'] );
				$value['list_bg_color'] = sanitize_hex_color( $value['list_bg_color'] );
				$value['list_font_color'] = sanitize_hex_color( $value['list_font_color'] );

			// 作り方
			} elseif ( 'recipe_howto' === $value['content_select'] ) {
				$value['headline'] = sanitize_text_field( $value['headline'] );
				$value['headline_font_size'] = absint( $value['headline_font_size'] );
				$value['headline_font_size_mobile'] = absint( $value['headline_font_size_mobile'] );
				$value['headline_font_color'] = sanitize_hex_color( $value['headline_font_color'] );
				$value['headline_border_color'] = sanitize_hex_color( $value['headline_border_color'] );
				$value['headline_bg_color'] = sanitize_hex_color( $value['headline_bg_color'] );
				$value['headline_icon_color'] = sanitize_hex_color( $value['headline_icon_color'] );
				$value['icon_image'] = sanitize_text_field( $value['icon_image'] );
				$value['hide_icon'] = ! empty( $value['hide_icon'] ) ? 1 : 0;
				$howto_list = array();
				if ( $value['howto_list'] && is_array( $value['howto_list'] ) ) {
					foreach( array_values( $value['howto_list'] ) as $repeater_value ) {
						$howto_list[] = array_merge( $cb_contents[$value['content_select']]['repeater_howto_list_row_default'], $repeater_value );
					}
				}
				$value['howto_list'] = $howto_list;
				$value['list_bg_color'] = sanitize_hex_color( $value['list_bg_color'] );
				$value['list_font_color'] = sanitize_hex_color( $value['list_font_color'] );

			// ポイント
			} elseif ( 'recipe_point' === $value['content_select'] ) {
				$value['headline'] = sanitize_text_field( $value['headline'] );
				$value['headline_font_size'] = absint( $value['headline_font_size'] );
				$value['headline_font_size_mobile'] = absint( $value['headline_font_size_mobile'] );
				$value['headline_font_color'] = sanitize_hex_color( $value['headline_font_color'] );
				$value['headline_border_color'] = sanitize_hex_color( $value['headline_border_color'] );
				$value['headline_bg_color'] = sanitize_hex_color( $value['headline_bg_color'] );
				$value['headline_icon_color'] = sanitize_hex_color( $value['headline_icon_color'] );
				$value['icon_image'] = sanitize_text_field( $value['icon_image'] );
				$value['hide_icon'] = ! empty( $value['hide_icon'] ) ? 1 : 0;
				$value['desc'] = $value['desc'];

			// フリースペース
			} elseif ( 'recipe_free' === $value['content_select'] ) {
				$value['desc'] = $value['desc'];


			}

			$cb_data[] = $value;
		}

		if ( $cb_data ) {
			update_post_meta( $post_id, 'recipe_contents_builder', $cb_data );
		} else {
			delete_post_meta( $post_id, 'recipe_contents_builder' );
		}
	}
}
add_action('save_post', 'save_recipe_meta_box');


/**
 * コンテンツビルダー コンテンツ一覧取得
 */
function recipe_cb_get_contents() {
	return array(
		'recipe_material' => array(
			'name' => 'recipe_material',
			'label' => __( 'Table content', 'tcd-w' ),
			'default' => array(
				'headline' => __( 'Table content', 'tcd-w' ),
				'headline_font_size' => 20,
				'headline_font_size_mobile' => 16,
				'headline_font_color' => '#000000',
				'headline_border_color' => '#dddddd',
				'headline_bg_color' => '#ffffff',
				'headline_icon_color' => '#000000',
				'hide_icon' => '',
				'icon_image' => false,
				'material_list' => array(),
				'list_label1' => __( 'Material', 'tcd-w' ),
				'list_label2' => __( 'Amount', 'tcd-w' ),
				'list_bg_color' => '#edf8fd',
				'list_font_color' => '#000000'
			),
			'repeater_material_list_row_default' => array(
				'name' => '',
				'amount' => ''
			)
		),
		'recipe_howto' => array(
			'name' => 'recipe_material',
			'label' => __( 'Procedure list', 'tcd-w' ),
			'default' => array(
				'headline' => __( 'Procedure list', 'tcd-w' ),
				'headline_font_size' => 20,
				'headline_font_size_mobile' => 16,
				'headline_font_color' => '#000000',
				'headline_border_color' => '#dddddd',
				'headline_bg_color' => '#ffffff',
				'headline_icon_color' => '#000000',
				'hide_icon' => '',
				'icon_image' => false,
				'howto_list' => array(),
				'list_bg_color' => '#edf8fd',
				'list_font_color' => '#000000'
			),
			'repeater_howto_list_row_default' => array(
				'content' => ''
			)
		),
		'recipe_point' => array(
			'name' => 'recipe_point',
			'label' => __( 'Point', 'tcd-w' ),
			'default' => array(
				'headline' => __( 'Point', 'tcd-w' ),
				'headline_font_size' => 20,
				'headline_font_size_mobile' => 16,
				'headline_font_color' => '#000000',
				'headline_border_color' => '#dddddd',
				'headline_bg_color' => '#ffffff',
				'headline_icon_color' => '#000000',
				'hide_icon' => '',
				'icon_image' => false,
				'desc' => ''
			),
		),
		'recipe_free' => array(
			'name' => 'recipe_free',
			'label' => __( 'Free space', 'tcd-w' ),
			'default' => array(
				'desc' => ''
			),
		)
	);
}

/**
 * コンテンツビルダー用 コンテンツ選択プルダウン
 */
function the_recipe_cb_content_select( $cb_index = 'cb_cloneindex', $selected = null ) {
	$cb_contents = recipe_cb_get_contents();

	if ( $selected && isset( $cb_contents[$selected] ) ) {
		$add_class = ' hidden';
	} else {
		$add_class = '';
	}

	$out = '<select name="recipe_contents_builder[' . esc_attr( $cb_index ) . '][content_select]" class="cb_content_select' . $add_class . '">';
	$out .= '<option value="">' . __( 'Choose the content', 'tcd-w' ) . '</option>';

	foreach ( $cb_contents as $key => $value ) {
		$out .= '<option value="' . esc_attr( $key ) . '"' . selected( $key, $selected, false ) . '>' . esc_html( $value['label'] ) . '</option>';
	}

	$out .= '</select>';

	echo $out;
}

/**
 * コンテンツビルダー用 コンテンツ設定
 */
function the_recipe_cb_content_setting( $cb_index = 'cb_cloneindex', $cb_content_select = null, $value = array() ) {

	$cb_contents = recipe_cb_get_contents();

	// 不明なコンテンツの場合は終了
	if ( ! $cb_content_select || ! isset( $cb_contents[$cb_content_select] ) ) return false;

	// コンテンツデフォルト値に入力値をマージ
	if ( isset( $cb_contents[$cb_content_select]['default'] ) ) {
		$value = array_merge( (array) $cb_contents[$cb_content_select]['default'], $value );
	}
?>
  <div class="cb_content_wrap cf <?php echo esc_attr( $cb_content_select ); ?>">

   <?php
       // 材料 -------------------------------------------------------------------------
       if ( 'recipe_material' === $cb_content_select ) :
   ?>
   <h3 class="cb_content_headline"><?php echo esc_html( $cb_contents[$cb_content_select]['label'] ); ?><span></span></h3>
   <div class="cb_content">

    <h4 class="theme_option_headline2"><?php _e( 'Headline setting', 'tcd-w' ); ?></h4>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w');  ?></span><input class="full_width change_content_headline" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline]" value="<?php echo esc_attr( $value['headline'] ); ?>" /></li>
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_font_size]" value="<?php echo esc_attr( $value['headline_font_size'] ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_font_size_mobile]" value="<?php echo esc_attr( $value['headline_font_size_mobile'] ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_font_color]" value="<?php echo esc_attr( $value['headline_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Border color', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_border_color]" value="<?php echo esc_attr( $value['headline_border_color'] ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_bg_color]" value="<?php echo esc_attr( $value['headline_bg_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Hide icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="recipe_contents_builder[<?php echo $cb_index; ?>][hide_icon]" value="1" <?php checked( $value['hide_icon'], 1 ); ?>></li>
    </ul>
    <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($value['hide_icon']) { echo ' display:none;'; }; ?>">
     <li class="cf"><span class="label"><?php _e('Icon background color', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_icon_color]" value="<?php echo esc_attr( $value['headline_icon_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Icon image', 'tcd-w');  ?></span>
      <div class="cf cf_media_field hide-if-no-js">
       <input type="hidden" class="cf_media_id" name="recipe_contents_builder[<?php echo $cb_index; ?>][icon_image]" id="recipe_contents_builder-<?php echo $cb_index; ?>-icon_image" value="<?php echo esc_attr( $value['icon_image'] ); ?>">
       <div class="preview_field"><?php if ( $value['icon_image'] ) echo wp_get_attachment_image( $value['icon_image'], 'medium' ); ?></div>
        <div class="buttton_area">
         <input type="button" class="cfmf-select-img button" value="<?php _e( 'Select Image', 'tcd-w' ); ?>">
         <input type="button" class="cfmf-delete-img button<?php if ( ! $value['icon_image'] ) echo ' hidden'; ?>" value="<?php _e( 'Remove Image', 'tcd-w'); ?>">
        </div>
      </div>
      <div class="theme_option_message2">
       <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
       <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
      </div>
     </li>
    </ul>
    <?php // 材料一覧 ---------- ?>
    <h3 class="theme_option_headline2"><?php _e( 'Table content', 'tcd-w' ); ?></h3>
    <div class="theme_option_message2">
     <p><?php _e('You can change order by dragging each item.', 'tcd-w'); ?></p>
    </div>
    <div class="repeater-wrapper">
     <div class="repeater sortable" data-delete-confirm="<?php _e( 'Delete?', 'tcd-w' ); ?>">
      <?php
           if ( $value['material_list'] && is_array( $value['material_list'] ) ) :
             foreach ( $value['material_list'] as $repeater_key => $repeater_value ) :
               $repeater_value = array_merge( $cb_contents[$cb_content_select]['repeater_material_list_row_default'], $repeater_value );
      ?>
      <div class="sub_box repeater-item repeater-item-<?php echo esc_attr( $repeater_key ); ?>">
       <h4 class="theme_option_subbox_headline"><?php _e( 'New item', 'tcd-w' ); ?></h4>
       <div class="sub_box_content">
        <h4 class="theme_option_headline2"><?php _e( 'Headline', 'tcd-w' ); ?></h4>
        <p><input class="repeater-label large-text" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][material_list][<?php echo esc_attr( $repeater_key ); ?>][name]" value="<?php echo esc_attr( isset( $repeater_value['name'] ) ? $repeater_value['name'] : '' ); ?>"></p>
        <h4 class="theme_option_headline2"><?php _e( 'Contents', 'tcd-w' ); ?></h4>
        <p><input class="large-text" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][material_list][<?php echo esc_attr( $repeater_key ); ?>][amount]" value="<?php echo esc_attr( isset( $repeater_value['amount'] ) ? $repeater_value['amount'] : '' ); ?>"></p>
        <p class="delete-row right-align"><a href="#" class="button button-secondary button-delete-row"><?php _e( 'Delete item', 'tcd-w' ); ?></a></p>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php
             endforeach;
           endif;

           $repeater_key = 'addindex';
           $repeater_value = $cb_contents[$cb_content_select]['repeater_material_list_row_default'];
           ob_start();
      ?>
      <div class="sub_box repeater-item repeater-item-<?php echo esc_attr( $repeater_key ); ?>">
       <h4 class="theme_option_subbox_headline"><?php _e( 'New item', 'tcd-w' ); ?></h4>
       <div class="sub_box_content">
        <h4 class="theme_option_headline2"><?php _e( 'Headline', 'tcd-w' ); ?></h4>
        <p><input class="repeater-label large-text" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][material_list][<?php echo esc_attr( $repeater_key ); ?>][name]" value="<?php echo esc_attr( isset( $repeater_value['name'] ) ? $repeater_value['name'] : '' ); ?>"></p>
        <h4 class="theme_option_headline2"><?php _e( 'Contents', 'tcd-w' ); ?></h4>
        <p><input class="large-text" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][material_list][<?php echo esc_attr( $repeater_key ); ?>][amount]" value="<?php echo esc_attr( isset( $repeater_value['amount'] ) ? $repeater_value['amount'] : '' ); ?>"></p>
        <p class="delete-row right-align"><a href="#" class="button button-secondary button-delete-row"><?php _e( 'Delete item', 'tcd-w' ); ?></a></p>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php
           $clone = ob_get_clean();
      ?>
     </div><!-- END .repeater -->
     <a href="#" class="button button-secondary button-add-row" data-clone="<?php echo esc_attr( $clone ); ?>"><?php _e( 'Add item', 'tcd-w' ); ?></a>
    </div><!-- END .repeater-wrapper -->
    <?php // 材料一覧ここまで ---------- ?>
    <h3 class="theme_option_headline2"><?php _e('Table content other setting', 'tcd-w'); ?></h3>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Label for headline', 'tcd-w'); ?></span><input class="full_width" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][list_label1]" value="<?php echo esc_attr( $value['list_label1'] ); ?>" /></li>
     <li class="cf"><span class="label"><?php _e('Label for contents', 'tcd-w'); ?></span><input class="full_width" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][list_label2]" value="<?php echo esc_attr( $value['list_label2'] ); ?>" /></li>
     <li class="cf color_picker_bottom"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][list_bg_color]" value="<?php echo esc_attr( $value['list_bg_color'] ); ?>" data-default-color="#edf8fd" class="c-color-picker"></p>
     <li class="cf color_picker_bottom"><span class="label"><?php _e('Font color of headline', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][list_font_color]" value="<?php echo esc_attr( $value['list_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></p>
    </ul>
    <ul class="button_list cf">
     <li><a href="#" class="button-ml close-content"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div>

   <?php
       // 作り方 -------------------------------------------------------------------------
       elseif ( 'recipe_howto' === $cb_content_select ) :
   ?>
   <h3 class="cb_content_headline"><?php echo esc_html( $cb_contents[$cb_content_select]['label'] ); ?><span></span></h3>
   <div class="cb_content">

    <h4 class="theme_option_headline2"><?php _e( 'Headline setting', 'tcd-w' ); ?></h4>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w');  ?></span><input class="full_width change_content_headline" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline]" value="<?php echo esc_attr( $value['headline'] ); ?>" /></li>
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_font_size]" value="<?php echo esc_attr( $value['headline_font_size'] ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_font_size_mobile]" value="<?php echo esc_attr( $value['headline_font_size_mobile'] ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_font_color]" value="<?php echo esc_attr( $value['headline_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Border color', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_border_color]" value="<?php echo esc_attr( $value['headline_border_color'] ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_bg_color]" value="<?php echo esc_attr( $value['headline_bg_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Hide icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="recipe_contents_builder[<?php echo $cb_index; ?>][hide_icon]" value="1" <?php checked( $value['hide_icon'], 1 ); ?>></li>
    </ul>
    <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($value['hide_icon']) { echo ' display:none;'; }; ?>">
     <li class="cf"><span class="label"><?php _e('Icon background color', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_icon_color]" value="<?php echo esc_attr( $value['headline_icon_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Icon image', 'tcd-w');  ?></span>
      <div class="cf cf_media_field hide-if-no-js">
       <input type="hidden" class="cf_media_id" name="recipe_contents_builder[<?php echo $cb_index; ?>][icon_image]" id="recipe_contents_builder-<?php echo $cb_index; ?>-icon_image" value="<?php echo esc_attr( $value['icon_image'] ); ?>">
       <div class="preview_field"><?php if ( $value['icon_image'] ) echo wp_get_attachment_image( $value['icon_image'], 'medium' ); ?></div>
        <div class="buttton_area">
         <input type="button" class="cfmf-select-img button" value="<?php _e( 'Select Image', 'tcd-w' ); ?>">
         <input type="button" class="cfmf-delete-img button<?php if ( ! $value['icon_image'] ) echo ' hidden'; ?>" value="<?php _e( 'Remove Image', 'tcd-w'); ?>">
        </div>
      </div>
      <div class="theme_option_message2">
       <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
       <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
      </div>
     </li>
    </ul>
    <?php // 作り方一覧 ---------- ?>
    <h3 class="theme_option_headline2"><?php _e( 'Procedure list', 'tcd-w' ); ?></h3>
    <div class="theme_option_message2">
     <p><?php _e('You can change order by dragging each item.', 'tcd-w'); ?></p>
    </div>
    <div class="repeater-wrapper">
     <div class="repeater sortable" data-delete-confirm="<?php _e( 'Delete?', 'tcd-w' ); ?>">
      <?php
           if ( $value['howto_list'] && is_array( $value['howto_list'] ) ) :
             foreach ( $value['howto_list'] as $repeater_key => $repeater_value ) :
               $repeater_value = array_merge( $cb_contents[$cb_content_select]['repeater_howto_list_row_default'], $repeater_value );
      ?>
      <div class="sub_box repeater-item repeater-item-<?php echo esc_attr( $repeater_key ); ?>">
       <h4 class="theme_option_subbox_headline"><?php _e( 'New item', 'tcd-w' ); ?></h4>
       <div class="sub_box_content">
        <h4 class="theme_option_headline2"><?php _e( 'Contents', 'tcd-w' ); ?></h4>
        <textarea class="repeater-label large-text" cols="50" rows="2" name="recipe_contents_builder[<?php echo $cb_index; ?>][howto_list][<?php echo esc_attr( $repeater_key ); ?>][content]"><?php echo esc_textarea( isset( $repeater_value['content'] ) ? $repeater_value['content'] : '' ); ?></textarea>
        <p class="delete-row right-align"><a href="#" class="button button-secondary button-delete-row"><?php _e( 'Delete item', 'tcd-w' ); ?></a></p>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php
             endforeach;
           endif;

           $repeater_key = 'addindex';
           $repeater_value = $cb_contents[$cb_content_select]['repeater_howto_list_row_default'];
           ob_start();
      ?>
      <div class="sub_box repeater-item repeater-item-<?php echo esc_attr( $repeater_key ); ?>">
       <h4 class="theme_option_subbox_headline"><?php _e( 'New item', 'tcd-w' ); ?></h4>
       <div class="sub_box_content">
        <h4 class="theme_option_headline2"><?php _e( 'Contents', 'tcd-w' ); ?></h4>
        <textarea class="repeater-label large-text" cols="50" rows="2" name="recipe_contents_builder[<?php echo $cb_index; ?>][howto_list][<?php echo esc_attr( $repeater_key ); ?>][content]"><?php echo esc_textarea( isset( $repeater_value['content'] ) ? $repeater_value['content'] : '' ); ?></textarea>
        <p class="delete-row right-align"><a href="#" class="button button-secondary button-delete-row"><?php _e( 'Delete item', 'tcd-w' ); ?></a></p>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php
           $clone = ob_get_clean();
      ?>
     </div><!-- END .repeater -->
     <a href="#" class="button button-secondary button-add-row" data-clone="<?php echo esc_attr( $clone ); ?>"><?php _e( 'Add item', 'tcd-w' ); ?></a>
    </div><!-- END .repeater-wrapper -->
    <?php // 作り方一覧ここまで ---------- ?>
    <h3 class="theme_option_headline2"><?php _e('Procedure list other setting', 'tcd-w'); ?></h3>
    <ul class="option_list">
     <li class="cf color_picker_bottom"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][list_bg_color]" value="<?php echo esc_attr( $value['list_bg_color'] ); ?>" data-default-color="#edf8fd" class="c-color-picker"></p>
     <li class="cf color_picker_bottom"><span class="label"><?php _e('Font color of headline', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][list_font_color]" value="<?php echo esc_attr( $value['list_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></p>
    </ul>
   </div>

   <?php
       // ポイント -------------------------------------------------------------------------
       elseif ( 'recipe_point' === $cb_content_select ) :
   ?>
   <h3 class="cb_content_headline"><?php echo esc_html( $cb_contents[$cb_content_select]['label'] ); ?><span></span></h3>
   <div class="cb_content">

    <h4 class="theme_option_headline2"><?php _e( 'Headline setting', 'tcd-w' ); ?></h4>
    <ul class="option_list">
     <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w');  ?></span><input class="full_width change_content_headline" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline]" value="<?php echo esc_attr( $value['headline'] ); ?>" /></li>
     <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_font_size]" value="<?php echo esc_attr( $value['headline_font_size'] ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_font_size_mobile]" value="<?php echo esc_attr( $value['headline_font_size_mobile'] ); ?>" /><span>px</span></li>
     <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_font_color]" value="<?php echo esc_attr( $value['headline_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Border color', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_border_color]" value="<?php echo esc_attr( $value['headline_border_color'] ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_bg_color]" value="<?php echo esc_attr( $value['headline_bg_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Hide icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="recipe_contents_builder[<?php echo $cb_index; ?>][hide_icon]" value="1" <?php checked( $value['hide_icon'], 1 ); ?>></li>
    </ul>
    <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($value['hide_icon']) { echo ' display:none;'; }; ?>">
     <li class="cf"><span class="label"><?php _e('Icon background color', 'tcd-w'); ?></span><input type="text" name="recipe_contents_builder[<?php echo $cb_index; ?>][headline_icon_color]" value="<?php echo esc_attr( $value['headline_icon_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Icon image', 'tcd-w');  ?></span>
      <div class="cf cf_media_field hide-if-no-js">
       <input type="hidden" class="cf_media_id" name="recipe_contents_builder[<?php echo $cb_index; ?>][icon_image]" id="recipe_contents_builder-<?php echo $cb_index; ?>-icon_image" value="<?php echo esc_attr( $value['icon_image'] ); ?>">
       <div class="preview_field"><?php if ( $value['icon_image'] ) echo wp_get_attachment_image( $value['icon_image'], 'medium' ); ?></div>
        <div class="buttton_area">
         <input type="button" class="cfmf-select-img button" value="<?php _e( 'Select Image', 'tcd-w' ); ?>">
         <input type="button" class="cfmf-delete-img button<?php if ( ! $value['icon_image'] ) echo ' hidden'; ?>" value="<?php _e( 'Remove Image', 'tcd-w'); ?>">
        </div>
      </div>
      <div class="theme_option_message2">
       <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
       <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
      </div>
     </li>
    </ul>
    <h3 class="theme_option_headline2"><?php _e('Description', 'tcd-w'); ?></h3>
    <?php wp_editor( $value['desc'], 'wysiwyg_editor-' . $cb_index, array( 'textarea_name' => 'recipe_contents_builder[' . $cb_index . '][desc]', 'textarea_rows' => 10 )); ?>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div>

   <?php
       // フリースペース -------------------------------------------------------------------------
       elseif ( 'recipe_free' === $cb_content_select ) :
   ?>
   <h3 class="cb_content_headline"><?php echo esc_html( $cb_contents[$cb_content_select]['label'] ); ?><span></span></h3>
   <div class="cb_content">
    <h3 class="theme_option_headline2"><?php _e('Description', 'tcd-w'); ?></h3>
    <?php wp_editor( $value['desc'], 'wysiwyg_editor-' . $cb_index, array( 'textarea_name' => 'recipe_contents_builder[' . $cb_index . '][desc]', 'textarea_rows' => 10 )); ?>
    <ul class="button_list cf">
     <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div>

   <?php else : ?>
   <h3 class="cb_content_headline"><?php echo esc_html( $cb_content_select ); ?></h3>
   <div class="cb_content">
    <ul class="button_list cf">
     <li><a href="#" class="button-ml close-content"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
    </ul>
   </div>
   <?php endif; ?>

  </div><!-- END .cb_content_wrap -->
<?php
}

/**
 * クローン用のリッチエディター化処理をしないようにする
 * クローン後のリッチエディター化はjsで行う
 */
function recipe_cb_tiny_mce_before_init( $mceInit, $editor_id ) {
	if ( strpos( $editor_id, 'cb_cloneindex' ) !== false ) {
		$mceInit['wp_skip_init'] = true;
	}
	return $mceInit;
}
add_filter( 'tiny_mce_before_init', 'recipe_cb_tiny_mce_before_init', 10, 2 );







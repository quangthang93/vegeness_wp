<?php

// カテゴリー編集用入力欄を出力 -------------------------------------------------------
function recipe_category_edit_extra_fields( $term ) {
	$term_meta = get_option( 'taxonomy_' . $term->term_id, array() );
	$term_meta = array_merge( array(
		'main_color' => '#ff7f00',
		'sub_color' => '#fbc525',
		'headline_font_size' => '14',
		'headline_font_size_mobile' => '12',
		'image' => null,
		'desc' => '',
		'use_overlay' => '1',
		'overlay_color' => '#000000',
		'overlay_opacity' => '0.5'
	), $term_meta );
?>
<tr class="form-field">
	<th colspan="2">

<div class="custom_category_meta<?php if(0 < $term->parent){ echo ' has_parent'; } ?>">
 <h3 class="ccm_headline"><?php _e( 'Additional data', 'tcd-w' ); ?></h3>

 <div class="ccm_content clearfix hide_if_child">
  <h4 class="headline"><?php _e( 'Color setting', 'tcd-w' ); ?></h4>
  <div class="theme_option_message2">
   <p><?php _e('Sub color will be used when user mouse over on the headline.', 'tcd-w'); ?></p>
  </div>
  <div class="input_field">
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Main color', 'tcd-w'); ?></span><input type="text" name="term_meta[main_color]" value="<?php echo esc_attr( $term_meta['main_color'] ); ?>" data-default-color="#ff7f00" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Sub color', 'tcd-w'); ?></span><input type="text" name="term_meta[sub_color]" value="<?php echo esc_attr( $term_meta['sub_color'] ); ?>" data-default-color="#fbc525" class="c-color-picker"></li>
   </ul>
  </div><!-- END input_field -->
 </div><!-- END ccm_content -->

 <div class="ccm_content clearfix hide_if_child">
  <h4 class="headline"><?php _e( 'Font size setting', 'tcd-w' ); ?></h4>
  <div class="theme_option_message2">
   <p><?php _e('This font size will be used in archive page and single page.', 'tcd-w'); ?></p>
  </div>
  <div class="input_field">
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Font size of headline', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="term_meta[headline_font_size]" value="<?php echo esc_attr( $term_meta['headline_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size of headline (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="term_meta[headline_font_size_mobile]" value="<?php echo esc_attr( $term_meta['headline_font_size_mobile'] ); ?>" /><span>px</span></li>
   </ul>
  </div><!-- END input_field -->
 </div><!-- END ccm_content -->

 <div class="ccm_content clearfix hide_if_child">
  <h4 class="headline"><?php _e( 'Image', 'tcd-w' ); ?></h4>
  <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '810', '455'); ?></p>
  <div class="input_field">
   <div class="image_box cf">
    <div class="cf cf_media_field hide-if-no-js image">
     <input type="hidden" value="<?php if ( $term_meta['image'] ) echo esc_attr( $term_meta['image'] ); ?>" id="image" name="term_meta[image]" class="cf_media_id">
     <div class="preview_field"><?php if ( $term_meta['image'] ) echo wp_get_attachment_image( $term_meta['image'], 'medium'); ?></div>
     <div class="button_area">
      <input type="button" value="<?php _e( 'Select Image', 'tcd-w' ); ?>" class="cfmf-select-img button">
      <input type="button" value="<?php _e( 'Remove Image', 'tcd-w' ); ?>" class="cfmf-delete-img button <?php if ( ! $term_meta['image'] ) echo 'hidden'; ?>">
     </div>
    </div>
   </div>
  </div><!-- END input_field -->
 </div><!-- END ccm_content -->

 <div class="ccm_content clearfix">
  <h4 class="headline"><?php _e( 'Description', 'tcd-w' ); ?></h4>
  <div class="input_field">
   <p><textarea class="large-text" cols="50" rows="3" name="term_meta[desc]"><?php echo esc_textarea( $term_meta['desc'] ); ?></textarea></p>
  </div><!-- END input_field -->
 </div><!-- END ccm_content -->

 <div class="ccm_content clearfix hide_if_child">
  <h4 class="headline"><?php _e( 'Overlay setting', 'tcd-w' ); ?></h4>
  <div class="input_field">
   <p class="displayment_checkbox"><label><input name="term_meta[use_overlay]" type="checkbox" value="1" <?php checked( $term_meta['use_overlay'], 1 ); ?>><?php _e( 'Use overlay', 'tcd-w' ); ?></label></p>
   <div class="blog_show_overlay" style="<?php if($term_meta['use_overlay'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
    <ul class="color_field" style="border-top:1px dotted #ccc; padding-top:12px;">
     <li class="cf"><span class="label"><?php _e('Color of overlay', 'tcd-w'); ?></span><input type="text" name="term_meta[overlay_color]" value="<?php echo esc_attr( $term_meta['overlay_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
     <li class="cf"><span class="label"><?php _e('Transparency of overlay', 'tcd-w'); ?></span><input class="hankaku" style="width:70px;" type="number" max="1" min="0" step="0.1" name="term_meta[overlay_opacity]" value="<?php echo esc_attr( $term_meta['overlay_opacity'] ); ?>" /><p><?php _e('Please specify the number of 0.1 from 0.9. Overlay color will be more transparent as the number is small.', 'tcd-w');  ?></p></li>
    </ul>
   </div><!-- END .blog_show_catch -->
  </div><!-- END input_field -->
 </div><!-- END ccm_content -->

</div><!-- END .custom_category_meta -->

 </th>
</tr><!-- END .form-field -->
<?php
}
add_action( 'recipe_category_edit_form_fields', 'recipe_category_edit_extra_fields' );


// データを保存 -------------------------------------------------------
function recipe_category_save_extra_fileds( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$term_meta = get_option( "taxonomy_{$term_id}", array() );
		$meta_keys = array(
			'main_color',
			'sub_color',
			'headline_font_size',
			'headline_font_size_mobile',
			'image',
			'desc',
			'use_overlay',
			'overlay_color',
			'overlay_opacity'
		);
		foreach ( $meta_keys as $key ) {
			if ( isset( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}

		update_option( "taxonomy_{$term_id}", $term_meta );
	}
}
add_action( 'edited_recipe_category', 'recipe_category_save_extra_fileds' );


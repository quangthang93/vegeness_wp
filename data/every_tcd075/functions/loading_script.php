<?php
     function has_loading_screen(){
       $options = get_design_plus_option();
?>
<script>

 <?php if(wp_is_mobile()) { ?>
 jQuery(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
      window.location.reload()
    }
 });
 <?php }; ?>

 jQuery(document).ready(function($){

  <?php
       // front page : pause slider while load screen is displayed -----------------------------------
       if(is_front_page()) {
         if($options['index_header_content_type'] == 'type1') {
           echo "$('#index_slider').slick('slickPause');\n";
         }
       };
  ?>

  function after_load() {
    $('#site_loader_overlay').delay(600).fadeOut(900);

    <?php
         // front page -----------------------------------
         if(is_front_page()) {
           if($options['index_header_content_type'] == 'type1') {
    ?>
    $('#index_slider').slick('setPosition');
    $('#index_slider').slick('slickPlay');
    $('#index_slider .caption.mobile').addClass('animate');
    <?php
           };
         };
         // 404 -----------------------------------
         if(is_404()) {
           echo "$('#page_404_header').addClass('animate');\n";
         };
         // widget -----------------------------------
         if ( is_active_widget(false, false, 'recipe_slider_widget', true) ) {
           echo "if( $('.recipe_slider').length ){\n";
             echo "$('.recipe_slider').slick('setPosition');\n";
           echo "}\n";
         };
         // page builder -----------------------------------
         if(is_single() || is_page()) {
           if(page_builder_has_widget('pb-widget-slider')) {
    ?>
    $('.pb_slider').slick('setPosition');
    <?php
           };
         };
    ?>

  }

  <?php if ($options['load_icon'] != 'type4') { ?>
  $(window).load(function () {
    after_load();
  });
  <?php }; ?>

  $(function(){
    <?php if ($options['load_icon'] == 'type4') { ?>
    $('#site_loader_logo').addClass('active');
    <?php }; ?>
    setTimeout(function(){
      if( $('#site_loader_overlay').is(':visible') ) {
        after_load();
      }
    }, <?php if($options['load_time']) { echo esc_html($options['load_time']); } else { echo '7000'; }; ?>);
  });

 });

</script>
<?php } ?>
<?php
     // no loading ------------------------------------------------------------------------------------------------------------------
     function no_loading_screen(){
       $options = get_design_plus_option();
?>
<script>
jQuery(document).ready(function($){
  <?php
       // front page -----------------------------------
       if(is_front_page()) {
         if($options['index_header_content_type'] == 'type1') {
  ?>
  $('#index_slider .caption.mobile').addClass('animate');
  <?php
         };
       };

       // 404 -----------------------------------
       if(is_404()) {
         echo "$('#page_404_header').addClass('animate');\n";
       };
  ?>
});
</script>
<?php } ?>
<?php
     get_header();
     $options = get_design_plus_option();
?>
<?php get_template_part('template-parts/breadcrumb'); ?>
<?php
     $page_header_type = get_post_meta($post->ID, 'page_header_type', true);
     if(empty($page_header_type)){
       $page_header_type = 'type1';
     }

     // if page header type1 -------------
     if($page_header_type == 'type1') {

     $font_type = get_post_meta($post->ID, 'page_title_font_type', true);
     if(empty($font_type)){
       $font_type = 'type3';
     }

     $sub_title = get_post_meta($post->ID, 'page_sub_title', true);

     $font_color = get_post_meta($post->ID, 'page_font_color', true);
     if(empty($font_color)){
       $font_color = '#FFFFFF';
     }

     $bg_color = get_post_meta($post->ID, 'page_bg_color', true);
     if(empty($bg_color)){
       $bg_color = '#666666';
     }

     $image_id = get_post_meta($post->ID, 'page_bg_image', true);
     if($image_id){
       $image = wp_get_attachment_image_src( $image_id, 'full' );
       if(is_mobile()) {
         $image_mobile_id = get_post_meta($post->ID, 'page_bg_image_mobile', true);
         if($image_mobile_id){
           $image = wp_get_attachment_image_src( $image_mobile_id, 'full');
         }
       }
     };
     $use_overlay = get_post_meta($post->ID, 'page_use_overlay', true);
     if($use_overlay) {
       $overlay_color = get_post_meta($post->ID, 'page_overlay_color', true);
       if(empty($overlay_color)){
         $overlay_color = '#000000';
       }
       $overlay_color = hex2rgb($overlay_color);
       $overlay_color = implode(",",$overlay_color);
       $overlay_opacity = get_post_meta($post->ID, 'page_overlay_opacity', true);
       if(empty($overlay_opacity)){
         $overlay_opacity = '0.5';
       }
     }
?>
<div id="wide_page_header" style="<?php if($image_id) { ?>background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;<?php } else { ?>background:<?php echo esc_attr($bg_color); ?><?php }; ?>">
 <div id="wide_page_header_inner" style="color:<?php echo esc_html($font_color); ?>;">
  <h2 class="catch rich_font_<?php echo esc_attr($font_type); ?>"><?php the_title(); ?></h2>
  <?php if($sub_title){ ?><p class="desc"><?php echo nl2br(esc_html($sub_title)); ?></p><?php }; ?>
 </div>
 <?php if($use_overlay) { ?><div class="overlay" style="background:rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>);"></div><?php }; ?>
</div>
<?php }; // if page header type1 ---------- ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <?php
       // if page header type2 -------------
       if($page_header_type == 'type2') {
  ?>
  <div id="page_design_headline_area">
   <h3 class="design_headline clearfix<?php if(get_post_meta($post->ID, 'page_headline_hide_icon', true)) { echo ' hide_icon'; }; ?>" id="page_header_design_headline"><?php the_title(); ?></h3>
  </div>
  <?php }; // if page header type2 ---------- ?>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <article id="article" class="page">

   <?php // post content ------------------------------------------------------------------------------------------------------------------------ ?>
   <div class="post_content clearfix">
    <?php
         the_content();
         if ( ! post_password_required() ) {
           $pagenation_type = get_post_meta($post->ID, 'pagenation_type', true);
           if($pagenation_type == 'type3') {
             $pagenation_type = $options['pagenation_type'];
           };
           if ( $pagenation_type == 'type2' ) {
             if ( $page < $numpages && preg_match( '/href="(.*?)"/', _wp_link_page( $page + 1 ), $matches ) ) :
    ?>
    <div id="p_readmore">
     <a class="button" href="<?php echo esc_url( $matches[1] ); ?>"><?php _e( 'Read more', 'tcd-w' ); ?></a>
     <p class="num"><?php echo $page . ' / ' . $numpages; ?></p>
    </div>
    <?php
             endif;
           } else {
             custom_wp_link_pages();
           }
         }
    ?>
   </div>

  </article><!-- END #article -->

  <?php endwhile; endif; ?>

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
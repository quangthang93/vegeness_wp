<?php
     get_header();
     $options = get_design_plus_option();
     $query_obj = get_queried_object();
     $cat_id = $query_obj->term_id;
     $post_count = $query_obj->count;
     $parent_cat_id = $query_obj->parent;
     if($parent_cat_id == 0){ // parent category page
       $category_name = single_cat_title('', false);
       $term_meta = get_option( 'taxonomy_' . $cat_id, array() );
       $desc = "";
       if (!empty($term_meta['desc'])){
         $desc = $term_meta['desc'];
       }
     } else { // child category page
       $parent_category_data = get_term_by('id', $parent_cat_id, 'recipe_category');
       $category_name = $parent_category_data->name;
       $child_category_term_meta = get_option( 'taxonomy_' . $cat_id, array() );
       $term_meta = get_option( 'taxonomy_' . $parent_cat_id, array() );
       $desc = "";
       if (!empty($child_category_term_meta['desc'])){
         $desc = $child_category_term_meta['desc'];
       }
     }
     $category_color = "#009fe1";
     if (!empty($term_meta['main_color'])){
       $category_color = $term_meta['main_color'];
     }
     $image_id = $options['recipe_bg_image'];
     if(!empty($image_id)) {
       $image = wp_get_attachment_image_src($image_id, 'full');
     }
     $use_overlay = $options['recipe_use_overlay'];
     if($use_overlay) {
       $overlay_color = hex2rgb($options['recipe_overlay_color']);
       $overlay_color = implode(",",$overlay_color);
       $overlay_opacity = $options['recipe_overlay_opacity'];
     }
     // overwrite the data if category data exist
     if (!empty($term_meta['image'])){
       $image = wp_get_attachment_image_src( $term_meta['image'], 'full' );
     }
     if (!empty($term_meta['use_overlay'])){
       if (!empty($term_meta['overlay_color'])){
         $overlay_color = hex2rgb($term_meta['overlay_color']);
         $overlay_color = implode(",",$overlay_color);
         if (!empty($term_meta['overlay_opacity'])){
           $overlay_opacity = $term_meta['overlay_opacity'];
         } else {
           $overlay_opacity = '0.3';
         }
       }
     }
?>
<?php get_template_part('template-parts/breadcrumb'); ?>

<div id="main_contents" class="clearfix">

 <div id="main_col" class="clearfix">

  <?php
       if($parent_cat_id == 0){ // parent category page
         if(!is_paged()) {
  ?>
  <div id="page_header" <?php if($image_id) { ?>style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"<?php }; ?>>
   <?php if($category_name){ ?><h2 class="headline rich_font" style="background:<?php echo esc_attr($category_color); ?>;"><span><?php echo wp_kses_post(nl2br($category_name)); ?></span></h2><?php }; ?>
   <?php if($desc){ ?><p class="desc"><?php echo wp_kses_post(nl2br($desc)); ?></p><?php }; ?>
   <?php if($use_overlay) { ?><div class="overlay" style="background: -webkit-linear-gradient(top, transparent 50%, rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>) 100%); background: linear-gradient(to bottom, transparent 50%, rgba(<?php echo esc_html($overlay_color); ?>,<?php echo esc_html($overlay_opacity); ?>) 100%);"></div><?php }; ?>
  </div>
  <?php
         };
       };
  ?>

  <div id="recipe_categry">

   <?php
        if($parent_cat_id == 0){ // parent category page
          if(!is_paged()) {
            $recipe_category = get_terms( 'recipe_category', array( 'hide_empty' => true, 'orderby' => 'id', 'parent' => $cat_id ) );
            if ( $recipe_category && ! is_wp_error( $recipe_category ) ) :
   ?>
   <div id="recipe_child_category_list_wrap_fix">
   <div id="recipe_child_category_list_wrap">
    <ol id="recipe_child_category_list" class="clearfix">
     <?php
          foreach ( $recipe_category as $child_cat ):
            $child_cat_id = $child_cat->term_id;
     ?>
     <li><a href="<?php echo esc_url(get_term_link($child_cat_id,'recipe_category')); ?>"><?php echo esc_html($child_cat->name); ?></a></li>
     <?php endforeach; ?>
    </ol>
   </div>
   </div>
   <?php endif; }; ?>

   <div id="archive_headline" class="clearfix">
    <p class="category rich_font" style="background:<?php echo esc_attr($category_color); ?>;"><?php echo esc_html($category_name); ?></p>
    <?php printf(__('<h2 class="title rich_font">%s %s</h2>', 'tcd-w'),$category_name,$options['category_recipe_headline_label']); ?>
    <?php if($options['show_category_recipe_post_count']) { ?><p class="post_count"><?php echo number_format($post_count) . esc_html($options['category_recipe_post_count_label']); ?></p><?php }; ?>
   </div>

   <?php } else { // child category page ?>

   <div id="archive_headline" class="clearfix">
    <p class="category rich_font" style="background:<?php echo esc_attr($category_color); ?>;"><?php echo esc_html($category_name); ?></p>
    <h2 class="title rich_font"><?php echo esc_html(single_cat_title('', false)); ?></h2>
    <?php if($options['show_category_recipe_post_count']) { ?><p class="post_count"><?php echo number_format($post_count) . esc_html($options['category_recipe_post_count_label']); ?></p><?php }; ?>
   </div>

   <?php if($desc){ ?><p class="child_category_desc"><?php echo wp_kses_post(nl2br($desc)); ?></p><?php }; ?>

   <?php }; ?>

   <?php if ( have_posts() ) : ?>

   <div class="recipe_list type2 clearfix">
    <?php
         while ( have_posts() ) : the_post();
           $recipe_type = get_post_meta($post->ID, 'recipe_type', true);
           $premium_recipe = get_post_meta($post->ID,'premium_recipe',true);
           if(has_post_thumbnail()) {
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'size1' );
           } elseif($options['no_image2']) {
             $image = wp_get_attachment_image_src( $options['no_image2'], 'full' );
           } else {
             $image = array();
             $image[0] = esc_url(get_bloginfo('template_url')) . "/img/common/no_image2.gif";
           }
    ?>
    <article class="item<?php if(!is_user_logged_in() && $premium_recipe) { echo ' register_link'; }; ?>">
     <a class="link animate_background" href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>">
      <div class="image_wrap">
       <?php if($premium_recipe && $options['category_recipe_list_show_premium_icon']) { ?><div class="premium_icon"></div><?php }; ?>
       <div class="image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;"></div>
      </div>
     </a>
     <div class="title_area">
      <h3 class="title"><a href="<?php if(!is_user_logged_in() && $premium_recipe) { echo '#'; } else { the_permalink(); }; ?>"><span><?php the_title(); ?></span></a></h3>
      <?php
           if($parent_cat_id == 0){  // parent category page
             $args = array( 'orderby' => 'term_order' );
             $recipe_child_category = wp_get_post_terms( $post->ID, 'recipe_category' ,$args);
             if ($recipe_child_category && $options['category_recipe_list_show_category']) {
      ?>
      <p class="post_meta">
       <?php
            foreach ( $recipe_child_category as $child_cat ) :
              if($child_cat->parent != 0) {
                 $child_cat_name = $child_cat->name;
                 $child_cat_id = $child_cat->term_id;
       ?>
       <a href="<?php echo esc_url(get_term_link($child_cat_id,'recipe_category')); ?>"><?php echo esc_html($child_cat_name); ?></a>
       <?php
              break;
              }
             endforeach;
       ?>
      </p>
      <?php
             };
           } else { // child category page
             if($options['category_recipe_list_show_post_view']) {
      ?>
      <p class="post_meta"><?php if($recipe_type != 'type2') { _e('Hits:', 'tcd-w'); } else { _e('Views:', 'tcd-w'); }; ?><?php the_post_views(); ?></p>
      <?php
             };
           };
      ?>
     </div>
    </article>
    <?php endwhile; ?>
   </div><!-- END .recipe_list1 -->

   <?php get_template_part('template-parts/navigation'); ?>

   <?php else: ?>

   <p id="no_post"><?php _e('There is no registered post.', 'tcd-w');  ?></p>

   <?php endif; ?>

  </div><!-- END recipe_archive -->

 </div><!-- END #main_col -->

 <?php get_sidebar(); ?>

</div><!-- END #main_contents -->

<?php get_footer(); ?>
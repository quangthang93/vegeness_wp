<?php
/*
 * フッターの設定
 */


// Add default values
add_filter( 'before_getting_design_plus_option', 'add_footer_dp_default_options' );


// Add label of footer tab
add_action( 'tcd_tab_labels', 'add_footer_tab_label' );


// Add HTML of footer tab
add_action( 'tcd_tab_panel', 'add_footer_tab_panel' );


// Register sanitize function
add_filter( 'theme_options_validate', 'add_footer_theme_options_validate' );


// タブの名前
function add_footer_tab_label( $tab_labels ) {
	$tab_labels['footer'] = __( 'Footer', 'tcd-w' );
	return $tab_labels;
}


// 初期値
function add_footer_dp_default_options( $dp_default_options ) {

	// ウィジェットエリア
	$dp_default_options['show_footer_menu'] = 1;
	$dp_default_options['show_footer_category_menu'] = 1;
	$dp_default_options['footer_menu_headline'] = __( 'Menu', 'tcd-w' );
	$dp_default_options['footer_category_menu_headline'] = __( 'Category', 'tcd-w' );
	$dp_default_options['footer_category_exclude_cat'] = '';
	$dp_default_options['footer_bg_type'] = 'type1';
	$dp_default_options['footer_widget_bg_image'] = false;
	$dp_default_options['footer_widget_video'] = false;
	$dp_default_options['footer_widget_video_image'] = false;
	$dp_default_options['footer_widget_use_overlay'] = 1;
	$dp_default_options['footer_widget_overlay_color'] = '#000000';
	$dp_default_options['footer_widget_overlay_opacity'] = '0.3';
	$dp_default_options['footer_widget_font_color'] = '#ffffff';
	$dp_default_options['footer_widget_headline_color'] = '#ff8000';
	$dp_default_options['footer_widget_border_color'] = '#ffffff';
	$dp_default_options['footer_widget_border_opacity'] = '0.3';

  //SNS
	$dp_default_options['footer_facebook_url'] = '';
	$dp_default_options['footer_twitter_url'] = '';
	$dp_default_options['footer_instagram_url'] = '';
	$dp_default_options['footer_pinterest_url'] = '';
	$dp_default_options['footer_youtube_url'] = '';
	$dp_default_options['footer_contact_url'] = '';
	$dp_default_options['footer_show_rss'] = 1;

  //コピーライト
	$dp_default_options['copyright'] = 'Copyright &copy; 2018';

	// フッターの固定メニュー
	$dp_default_options['footer_bar_display'] = 'type3';
	$dp_default_options['footer_bar_tp'] = 0.8;
	$dp_default_options['footer_bar_bg'] = '#FFFFFF';
	$dp_default_options['footer_bar_border'] = '#DDDDDD';
	$dp_default_options['footer_bar_color'] = '#000000';
	$dp_default_options['footer_bar_btns'] = array(
		array(
			'type' => 'type1',
			'label' => '',
			'url' => '',
			'number' => '',
			'target' => 0,
			'icon' => 'file-text'
		)
	);

	return $dp_default_options;

}


// 入力欄の出力　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_footer_tab_panel( $options ) {

  global $dp_default_options, $footer_bar_display_options, $footer_bar_button_options, $footer_bar_icon_options, $fixed_footer_banner_type_options, $fixed_footer_sub_content_type_options;
  $recipe_category_label = $options['recipe_category_label'] ? esc_html( $options['recipe_category_label'] ) : __( 'Recipe category', 'tcd-w' );

?>

<div id="tab-content-footer" class="tab-content">


   <?php // ウィジェット -------------------------------------------------------------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Widget area setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e( 'Custom menu setting', 'tcd-w' ); ?></h4>
     <div class="theme_option_message2">
      <p><?php _e('Please register custom menu from <a href="./nav-menus.php">custom navigaton page</a>.', 'tcd-w'); ?></p>
     </div>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Display custom menu', 'tcd-w'); ?></span><input name="dp_options[show_footer_menu]" type="checkbox" value="1" <?php checked( $options['show_footer_menu'], 1 ); ?>></li>
      <li class="cf"><span class="label"><?php _e('Headline of custom menu', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[footer_menu_headline]" value="<?php esc_attr_e( $options['footer_menu_headline'] ); ?>" /></li>
     </ul>
     <h4 class="theme_option_headline2"><?php printf(__('%s menu setting', 'tcd-w'), $recipe_category_label); ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php printf(__('Display %s menu', 'tcd-w'), $recipe_category_label); ?></span><input name="dp_options[show_footer_category_menu]" type="checkbox" value="1" <?php checked( $options['show_footer_category_menu'], 1 ); ?>></li>
      <li class="cf"><span class="label"><?php printf(__('Headline of %s menu', 'tcd-w'), $recipe_category_label); ?></span><input class="full_width" type="text" name="dp_options[footer_category_menu_headline]" value="<?php esc_attr_e( $options['footer_category_menu_headline'] ); ?>" /></li>
      <li class="cf"><span class="label"><?php _e('Categories to exclude', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[footer_category_exclude_cat]" value="<?php esc_attr_e( $options['footer_category_exclude_cat'] ); ?>" /><div class="theme_option_message2"><p><?php _e('Enter a comma-seperated list of category ID numbers, example 2,4,10<br />(Don\'t enter comma for last number).', 'tcd-w'); ?></p></div></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e( 'Color setting', 'tcd-w' ); ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Headline color', 'tcd-w'); ?></span><input type="text" name="dp_options[footer_widget_headline_color]" value="<?php echo esc_attr( $options['footer_widget_headline_color'] ); ?>" data-default-color="#ff8000" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[footer_widget_font_color]" value="<?php echo esc_attr( $options['footer_widget_font_color'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Border color', 'tcd-w'); ?></span><input type="text" name="dp_options[footer_widget_border_color]" value="<?php echo esc_attr( $options['footer_widget_border_color'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Transparency of border color', 'tcd-w'); ?></span><input class="hankaku" style="width:70px;" type="number" max="1" min="0" step="0.1" name="dp_options[footer_widget_border_opacity]" value="<?php echo esc_attr( $options['footer_widget_border_opacity'] ); ?>" /><p><?php _e('Please specify the number of 0.1 from 0.9. Overlay color will be more transparent as the number is small.', 'tcd-w');  ?></p></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e('Background setting', 'tcd-w'); ?></h4>
     <ul class="design_radio_button">
      <li>
       <input type="radio" id="footer_bg_type1" name="dp_options[footer_bg_type]" value="type1" <?php checked( $options['footer_bg_type'], 'type1' ); ?> />
       <label for="footer_bg_type1"><?php _e('Image', 'tcd-w'); ?></label>
      </li>
      <li>
       <input type="radio" id="footer_bg_type2" name="dp_options[footer_bg_type]" value="type2" <?php checked( $options['footer_bg_type'], 'type2' ); ?> />
       <label for="footer_bg_type2"><?php _e('Video', 'tcd-w'); ?></label>
      </li>
     </ul>
     <div id="footer_bg_type1_area" style="<?php if($options['footer_bg_type'] == 'type1') { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <h4 class="theme_option_headline2"><?php _e('Background image setting', 'tcd-w'); ?></h4>
      <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '1450', '650'); ?></p>
      <div class="image_box cf">
       <div class="cf cf_media_field hide-if-no-js footer_widget_bg_image">
        <input type="hidden" value="<?php echo esc_attr( $options['footer_widget_bg_image'] ); ?>" id="footer_widget_bg_image" name="dp_options[footer_widget_bg_image]" class="cf_media_id">
        <div class="preview_field"><?php if($options['footer_widget_bg_image']){ echo wp_get_attachment_image($options['footer_widget_bg_image'], 'medium'); }; ?></div>
        <div class="buttton_area">
         <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
         <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['footer_widget_bg_image']){ echo 'hidden'; }; ?>">
        </div>
       </div>
      </div>
     </div>
     <div id="footer_bg_type2_area" style="<?php if($options['footer_bg_type'] == 'type2') { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <h4 class="theme_option_headline2"><?php _e('Video setting', 'tcd-w'); ?></h4>
      <div class="theme_option_message2">
       <p><?php _e('Please upload MP4 format file.', 'tcd-w');  ?></p>
       <p><?php _e('Web browser takes few second to load the data of video so we recommend to use loading screen if you want to display video.', 'tcd-w'); ?></p>
      </div>
      <div class="image_box cf">
       <div class="cf cf_media_field hide-if-no-js footer_widget_video">
        <input type="hidden" value="<?php echo esc_attr( $options['footer_widget_video'] ); ?>" id="footer_widget_video" name="dp_options[footer_widget_video]" class="cf_media_id">
        <div class="preview_field preview_field_video">
         <?php if($options['footer_widget_video']){ ?>
         <h4><?php _e( 'Uploaded MP4 file', 'tcd-w' ); ?></h4>
         <p><?php echo esc_url(wp_get_attachment_url($options['footer_widget_video'])); ?></p>
         <?php }; ?>
        </div>
        <div class="buttton_area">
         <input type="button" value="<?php _e('Select MP4 file', 'tcd-w'); ?>" class="cfmf-select-video button">
         <input type="button" value="<?php _e('Remove MP4 file', 'tcd-w'); ?>" class="cfmf-delete-video button <?php if(!$options['footer_widget_video']){ echo 'hidden'; }; ?>">
        </div>
       </div>
      </div>
      <h4 class="theme_option_headline2"><?php _e('Substitute image', 'tcd-w');  ?></h4>
      <div class="theme_option_message2">
       <p><?php _e('If the mobile device can\'t play video this image will be displayed instead.', 'tcd-w');  ?></p>
      </div>
      <div class="image_box cf">
       <div class="cf cf_media_field hide-if-no-js footer_widget_video_image">
        <input type="hidden" value="<?php echo esc_attr( $options['footer_widget_video_image'] ); ?>" id="footer_widget_video_image" name="dp_options[footer_widget_video_image]" class="cf_media_id">
        <div class="preview_field"><?php if($options['footer_widget_video_image']){ echo wp_get_attachment_image($options['footer_widget_video_image'], 'full'); }; ?></div>
        <div class="buttton_area">
         <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
         <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['footer_widget_video_image']){ echo 'hidden'; }; ?>">
        </div>
       </div>
      </div>
     </div>
     <h4 class="theme_option_headline2"><?php _e( 'Overlay setting for background', 'tcd-w' ); ?></h4>
     <p class="displayment_checkbox"><label><input name="dp_options[footer_widget_use_overlay]" type="checkbox" value="1" <?php checked( $options['footer_widget_use_overlay'], 1 ); ?>><?php _e( 'Use overlay', 'tcd-w' ); ?></label></p>
     <div style="<?php if($options['footer_widget_use_overlay'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
       <li class="cf"><span class="label"><?php _e('Color of overlay', 'tcd-w'); ?></span><input type="text" name="dp_options[footer_widget_overlay_color]" value="<?php echo esc_attr( $options['footer_widget_overlay_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
       <li class="cf">
        <span class="label"><?php _e('Transparency of overlay', 'tcd-w'); ?></span><input class="hankaku" style="width:70px;" type="number" max="1" min="0" step="0.1" name="dp_options[footer_widget_overlay_opacity]" value="<?php echo esc_attr( $options['footer_widget_overlay_opacity'] ); ?>" />
        <div class="theme_option_message2" style="clear:both; margin:7px 0 0 0;">
         <p><?php _e('Please specify the number of 0.1 from 0.9. Overlay color will be more transparent as the number is small.', 'tcd-w');  ?></p>
        </div>
       </li>
      </ul>
     </div>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // SNSボタンの設定 ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('SNS button setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <div class="theme_option_message2">
      <p><?php _e('Enter url of your Twitter, Facebook, Instagram, Pinterest, Flickr, Tumblr, and contact page. Please leave the field empty if you don\'t want to display certain sns button.', 'tcd-w');  ?></p>
     </div>
     <ul>
      <li>
       <label style="display:inline-block; min-width:140px;"><?php _e('Facebook URL', 'tcd-w');  ?></label>
       <input id="dp_options[footer_facebook_url]" class="regular-text" type="text" name="dp_options[footer_facebook_url]" value="<?php esc_attr_e( $options['footer_facebook_url'] ); ?>" />
      </li>
      <li>
       <label style="display:inline-block; min-width:140px;"><?php _e('Twitter URL', 'tcd-w');  ?></label>
       <input id="dp_options[footer_twitter_url]" class="regular-text" type="text" name="dp_options[footer_twitter_url]" value="<?php esc_attr_e( $options['footer_twitter_url'] ); ?>" />
      </li>
      <li>
       <label style="display:inline-block; min-width:140px;"><?php _e('Instagram URL', 'tcd-w');  ?></label>
       <input id="dp_options[footer_instagram_url]" class="regular-text" type="text" name="dp_options[footer_instagram_url]" value="<?php esc_attr_e( $options['footer_instagram_url'] ); ?>" />
      </li>
      <li>
       <label style="display:inline-block; min-width:140px;"><?php _e('Pinterest URL', 'tcd-w');  ?></label>
       <input id="dp_options[footer_pinterest_url]" class="regular-text" type="text" name="dp_options[footer_pinterest_url]" value="<?php esc_attr_e( $options['footer_pinterest_url'] ); ?>" />
      </li>
      <li>
       <label style="display:inline-block; min-width:140px;"><?php _e('Youtube URL', 'tcd-w');  ?></label>
       <input id="dp_options[footer_youtube_url]" class="regular-text" type="text" name="dp_options[footer_youtube_url]" value="<?php esc_attr_e( $options['footer_youtube_url'] ); ?>" />
      </li>
      <li>
       <label style="display:inline-block; min-width:140px;"><?php _e('Your Contact page URL (You can use mailto:)', 'tcd-w');  ?></label>
       <input id="dp_options[footer_contact_url]" class="regular-text" type="text" name="dp_options[footer_contact_url]" value="<?php esc_attr_e( $options['footer_contact_url'] ); ?>" />
      </li>
     </ul>
     <hr />
     <p><label><input id="dp_options[footer_show_rss]" name="dp_options[footer_show_rss]" type="checkbox" value="1" <?php checked( '1', $options['footer_show_rss'] ); ?> /> <?php _e('Display RSS button', 'tcd-w');  ?></label></p>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // コピーライトの設定 ------------------------------------------------------------ ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Copyright setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <input id="dp_options[copyright]" class="regular-text" type="text" name="dp_options[copyright]" value="<?php echo esc_attr($options['copyright']); ?>" />
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // フッターバーの設定 -------------------------------------------------------------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e( 'Footer bar setting (mobile device only)', 'tcd-w' ); ?></h3>
    <div class="theme_option_field_ac_content">
      <div class="theme_option_message2">
       <p><?php _e( 'Footer bar will only be displayed at mobile device.', 'tcd-w' ); ?>
      </div>
      <h4 class="theme_option_headline2"><?php _e('Display type of the footer bar', 'tcd-w'); ?></h4>
      <ul class="design_radio_button">
       <?php foreach ( $footer_bar_display_options as $option ) { ?>
       <li>
        <input type="radio" id="footer_bar_display_<?php esc_attr_e( $option['value'] ); ?>" name="dp_options[footer_bar_display]" value="<?php esc_attr_e( $option['value'] ); ?>" <?php checked( $options['footer_bar_display'], $option['value'] ); ?> />
        <label for="footer_bar_display_<?php esc_attr_e( $option['value'] ); ?>"><?php echo $option['label']; ?></label>
       </li>
       <?php } ?>
      </ul>
      <h4 class="theme_option_headline2"><?php _e('Settings for the appearance of the footer bar', 'tcd-w'); ?></h4>
      <ul class="color_field">
       <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="dp_options[footer_bar_bg]" value="<?php echo esc_attr( $options['footer_bar_bg'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
       <li class="cf"><span class="label"><?php _e('Border color', 'tcd-w'); ?></span><input type="text" name="dp_options[footer_bar_border]" value="<?php echo esc_attr( $options['footer_bar_border'] ); ?>" data-default-color="#DDDDDD" class="c-color-picker"></li>
       <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[footer_bar_color]" value="<?php echo esc_attr( $options['footer_bar_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
       <li class="cf"><span class="label"><?php _e('Opacity of background', 'tcd-w'); ?></span><input id="dp_options[footer_bar_tp]" class="font_size hankaku" type="text" name="dp_options[footer_bar_tp]" value="<?php echo esc_attr( $options['footer_bar_tp'] ); ?>" /><p><?php _e('Please enter the number 0 - 1.0. (e.g. 0.8)', 'tcd-w'); ?></p></li>
      </ul>
      <h4 class="theme_option_headline2"><?php _e('Settings for the contents of the footer bar', 'tcd-w'); ?></h4>
      <div class="theme_option_message2">
       <p><?php _e( 'You can display the button with icon in footer bar. (We recommend you to set max 4 buttons.)', 'tcd-w' ); ?><br><?php _e( 'You can select button types below.', 'tcd-w' ); ?></p>
      </div>
      <table class="table-border">
       <tr>
        <th><?php _e( 'Default', 'tcd-w' ); ?></th>
        <td><?php _e( 'You can set link URL.', 'tcd-w' ); ?></td>
       </tr>
       <tr>
        <th><?php _e( 'Share', 'tcd-w' ); ?></th>
        <td><?php _e( 'Share buttons are displayed if you tap this button.', 'tcd-w' ); ?></td>
       </tr>
       <tr>
        <th><?php _e( 'Telephone', 'tcd-w' ); ?></th>
        <td><?php _e( 'You can call this number.', 'tcd-w' ); ?></td>
       </tr>
      </table>
      <p><?php _e( 'Click "Add item", and set the button for footer bar. You can drag the item to change their order.', 'tcd-w' ); ?></p>
      <div class="repeater-wrapper">
       <div class="repeater sortable" data-delete-confirm="<?php _e( 'Delete?', 'tcd-w' ); ?>">
<?php
    if ( $options['footer_bar_btns'] ) :
      foreach ( $options['footer_bar_btns'] as $key => $value ) :  
?>
      <div class="sub_box repeater-item repeater-item-<?php echo esc_attr( $key ); ?>">
       <h4 class="theme_option_subbox_headline"><?php echo esc_attr( $value['label'] ); ?></h4>
       <div class="sub_box_content">
        <p class="footer-bar-target" style="<?php if ( $value['type'] !== 'type1' ) { echo 'display: none;'; } ?>"><label><input name="dp_options[repeater_footer_bar_btns][<?php echo esc_attr( $key ); ?>][target]" type="checkbox" value="1" <?php checked( $value['target'], 1 ); ?>><?php _e( 'Open with new window', 'tcd-w' ); ?></label></p>
        <table class="table-repeater">
         <tr class="footer-bar-type">
          <th><label><?php _e( 'Button type', 'tcd-w' ); ?></label></th>
          <td>
           <select name="dp_options[repeater_footer_bar_btns][<?php echo esc_attr( $key ); ?>][type]">
            <?php foreach( $footer_bar_button_options as $option ) : ?>
            <option value="<?php echo esc_attr( $option['value'] ); ?>" <?php selected( $value['type'], $option['value'] ); ?>><?php esc_html_e( $option['label'], 'tcd-w' ); ?></option>
            <?php endforeach; ?>
           </select>
          </td>
         </tr>
         <tr>
          <th><label for="dp_options[footer_bar_btn<?php echo esc_attr( $key ); ?>_label]"><?php _e( 'Button label', 'tcd-w' ); ?></label></th>
          <td><input id="dp_options[footer_bar_btn<?php echo esc_attr( $key ); ?>_label]" class="large-text repeater-label" type="text" name="dp_options[repeater_footer_bar_btns][<?php echo esc_attr( $key ); ?>][label]" value="<?php echo esc_attr( $value['label'] ); ?>"></td>
         </tr>
         <tr class="footer-bar-url" style="<?php if ( $value['type'] !== 'type1' ) { echo 'display: none;'; } ?>">
          <th><label for="dp_options[footer_bar_btn<?php echo esc_attr( $key ); ?>_url]"><?php _e( 'Link URL', 'tcd-w' ); ?></label></th>
          <td><input id="dp_options[footer_bar_btn<?php echo esc_attr( $key ); ?>_url]" class="large-text" type="text" name="dp_options[repeater_footer_bar_btns][<?php echo esc_attr( $key ); ?>][url]" value="<?php echo esc_attr( $value['url'] ); ?>"></td>
         </tr>
         <tr class="footer-bar-number" style="<?php if ( $value['type'] !== 'type3' ) { echo 'display: none;'; } ?>">
          <th><label for="dp_options[footer_bar_btn<?php echo esc_attr( $key ); ?>_number]"><?php _e( 'Phone number', 'tcd-w' ); ?></label></th>
          <td><input id="dp_options[footer_bar_btn<?php echo esc_attr( $key ); ?>_number]" class="large-text" type="text" name="dp_options[repeater_footer_bar_btns][<?php echo esc_attr( $key ); ?>][number]" value="<?php echo esc_attr( $value['number'] ); ?>"></td>
         </tr>
         <tr>
          <th><?php _e( 'Button icon', 'tcd-w' ); ?></th>
          <td>
           <?php foreach( $footer_bar_icon_options as $option ) : ?>
           <p><label><input type="radio" name="dp_options[repeater_footer_bar_btns][<?php echo esc_attr( $key ); ?>][icon]" value="<?php echo esc_attr( $option['value'] ); ?>" <?php checked( $option['value'], $value['icon'] ); ?>><span class="icon icon-<?php echo esc_attr( $option['value'] ); ?>"></span><?php esc_html_e( $option['label'], 'tcd-w' ); ?></label></p>
           <?php endforeach; ?>
          </td>
         </tr>
        </table>
        <p class="delete-row right-align"><a href="#" class="button button-secondary button-delete-row"><?php _e( 'Delete item', 'tcd-w' ); ?></a></p>
       </div>
      </div>
<?php
      endforeach;
    endif;

    $key = 'addindex';
    ob_start();
?>
      <div class="sub_box repeater-item repeater-item-<?php echo $key; ?>">
       <h4 class="theme_option_subbox_headline"><?php _e( 'New item', 'tcd-w' ); ?></h4>
       <div class="sub_box_content">
        <p class="footer-bar-target"><label><input name="dp_options[repeater_footer_bar_btns][<?php echo esc_attr( $key ); ?>][target]" type="checkbox" value="1"><?php _e( 'Open with new window', 'tcd-w' ); ?></label></p>
        <table class="table-repeater">
         <tr class="footer-bar-type">
          <th><label><?php _e( 'Button type', 'tcd-w' ); ?></label></th>
          <td>
           <select name="dp_options[repeater_footer_bar_btns][<?php echo esc_attr( $key ); ?>][type]">
            <?php foreach( $footer_bar_button_options as $option ) : ?>
            <option value="<?php echo esc_attr( $option['value'] ); ?>"><?php esc_html_e( $option['label'], 'tcd-w' ); ?></option>
            <?php endforeach; ?>
           </select>
          </td>
         </tr>
         <tr>
          <th><label for="dp_options[repeater_footer_bar_btn<?php echo esc_attr( $key ); ?>_label]"><?php _e( 'Button label', 'tcd-w' ); ?></label></th>
          <td><input id="dp_options[footer_bar_btn<?php echo esc_attr( $key ); ?>_label]" class="large-text repeater-label" type="text" name="dp_options[repeater_footer_bar_btns][<?php echo esc_attr( $key ); ?>][label]" value=""></td>
         </tr>
         <tr class="footer-bar-url">
          <th><label for="dp_options[footer_bar_btn<?php echo esc_attr( $key ); ?>_url]"><?php _e( 'Link URL', 'tcd-w' ); ?></label></th>
          <td><input id="dp_options[footer_bar_btn<?php echo esc_attr( $key ); ?>_url]" class="large-text" type="text" name="dp_options[repeater_footer_bar_btns][<?php echo esc_attr( $key ); ?>][url]" value=""></td>
         </tr>
         <tr class="footer-bar-number" style="display: none;">
          <th><label for="dp_options[footer_bar_btn<?php echo esc_attr( $key ); ?>_number]"><?php _e( 'Phone number', 'tcd-w' ); ?></label></th>
          <td><input id="dp_options[footer_bar_btn<?php echo esc_attr( $key ); ?>_number]" class="large-text" type="text" name="dp_options[repeater_footer_bar_btns][<?php echo esc_attr( $key ); ?>][number]" value=""></td>
         </tr>
         <tr>
          <th><?php _e( 'Button icon', 'tcd-w' ); ?></th>
          <td>
           <?php foreach( $footer_bar_icon_options as $option ) : ?>
           <p><label><input type="radio" name="dp_options[repeater_footer_bar_btns][<?php echo esc_attr( $key ); ?>][icon]" value="<?php echo esc_attr( $option['value'] ); ?>"<?php if ( 'file-text' == $option['value'] ) { echo ' checked="checked"'; } ?>><span class="icon icon-<?php echo esc_attr( $option['value'] ); ?>"></span><?php esc_html_e( $option['label'], 'tcd-w' ); ?></label></p>
           <?php endforeach; ?>
          </td>
         </tr>
        </table>
        <p class="delete-row right-align"><a href="#" class="button button-secondary button-delete-row"><?php _e( 'Delete item', 'tcd-w' ); ?></a></p>
       </div>
      </div>
<?php
    $clone = ob_get_clean();
?>
       </div><!-- END .repeater -->
       <a href="#" class="button button-secondary button-add-row" data-clone="<?php echo esc_attr( $clone ); ?>"><?php _e( 'Add item', 'tcd-w' ); ?></a>
      </div><!-- END .repeater-wrapper -->
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


</div><!-- END .tab-content -->

<?php
} // END add_footer_tab_panel()


// バリデーション　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_footer_theme_options_validate( $input ) {

  global $dp_default_options, $footer_bar_display_options, $footer_bar_button_options, $footer_bar_icon_options, $fixed_footer_banner_type_options, $fixed_footer_sub_content_type_options;

  // ウィジェット
  $input['footer_widget_font_color'] = wp_filter_nohtml_kses( $input['footer_widget_font_color'] );
  $input['footer_widget_headline_color'] = wp_filter_nohtml_kses( $input['footer_widget_headline_color'] );
  $input['footer_widget_border_color'] = wp_filter_nohtml_kses( $input['footer_widget_border_color'] );
  $input['footer_widget_border_opacity'] = wp_filter_nohtml_kses( $input['footer_widget_border_opacity'] );
  $input['footer_bg_type'] = wp_filter_nohtml_kses( $input['footer_bg_type'] );
  $input['footer_widget_video'] = wp_filter_nohtml_kses( $input['footer_widget_video'] );
  $input['footer_widget_video_image'] = wp_filter_nohtml_kses( $input['footer_widget_video_image'] );
  $input['footer_widget_bg_image'] = wp_filter_nohtml_kses( $input['footer_widget_bg_image'] );
  if ( ! isset( $input['footer_widget_use_overlay'] ) )
    $input['footer_widget_use_overlay'] = null;
    $input['footer_widget_use_overlay'] = ( $input['footer_widget_use_overlay'] == 1 ? 1 : 0 );
  $input['footer_widget_overlay_color'] = wp_filter_nohtml_kses( $input['footer_widget_overlay_color'] );
  $input['footer_widget_overlay_opacity'] = wp_filter_nohtml_kses( $input['footer_widget_overlay_opacity'] );
  if ( ! isset( $input['show_footer_menu'] ) )
    $input['show_footer_menu'] = null;
    $input['show_footer_menu'] = ( $input['show_footer_menu'] == 1 ? 1 : 0 );
  if ( ! isset( $input['show_footer_category_menu'] ) )
    $input['show_footer_category_menu'] = null;
    $input['show_footer_category_menu'] = ( $input['show_footer_category_menu'] == 1 ? 1 : 0 );
  $input['footer_menu_headline'] = wp_filter_nohtml_kses( $input['footer_menu_headline'] );
  $input['footer_category_menu_headline'] = wp_filter_nohtml_kses( $input['footer_category_menu_headline'] );
  $input['footer_category_exclude_cat'] = wp_filter_nohtml_kses( $input['footer_category_exclude_cat'] );

  //フッターのSNSボタンの設定
  $input['footer_facebook_url'] = wp_filter_nohtml_kses( $input['footer_facebook_url'] );
  $input['footer_twitter_url'] = wp_filter_nohtml_kses( $input['footer_twitter_url'] );
  $input['footer_instagram_url'] = wp_filter_nohtml_kses( $input['footer_instagram_url'] );
  $input['footer_pinterest_url'] = wp_filter_nohtml_kses( $input['footer_pinterest_url'] );
  $input['footer_youtube_url'] = wp_filter_nohtml_kses( $input['footer_youtube_url'] );
  $input['footer_contact_url'] = wp_filter_nohtml_kses( $input['footer_contact_url'] );
  if ( ! isset( $input['footer_show_rss'] ) )
    $input['footer_show_rss'] = null;
    $input['footer_show_rss'] = ( $input['footer_show_rss'] == 1 ? 1 : 0 );

  // コピーライト
  $input['copyright'] = wp_kses_post($input['copyright']);

  // スマホ用固定フッターバーの設定
  $footer_bar_btns = array();
  if ( ! isset( $input['repeater_footer_bar_btns'] ) && ! empty( $input['footer_bar_btns'] ) && is_array($input['footer_bar_btns'] ) ) :
    $input['repeater_footer_bar_btns'] = $input['footer_bar_btns'];
  endif;
  if ( isset( $input['repeater_footer_bar_btns'] ) ) :
	  foreach ( (array)$input['repeater_footer_bar_btns'] as $key => $value ) {
	    $footer_bar_btns[] = array(
	      'type' => ( isset( $input['repeater_footer_bar_btns'][$key]['type'] ) && array_key_exists( $input['repeater_footer_bar_btns'][$key]['type'], $footer_bar_button_options ) ) ? $input['repeater_footer_bar_btns'][$key]['type'] : 'type1',
	      'label' => isset( $input['repeater_footer_bar_btns'][$key]['label'] ) ? wp_filter_nohtml_kses( $input['repeater_footer_bar_btns'][$key]['label'] ) : '',
	      'url' => isset( $input['repeater_footer_bar_btns'][$key]['url'] ) ? wp_filter_nohtml_kses( $input['repeater_footer_bar_btns'][$key]['url'] ) : '',
	      'number' => isset( $input['repeater_footer_bar_btns'][$key]['number'] ) ? wp_filter_nohtml_kses( $input['repeater_footer_bar_btns'][$key]['number'] ) : '',
	      'target' => ! empty( $input['repeater_footer_bar_btns'][$key]['target'] ) ? 1 : 0,
	      'icon' => ( isset( $input['repeater_footer_bar_btns'][$key]['icon'] ) && array_key_exists( $input['repeater_footer_bar_btns'][$key]['icon'], $footer_bar_icon_options ) ) ? $input['repeater_footer_bar_btns'][$key]['icon'] : 'file-text'
	    );
	  }
	  unset( $input['repeater_footer_bar_btns'] );
  endif;
  $input['footer_bar_btns'] = $footer_bar_btns;

	return $input;

};


?>
<?php
/*
 * 実績の設定
 */


// Add default values
add_filter( 'before_getting_design_plus_option', 'add_recipe_dp_default_options' );


//  Add label of recipe tab
add_action( 'tcd_tab_labels', 'add_recipe_tab_label' );


// Add HTML of recipe tab
add_action( 'tcd_tab_panel', 'add_recipe_tab_panel' );


// Register sanitize function
add_filter( 'theme_options_validate', 'add_recipe_theme_options_validate' );


// タブの名前
function add_recipe_tab_label( $tab_labels ) {
  $options = get_design_plus_option();
  $tab_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );
  $tab_labels['recipe'] = $tab_label;
	return $tab_labels;
}


// 初期値
function add_recipe_dp_default_options( $dp_default_options ) {

	// 基本設定
	$dp_default_options['recipe_label'] = __( 'Recipe', 'tcd-w' );
	$dp_default_options['recipe_slug'] = 'recipe';
	$dp_default_options['recipe_category_label'] = __( 'Recipe category', 'tcd-w' );
	$dp_default_options['recipe_category_slug'] = 'recipe_category';
	$dp_default_options['recipe_premium_icon_color'] = '#bcab4a';
	$dp_default_options['recipe_premium_icon_image'] = false;

	// ヘッダー
	$dp_default_options['recipe_headline'] = __( 'Recipe', 'tcd-w' );
	$dp_default_options['recipe_desc'] = __( 'Description will be displayed here.<br />Description will be displayed here.', 'tcd-w' );
	$dp_default_options['recipe_headline_font_size'] = '14';
	$dp_default_options['recipe_headline_font_size_mobile'] = '12';
	$dp_default_options['recipe_desc_font_size'] = '16';
	$dp_default_options['recipe_desc_font_size_mobile'] = '14';
	$dp_default_options['recipe_headline_color'] = '#ff7f00';
	$dp_default_options['recipe_desc_color'] = '#FFFFFF';
	$dp_default_options['recipe_bg_image'] = false;
	$dp_default_options['recipe_use_overlay'] = 1;
	$dp_default_options['recipe_overlay_color'] = '#000000';
	$dp_default_options['recipe_overlay_opacity'] = '0.5';

	// アーカイブページ
	$dp_default_options['archive_recipe_desc_font_size'] = '16';
	$dp_default_options['archive_recipe_desc_font_size_mobile'] = '14';

	// カテゴリーページ
	$dp_default_options['category_recipe_headline_font_size'] = '20';
	$dp_default_options['category_recipe_headline_font_size_mobile'] = '15';
	$dp_default_options['category_recipe_headline_label'] = __( 'recipe', 'tcd-w' );
	$dp_default_options['show_category_recipe_post_count'] = 1;
	$dp_default_options['category_recipe_post_count_label'] = __( 'recipe', 'tcd-w' );
	$dp_default_options['category_recipe_desc_font_size'] = '16';
	$dp_default_options['category_recipe_desc_font_size_mobile'] = '14';
	$dp_default_options['category_recipe_list_title_font_size'] = '16';
	$dp_default_options['category_recipe_list_title_font_size_mobile'] = '14';
	$dp_default_options['category_recipe_list_show_category'] = 1;
	$dp_default_options['category_recipe_list_show_post_view'] = 1;
	$dp_default_options['category_recipe_list_show_premium_icon'] = 1;
	$dp_default_options['category_recipe_num'] = '18';

	// 記事ページ
	$dp_default_options['single_recipe_title_font_size'] = '26';
	$dp_default_options['single_recipe_content_font_size'] = '16';
	$dp_default_options['single_recipe_title_font_size_mobile'] = '20';
	$dp_default_options['single_recipe_content_font_size_mobile'] = '14';
	$dp_default_options['single_recipe_show_author'] = 1;
	$dp_default_options['single_recipe_author_label'] = __( 'Author', 'tcd-w' );
	$dp_default_options['single_recipe_show_category'] = 1;
	$dp_default_options['single_recipe_show_post_view'] = 1;
	$dp_default_options['show_recipe_nav'] = 1;
	$dp_default_options['single_recipe_show_copy_button'] = 1;

  // LIKEボタン
	$dp_default_options['show_recipe_like_button'] = 1;
	$dp_default_options['recipe_like_button_label'] = __( 'LIKE', 'tcd-w' );
	$dp_default_options['recipe_delete_like_button_label'] = __( 'DELETE LIKE', 'tcd-w' );
	$dp_default_options['recipe_like_button_color'] = '#ff8001';

	// 関連記事
	$dp_default_options['show_related_recipe'] = 1;
	$dp_default_options['related_recipe_headline'] = __( 'Related recipe', 'tcd-w' );
	$dp_default_options['related_recipe_headline_font_size'] = '20';
	$dp_default_options['related_recipe_headline_font_size_mobile'] = '16';
	$dp_default_options['related_recipe_headline_font_color'] = '#000000';
	$dp_default_options['related_recipe_headline_bg_color'] = '#ffffff';
	$dp_default_options['related_recipe_headline_border_color'] = '#dddddd';
	$dp_default_options['hide_related_recipe_headline_icon'] = '';
	$dp_default_options['related_recipe_headline_icon_color'] = '#000000';
	$dp_default_options['related_recipe_headline_image'] = false;
	$dp_default_options['related_recipe_post_num'] = '6';
	$dp_default_options['related_recipe_list_title_font_size'] = '16';
	$dp_default_options['related_recipe_list_title_font_size_mobile'] = '14';
	$dp_default_options['related_recipe_list_show_post_view'] = 1;
	$dp_default_options['related_recipe_list_show_premium_icon'] = 1;


	return $dp_default_options;

}


// 入力欄の出力　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_recipe_tab_panel( $options ) {

  global $dp_default_options, $pagenation_type_options;
  $recipe_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );
  $recipe_category_label = $options['recipe_category_label'] ? esc_html( $options['recipe_category_label'] ) : __( 'Recipe category', 'tcd-w' );

?>

<div id="tab-content-recipe" class="tab-content">

   <?php // 基本設定 -------------------------------------------------------------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Basic setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Name of content', 'tcd-w');  ?></h4>
     <div class="theme_option_message2">
      <p><?php _e('This name will also be used in breadcrumb link and page header.', 'tcd-w'); ?></p>
     </div>
     <input class="regular-text" type="text" name="dp_options[recipe_label]" value="<?php echo esc_attr( $options['recipe_label'] ); ?>" />
     <h4 class="theme_option_headline2"><?php _e('Slug setting', 'tcd-w');  ?></h4>
     <div class="theme_option_message2">
      <p><?php _e('Please enter word by alphabet only.<br />After changing slug, please update permalink setting form <a href="./options-permalink.php"><strong>permalink option page</strong></a>.', 'tcd-w'); ?></p>
     </div>
     <p><input class="hankaku regular-text" type="text" name="dp_options[recipe_slug]" value="<?php echo sanitize_title( $options['recipe_slug'] ); ?>" /></p>
     <h4 class="theme_option_headline2"><?php printf(__('%s setting', 'tcd-w'), $recipe_category_label); ?></h4>
     <div class="theme_option_message2">
      <p><?php _e('Please enter word by alphabet only.<br />After changing slug, please update permalink setting form <a href="./options-permalink.php"><strong>permalink option page</strong></a>.', 'tcd-w'); ?></p>
     </div>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Name', 'tcd-w'); ?></span><input type="text" name="dp_options[recipe_category_label]" value="<?php esc_attr_e( $options['recipe_category_label'] ); ?>" /></li>
      <li class="cf"><span class="label"><?php _e('Slug', 'tcd-w'); ?></span><input type="text" name="dp_options[recipe_category_slug]" value="<?php echo sanitize_title( $options['recipe_category_slug'] ); ?>" /></li>
     </ul>
     <h4 class="theme_option_headline2"><?php printf(__('Premium %s icon setting', 'tcd-w'), $recipe_label); ?></h4>
     <div class="theme_option_message2">
      <p><?php _e('Upload your original image, if you want to change the icon.', 'tcd-w'); ?></p>
      <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
     </div>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="dp_options[recipe_premium_icon_color]" value="<?php echo esc_attr( $options['recipe_premium_icon_color'] ); ?>" data-default-color="#bcab4a" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Icon image', 'tcd-w');  ?></span>
       <div class="image_box cf">
        <div class="cf cf_media_field hide-if-no-js recipe_premium_icon_image">
         <input type="hidden" value="<?php echo esc_attr( $options['recipe_premium_icon_image'] ); ?>" id="recipe_premium_icon_image" name="dp_options[recipe_premium_icon_image]" class="cf_media_id">
         <div class="preview_field"><?php if($options['recipe_premium_icon_image']){ echo wp_get_attachment_image($options['recipe_premium_icon_image'], 'full'); }; ?></div>
         <div class="buttton_area">
          <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
          <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['recipe_premium_icon_image']){ echo 'hidden'; }; ?>">
         </div>
        </div>
       </div>
      </li>
     </ul>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

   <?php // ヘッダーの設定 ----------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php printf(__('%s archive page header setting (First level)', 'tcd-w'), $recipe_label); ?></h3>
    <div class="theme_option_field_ac_content">
     <div class="theme_option_message2">
      <p><?php _e('You can set category page header from <a target="_blank" href="./edit-tags.php?taxonomy=recipe_category&post_type=recipe">category edit page</a>.', 'tcd-w'); ?></p>
     </div>
     <h4 class="theme_option_headline2"><?php _e('Headline', 'tcd-w');  ?></h4>
     <textarea class="large-text" cols="50" rows="2" name="dp_options[recipe_headline]"><?php echo esc_textarea(  $options['recipe_headline'] ); ?></textarea>
     <h4 class="theme_option_headline2"><?php _e('Description', 'tcd-w');  ?></h4>
     <textarea class="large-text" cols="50" rows="3" name="dp_options[recipe_desc]"><?php echo esc_textarea(  $options['recipe_desc'] ); ?></textarea>
     <h4 class="theme_option_headline2"><?php _e('Font size setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[recipe_headline_font_size]" value="<?php esc_attr_e( $options['recipe_headline_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Description', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[recipe_desc_font_size]" value="<?php esc_attr_e( $options['recipe_desc_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Headline (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[recipe_headline_font_size_mobile]" value="<?php esc_attr_e( $options['recipe_headline_font_size_mobile'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Description (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[recipe_desc_font_size_mobile]" value="<?php esc_attr_e( $options['recipe_desc_font_size_mobile'] ); ?>" /><span>px</span></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e('Color setting', 'tcd-w');  ?></h4>
     <div class="theme_option_message2">
      <p><?php _e('Background color will be used if background image is not registered.', 'tcd-w'); ?></p>
     </div>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[recipe_headline_color]" value="<?php echo esc_attr( $options['recipe_headline_color'] ); ?>" data-default-color="#ff7f00" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Font color of description', 'tcd-w'); ?></span><input type="text" name="dp_options[recipe_desc_color]" value="<?php echo esc_attr( $options['recipe_desc_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e('Background image', 'tcd-w'); ?></h4>
     <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '810', '455'); ?></p>
     <div class="image_box cf">
      <div class="cf cf_media_field hide-if-no-js recipe_bg_image">
       <input type="hidden" value="<?php echo esc_attr( $options['recipe_bg_image'] ); ?>" id="recipe_bg_image" name="dp_options[recipe_bg_image]" class="cf_media_id">
       <div class="preview_field"><?php if($options['recipe_bg_image']){ echo wp_get_attachment_image($options['recipe_bg_image'], 'medium'); }; ?></div>
       <div class="buttton_area">
        <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
        <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['recipe_bg_image']){ echo 'hidden'; }; ?>">
       </div>
      </div>
     </div>
     <h4 class="theme_option_headline2"><?php _e( 'Overlay setting', 'tcd-w' ); ?></h4>
     <p class="displayment_checkbox"><label><input name="dp_options[recipe_use_overlay]" type="checkbox" value="1" <?php checked( $options['recipe_use_overlay'], 1 ); ?>><?php _e( 'Use overlay', 'tcd-w' ); ?></label></p>
     <div class="recipe_show_overlay" style="<?php if($options['recipe_use_overlay'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
       <li class="cf"><span class="label"><?php _e('Color of overlay', 'tcd-w'); ?></span><input type="text" name="dp_options[recipe_overlay_color]" value="<?php echo esc_attr( $options['recipe_overlay_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
       <li class="cf">
        <span class="label"><?php _e('Transparency of overlay', 'tcd-w'); ?></span><input class="hankaku" style="width:70px;" type="number" max="1" min="0" step="0.1" name="dp_options[recipe_overlay_opacity]" value="<?php echo esc_attr( $options['recipe_overlay_opacity'] ); ?>" />
        <div class="theme_option_message2">
         <p><?php _e('Please specify the number of 0.1 from 0.9. Overlay color will be more transparent as the number is small.', 'tcd-w');  ?></p>
        </div>
       </li>
      </ul>
     </div>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

   <?php // アーカイブページの設定 ----------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php printf(__('%s archive page setting (First level)', 'tcd-w'), $recipe_label); ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Category list setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Font size of description', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[archive_recipe_desc_font_size]" value="<?php esc_attr_e( $options['archive_recipe_desc_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Font size of description (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[archive_recipe_desc_font_size_mobile]" value="<?php esc_attr_e( $options['archive_recipe_desc_font_size_mobile'] ); ?>" /><span>px</span></li>
     </ul>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

   <?php // カテゴリーページの設定 ----------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Category page setting (Second, Third level)', 'tcd-w'); ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Parent category page headline setting (Second level)', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Font size of headline', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[category_recipe_headline_font_size]" value="<?php esc_attr_e( $options['category_recipe_headline_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Font size of headline (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[category_recipe_headline_font_size_mobile]" value="<?php esc_attr_e( $options['category_recipe_headline_font_size_mobile'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('label of headline', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[category_recipe_headline_label]" value="<?php echo esc_attr( $options['category_recipe_headline_label'] ); ?>" /></li>
      <li class="cf"><span class="label"><?php _e('Display total post', 'tcd-w'); ?></span><input name="dp_options[show_category_recipe_post_count]" type="checkbox" value="1" <?php checked( $options['show_category_recipe_post_count'], 1 ); ?>></li>
      <li class="cf"><span class="label"><?php _e('Label of total count', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[category_recipe_post_count_label]" value="<?php echo esc_attr( $options['category_recipe_post_count_label'] ); ?>" /></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e('Child category description setting (Third level)', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Font size of description', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[category_recipe_desc_font_size]" value="<?php esc_attr_e( $options['category_recipe_desc_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Font size of description (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[category_recipe_desc_font_size_mobile]" value="<?php esc_attr_e( $options['category_recipe_desc_font_size_mobile'] ); ?>" /><span>px</span></li>
     </ul>
     <h4 class="theme_option_headline2"><?php printf(__('%s list setting (Second, Third level)', 'tcd-w'), $recipe_label); ?></h4>
     <ul class="option_list">
      <li class="cf">
       <span class="label"><?php _e('Number of post to display', 'tcd-w'); ?></span>
       <select name="dp_options[category_recipe_num]">
        <?php for($i=12; $i<= 30; $i++): if( ($i % 3) == 0 ){ ?>
        <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $options['category_recipe_num'], $i ); ?>><?php echo esc_html($i); ?></option>
        <?php }; endfor; ?>
       </select>
      </li>
      <li class="cf"><span class="label"><?php _e('Font size of title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[category_recipe_list_title_font_size]" value="<?php esc_attr_e( $options['category_recipe_list_title_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Font size of title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[category_recipe_list_title_font_size_mobile]" value="<?php esc_attr_e( $options['category_recipe_list_title_font_size_mobile'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php printf(__('Display premium %s icon', 'tcd-w'), $recipe_label); ?></span><input name="dp_options[category_recipe_list_show_premium_icon]" type="checkbox" value="1" <?php checked( '1', $options['category_recipe_list_show_premium_icon'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Display child category in parent category page', 'tcd-w');  ?></span><input name="dp_options[category_recipe_list_show_category]" type="checkbox" value="1" <?php checked( '1', $options['category_recipe_list_show_category'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Display post view count in child category page', 'tcd-w');  ?></span><input name="dp_options[category_recipe_list_show_post_view]" type="checkbox" value="1" <?php checked( '1', $options['category_recipe_list_show_post_view'] ); ?> /></li>
     </ul>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

   <?php // 記事ページの設定 -------------------------------------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Single page setting (Fourth level)', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Font size setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[single_recipe_title_font_size]" value="<?php esc_attr_e( $options['single_recipe_title_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Content', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[single_recipe_content_font_size]" value="<?php esc_attr_e( $options['single_recipe_content_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Title (mobile)', 'tcd-w');  ?></span><input class="font_size hankaku" type="text" name="dp_options[single_recipe_title_font_size_mobile]" value="<?php esc_attr_e( $options['single_recipe_title_font_size_mobile'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Content (mobile)', 'tcd-w');  ?></span><input class="font_size hankaku" type="text" name="dp_options[single_recipe_content_font_size_mobile]" value="<?php esc_attr_e( $options['single_recipe_content_font_size_mobile'] ); ?>" /><span>px</span></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e('Display setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Display author under title', 'tcd-w');  ?></span><input name="dp_options[single_recipe_show_author]" type="checkbox" value="1" <?php checked( '1', $options['single_recipe_show_author'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Label of author', 'tcd-w');  ?></span><input class="full_width" type="text" name="dp_options[single_recipe_author_label]" value="<?php esc_attr_e( $options['single_recipe_author_label'] ); ?>" /></li>
      <li class="cf"><span class="label"><?php _e('Display child category under title', 'tcd-w');  ?></span><input name="dp_options[single_recipe_show_category]" type="checkbox" value="1" <?php checked( '1', $options['single_recipe_show_category'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Display post view count under title', 'tcd-w');  ?></span><input name="dp_options[single_recipe_show_post_view]" type="checkbox" value="1" <?php checked( '1', $options['single_recipe_show_post_view'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Display copy URL and title button under image', 'tcd-w');  ?></span><input name="dp_options[single_recipe_show_copy_button]" type="checkbox" value="1" <?php checked( '1', $options['single_recipe_show_copy_button'] ); ?> /></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e('LIKE button setting', 'tcd-w');  ?></h4>
     <p class="displayment_checkbox"><label><input name="dp_options[show_recipe_like_button]" type="checkbox" value="1" <?php checked( $options['show_recipe_like_button'], 1 ); ?>><?php _e('Display like button', 'tcd-w'); ?></label></p>
     <div style="<?php if($options['show_recipe_like_button'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
       <li class="cf"><span class="label"><?php _e('Label for like button', 'tcd-w');  ?></span><input class="full_width" type="text" name="dp_options[recipe_like_button_label]" value="<?php esc_attr_e( $options['recipe_like_button_label'] ); ?>" /></li>
       <li class="cf"><span class="label"><?php _e('Label for delete like button', 'tcd-w');  ?></span><input class="full_width" type="text" name="dp_options[recipe_delete_like_button_label]" value="<?php esc_attr_e( $options['recipe_delete_like_button_label'] ); ?>" /></li>
       <li class="cf"><span class="label"><?php _e('Color', 'tcd-w'); ?></span><input type="text" name="dp_options[recipe_like_button_color]" value="<?php echo esc_attr( $options['recipe_like_button_color'] ); ?>" data-default-color="#ff8001" class="c-color-picker"></li>
      </ul>
     </div>
     <h4 class="theme_option_headline2"><?php printf(__('Related %s setting', 'tcd-w'), $recipe_label); ?></h4>
     <p class="displayment_checkbox"><label><input name="dp_options[show_related_recipe]" type="checkbox" value="1" <?php checked( $options['show_related_recipe'], 1 ); ?>><?php printf(__('Display related %s', 'tcd-w'), $recipe_label); ?></label></p>
     <div style="<?php if($options['show_related_recipe'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
       <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w');  ?></span><input class="full_width" type="text" name="dp_options[related_recipe_headline]" value="<?php esc_attr_e( $options['related_recipe_headline'] ); ?>" /></li>
       <li class="cf"><span class="label"><?php _e('Font size of headline', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[related_recipe_headline_font_size]" value="<?php esc_attr_e( $options['related_recipe_headline_font_size'] ); ?>" /><span>px</span></li>
       <li class="cf"><span class="label"><?php _e('Font size of headline (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[related_recipe_headline_font_size_mobile]" value="<?php esc_attr_e( $options['related_recipe_headline_font_size_mobile'] ); ?>" /><span>px</span></li>
       <li class="cf"><span class="label"><?php _e('Font color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[related_recipe_headline_font_color]" value="<?php echo esc_attr( $options['related_recipe_headline_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
       <li class="cf"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[related_recipe_headline_bg_color]" value="<?php echo esc_attr( $options['related_recipe_headline_bg_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
       <li class="cf"><span class="label"><?php _e('Border color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[related_recipe_headline_border_color]" value="<?php echo esc_attr( $options['related_recipe_headline_border_color'] ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
       <li class="cf"><span class="label"><?php _e('Hide headline icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="dp_options[hide_related_recipe_headline_icon]" value="1" <?php checked( $options['hide_related_recipe_headline_icon'], 1 ); ?>></li>
      </ul>
      <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($options['hide_related_recipe_headline_icon']) { echo ' display:none;'; }; ?>">
       <li class="cf"><span class="label"><?php _e('Background color of headline icon', 'tcd-w'); ?></span><input type="text" name="dp_options[related_recipe_headline_icon_color]" value="<?php echo esc_attr( $options['related_recipe_headline_icon_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
       <li class="cf"><span class="label"><?php _e('Headline icon image', 'tcd-w');  ?></span>
        <div class="image_box cf">
         <div class="cf cf_media_field hide-if-no-js related_recipe_headline_image">
          <input type="hidden" value="<?php echo esc_attr( $options['related_recipe_headline_image'] ); ?>" id="related_recipe_headline_image" name="dp_options[related_recipe_headline_image]" class="cf_media_id">
          <div class="preview_field"><?php if($options['related_recipe_headline_image']){ echo wp_get_attachment_image($options['related_recipe_headline_image'], 'full'); }; ?></div>
          <div class="buttton_area">
           <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
           <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['related_recipe_headline_image']){ echo 'hidden'; }; ?>">
          </div>
         </div>
        </div>
        <div class="theme_option_message2">
         <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
         <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
        </div>
       </li>
      </ul>
      <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;">
       <li class="cf"><span class="label"><?php _e('Number of post to display', 'tcd-w');  ?></span>
        <select name="dp_options[related_recipe_post_num]">
         <?php for($i=3; $i<= 12; $i++): ?>
         <?php if( $i % 3 == 0 ){ ?>
         <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $options['related_recipe_post_num'], $i ); ?>><?php echo esc_html($i); ?></option>
         <?php }; ?>
         <?php endfor; ?>
        </select>
       </li>
       <li class="cf"><span class="label"><?php _e('Font size of title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[related_recipe_list_title_font_size]" value="<?php esc_attr_e( $options['related_recipe_list_title_font_size'] ); ?>" /><span>px</span></li>
       <li class="cf"><span class="label"><?php _e('Font size of title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[related_recipe_list_title_font_size_mobile]" value="<?php esc_attr_e( $options['related_recipe_list_title_font_size_mobile'] ); ?>" /><span>px</span></li>
       <li class="cf"><span class="label"><?php printf(__('Display premium %s icon', 'tcd-w'), $recipe_label); ?></span><input name="dp_options[related_recipe_list_show_premium_icon]" type="checkbox" value="1" <?php checked( '1', $options['related_recipe_list_show_premium_icon'] ); ?> /></li>
       <li class="cf"><span class="label"><?php _e('Display post view count', 'tcd-w');  ?></span><input name="dp_options[related_recipe_list_show_post_view]" type="checkbox" value="1" <?php checked( '1', $options['related_recipe_list_show_post_view'] ); ?> /></li>
      </ul>
     </div>
     <h4 class="theme_option_headline2"><?php _e('Other setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Display next previous post link', 'tcd-w');  ?></span><input name="dp_options[show_recipe_nav]" type="checkbox" value="1" <?php checked( '1', $options['show_recipe_nav'] ); ?> /></li>
     </ul>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


</div><!-- END .tab-content -->

<?php
} // END add_recipe_tab_panel()


// バリデーション　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_recipe_theme_options_validate( $input ) {

  global $dp_default_options, $pagenation_type_options, $recipe_list_animation_type_options;

  // 基本設定
  $input['recipe_label'] = wp_filter_nohtml_kses( $input['recipe_label'] );
  $input['recipe_category_label'] = wp_filter_nohtml_kses( $input['recipe_category_label'] );
  $input['recipe_category_slug'] = sanitize_title( $input['recipe_category_slug'] );
  $input['recipe_premium_icon_color'] = wp_filter_nohtml_kses( $input['recipe_premium_icon_color'] );
  $input['recipe_premium_icon_image'] = wp_filter_nohtml_kses( $input['recipe_premium_icon_image'] );

  //ヘッダーの設定
  $input['recipe_headline'] = wp_filter_nohtml_kses( $input['recipe_headline'] );
  $input['recipe_desc'] = wp_filter_nohtml_kses( $input['recipe_desc'] );
  $input['recipe_headline_font_size'] = wp_filter_nohtml_kses( $input['recipe_headline_font_size'] );
  $input['recipe_headline_font_size_mobile'] = wp_filter_nohtml_kses( $input['recipe_headline_font_size_mobile'] );
  $input['recipe_desc_font_size'] = wp_filter_nohtml_kses( $input['recipe_desc_font_size'] );
  $input['recipe_desc_font_size_mobile'] = wp_filter_nohtml_kses( $input['recipe_desc_font_size_mobile'] );
  $input['recipe_headline_color'] = wp_filter_nohtml_kses( $input['recipe_headline_color'] );
  $input['recipe_desc_color'] = wp_filter_nohtml_kses( $input['recipe_desc_color'] );
  $input['recipe_bg_image'] = wp_filter_nohtml_kses( $input['recipe_bg_image'] );
  if ( ! isset( $input['recipe_use_overlay'] ) )
    $input['recipe_use_overlay'] = null;
    $input['recipe_use_overlay'] = ( $input['recipe_use_overlay'] == 1 ? 1 : 0 );
  $input['recipe_overlay_color'] = wp_filter_nohtml_kses( $input['recipe_overlay_color'] );
  $input['recipe_overlay_opacity'] = wp_filter_nohtml_kses( $input['recipe_overlay_opacity'] );

  // アーカイブ・カテゴリーページ
  $input['archive_recipe_desc_font_size'] = wp_filter_nohtml_kses( $input['archive_recipe_desc_font_size'] );
  $input['archive_recipe_desc_font_size_mobile'] = wp_filter_nohtml_kses( $input['archive_recipe_desc_font_size_mobile'] );
  $input['category_recipe_headline_font_size'] = wp_filter_nohtml_kses( $input['category_recipe_headline_font_size'] );
  $input['category_recipe_headline_font_size_mobile'] = wp_filter_nohtml_kses( $input['category_recipe_headline_font_size_mobile'] );
  $input['category_recipe_headline_label'] = wp_filter_nohtml_kses( $input['category_recipe_headline_label'] );
  $input['category_recipe_post_count_label'] = wp_filter_nohtml_kses( $input['category_recipe_post_count_label'] );
  if ( ! isset( $input['show_category_recipe_post_count'] ) )
    $input['show_category_recipe_post_count'] = null;
    $input['show_category_recipe_post_count'] = ( $input['show_category_recipe_post_count'] == 1 ? 1 : 0 );
  $input['category_recipe_desc_font_size'] = wp_filter_nohtml_kses( $input['category_recipe_desc_font_size'] );
  $input['category_recipe_desc_font_size_mobile'] = wp_filter_nohtml_kses( $input['category_recipe_desc_font_size_mobile'] );
  $input['category_recipe_list_title_font_size'] = wp_filter_nohtml_kses( $input['category_recipe_list_title_font_size'] );
  $input['category_recipe_list_title_font_size_mobile'] = wp_filter_nohtml_kses( $input['category_recipe_list_title_font_size_mobile'] );
  if ( ! isset( $input['category_recipe_list_show_category'] ) )
    $input['category_recipe_list_show_category'] = null;
    $input['category_recipe_list_show_category'] = ( $input['category_recipe_list_show_category'] == 1 ? 1 : 0 );
  if ( ! isset( $input['category_recipe_list_show_post_view'] ) )
    $input['category_recipe_list_show_post_view'] = null;
    $input['category_recipe_list_show_post_view'] = ( $input['category_recipe_list_show_post_view'] == 1 ? 1 : 0 );
  if ( ! isset( $input['category_recipe_list_show_premium_icon'] ) )
    $input['category_recipe_list_show_premium_icon'] = null;
    $input['category_recipe_list_show_premium_icon'] = ( $input['category_recipe_list_show_premium_icon'] == 1 ? 1 : 0 );
  $input['category_recipe_num'] = wp_filter_nohtml_kses( $input['category_recipe_num'] );


  // 記事ページ
  $input['single_recipe_title_font_size'] = wp_filter_nohtml_kses( $input['single_recipe_title_font_size'] );
  $input['single_recipe_content_font_size'] = wp_filter_nohtml_kses( $input['single_recipe_content_font_size'] );
  $input['single_recipe_title_font_size_mobile'] = wp_filter_nohtml_kses( $input['single_recipe_title_font_size_mobile'] );
  $input['single_recipe_content_font_size_mobile'] = wp_filter_nohtml_kses( $input['single_recipe_content_font_size_mobile'] );
  if ( ! isset( $input['show_recipe_nav'] ) )
    $input['show_recipe_nav'] = null;
    $input['show_recipe_nav'] = ( $input['show_recipe_nav'] == 1 ? 1 : 0 );
  if ( ! isset( $input['single_recipe_show_author'] ) )
    $input['single_recipe_show_author'] = null;
    $input['single_recipe_show_author'] = ( $input['single_recipe_show_author'] == 1 ? 1 : 0 );
  $input['single_recipe_author_label'] = wp_filter_nohtml_kses( $input['single_recipe_author_label'] );
  if ( ! isset( $input['single_recipe_show_category'] ) )
    $input['single_recipe_show_category'] = null;
    $input['single_recipe_show_category'] = ( $input['single_recipe_show_category'] == 1 ? 1 : 0 );
  if ( ! isset( $input['single_recipe_show_post_view'] ) )
    $input['single_recipe_show_post_view'] = null;
    $input['single_recipe_show_post_view'] = ( $input['single_recipe_show_post_view'] == 1 ? 1 : 0 );
  if ( ! isset( $input['single_recipe_show_copy_button'] ) )
    $input['single_recipe_show_copy_button'] = null;
    $input['single_recipe_show_copy_button'] = ( $input['single_recipe_show_copy_button'] == 1 ? 1 : 0 );


  // LIKEボタン
  if ( ! isset( $input['show_recipe_like_button'] ) )
    $input['show_recipe_like_button'] = null;
    $input['show_recipe_like_button'] = ( $input['show_recipe_like_button'] == 1 ? 1 : 0 );
  $input['recipe_like_button_label'] = wp_filter_nohtml_kses( $input['recipe_like_button_label'] );
  $input['recipe_delete_like_button_label'] = wp_filter_nohtml_kses( $input['recipe_delete_like_button_label'] );
  $input['recipe_like_button_color'] = wp_filter_nohtml_kses( $input['recipe_like_button_color'] );

  // 関連記事
  if ( ! isset( $input['show_related_recipe'] ) )
    $input['show_related_recipe'] = null;
    $input['show_related_recipe'] = ( $input['show_related_recipe'] == 1 ? 1 : 0 );
  $input['related_recipe_headline'] = wp_filter_nohtml_kses( $input['related_recipe_headline'] );
  $input['related_recipe_headline_font_size'] = wp_filter_nohtml_kses( $input['related_recipe_headline_font_size'] );
  $input['related_recipe_headline_font_size_mobile'] = wp_filter_nohtml_kses( $input['related_recipe_headline_font_size_mobile'] );
  $input['related_recipe_headline_font_color'] = wp_filter_nohtml_kses( $input['related_recipe_headline_font_color'] );
  $input['related_recipe_headline_bg_color'] = wp_filter_nohtml_kses( $input['related_recipe_headline_bg_color'] );
  $input['related_recipe_headline_border_color'] = wp_filter_nohtml_kses( $input['related_recipe_headline_border_color'] );
  $input['related_recipe_headline_icon_color'] = wp_filter_nohtml_kses( $input['related_recipe_headline_icon_color'] );
  if ( ! isset( $input['hide_related_recipe_headline_icon'] ) )
    $input['hide_related_recipe_headline_icon'] = null;
    $input['hide_related_recipe_headline_icon'] = ( $input['hide_related_recipe_headline_icon'] == 1 ? 1 : 0 );
  $input['recipe_premium_icon_image'] = wp_filter_nohtml_kses( $input['recipe_premium_icon_image'] );
  $input['related_recipe_post_num'] = wp_filter_nohtml_kses( $input['related_recipe_post_num'] );
  $input['related_recipe_list_title_font_size'] = wp_filter_nohtml_kses( $input['related_recipe_list_title_font_size'] );
  $input['related_recipe_list_title_font_size_mobile'] = wp_filter_nohtml_kses( $input['related_recipe_list_title_font_size_mobile'] );
  if ( ! isset( $input['related_recipe_list_show_post_view'] ) )
    $input['related_recipe_list_show_post_view'] = null;
    $input['related_recipe_list_show_post_view'] = ( $input['related_recipe_list_show_post_view'] == 1 ? 1 : 0 );
  if ( ! isset( $input['related_recipe_list_show_premium_icon'] ) )
    $input['related_recipe_list_show_premium_icon'] = null;
    $input['related_recipe_list_show_premium_icon'] = ( $input['related_recipe_list_show_premium_icon'] == 1 ? 1 : 0 );


	return $input;

};


?>
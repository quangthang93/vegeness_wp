<?php
/*
 * お知らせの設定
 */


// Add default values
add_filter( 'before_getting_design_plus_option', 'add_news_dp_default_options' );


// Add label of logo tab
add_action( 'tcd_tab_labels', 'add_news_tab_label' );


// Add HTML of logo tab
add_action( 'tcd_tab_panel', 'add_news_tab_panel' );


// Register sanitize function
add_filter( 'theme_options_validate', 'add_news_theme_options_validate' );


// タブの名前
function add_news_tab_label( $tab_labels ) {
  $options = get_design_plus_option();
  $tab_label = $options['news_label'] ? esc_html( $options['news_label'] ) : __( 'News', 'tcd-w' );
  $tab_labels['news'] = $tab_label;
  return $tab_labels;
}


// 初期値
function add_news_dp_default_options( $dp_default_options ) {

	// 基本設定
	$dp_default_options['news_label'] = __( 'News', 'tcd-w' );
	$dp_default_options['news_slug'] = 'news';

	// アーカイブページ
	$dp_default_options['archive_news_headline_font_size'] = '20';
	$dp_default_options['archive_news_headline_font_size_mobile'] = '15';
	$dp_default_options['archive_news_headline_font_color'] = '#000000';
	$dp_default_options['archive_news_headline_bg_color'] = '#ffffff';
	$dp_default_options['archive_news_headline_border_color'] = '#dddddd';
	$dp_default_options['hide_archive_news_headline_icon'] = '';
	$dp_default_options['archive_news_headline_icon_color'] = '#000000';
	$dp_default_options['archive_news_headline_image'] = false;
	$dp_default_options['archive_news_title_font_size'] = '18';
	$dp_default_options['archive_news_title_font_size_mobile'] = '14';
	$dp_default_options['show_archive_news_date'] = 1;
	$dp_default_options['news_no_image'] = 'display';
	$dp_default_options['archive_news_num'] = '15';

	// 詳細ページ
	$dp_default_options['single_news_title_font_size'] = '38';
	$dp_default_options['single_news_content_font_size'] = '16';
	$dp_default_options['single_news_title_font_size_mobile'] = '20';
	$dp_default_options['single_news_content_font_size_mobile'] = '13';
	$dp_default_options['show_news_image'] = 1;
	$dp_default_options['show_news_date'] = 1;
	$dp_default_options['show_news_category'] = 1;
	$dp_default_options['show_sns_top_news'] = 1;
	$dp_default_options['show_sns_btm_news'] = 1;
	$dp_default_options['show_copy_top_news'] = 1;
	$dp_default_options['show_copy_btm_news'] = 1;
	$dp_default_options['show_news_nav'] = 1;

	// 最新のお知らせ一覧
	$dp_default_options['show_recent_news'] = 1;
	$dp_default_options['recent_news_headline'] = __( 'Recent news', 'tcd-w' );
	$dp_default_options['recent_news_link_label'] = __( 'News list', 'tcd-w' );
	$dp_default_options['recent_news_headline_font_size'] = '20';
	$dp_default_options['recent_news_headline_font_size_mobile'] = '15';
	$dp_default_options['recent_news_headline_font_color'] = '#000000';
	$dp_default_options['recent_news_headline_bg_color'] = '#ffffff';
	$dp_default_options['recent_news_headline_border_color'] = '#dddddd';
	$dp_default_options['hide_recent_news_headline_icon'] = '';
	$dp_default_options['recent_news_headline_icon_color'] = '#000000';
	$dp_default_options['recent_news_headline_image'] = false;
	$dp_default_options['show_recent_news_date'] = 1;
	$dp_default_options['show_recent_news_link'] = 1;
	$dp_default_options['recent_news_num'] = '5';

	// 広告
	for ( $i = 1; $i <= 2; $i++ ) {
		$dp_default_options['news_single_top_ad_code' . $i] = '';
		$dp_default_options['news_single_top_ad_image' . $i] = false;
		$dp_default_options['news_single_top_ad_url' . $i] = '';
	}
	for ( $i = 1; $i <= 2; $i++ ) {
		$dp_default_options['news_single_bottom_ad_code' . $i] = '';
		$dp_default_options['news_single_bottom_ad_image' . $i] = false;
		$dp_default_options['news_single_bottom_ad_url' . $i] = '';
	}
	for ( $i = 1; $i <= 2; $i++ ) {
		$dp_default_options['news_single_shortcode_ad_code' . $i] = '';
		$dp_default_options['news_single_shortcode_ad_image' . $i] = false;
		$dp_default_options['news_single_shortcode_ad_url' . $i] = '';
	}
	for ( $i = 1; $i <= 2; $i++ ) {
		$dp_default_options['news_single_mobile_ad_code' . $i] = '';
		$dp_default_options['news_single_mobile_ad_image' . $i] = false;
		$dp_default_options['news_single_mobile_ad_url' . $i] = '';
	}

	return $dp_default_options;

}


// 入力欄の出力　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_news_tab_panel( $options ) {

  global $dp_default_options, $no_image_options;
  $news_label = $options['news_label'] ? esc_html( $options['news_label'] ) : __( 'News', 'tcd-w' );

?>

<div id="tab-content-news" class="tab-content">


   <?php // 基本設定 -------------------------------------------------------------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Basic setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Name of content', 'tcd-w');  ?></h4>
     <div class="theme_option_message2">
      <p><?php _e('This name will also be used in breadcrumb link.', 'tcd-w'); ?></p>
     </div>
     <input id="dp_options[news_label]" class="regular-text" type="text" name="dp_options[news_label]" value="<?php echo esc_attr( $options['news_label'] ); ?>" />
     <h4 class="theme_option_headline2"><?php _e('Slug setting', 'tcd-w');  ?></h4>
     <div class="theme_option_message2">
      <p><?php _e('Please enter word by alphabet only.<br />After changing slug, please update permalink setting form <a href="./options-permalink.php"><strong>permalink option page</strong></a>.', 'tcd-w'); ?></p>
     </div>
     <p><input id="dp_options[news_slug]" class="hankaku regular-text" type="text" name="dp_options[news_slug]" value="<?php echo sanitize_title( $options['news_slug'] ); ?>" /></p>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // アーカイブページの設定 ----------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Archive page setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Font size of headline', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[archive_news_headline_font_size]" value="<?php esc_attr_e( $options['archive_news_headline_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Font size of headline (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[archive_news_headline_font_size_mobile]" value="<?php esc_attr_e( $options['archive_news_headline_font_size_mobile'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Font color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[archive_news_headline_font_color]" value="<?php echo esc_attr( $options['archive_news_headline_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[archive_news_headline_bg_color]" value="<?php echo esc_attr( $options['archive_news_headline_bg_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Border color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[archive_news_headline_border_color]" value="<?php echo esc_attr( $options['archive_news_headline_border_color'] ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Hide headline icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="dp_options[hide_archive_news_headline_icon]" value="1" <?php checked( $options['hide_archive_news_headline_icon'], 1 ); ?>></li>
     </ul>
     <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($options['hide_archive_news_headline_icon']) { echo ' display:none;'; }; ?>">
      <li class="cf"><span class="label"><?php _e('Background color of headline icon', 'tcd-w'); ?></span><input type="text" name="dp_options[archive_news_headline_icon_color]" value="<?php echo esc_attr( $options['archive_news_headline_icon_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Headline icon image', 'tcd-w');  ?></span>
       <div class="image_box cf">
        <div class="cf cf_media_field hide-if-no-js archive_news_headline_image">
         <input type="hidden" value="<?php echo esc_attr( $options['archive_news_headline_image'] ); ?>" id="archive_news_headline_image" name="dp_options[archive_news_headline_image]" class="cf_media_id">
         <div class="preview_field"><?php if($options['archive_news_headline_image']){ echo wp_get_attachment_image($options['archive_news_headline_image'], 'full'); }; ?></div>
         <div class="buttton_area">
          <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
          <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['archive_news_headline_image']){ echo 'hidden'; }; ?>">
         </div>
        </div>
       </div>
       <div class="theme_option_message2">
        <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
        <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
       </div>
      </li>
     </ul>
     <h4 class="theme_option_headline2"><?php printf(__('%s list setting', 'tcd-w'), $news_label); ?></h4>
     <ul class="option_list">
      <li class="cf">
       <span class="label"><?php _e('Number of post to display', 'tcd-w'); ?></span>
       <select name="dp_options[archive_news_num]">
        <?php for($i=10; $i<= 20; $i++): ?>
        <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $options['archive_news_num'], $i ); ?>><?php echo esc_html($i); ?></option>
        <?php endfor; ?>
       </select>
      </li>
      <li class="cf"><span class="label"><?php _e('Font size of title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[archive_news_title_font_size]" value="<?php esc_attr_e( $options['archive_news_title_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Font size of title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[archive_news_title_font_size_mobile]" value="<?php esc_attr_e( $options['archive_news_title_font_size_mobile'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Display date', 'tcd-w'); ?></span><input name="dp_options[show_archive_news_date]" type="checkbox" value="1" <?php checked( '1', $options['show_archive_news_date'] ); ?> /></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e('When the featured image is not registered', 'tcd-w');  ?></h4>
     <ul class="design_radio_button">
      <?php foreach ( $no_image_options as $option ) : ?>
      <li>
       <input type="radio" id="news_no_image_<?php esc_attr_e( $option['value'] ); ?>" name="dp_options[news_no_image]" value="<?php echo esc_attr( $option['value'] ); ?>" <?php checked( $option['value'], $options['news_no_image'] ); ?>>
       <label for="news_no_image_<?php esc_attr_e( $option['value'] ); ?>">
        <span><?php echo esc_html( $option['label'] ); ?></span>
       </label>
      </li>
      <?php endforeach; ?>
     </ul>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // 詳細ページの設定 ----------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Single page setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Font size setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[single_news_title_font_size]" value="<?php esc_attr_e( $options['single_news_title_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Content', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[single_news_content_font_size]" value="<?php esc_attr_e( $options['single_news_content_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Title (mobile)', 'tcd-w');  ?></span><input class="font_size hankaku" type="text" name="dp_options[single_news_title_font_size_mobile]" value="<?php esc_attr_e( $options['single_news_title_font_size_mobile'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Content (mobile)', 'tcd-w');  ?></span><input class="font_size hankaku" type="text" name="dp_options[single_news_content_font_size_mobile]" value="<?php esc_attr_e( $options['single_news_content_font_size_mobile'] ); ?>" /><span>px</span></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e('Display setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Display date under post title', 'tcd-w');  ?></span><input name="dp_options[show_news_date]" type="checkbox" value="1" <?php checked( '1', $options['show_news_date'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Display category under post title', 'tcd-w');  ?></span><input name="dp_options[show_news_category]" type="checkbox" value="1" <?php checked( '1', $options['show_news_category'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Display featured image', 'tcd-w');  ?></span><input name="dp_options[show_news_image]" type="checkbox" value="1" <?php checked( '1', $options['show_news_image'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Display next previous post link', 'tcd-w');  ?></span><input name="dp_options[show_news_nav]" type="checkbox" value="1" <?php checked( '1', $options['show_news_nav'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Display social button under featured image', 'tcd-w');  ?></span><input name="dp_options[show_sns_top_news]" type="checkbox" value="1" <?php checked( '1', $options['show_sns_top_news'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Display social button under post content', 'tcd-w');  ?></span><input name="dp_options[show_sns_btm_news]" type="checkbox" value="1" <?php checked( '1', $options['show_sns_btm_news'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Display "COPY Title&amp;URL" button under featured image', 'tcd-w');  ?></span><input name="dp_options[show_copy_top_news]" type="checkbox" value="1" <?php checked( '1', $options['show_copy_top_news'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Display "COPY Title&amp;URL" button under post content', 'tcd-w');  ?></span><input name="dp_options[show_copy_btm_news]" type="checkbox" value="1" <?php checked( '1', $options['show_copy_btm_news'] ); ?> /></li>
     </ul>
     <h4 class="theme_option_headline2"><?php printf(__('Recent %s list setting', 'tcd-w'), $news_label); ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php printf(__('Display recent %s list', 'tcd-w'), $news_label); ?></span><input name="dp_options[show_recent_news]" type="checkbox" value="1" <?php checked( '1', $options['show_recent_news'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w');  ?></span><input class="full_width" type="text" name="dp_options[recent_news_headline]" value="<?php esc_attr_e( $options['recent_news_headline'] ); ?>" /></li>
      <li class="cf"><span class="label"><?php _e('Font size of headline', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[recent_news_headline_font_size]" value="<?php esc_attr_e( $options['recent_news_headline_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Font size of headline (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[recent_news_headline_font_size_mobile]" value="<?php esc_attr_e( $options['recent_news_headline_font_size_mobile'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Font color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[recent_news_headline_font_color]" value="<?php echo esc_attr( $options['recent_news_headline_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[recent_news_headline_bg_color]" value="<?php echo esc_attr( $options['recent_news_headline_bg_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Border color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[recent_news_headline_border_color]" value="<?php echo esc_attr( $options['recent_news_headline_border_color'] ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Hide headline icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="dp_options[hide_recent_news_headline_icon]" value="1" <?php checked( $options['hide_recent_news_headline_icon'], 1 ); ?>></li>
     </ul>
     <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($options['hide_recent_news_headline_icon']) { echo ' display:none;'; }; ?>">
      <li class="cf"><span class="label"><?php _e('Background color of headline icon', 'tcd-w'); ?></span><input type="text" name="dp_options[recent_news_headline_icon_color]" value="<?php echo esc_attr( $options['recent_news_headline_icon_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Headline icon image', 'tcd-w');  ?></span>
       <div class="image_box cf">
        <div class="cf cf_media_field hide-if-no-js recent_news_headline_image">
         <input type="hidden" value="<?php echo esc_attr( $options['recent_news_headline_image'] ); ?>" id="recent_news_headline_image" name="dp_options[recent_news_headline_image]" class="cf_media_id">
         <div class="preview_field"><?php if($options['recent_news_headline_image']){ echo wp_get_attachment_image($options['recent_news_headline_image'], 'full'); }; ?></div>
         <div class="buttton_area">
          <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
          <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['recent_news_headline_image']){ echo 'hidden'; }; ?>">
         </div>
        </div>
       </div>
       <div class="theme_option_message2">
        <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
        <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
       </div>
      </li>
     </ul>
     <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;">
      <li class="cf">
       <span class="label"><?php _e('Number of post to display', 'tcd-w');  ?></span>
       <select name="dp_options[recent_news_num]">
        <?php for($i=5; $i<= 10; $i++): ?>
        <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $options['recent_news_num'], $i ); ?>><?php echo esc_html($i); ?></option>
        <?php endfor; ?>
       </select>
      </li>
      <li class="cf"><span class="label"><?php _e('Display date', 'tcd-w'); ?></span><input name="dp_options[show_recent_news_date]" type="checkbox" value="1" <?php checked( '1', $options['show_recent_news_date'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Display archive link', 'tcd-w'); ?></span><input name="dp_options[show_recent_news_link]" type="checkbox" value="1" <?php checked( '1', $options['show_recent_news_link'] ); ?> /></li>
      <li class="cf"><span class="label"><?php _e('Label of archive link', 'tcd-w');  ?></span><input class="full_width" type="text" name="dp_options[recent_news_link_label]" value="<?php esc_attr_e( $options['recent_news_link_label'] ); ?>" /></li>
     </ul>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // 広告の登録（アイキャッチ画像の下） -------------------------------------------------------------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Banner setting (Under featured image)', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <p><?php _e('This banner will be displayed after featured image.', 'tcd-w');  ?></p>
     <?php for($i = 1; $i <= 2; $i++) : ?>
     <div class="sub_box cf">
      <h3 class="theme_option_subbox_headline"><?php if($i == 1) { ?><?php _e('Left banner', 'tcd-w');  ?><?php } else { ?><?php _e('Right banner', 'tcd-w');  ?><?php }; ?></h3>
      <div class="sub_box_content">
       <h4 class="theme_option_headline2"><?php _e('Banner code', 'tcd-w');  ?></h4>
       <p><?php _e('If you are using google adsense, enter all code below.', 'tcd-w');  ?></p>
       <textarea id="dp_options[news_single_top_ad_code<?php echo $i; ?>]" class="large-text" cols="50" rows="10" name="dp_options[news_single_top_ad_code<?php echo $i; ?>]"><?php echo esc_textarea( $options['news_single_top_ad_code'.$i] ); ?></textarea>
       <div class="theme_option_message">
        <p><?php _e('If you are not using google adsense, you can register your banner image and affiliate code individually.', 'tcd-w');  ?></p>
       </div>
       <h4 class="theme_option_headline2"><?php _e('Register banner image.', 'tcd-w'); ?></h4>
       <p><?php _e('Recommend image size. Width:300px Height:250px', 'tcd-w'); ?></p>
       <div class="image_box cf">
        <div class="cf cf_media_field hide-if-no-js news_single_top_ad_image<?php echo $i; ?>">
         <input type="hidden" value="<?php echo esc_attr( $options['news_single_top_ad_image'.$i] ); ?>" id="news_single_top_ad_image<?php echo $i; ?>" name="dp_options[news_single_top_ad_image<?php echo $i; ?>]" class="cf_media_id">
         <div class="preview_field"><?php if($options['news_single_top_ad_image'.$i]){ echo wp_get_attachment_image($options['news_single_top_ad_image'.$i], 'medium'); }; ?></div>
         <div class="buttton_area">
          <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
          <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['news_single_top_ad_image'.$i]){ echo 'hidden'; }; ?>">
         </div>
        </div>
       </div>
       <h4 class="theme_option_headline2"><?php _e('Register affiliate code', 'tcd-w');  ?></h4>
       <input id="dp_options[news_single_top_ad_url<?php echo $i; ?>]" class="regular-text" type="text" name="dp_options[news_single_top_ad_url<?php echo $i; ?>]" value="<?php esc_attr_e( $options['news_single_top_ad_url'.$i] ); ?>" />
       <ul class="button_list cf">
        <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
        <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
       </ul>
      </div><!-- END .sub_box_content -->
     </div><!-- END .sub_box -->
     <?php endfor; ?>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

   <?php // 広告の登録（記事本文の下） -------------------------------------------------------------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Banner setting (Under post content)', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <p><?php _e('This banner will be displayed before recent news list.', 'tcd-w');  ?></p>
     <?php for($i = 1; $i <= 2; $i++) : ?>
     <div class="sub_box cf"> 
      <h3 class="theme_option_subbox_headline"><?php if($i == 1) { ?><?php _e('Left banner', 'tcd-w');  ?><?php } else { ?><?php _e('Right banner', 'tcd-w');  ?><?php }; ?></h3>
      <div class="sub_box_content">
       <h4 class="theme_option_headline2"><?php _e('Banner code', 'tcd-w');  ?></h4>
       <p><?php _e('If you are using google adsense, enter all code below.', 'tcd-w');  ?></p>
       <textarea id="dp_options[news_single_bottom_ad_code<?php echo $i; ?>]" class="large-text" cols="50" rows="10" name="dp_options[news_single_bottom_ad_code<?php echo $i; ?>]"><?php echo esc_textarea( $options['news_single_bottom_ad_code'.$i] ); ?></textarea>
       <div class="theme_option_message">
        <p><?php _e('If you are not using google adsense, you can register your banner image and affiliate code individually.', 'tcd-w');  ?></p>
       </div>
       <h4 class="theme_option_headline2"><?php _e('Register banner image.', 'tcd-w'); ?></h4>
       <p><?php _e('Recommend image size. Width:300px Height:250px', 'tcd-w'); ?></p>
       <div class="image_box cf">
        <div class="cf cf_media_field hide-if-no-js news_single_bottom_ad_image<?php echo $i; ?>">
         <input type="hidden" value="<?php echo esc_attr( $options['news_single_bottom_ad_image'.$i] ); ?>" id="news_single_bottom_ad_image<?php echo $i; ?>" name="dp_options[news_single_bottom_ad_image<?php echo $i; ?>]" class="cf_media_id">
         <div class="preview_field"><?php if($options['news_single_bottom_ad_image'.$i]){ echo wp_get_attachment_image($options['news_single_bottom_ad_image'.$i], 'medium'); }; ?></div>
         <div class="buttton_area">
          <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
          <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['news_single_bottom_ad_image'.$i]){ echo 'hidden'; }; ?>">
         </div>
        </div>
       </div>
       <h4 class="theme_option_headline2"><?php _e('Register affiliate code', 'tcd-w');  ?></h4>
       <input id="dp_options[news_single_bottom_ad_url<?php echo $i; ?>]" class="regular-text" type="text" name="dp_options[news_single_bottom_ad_url<?php echo $i; ?>]" value="<?php esc_attr_e( $options['news_single_bottom_ad_url'.$i] ); ?>" />
       <ul class="button_list cf">
        <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
        <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
       </ul>
      </div><!-- END .sub_box_content -->
     </div><!-- END .sub_box -->
     <?php endfor; ?>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

   <?php // 広告の登録（ショートコード） -------------------------------------------------------------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Banner setting (Short code)', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <p><?php _e('Please copy and paste the short code inside the content to show this banner.', 'tcd-w'); ?></p>
     <p><?php _e('Short code', 'tcd-w');  ?> : <input type="text" readonly="readonly" value="[news_s_ad]" /></p>
     <?php for($i = 1; $i <= 2; $i++) : ?>
     <div class="sub_box cf"> 
      <h3 class="theme_option_subbox_headline"><?php if($i == 1) { ?><?php _e('Left banner', 'tcd-w');  ?><?php } else { ?><?php _e('Right banner', 'tcd-w');  ?><?php }; ?></h3>
      <div class="sub_box_content">
       <h4 class="theme_option_headline2"><?php _e('Banner code', 'tcd-w');  ?></h4>
       <p><?php _e('If you are using google adsense, enter all code below.', 'tcd-w');  ?></p>
       <textarea id="dp_options[news_single_shortcode_ad_code<?php echo $i; ?>]" class="large-text" cols="50" rows="10" name="dp_options[news_single_shortcode_ad_code<?php echo $i; ?>]"><?php echo esc_textarea( $options['news_single_shortcode_ad_code'.$i] ); ?></textarea>
       <div class="theme_option_message">
        <p><?php _e('If you are not using google adsense, you can register your banner image and affiliate code individually.', 'tcd-w');  ?></p>
       </div>
       <h4 class="theme_option_headline2"><?php _e('Register banner image.', 'tcd-w'); ?></h4>
       <p><?php _e('Recommend image size. Width:300px Height:250px', 'tcd-w'); ?></p>
       <div class="image_box cf">
        <div class="cf cf_media_field hide-if-no-js news_single_shortcode_ad_image<?php echo $i; ?>">
         <input type="hidden" value="<?php echo esc_attr( $options['news_single_shortcode_ad_image'.$i] ); ?>" id="news_single_shortcode_ad_image<?php echo $i; ?>" name="dp_options[news_single_shortcode_ad_image<?php echo $i; ?>]" class="cf_media_id">
         <div class="preview_field"><?php if($options['news_single_shortcode_ad_image'.$i]){ echo wp_get_attachment_image($options['news_single_shortcode_ad_image'.$i], 'medium'); }; ?></div>
         <div class="buttton_area">
          <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
          <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['news_single_shortcode_ad_image'.$i]){ echo 'hidden'; }; ?>">
         </div>
        </div>
       </div>
       <h4 class="theme_option_headline2"><?php _e('Register affiliate code', 'tcd-w');  ?></h4>
       <input id="dp_options[news_single_shortcode_ad_url<?php echo $i; ?>]" class="regular-text" type="text" name="dp_options[news_single_shortcode_ad_url<?php echo $i; ?>]" value="<?php esc_attr_e( $options['news_single_shortcode_ad_url'.$i] ); ?>" />
       <ul class="button_list cf">
        <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
        <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
       </ul>
      </div><!-- END .sub_box_content -->
     </div><!-- END .sub_box -->
     <?php endfor; ?>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

   <?php // 広告の登録（スマホ専用） -------------------------------------------------------------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Banner setting (Mobile device)', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <p><?php _e('This banner will be displayed on mobile device.', 'tcd-w');  ?></p>
     <?php for($i = 1; $i <= 2; $i++) : ?>
     <div class="sub_box cf"> 
      <h3 class="theme_option_subbox_headline"><?php if($i == 1) { ?><?php _e('Top banner', 'tcd-w');  ?><?php } else { ?><?php _e('Bottom banner', 'tcd-w');  ?><?php }; ?></h3>
      <div class="sub_box_content">
       <h4 class="theme_option_headline2"><?php _e('Banner code', 'tcd-w');  ?></h4>
       <p><?php _e('If you are using google adsense, enter all code below.', 'tcd-w');  ?></p>
       <textarea id="dp_options[news_single_mobile_ad_code<?php echo $i; ?>]" class="large-text" cols="50" rows="10" name="dp_options[news_single_mobile_ad_code<?php echo $i; ?>]"><?php echo esc_textarea( $options['news_single_mobile_ad_code'.$i] ); ?></textarea>
       <div class="theme_option_message">
        <p><?php _e('If you are not using google adsense, you can register your banner image and affiliate code individually.', 'tcd-w');  ?></p>
       </div>
       <h4 class="theme_option_headline2"><?php _e('Register banner image.', 'tcd-w'); ?></h4>
       <p><?php _e('Recommend image size. Width:300px Height:250px', 'tcd-w'); ?></p>
       <div class="image_box cf">
        <div class="cf cf_media_field hide-if-no-js news_single_mobile_ad_image<?php echo $i; ?>">
         <input type="hidden" value="<?php echo esc_attr( $options['news_single_mobile_ad_image'.$i] ); ?>" id="news_single_mobile_ad_image<?php echo $i; ?>" name="dp_options[news_single_mobile_ad_image<?php echo $i; ?>]" class="cf_media_id">
         <div class="preview_field"><?php if($options['news_single_mobile_ad_image'.$i]){ echo wp_get_attachment_image($options['news_single_mobile_ad_image'.$i], 'medium'); }; ?></div>
         <div class="buttton_area">
          <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
          <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['news_single_mobile_ad_image'.$i]){ echo 'hidden'; }; ?>">
         </div>
        </div>
       </div>
       <h4 class="theme_option_headline2"><?php _e('Register affiliate code', 'tcd-w');  ?></h4>
       <input id="dp_options[news_single_mobile_ad_url<?php echo $i; ?>]" class="regular-text" type="text" name="dp_options[news_single_mobile_ad_url<?php echo $i; ?>]" value="<?php esc_attr_e( $options['news_single_mobile_ad_url'.$i] ); ?>" />
       <ul class="button_list cf">
        <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
        <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
       </ul>
      </div><!-- END .sub_box_content -->
     </div><!-- END .sub_box -->
     <?php endfor; ?>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


</div><!-- END .tab-content -->

<?php
} // END add_news_tab_panel()


// バリデーション　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_news_theme_options_validate( $input ) {

  global $dp_default_options, $no_image_options;

  //基本設定
  $input['news_slug'] = sanitize_title( $input['news_slug'] );
  $input['news_label'] = wp_filter_nohtml_kses( $input['news_label'] );
  if ( ! isset( $input['news_no_image'] ) )
    $input['news_no_image'] = null;
  if ( ! array_key_exists( $input['news_no_image'], $no_image_options ) )
    $input['news_no_image'] = null;

  //アーカイブページ
  $input['archive_news_headline_font_size'] = wp_filter_nohtml_kses( $input['archive_news_headline_font_size'] );
  $input['archive_news_headline_font_size_mobile'] = wp_filter_nohtml_kses( $input['archive_news_headline_font_size_mobile'] );
  $input['archive_news_headline_font_color'] = wp_filter_nohtml_kses( $input['archive_news_headline_font_color'] );
  $input['archive_news_headline_bg_color'] = wp_filter_nohtml_kses( $input['archive_news_headline_bg_color'] );
  $input['archive_news_headline_border_color'] = wp_filter_nohtml_kses( $input['archive_news_headline_border_color'] );
  $input['archive_news_headline_icon_color'] = wp_filter_nohtml_kses( $input['archive_news_headline_icon_color'] );
  if ( ! isset( $input['hide_archive_news_headline_icon'] ) )
    $input['hide_archive_news_headline_icon'] = null;
    $input['hide_archive_news_headline_icon'] = ( $input['hide_archive_news_headline_icon'] == 1 ? 1 : 0 );
  $input['archive_news_headline_image'] = wp_filter_nohtml_kses( $input['archive_news_headline_image'] );
  $input['archive_news_title_font_size'] = wp_filter_nohtml_kses( $input['archive_news_title_font_size'] );
  $input['archive_news_title_font_size_mobile'] = wp_filter_nohtml_kses( $input['archive_news_title_font_size_mobile'] );
  if ( ! isset( $input['show_archive_news_date'] ) )
    $input['show_archive_news_date'] = null;
    $input['show_archive_news_date'] = ( $input['show_archive_news_date'] == 1 ? 1 : 0 );
  $input['archive_news_num'] = wp_filter_nohtml_kses( $input['archive_news_num'] );

  //詳細ページ
  $input['single_news_title_font_size'] = wp_filter_nohtml_kses( $input['single_news_title_font_size'] );
  $input['single_news_content_font_size'] = wp_filter_nohtml_kses( $input['single_news_content_font_size'] );
  $input['single_news_title_font_size_mobile'] = wp_filter_nohtml_kses( $input['single_news_title_font_size_mobile'] );
  $input['single_news_content_font_size_mobile'] = wp_filter_nohtml_kses( $input['single_news_content_font_size_mobile'] );
  if ( ! isset( $input['show_news_date'] ) )
    $input['show_news_date'] = null;
    $input['show_news_date'] = ( $input['show_news_date'] == 1 ? 1 : 0 );
  if ( ! isset( $input['show_news_category'] ) )
    $input['show_news_category'] = null;
    $input['show_news_category'] = ( $input['show_news_category'] == 1 ? 1 : 0 );
  if ( ! isset( $input['show_news_nav'] ) )
    $input['show_news_nav'] = null;
    $input['show_news_nav'] = ( $input['show_news_nav'] == 1 ? 1 : 0 );
  if ( ! isset( $input['show_news_image'] ) )
    $input['show_news_image'] = null;
    $input['show_news_image'] = ( $input['show_news_image'] == 1 ? 1 : 0 );
  if ( ! isset( $input['show_sns_top_news'] ) )
    $input['show_sns_top_news'] = null;
    $input['show_sns_top_news'] = ( $input['show_sns_top_news'] == 1 ? 1 : 0 );
  if ( ! isset( $input['show_sns_btm_news'] ) )
    $input['show_sns_btm_news'] = null;
    $input['show_sns_btm_news'] = ( $input['show_sns_btm_news'] == 1 ? 1 : 0 );
  if ( ! isset( $input['show_copy_top_news'] ) )
    $input['show_copy_top_news'] = null;
    $input['show_copy_top_news'] = ( $input['show_copy_top_news'] == 1 ? 1 : 0 );
  if ( ! isset( $input['show_copy_btm_news'] ) )
    $input['show_copy_btm_news'] = null;
    $input['show_copy_btm_news'] = ( $input['show_copy_btm_news'] == 1 ? 1 : 0 );


  // 最新のお知らせ一覧
  if ( ! isset( $input['show_recent_news'] ) )
    $input['show_recent_news'] = null;
    $input['show_recent_news'] = ( $input['show_recent_news'] == 1 ? 1 : 0 );
  $input['recent_news_headline'] = wp_filter_nohtml_kses( $input['recent_news_headline'] );
  $input['recent_news_link_label'] = wp_filter_nohtml_kses( $input['recent_news_link_label'] );
  $input['recent_news_headline_font_size'] = wp_filter_nohtml_kses( $input['recent_news_headline_font_size'] );
  $input['recent_news_headline_font_size_mobile'] = wp_filter_nohtml_kses( $input['recent_news_headline_font_size_mobile'] );
  $input['recent_news_headline_font_color'] = wp_filter_nohtml_kses( $input['recent_news_headline_font_color'] );
  $input['recent_news_headline_bg_color'] = wp_filter_nohtml_kses( $input['recent_news_headline_bg_color'] );
  $input['recent_news_headline_border_color'] = wp_filter_nohtml_kses( $input['recent_news_headline_border_color'] );
  $input['recent_news_headline_icon_color'] = wp_filter_nohtml_kses( $input['recent_news_headline_icon_color'] );
  if ( ! isset( $input['hide_recent_news_headline_icon'] ) )
    $input['hide_recent_news_headline_icon'] = null;
    $input['hide_recent_news_headline_icon'] = ( $input['hide_recent_news_headline_icon'] == 1 ? 1 : 0 );
  $input['recent_news_headline_image'] = wp_filter_nohtml_kses( $input['recent_news_headline_image'] );
  if ( ! isset( $input['show_recent_news_date'] ) )
    $input['show_recent_news_date'] = null;
    $input['show_recent_news_date'] = ( $input['show_recent_news_date'] == 1 ? 1 : 0 );
  if ( ! isset( $input['show_recent_news_link'] ) )
    $input['show_recent_news_link'] = null;
    $input['show_recent_news_link'] = ( $input['show_recent_news_link'] == 1 ? 1 : 0 );
  $input['recent_news_num'] = wp_filter_nohtml_kses( $input['recent_news_num'] );


  // 広告
  for ( $i = 1; $i <= 2; $i++ ) {
    $input['news_single_top_ad_code'.$i] = $input['news_single_top_ad_code'.$i];
    $input['news_single_top_ad_image'.$i] = wp_filter_nohtml_kses( $input['news_single_top_ad_image'.$i] );
    $input['news_single_top_ad_url'.$i] = wp_filter_nohtml_kses( $input['news_single_top_ad_url'.$i] );
  }
  for ( $i = 1; $i <= 2; $i++ ) {
    $input['news_single_bottom_ad_code'.$i] = $input['news_single_bottom_ad_code'.$i];
    $input['news_single_bottom_ad_image'.$i] = wp_filter_nohtml_kses( $input['news_single_bottom_ad_image'.$i] );
    $input['news_single_bottom_ad_url'.$i] = wp_filter_nohtml_kses( $input['news_single_bottom_ad_url'.$i] );
  }
  for ( $i = 1; $i <= 2; $i++ ) {
    $input['news_single_shortcode_ad_code'.$i] = $input['news_single_shortcode_ad_code'.$i];
    $input['news_single_shortcode_ad_image'.$i] = wp_filter_nohtml_kses( $input['news_single_shortcode_ad_image'.$i] );
    $input['news_single_shortcode_ad_url'.$i] = wp_filter_nohtml_kses( $input['news_single_shortcode_ad_url'.$i] );
  }
  for ( $i = 1; $i <= 2; $i++ ) {
    $input['news_single_mobile_ad_code'.$i] = $input['news_single_mobile_ad_code'.$i];
    $input['news_single_mobile_ad_image'.$i] = wp_filter_nohtml_kses( $input['news_single_mobile_ad_image'.$i] );
    $input['news_single_mobile_ad_url'.$i] = wp_filter_nohtml_kses( $input['news_single_mobile_ad_url'.$i] );
  }

	return $input;

};


?>
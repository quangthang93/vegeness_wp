<?php
/*
 * 専門家の設定
 */


// Add default values
add_filter( 'before_getting_design_plus_option', 'add_author_dp_default_options' );


// Add label of logo tab
add_action( 'tcd_tab_labels', 'add_author_tab_label' );


// Add HTML of logo tab
add_action( 'tcd_tab_panel', 'add_author_tab_panel' );


// Register sanitize function
add_filter( 'theme_options_validate', 'add_author_theme_options_validate' );


// タブの名前
function add_author_tab_label( $tab_labels ) {
	$tab_labels['author'] = __( 'Author', 'tcd-w' );
	return $tab_labels;
}


// 初期値
function add_author_dp_default_options( $dp_default_options ) {

	// ヘッダー
	$dp_default_options['author_parent_page_url'] = '';
	$dp_default_options['author_parent_page_name'] = '';
	$dp_default_options['author_recipe_name_font_size'] = '24';
	$dp_default_options['author_recipe_name_font_size_mobile'] = '18';
	$dp_default_options['author_recipe_sub_title_font_size'] = '14';
	$dp_default_options['author_recipe_sub_title_font_size_mobile'] = '10';
	$dp_default_options['author_recipe_desc_font_size'] = '16';
	$dp_default_options['author_recipe_desc_font_size_mobile'] = '14';
	$dp_default_options['author_recipe_blur'] = 10;
	$dp_default_options['author_recipe_overlay_color'] = '#FFFFFF';
	$dp_default_options['author_recipe_overlay_opacity'] = '0.4';

	// アーカイブページ
	$dp_default_options['author_recipe_headline_label'] = __( 'recipe', 'tcd-w' );
	$dp_default_options['author_recipe_headline_font_size'] = '20';
	$dp_default_options['author_recipe_headline_font_size_mobile'] = '15';
	$dp_default_options['author_recipe_headline_font_color'] = '#000000';
	$dp_default_options['author_recipe_headline_bg_color'] = '#ffffff';
	$dp_default_options['author_recipe_headline_border_color'] = '#dddddd';
	$dp_default_options['hide_author_recipe_headline_icon'] = '';
	$dp_default_options['author_recipe_headline_icon_color'] = '#000000';
	$dp_default_options['author_recipe_headline_image'] = false;
	$dp_default_options['show_author_recipe_post_count'] = 1;
	$dp_default_options['author_recipe_post_count_label'] = __( 'recipe', 'tcd-w' );
	$dp_default_options['author_recipe_list_title_font_size'] = '16';
	$dp_default_options['author_recipe_list_title_font_size_mobile'] = '14';
	$dp_default_options['author_recipe_list_show_post_view'] = 1;
	$dp_default_options['author_recipe_list_show_premium_icon'] = 1;
	$dp_default_options['author_recipe_num'] = '18';

	return $dp_default_options;

}


// 入力欄の出力　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_author_tab_panel( $options ) {

  global $dp_default_options;
  $recipe_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );

?>

<div id="tab-content-author" class="tab-content">

   <?php // 基本設定設定 ----------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Basic setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Author list page setting', 'tcd-w');  ?></h4>
     <div class="theme_option_message2">
      <p><?php _e('You can create designed author list page by selecting "Author list" page template in page edit screen.', 'tcd-w');  ?></p>
     </div>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Name of author list page', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[author_parent_page_name]" value="<?php esc_attr_e( $options['author_parent_page_name'] ); ?>" /></li>
      <li class="cf"><span class="label"><?php _e('URL of author list page', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[author_parent_page_url]" value="<?php esc_attr_e( $options['author_parent_page_url'] ); ?>" /></li>
     </ul>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

   <?php // ヘッダーの設定 ----------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Header setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Font size setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Name', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[author_recipe_name_font_size]" value="<?php esc_attr_e( $options['author_recipe_name_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Sub title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[author_recipe_sub_title_font_size]" value="<?php esc_attr_e( $options['author_recipe_sub_title_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Description', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[author_recipe_desc_font_size]" value="<?php esc_attr_e( $options['author_recipe_desc_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Name (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[author_recipe_name_font_size_mobile]" value="<?php esc_attr_e( $options['author_recipe_name_font_size_mobile'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Sub title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[author_recipe_sub_title_font_size_mobile]" value="<?php esc_attr_e( $options['author_recipe_sub_title_font_size_mobile'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Description (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[author_recipe_desc_font_size_mobile]" value="<?php esc_attr_e( $options['author_recipe_desc_font_size_mobile'] ); ?>" /><span>px</span></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e( 'Frost blur image setting', 'tcd-w' ); ?></h4>
     <ul class="option_list">
      <li class="cf">
       <span class="label"><?php _e('Strenth of blur effect', 'tcd-w'); ?></span><input class="hankaku" style="width:70px;" type="number" max="10" min="0" step="1" name="dp_options[author_recipe_blur]" value="<?php echo esc_attr( $options['author_recipe_blur'] ); ?>" />
       <div class="theme_option_message2">
        <p><?php _e('Please specify the number of 1 from 10. Blur effect will be stronger as the number is large.', 'tcd-w');  ?></p>
       </div>
      </li>
      <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="dp_options[author_recipe_overlay_color]" value="<?php echo esc_attr( $options['author_recipe_overlay_color'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
      <li class="cf">
       <span class="label"><?php _e('Transparency of background color', 'tcd-w'); ?></span><input class="hankaku" style="width:70px;" type="number" max="1" min="0" step="0.1" name="dp_options[author_recipe_overlay_opacity]" value="<?php echo esc_attr( $options['author_recipe_overlay_opacity'] ); ?>" />
       <div class="theme_option_message2">
        <p><?php _e('Please specify the number of 0.1 from 0.9. Overlay color will be more transparent as the number is small.', 'tcd-w');  ?></p>
       </div>
      </li>
     </ul>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

   <?php // アーカイブページの設定 ----------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Archive page setting', 'tcd-w'); ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[author_recipe_headline_label]" value="<?php echo esc_attr( $options['author_recipe_headline_label'] ); ?>" /></li>
      <li class="cf"><span class="label"><?php _e('Font size of headline', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[author_recipe_headline_font_size]" value="<?php esc_attr_e( $options['author_recipe_headline_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Font size of headline (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[author_recipe_headline_font_size_mobile]" value="<?php esc_attr_e( $options['author_recipe_headline_font_size_mobile'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Font color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[author_recipe_headline_font_color]" value="<?php echo esc_attr( $options['author_recipe_headline_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[author_recipe_headline_bg_color]" value="<?php echo esc_attr( $options['author_recipe_headline_bg_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Border color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[author_recipe_headline_border_color]" value="<?php echo esc_attr( $options['author_recipe_headline_border_color'] ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Hide headline icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="dp_options[hide_author_recipe_headline_icon]" value="1" <?php checked( $options['hide_author_recipe_headline_icon'], 1 ); ?>></li>
     </ul>
     <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($options['hide_author_recipe_headline_icon']) { echo ' display:none;'; }; ?>">
      <li class="cf"><span class="label"><?php _e('Background color of headline icon', 'tcd-w'); ?></span><input type="text" name="dp_options[author_recipe_headline_icon_color]" value="<?php echo esc_attr( $options['author_recipe_headline_icon_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
      <li class="cf"><span class="label"><?php _e('Headline icon image', 'tcd-w');  ?></span>
       <div class="image_box cf">
        <div class="cf cf_media_field hide-if-no-js author_recipe_headline_image">
         <input type="hidden" value="<?php echo esc_attr( $options['author_recipe_headline_image'] ); ?>" id="author_recipe_headline_image" name="dp_options[author_recipe_headline_image]" class="cf_media_id">
         <div class="preview_field"><?php if($options['author_recipe_headline_image']){ echo wp_get_attachment_image($options['author_recipe_headline_image'], 'full'); }; ?></div>
         <div class="buttton_area">
          <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
          <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['author_recipe_headline_image']){ echo 'hidden'; }; ?>">
         </div>
        </div>
       </div>
       <div class="theme_option_message2">
        <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
        <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
       </div>
      </li>
     </ul>
     <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;">
      <li class="cf"><span class="label"><?php _e('Display total post', 'tcd-w'); ?></span><input name="dp_options[show_author_recipe_post_count]" type="checkbox" value="1" <?php checked( $options['show_author_recipe_post_count'], 1 ); ?>></li>
      <li class="cf"><span class="label"><?php _e('label of total count', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[author_recipe_post_count_label]" value="<?php echo esc_attr( $options['author_recipe_post_count_label'] ); ?>" /></li>
     </ul>
     <h4 class="theme_option_headline2"><?php printf(__('%s list setting', 'tcd-w'), $recipe_label); ?></h4>
     <ul class="option_list">
      <li class="cf">
       <span class="label"><?php _e('Number of post to display', 'tcd-w'); ?></span>
       <select name="dp_options[author_recipe_num]">
        <?php for($i=12; $i<= 30; $i++): if( ($i % 3) == 0 ){ ?>
        <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $options['author_recipe_num'], $i ); ?>><?php echo esc_html($i); ?></option>
        <?php }; endfor; ?>
       </select>
      </li>
      <li class="cf"><span class="label"><?php _e('Font size of title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[author_recipe_list_title_font_size]" value="<?php esc_attr_e( $options['author_recipe_list_title_font_size'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Font size of title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[author_recipe_list_title_font_size_mobile]" value="<?php esc_attr_e( $options['author_recipe_list_title_font_size_mobile'] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Display post view count', 'tcd-w');  ?></span><input name="dp_options[author_recipe_list_show_post_view]" type="checkbox" value="1" <?php checked( '1', $options['author_recipe_list_show_post_view'] ); ?> /></li>
      <li class="cf"><span class="label"><?php printf(__('Display premium %s icon', 'tcd-w'), $recipe_label); ?></span><input name="dp_options[author_recipe_list_show_premium_icon]" type="checkbox" value="1" <?php checked( '1', $options['author_recipe_list_show_premium_icon'] ); ?> /></li>
     </ul>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

</div><!-- END .tab-content -->

<?php
} // END add_author_tab_panel()


// バリデーション　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_author_theme_options_validate( $input ) {

  global $dp_default_options;

  // 基本設定
  $input['author_parent_page_name'] = wp_filter_nohtml_kses( $input['author_parent_page_name'] );
  $input['author_parent_page_url'] = wp_filter_nohtml_kses( $input['author_parent_page_url'] );
  $input['author_recipe_name_font_size'] = wp_filter_nohtml_kses( $input['author_recipe_name_font_size'] );
  $input['author_recipe_name_font_size_mobile'] = wp_filter_nohtml_kses( $input['author_recipe_name_font_size_mobile'] );
  $input['author_recipe_sub_title_font_size'] = wp_filter_nohtml_kses( $input['author_recipe_sub_title_font_size'] );
  $input['author_recipe_sub_title_font_size_mobile'] = wp_filter_nohtml_kses( $input['author_recipe_sub_title_font_size_mobile'] );
  $input['author_recipe_desc_font_size'] = wp_filter_nohtml_kses( $input['author_recipe_desc_font_size'] );
  $input['author_recipe_desc_font_size_mobile'] = wp_filter_nohtml_kses( $input['author_recipe_desc_font_size_mobile'] );
  $input['author_recipe_blur'] = wp_filter_nohtml_kses( $input['author_recipe_blur'] );
  $input['author_recipe_overlay_color'] = wp_filter_nohtml_kses( $input['author_recipe_overlay_color'] );
  $input['author_recipe_overlay_opacity'] = wp_filter_nohtml_kses( $input['author_recipe_overlay_opacity'] );

  //アーカイブページ
  $input['author_recipe_headline_label'] = wp_filter_nohtml_kses( $input['author_recipe_headline_label'] );
  $input['author_recipe_headline_font_size'] = wp_filter_nohtml_kses( $input['author_recipe_headline_font_size'] );
  $input['author_recipe_headline_font_size_mobile'] = wp_filter_nohtml_kses( $input['author_recipe_headline_font_size_mobile'] );
  $input['author_recipe_headline_font_color'] = wp_filter_nohtml_kses( $input['author_recipe_headline_font_color'] );
  $input['author_recipe_headline_bg_color'] = wp_filter_nohtml_kses( $input['author_recipe_headline_bg_color'] );
  $input['author_recipe_headline_border_color'] = wp_filter_nohtml_kses( $input['author_recipe_headline_border_color'] );
  $input['author_recipe_headline_icon_color'] = wp_filter_nohtml_kses( $input['author_recipe_headline_icon_color'] );
  if ( ! isset( $input['hide_author_recipe_headline_icon'] ) )
    $input['hide_author_recipe_headline_icon'] = null;
    $input['hide_author_recipe_headline_icon'] = ( $input['hide_author_recipe_headline_icon'] == 1 ? 1 : 0 );
  $input['author_recipe_headline_image'] = wp_filter_nohtml_kses( $input['author_recipe_headline_image'] );
  $input['author_recipe_post_count_label'] = wp_filter_nohtml_kses( $input['author_recipe_post_count_label'] );
  if ( ! isset( $input['show_author_recipe_post_count'] ) )
    $input['show_author_recipe_post_count'] = null;
    $input['show_author_recipe_post_count'] = ( $input['show_author_recipe_post_count'] == 1 ? 1 : 0 );

  //詳細ページ
  $input['author_recipe_list_title_font_size'] = wp_filter_nohtml_kses( $input['author_recipe_list_title_font_size'] );
  $input['author_recipe_list_title_font_size_mobile'] = wp_filter_nohtml_kses( $input['author_recipe_list_title_font_size_mobile'] );
  if ( ! isset( $input['author_recipe_list_show_post_view'] ) )
    $input['author_recipe_list_show_post_view'] = null;
    $input['author_recipe_list_show_post_view'] = ( $input['author_recipe_list_show_post_view'] == 1 ? 1 : 0 );
  if ( ! isset( $input['author_recipe_list_show_premium_icon'] ) )
    $input['author_recipe_list_show_premium_icon'] = null;
    $input['author_recipe_list_show_premium_icon'] = ( $input['author_recipe_list_show_premium_icon'] == 1 ? 1 : 0 );
  $input['author_recipe_num'] = wp_filter_nohtml_kses( $input['author_recipe_num'] );

	return $input;

};


?>
<?php
/*
 * マイページの設定
 */

// 通知間隔オプション
global $notify_schedule_type_options;
$notify_schedule_type_options = array(
	'type1' => array(
		'value' => 'type1',
		'label' => __( 'Everyday', 'tcd-w' )
	),
	'type2' => array(
		'value' => 'type2',
		'label' => __( 'Days interval', 'tcd-w' )
	),
	'type3' => array(
		'value' => 'type3',
		'label' => __( 'Day of the week', 'tcd-w' )
	),
	'type4' => array(
		'value' => 'type4',
		'label' => __( 'Select days', 'tcd-w' )
	)
);

// Add default values
add_filter( 'before_getting_design_plus_option', 'add_mypage_dp_default_options' );


// Add label of logo tab
add_action( 'tcd_tab_labels', 'add_mypage_tab_label' );


// Add HTML of logo tab
add_action( 'tcd_tab_panel', 'add_mypage_tab_panel' );


// Register sanitize function
add_filter( 'theme_options_validate', 'add_mypage_theme_options_validate' );


// タブの名前
function add_mypage_tab_label( $tab_labels ) {
	$tab_labels['mypage'] = __( 'Account page', 'tcd-w' );
	return $tab_labels;
}


// 初期値
function add_mypage_dp_default_options( $dp_default_options ) {

	// アカウントページの設定
	$dp_default_options['memberpage_page_id'] = 1;

	// お知らせ
	$dp_default_options['account_news_label'] = __( 'Member news', 'tcd-w' );
	$dp_default_options['account_news_list_num'] = '8';
	for ( $i = 1; $i <= 2; $i++ ) {
		$dp_default_options['account_news_ad_code' . $i] = '';
		$dp_default_options['account_news_ad_image' . $i] = false;
		$dp_default_options['account_news_ad_url' . $i] = '';
	}

	// お気に入り
	$dp_default_options['account_like_label'] = __( 'Favorites', 'tcd-w' );
	$dp_default_options['account_like_list_num'] = '9';

	// お知らせ通知
	$dp_default_options['use_member_news_notify'] = 1;
	$dp_default_options['member_news_notify_schedule_type'] = 'type1';
	$dp_default_options['member_news_notify_schedule_type2'] = 2;
	$dp_default_options['member_news_notify_schedule_type3'] = 0;
	$dp_default_options['member_news_notify_schedule_type4'] = array( 1, 15 );
	$dp_default_options['member_news_notify_hour'] = '12';
	$dp_default_options['member_news_notify_minute'] = '00';
	$dp_default_options['mail_member_news_notify_subject'] = __( 'New information / [blog_name]', 'tcd-w' );
	$dp_default_options['mail_member_news_notify_body'] = __( 'Dear [user_display_name],

Thank you for using [blog_name].

We will notify you of new information.
Please check the URL below.
[mypage_url]

Sincerely, [blog_name]', 'tcd-w' );

	return $dp_default_options;
}


// 入力欄の出力　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_mypage_tab_panel( $options ) {

	global $dp_default_options, $notify_schedule_type_options, $wp_locale;

?>

<div id="tab-content-mypage" class="tab-content">

   <?php // アカウントページの設定 ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Account page setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Page setting', 'tcd-w');  ?></h4>
     <p><?php _e( 'Set a page to be displayed as a "Account page".', 'tcd-w' ); ?></p>
<?php
$exclude_pages = array();
if ( 'page' === get_option( 'show_on_front' ) ) {
	$exclude_pages = array(
		get_option( 'page_on_front' ),
		get_option( 'page_for_posts' )
	);
}
wp_dropdown_pages( array(
	'exclude' => $exclude_pages,
	'name' => 'dp_options[memberpage_page_id]',
	'selected' => $options['memberpage_page_id'],
	'show_option_none' => ' '
) );
?>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // お知らせの設定 ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('News area setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w');  ?></h4>
     <input class="full_width" type="text" name="dp_options[account_news_label]" value="<?php esc_attr_e( $options['account_news_label'] ); ?>" />
     <h4 class="theme_option_headline2"><?php _e('Number of post to display', 'tcd-w');  ?></h4>
     <select name="dp_options[account_news_list_num]">
      <?php for($i=5; $i<= 20; $i++): ?>
      <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $options['account_news_list_num'], $i ); ?>><?php echo esc_html($i); ?></option>
      <?php endfor; ?>
     </select>

     <h4 class="theme_option_headline2"><?php _e('Banner setting', 'tcd-w');  ?></h4>
     <?php for($i = 1; $i <= 2; $i++) : ?>
     <div class="sub_box cf">
      <h3 class="theme_option_subbox_headline"><?php if($i == 1) { ?><?php _e('Left banner', 'tcd-w');  ?><?php } else { ?><?php _e('Right banner', 'tcd-w');  ?><?php }; ?></h3>
      <div class="sub_box_content">
       <h4 class="theme_option_headline2"><?php _e('Banner code', 'tcd-w');  ?></h4>
       <p><?php _e('If you are using google adsense, enter all code below.', 'tcd-w');  ?></p>
       <textarea id="dp_options[account_news_ad_code<?php echo $i; ?>]" class="large-text" cols="50" rows="10" name="dp_options[account_news_ad_code<?php echo $i; ?>]"><?php echo esc_textarea( $options['account_news_ad_code'.$i] ); ?></textarea>
       <div class="theme_option_message">
        <p><?php _e('If you are not using google adsense, you can register your banner image and affiliate code individually.', 'tcd-w');  ?></p>
       </div>
       <h4 class="theme_option_headline2"><?php _e('Register banner image.', 'tcd-w'); ?></h4>
       <p><?php _e('Recommend image size. Width:300px Height:250px', 'tcd-w'); ?></p>
       <div class="image_box cf">
        <div class="cf cf_media_field hide-if-no-js account_news_ad_image<?php echo $i; ?>">
         <input type="hidden" value="<?php echo esc_attr( $options['account_news_ad_image'.$i] ); ?>" id="account_news_ad_image<?php echo $i; ?>" name="dp_options[account_news_ad_image<?php echo $i; ?>]" class="cf_media_id">
         <div class="preview_field"><?php if($options['account_news_ad_image'.$i]){ echo wp_get_attachment_image($options['account_news_ad_image'.$i], 'medium'); }; ?></div>
         <div class="buttton_area">
          <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
          <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['account_news_ad_image'.$i]){ echo 'hidden'; }; ?>">
         </div>
        </div>
       </div>
       <h4 class="theme_option_headline2"><?php _e('Register affiliate code', 'tcd-w');  ?></h4>
       <input id="dp_options[account_news_ad_url<?php echo $i; ?>]" class="regular-text" type="text" name="dp_options[account_news_ad_url<?php echo $i; ?>]" value="<?php esc_attr_e( $options['account_news_ad_url'.$i] ); ?>" />
       <ul class="button_list cf">
        <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
        <li><a class="close_sub_box button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
       </ul>
      </div><!-- END .sub_box_content -->
     </div><!-- END .sub_box -->
     <?php endfor; ?>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // お気に入りの設定 ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Favorites area setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w');  ?></h4>
     <input class="full_width" type="text" name="dp_options[account_like_label]" value="<?php esc_attr_e( $options['account_like_label'] ); ?>" />
     <h4 class="theme_option_headline2"><?php _e('Number of post to display', 'tcd-w');  ?></h4>
     <select name="dp_options[account_like_list_num]">
      <?php for($i=3; $i<= 12; $i++): ?>
      <?php if( $i % 3 == 0 ){ ?>
      <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $options['account_like_list_num'], $i ); ?>><?php echo esc_html($i); ?></option>
      <?php }; ?>
      <?php endfor; ?>
     </select>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

   <?php // お知らせ通知の設定 ?>
   <div class="theme_option_field cf theme_option_field_ac member_news_notify">
    <h3 class="theme_option_headline"><?php _e('Member news notification setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <p><?php _e( 'Notify the member news added after the last notification.', 'tcd-w' ); ?></p>
     <div class="theme_option_message2">
      <p><?php _e( 'When using this function, recommend Cron setting to access wp-cron.php on the server. If you do not set Cron, time may be lost or processing may not be executed.', 'tcd-w' ); ?></p>
      <p><?php _e( 'Cron is runs programs periodically. Please check the setting manual / support etc of your server setting.', 'tcd-w' ); ?></p>
      <p><?php _e( 'Depending on the server and plan, it may not be available', 'tcd-w' ); ?></p>
     </div>
     <h4 class="theme_option_headline2"><?php _e( 'Use notification setting', 'tcd-w' ); ?></h4>
     <p class="displayment_checkbox"><label><input name="dp_options[use_member_news_notify]" type="checkbox" value="1" <?php checked( $options['use_member_news_notify'], 1 ); ?>><?php _e( 'Use member news notification', 'tcd-w' ); ?></label></p>
     <div style="<?php if ( ! $options['use_member_news_notify'] ) echo 'display:none;'; ?>">
      <h4 class="theme_option_headline2"><?php _e( 'Notification schedule', 'tcd-w' ); ?></h4>
      <fieldset class="p-notification-schedule">
       <p>
        <label><input type="radio" name="dp_options[member_news_notify_schedule_type]" value="type1" data-toggle-reverse=".member_news_notify_schedule_type4" <?php checked( 'type1', $options['member_news_notify_schedule_type'] ); ?>><?php echo esc_html( $notify_schedule_type_options['type1']['label'] ); ?></label>
       </p>
       <p>
        <label><input type="radio" name="dp_options[member_news_notify_schedule_type]" value="type2" data-toggle-reverse=".member_news_notify_schedule_type4" <?php checked( 'type2', $options['member_news_notify_schedule_type'] ); ?>><?php echo esc_html( $notify_schedule_type_options['type2']['label'] ); ?></label>
        <input class="small-text" type="number" name="dp_options[member_news_notify_schedule_type2]" value="<?php echo esc_attr( $options['member_news_notify_schedule_type2'] ); ?>"> <?php _e( 'days interval', 'tcd-w' ); ?>
        <small class="description"><?php _e( 'When set 1 days, same as everyday.', 'tcd-w' ); ?></small>
       </p>
       <p>
        <label><input type="radio" name="dp_options[member_news_notify_schedule_type]" value="type3" data-toggle-reverse=".member_news_notify_schedule_type4" <?php checked( 'type3', $options['member_news_notify_schedule_type'] ); ?>><?php echo esc_html( $notify_schedule_type_options['type3']['label'] ); ?></label>
        <select name="dp_options[member_news_notify_schedule_type3]">
<?php
	for ( $i = 0; $i <= 6; $i++ ) :
		echo '<option value="' . esc_attr( $i ) . '" ' . selected( $i, $options['member_news_notify_schedule_type3'], false ) . '>' . $wp_locale->get_weekday( $i ) . '</option>';
	endfor;
?>
        </select>
       </p>
       <p>
        <label><input type="radio" name="dp_options[member_news_notify_schedule_type]" value="type4" <?php checked( 'type4', $options['member_news_notify_schedule_type'] ); ?> data-toggle=".member_news_notify_schedule_type4"><?php echo esc_html( $notify_schedule_type_options['type4']['label'] ); ?></label>
       </p>
       <div class="member_news_notify_schedule_type4 p-select-days">
<?php
	for ( $i = 1; $i <= 31; $i++ ) :
		echo '<label><input type="checkbox" name="dp_options[member_news_notify_schedule_type4][]" value="' . esc_attr( $i ) . '"' . ( in_array( $i, $options['member_news_notify_schedule_type4'] ) ? ' checked="checked"' : '' ) . '><br>' . esc_html( $i ) . '</label>';
	endfor;
?>
       </div>
      </fieldset>
      <h4 class="theme_option_headline2"><?php _e( 'Notification time', 'tcd-w' ); ?></h4>
      <p>
       <select name="dp_options[member_news_notify_hour]">
<?php
	for ( $i = 0; $i <= 23; $i++ ) :
		$j = sprintf( '%02d' , $i );
		echo '<option value="' . esc_attr( $j ) . '" ' . selected( $j, $options['member_news_notify_hour'], false ) . '>' . $j . '</option>';
	endfor;
?>
       </select> : 
       <select name="dp_options[member_news_notify_minute]">
<?php
	for ( $i = 0; $i <= 59; $i++ ) :
		$j = sprintf( '%02d' , $i );
		echo '<option value="' . esc_attr( $j ) . '" ' . selected( $j, $options['member_news_notify_minute'], false ) . '>' . $j . '</option>';
	endfor;
?>
       </select>
      </p>
      <h4 class="theme_option_headline2"><?php _e('Title of email', 'tcd-w');  ?></h4>
      <input class="large-text" type="text" name="dp_options[mail_member_news_notify_subject]" value="<?php echo esc_attr( $options['mail_member_news_notify_subject'] ); ?>">
      <h4 class="theme_option_headline2"><?php _e('Content of email', 'tcd-w');  ?></h4>
      <textarea class="large-text" name="dp_options[mail_member_news_notify_body]" rows="10"><?php echo esc_textarea( $options['mail_member_news_notify_body'] ); ?></textarea>
      <p class="description"><?php _e( 'Available Variables', 'tcd-w' ); ?>: [blog_name], [blog_url], [user_display_name], [user_email], [mypage_url], [news_count]</p>
     </div>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

</div><!-- END .tab-content -->

<?php
} // END add_mypage_tab_panel()


// バリデーション　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_mypage_theme_options_validate( $input ) {

	global $dp_default_options, $notify_schedule_type_options;

	// アカウントページの設定
	$input['memberpage_page_id'] = absint( $input['memberpage_page_id'] );

	// お知らせ
	$input['account_news_label'] = wp_filter_nohtml_kses( $input['account_news_label'] );
	$input['account_news_list_num'] = wp_filter_nohtml_kses( $input['account_news_list_num'] );
	for ( $i = 1; $i <= 2; $i++ ) {
		$input['account_news_ad_code'.$i] = $input['account_news_ad_code'.$i];
		$input['account_news_ad_image'.$i] = wp_filter_nohtml_kses( $input['account_news_ad_image'.$i] );
		$input['account_news_ad_url'.$i] = wp_filter_nohtml_kses( $input['account_news_ad_url'.$i] );
	}

	// お気に入り
	$input['account_like_label'] = wp_filter_nohtml_kses( $input['account_like_label'] );
	$input['account_like_list_num'] = wp_filter_nohtml_kses( $input['account_like_list_num'] );

	// お知らせ通知
	$input['use_member_news_notify'] = ! empty( $input['use_member_news_notify'] ) ? 1 : 0;
	if ( ! isset( $input['member_news_notify_schedule_type'] ) || ! array_key_exists( $input['member_news_notify_schedule_type'], $notify_schedule_type_options ) )
		$input['member_news_notify_schedule_type'] = $dp_default_options['member_news_notify_schedule_type'];
	$input['member_news_notify_schedule_type2'] = absint( $input['member_news_notify_schedule_type2'] );
	if ( ! $input['member_news_notify_schedule_type2'] )
		$input['member_news_notify_schedule_type2'] = $dp_default_options['member_news_notify_schedule_type2'];
	$input['member_news_notify_schedule_type3'] = absint( $input['member_news_notify_schedule_type3'] );
	if ( 6 < $input['member_news_notify_schedule_type3'] )
		$input['member_news_notify_schedule_type3'] = $dp_default_options['member_news_notify_schedule_type3'];
	if ( ! empty( $input['member_news_notify_schedule_type4'] ) && is_array( $input['member_news_notify_schedule_type4'] ) ) {
		$member_news_notify_schedule_type4 = array();
		foreach( $input['member_news_notify_schedule_type4'] as $key => $value ) {
			$value = absint( $value );
			if ( 1 <= $value && 31 >= $value ) {
				$member_news_notify_schedule_type4[] = $value;
			}
		}
		$input['member_news_notify_schedule_type4'] = $member_news_notify_schedule_type4;
	}
	if ( empty( $input['member_news_notify_schedule_type4'] ) )
		$input['member_news_notify_schedule_type4'] = $dp_default_options['member_news_notify_schedule_type4'];

	$input['member_news_notify_hour'] = absint( $input['member_news_notify_hour'] );
	if ( 23 < $input['member_news_notify_hour'] )
		$input['member_news_notify_hour'] = 23;
	$input['member_news_notify_hour'] = sprintf( '%02d', $input['member_news_notify_hour'] );
	$input['member_news_notify_minute'] = absint( $input['member_news_notify_minute'] );
	if ( 59 < $input['member_news_notify_minute'] )
		$input['member_news_notify_minute'] = 59;
	$input['member_news_notify_minute'] = sprintf( '%02d', $input['member_news_notify_minute'] );
	$input['mail_member_news_notify_subject'] = $input['mail_member_news_notify_subject'];
	$input['mail_member_news_notify_body'] = $input['mail_member_news_notify_body'];

	// 通知スケジュールイベント
	set_tcd_membership_notify_schedule_event( $input, $GLOBALS['dp_options'] );

	return $input;
}


?>
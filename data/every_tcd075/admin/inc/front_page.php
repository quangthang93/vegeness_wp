<?php
/*
 * トップページの設定
 */


// Add default values
add_filter( 'before_getting_design_plus_option', 'add_front_page_dp_default_options' );


// Add label of front page tab
add_action( 'tcd_tab_labels', 'add_front_page_tab_label' );


// Add HTML of front page tab
add_action( 'tcd_tab_panel', 'add_front_page_tab_panel' );


// Register sanitize function
add_filter( 'theme_options_validate', 'add_front_page_theme_options_validate' );


// タブの名前
function add_front_page_tab_label( $tab_labels ) {
	$tab_labels['front_page'] = __( 'Front page', 'tcd-w' );
	return $tab_labels;
}


// 初期値
function add_front_page_dp_default_options( $dp_default_options ) {

  // ヘッダーコンテンツ
	$dp_default_options['index_header_content_type'] = 'type1';

  // 画像スライダー
	$dp_default_options['index_slider_time'] = '7000';
	$dp_default_options['index_slider_animation_type'] = 'type1';
	for ( $i = 1; $i <= 3; $i++ ) {
		$dp_default_options['index_slider_image' . $i] = false;
		$dp_default_options['index_slider_image_mobile' . $i] = false;

		$dp_default_options['index_slider_catch' . $i] = __( 'Catchprase will be displayed here.', 'tcd-w' );
		$dp_default_options['index_slider_catch_font_type' . $i] = 'type3';
		$dp_default_options['index_slider_catch_font_size' . $i] = '38';
		$dp_default_options['index_slider_catch_font_size_mobile' . $i] = '20';
		$dp_default_options['index_slider_catch_color' . $i] = '#FFFFFF';

		$dp_default_options['index_slider_desc' . $i] = __( 'Description will be displayed here.<br />Description will be displayed here.', 'tcd-w' );
		$dp_default_options['index_slider_desc_font_size' . $i] = '16';
		$dp_default_options['index_slider_desc_font_size_mobile' . $i] = '13';
		$dp_default_options['index_slider_desc_color' . $i] = '#FFFFFF';

		$dp_default_options['index_slider_use_shadow' . $i] = 0;
		$dp_default_options['index_slider_shadow_h'.$i] = 0;
		$dp_default_options['index_slider_shadow_v'.$i] = 0;
		$dp_default_options['index_slider_shadow_b'.$i] = 0;
		$dp_default_options['index_slider_shadow_c'.$i] = '#888888';

		$dp_default_options['index_slider_content_direction' . $i] = 'type2';

		$dp_default_options['index_slider_show_button' . $i] = 1;
		$dp_default_options['index_slider_button_label' . $i] = 'MORE DETAIL';
		$dp_default_options['index_slider_button_bg_color' . $i] = '#ff7f00';
		$dp_default_options['index_slider_button_font_color' . $i] = '#FFFFFF';
		$dp_default_options['index_slider_button_bg_color_hover' . $i] = '#fbc525';
		$dp_default_options['index_slider_button_font_color_hover' . $i] = '#FFFFFF';
		$dp_default_options['index_slider_url' . $i] = '#';
		$dp_default_options['index_slider_target' . $i] = 1;

		$dp_default_options['index_slider_use_overlay' . $i] = 1;
		$dp_default_options['index_slider_overlay_color' . $i] = '#000000';
		$dp_default_options['index_slider_overlay_opacity' . $i] = '0.3';
		$dp_default_options['index_slider_use_overlay_mobile' . $i] = '';
		$dp_default_options['index_slider_overlay_color_mobile' . $i] = '#000000';
		$dp_default_options['index_slider_overlay_opacity_mobile' . $i] = '0.3';
	}

  //画像スライダースマホ用設定
	$dp_default_options['index_slider_mobile_content_type'] = 'type1';
	$dp_default_options['index_slider_mobile_logo_image'] = '';
	$dp_default_options['index_slider_mobile_logo_image_width'] = '';

	$dp_default_options['index_slider_mobile_catch'] = '';
	$dp_default_options['index_slider_mobile_catch_font_type'] = 'type2';
	$dp_default_options['index_slider_mobile_catch_font_size'] = '28';
	$dp_default_options['index_slider_mobile_catch_color'] = '#FFFFFF';
	$dp_default_options['index_slider_mobile_use_shadow'] = 0;
	$dp_default_options['index_slider_mobile_shadow_h'] = 0;
	$dp_default_options['index_slider_mobile_shadow_v'] = 0;
	$dp_default_options['index_slider_mobile_shadow_b'] = 0;
	$dp_default_options['index_slider_mobile_shadow_c'] = '#888888';

  // 動画
	$dp_default_options['index_video'] = false;

  // Youtube
	$dp_default_options['index_youtube_url'] = '';

	// 代替画像
	$dp_default_options['index_movie_image'] = false;

  // 動画用コンテンツ
	$dp_default_options['index_movie_content_type'] = 'type3';
	$dp_default_options['index_movie_bottom_content_type'] = 'type1';

	$dp_default_options['index_movie_catch'] = __( 'Catchprase will be displayed here.', 'tcd-w' );
	$dp_default_options['index_movie_catch_font_type'] = 'type3';
	$dp_default_options['index_movie_catch_font_size'] = '38';
	$dp_default_options['index_movie_catch_font_size_mobile'] = '20';
	$dp_default_options['index_movie_catch_color'] = '#FFFFFF';

	$dp_default_options['index_movie_desc'] = __( 'Description will be displayed here.<br />Description will be displayed here.', 'tcd-w' );
	$dp_default_options['index_movie_desc_font_size'] = '16';
	$dp_default_options['index_movie_desc_font_size_mobile'] = '13';
	$dp_default_options['index_movie_desc_color'] = '#FFFFFF';

	$dp_default_options['index_movie_use_shadow'] = 0;
	$dp_default_options['index_movie_shadow_h'] = 0;
	$dp_default_options['index_movie_shadow_v'] = 0;
	$dp_default_options['index_movie_shadow_b'] = 0;
	$dp_default_options['index_movie_shadow_c'] = '#888888';

	$dp_default_options['index_movie_show_button'] = 0;
	$dp_default_options['index_movie_button_label'] = 'MORE DETAIL';
	$dp_default_options['index_movie_button_bg_color'] = '#ff7f00';
	$dp_default_options['index_movie_button_font_color'] = '#FFFFFF';
	$dp_default_options['index_movie_button_bg_color_hover'] = '#fbc525';
	$dp_default_options['index_movie_button_font_color_hover'] = '#FFFFFF';
	$dp_default_options['index_movie_url'] = '#';
	$dp_default_options['index_movie_target'] = 1;

	$dp_default_options['index_movie_use_overlay'] = 1;
	$dp_default_options['index_movie_overlay_color'] = '#000000';
	$dp_default_options['index_movie_overlay_opacity'] = '0.3';

  // 動画用コンテンツ（スマホ用）
	$dp_default_options['index_movie_mobile_content_type'] = 'type1';

	$dp_default_options['index_movie_content_type_mobile'] = 'type3';

	$dp_default_options['index_movie_catch_mobile'] = __( 'Catchprase will be displayed here.', 'tcd-w' );
	$dp_default_options['index_movie_catch_font_type_mobile'] = 'type3';
	$dp_default_options['index_movie_mobile_catch_font_size'] = '20';
	$dp_default_options['index_movie_catch_color_mobile'] = '#FFFFFF';

	$dp_default_options['index_movie_use_shadow_mobile'] = 0;
	$dp_default_options['index_movie_shadow_h_mobile'] = 0;
	$dp_default_options['index_movie_shadow_v_mobile'] = 0;
	$dp_default_options['index_movie_shadow_b_mobile'] = 0;
	$dp_default_options['index_movie_shadow_c_mobile'] = '#888888';

  // オーバーレイ
	$dp_default_options['use_header_overlay'] = 1;
	$dp_default_options['header_overlay_color'] = '#000000';
	$dp_default_options['header_overlay_opacity'] = '0.3';
	$dp_default_options['use_header_overlay_gd'] = 1;


  // 総レシピ数
	$dp_default_options['show_index_total_recipe'] = 1;
	$dp_default_options['index_total_recipe_color'] = '#000000';
	$dp_default_options['index_total_recipe_headline'] = __( 'Total recipes', 'tcd-w' );
	$dp_default_options['index_total_recipe_label'] = __( 'recipes', 'tcd-w' );


  // レシピスライダー
	$dp_default_options['show_index_recipe_slider'] = 1;
	$dp_default_options['index_recipe_slider_num'] = 9;
	$dp_default_options['index_recipe_slider_post_type'] = 'all';
	$dp_default_options['index_recipe_slider_show_premium_icon'] = 1;
	$dp_default_options['index_recipe_slider_show_category'] = 1;
	$dp_default_options['index_recipe_slider_show_count'] = 1;
	$dp_default_options['index_recipe_slider_font_size'] = '16';
	$dp_default_options['index_recipe_slider_font_size_mobile'] = '14';

  // 無料会員登録メッセージ
	$dp_default_options['show_index_welcome'] = 'type1';
	$dp_default_options['index_welcome_catch'] = __( 'Catchprase will be displayed here.', 'tcd-w' );
	$dp_default_options['index_welcome_desc'] = __( 'Description will be displayed here.<br />Description will be displayed here.', 'tcd-w' );
	$dp_default_options['index_welcome_catch_font_type'] = 'type3';
	$dp_default_options['index_welcome_catch_font_size'] = '38';
	$dp_default_options['index_welcome_catch_font_size_mobile'] = '20';
	$dp_default_options['index_welcome_desc_font_size'] = '18';
	$dp_default_options['index_welcome_desc_font_size_mobile'] = '15';
	$dp_default_options['index_welcome_show_button'] = 0;
	$dp_default_options['index_welcome_button_label'] = __( 'Join Free', 'tcd-w' );
	$dp_default_options['index_welcome_button_bg_color'] = '#ff7f00';
	$dp_default_options['index_welcome_button_font_color'] = '#FFFFFF';
	$dp_default_options['index_welcome_button_bg_color_hover'] = '#fbc525';
	$dp_default_options['index_welcome_button_font_color_hover'] = '#FFFFFF';


  // コンテンツビルダー
	$dp_default_options['contents_builder'] = array(
		array(
			"cb_content_select" => "recent_recipe_list",
			"cb_display" => "type1",
			"recent_headline" => __( 'Recent recipe', 'tcd-w' ),
			"recent_headline_font_size" => '20',
			"recent_headline_font_size_mobile" => '15',
			"recent_headline_font_color" => "#000000",
			"recent_headline_bg_color" => "#ffffff",
			"recent_headline_border_color" => "#dddddd",
			"hide_recent_headline_icon" => 0,
			"recent_headline_icon_color" => "#000000",
			"recent_headline_image" => false,
			"recent_desc" => '',
			"recent_desc_font_size" => '16',
			"recent_desc_font_size_mobile" => '13',
			"recent_num" => '6',
			"recent_title_font_size" => '16',
			"recent_title_font_size_mobile" => '14',
			"recent_show_category" => 1,
			"recent_show_date" => 1,
			"recent_show_premium" => 1,
		),
		array(
			"cb_content_select" => "news_list",
			"cb_display" => "type1",
			"news_headline" => __( 'News', 'tcd-w' ),
			"news_headline_font_color" => "#000000",
			"news_headline_bg_color" => "#ffffff",
			"news_headline_border_color" => "#dddddd",
			"hide_news_headline_icon" => 0,
			"news_headline_icon_color" => "#000000",
			"news_headline_image" => false,
			"news_headline_font_size" => "20",
			"news_headline_font_size_mobile" => "15",
			"news_desc" => "",
			"news_desc_font_size" => "16",
			"news_desc_font_size_mobile" => "13",
			"news_num" => "3",
			"news_date_color" => "#ff0000"
		),
		array(
			"cb_content_select" => "banner",
			"cb_display" => "type1",
			"banner_title1" => __( 'Banner title', 'tcd-w' ),
			"banner_title_font_type1" => "type3",
			"banner_sub_title1" => __( 'Banner sub title', 'tcd-w' ),
			"banner_title_font_size1" => "24",
			"banner_sub_title_font_size1" => "14",
			"banner_font_color1" => "#ffffff",
			"banner_image1" => false,
			"banner_url1" => "#",
			"banner_target1" => 1,
			"banner_use_overlay1" => 1,
			"banner_overlay_color1" => "#000000",
			"banner_title2" => __( 'Banner title', 'tcd-w' ),
			"banner_title_font_type2" => "type3",
			"banner_sub_title2" => __( 'Banner sub title', 'tcd-w' ),
			"banner_title_font_size2" => "24",
			"banner_sub_title_font_size2" => "14",
			"banner_font_color2" => "#ffffff",
			"banner_image2" => false,
			"banner_url2" => "#",
			"banner_target2" => 1,
			"banner_use_overlay2" => 1,
			"banner_overlay_color2" => "#000000",
			"banner_title3" => "",
			"banner_title_font_type3" => "type3",
			"banner_sub_title3" => "",
			"banner_title_font_size3" => "26",
			"banner_sub_title_font_size3" => "16",
			"banner_font_color3" => "#ffffff",
			"banner_image3" => false,
			"banner_url3" => "",
			"banner_target3" => 0,
			"banner_use_overlay3" => 1,
			"banner_overlay_color3" => "#000000",
			"banner_title4" => "",
			"banner_title_font_type4" => "type3",
			"banner_sub_title4" => "",
			"banner_title_font_size4" => "26",
			"banner_sub_title_font_size4" => "16",
			"banner_font_color4" => "#ffffff",
			"banner_image4" => false,
			"banner_url4" => "",
			"banner_target4" => 0,
			"banner_use_overlay4" => 1,
			"banner_overlay_color4" => "#000000"
		),
		array(
			"cb_content_select" => "popular_recipe_list",
			"cb_display" => "type1",
			"popular_headline" => __( 'Popular recipe', 'tcd-w' ),
			"popular_headline_font_color" => "#000000",
			"popular_headline_bg_color" => "#ffffff",
			"popular_headline_border_color" => "#dddddd",
			"hide_popular_headline_icon" => 0,
			"popular_headline_icon_color" => "#000000",
			"popular_headline_image" => "",
			"popular_headline_font_size" => "20",
			"popular_headline_font_size_mobile" => "15",
			"popular_desc" => __( 'Description will be displayed here.<br />Description will be displayed here.', 'tcd-w' ),
			"popular_desc_font_size" => "16",
			"popular_desc_font_size_mobile" => "13",
			"popular_num" => "9",
			"popular_range" => "",
			"popular_title_font_size" => "16",
			"popular_title_font_size_mobile" => "14",
			"popular_show_premium" => 1,
			"popular_show_category" => 1,
			"popular_show_post_view" => 1
		),
		array(
			"cb_content_select" => "recent_blog_list",
			"cb_display" => "type1",
			"recent_blog_headline" => __( 'Recent blog', 'tcd-w' ),
			"recent_blog_headline_font_color" => "#000000",
			"recent_blog_headline_bg_color" => "#ffffff",
			"recent_blog_headline_border_color" => "#dddddd",
			"hide_recent_blog_headline_icon" => 0,
			"recent_blog_headline_icon_color" => "#000000",
			"recent_blog_headline_image" => "",
			"recent_blog_headline_font_size" => "20",
			"recent_blog_headline_font_size_mobile" => "15",
			"recent_blog_desc" => "",
			"recent_blog_desc_font_size" => "16",
			"recent_blog_desc_font_size_mobile" => "13",
			"recent_blog_num" => "6",
			"recent_blog_title_font_size" => "16",
			"recent_blog_title_font_size_mobile" => "14",
			"recent_blog_show_date" => 1,
			"recent_blog_show_premium" => 1
		),
		array(
			"cb_content_select" => "featured_recipe_list",
			"cb_display" => "type1",
			"featured_headline" => __( 'Featured recipe', 'tcd-w' ),
			"featured_headline_font_color" => "#000000",
			"featured_headline_bg_color" => "#ffffff",
			"featured_headline_border_color" => "#dddddd",
			"hide_featured_headline_icon" => 0,
			"featured_headline_icon_color" => "#000000",
			"featured_headline_image" => false,
			"featured_headline_font_size" => "20",
			"featured_headline_font_size_mobile" => "15",
			"featured_desc" => __( 'Description will be displayed here.<br />Description will be displayed here.', 'tcd-w' ),
			"featured_desc_font_size" => "16",
			"featured_desc_font_size_mobile" => "13",
			"featured_banner_type1" => "type1",
			"featured_title1" => __( 'Title will be displayed here', 'tcd-w' ),
			"featured_title_font_type1" => "type3",
			"featured_sub_title1" => __( 'Sub title', 'tcd-w' ),
			"featured_title_font_size1" => "26",
			"featured_sub_title_font_size1" => "16",
			"featured_title_font_size_mobile1" => "20",
			"featured_sub_title_font_size_mobile1" => "13",
			"featured_type1_font_color1" => "#ff8000",
			"featured_type1_border_color1" => "#ff8000",
			"featured_type2_font_color1" => "#ffffff",
			"featured_image1" => false,
			"featured_page1" => "",
			"featured_url1" => "#",
			"featured_target1" => 1,
			"featured_use_overlay1" => 1,
			"featured_overlay_color1" => "#000000",
			"featured_banner_type2" => "type2",
			"featured_title2" => __( 'Title will be displayed here', 'tcd-w' ),
			"featured_title_font_type2" => "type3",
			"featured_sub_title2" => "",
			"featured_title_font_size2" => "26",
			"featured_sub_title_font_size2" => "16",
			"featured_title_font_size_mobile2" => "20",
			"featured_sub_title_font_size_mobile2" => "13",
			"featured_type1_font_color2" => "#ff8000",
			"featured_type1_border_color2" => "#ff8000",
			"featured_type2_font_color2" => "#ffffff",
			"featured_image2" => false,
			"featured_page2" => "",
			"featured_url2" => "#",
			"featured_target2" => 1,
			"featured_use_overlay2" => 1,
			"featured_overlay_color2" => "#000000",
			"featured_banner_type3" => "type1",
			"featured_title3" => '',
			"featured_title_font_type3" => "type3",
			"featured_sub_title3" =>  '',
			"featured_title_font_size3" => "26",
			"featured_sub_title_font_size3" => "16",
			"featured_title_font_size_mobile3" => "20",
			"featured_sub_title_font_size_mobile3" => "13",
			"featured_type1_font_color3" => "#ff8000",
			"featured_type1_border_color3" => "#ff8000",
			"featured_type2_font_color3" => "#ffffff",
			"featured_image3" => false,
			"featured_page3" => "",
			"featured_url3" => "#",
			"featured_target3" => 1,
			"featured_use_overlay3" => 1,
			"featured_overlay_color3" => "#000000",
			"featured_banner_type4" => "type1",
			"featured_title4" => "",
			"featured_title_font_type4" => "type3",
			"featured_sub_title4" => "",
			"featured_title_font_size4" => "26",
			"featured_sub_title_font_size4" => "16",
			"featured_title_font_size_mobile4" => "20",
			"featured_sub_title_font_size_mobile4" => "13",
			"featured_type1_font_color4" => "#ff8000",
			"featured_type1_border_color4" => "#ff8000",
			"featured_type2_font_color4" => "#ffffff",
			"featured_image4" => "",
			"featured_page4" => "",
			"featured_url4" => "",
			"featured_target4" => 0,
			"featured_use_overlay4" => 1,
			"featured_overlay_color4" => "#000000"
		)
	);

	return $dp_default_options;

}

// 入力欄の出力　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_front_page_tab_panel( $options ) {

  global $dp_default_options, $index_content_type_options, $time_options, $font_type_options, $content_direction_options, $index_slider_content_type_options, $index_slider_animation_type_options;
  $recipe_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );
  $news_label = $options['news_label'] ? esc_html( $options['news_label'] ) : __( 'News', 'tcd-w' );

?>

<div id="tab-content-logo" class="tab-content">

   <?php // ヘッダーコンテンツの設定 ---------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Header content setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">

     <?php // コンテンツのタイプ ---------- ?>
     <h4 class="theme_option_headline2"><?php _e('Background type', 'tcd-w');  ?></h4>
     <ul class="design_radio_button">
      <?php foreach ( $index_content_type_options as $option ) { ?>
      <li>
       <input type="radio" id="index_header_content_button_<?php esc_attr_e( $option['value'] ); ?>" name="dp_options[index_header_content_type]" value="<?php esc_attr_e( $option['value'] ); ?>" <?php checked( $options['index_header_content_type'], $option['value'] ); ?> />
       <label for="index_header_content_button_<?php esc_attr_e( $option['value'] ); ?>"><?php echo $option['label']; ?></label>
      </li>
      <?php } ?>
     </ul>

     <?php // 画像スライダー ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ ?>
     <div id="index_content_slider" style="<?php if($options['index_header_content_type'] == 'type1') { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <h4 class="theme_option_headline2"><?php _e('Image slider setting', 'tcd-w');  ?></h4>

      <?php // 繰り返し PCの設定----- ?>
      <?php for($i = 1; $i <= 3; $i++) : ?>
      <div class="sub_box cf"> 
       <h3 class="theme_option_subbox_headline"><?php printf(__('Slide%s setting', 'tcd-w'), $i); ?></h3>
       <div class="sub_box_content">
        <?php // 画像の設定 ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e( 'Image', 'tcd-w' ); ?></h4>
        <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '1450', '550'); ?></p>
        <div class="image_box cf index_slider_image<?php echo $i; ?>">
         <div class="cf cf_media_field hide-if-no-js index_slider_image<?php echo $i; ?>">
          <input type="hidden" value="<?php echo esc_attr( $options['index_slider_image'.$i] ); ?>" id="index_slider_image<?php echo $i; ?>" name="dp_options[index_slider_image<?php echo $i; ?>]" class="cf_media_id">
          <div class="preview_field"><?php if($options['index_slider_image'.$i]){ echo wp_get_attachment_image($options['index_slider_image'.$i], 'full'); }; ?></div>
          <div class="buttton_area">
           <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
           <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['index_slider_image'.$i]){ echo 'hidden'; }; ?>">
          </div>
         </div>
        </div>
        <?php // キャッチフレーズ ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e( 'Catchphrase setting', 'tcd-w' ); ?></h4>
        <textarea class="large-text" cols="50" rows="3" name="dp_options[index_slider_catch<?php echo $i; ?>]"><?php echo esc_textarea(  $options['index_slider_catch'.$i] ); ?></textarea>
        <ul class="option_list">
         <li class="cf"><span class="label"><?php _e('Font type', 'tcd-w');  ?></span>
          <select name="dp_options[index_slider_catch_font_type<?php echo $i; ?>]">
           <?php foreach ( $font_type_options as $option ) { ?>
           <option style="padding-right: 10px;" value="<?php echo esc_attr($option['value']); ?>" <?php selected( $options['index_slider_catch_font_type'.$i], $option['value'] ); ?>><?php echo $option['label']; ?></option>
           <?php } ?>
          </select>
         </li>
         <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_slider_catch_font_size<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_catch_font_size'.$i] ); ?>" /><span>px</span></li>
         <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_slider_catch_font_size_mobile<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_catch_font_size_mobile'.$i] ); ?>" /><span>px</span></li>
         <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_slider_catch_color<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_catch_color'.$i] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
        </ul>
        <?php // 説明文 ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e('Description', 'tcd-w');  ?></h4>
        <textarea class="large-text" cols="50" rows="3" name="dp_options[index_slider_desc<?php echo $i; ?>]"><?php echo esc_textarea(  $options['index_slider_desc'.$i] ); ?></textarea>
        <ul class="option_list">
         <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_slider_desc_font_size<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_desc_font_size'.$i] ); ?>" /><span>px</span></li>
         <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_slider_desc_font_size_mobile<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_desc_font_size_mobile'.$i] ); ?>" /><span>px</span></li>
         <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_slider_desc_color<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_desc_color'.$i] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
        </ul>
        <?php // テキストシャドウ ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e( 'Dropshadow setting', 'tcd-w' ); ?></h4>
        <p class="displayment_checkbox"><label><input name="dp_options[index_slider_use_shadow<?php echo $i; ?>]" type="checkbox" value="1" <?php checked( $options['index_slider_use_shadow'.$i], 1 ); ?>><?php _e( 'Use dropshadow on text content', 'tcd-w' ); ?></label></p>
        <div style="<?php if($options['index_slider_use_shadow'.$i] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
         <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
          <li class="cf"><span class="label"><?php _e('Dropshadow position (left)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_slider_shadow_h<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_shadow_h'.$i] ); ?>"><span>px</span></li>
          <li class="cf"><span class="label"><?php _e('Dropshadow position (top)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_slider_shadow_v<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_shadow_v'.$i] ); ?>"><span>px</span></li>
          <li class="cf"><span class="label"><?php _e('Dropshadow size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_slider_shadow_b<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_shadow_b'.$i] ); ?>"><span>px</span></li>
          <li class="cf"><span class="label"><?php _e('Dropshadow color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_slider_shadow_c<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_shadow_c'.$i] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
         </ul>
        </div>
        <?php // ボタン ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e('Button setting', 'tcd-w');  ?></h4>
        <p class="displayment_checkbox"><label><input class="index_slider_show_button<?php echo $i; ?>" name="dp_options[index_slider_show_button<?php echo $i; ?>]" type="checkbox" value="1" <?php checked( $options['index_slider_show_button'.$i], 1 ); ?>><?php _e( 'Display button', 'tcd-w' ); ?></label></p>
        <div style="<?php if($options['index_slider_show_button'.$i] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
         <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
          <li class="cf"><span class="label"><?php _e('label', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[index_slider_button_label<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_button_label'.$i] ); ?>" /></li>
          <li class="cf"><span class="label"><?php _e('URL', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[index_slider_url<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_url'.$i] ); ?>"></li>
          <li class="cf"><span class="label"><?php _e('Open link in new window', 'tcd-w'); ?></span><input name="dp_options[index_slider_target<?php echo $i; ?>]" type="checkbox" value="1" <?php checked( $options['index_slider_target'.$i], 1 ); ?>></li>
          <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_slider_button_bg_color<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_button_bg_color'.$i] ); ?>" data-default-color="#ff7f00" class="c-color-picker"></li>
          <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_slider_button_font_color<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_button_font_color'.$i] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
          <li class="cf"><span class="label"><?php _e('Background color on mouse over', 'tcd-w'); ?></span><input type="text" name="dp_options[index_slider_button_bg_color_hover<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_button_bg_color_hover'.$i] ); ?>" data-default-color="#fbc525" class="c-color-picker"></li>
          <li class="cf"><span class="label"><?php _e('Font color on mouse over', 'tcd-w'); ?></span><input type="text" name="dp_options[index_slider_button_font_color_hover<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_button_font_color_hover'.$i] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
         </ul>
        </div>
        <?php // オーバーレイ ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e( 'Overlay setting', 'tcd-w' ); ?></h4>
        <p class="displayment_checkbox"><label><input class="index_slider_use_overlay<?php echo $i; ?>" name="dp_options[index_slider_use_overlay<?php echo $i; ?>]" type="checkbox" value="1" <?php checked( $options['index_slider_use_overlay'.$i], 1 ); ?>><?php _e( 'Use overlay', 'tcd-w' ); ?></label></p>
        <div style="<?php if($options['index_slider_use_overlay'.$i] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
         <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
          <li class="cf"><span class="label"><?php _e('Color of overlay', 'tcd-w'); ?></span><input class="c-color-picker index_slider_overlay_color<?php echo $i; ?>" type="text" name="dp_options[index_slider_overlay_color<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_overlay_color'.$i] ); ?>" data-default-color="#000000"></li>
          <li class="cf"><span class="label"><?php _e('Transparency of overlay', 'tcd-w'); ?></span><input class="hankaku index_slider_overlay_opacity<?php echo $i; ?>" style="width:70px;" type="number" max="1" min="0" step="0.1" name="dp_options[index_slider_overlay_opacity<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_overlay_opacity'.$i] ); ?>" /><p><?php _e('Please specify the number of 0.1 from 0.9. Overlay color will be more transparent as the number is small.', 'tcd-w');  ?></p></li>
         </ul>
        </div>
        <?php // コンテンツの方向 ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e('Direction of content', 'tcd-w');  ?></h4>
        <ul class="design_radio_button">
         <?php foreach ( $content_direction_options as $option ) { ?>
         <li>
          <input type="radio" id="index_slider_content_direction<?php echo $i . '_' . esc_attr($option['value']); ?>" name="dp_options[index_slider_content_direction<?php echo $i; ?>]" value="<?php esc_attr_e( $option['value'] ); ?>" <?php checked( $options['index_slider_content_direction'.$i], $option['value'] ); ?> />
          <label for="index_slider_content_direction<?php echo $i . '_' . esc_attr($option['value']); ?>"><?php echo $option['label']; ?></label>
         </li>
         <?php } ?>
        </ul>
        <ul class="button_list cf">
         <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
         <li><a class="close_sub_box button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
        </ul>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php endfor; ?>
      <?php // 繰り返し PCの設定　ここまで ----- ?>

      <?php // モバイルサイズ専用設定 ---------------------------------------------------------------------------------------------------------------- ?>
      <div class="sub_box cf">
       <h3 class="theme_option_subbox_headline"><?php _e('Mobile size setting', 'tcd-w'); ?></h3>
       <div class="sub_box_content">
        <div class="theme_option_message2" style="margin-top:15px;">
         <p><?php _e( 'This content will be replaced by content above when the web browser become mobile size.', 'tcd-w' ); ?></p>
        </div>
        <?php //コンテンツの設定 -------------------------- ?>
        <h4 class="theme_option_headline2"><?php _e( 'Content type setting', 'tcd-w' ); ?></h4>
        <ul class="design_radio_button">
         <?php foreach ( $index_slider_content_type_options as $option ) { ?>
         <li class="index_slider_mobile_content_<?php esc_attr_e( $option['value'] ); ?>_button">
          <input type="radio" id="index_slider_mobile_content_type_<?php esc_attr_e( $option['value'] ); ?>" name="dp_options[index_slider_mobile_content_type]" value="<?php esc_attr_e( $option['value'] ); ?>" <?php checked( $options['index_slider_mobile_content_type'], $option['value'] ); ?> />
          <label for="index_slider_mobile_content_type_<?php esc_attr_e( $option['value'] ); ?>"><?php echo $option['label']; ?></label>
         </li>
         <?php } ?>
        </ul>
        <?php // スマホ専用キャッチフレーズ ------------------------ ?>
        <div class="index_slider_catch_area_mobile" style="<?php if($options['index_slider_mobile_content_type'] == 'type2') { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
         <h4 class="theme_option_headline2"><?php _e( 'Catchphrase setting', 'tcd-w' ); ?></h4>
         <textarea class="large-text" cols="50" rows="3" name="dp_options[index_slider_mobile_catch]"><?php echo esc_textarea(  $options['index_slider_mobile_catch'] ); ?></textarea>
         <ul class="option_list">
          <li class="cf"><span class="label"><?php _e('Font type', 'tcd-w');  ?></span>
           <select name="dp_options[index_slider_mobile_catch_font_type]">
            <?php foreach ( $font_type_options as $option ) { ?>
            <option style="padding-right: 10px;" value="<?php echo esc_attr($option['value']); ?>" <?php selected( $options['index_slider_mobile_catch_font_type'], $option['value'] ); ?>><?php echo $option['label']; ?></option>
            <?php } ?>
           </select>
          </li>
          <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_slider_mobile_catch_font_size]" value="<?php echo esc_attr( $options['index_slider_mobile_catch_font_size'] ); ?>" /><span>px</span></li>
          <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_slider_mobile_catch_color]" value="<?php echo esc_attr( $options['index_slider_mobile_catch_color'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
         </ul>
         <?php // テキストシャドウ ----------------------- ?>
         <h4 class="theme_option_headline2"><?php _e( 'Dropshadow setting', 'tcd-w' ); ?></h4>
         <p class="displayment_checkbox"><label><input name="dp_options[index_slider_mobile_use_shadow]" type="checkbox" value="1" <?php checked( $options['index_slider_mobile_use_shadow'], 1 ); ?>><?php _e( 'Use dropshadow on text content', 'tcd-w' ); ?></label></p>
         <div style="<?php if($options['index_slider_mobile_use_shadow'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
          <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
           <li class="cf"><span class="label"><?php _e('Dropshadow position (left)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_slider_mobile_shadow_h]" value="<?php echo esc_attr( $options['index_slider_mobile_shadow_h'] ); ?>"><span>px</span></li>
           <li class="cf"><span class="label"><?php _e('Dropshadow position (top)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_slider_mobile_shadow_v]" value="<?php echo esc_attr( $options['index_slider_mobile_shadow_v'] ); ?>"><span>px</span></li>
           <li class="cf"><span class="label"><?php _e('Dropshadow size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_slider_mobile_shadow_b]" value="<?php echo esc_attr( $options['index_slider_mobile_shadow_b'] ); ?>"><span>px</span></li>
           <li class="cf"><span class="label"><?php _e('Dropshadow color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_slider_mobile_shadow_c]" value="<?php echo esc_attr( $options['index_slider_mobile_shadow_c'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
          </ul>
         </div>
        </div><!-- END .index_slider_catch_area_mobile -->
        <?php // 繰り返し 画像の設定 ----------------------------- ?>
        <?php for($i = 1; $i <= 3; $i++) : ?>
        <div class="sub_box cf"<?php if($i == 1){ echo ' style="margin-top:25px;"'; }; ?>>
         <h3 class="theme_option_subbox_headline"><?php printf(__('Slide%s setting (mobile size)', 'tcd-w'), $i); ?></h3>
         <div class="sub_box_content">
          <div class="theme_option_message2" style="margin-top:10px;">
           <p><?php _e( 'You can set images for mobile size. If it is not specified, PC image will be displayed instead.', 'tcd-w' ); ?></p>
          </div>
          <h4 class="theme_option_headline2"><?php _e( 'Image', 'tcd-w' ); ?></h4>
          <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '750', '1334'); ?></p>
          <div class="image_box cf">
           <div class="cf cf_media_field hide-if-no-js index_slider_image_mobile<?php echo $i; ?>">
            <input type="hidden" value="<?php echo esc_attr( $options['index_slider_image_mobile'.$i] ); ?>" id="index_slider_image_mobile<?php echo $i; ?>" name="dp_options[index_slider_image_mobile<?php echo $i; ?>]" class="cf_media_id">
            <div class="preview_field"><?php if($options['index_slider_image_mobile'.$i]){ echo wp_get_attachment_image($options['index_slider_image_mobile'.$i], 'full'); }; ?></div>
            <div class="buttton_area">
             <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
             <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['index_slider_image_mobile'.$i]){ echo 'hidden'; }; ?>">
            </div>
           </div>
          </div>
          <?php // オーバーレイ ----------------------- ?>
          <h4 class="theme_option_headline2"><?php _e( 'Overlay setting', 'tcd-w' ); ?></h4>
          <p class="displayment_checkbox"><label><input class="index_slider_use_overlay_mobile<?php echo $i; ?>" name="dp_options[index_slider_use_overlay_mobile<?php echo $i; ?>]" type="checkbox" value="1" <?php checked( $options['index_slider_use_overlay_mobile'.$i], 1 ); ?>><?php _e( 'Use overlay', 'tcd-w' ); ?></label></p>
          <div class="index_slider_show_overlay_mobile" style="<?php if($options['index_slider_use_overlay_mobile'.$i] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
           <ul class="color_field" style="border-top:1px dotted #ccc; padding-top:12px;">
            <li class="cf"><span class="label"><?php _e('Color of overlay', 'tcd-w'); ?></span><input class="c-color-picker index_slider_overlay_color_mobile<?php echo $i; ?>" type="text" name="dp_options[index_slider_overlay_color_mobile<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_overlay_color_mobile'.$i] ); ?>" data-default-color="#000000"></li>
            <li class="cf"><span class="label"><?php _e('Transparency of overlay', 'tcd-w'); ?></span><input class="hankaku index_slider_overlay_opacity_mobile<?php echo $i; ?>" style="width:70px;" type="number" max="1" min="0" step="0.1" name="dp_options[index_slider_overlay_opacity_mobile<?php echo $i; ?>]" value="<?php echo esc_attr( $options['index_slider_overlay_opacity_mobile'.$i] ); ?>" /><p><?php _e('Please specify the number of 0.1 from 0.9. Overlay color will be more transparent as the number is small.', 'tcd-w');  ?></p></li>
           </ul>
          </div><!-- END .index_slider_show_overlay_mobile -->
          <ul class="button_list cf">
           <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
           <li><a class="close_sub_box button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
          </ul>
         </div><!-- END .sub_box_content -->
        </div><!-- END .sub_box -->
        <?php endfor; ?>
        <?php // 繰り返し ここまで ----- ?>
        <ul class="button_list cf">
         <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
         <li><a class="close_sub_box button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
        </ul>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php // モバイルサイズ用設定ここまで --------------- ?>

      <?php // スピードの設定 ---------- ?>
      <h4 class="theme_option_headline2"><?php _e('Slider speed setting', 'tcd-w');  ?></h4>
      <select name="dp_options[index_slider_time]">
       <?php
            $i = 1;
            foreach ( $time_options as $option ):
              if( $i >= 5 && $i <= 10 ){
       ?>
       <option style="padding-right: 10px;" value="<?php echo esc_attr( $option['value'] ); ?>" <?php selected( $options['index_slider_time'], $option['value'] ); ?>><?php echo esc_html($option['label']); ?></option>
       <?php
              }
              $i++;
           endforeach;
       ?>
      </select>

      <?php // アニメーションのタイプ ---------- ?>
      <h4 class="theme_option_headline2"><?php _e('Animation type', 'tcd-w');  ?></h4>
      <ul class="design_radio_button">
       <?php foreach ( $index_slider_animation_type_options as $option ) { ?>
       <li>
        <input type="radio" id="index_slider_animation_type_<?php esc_attr_e( $option['value'] ); ?>" name="dp_options[index_slider_animation_type]" value="<?php esc_attr_e( $option['value'] ); ?>" <?php checked( $options['index_slider_animation_type'], $option['value'] ); ?> />
        <label for="index_slider_animation_type_<?php esc_attr_e( $option['value'] ); ?>"><?php echo $option['label']; ?></label>
       </li>
       <?php } ?>
      </ul>

     </div><!-- END #header_content_slider スライダーの設定はここまで-->

     <?php // 動画 ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ ?>
     <div id="index_content_video" style="<?php if($options['index_header_content_type'] == 'type2') { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <h4 class="theme_option_headline2"><?php _e('Video setting', 'tcd-w');  ?></h4>
      <div class="theme_option_message2">
       <p><?php _e('Please upload MP4 format file.', 'tcd-w');  ?></p>
       <p><?php _e('Web browser takes few second to load the data of video so we recommend to use loading screen if you want to display video.', 'tcd-w'); ?></p>
      </div>
      <div class="image_box cf">
       <div class="cf cf_media_field hide-if-no-js index_video">
        <input type="hidden" value="<?php echo esc_attr( $options['index_video'] ); ?>" id="index_video" name="dp_options[index_video]" class="cf_media_id">
        <div class="preview_field preview_field_video">
         <?php if($options['index_video']){ ?>
         <h4><?php _e( 'Uploaded MP4 file', 'tcd-w' ); ?></h4>
         <p><?php echo esc_url(wp_get_attachment_url($options['index_video'])); ?></p>
         <?php }; ?>
        </div>
        <div class="buttton_area">
         <input type="button" value="<?php _e('Select MP4 file', 'tcd-w'); ?>" class="cfmf-select-video button">
         <input type="button" value="<?php _e('Remove MP4 file', 'tcd-w'); ?>" class="cfmf-delete-video button <?php if(!$options['index_video']){ echo 'hidden'; }; ?>">
        </div>
       </div>
      </div>
     </div><!-- END #header_content_video 動画の設定はここまで -->

     <?php // YouTube ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ ?>
     <div id="index_content_youtube" style="<?php if($options['index_header_content_type'] == 'type3') { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <h4 class="theme_option_headline2"><?php _e('Youtube setting', 'tcd-w');  ?></h4>
      <div class="theme_option_message2">
       <p><?php _e('Please enter Youtube URL.', 'tcd-w');  ?></p>
       <p><?php _e('Web browser takes few second to load the data of video so we recommend to use loading screen if you want to display video.', 'tcd-w'); ?></p>
      </div>
      <input id="dp_options[index_youtube_url]" class="regular-text" type="text" name="dp_options[index_youtube_url]" value="<?php esc_attr_e( $options['index_youtube_url'] ); ?>" />
     </div><!-- END #header_content_youtube YouTubeの設定はここまで -->

     <?php // 代替画像、動画用キャッチフレーズ ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ ?>
     <div id="index_movie_content" style="<?php if($options['index_header_content_type'] != 'type1') { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <?php // 代替画像 ----------- ?>
      <h4 class="theme_option_headline2"><?php _e('Substitute image', 'tcd-w');  ?></h4>
      <div class="theme_option_message2">
       <p><?php _e('If the mobile device can\'t play video this image will be displayed instead.', 'tcd-w');  ?></p>
      </div>
      <div class="image_box cf">
       <div class="cf cf_media_field hide-if-no-js index_movie_image">
        <input type="hidden" value="<?php echo esc_attr( $options['index_movie_image'] ); ?>" id="index_movie_image" name="dp_options[index_movie_image]" class="cf_media_id">
        <div class="preview_field"><?php if($options['index_movie_image']){ echo wp_get_attachment_image($options['index_movie_image'], 'full'); }; ?></div>
        <div class="buttton_area">
         <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
         <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['index_movie_image']){ echo 'hidden'; }; ?>">
        </div>
       </div>
      </div>
      <h4 class="theme_option_headline2"><?php _e('Contents setting', 'tcd-w');  ?></h4>
      <?php // PC用キャッチフレーズ ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ ?>
      <div class="sub_box cf">
       <h3 class="theme_option_subbox_headline"><?php _e('Default content', 'tcd-w'); ?></h3>
       <div class="sub_box_content">
        <?php // キャッチフレーズ ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e( 'Catchphrase setting', 'tcd-w' ); ?></h4>
        <textarea class="large-text" cols="50" rows="3" name="dp_options[index_movie_catch]"><?php echo esc_textarea(  $options['index_movie_catch'] ); ?></textarea>
        <ul class="option_list">
         <li class="cf"><span class="label"><?php _e('Font type', 'tcd-w');  ?></span>
          <select name="dp_options[index_movie_catch_font_type]">
           <?php foreach ( $font_type_options as $option ) { ?>
           <option style="padding-right: 10px;" value="<?php echo esc_attr($option['value']); ?>" <?php selected( $options['index_movie_catch_font_type'], $option['value'] ); ?>><?php echo $option['label']; ?></option>
           <?php } ?>
          </select>
         </li>
         <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_movie_catch_font_size]" value="<?php echo esc_attr( $options['index_movie_catch_font_size'] ); ?>" /><span>px</span></li>
         <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_movie_catch_font_size_mobile]" value="<?php echo esc_attr( $options['index_movie_catch_font_size_mobile'] ); ?>" /><span>px</span></li>
         <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_movie_catch_color]" value="<?php echo esc_attr( $options['index_movie_catch_color'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
        </ul>
        <?php // 説明文 ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e('Description', 'tcd-w');  ?></h4>
        <textarea class="large-text" cols="50" rows="3" name="dp_options[index_movie_desc]"><?php echo esc_textarea(  $options['index_movie_desc'] ); ?></textarea>
        <ul class="option_list">
         <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_movie_desc_font_size]" value="<?php echo esc_attr( $options['index_movie_desc_font_size'] ); ?>" /><span>px</span></li>
         <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_movie_desc_font_size_mobile]" value="<?php echo esc_attr( $options['index_movie_desc_font_size_mobile'] ); ?>" /><span>px</span></li>
         <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_movie_desc_color]" value="<?php echo esc_attr( $options['index_movie_desc_color'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
        </ul>
        <?php // テキストシャドウ ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e( 'Dropshadow setting', 'tcd-w' ); ?></h4>
        <p class="displayment_checkbox"><label><input name="dp_options[index_movie_use_shadow]" type="checkbox" value="1" <?php checked( $options['index_movie_use_shadow'], 1 ); ?>><?php _e( 'Use dropshadow on text content', 'tcd-w' ); ?></label></p>
        <div style="<?php if($options['index_movie_use_shadow'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
         <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
          <li class="cf"><span class="label"><?php _e('Dropshadow position (left)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_movie_shadow_h]" value="<?php echo esc_attr( $options['index_movie_shadow_h'] ); ?>"><span>px</span></li>
          <li class="cf"><span class="label"><?php _e('Dropshadow position (top)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_movie_shadow_v]" value="<?php echo esc_attr( $options['index_movie_shadow_v'] ); ?>"><span>px</span></li>
          <li class="cf"><span class="label"><?php _e('Dropshadow size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_movie_shadow_b]" value="<?php echo esc_attr( $options['index_movie_shadow_b'] ); ?>"><span>px</span></li>
          <li class="cf"><span class="label"><?php _e('Dropshadow color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_movie_shadow_c]" value="<?php echo esc_attr( $options['index_movie_shadow_c'] ); ?>" data-default-color="#888888" class="c-color-picker"></li>
         </ul>
        </div>
        <?php // ボタン ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e('Button setting', 'tcd-w');  ?></h4>
        <p class="displayment_checkbox"><label><input class="index_movie_show_button" name="dp_options[index_movie_show_button]" type="checkbox" value="1" <?php checked( $options['index_movie_show_button'], 1 ); ?>><?php _e( 'Display button', 'tcd-w' ); ?></label></p>
        <div style="<?php if($options['index_movie_show_button'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
         <ul class="option_list">
          <li class="cf"><span class="label"><?php _e('label', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[index_movie_button_label]" value="<?php echo esc_attr( $options['index_movie_button_label'] ); ?>" /></li>
          <li class="cf"><span class="label"><?php _e('URL', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[index_movie_url]" value="<?php echo esc_attr( $options['index_movie_url'] ); ?>"></li>
          <li class="cf"><span class="label"><?php _e('Open link in new window', 'tcd-w'); ?></span><input name="dp_options[index_movie_target]" type="checkbox" value="1" <?php checked( $options['index_movie_target'], 1 ); ?>></li>
          <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_movie_button_bg_color]" value="<?php echo esc_attr( $options['index_movie_button_bg_color'] ); ?>" data-default-color="#ff7f00" class="c-color-picker"></li>
          <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_movie_button_font_color]" value="<?php echo esc_attr( $options['index_movie_button_font_color'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
          <li class="cf"><span class="label"><?php _e('Background color on mouse over', 'tcd-w'); ?></span><input type="text" name="dp_options[index_movie_button_bg_color_hover]" value="<?php echo esc_attr( $options['index_movie_button_bg_color_hover'] ); ?>" data-default-color="#fbc525" class="c-color-picker"></li>
          <li class="cf"><span class="label"><?php _e('Font color on mouse over', 'tcd-w'); ?></span><input type="text" name="dp_options[index_movie_button_font_color_hover]" value="<?php echo esc_attr( $options['index_movie_button_font_color_hover'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
         </ul>
        </div>
        <?php // オーバーレイ ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e( 'Overlay setting', 'tcd-w' ); ?></h4>
        <p class="displayment_checkbox"><label><input class="index_movie_use_overlay" name="dp_options[index_movie_use_overlay]" type="checkbox" value="1" <?php checked( $options['index_movie_use_overlay'], 1 ); ?>><?php _e( 'Use overlay', 'tcd-w' ); ?></label></p>
        <div style="<?php if($options['index_movie_use_overlay'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
         <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
          <li class="cf"><span class="label"><?php _e('Color of overlay', 'tcd-w'); ?></span><input class="c-color-picker index_movie_overlay_color" type="text" name="dp_options[index_movie_overlay_color]" value="<?php echo esc_attr( $options['index_movie_overlay_color'] ); ?>" data-default-color="#000000"></li>
          <li class="cf"><span class="label"><?php _e('Transparency of overlay', 'tcd-w'); ?></span><input class="hankaku index_movie_overlay_opacity" style="width:70px;" type="number" max="1" min="0" step="0.1" name="dp_options[index_movie_overlay_opacity]" value="<?php echo esc_attr( $options['index_movie_overlay_opacity'] ); ?>" /><p><?php _e('Please specify the number of 0.1 from 0.9. Overlay color will be more transparent as the number is small.', 'tcd-w');  ?></p></li>
         </ul>
        </div>
        <ul class="button_list cf">
         <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
         <li><a class="close_sub_box button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
        </ul>
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
      <?php // モバイルサイズ用キャッチフレーズ ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ ?>
      <div class="sub_box cf">
       <h3 class="theme_option_subbox_headline"><?php _e('Content for mobile size', 'tcd-w'); ?></h3>
       <div class="sub_box_content">
        <?php //コンテンツの設定 -------------------------- ?>
        <h4 class="theme_option_headline2"><?php _e( 'Content type setting', 'tcd-w' ); ?></h4>
        <div class="theme_option_message2" style="margin-top:15px;">
         <p><?php _e( 'This content will be replaced by content above when the web browser become mobile size.', 'tcd-w' ); ?></p>
        </div>
        <ul class="design_radio_button">
         <?php foreach ( $index_slider_content_type_options as $option ) { ?>
         <li class="index_movie_mobile_content_<?php esc_attr_e( $option['value'] ); ?>_button">
          <input type="radio" id="index_movie_mobile_content_type_<?php esc_attr_e( $option['value'] ); ?>" name="dp_options[index_movie_mobile_content_type]" value="<?php esc_attr_e( $option['value'] ); ?>" <?php checked( $options['index_movie_mobile_content_type'], $option['value'] ); ?> />
          <label for="index_movie_mobile_content_type_<?php esc_attr_e( $option['value'] ); ?>"><?php echo $option['label']; ?></label>
         </li>
         <?php } ?>
        </ul>
        <div id="index_movie_mobile_content_type2_area" style="<?php if($options['index_movie_mobile_content_type'] == 'type2') { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
        <?php // キャッチフレーズ ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e( 'Catchphrase setting', 'tcd-w' ); ?></h4>
        <textarea class="large-text" cols="50" rows="3" name="dp_options[index_movie_catch_mobile]"><?php echo esc_textarea(  $options['index_movie_catch_mobile'] ); ?></textarea>
        <ul class="option_list">
         <li class="cf"><span class="label"><?php _e('Font type', 'tcd-w');  ?></span>
          <select name="dp_options[index_movie_catch_font_type_mobile]">
           <?php foreach ( $font_type_options as $option ) { ?>
           <option style="padding-right: 10px;" value="<?php echo esc_attr($option['value']); ?>" <?php selected( $options['index_movie_catch_font_type_mobile'], $option['value'] ); ?>><?php echo $option['label']; ?></option>
           <?php } ?>
          </select>
         </li>
         <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_movie_mobile_catch_font_size]" value="<?php echo esc_attr( $options['index_movie_mobile_catch_font_size'] ); ?>" /><span>px</span></li>
         <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_movie_catch_color_mobile]" value="<?php echo esc_attr( $options['index_movie_catch_color_mobile'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
        </ul>
        <?php // テキストシャドウ ----------------------- ?>
        <h4 class="theme_option_headline2"><?php _e( 'Dropshadow setting', 'tcd-w' ); ?></h4>
        <p class="displayment_checkbox"><label><input name="dp_options[index_movie_use_shadow_mobile]" type="checkbox" value="1" <?php checked( $options['index_movie_use_shadow_mobile'], 1 ); ?>><?php _e( 'Use dropshadow on text content', 'tcd-w' ); ?></label></p>
        <div style="<?php if($options['index_movie_use_shadow_mobile'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
         <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
          <li class="cf"><span class="label"><?php _e('Dropshadow position (left)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_movie_shadow_h_mobile]" value="<?php echo esc_attr( $options['index_movie_shadow_h_mobile'] ); ?>"><span>px</span></li>
          <li class="cf"><span class="label"><?php _e('Dropshadow position (top)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_movie_shadow_v_mobile]" value="<?php echo esc_attr( $options['index_movie_shadow_v_mobile'] ); ?>"><span>px</span></li>
          <li class="cf"><span class="label"><?php _e('Dropshadow size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_movie_shadow_b_mobile]" value="<?php echo esc_attr( $options['index_movie_shadow_b_mobile'] ); ?>"><span>px</span></li>
          <li class="cf"><span class="label"><?php _e('Dropshadow color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_movie_shadow_c_mobile]" value="<?php echo esc_attr( $options['index_movie_shadow_c_mobile'] ); ?>" data-default-color="#888888" class="c-color-picker"></li>
         </ul>
        </div>
        <ul class="button_list cf">
         <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
         <li><a class="close_sub_box button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
        </ul>
       </div><!-- END use same setting -->
       </div><!-- END .sub_box_content -->
      </div><!-- END .sub_box -->
     </div><!-- END index_movie_content -->
     <?php // コンテンツの高さ ---------- ?>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->
   <?php // ヘッダーコンテンツここまで -- ?>


   <?php // 総レシピ数の設定 ----------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php printf(__('Total %s setting', 'tcd-w'), $recipe_label); ?></h3>
    <div class="theme_option_field_ac_content">
     <p class="displayment_checkbox"><label><input name="dp_options[show_index_total_recipe]" type="checkbox" value="1" <?php checked( $options['show_index_total_recipe'], 1 ); ?>><?php printf(__('Display total %s at header', 'tcd-w'), $recipe_label); ?></label></p>
     <div style="<?php if($options['show_index_recipe_slider'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
       <li class="cf"><span class="label"><?php _e('Headline', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[index_total_recipe_headline]" value="<?php echo esc_attr( $options['index_total_recipe_headline'] ); ?>" /></li>
       <li class="cf"><span class="label"><?php _e('Label of total count', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[index_total_recipe_label]" value="<?php echo esc_attr( $options['index_total_recipe_label'] ); ?>" /></li>
       <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_total_recipe_color]" value="<?php echo esc_attr( $options['index_total_recipe_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
      </ul>
     </div>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // レシピスライダーの設定 ----------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php printf(__('%s slider setting', 'tcd-w'), $recipe_label); ?></h3>
    <div class="theme_option_field_ac_content">
     <p class="displayment_checkbox"><label><input name="dp_options[show_index_recipe_slider]" type="checkbox" value="1" <?php checked( $options['show_index_recipe_slider'], 1 ); ?>><?php printf(__('Display %s slider', 'tcd-w'), $recipe_label); ?></label></p>
     <div style="<?php if($options['show_index_recipe_slider'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
       <li class="cf"><span class="label"><?php _e('Post type', 'tcd-w');  ?></span>
        <select name="dp_options[index_recipe_slider_post_type]">
         <option style="padding-right: 10px;" value="all" <?php selected( $options['index_recipe_slider_post_type'], 'all' ); ?>><?php printf(__('All %s', 'tcd-w'), $recipe_label); ?></option>
         <option style="padding-right: 10px;" value="premium_recipe" <?php selected( $options['index_recipe_slider_post_type'], 'premium_recipe' ); ?>><?php printf(__('Premium %s', 'tcd-w'), $recipe_label); ?></option>
         <option style="padding-right: 10px;" value="featured_recipe1" <?php selected( $options['index_recipe_slider_post_type'], 'featured_recipe1' ); ?>><?php printf(__('Featured %s', 'tcd-w'), $recipe_label); ?>1</option>
         <option style="padding-right: 10px;" value="featured_recipe2" <?php selected( $options['index_recipe_slider_post_type'], 'featured_recipe2' ); ?>><?php printf(__('Featured %s', 'tcd-w'), $recipe_label); ?>2</option>
         <option style="padding-right: 10px;" value="featured_recipe3" <?php selected( $options['index_recipe_slider_post_type'], 'featured_recipe3' ); ?>><?php printf(__('Featured %s', 'tcd-w'), $recipe_label); ?>3</option>
        </select>
       </li>
       <li class="cf"><span class="label"><?php _e('Number of post to display', 'tcd-w');  ?></span>
        <select name="dp_options[index_recipe_slider_num]">
         <?php for($i=6; $i<= 20; $i++): ?>
         <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $options['index_recipe_slider_num'], $i ); ?>><?php echo esc_html($i); ?></option>
         <?php endfor; ?>
        </select>
       </li>
       <li class="cf"><span class="label"><?php _e('Font size of title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_recipe_slider_font_size]" value="<?php esc_attr_e( $options['index_recipe_slider_font_size'] ); ?>" /><span>px</span></li>
       <li class="cf"><span class="label"><?php _e('Font size of title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_recipe_slider_font_size_mobile]" value="<?php esc_attr_e( $options['index_recipe_slider_font_size_mobile'] ); ?>" /><span>px</span></li>
       <li class="cf"><span class="label"><?php printf(__('Display premium %s icon', 'tcd-w'), $recipe_label); ?></span><input name="dp_options[index_recipe_slider_show_premium_icon]" type="checkbox" value="1" <?php checked( '1', $options['index_recipe_slider_show_premium_icon'] ); ?> /></li>
       <li class="cf"><span class="label"><?php _e('Display category', 'tcd-w');  ?></span><input name="dp_options[index_recipe_slider_show_category]" type="checkbox" value="1" <?php checked( '1', $options['index_recipe_slider_show_category'] ); ?> /></li>
       <li class="cf"><span class="label"><?php _e('Display post view count', 'tcd-w');  ?></span><input name="dp_options[index_recipe_slider_show_count]" type="checkbox" value="1" <?php checked( '1', $options['index_recipe_slider_show_count'] ); ?> /></li>
      </ul>
     </div>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // 無料会員登録メッセージの設定 ----------------------------------------- ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Welcome message setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e( 'Display setting', 'tcd-w' ); ?></h4>
     <ul class="design_radio_button">
      <li>
       <input type="radio" id="show_index_welcome_type1" name="dp_options[show_index_welcome]" value="type1" <?php checked( $options['show_index_welcome'], 'type1' ); ?> />
       <label for="show_index_welcome_type1"><?php _e('Display welcome message to all user', 'tcd-w'); ?></label>
      </li>
      <li>
       <input type="radio" id="show_index_welcome_type2" name="dp_options[show_index_welcome]" value="type2" <?php checked( $options['show_index_welcome'], 'type2' ); ?> />
       <label for="show_index_welcome_type2"><?php _e('Display welcome message to logout user', 'tcd-w'); ?></label>
      </li>
      <li>
       <input type="radio" id="show_index_welcome_type3" name="dp_options[show_index_welcome]" value="type3" <?php checked( $options['show_index_welcome'], 'type3' ); ?> />
       <label for="show_index_welcome_type3"><?php _e('Hide welcome message', 'tcd-w'); ?></label>
      </li>
     </ul>
      <h4 class="theme_option_headline2"><?php _e( 'Catchphrase setting', 'tcd-w' ); ?></h4>
      <textarea class="large-text" cols="50" rows="3" name="dp_options[index_welcome_catch]"><?php echo esc_textarea(  $options['index_welcome_catch'] ); ?></textarea>
      <ul class="option_list">
       <li class="cf"><span class="label"><?php _e('Font type', 'tcd-w');  ?></span>
        <select name="dp_options[index_welcome_catch_font_type]">
         <?php foreach ( $font_type_options as $option ) { ?>
         <option style="padding-right: 10px;" value="<?php echo esc_attr($option['value']); ?>" <?php selected( $options['index_welcome_catch_font_type'], $option['value'] ); ?>><?php echo $option['label']; ?></option>
         <?php } ?>
        </select>
       </li>
       <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_welcome_catch_font_size]" value="<?php echo esc_attr( $options['index_welcome_catch_font_size'] ); ?>" /><span>px</span></li>
       <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_welcome_catch_font_size_mobile]" value="<?php echo esc_attr( $options['index_welcome_catch_font_size_mobile'] ); ?>" /><span>px</span></li>
      </ul>
      <h4 class="theme_option_headline2"><?php _e( 'Description setting', 'tcd-w' ); ?></h4>
      <textarea class="large-text" cols="50" rows="3" name="dp_options[index_welcome_desc]"><?php echo esc_textarea(  $options['index_welcome_desc'] ); ?></textarea>
      <ul class="option_list">
       <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_welcome_desc_font_size]" value="<?php echo esc_attr( $options['index_welcome_desc_font_size'] ); ?>" /><span>px</span></li>
       <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[index_welcome_desc_font_size_mobile]" value="<?php echo esc_attr( $options['index_welcome_desc_font_size_mobile'] ); ?>" /><span>px</span></li>
      </ul>
      <h4 class="theme_option_headline2"><?php _e('Member registration button setting', 'tcd-w');  ?></h4>
      <p class="displayment_checkbox"><label><input class="index_welcome_show_button" name="dp_options[index_welcome_show_button]" type="checkbox" value="1" <?php checked( $options['index_welcome_show_button'], 1 ); ?>><?php _e( 'Display button', 'tcd-w' ); ?></label></p>
      <div style="<?php if($options['index_welcome_show_button'] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
       <ul class="option_list" style="border-top:1px dotted #ccc; padding-top:12px;">
        <li class="cf"><span class="label"><?php _e('label', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[index_welcome_button_label]" value="<?php echo esc_attr( $options['index_welcome_button_label'] ); ?>" /></li>
        <li class="cf"><span class="label"><?php _e('Background color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_welcome_button_bg_color]" value="<?php echo esc_attr( $options['index_welcome_button_bg_color'] ); ?>" data-default-color="#ff7f00" class="c-color-picker"></li>
        <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[index_welcome_button_font_color]" value="<?php echo esc_attr( $options['index_welcome_button_font_color'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
        <li class="cf"><span class="label"><?php _e('Background color on mouse over', 'tcd-w'); ?></span><input type="text" name="dp_options[index_welcome_button_bg_color_hover]" value="<?php echo esc_attr( $options['index_welcome_button_bg_color_hover'] ); ?>" data-default-color="#fbc525" class="c-color-picker"></li>
        <li class="cf"><span class="label"><?php _e('Font color on mouse over', 'tcd-w'); ?></span><input type="text" name="dp_options[index_welcome_button_font_color_hover]" value="<?php echo esc_attr( $options['index_welcome_button_font_color_hover'] ); ?>" data-default-color="#FFFFFF" class="c-color-picker"></li>
       </ul>
      </div>
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // コンテンツビルダー ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ ?>
   <div class="theme_option_field theme_option_field_ac open active show_arrow">
    <h3 class="theme_option_headline"><?php _e('Content builder', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <div class="theme_option_message no_arrow">
      <?php echo __( '<p>You can build contents freely with this function.</p><br /><p>STEP1: Click Add content button.<br />STEP2: Select content from dropdown menu.<br />STEP3: Input data and save the option.</p><br /><p>You can change order by dragging MOVE button and you can delete content by clicking DELETE button.</p>', 'tcd-w' ); ?>
     </div>
     <h4 class="theme_option_headline2"><?php _e( 'Content image', 'tcd-w' ); ?></h4>
     <ul class="design_button_list cf rebox_group">
      <li><a href="<?php bloginfo('template_url'); ?>/admin/img/cb_content1.jpg" title="<?php printf(__('Recent %s list', 'tcd-w'), $recipe_label); ?>"><?php printf(__('Recent %s list', 'tcd-w'), $recipe_label); ?></a></li>
      <li><a href="<?php bloginfo('template_url'); ?>/admin/img/cb_content2.jpg" title="<?php printf(__('Popular %s list', 'tcd-w'), $recipe_label); ?>"><?php printf(__('Popular %s list', 'tcd-w'), $recipe_label); ?></a></li>
      <li><a href="<?php bloginfo('template_url'); ?>/admin/img/cb_content3.jpg" title="<?php printf(__('Featured %s list', 'tcd-w'), $recipe_label); ?>"><?php printf(__('Featured %s list', 'tcd-w'), $recipe_label); ?></a></li>
      <li><a href="<?php bloginfo('template_url'); ?>/admin/img/cb_content4.jpg" title="<?php _e('Recent blog list', 'tcd-w'); ?>"><?php _e('Recent blog list', 'tcd-w'); ?></a></li>
      <li><a href="<?php bloginfo('template_url'); ?>/admin/img/cb_content5.jpg" title="<?php printf(__('%s list', 'tcd-w'), $news_label); ?>"><?php printf(__('%s list', 'tcd-w'), $news_label); ?></a></li>
      <li><a href="<?php bloginfo('template_url'); ?>/admin/img/cb_content6.jpg" title="<?php _e( 'Banner', 'tcd-w' ); ?>"><?php _e( 'Banner', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->

   <div id="contents_builder_wrap">
    <div id="contents_builder">
     <p class="cb_message"><?php _e( 'Click Add content button to start content builder', 'tcd-w' ); ?></p>
     <?php
          if (!empty($options['contents_builder'])) {
            foreach($options['contents_builder'] as $key => $content) :
              $cb_index = 'cb_'.$key.'_'.mt_rand(0,999999);
     ?>
     <div class="cb_row">
      <ul class="cb_button cf">
       <li><span class="cb_move"><?php echo __('Move', 'tcd-w'); ?></span></li>
       <li><span class="cb_delete"><?php echo __('Delete', 'tcd-w'); ?></span></li>
      </ul>
      <div class="cb_column_area cf">
       <div class="cb_column">
        <input type="hidden" class="cb_index" value="<?php echo $cb_index; ?>" />
        <?php the_cb_content_select($cb_index, $content['cb_content_select']); ?>
        <?php if (!empty($content['cb_content_select'])) the_cb_content_setting($cb_index, $content['cb_content_select'], $content); ?>
       </div>
      </div><!-- END .cb_column_area -->
     </div><!-- END .cb_row -->
     <?php
          endforeach;
         };
     ?>
    </div><!-- END #contents_builder -->
    <ul class="button_list cf" id="cb_add_row_buttton_area">
     <li><input type="button" value="<?php echo __( 'Add content', 'tcd-w' ); ?>" class="button-ml add_row"></li>
     <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
    </ul>
   </div><!-- END #contents_builder_wrap -->

   <?php // コンテンツビルダー追加用 非表示 ?>
   <div id="contents_builder-clone" class="hidden">
    <div class="cb_row">
     <ul class="cb_button cf">
      <li><span class="cb_move"><?php echo __('Move', 'tcd-w'); ?></span></li>
      <li><span class="cb_delete"><?php echo __('Delete', 'tcd-w'); ?></span></li>
     </ul>
     <div class="cb_column_area cf">
      <div class="cb_column">
       <input type="hidden" class="cb_index" value="cb_cloneindex" />
       <?php the_cb_content_select('cb_cloneindex'); ?>
      </div>
     </div><!-- END .cb_column_area -->
    </div><!-- END .cb_row -->
    <?php
         the_cb_content_setting('cb_cloneindex', 'recent_recipe_list');
         the_cb_content_setting('cb_cloneindex', 'popular_recipe_list');
         the_cb_content_setting('cb_cloneindex', 'featured_recipe_list');
         the_cb_content_setting('cb_cloneindex', 'recent_blog_list');
         the_cb_content_setting('cb_cloneindex', 'news_list');
         the_cb_content_setting('cb_cloneindex', 'banner');
         the_cb_content_setting('cb_cloneindex', 'free_space');
    ?>
   </div><!-- END #contents_builder-clone.hidden -->
   <?php // コンテンツビルダーここまで ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ ?>


</div><!-- END .tab-content -->

<?php
} // END add_front_page_tab_panel()


// バリデーション　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_front_page_theme_options_validate( $input ) {

  global $dp_default_options, $index_content_type_options, $time_options, $font_type_options, $content_direction_options, $index_slider_content_type_options, $index_slider_animation_type_options, $index_slider_content_type_options;

  // ヘッダーコンテンツ
  if ( ! isset( $input['index_header_content_type'] ) )
    $input['index_header_content_type'] = null;
  if ( ! array_key_exists( $input['index_header_content_type'], $index_content_type_options ) )
    $input['index_header_content_type'] = null;

  // 画像スライダーの時間
  if ( ! isset( $value['index_slider_time'] ) )
    $value['index_slider_time'] = null;
  if ( ! array_key_exists( $value['index_slider_time'], $time_options ) )
    $value['index_slider_time'] = null;

  // 画像スライダーのアニメーション
  if ( ! isset( $input['index_slider_animation_type'] ) )
    $input['index_slider_animation_type'] = null;
  if ( ! array_key_exists( $input['index_slider_animation_type'], $index_slider_animation_type_options ) )
    $input['index_slider_animation_type'] = null;

  // 画像スライダー
  for ( $i = 1; $i <= 3; $i++ ) {
    // 画像の設定
    $input['index_slider_image'.$i] = wp_filter_nohtml_kses( $input['index_slider_image'.$i] );
    $input['index_slider_image_mobile'.$i] = wp_filter_nohtml_kses( $input['index_slider_image_mobile'.$i] );

    // キャッチフレーズ
    $input['index_slider_catch'.$i] = wp_filter_nohtml_kses( $input['index_slider_catch'.$i] );
    if ( ! isset( $value['index_slider_catch_font_type'.$i] ) )
      $value['index_slider_catch_font_type'.$i] = null;
    if ( ! array_key_exists( $value['index_slider_catch_font_type'.$i], $font_type_options ) )
      $value['index_slider_catch_font_type'.$i] = null;
    $input['index_slider_catch_font_size'.$i] = wp_filter_nohtml_kses( $input['index_slider_catch_font_size'.$i] );
    $input['index_slider_catch_font_size_mobile'.$i] = wp_filter_nohtml_kses( $input['index_slider_catch_font_size_mobile'.$i] );
    $input['index_slider_catch_color'.$i] = wp_filter_nohtml_kses( $input['index_slider_catch_color'.$i] );

    // 説明文
    $input['index_slider_desc'.$i] = wp_filter_nohtml_kses( $input['index_slider_desc'.$i] );
    $input['index_slider_desc_font_size'.$i] = wp_filter_nohtml_kses( $input['index_slider_desc_font_size'.$i] );
    $input['index_slider_desc_font_size_mobile'.$i] = wp_filter_nohtml_kses( $input['index_slider_desc_font_size_mobile'.$i] );
    $input['index_slider_desc_color'.$i] = wp_filter_nohtml_kses( $input['index_slider_desc_color'.$i] );

    // テキストシャドウ
    if ( ! isset( $input['index_slider_use_shadow'.$i] ) )
      $input['index_slider_use_shadow'.$i] = null;
      $input['index_slider_use_shadow'.$i] = ( $input['index_slider_use_shadow'.$i] == 1 ? 1 : 0 );
    $input['index_slider_shadow_h'.$i] = wp_filter_nohtml_kses( $input['index_slider_shadow_h'.$i] );
    $input['index_slider_shadow_v'.$i] = wp_filter_nohtml_kses( $input['index_slider_shadow_v'.$i] );
    $input['index_slider_shadow_b'.$i] = wp_filter_nohtml_kses( $input['index_slider_shadow_b'.$i] );
    $input['index_slider_shadow_c'.$i] = wp_filter_nohtml_kses( $input['index_slider_shadow_c'.$i] );

    // ボタン
    if ( ! isset( $input['index_slider_show_button'.$i] ) )
      $input['index_slider_show_button'.$i] = null;
      $input['index_slider_show_button'.$i] = ( $input['index_slider_show_button'.$i] == 1 ? 1 : 0 );
    $input['index_slider_button_label'.$i] = wp_filter_nohtml_kses( $input['index_slider_button_label'.$i] );
    $input['index_slider_button_bg_color'.$i] = wp_filter_nohtml_kses( $input['index_slider_button_bg_color'.$i] );
    $input['index_slider_button_font_color'.$i] = wp_filter_nohtml_kses( $input['index_slider_button_font_color'.$i] );
    $input['index_slider_button_bg_color_hover'.$i] = wp_filter_nohtml_kses( $input['index_slider_button_bg_color_hover'.$i] );
    $input['index_slider_button_font_color_hover'.$i] = wp_filter_nohtml_kses( $input['index_slider_button_font_color_hover'.$i] );
    $input['index_slider_url'.$i] = wp_filter_nohtml_kses( $input['index_slider_url'.$i] );
    if ( ! isset( $input['index_slider_target'.$i] ) )
      $input['index_slider_target'.$i] = null;
      $input['index_slider_target'.$i] = ( $input['index_slider_target'.$i] == 1 ? 1 : 0 );

    // オーバーレイ
    if ( ! isset( $input['index_slider_use_overlay'.$i] ) )
      $input['index_slider_use_overlay'.$i] = null;
      $input['index_slider_use_overlay'.$i] = ( $input['index_slider_use_overlay'.$i] == 1 ? 1 : 0 );
    $input['index_slider_overlay_color'.$i] = wp_filter_nohtml_kses( $input['index_slider_overlay_color'.$i] );
    $input['index_slider_overlay_opacity'.$i] = wp_filter_nohtml_kses( $input['index_slider_overlay_opacity'.$i] );
    if ( ! isset( $input['index_slider_use_overlay_mobile'.$i] ) )
      $input['index_slider_use_overlay_mobile'.$i] = null;
      $input['index_slider_use_overlay_mobile'.$i] = ( $input['index_slider_use_overlay_mobile'.$i] == 1 ? 1 : 0 );
    $input['index_slider_overlay_color_mobile'.$i] = wp_filter_nohtml_kses( $input['index_slider_overlay_color_mobile'.$i] );
    $input['index_slider_overlay_opacity_mobile'.$i] = wp_filter_nohtml_kses( $input['index_slider_overlay_opacity_mobile'.$i] );

    // コンテンツの方向
    if ( ! isset( $value['index_slider_content_direction'.$i] ) )
      $value['index_slider_content_direction'.$i] = null;
    if ( ! array_key_exists( $value['index_slider_content_direction'.$i], $content_direction_options ) )
      $value['index_slider_content_direction'.$i] = null;

  }

  // 画像スライダー　スマホ用　コンテンツのタイプ
  if ( ! isset( $value['index_slider_mobile_content_type'] ) )
    $value['index_slider_mobile_content_type'] = null;
  if ( ! array_key_exists( $value['index_slider_mobile_content_type'], $index_slider_content_type_options ) )
    $value['index_slider_mobile_content_type'] = null;

  // 画像スライダー　スマホ用　キャッチフレーズ
  $input['index_slider_mobile_catch'] = wp_filter_nohtml_kses( $input['index_slider_mobile_catch'] );
  if ( ! isset( $value['index_slider_mobile_catch_font_type'] ) )
    $value['index_slider_mobile_catch_font_type'] = null;
  if ( ! array_key_exists( $value['index_slider_mobile_catch_font_type'], $font_type_options ) )
    $value['index_slider_mobile_catch_font_type'] = null;
  $input['index_slider_mobile_catch_font_size'] = wp_filter_nohtml_kses( $input['index_slider_mobile_catch_font_size'] );
  $input['index_slider_mobile_catch_color'] = wp_filter_nohtml_kses( $input['index_slider_mobile_catch_color'] );

  // 画像スライダー　スマホ用　ロゴ
  $input['index_slider_mobile_logo_image'] = absint( $input['index_slider_mobile_logo_image'] );
  $input['index_slider_mobile_logo_image_width'] = absint( $input['index_slider_mobile_logo_image_width'] );

  // 画像スライダー　スマホ用　テキストシャドウ
  if ( ! isset( $input['index_slider_mobile_use_shadow'] ) )
    $input['index_slider_mobile_use_shadow'] = null;
    $input['index_slider_mobile_use_shadow'] = ( $input['index_slider_mobile_use_shadow'] == 1 ? 1 : 0 );
  $input['index_slider_mobile_shadow_h'] = wp_filter_nohtml_kses( $input['index_slider_mobile_shadow_h'] );
  $input['index_slider_mobile_shadow_v'] = wp_filter_nohtml_kses( $input['index_slider_mobile_shadow_v'] );
  $input['index_slider_mobile_shadow_b'] = wp_filter_nohtml_kses( $input['index_slider_mobile_shadow_b'] );
  $input['index_slider_mobile_shadow_c'] = wp_filter_nohtml_kses( $input['index_slider_mobile_shadow_c'] );

  // 動画 ----------------------------------------------------------------
  $input['index_video'] = wp_filter_nohtml_kses( $input['index_video'] );

  // Youtube --------------------------------------------------------------
  $input['index_youtube_url'] = wp_filter_nohtml_kses( $input['index_youtube_url'] );

  // 動画用コンテンツ PC用 -----------------------------------------------------
  // 代替画像
  $input['index_movie_image'] = wp_filter_nohtml_kses( $input['index_movie_image'] );

  // キャッチフレーズ
  $input['index_movie_catch'] = wp_filter_nohtml_kses( $input['index_movie_catch'] );
  if ( ! isset( $value['index_movie_catch_font_type'] ) )
    $value['index_movie_catch_font_type'] = null;
  if ( ! array_key_exists( $value['index_movie_catch_font_type'], $font_type_options ) )
    $value['index_movie_catch_font_type'] = null;
  $input['index_movie_catch_font_size'] = wp_filter_nohtml_kses( $input['index_movie_catch_font_size'] );
  $input['index_movie_catch_font_size_mobile'] = wp_filter_nohtml_kses( $input['index_movie_catch_font_size_mobile'] );
  $input['index_movie_catch_color'] = wp_filter_nohtml_kses( $input['index_movie_catch_color'] );

  // 説明文
  $input['index_movie_desc'] = wp_filter_nohtml_kses( $input['index_movie_desc'] );
  $input['index_movie_desc_font_size'] = wp_filter_nohtml_kses( $input['index_movie_desc_font_size'] );
  $input['index_movie_desc_font_size_mobile'] = wp_filter_nohtml_kses( $input['index_movie_desc_font_size_mobile'] );
  $input['index_movie_desc_color'] = wp_filter_nohtml_kses( $input['index_movie_desc_color'] );

  // テキストシャドウ
  if ( ! isset( $input['index_movie_use_shadow'] ) )
    $input['index_movie_use_shadow'] = null;
    $input['index_movie_use_shadow'] = ( $input['index_movie_use_shadow'] == 1 ? 1 : 0 );
  $input['index_movie_shadow_h'] = wp_filter_nohtml_kses( $input['index_movie_shadow_h'] );
  $input['index_movie_shadow_v'] = wp_filter_nohtml_kses( $input['index_movie_shadow_v'] );
  $input['index_movie_shadow_b'] = wp_filter_nohtml_kses( $input['index_movie_shadow_b'] );
  $input['index_movie_shadow_c'] = wp_filter_nohtml_kses( $input['index_movie_shadow_c'] );

  // ボタン
  if ( ! isset( $input['index_movie_show_button'] ) )
    $input['index_movie_show_button'] = null;
    $input['index_movie_show_button'] = ( $input['index_movie_show_button'] == 1 ? 1 : 0 );
  $input['index_movie_button_label'] = wp_filter_nohtml_kses( $input['index_movie_button_label'] );
  $input['index_movie_button_bg_color'] = wp_filter_nohtml_kses( $input['index_movie_button_bg_color'] );
  $input['index_movie_button_font_color'] = wp_filter_nohtml_kses( $input['index_movie_button_font_color'] );
  $input['index_movie_button_bg_color_hover'] = wp_filter_nohtml_kses( $input['index_movie_button_bg_color_hover'] );
  $input['index_movie_button_font_color_hover'] = wp_filter_nohtml_kses( $input['index_movie_button_font_color_hover'] );
  $input['index_movie_url'] = wp_filter_nohtml_kses( $input['index_movie_url'] );
  if ( ! isset( $input['index_movie_target'] ) )
    $input['index_movie_target'] = null;
    $input['index_movie_target'] = ( $input['index_movie_target'] == 1 ? 1 : 0 );

  // オーバーレイ
  if ( ! isset( $input['index_movie_use_overlay'] ) )
    $input['index_movie_use_overlay'] = null;
    $input['index_movie_use_overlay'] = ( $input['index_movie_use_overlay'] == 1 ? 1 : 0 );
  $input['index_movie_overlay_color'] = wp_filter_nohtml_kses( $input['index_movie_overlay_color'] );
  $input['index_movie_overlay_opacity'] = wp_filter_nohtml_kses( $input['index_movie_overlay_opacity'] );

  // 動画用コンテンツ スマホ用 -----------------------------------------------------
  if ( ! isset( $value['index_movie_mobile_content_type'] ) )
    $value['index_movie_mobile_content_type'] = null;
  if ( ! array_key_exists( $value['index_movie_mobile_content_type'], $index_slider_content_type_options ) )
    $value['index_movie_mobile_content_type'] = null;

  // キャッチフレーズ
  $input['index_movie_catch_mobile'] = wp_filter_nohtml_kses( $input['index_movie_catch_mobile'] );
  if ( ! isset( $value['index_movie_catch_font_type_mobile'] ) )
    $value['index_movie_catch_font_type_mobile'] = null;
  if ( ! array_key_exists( $value['index_movie_catch_font_type_mobile'], $font_type_options ) )
    $value['index_movie_catch_font_type_mobile'] = null;
  $input['index_movie_mobile_catch_font_size'] = wp_filter_nohtml_kses( $input['index_movie_mobile_catch_font_size'] );
  $input['index_movie_catch_color_mobile'] = wp_filter_nohtml_kses( $input['index_movie_catch_color_mobile'] );

  // テキストシャドウ
  if ( ! isset( $input['index_movie_use_shadow_mobile'] ) )
    $input['index_movie_use_shadow_mobile'] = null;
    $input['index_movie_use_shadow_mobile'] = ( $input['index_movie_use_shadow_mobile'] == 1 ? 1 : 0 );
  $input['index_movie_shadow_h_mobile'] = wp_filter_nohtml_kses( $input['index_movie_shadow_h_mobile'] );
  $input['index_movie_shadow_v_mobile'] = wp_filter_nohtml_kses( $input['index_movie_shadow_v_mobile'] );
  $input['index_movie_shadow_b_mobile'] = wp_filter_nohtml_kses( $input['index_movie_shadow_b_mobile'] );
  $input['index_movie_shadow_c_mobile'] = wp_filter_nohtml_kses( $input['index_movie_shadow_c_mobile'] );

  // 総レシピ数
  if ( ! isset( $input['show_index_total_recipe'] ) )
    $input['show_index_total_recipe'] = null;
    $input['show_index_total_recipe'] = ( $input['show_index_total_recipe'] == 1 ? 1 : 0 );
  $input['index_total_recipe_headline'] = wp_filter_nohtml_kses( $input['index_total_recipe_headline'] );
  $input['index_total_recipe_label'] = wp_filter_nohtml_kses( $input['index_total_recipe_label'] );
  $input['index_total_recipe_color'] = wp_filter_nohtml_kses( $input['index_total_recipe_color'] );

  // レシピスライダー
  if ( ! isset( $input['show_index_recipe_slider'] ) )
    $input['show_index_recipe_slider'] = null;
    $input['show_index_recipe_slider'] = ( $input['show_index_recipe_slider'] == 1 ? 1 : 0 );
  $input['index_recipe_slider_num'] = wp_filter_nohtml_kses( $input['index_recipe_slider_num'] );
  $input['index_recipe_slider_post_type'] = wp_filter_nohtml_kses( $input['index_recipe_slider_post_type'] );
  $input['index_recipe_slider_font_size'] = wp_filter_nohtml_kses( $input['index_recipe_slider_font_size'] );
  $input['index_recipe_slider_font_size_mobile'] = wp_filter_nohtml_kses( $input['index_recipe_slider_font_size_mobile'] );
  if ( ! isset( $input['index_recipe_slider_show_premium_icon'] ) )
    $input['index_recipe_slider_show_premium_icon'] = null;
    $input['index_recipe_slider_show_premium_icon'] = ( $input['index_recipe_slider_show_premium_icon'] == 1 ? 1 : 0 );
  if ( ! isset( $input['index_recipe_slider_show_category'] ) )
    $input['index_recipe_slider_show_category'] = null;
    $input['index_recipe_slider_show_category'] = ( $input['index_recipe_slider_show_category'] == 1 ? 1 : 0 );
  if ( ! isset( $input['index_recipe_slider_show_count'] ) )
    $input['index_recipe_slider_show_count'] = null;
    $input['index_recipe_slider_show_count'] = ( $input['index_recipe_slider_show_count'] == 1 ? 1 : 0 );

  // 無料会員登録メッセージ
  $input['show_index_welcome'] = wp_filter_nohtml_kses( $input['show_index_welcome'] );
  $input['index_welcome_catch'] = wp_filter_nohtml_kses( $input['index_welcome_catch'] );
  $input['index_welcome_desc'] = wp_filter_nohtml_kses( $input['index_welcome_desc'] );
  $input['index_welcome_catch_font_size'] = wp_filter_nohtml_kses( $input['index_welcome_catch_font_size'] );
  $input['index_welcome_catch_font_size_mobile'] = wp_filter_nohtml_kses( $input['index_welcome_catch_font_size_mobile'] );
  $input['index_welcome_desc_font_size'] = wp_filter_nohtml_kses( $input['index_welcome_desc_font_size'] );
  $input['index_welcome_desc_font_size_mobile'] = wp_filter_nohtml_kses( $input['index_welcome_desc_font_size_mobile'] );
  if ( ! isset( $input['index_welcome_show_button'] ) )
    $input['index_welcome_show_button'] = null;
    $input['index_welcome_show_button'] = ( $input['index_welcome_show_button'] == 1 ? 1 : 0 );
  $input['index_welcome_button_label'] = wp_filter_nohtml_kses( $input['index_welcome_button_label'] );
  $input['index_welcome_button_bg_color'] = wp_filter_nohtml_kses( $input['index_welcome_button_bg_color'] );
  $input['index_welcome_button_font_color'] = wp_filter_nohtml_kses( $input['index_welcome_button_font_color'] );
  $input['index_welcome_button_bg_color_hover'] = wp_filter_nohtml_kses( $input['index_welcome_button_bg_color_hover'] );
  $input['index_welcome_button_font_color_hover'] = wp_filter_nohtml_kses( $input['index_welcome_button_font_color_hover'] );


  // コンテンツビルダー -----------------------------------------------------------------------------
  if (!empty($input['contents_builder'])) {

    $input_cb = $input['contents_builder'];
    $input['contents_builder'] = array();

    foreach($input_cb as $key => $value) {

      // クローン用はスルー
      //if (in_array($key, array('cb_cloneindex', 'cb_cloneindex2'))) continue;
      if (in_array($key, array('cb_cloneindex', 'cb_cloneindex2'), true)) continue;


      if (!isset($value['cb_display']) || !in_array($value['cb_display'], array('type1', 'type2', 'type3', 'type4')))
        $value['cb_display'] = 'type1';

      // 新着レシピ -----------------------------------------------------------------------
      if ($value['cb_content_select'] == 'recent_recipe_list') {

        $value['recent_headline'] = wp_filter_nohtml_kses( $value['recent_headline'] );
        $value['recent_headline_font_size'] = wp_filter_nohtml_kses( $value['recent_headline_font_size'] );
        $value['recent_headline_font_size_mobile'] = wp_filter_nohtml_kses( $value['recent_headline_font_size_mobile'] );
        $value['recent_headline_font_color'] = wp_filter_nohtml_kses( $value['recent_headline_font_color'] );
        $value['recent_headline_bg_color'] = wp_filter_nohtml_kses( $value['recent_headline_bg_color'] );
        $value['recent_headline_border_color'] = wp_filter_nohtml_kses( $value['recent_headline_border_color'] );
        $value['recent_headline_icon_color'] = wp_filter_nohtml_kses( $value['recent_headline_icon_color'] );
        $value['recent_headline_image'] = wp_filter_nohtml_kses( $value['recent_headline_image'] );
        if ( ! isset( $value['hide_recent_headline_icon'] ) )
          $value['hide_recent_headline_icon'] = null;
          $value['hide_recent_headline_icon'] = ( $value['hide_recent_headline_icon'] == 1 ? 1 : 0 );

        $value['recent_desc'] = wp_filter_nohtml_kses( $value['recent_desc'] );
        $value['recent_desc_font_size'] = wp_filter_nohtml_kses( $value['recent_desc_font_size'] );
        $value['recent_desc_font_size_mobile'] = wp_filter_nohtml_kses( $value['recent_desc_font_size_mobile'] );

        $value['recent_num'] = wp_filter_nohtml_kses( $value['recent_num'] );
        $value['recent_title_font_size'] = wp_filter_nohtml_kses( $value['recent_title_font_size'] );
        $value['recent_title_font_size_mobile'] = wp_filter_nohtml_kses( $value['recent_title_font_size_mobile'] );
        if ( ! isset( $value['recent_show_category'] ) )
          $value['recent_show_category'] = null;
          $value['recent_show_category'] = ( $value['recent_show_category'] == 1 ? 1 : 0 );
        if ( ! isset( $value['recent_show_date'] ) )
          $value['recent_show_date'] = null;
          $value['recent_show_date'] = ( $value['recent_show_date'] == 1 ? 1 : 0 );
        if ( ! isset( $value['recent_show_premium'] ) )
          $value['recent_show_premium'] = null;
          $value['recent_show_premium'] = ( $value['recent_show_premium'] == 1 ? 1 : 0 );

      // 人気レシピ -----------------------------------------------------------------------
      } elseif ($value['cb_content_select'] == 'popular_recipe_list') {

        $value['popular_headline'] = wp_filter_nohtml_kses( $value['popular_headline'] );
        $value['popular_headline_font_size'] = wp_filter_nohtml_kses( $value['popular_headline_font_size'] );
        $value['popular_headline_font_size_mobile'] = wp_filter_nohtml_kses( $value['popular_headline_font_size_mobile'] );
        $value['popular_headline_font_color'] = wp_filter_nohtml_kses( $value['popular_headline_font_color'] );
        $value['popular_headline_bg_color'] = wp_filter_nohtml_kses( $value['popular_headline_bg_color'] );
        $value['popular_headline_border_color'] = wp_filter_nohtml_kses( $value['popular_headline_border_color'] );
        $value['popular_headline_icon_color'] = wp_filter_nohtml_kses( $value['popular_headline_icon_color'] );
        $value['popular_headline_image'] = wp_filter_nohtml_kses( $value['popular_headline_image'] );
        if ( ! isset( $value['hide_popular_headline_icon'] ) )
          $value['hide_popular_headline_icon'] = null;
          $value['hide_popular_headline_icon'] = ( $value['hide_popular_headline_icon'] == 1 ? 1 : 0 );

        $value['popular_desc'] = wp_filter_nohtml_kses( $value['popular_desc'] );
        $value['popular_desc_font_size'] = wp_filter_nohtml_kses( $value['popular_desc_font_size'] );
        $value['popular_desc_font_size_mobile'] = wp_filter_nohtml_kses( $value['popular_desc_font_size_mobile'] );

        $value['popular_num'] = wp_filter_nohtml_kses( $value['popular_num'] );
        $value['popular_range'] = wp_filter_nohtml_kses( $value['popular_range'] );
        $value['popular_title_font_size'] = wp_filter_nohtml_kses( $value['popular_title_font_size'] );
        $value['popular_title_font_size_mobile'] = wp_filter_nohtml_kses( $value['popular_title_font_size_mobile'] );
        if ( ! isset( $value['popular_show_category'] ) )
          $value['popular_show_category'] = null;
          $value['popular_show_category'] = ( $value['popular_show_category'] == 1 ? 1 : 0 );
        if ( ! isset( $value['popular_show_post_view'] ) )
          $value['popular_show_post_view'] = null;
          $value['popular_show_post_view'] = ( $value['popular_show_post_view'] == 1 ? 1 : 0 );
        if ( ! isset( $value['popular_show_premium'] ) )
          $value['popular_show_premium'] = null;
          $value['popular_show_premium'] = ( $value['popular_show_premium'] == 1 ? 1 : 0 );

      // 特集レシピ -----------------------------------------------------------------------
      } elseif ($value['cb_content_select'] == 'featured_recipe_list') {

        $value['featured_headline'] = wp_filter_nohtml_kses( $value['featured_headline'] );
        $value['featured_headline_font_size'] = wp_filter_nohtml_kses( $value['featured_headline_font_size'] );
        $value['featured_headline_font_size_mobile'] = wp_filter_nohtml_kses( $value['featured_headline_font_size_mobile'] );
        $value['featured_headline_font_color'] = wp_filter_nohtml_kses( $value['featured_headline_font_color'] );
        $value['featured_headline_bg_color'] = wp_filter_nohtml_kses( $value['featured_headline_bg_color'] );
        $value['featured_headline_border_color'] = wp_filter_nohtml_kses( $value['featured_headline_border_color'] );
        $value['featured_headline_icon_color'] = wp_filter_nohtml_kses( $value['featured_headline_icon_color'] );
        $value['featured_headline_image'] = wp_filter_nohtml_kses( $value['featured_headline_image'] );
        if ( ! isset( $value['hide_featured_headline_icon'] ) )
          $value['hide_featured_headline_icon'] = null;
          $value['hide_featured_headline_icon'] = ( $value['hide_featured_headline_icon'] == 1 ? 1 : 0 );

        $value['featured_desc'] = wp_filter_nohtml_kses( $value['featured_desc'] );
        $value['featured_desc_font_size'] = wp_filter_nohtml_kses( $value['featured_desc_font_size'] );
        $value['featured_desc_font_size_mobile'] = wp_filter_nohtml_kses( $value['featured_desc_font_size_mobile'] );

        for ( $i = 1; $i <= 4; $i++ ) {
          $value['featured_banner_type'.$i] = wp_filter_nohtml_kses( $value['featured_banner_type'.$i] );
          $value['featured_image'.$i] = wp_filter_nohtml_kses( $value['featured_image'.$i] );
          $value['featured_url'.$i] = wp_filter_nohtml_kses( $value['featured_url'.$i] );
          if ( ! isset( $value['featured_target'.$i] ) )
            $value['featured_target'.$i] = null;
            $value['featured_target'.$i] = ( $value['featured_target'.$i] == 1 ? 1 : 0 );
          $value['featured_type1_border_color'.$i] = wp_filter_nohtml_kses( $value['featured_type1_border_color'.$i] );
          $value['featured_type1_font_color'.$i] = wp_filter_nohtml_kses( $value['featured_type1_font_color'.$i] );
          $value['featured_type2_font_color'.$i] = wp_filter_nohtml_kses( $value['featured_type2_font_color'.$i] );
          if ( ! isset( $value['featured_use_overlay'.$i] ) )
            $value['featured_use_overlay'.$i] = null;
            $value['featured_use_overlay'.$i] = ( $value['featured_use_overlay'.$i] == 1 ? 1 : 0 );
          $value['featured_overlay_color'.$i] = wp_filter_nohtml_kses( $value['featured_overlay_color'.$i] );
          $value['featured_title'.$i] = wp_filter_nohtml_kses( $value['featured_title'.$i] );
          $value['featured_sub_title'.$i] = wp_filter_nohtml_kses( $value['featured_sub_title'.$i] );
          $value['featured_title_font_size'.$i] = wp_filter_nohtml_kses( $value['featured_title_font_size'.$i] );
          $value['featured_title_font_size_mobile'.$i] = wp_filter_nohtml_kses( $value['featured_title_font_size_mobile'.$i] );
          $value['featured_sub_title_font_size'.$i] = wp_filter_nohtml_kses( $value['featured_sub_title_font_size'.$i] );
          $value['featured_sub_title_font_size_mobile'.$i] = wp_filter_nohtml_kses( $value['featured_sub_title_font_size_mobile'.$i] );
          if ( ! isset( $value['featured_title_font_type'.$i] ) )
            $value['featured_title_font_type'.$i] = null;
          if ( ! array_key_exists( $value['featured_title_font_type'.$i], $font_type_options ) )
            $value['featured_title_font_type'.$i] = null;
          $value['featured_page'.$i] = wp_filter_nohtml_kses( $value['featured_page'.$i] );
        };

      // 新着ブログ -----------------------------------------------------------------------
      } elseif ($value['cb_content_select'] == 'recent_blog_list') {

        $value['recent_blog_headline'] = wp_filter_nohtml_kses( $value['recent_blog_headline'] );
        $value['recent_blog_headline_font_size'] = wp_filter_nohtml_kses( $value['recent_blog_headline_font_size'] );
        $value['recent_blog_headline_font_size_mobile'] = wp_filter_nohtml_kses( $value['recent_blog_headline_font_size_mobile'] );
        $value['recent_blog_headline_font_color'] = wp_filter_nohtml_kses( $value['recent_blog_headline_font_color'] );
        $value['recent_blog_headline_bg_color'] = wp_filter_nohtml_kses( $value['recent_blog_headline_bg_color'] );
        $value['recent_blog_headline_border_color'] = wp_filter_nohtml_kses( $value['recent_blog_headline_border_color'] );
        $value['recent_blog_headline_icon_color'] = wp_filter_nohtml_kses( $value['recent_blog_headline_icon_color'] );
        $value['recent_blog_headline_image'] = wp_filter_nohtml_kses( $value['recent_blog_headline_image'] );
        if ( ! isset( $value['hide_recent_blog_headline_icon'] ) )
          $value['hide_recent_blog_headline_icon'] = null;
          $value['hide_recent_blog_headline_icon'] = ( $value['hide_recent_blog_headline_icon'] == 1 ? 1 : 0 );

        $value['recent_blog_desc'] = wp_filter_nohtml_kses( $value['recent_blog_desc'] );
        $value['recent_blog_desc_font_size'] = wp_filter_nohtml_kses( $value['recent_blog_desc_font_size'] );
        $value['recent_blog_desc_font_size_mobile'] = wp_filter_nohtml_kses( $value['recent_blog_desc_font_size_mobile'] );

        $value['recent_blog_num'] = wp_filter_nohtml_kses( $value['recent_blog_num'] );
        $value['recent_blog_title_font_size'] = wp_filter_nohtml_kses( $value['recent_blog_title_font_size'] );
        $value['recent_blog_title_font_size_mobile'] = wp_filter_nohtml_kses( $value['recent_blog_title_font_size_mobile'] );
        if ( ! isset( $value['recent_blog_show_date'] ) )
          $value['recent_blog_show_date'] = null;
          $value['recent_blog_show_date'] = ( $value['recent_blog_show_date'] == 1 ? 1 : 0 );
        if ( ! isset( $value['recent_blog_show_premium'] ) )
          $value['recent_blog_show_premium'] = null;
          $value['recent_blog_show_premium'] = ( $value['recent_blog_show_premium'] == 1 ? 1 : 0 );

      // お知らせ -----------------------------------------------------------------------
      } elseif ($value['cb_content_select'] == 'news_list') {

        $value['news_headline'] = wp_filter_nohtml_kses( $value['news_headline'] );
        $value['news_headline_font_size'] = wp_filter_nohtml_kses( $value['news_headline_font_size'] );
        $value['news_headline_font_size_mobile'] = wp_filter_nohtml_kses( $value['news_headline_font_size_mobile'] );
        $value['news_headline_font_color'] = wp_filter_nohtml_kses( $value['news_headline_font_color'] );
        $value['news_headline_bg_color'] = wp_filter_nohtml_kses( $value['news_headline_bg_color'] );
        $value['news_headline_border_color'] = wp_filter_nohtml_kses( $value['news_headline_border_color'] );
        $value['news_headline_icon_color'] = wp_filter_nohtml_kses( $value['news_headline_icon_color'] );
        $value['news_headline_image'] = wp_filter_nohtml_kses( $value['news_headline_image'] );
        if ( ! isset( $value['hide_news_headline_icon'] ) )
          $value['hide_news_headline_icon'] = null;
          $value['hide_news_headline_icon'] = ( $value['hide_news_headline_icon'] == 1 ? 1 : 0 );

        $value['news_desc'] = wp_filter_nohtml_kses( $value['news_desc'] );
        $value['news_desc_font_size'] = wp_filter_nohtml_kses( $value['news_desc_font_size'] );
        $value['news_desc_font_size_mobile'] = wp_filter_nohtml_kses( $value['news_desc_font_size_mobile'] );

        $value['news_num'] = wp_filter_nohtml_kses( $value['news_num'] );
        $value['news_date_color'] = wp_filter_nohtml_kses( $value['news_date_color'] );

      // バナー一覧 -----------------------------------------------------------------------
      } elseif ($value['cb_content_select'] == 'banner') {

        for ( $i = 1; $i <= 4; $i++ ) {
          $value['banner_image'.$i] = wp_filter_nohtml_kses( $value['banner_image'.$i] );
          $value['banner_url'.$i] = wp_filter_nohtml_kses( $value['banner_url'.$i] );
          if ( ! isset( $value['banner_target'.$i] ) )
            $value['banner_target'.$i] = null;
            $value['banner_target'.$i] = ( $value['banner_target'.$i] == 1 ? 1 : 0 );
          $value['banner_font_color'.$i] = wp_filter_nohtml_kses( $value['banner_font_color'.$i] );
          if ( ! isset( $value['banner_use_overlay'.$i] ) )
            $value['banner_use_overlay'.$i] = null;
            $value['banner_use_overlay'.$i] = ( $value['banner_use_overlay'.$i] == 1 ? 1 : 0 );
          $value['banner_overlay_color'.$i] = wp_filter_nohtml_kses( $value['banner_overlay_color'.$i] );
          $value['banner_title'.$i] = wp_filter_nohtml_kses( $value['banner_title'.$i] );
          $value['banner_sub_title'.$i] = wp_filter_nohtml_kses( $value['banner_sub_title'.$i] );
          $value['banner_title_font_size'.$i] = wp_filter_nohtml_kses( $value['banner_title_font_size'.$i] );
          $value['banner_sub_title_font_size'.$i] = wp_filter_nohtml_kses( $value['banner_sub_title_font_size'.$i] );
          if ( ! isset( $value['banner_title_font_type'.$i] ) )
            $value['banner_title_font_type'.$i] = null;
          if ( ! array_key_exists( $value['banner_title_font_type'.$i], $font_type_options ) )
            $value['banner_title_font_type'.$i] = null;
        };

      //自由入力欄 -----------------------------------------------------------------------
      } elseif ($value['cb_content_select'] == 'free_space') {

        if ( ! isset( $value['free_space'] )) {
          $value['free_space'] = null;
        } else {
          $value['free_space'] = $value['free_space'];
        }

      }

      $input['contents_builder'][] = $value;

    }

  } //コンテンツビルダーここまで -----------------------------------------------------------------------

  return $input;

};


/**
 * コンテンツビルダー用 コンテンツ選択プルダウン　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
 */
function the_cb_content_select($cb_index = 'cb_cloneindex', $selected = null) {

  $options = get_design_plus_option();
  $recipe_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );
  $news_label = $options['news_label'] ? esc_html( $options['news_label'] ) : __( 'News', 'tcd-w' );

	$cb_content_select = array(
		'recent_recipe_list' => sprintf(__('Recent %s list', 'tcd-w'), $recipe_label),
		'popular_recipe_list' => sprintf(__('Popular %s list', 'tcd-w'), $recipe_label),
		'featured_recipe_list' => sprintf(__('Featured %s list', 'tcd-w'), $recipe_label),
		'recent_blog_list' => __('Recent blog list', 'tcd-w'),
		'news_list' => sprintf(__('%s list', 'tcd-w'), $news_label),
		'banner' => __('Banner', 'tcd-w'),
		'free_space' => __('Free space', 'tcd-w')
	);

	if ($selected && isset($cb_content_select[$selected])) {
		$add_class = ' hidden';
	} else {
		$add_class = '';
	}

	$out = '<select name="dp_options[contents_builder]['.esc_attr($cb_index).'][cb_content_select]" class="cb_content_select'.$add_class.'">';
	$out .= '<option value="" style="padding-right: 10px;">'.__("Choose the content", "tcd-w").'</option>';

	foreach($cb_content_select as $key => $value) {
		$attr = '';
		if ($key == $selected) {
			$attr = ' selected="selected"';
		}
		$out .= '<option value="'.esc_attr($key).'"'.$attr.' style="padding-right: 10px;">'.esc_html($value).'</option>';
	}

	$out .= '</select>';

	echo $out; 
}

/**
 * コンテンツビルダー用 コンテンツ設定　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
 */
function the_cb_content_setting($cb_index = 'cb_cloneindex', $cb_content_select = null, $value = array()) {

  global $content_direction_options, $font_type_options;
  $options = get_design_plus_option();
  $recipe_label = $options['recipe_label'] ? esc_html( $options['recipe_label'] ) : __( 'Recipe', 'tcd-w' );
  $news_label = $options['news_label'] ? esc_html( $options['news_label'] ) : __( 'News', 'tcd-w' );

  if (!isset($value['cb_display'])) $value['cb_display'] = 'type1';
?>

<div class="cb_content_wrap cf <?php echo esc_attr($cb_content_select); ?>">

<?php
     // 新着レシピ　-------------------------------------------------------------
     if ($cb_content_select == 'recent_recipe_list') {

       if (!isset($value['recent_headline'])) { $value['recent_headline'] = ''; }
       if (!isset($value['recent_headline_font_size'])) { $value['recent_headline_font_size'] = '20'; }
       if (!isset($value['recent_headline_font_size_mobile'])) { $value['recent_headline_font_size_mobile'] = '15'; }
       if (!isset($value['recent_headline_font_color'])) { $value['recent_headline_font_color'] = '#000000'; }
       if (!isset($value['recent_headline_bg_color'])) { $value['recent_headline_bg_color'] = '#ffffff'; }
       if (!isset($value['recent_headline_border_color'])) { $value['recent_headline_border_color'] = '#dddddd'; }
       if (!isset($value['recent_headline_icon_color'])) { $value['recent_headline_icon_color'] = '#000000'; }
       if (!isset($value['recent_headline_image'])) { $value['recent_headline_image'] = false; }
       if (!isset($value['hide_recent_headline_icon'])) { $value['hide_recent_headline_icon'] = ''; }

       if (!isset($value['recent_desc'])) { $value['recent_desc'] = ''; }
       if (!isset($value['recent_desc_font_size'])) { $value['recent_desc_font_size'] = '16'; }
       if (!isset($value['recent_desc_font_size_mobile'])) { $value['recent_desc_font_size_mobile'] = '13'; }

       if (!isset($value['recent_num'])) { $value['recent_num'] = '6'; }
       if (!isset($value['recent_title_font_size'])) { $value['recent_title_font_size'] = '16'; }
       if (!isset($value['recent_title_font_size_mobile'])) { $value['recent_title_font_size_mobile'] = '14'; }
       if (!isset($value['recent_show_category'])) { $value['recent_show_category'] = 1; }
       if (!isset($value['recent_show_date'])) { $value['recent_show_date'] = 1; }
       if (!isset($value['recent_show_premium'])) { $value['recent_show_premium'] = 1; }


?>

  <h3 class="cb_content_headline"><?php printf(__('Recent %s list', 'tcd-w'), $recipe_label);  ?></h3>
  <div class="cb_content">

   <ul class="design_radio_button">
    <li>
     <input type="radio" id="cb_display-type1-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type1" <?php checked( $value['cb_display'], 'type1' ); ?> />
     <label for="cb_display-type1-<?php echo $cb_index; ?>"><?php _e('Display this content to all user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type2-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type2" <?php checked( $value['cb_display'], 'type2' ); ?> />
     <label for="cb_display-type2-<?php echo $cb_index; ?>"><?php _e('Display this content only to logged-in user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type3-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type3" <?php checked( $value['cb_display'], 'type3' ); ?> />
     <label for="cb_display-type3-<?php echo $cb_index; ?>"><?php _e('Display this content only to logout user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type4-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type4" <?php checked( $value['cb_display'], 'type4' ); ?> />
     <label for="cb_display-type4-<?php echo $cb_index; ?>"><?php _e('Hide this content', 'tcd-w'); ?></label>
    </li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w');  ?></h4>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Label', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_headline]" value="<?php echo esc_attr( $value['recent_headline'] ); ?>"></li>
    <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_headline_font_size]" value="<?php esc_attr_e( $value['recent_headline_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_headline_font_size_mobile]" value="<?php esc_attr_e( $value['recent_headline_font_size_mobile'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_headline_font_color]" value="<?php echo esc_attr( $value['recent_headline_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_headline_bg_color]" value="<?php echo esc_attr( $value['recent_headline_bg_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Border color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_headline_border_color]" value="<?php echo esc_attr( $value['recent_headline_border_color'] ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Hide headline icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="dp_options[contents_builder][<?php echo $cb_index; ?>][hide_recent_headline_icon]" value="1" <?php checked( $value['hide_recent_headline_icon'], 1 ); ?>></li>
   </ul>
   <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($value['hide_recent_headline_icon']) { echo ' display:none;'; }; ?>">
    <li class="cf"><span class="label"><?php _e('Background color of headline icon', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_headline_icon_color]" value="<?php echo esc_attr( $value['recent_headline_icon_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Icon image', 'tcd-w');  ?></span>
     <div class="image_box cf">
      <div class="cf cf_media_field hide-if-no-js recent_headline_image">
       <input type="hidden" value="<?php echo esc_attr( $value['recent_headline_image'] ); ?>" id="recent_headline_image-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_headline_image]" class="cf_media_id">
       <div class="preview_field"><?php if($value['recent_headline_image']){ echo wp_get_attachment_image($value['recent_headline_image'], 'medium'); }; ?></div>
       <div class="buttton_area">
        <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
        <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$value['recent_headline_image']){ echo 'hidden'; }; ?>">
       </div>
      </div>
     </div>
     <div class="theme_option_message2">
      <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
      <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
     </div>
    </li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Description', 'tcd-w');  ?></h4>
   <textarea class="large-text" cols="50" rows="3" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_desc]"><?php echo esc_textarea(  $value['recent_desc'] ); ?></textarea>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_desc_font_size]" value="<?php esc_attr_e( $value['recent_desc_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_desc_font_size_mobile]" value="<?php esc_attr_e( $value['recent_desc_font_size_mobile'] ); ?>" /><span>px</span></li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Post list setting', 'tcd-w');  ?></h4>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Number of post to display', 'tcd-w');  ?></span>
     <select name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_num]">
      <?php for($i=3; $i<= 12; $i++): ?>
      <?php if( $i % 3 == 0 ){ ?>
      <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $value['recent_num'], $i ); ?>><?php echo esc_html($i); ?></option>
      <?php }; ?>
      <?php endfor; ?>
     </select>
    </li>
    <li class="cf"><span class="label"><?php _e('Font size of title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_title_font_size]" value="<?php esc_attr_e( $value['recent_title_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size of title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_title_font_size_mobile]" value="<?php esc_attr_e( $value['recent_title_font_size_mobile'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php printf(__('Display premium %s icon', 'tcd-w'), $recipe_label); ?></span><input name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_show_premium]" type="checkbox" value="1" <?php checked( '1', $value['recent_show_premium'] ); ?> /></li>
    <li class="cf"><span class="label"><?php _e('Display category', 'tcd-w');  ?></span><input name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_show_category]" type="checkbox" value="1" <?php checked( '1', $value['recent_show_category'] ); ?> /></li>
    <li class="cf"><span class="label"><?php _e('Display date', 'tcd-w');  ?></span><input name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_show_date]" type="checkbox" value="1" <?php checked( '1', $value['recent_show_date'] ); ?> /></li>
   </ul>

<?php
     // 人気のレシピ　-------------------------------------------------------------
     } elseif ($cb_content_select == 'popular_recipe_list') {

       if (!isset($value['popular_headline'])) { $value['popular_headline'] = ''; }
       if (!isset($value['popular_headline_font_size'])) { $value['popular_headline_font_size'] = '20'; }
       if (!isset($value['popular_headline_font_size_mobile'])) { $value['popular_headline_font_size_mobile'] = '15'; }
       if (!isset($value['popular_headline_font_color'])) { $value['popular_headline_font_color'] = '#000000'; }
       if (!isset($value['popular_headline_bg_color'])) { $value['popular_headline_bg_color'] = '#ffffff'; }
       if (!isset($value['popular_headline_border_color'])) { $value['popular_headline_border_color'] = '#dddddd'; }
       if (!isset($value['popular_headline_icon_color'])) { $value['popular_headline_icon_color'] = '#000000'; }
       if (!isset($value['popular_headline_image'])) { $value['popular_headline_image'] = false; }
       if (!isset($value['hide_popular_headline_icon'])) { $value['hide_popular_headline_icon'] = ''; }

       if (!isset($value['popular_desc'])) { $value['popular_desc'] = ''; }
       if (!isset($value['popular_desc_font_size'])) { $value['popular_desc_font_size'] = '16'; }
       if (!isset($value['popular_desc_font_size_mobile'])) { $value['popular_desc_font_size_mobile'] = '13'; }

       if (!isset($value['popular_num'])) { $value['popular_num'] = '9'; }
       if (!isset($value['popular_range'])) { $value['popular_range'] = ''; }
       if (!isset($value['popular_title_font_size'])) { $value['popular_title_font_size'] = '16'; }
       if (!isset($value['popular_title_font_size_mobile'])) { $value['popular_title_font_size_mobile'] = '14'; }
       if (!isset($value['popular_show_category'])) { $value['popular_show_category'] = 1; }
       if (!isset($value['popular_show_post_view'])) { $value['popular_show_post_view'] = 1; }
       if (!isset($value['popular_show_premium'])) { $value['popular_show_premium'] = 1; }

?>

  <h3 class="cb_content_headline"><?php printf(__('Popular %s list', 'tcd-w'), $recipe_label);  ?></h3>
  <div class="cb_content">

   <ul class="design_radio_button">
    <li>
     <input type="radio" id="cb_display-type1-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type1" <?php checked( $value['cb_display'], 'type1' ); ?> />
     <label for="cb_display-type1-<?php echo $cb_index; ?>"><?php _e('Display this content to all user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type2-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type2" <?php checked( $value['cb_display'], 'type2' ); ?> />
     <label for="cb_display-type2-<?php echo $cb_index; ?>"><?php _e('Display this content only to logged-in user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type3-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type3" <?php checked( $value['cb_display'], 'type3' ); ?> />
     <label for="cb_display-type3-<?php echo $cb_index; ?>"><?php _e('Display this content only to logout user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type4-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type4" <?php checked( $value['cb_display'], 'type4' ); ?> />
     <label for="cb_display-type4-<?php echo $cb_index; ?>"><?php _e('Hide this content', 'tcd-w'); ?></label>
    </li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w');  ?></h4>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Label', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_headline]" value="<?php echo esc_attr( $value['popular_headline'] ); ?>"></li>
    <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_headline_font_size]" value="<?php esc_attr_e( $value['popular_headline_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_headline_font_size_mobile]" value="<?php esc_attr_e( $value['popular_headline_font_size_mobile'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_headline_font_color]" value="<?php echo esc_attr( $value['popular_headline_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_headline_bg_color]" value="<?php echo esc_attr( $value['popular_headline_bg_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Border color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_headline_border_color]" value="<?php echo esc_attr( $value['popular_headline_border_color'] ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Hide headline icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="dp_options[contents_builder][<?php echo $cb_index; ?>][hide_popular_headline_icon]" value="1" <?php checked( $value['hide_popular_headline_icon'], 1 ); ?>></li>
   </ul>
   <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($value['hide_popular_headline_icon']) { echo ' display:none;'; }; ?>">
    <li class="cf"><span class="label"><?php _e('Background color of headline icon', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_headline_icon_color]" value="<?php echo esc_attr( $value['popular_headline_icon_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Icon image', 'tcd-w');  ?></span>
     <div class="image_box cf">
      <div class="cf cf_media_field hide-if-no-js popular_headline_image">
       <input type="hidden" value="<?php echo esc_attr( $value['popular_headline_image'] ); ?>" id="popular_headline_image-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_headline_image]" class="cf_media_id">
       <div class="preview_field"><?php if($value['popular_headline_image']){ echo wp_get_attachment_image($value['popular_headline_image'], 'medium'); }; ?></div>
       <div class="buttton_area">
        <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
        <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$value['popular_headline_image']){ echo 'hidden'; }; ?>">
       </div>
      </div>
     </div>
     <div class="theme_option_message2">
      <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
      <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
     </div>
    </li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Description', 'tcd-w');  ?></h4>
   <textarea class="large-text" cols="50" rows="3" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_desc]"><?php echo esc_textarea(  $value['popular_desc'] ); ?></textarea>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_desc_font_size]" value="<?php esc_attr_e( $value['popular_desc_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_desc_font_size_mobile]" value="<?php esc_attr_e( $value['popular_desc_font_size_mobile'] ); ?>" /><span>px</span></li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Post list setting', 'tcd-w');  ?></h4>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Number of post to display', 'tcd-w');  ?></span>
     <select name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_num]">
      <?php for($i=3; $i<= 12; $i++): ?>
      <?php if( $i % 3 == 0 ){ ?>
      <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $value['popular_num'], $i ); ?>><?php echo esc_html($i); ?></option>
      <?php }; ?>
      <?php endfor; ?>
     </select>
    </li>
    <li class="cf"><span class="label"><?php _e('Scope of popular post', 'tcd-w');  ?></span>
     <select name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_range]">
     <option value="day" <?php selected('day', $value['popular_range']); ?>><?php _e('Daily', 'tcd-w'); ?></option>
     <option value="week" <?php selected('week', $value['popular_range']); ?>><?php _e('Weekly', 'tcd-w'); ?></option>
     <option value="month" <?php selected('month', $value['popular_range']); ?>><?php _e('Monthly', 'tcd-w'); ?></option>
     <option value="year" <?php selected('year', $value['popular_range']); ?>><?php _e('Yearly', 'tcd-w'); ?></option>
     <option value="" <?php selected('', $value['popular_range']); ?>><?php _e('All time', 'tcd-w'); ?></option>
     </select>
    </li>
    <li class="cf"><span class="label"><?php _e('Font size of title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_title_font_size]" value="<?php esc_attr_e( $value['popular_title_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size of title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_title_font_size_mobile]" value="<?php esc_attr_e( $value['popular_title_font_size_mobile'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php printf(__('Display premium %s icon', 'tcd-w'), $recipe_label); ?></span><input name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_show_premium]" type="checkbox" value="1" <?php checked( '1', $value['popular_show_premium'] ); ?> /></li>
    <li class="cf"><span class="label"><?php _e('Display category', 'tcd-w');  ?></span><input name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_show_category]" type="checkbox" value="1" <?php checked( '1', $value['popular_show_category'] ); ?> /></li>
    <li class="cf"><span class="label"><?php _e('Display post view count', 'tcd-w');  ?></span><input name="dp_options[contents_builder][<?php echo $cb_index; ?>][popular_show_post_view]" type="checkbox" value="1" <?php checked( '1', $value['popular_show_post_view'] ); ?> /></li>
   </ul>

<?php
     // 特集レシピ　-------------------------------------------------------------
     } elseif ($cb_content_select == 'featured_recipe_list') {

       if (!isset($value['featured_headline'])) { $value['featured_headline'] = ''; }
       if (!isset($value['featured_headline_font_size'])) { $value['featured_headline_font_size'] = '20'; }
       if (!isset($value['featured_headline_font_size_mobile'])) { $value['featured_headline_font_size_mobile'] = '15'; }
       if (!isset($value['featured_headline_font_color'])) { $value['featured_headline_font_color'] = '#000000'; }
       if (!isset($value['featured_headline_bg_color'])) { $value['featured_headline_bg_color'] = '#ffffff'; }
       if (!isset($value['featured_headline_border_color'])) { $value['featured_headline_border_color'] = '#dddddd'; }
       if (!isset($value['featured_headline_icon_color'])) { $value['featured_headline_icon_color'] = '#000000'; }
       if (!isset($value['featured_headline_image'])) { $value['featured_headline_image'] = false; }
       if (!isset($value['hide_featured_headline_icon'])) { $value['hide_featured_headline_icon'] = ''; }

       if (!isset($value['featured_desc'])) { $value['featured_desc'] = ''; }
       if (!isset($value['featured_desc_font_size'])) { $value['featured_desc_font_size'] = '16'; }
       if (!isset($value['featured_desc_font_size_mobile'])) { $value['featured_desc_font_size_mobile'] = '13'; }

       for ( $i = 1; $i <= 4; $i++ ) {
         if (!isset($value['featured_banner_type'.$i])) { $value['featured_banner_type'.$i] = 'type1'; }
         if (!isset($value['featured_image'.$i])) { $value['featured_image'.$i] = false; }
         if (!isset($value['featured_url'.$i])) { $value['featured_url'.$i] = ''; }
         if (!isset($value['featured_target'.$i])) { $value['featured_target'.$i] = ''; }
         if (!isset($value['featured_type1_border_color'.$i])) { $value['featured_type1_border_color'.$i] = '#ff8000'; }
         if (!isset($value['featured_type1_font_color'.$i])) { $value['featured_type1_font_color'.$i] = '#ff8000'; }
         if (!isset($value['featured_type2_font_color'.$i])) { $value['featured_type2_font_color'.$i] = '#ffffff'; }
         if (!isset($value['featured_use_overlay'.$i])) { $value['featured_use_overlay'.$i] = '1'; }
         if (!isset($value['featured_overlay_color'.$i])) { $value['featured_overlay_color'.$i] = '#000000'; }
         if (!isset($value['featured_title'.$i])) { $value['featured_title'.$i] = ''; }
         if (!isset($value['featured_sub_title'.$i])) { $value['featured_sub_title'.$i] = ''; }
         if (!isset($value['featured_title_font_size'.$i])) { $value['featured_title_font_size'.$i] = '26'; }
         if (!isset($value['featured_title_font_size_mobile'.$i])) { $value['featured_title_font_size_mobile'.$i] = '20'; }
         if (!isset($value['featured_sub_title_font_size'.$i])) { $value['featured_sub_title_font_size'.$i] = '16'; }
         if (!isset($value['featured_sub_title_font_size_mobile'.$i])) { $value['featured_sub_title_font_size_mobile'.$i] = '13'; }
         if (!isset($value['featured_title_font_type'.$i])) { $value['featured_title_font_type'.$i] = 'type3'; }
         if (!isset($value['featured_page'.$i])) { $value['featured_page'.$i] = ''; }
       }
?>

  <h3 class="cb_content_headline"><?php printf(__('Featured %s list', 'tcd-w'), $recipe_label);  ?></h3>
  <div class="cb_content">

   <ul class="design_radio_button">
    <li>
     <input type="radio" id="cb_display-type1-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type1" <?php checked( $value['cb_display'], 'type1' ); ?> />
     <label for="cb_display-type1-<?php echo $cb_index; ?>"><?php _e('Display this content to all user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type2-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type2" <?php checked( $value['cb_display'], 'type2' ); ?> />
     <label for="cb_display-type2-<?php echo $cb_index; ?>"><?php _e('Display this content only to logged-in user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type3-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type3" <?php checked( $value['cb_display'], 'type3' ); ?> />
     <label for="cb_display-type3-<?php echo $cb_index; ?>"><?php _e('Display this content only to logout user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type4-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type4" <?php checked( $value['cb_display'], 'type4' ); ?> />
     <label for="cb_display-type4-<?php echo $cb_index; ?>"><?php _e('Hide this content', 'tcd-w'); ?></label>
    </li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w');  ?></h4>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Label', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_headline]" value="<?php echo esc_attr( $value['featured_headline'] ); ?>"></li>
    <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_headline_font_size]" value="<?php esc_attr_e( $value['featured_headline_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_headline_font_size_mobile]" value="<?php esc_attr_e( $value['featured_headline_font_size_mobile'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_headline_font_color]" value="<?php echo esc_attr( $value['featured_headline_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_headline_bg_color]" value="<?php echo esc_attr( $value['featured_headline_bg_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Border color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_headline_border_color]" value="<?php echo esc_attr( $value['featured_headline_border_color'] ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Hide headline icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="dp_options[contents_builder][<?php echo $cb_index; ?>][hide_featured_headline_icon]" value="1" <?php checked( $value['hide_featured_headline_icon'], 1 ); ?>></li>
   </ul>
   <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($value['hide_featured_headline_icon']) { echo ' display:none;'; }; ?>">
    <li class="cf"><span class="label"><?php _e('Background color of headline icon', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_headline_icon_color]" value="<?php echo esc_attr( $value['featured_headline_icon_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Icon image', 'tcd-w');  ?></span>
     <div class="image_box cf">
      <div class="cf cf_media_field hide-if-no-js featured_headline_image">
       <input type="hidden" value="<?php echo esc_attr( $value['featured_headline_image'] ); ?>" id="featured_headline_image-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_headline_image]" class="cf_media_id">
       <div class="preview_field"><?php if($value['featured_headline_image']){ echo wp_get_attachment_image($value['featured_headline_image'], 'medium'); }; ?></div>
       <div class="buttton_area">
        <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
        <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$value['featured_headline_image']){ echo 'hidden'; }; ?>">
       </div>
      </div>
     </div>
     <div class="theme_option_message2">
      <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
      <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
     </div>
    </li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Description', 'tcd-w');  ?></h4>
   <textarea class="large-text" cols="50" rows="3" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_desc]"><?php echo esc_textarea(  $value['featured_desc'] ); ?></textarea>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_desc_font_size]" value="<?php esc_attr_e( $value['featured_desc_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size (moble)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_desc_font_size_mobile]" value="<?php esc_attr_e( $value['featured_desc_font_size_mobile'] ); ?>" /><span>px</span></li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Banner setting', 'tcd-w');  ?></h4>
   <div class="theme_option_message2">
    <p><?php _e('Border will be displayed around banner if you chose banner type1.', 'tcd-w'); ?></p>
   </div>
   <?php for($i = 1; $i <= 4; $i++) : ?>
   <div class="sub_box cf">
    <h3 class="theme_option_subbox_headline"><?php if($value['featured_title'.$i]){ echo esc_html($value['featured_title'.$i]); } else { printf(__('Banner%s', 'tcd-w'), $i); }; ?></h3>
    <div class="sub_box_content">

     <h4 class="theme_option_headline2"><?php _e('Banner type', 'tcd-w');  ?></h4>
     <ul class="design_radio_button">
      <li class="featured_banner_type1">
       <input type="radio" id="featured_banner_<?php echo $cb_index; ?>_<?php echo $i; ?>_type1" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_banner_type<?php echo $i; ?>]" value="type1" <?php checked( $value['featured_banner_type'.$i], 'type1' ); ?> />
       <label for="featured_banner_<?php echo $cb_index; ?>_<?php echo $i; ?>_type1"><?php _e('Type1 - Half width image', 'tcd-w'); ?></label>
      </li>
      <li class="featured_banner_type2">
       <input type="radio" id="featured_banner_<?php echo $cb_index; ?>_<?php echo $i; ?>_type2" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_banner_type<?php echo $i; ?>]" value="type2" <?php checked( $value['featured_banner_type'.$i], 'type2' ); ?> />
       <label for="featured_banner_<?php echo $cb_index; ?>_<?php echo $i; ?>_type2"><?php _e('Type2 - Full width image', 'tcd-w'); ?></label>
      </li>
     </ul>

     <h4 class="theme_option_headline2"><?php _e('Title', 'tcd-w');  ?></h4>
     <textarea class="headline_label large-text" cols="50" rows="2" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_title<?php echo $i; ?>]"><?php echo esc_textarea(  $value['featured_title'.$i] ); ?></textarea>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Font type', 'tcd-w');  ?></span>
       <select name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_title_font_type<?php echo $i; ?>]">
        <?php foreach ( $font_type_options as $option ) { ?>
        <option style="padding-right: 10px;" value="<?php echo esc_attr($option['value']); ?>" <?php selected( $value['featured_title_font_type'.$i], $option['value'] ); ?>><?php echo $option['label']; ?></option>
        <?php } ?>
       </select>
      </li>
     </ul>

     <h4 class="theme_option_headline2"><?php _e('Sub title', 'tcd-w');  ?></h4>
     <textarea class="large-text" cols="50" rows="2" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_sub_title<?php echo $i; ?>]"><?php echo esc_textarea(  $value['featured_sub_title'.$i] ); ?></textarea>

     <h4 class="theme_option_headline2"><?php _e('Font size setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_title_font_size<?php echo $i; ?>]" value="<?php esc_attr_e( $value['featured_title_font_size'.$i] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Sub title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_sub_title_font_size<?php echo $i; ?>]" value="<?php esc_attr_e( $value['featured_sub_title_font_size'.$i] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_title_font_size_mobile<?php echo $i; ?>]" value="<?php esc_attr_e( $value['featured_title_font_size_mobile'.$i] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Sub title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_sub_title_font_size_mobile<?php echo $i; ?>]" value="<?php esc_attr_e( $value['featured_sub_title_font_size_mobile'.$i] ); ?>" /><span>px</span></li>
     </ul>

     <div class="featured_banner_type1_field" style="<?php if($value['featured_banner_type'.$i] == 'type1') { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <h4 class="theme_option_headline2"><?php _e('Color setting', 'tcd-w');  ?></h4>
      <ul class="option_list">
       <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_type1_font_color<?php echo $i; ?>]" value="<?php echo esc_attr( $value['featured_type1_font_color'.$i] ); ?>" data-default-color="#ff8000" class="c-color-picker"></li>
       <li class="cf"><span class="label"><?php _e('Border color', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_type1_border_color<?php echo $i; ?>]" value="<?php echo esc_attr( $value['featured_type1_border_color'.$i] ); ?>" data-default-color="#ff8000" class="c-color-picker"></li>
      </ul>
     </div>

     <div class="featured_banner_type2_field" style="<?php if($value['featured_banner_type'.$i] == 'type2') { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <h4 class="theme_option_headline2"><?php _e('Color setting', 'tcd-w');  ?></h4>
      <ul class="option_list">
       <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_type2_font_color<?php echo $i; ?>]" value="<?php echo esc_attr( $value['featured_type2_font_color'.$i] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
      </ul>
     </div>

     <h4 class="theme_option_headline2"><?php _e('Image', 'tcd-w');  ?></h4>
     <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '810', '150'); ?></p>
     <div class="image_box cf">
      <div class="cf cf_media_field hide-if-no-js featured_image<?php echo $i; ?>">
       <input type="hidden" value="<?php echo esc_attr( $value['featured_image'.$i] ); ?>" id="featured_image-<?php echo $cb_index; ?>-<?php echo $i; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_image<?php echo $i; ?>]" class="cf_media_id">
       <div class="preview_field"><?php if($value['featured_image'.$i]){ echo wp_get_attachment_image($value['featured_image'.$i], 'medium'); }; ?></div>
       <div class="buttton_area">
        <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
        <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$value['featured_image'.$i]){ echo 'hidden'; }; ?>">
       </div>
      </div>
     </div>

     <h4 class="theme_option_headline2"><?php _e('URL', 'tcd-w');  ?></h4>
     <?php
          __('&mdash; Select &mdash;', 'tcd-w');
          $featured_dropdown_pages = wp_dropdown_pages( array(
            'class' => 'featured_recipe_list-featured_page',
            'echo' => false,
            'meta_key'     => '_wp_page_template',
            'meta_value'   => 'page-featured-post-list.php',
            'name' => 'dp_options[contents_builder]['.$cb_index.'][featured_page'.$i.']',
            'selected' => $value['featured_page'.$i],
            'show_option_none' => __( '&mdash; Select &mdash;', 'tcd-w' )
          ));
          if ( $featured_dropdown_pages ) :
     ?>
     <div class="theme_option_message2">
      <p><?php _e('Select the page below, if you want to link to specific featured page.<br />You can also enter url directly to input field if you want to link to other page.', 'tcd-w'); ?></p>
     </div>
     <div class="select_page" style="margin-bottom:20px;">
      <?php echo $featured_dropdown_pages; ?>
     </div>
     <?php endif; ?>
     <input type="text" style="width:100%;" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_url<?php echo $i; ?>]" value="<?php echo esc_attr( $value['featured_url'.$i] ); ?>">
     <p><label><input name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_target<?php echo $i; ?>]" type="checkbox" value="1" <?php checked( $value['featured_target'.$i], 1 ); ?>><?php _e('Open link in new window', 'tcd-w'); ?></label></p>

     <div class="featured_banner_type2_field" style="<?php if($value['featured_banner_type'.$i] == 'type2') { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <h4 class="theme_option_headline2"><?php _e( 'Overlay setting', 'tcd-w' ); ?></h4>
      <p class="displayment_checkbox"><label><input name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_use_overlay<?php echo $i; ?>]" type="checkbox" value="1" <?php checked( $value['featured_use_overlay'.$i], 1 ); ?>><?php _e( 'Use overlay', 'tcd-w' ); ?></label></p>
      <div style="<?php if($value['featured_use_overlay'.$i] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
       <ul class="option_list" style="border-top:1px dotted #ddd; padding:8px 0 0 0;">
        <li class="cf"><span class="label"><?php _e('Color of overlay', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][featured_overlay_color<?php echo $i; ?>]" value="<?php echo esc_attr( $value['featured_overlay_color'.$i] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
       </ul>
      </div><!-- END .header_slider_show_overlay -->
     </div>

     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_sub_box button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .sub_box_content -->
   </div><!-- END .sub_box -->
   <?php endfor; ?>

<?php
     // 新着ブログ　-------------------------------------------------------------
     } elseif ($cb_content_select == 'recent_blog_list') {

       if (!isset($value['recent_blog_headline'])) { $value['recent_blog_headline'] = ''; }
       if (!isset($value['recent_blog_headline_font_size'])) { $value['recent_blog_headline_font_size'] = '20'; }
       if (!isset($value['recent_blog_headline_font_size_mobile'])) { $value['recent_blog_headline_font_size_mobile'] = '15'; }
       if (!isset($value['recent_blog_headline_font_color'])) { $value['recent_blog_headline_font_color'] = '#000000'; }
       if (!isset($value['recent_blog_headline_bg_color'])) { $value['recent_blog_headline_bg_color'] = '#ffffff'; }
       if (!isset($value['recent_blog_headline_border_color'])) { $value['recent_blog_headline_border_color'] = '#dddddd'; }
       if (!isset($value['recent_blog_headline_icon_color'])) { $value['recent_blog_headline_icon_color'] = '#000000'; }
       if (!isset($value['recent_blog_headline_image'])) { $value['recent_blog_headline_image'] = false; }
       if (!isset($value['hide_recent_blog_headline_icon'])) { $value['hide_recent_blog_headline_icon'] = ''; }

       if (!isset($value['recent_blog_desc'])) { $value['recent_blog_desc'] = ''; }
       if (!isset($value['recent_blog_desc_font_size'])) { $value['recent_blog_desc_font_size'] = '16'; }
       if (!isset($value['recent_blog_desc_font_size_mobile'])) { $value['recent_blog_desc_font_size_mobile'] = '13'; }

       if (!isset($value['recent_blog_num'])) { $value['recent_blog_num'] = '6'; }
       if (!isset($value['recent_blog_title_font_size'])) { $value['recent_blog_title_font_size'] = '16'; }
       if (!isset($value['recent_blog_title_font_size_mobile'])) { $value['recent_blog_title_font_size_mobile'] = '14'; }
       if (!isset($value['recent_blog_show_date'])) { $value['recent_blog_show_date'] = 1; }
       if (!isset($value['recent_blog_show_premium'])) { $value['recent_blog_show_premium'] = 1; }


?>

  <h3 class="cb_content_headline"><?php _e('Recent blog list', 'tcd-w');  ?></h3>
  <div class="cb_content">

   <ul class="design_radio_button">
    <li>
     <input type="radio" id="cb_display-type1-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type1" <?php checked( $value['cb_display'], 'type1' ); ?> />
     <label for="cb_display-type1-<?php echo $cb_index; ?>"><?php _e('Display this content to all user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type2-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type2" <?php checked( $value['cb_display'], 'type2' ); ?> />
     <label for="cb_display-type2-<?php echo $cb_index; ?>"><?php _e('Display this content only to logged-in user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type3-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type3" <?php checked( $value['cb_display'], 'type3' ); ?> />
     <label for="cb_display-type3-<?php echo $cb_index; ?>"><?php _e('Display this content only to logout user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type4-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type4" <?php checked( $value['cb_display'], 'type4' ); ?> />
     <label for="cb_display-type4-<?php echo $cb_index; ?>"><?php _e('Hide this content', 'tcd-w'); ?></label>
    </li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w');  ?></h4>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Label', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_headline]" value="<?php echo esc_attr( $value['recent_blog_headline'] ); ?>"></li>
    <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_headline_font_size]" value="<?php esc_attr_e( $value['recent_blog_headline_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_headline_font_size_mobile]" value="<?php esc_attr_e( $value['recent_blog_headline_font_size_mobile'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_headline_font_color]" value="<?php echo esc_attr( $value['recent_blog_headline_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_headline_bg_color]" value="<?php echo esc_attr( $value['recent_blog_headline_bg_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Border color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_headline_border_color]" value="<?php echo esc_attr( $value['recent_blog_headline_border_color'] ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Hide headline icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="dp_options[contents_builder][<?php echo $cb_index; ?>][hide_recent_blog_headline_icon]" value="1" <?php checked( $value['hide_recent_blog_headline_icon'], 1 ); ?>></li>
   </ul>
   <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($value['hide_recent_blog_headline_icon']) { echo ' display:none;'; }; ?>">
    <li class="cf"><span class="label"><?php _e('Background color of headline icon', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_headline_icon_color]" value="<?php echo esc_attr( $value['recent_blog_headline_icon_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Icon image', 'tcd-w');  ?></span>
     <div class="image_box cf">
      <div class="cf cf_media_field hide-if-no-js recent_blog_headline_image">
       <input type="hidden" value="<?php echo esc_attr( $value['recent_blog_headline_image'] ); ?>" id="recent_blog_headline_image-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_headline_image]" class="cf_media_id">
       <div class="preview_field"><?php if($value['recent_blog_headline_image']){ echo wp_get_attachment_image($value['recent_blog_headline_image'], 'medium'); }; ?></div>
       <div class="buttton_area">
        <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
        <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$value['recent_blog_headline_image']){ echo 'hidden'; }; ?>">
       </div>
      </div>
     </div>
     <div class="theme_option_message2">
      <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
      <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
     </div>
    </li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Description', 'tcd-w');  ?></h4>
   <textarea class="large-text" cols="50" rows="3" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_desc]"><?php echo esc_textarea(  $value['recent_blog_desc'] ); ?></textarea>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_desc_font_size]" value="<?php esc_attr_e( $value['recent_blog_desc_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_desc_font_size_mobile]" value="<?php esc_attr_e( $value['recent_blog_desc_font_size_mobile'] ); ?>" /><span>px</span></li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Post list setting', 'tcd-w');  ?></h4>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Number of post to display', 'tcd-w');  ?></span>
     <select name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_num]">
      <?php for($i=3; $i<= 12; $i++): ?>
      <?php if( $i % 3 == 0 ){ ?>
      <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $value['recent_blog_num'], $i ); ?>><?php echo esc_html($i); ?></option>
      <?php }; ?>
      <?php endfor; ?>
     </select>
    </li>
    <li class="cf"><span class="label"><?php _e('Font size of title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_title_font_size]" value="<?php esc_attr_e( $value['recent_blog_title_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size of title (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_title_font_size_mobile]" value="<?php esc_attr_e( $value['recent_blog_title_font_size_mobile'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Display date', 'tcd-w');  ?></span><input name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_show_date]" type="checkbox" value="1" <?php checked( '1', $value['recent_blog_show_date'] ); ?> /></li>
    <li class="cf"><span class="label"><?php _e('Display premium post icon', 'tcd-w'); ?></span><input name="dp_options[contents_builder][<?php echo $cb_index; ?>][recent_blog_show_premium]" type="checkbox" value="1" <?php checked( '1', $value['recent_blog_show_premium'] ); ?> /></li>
   </ul>

<?php
     // お知らせ　-------------------------------------------------------------
     } elseif ($cb_content_select == 'news_list') {

       if (!isset($value['news_headline'])) { $value['news_headline'] = ''; }
       if (!isset($value['news_headline_font_size'])) { $value['news_headline_font_size'] = '20'; }
       if (!isset($value['news_headline_font_size_mobile'])) { $value['news_headline_font_size_mobile'] = '15'; }
       if (!isset($value['news_headline_font_color'])) { $value['news_headline_font_color'] = '#000000'; }
       if (!isset($value['news_headline_bg_color'])) { $value['news_headline_bg_color'] = '#ffffff'; }
       if (!isset($value['news_headline_border_color'])) { $value['news_headline_border_color'] = '#dddddd'; }
       if (!isset($value['news_headline_icon_color'])) { $value['news_headline_icon_color'] = '#000000'; }
       if (!isset($value['news_headline_image'])) { $value['news_headline_image'] = false; }
       if (!isset($value['hide_news_headline_icon'])) { $value['hide_news_headline_icon'] = ''; }

       if (!isset($value['news_desc'])) { $value['news_desc'] = ''; }
       if (!isset($value['news_desc_font_size'])) { $value['news_desc_font_size'] = '16'; }
       if (!isset($value['news_desc_font_size_mobile'])) { $value['news_desc_font_size_mobile'] = '13'; }

       if (!isset($value['news_num'])) { $value['news_num'] = 3; }
       if (!isset($value['news_date_color'])) { $value['news_date_color'] = '#ff0000'; }
?>

  <h3 class="cb_content_headline"><?php printf(__('%s list', 'tcd-w'), $news_label); ?></h3>
  <div class="cb_content">

   <ul class="design_radio_button">
    <li>
     <input type="radio" id="cb_display-type1-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type1" <?php checked( $value['cb_display'], 'type1' ); ?> />
     <label for="cb_display-type1-<?php echo $cb_index; ?>"><?php _e('Display this content to all user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type2-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type2" <?php checked( $value['cb_display'], 'type2' ); ?> />
     <label for="cb_display-type2-<?php echo $cb_index; ?>"><?php _e('Display this content only to logged-in user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type3-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type3" <?php checked( $value['cb_display'], 'type3' ); ?> />
     <label for="cb_display-type3-<?php echo $cb_index; ?>"><?php _e('Display this content only to logout user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type4-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type4" <?php checked( $value['cb_display'], 'type4' ); ?> />
     <label for="cb_display-type4-<?php echo $cb_index; ?>"><?php _e('Hide this content', 'tcd-w'); ?></label>
    </li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w');  ?></h4>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Label', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_headline]" value="<?php echo esc_attr( $value['news_headline'] ); ?>"></li>
    <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_headline_font_size]" value="<?php esc_attr_e( $value['news_headline_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_headline_font_size_mobile]" value="<?php esc_attr_e( $value['news_headline_font_size_mobile'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_headline_font_color]" value="<?php echo esc_attr( $value['news_headline_font_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Background color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_headline_bg_color]" value="<?php echo esc_attr( $value['news_headline_bg_color'] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Border color of headline', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_headline_border_color]" value="<?php echo esc_attr( $value['news_headline_border_color'] ); ?>" data-default-color="#dddddd" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Hide headline icon', 'tcd-w'); ?></span><input class="hide_icon" type="checkbox" name="dp_options[contents_builder][<?php echo $cb_index; ?>][hide_news_headline_icon]" value="1" <?php checked( $value['hide_news_headline_icon'], 1 ); ?>></li>
   </ul>
   <ul class="option_list" style="border-top:1px dotted #ddd; padding:10px 0 0 0; margin-top:-10px;<?php if($value['hide_news_headline_icon']) { echo ' display:none;'; }; ?>">
    <li class="cf"><span class="label"><?php _e('Background color of headline icon', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_headline_icon_color]" value="<?php echo esc_attr( $value['news_headline_icon_color'] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
    <li class="cf"><span class="label"><?php _e('Icon image', 'tcd-w');  ?></span>
     <div class="image_box cf">
      <div class="cf cf_media_field hide-if-no-js news_headline_image">
       <input type="hidden" value="<?php echo esc_attr( $value['news_headline_image'] ); ?>" id="news_headline_image-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_headline_image]" class="cf_media_id">
       <div class="preview_field"><?php if($value['news_headline_image']){ echo wp_get_attachment_image($value['news_headline_image'], 'medium'); }; ?></div>
       <div class="buttton_area">
        <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
        <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$value['news_headline_image']){ echo 'hidden'; }; ?>">
       </div>
      </div>
     </div>
     <div class="theme_option_message2">
      <p><?php _e('Upload your original image, if you want to change the icon of headline.', 'tcd-w'); ?></p>
      <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '20', '20'); ?></p>
     </div>
    </li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Description', 'tcd-w');  ?></h4>
   <textarea class="large-text" cols="50" rows="3" name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_desc]"><?php echo esc_textarea(  $value['news_desc'] ); ?></textarea>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_desc_font_size]" value="<?php esc_attr_e( $value['news_desc_font_size'] ); ?>" /><span>px</span></li>
    <li class="cf"><span class="label"><?php _e('Font size (mobile)', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_desc_font_size_mobile]" value="<?php esc_attr_e( $value['news_desc_font_size_mobile'] ); ?>" /><span>px</span></li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Post list setting', 'tcd-w');  ?></h4>
   <ul class="option_list">
    <li class="cf"><span class="label"><?php _e('Number of post to display', 'tcd-w');  ?></span>
     <select name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_num]">
      <?php for($i=3; $i<= 12; $i++): ?>
      <option style="padding-right: 10px;" value="<?php echo esc_attr($i); ?>" <?php selected( $value['news_num'], $i ); ?>><?php echo esc_html($i); ?></option>
      <?php endfor; ?>
     </select>
    </li>
    <li class="cf color_picker_bottom"><span class="label"><?php _e('Color of date', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][news_date_color]" value="<?php echo esc_attr( $value['news_date_color'] ); ?>" data-default-color="#ff0000" class="c-color-picker"></li>
   </ul>

<?php
     // バナー一覧　-------------------------------------------------------------
     } elseif ($cb_content_select == 'banner') {

       for ( $i = 1; $i <= 4; $i++ ) {
         if (!isset($value['banner_image'.$i])) { $value['banner_image'.$i] = false; }
         if (!isset($value['banner_url'.$i])) { $value['banner_url'.$i] = ''; }
         if (!isset($value['banner_target'.$i])) { $value['banner_target'.$i] = ''; }
         if (!isset($value['banner_font_color'.$i])) { $value['banner_font_color'.$i] = '#ffffff'; }
         if (!isset($value['banner_use_overlay'.$i])) { $value['banner_use_overlay'.$i] = '1'; }
         if (!isset($value['banner_overlay_color'.$i])) { $value['banner_overlay_color'.$i] = '#000000'; }
         if (!isset($value['banner_title'.$i])) { $value['banner_title'.$i] = ''; }
         if (!isset($value['banner_sub_title'.$i])) { $value['banner_sub_title'.$i] = ''; }
         if (!isset($value['banner_title_font_size'.$i])) { $value['banner_title_font_size'.$i] = '24'; }
         if (!isset($value['banner_sub_title_font_size'.$i])) { $value['banner_sub_title_font_size'.$i] = '14'; }
         if (!isset($value['banner_title_font_type'.$i])) { $value['banner_title_font_type'.$i] = 'type3'; }
       }

?>

  <h3 class="cb_content_headline"><?php _e('Banner', 'tcd-w'); ?></h3>
  <div class="cb_content">

   <ul class="design_radio_button">
    <li>
     <input type="radio" id="cb_display-type1-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type1" <?php checked( $value['cb_display'], 'type1' ); ?> />
     <label for="cb_display-type1-<?php echo $cb_index; ?>"><?php _e('Display this content to all user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type2-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type2" <?php checked( $value['cb_display'], 'type2' ); ?> />
     <label for="cb_display-type2-<?php echo $cb_index; ?>"><?php _e('Display this content only to logged-in user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type3-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type3" <?php checked( $value['cb_display'], 'type3' ); ?> />
     <label for="cb_display-type3-<?php echo $cb_index; ?>"><?php _e('Display this content only to logout user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type4-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type4" <?php checked( $value['cb_display'], 'type4' ); ?> />
     <label for="cb_display-type4-<?php echo $cb_index; ?>"><?php _e('Hide this content', 'tcd-w'); ?></label>
    </li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Banner setting', 'tcd-w');  ?></h4>
   <?php for($i = 1; $i <= 4; $i++) : ?>
   <div class="sub_box cf">
    <h3 class="theme_option_subbox_headline"><?php if($value['banner_title'.$i]){ echo esc_html($value['banner_title'.$i]); } else { printf(__('Banner%s', 'tcd-w'), $i); }; ?></h3>
    <div class="sub_box_content">

     <h4 class="theme_option_headline2"><?php _e('Title', 'tcd-w');  ?></h4>
     <textarea class="headline_label large-text" cols="50" rows="2" name="dp_options[contents_builder][<?php echo $cb_index; ?>][banner_title<?php echo $i; ?>]"><?php echo esc_textarea(  $value['banner_title'.$i] ); ?></textarea>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Font type', 'tcd-w');  ?></span>
       <select name="dp_options[contents_builder][<?php echo $cb_index; ?>][banner_title_font_type<?php echo $i; ?>]">
        <?php foreach ( $font_type_options as $option ) { ?>
        <option style="padding-right: 10px;" value="<?php echo esc_attr($option['value']); ?>" <?php selected( $value['banner_title_font_type'.$i], $option['value'] ); ?>><?php echo $option['label']; ?></option>
        <?php } ?>
       </select>
      </li>
     </ul>

     <h4 class="theme_option_headline2"><?php _e('Sub title', 'tcd-w');  ?></h4>
     <textarea class="large-text" cols="50" rows="2" name="dp_options[contents_builder][<?php echo $cb_index; ?>][banner_sub_title<?php echo $i; ?>]"><?php echo esc_textarea(  $value['banner_sub_title'.$i] ); ?></textarea>

     <h4 class="theme_option_headline2"><?php _e('Font size setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][banner_title_font_size<?php echo $i; ?>]" value="<?php esc_attr_e( $value['banner_title_font_size'.$i] ); ?>" /><span>px</span></li>
      <li class="cf"><span class="label"><?php _e('Sub title', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][banner_sub_title_font_size<?php echo $i; ?>]" value="<?php esc_attr_e( $value['banner_sub_title_font_size'.$i] ); ?>" /><span>px</span></li>
     </ul>

     <h4 class="theme_option_headline2"><?php _e('Color setting', 'tcd-w');  ?></h4>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Font color', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][banner_font_color<?php echo $i; ?>]" value="<?php echo esc_attr( $value['banner_font_color'.$i] ); ?>" data-default-color="#ffffff" class="c-color-picker"></li>
     </ul>

     <h4 class="theme_option_headline2"><?php _e('Image', 'tcd-w');  ?></h4>
     <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '394', '150'); ?></p>
     <div class="image_box cf">
      <div class="cf cf_media_field hide-if-no-js banner_image<?php echo $i; ?>">
       <input type="hidden" value="<?php echo esc_attr( $value['banner_image'.$i] ); ?>" id="banner_image-<?php echo $cb_index; ?>-<?php echo $i; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][banner_image<?php echo $i; ?>]" class="cf_media_id">
       <div class="preview_field"><?php if($value['banner_image'.$i]){ echo wp_get_attachment_image($value['banner_image'.$i], 'medium'); }; ?></div>
       <div class="buttton_area">
        <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
        <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$value['banner_image'.$i]){ echo 'hidden'; }; ?>">
       </div>
      </div>
     </div>

     <h4 class="theme_option_headline2"><?php _e('URL', 'tcd-w');  ?></h4>
     <input type="text" style="width:100%;" name="dp_options[contents_builder][<?php echo $cb_index; ?>][banner_url<?php echo $i; ?>]" value="<?php echo esc_attr( $value['banner_url'.$i] ); ?>">
     <p><label><input name="dp_options[contents_builder][<?php echo $cb_index; ?>][banner_target<?php echo $i; ?>]" type="checkbox" value="1" <?php checked( $value['banner_target'.$i], 1 ); ?>><?php _e('Open link in new window', 'tcd-w'); ?></label></p>

     <h4 class="theme_option_headline2"><?php _e( 'Overlay setting', 'tcd-w' ); ?></h4>
     <p class="displayment_checkbox"><label><input name="dp_options[contents_builder][<?php echo $cb_index; ?>][banner_use_overlay<?php echo $i; ?>]" type="checkbox" value="1" <?php checked( $value['banner_use_overlay'.$i], 1 ); ?>><?php _e( 'Use overlay', 'tcd-w' ); ?></label></p>
     <div style="<?php if($value['banner_use_overlay'.$i] == 1) { echo 'display:block;'; } else { echo 'display:none;'; }; ?>">
      <ul class="option_list" style="border-top:1px dotted #ddd; padding:8px 0 0 0;">
       <li class="cf"><span class="label"><?php _e('Color of overlay', 'tcd-w'); ?></span><input type="text" name="dp_options[contents_builder][<?php echo $cb_index; ?>][banner_overlay_color<?php echo $i; ?>]" value="<?php echo esc_attr( $value['banner_overlay_color'.$i] ); ?>" data-default-color="#000000" class="c-color-picker"></li>
      </ul>
     </div><!-- END .header_slider_show_overlay -->

     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_sub_box button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .sub_box_content -->
   </div><!-- END .sub_box -->
   <?php endfor; ?>


<?php
     // 自由入力欄　-------------------------------------------------------------
     } elseif ($cb_content_select == 'free_space') {

       if (!isset($value['free_space'])) {
         $value['free_space'] = '';
       }
?>
  <h3 class="cb_content_headline"><?php _e('Free space', 'tcd-w');  ?></h3>
  <div class="cb_content">

   <ul class="design_radio_button">
    <li>
     <input type="radio" id="cb_display-type1-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type1" <?php checked( $value['cb_display'], 'type1' ); ?> />
     <label for="cb_display-type1-<?php echo $cb_index; ?>"><?php _e('Display this content to all user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type2-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type2" <?php checked( $value['cb_display'], 'type2' ); ?> />
     <label for="cb_display-type2-<?php echo $cb_index; ?>"><?php _e('Display this content only to logged-in user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type3-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type3" <?php checked( $value['cb_display'], 'type3' ); ?> />
     <label for="cb_display-type3-<?php echo $cb_index; ?>"><?php _e('Display this content only to logout user', 'tcd-w'); ?></label>
    </li>
    <li>
     <input type="radio" id="cb_display-type4-<?php echo $cb_index; ?>" name="dp_options[contents_builder][<?php echo $cb_index; ?>][cb_display]" value="type4" <?php checked( $value['cb_display'], 'type4' ); ?> />
     <label for="cb_display-type4-<?php echo $cb_index; ?>"><?php _e('Hide this content', 'tcd-w'); ?></label>
    </li>
   </ul>

   <h4 class="theme_option_headline2"><?php _e('Free space', 'tcd-w');  ?></h4>
   <?php
        wp_editor(
          $value['free_space'],
          'cb_wysiwyg_editor-' . $cb_index,
          array (
            'textarea_name' => 'dp_options[contents_builder][' . $cb_index . '][free_space]'
          )
       );
   ?>

<?php
     // ボタンの表示　-------------------------------------------------------------
     } else {
?>
  <h3 class="cb_content_headline"><?php echo esc_html($cb_content_select); ?></h3>
  <div class="cb_content">

<?php
     }
?>

   <ul class="button_list cf">
    <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
    <li><a href="#" class="button-ml close-content"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
   </ul>

  </div><!-- END .cb_content -->

</div><!-- END .cb_content_wrap -->

<?php

} // END the_cb_content_setting()

/**
 * クローン用のリッチエディター化処理をしないようにする
 * クローン後のリッチエディター化はjsで行う
 */
function cb_tiny_mce_before_init( $mceInit, $editor_id ) {
	if ( strpos( $editor_id, 'cb_cloneindex' ) !== false ) {
		$mceInit['wp_skip_init'] = true;
	}
	return $mceInit;
}
add_filter( 'tiny_mce_before_init', 'cb_tiny_mce_before_init', 10, 2 );

?>
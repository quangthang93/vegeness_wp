<?php
/*
 * 会員管理の設定
 */


// Add default values
add_filter( 'before_getting_design_plus_option', 'add_member_dp_default_options' );


// Add label of logo tab
add_action( 'tcd_tab_labels', 'add_member_tab_label' );


// Add HTML of logo tab
add_action( 'tcd_tab_panel', 'add_member_tab_panel' );


// Register sanitize function
add_filter( 'theme_options_validate', 'add_member_theme_options_validate' );


// タブの名前
function add_member_tab_label( $tab_labels ) {
	$tab_labels['member'] = __( 'Member management', 'tcd-w' );
	return $tab_labels;
}


// 初期値
function add_member_dp_default_options( $dp_default_options ) {

	// 会員制設定
	$dp_default_options['users_can_register'] = get_option( 'users_can_register' ) ? 1 : 0;
	$dp_default_options['disable_wp_login_php'] = 1;
	$dp_default_options['use_registration_email_activation'] = 1;
	$dp_default_options['notice_sent_registration_email_activate'] =  __( "Sent email to entered email address.\nPlease read email and activate account.", 'tcd-w' );
	$dp_default_options['use_receive_email'] = 1;
	$dp_default_options['label_receive_email'] = __( 'Receive mail from web site', 'tcd-w' );

	// ログインフォームの設定
	$dp_default_options['register_headline'] = __( 'Member registration', 'tcd-w' );
	$dp_default_options['register_headline_font_size'] = '24';
	$dp_default_options['register_image'] = false;
	$dp_default_options['register_catch'] = __( 'To enjoy the full contents,\n please register as a member.', 'tcd-w' );
	$dp_default_options['register_catch_font_size'] = '16';
	$dp_default_options['show_register_policy'] = 1;
	$dp_default_options['register_policy_url'] = '';
	$dp_default_options['register_button_label'] = __( 'Free registration', 'tcd-w' );

	// メールの設定
	$dp_default_options['mail_from_email'] = get_bloginfo( 'admin_email' );
	$dp_default_options['mail_from_name'] = get_bloginfo( 'name' );
	$dp_default_options['mail_registration_activate_subject'] = __( 'Provisional registration is completed / [blog_name]', 'tcd-w' );
	$dp_default_options['mail_registration_activate_body'] = __( 'Dear [user_email],

This mail is delivered to those who were newly registered in [blog_name].

Your provisional registration is complete.

Please access the following URL and proceed with registration within 24hours.
[account_activation_url]

Sincerely, [blog_name]', 'tcd-w' );

	$dp_default_options['mail_registration_subject'] = __( 'Registration completed / [blog_name]', 'tcd-w' );
	$dp_default_options['mail_registration_body'] = __( 'Dear [user_display_name],

Thank you for registering for [blog_name].

Your registration is complete.

E-Mail address: [user_email]
Password: ******** (Do not display for personal information)
Login URL: [login_url]

If you forgot your password, you can reissue it at the URL below.
[reset_password_url]

We are looling forward to you enjoying [blog_name].', 'tcd-w' );

	$dp_default_options['mail_registration_admin_to'] = get_bloginfo( 'admin_email' );
	$dp_default_options['mail_registration_admin_subject'] = __( 'New registration / [blog_name]', 'tcd-w' );
	$dp_default_options['mail_registration_admin_body'] = __( 'You can find the new registration below.

-----
User name: [user_display_name]
E-Mail address: [user_email]', 'tcd-w' );

	$dp_default_options['mail_reset_password_subject'] = __( 'Reset password / [blog_name]', 'tcd-w' );
	$dp_default_options['mail_reset_password_body'] = __( 'Dear [user_display_name],

Thank you very much for using [blog_name].

You can reissue your password to access the URL below within 24hours.
[set_new_password_url]

Sincerely, [blog_name]', 'tcd-w' );

	$dp_default_options['mail_withdraw_subject'] = __( 'Withdrawal completed / [blog_name]', 'tcd-w' );
	$dp_default_options['mail_withdraw_body'] = __( 'Dear [user_display_name],

We notify you of the completion of withdrawal from [blog_name].
Thank you for using [blog_name].
We hope to see you again.

Sincerely, [blog_name]', 'tcd-w' );

	$dp_default_options['mail_withdraw_admin_to'] = get_bloginfo( 'admin_email' );
	$dp_default_options['mail_withdraw_admin_subject'] = __( 'Withdrawal / [blog_name]', 'tcd-w' );
	$dp_default_options['mail_withdraw_admin_body'] = __( 'We notify you of a withdrawal from [blog_name].

-----
User name: [user_display_name]
E-Mail address: [user_email]', 'tcd-w' );

	return $dp_default_options;
}


// 入力欄の出力　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_member_tab_panel( $options ) {

  global $dp_default_options;

?>

<div id="tab-content-member" class="tab-content">

   <?php // 会員制設定 ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Membership setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e( 'Frontend member registration', 'tcd-w' ); ?></h4>
     <p><label><input name="dp_options[users_can_register]" type="checkbox" value="1" <?php checked( 1, is_multisite() ? $options['users_can_register'] : get_option( 'users_can_register' ) ); ?>><?php _e( 'Frontend registration available', 'tcd-w' ); ?></label></p>
<?php
if ( is_multisite() ) :
?>
     <p class="description"><?php printf( __( 'In multisite, it is influenced by multisite "Allow new registrations" setting. (Current setting: %s)', 'tcd-w' ), get_option( 'users_can_register' ) ? __( 'Registration avalable', 'tcd-w' ) : __( 'Registration disabled', 'tcd-w' ) ); ?></p>
<?php
else :
?>
     <p class="description"><?php _e( 'This setting change the "Anyone can register" setting in <a href="options-general.php" target="_blank">WordPress General Settings</a>.', 'tcd-w' ); ?></p>
<?php
endif;
?>
     <h4 class="theme_option_headline2"><?php _e( 'WordPress login page', 'tcd-w' ); ?></h4>
     <label><input name="dp_options[disable_wp_login_php]" type="checkbox" value="1" <?php checked( 1, $options['disable_wp_login_php'] ); ?>><?php _e( 'Disable "wp-login.php"', 'tcd-w' ); ?></label>
     <h4 class="theme_option_headline2"><?php _e( 'User account activation in registration', 'tcd-w' ); ?></h4>
     <label><input id="use_registration_email_activation" name="dp_options[use_registration_email_activation]" type="checkbox" value="1" <?php checked( 1, $options['use_registration_email_activation'] ); ?>><?php _e( 'Send an account activation email', 'tcd-w' ); ?></label>
     <div class="registration_email_activation"<?php if (!$options['use_registration_email_activation']) echo ' style="display:none;" '; ?>>
      <h4 class="theme_option_headline2"><?php _e( 'Message after sent an account activation email', 'tcd-w' ); ?></h4>
      <textarea class="full_width" cols="50" rows="2" name="dp_options[notice_sent_registration_email_activate]"><?php echo esc_textarea(  $options['notice_sent_registration_email_activate'] ); ?></textarea>
     </div>
     <div class="displayment_checkbox">
      <h4 class="theme_option_headline2"><?php _e( 'Use notification setting', 'tcd-w' ); ?></h4>
      <div class="theme_option_message2">
       <p><?php _e( 'Check this box to notify members by e-mail.<br>*Note that the notification function for member news needs to be set separately in the account page → notification settings for member news.', 'tcd-w' ); ?></p>
      </div>
      <label><input id="use_receive_email" name="dp_options[use_receive_email]" type="checkbox" value="1" <?php checked( 1, $options['use_receive_email'] ); ?>><?php _e( 'Use notification', 'tcd-w' ); ?></label>
     </div>
     <div style="<?php if ( ! $options['use_receive_email'] ) echo 'display:none;'; ?>">
      <h4 class="theme_option_headline2"><?php _e( 'Notification label', 'tcd-w' ); ?></h4>
      <input class="full_width" name="dp_options[label_receive_email]" type="text" value="<?php echo esc_attr( $options['label_receive_email'] ); ?>">
     </div>

     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // ログインフォームの設定 ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('Register form setting', 'tcd-w');  ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Headline setting', 'tcd-w');  ?></h4>
     <input class="full_width" type="text" name="dp_options[register_headline]" value="<?php esc_attr_e( $options['register_headline'] ); ?>" />
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[register_headline_font_size]" value="<?php esc_attr_e( $options['register_headline_font_size'] ); ?>" /><span>px</span></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e('Header image', 'tcd-w');  ?></h4>
     <p><?php printf(__('Recommend image size. Width:%1$spx, Height:%2$spx.', 'tcd-w'), '500', '170'); ?></p>
     <div class="image_box cf">
      <div class="cf cf_media_field hide-if-no-js register_image">
       <input type="hidden" value="<?php echo esc_attr( $options['register_image'] ); ?>" id="register_image" name="dp_options[register_image]" class="cf_media_id">
       <div class="preview_field"><?php if($options['register_image']){ echo wp_get_attachment_image($options['register_image'], 'full'); }; ?></div>
       <div class="buttton_area">
        <input type="button" value="<?php _e('Select Image', 'tcd-w'); ?>" class="cfmf-select-img button">
        <input type="button" value="<?php _e('Remove Image', 'tcd-w'); ?>" class="cfmf-delete-img button <?php if(!$options['register_image']){ echo 'hidden'; }; ?>">
       </div>
      </div>
     </div>
     <h4 class="theme_option_headline2"><?php _e('Catchphrase setting', 'tcd-w');  ?></h4>
     <textarea class="full_width" cols="50" rows="2" name="dp_options[register_catch]"><?php echo esc_textarea(  $options['register_catch'] ); ?></textarea>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Font size', 'tcd-w'); ?></span><input class="font_size hankaku" type="text" name="dp_options[register_catch_font_size]" value="<?php esc_attr_e( $options['register_catch_font_size'] ); ?>" /><span>px</span></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e('Privacy policy setting', 'tcd-w'); ?></h4>
     <div class="theme_option_message2">
      <p><?php _e( 'You can set the URL of the page that explains the membership terms and conditions.', 'tcd-w' ); ?></p>
     </div>
     <ul class="option_list">
      <li class="cf"><span class="label"><?php _e('Display message', 'tcd-w'); ?></span><input name="dp_options[show_register_policy]" type="checkbox" value="1" <?php checked( $options['show_register_policy'], 1 ); ?>></li>
      <li class="cf"><span class="label"><?php _e('Privacy policy page URL', 'tcd-w'); ?></span><input class="full_width" type="text" name="dp_options[register_policy_url]" value="<?php esc_attr_e( $options['register_policy_url'] ); ?>" /></li>
     </ul>
     <h4 class="theme_option_headline2"><?php _e('Regsiter button', 'tcd-w');  ?></h4>
     <input class="full_width" type="text" name="dp_options[register_button_label]" value="<?php esc_attr_e( $options['register_button_label'] ); ?>" />
     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // メールの設定 ?>
   <div class="theme_option_field cf theme_option_field_ac">
    <h3 class="theme_option_headline"><?php _e('E-mail setting', 'tcd-w'); ?></h3>
    <div class="theme_option_field_ac_content">
     <h4 class="theme_option_headline2"><?php _e('Sender email address', 'tcd-w');  ?></h4>
     <div class="theme_option_message2">
      <p><?php _e( 'This is an email address automatically sent to registered users.<br>* If the user can not receive the email, they need to permit us to receive email address registered here.', 'tcd-w' ); ?></p>
     </div>
     <input class="full_width" type="text" name="dp_options[mail_from_email]" value="<?php echo esc_attr( $options['mail_from_email'] ); ?>">
     <h4 class="theme_option_headline2"><?php _e('Sender name', 'tcd-w');  ?></h4>
     <div class="theme_option_message2">
      <p><?php _e( 'This is the name that is displayed at the email sender when you register as a member or receive a notification.', 'tcd-w' ); ?></p>
     </div>
     <input class="full_width" type="text" name="dp_options[mail_from_name]" value="<?php echo esc_attr( $options['mail_from_name'] ); ?>">

     <div style="margin-top:30px;">
	     <?php // 仮会員登録メール -------------------------------------- ?>
	     <div class="registration_email_activation sub_box cf"<?php if (!$options['use_registration_email_activation']) echo ' style="display:none;" '; ?>>
	      <h3 class="theme_option_subbox_headline"><?php _e('Account activation email', 'tcd-w'); ?></h3>
	      <div class="sub_box_content">
	       <h4 class="theme_option_headline2"><?php _e('Title of email', 'tcd-w');  ?></h4>
	       <input class="full_width" type="text" name="dp_options[mail_registration_activate_subject]" value="<?php echo esc_attr( $options['mail_registration_activate_subject'] ); ?>">
	       <h4 class="theme_option_headline2"><?php _e('Content of email', 'tcd-w');  ?></h4>
	       <textarea class="full_width" cols="50" rows="10" name="dp_options[mail_registration_activate_body]"><?php echo esc_textarea( $options['mail_registration_activate_body'] ); ?></textarea>
	       <p class="description"><?php _e( 'Available Variables', 'tcd-w' ); ?>: [blog_name], [blog_url], [user_display_name], [user_email], [account_activation_url]<br><?php printf( __( '%s will expire in 24 hours.', 'tcd-w' ), '[account_activation_url]' ); ?></p>
	       <ul class="button_list cf">
	        <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
	        <li><a class="close_sub_box button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
	       </ul>
	      </div><!-- END .sub_box_content -->
	     </div><!-- END .sub_box -->

	     <?php // 会員登録メール -------------------------------------- ?>
	     <div class="sub_box cf">
	      <h3 class="theme_option_subbox_headline"><?php _e('Registration completion email', 'tcd-w'); ?></h3>
	      <div class="sub_box_content">
	       <h4 class="theme_option_headline2"><?php _e('Title of email', 'tcd-w');  ?></h4>
	       <input class="full_width" type="text" name="dp_options[mail_registration_subject]" value="<?php echo esc_attr( $options['mail_registration_subject'] ); ?>">
	       <h4 class="theme_option_headline2"><?php _e('Content of email', 'tcd-w');  ?></h4>
	       <textarea class="full_width" cols="50" rows="10" name="dp_options[mail_registration_body]"><?php echo esc_textarea( $options['mail_registration_body'] ); ?></textarea>
	       <p class="description"><?php _e( 'Available Variables', 'tcd-w' ); ?>: [blog_name], [blog_url], [user_display_name], [user_email], [password], [login_url], [mypage_url], [reset_password_url]</p>
	       <h4 class="theme_option_headline2"><?php _e( 'Destination email for admin notification', 'tcd-w' ); ?></h4>
	       <input class="regular-text" type="text" name="dp_options[mail_registration_admin_to]" value="<?php echo esc_attr( $options['mail_registration_admin_to'] ); ?>">
	       <p class="description"><?php _e( 'If empty, do not send admin notification.', 'tcd-w' ); ?></p>
	       <h4 class="theme_option_headline2"><?php _e('Title of admin notification', 'tcd-w');  ?></h4>
	       <input class="full_width" type="text" name="dp_options[mail_registration_admin_subject]" value="<?php echo esc_attr( $options['mail_registration_admin_subject'] ); ?>">
	       <h4 class="theme_option_headline2"><?php _e('Content of admin notification', 'tcd-w');  ?></h4>
	       <textarea class="full_width" cols="50" rows="10" name="dp_options[mail_registration_admin_body]"><?php echo esc_textarea( $options['mail_registration_admin_body'] ); ?></textarea>
	       <p class="description"><?php _e( 'Available Variables', 'tcd-w' ); ?>: [blog_name], [blog_url], [user_display_name], [user_email], [password], [login_url], [mypage_url], [reset_password_url]</p>
	       <ul class="button_list cf">
	        <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
	        <li><a class="close_sub_box button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
	       </ul>
	      </div><!-- END .sub_box_content -->
	     </div><!-- END .sub_box -->

	     <?php // パスワードの再発行メール -------------------------------------- ?>
	     <div class="sub_box cf">
	      <h3 class="theme_option_subbox_headline"><?php _e('Password reset email', 'tcd-w'); ?></h3>
	      <div class="sub_box_content">
	       <h4 class="theme_option_headline2"><?php _e('Title of email', 'tcd-w');  ?></h4>
	       <input class="full_width" type="text" name="dp_options[mail_reset_password_subject]" value="<?php echo esc_attr( $options['mail_reset_password_subject'] ); ?>">
	       <h4 class="theme_option_headline2"><?php _e('Content of email', 'tcd-w');  ?></h4>
	       <textarea class="full_width" cols="50" rows="10" name="dp_options[mail_reset_password_body]"><?php echo esc_textarea( $options['mail_reset_password_body'] ); ?></textarea>
	       <p class="description"><?php _e( 'Available Variables', 'tcd-w' ); ?>: [blog_name], [blog_url], [user_display_name], [user_email], [set_new_password_url]<br><?php printf( __( '%s will expire in 24 hours.', 'tcd-w' ), '[set_new_password_url]' ); ?></p>
	       <ul class="button_list cf">
	        <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
	        <li><a class="close_sub_box button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
	       </ul>
	      </div><!-- END .sub_box_content -->
	     </div><!-- END .sub_box -->

	     <?php // 退会メール -------------------------------------- ?>
	     <div class="sub_box cf">
	      <h3 class="theme_option_subbox_headline"><?php _e('Withdrawal email', 'tcd-w'); ?></h3>
	      <div class="sub_box_content">
	       <h4 class="theme_option_headline2"><?php _e('Title of email', 'tcd-w');  ?></h4>
	       <input class="full_width" type="text" name="dp_options[mail_withdraw_subject]" value="<?php echo esc_attr( $options['mail_withdraw_subject'] ); ?>">
	       <h4 class="theme_option_headline2"><?php _e('Content of email', 'tcd-w');  ?></h4>
	       <textarea class="full_width" cols="50" rows="10" name="dp_options[mail_withdraw_body]"><?php echo esc_textarea( $options['mail_withdraw_body'] ); ?></textarea>
	       <p class="description"><?php _e( 'Available Variables', 'tcd-w' ); ?>: [blog_name], [blog_url], [user_display_name], [user_email]</p>
	       <h4 class="theme_option_headline2"><?php _e( 'Destination email for admin notification', 'tcd-w' ); ?></h4>
	       <input class="regular-text" type="text" name="dp_options[mail_withdraw_admin_to]" value="<?php echo esc_attr( $options['mail_withdraw_admin_to'] ); ?>">
	       <p class="description"><?php _e( 'If empty, do not send admin notification.', 'tcd-w' ); ?></p>
	       <h4 class="theme_option_headline2"><?php _e('Title of admin notification', 'tcd-w');  ?></h4>
	       <input class="full_width" type="text" name="dp_options[mail_withdraw_admin_subject]" value="<?php echo esc_attr( $options['mail_withdraw_admin_subject'] ); ?>">
	       <h4 class="theme_option_headline2"><?php _e('Content of admin notification', 'tcd-w');  ?></h4>
	       <textarea class="full_width" cols="50" rows="10" name="dp_options[mail_withdraw_admin_body]"><?php echo esc_textarea( $options['mail_withdraw_admin_body'] ); ?></textarea>
	       <p class="description"><?php _e( 'Available Variables', 'tcd-w' ); ?>: [blog_name], [blog_url], [user_display_name], [user_email]</p>
	       <ul class="button_list cf">
	        <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
	        <li><a class="close_sub_box button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
	       </ul>
	      </div><!-- END .sub_box_content -->
	     </div><!-- END .sub_box -->
     </div>

     <ul class="button_list cf">
      <li><input type="submit" class="button-ml ajax_button" value="<?php echo __( 'Save Changes', 'tcd-w' ); ?>" /></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


   <?php // 会員エクスポートの設定 ?>
   <div class="theme_option_field cf theme_option_field_ac export_members">
    <h3 class="theme_option_headline"><?php _e('Export Members', 'tcd-w'); ?></h3>
    <div class="theme_option_field_ac_content">
     <div class="theme_option_message2">
      <p><?php _e( 'Export members as csv file.', 'tcd-w' ); ?></p>
     </div>
     <h4 class="theme_option_headline2"><?php _e( 'Registration date', 'tcd-w' ); ?></h4>
     <input type="text" class="tcd_export_members-date_from datepicker" value="" size="10">
     <?php _e( ' - ', 'tcd-w' ); ?>
     <input type="text" class="tcd_export_members-date_to datepicker" value="" size="10">
     <h4 class="theme_option_headline2 registration_email_activation"><?php _e( 'Registration type', 'tcd-w' ); ?></h4>
     <select class="tcd_export_members-registration_type registration_email_activation">
      <option value="standard"><?php _e( 'Standard Member (Account created in WordPress Users.)', 'tcd-w' ); ?></option>
      <option value="provisional"><?php _e( 'Provisional registration', 'tcd-w' ); ?></option>
     </select>
     <h4 class="theme_option_headline2 member_news_notify"><?php echo esc_html($options['label_receive_email'] ? $options['label_receive_email'] : __( 'Receive mail from web site', 'tcd-w' )); ?></h4>
     <select class="tcd_export_members-receive_email member_news_notify">
      <option value=""><?php _e( 'Not specified', 'tcd-w' ); ?></option>
      <option value="yes"><?php _e( 'Do receive', 'tcd-w' ); ?></option>
      <option value="no"><?php _e( 'Do not receive', 'tcd-w' ); ?></option>
     </select>
     <h4 class="theme_option_headline2"><?php _e( 'CSV Encoding', 'tcd-w' ); ?></h4>
     <select class="tcd_export_members-csv_encoding">
      <option value=""><?php _e( 'UTF-8', 'tcd-w' ); ?></option>
      <option value="sjis"><?php _e( 'Shift JIS (for Japanese Microsoft Excel)', 'tcd-w' ); ?></option>
     </select>
     <ul class="button_list cf">
      <li><a class="button-ml" id="tcd_export_members" href="#" data-nonce="<?php echo wp_create_nonce( 'tcd_export_members_nonce' ); ?>"><?php echo __( 'Export', 'tcd-w' ); ?></a></li>
      <li><a class="close_ac_content button-ml" href="#"><?php echo __( 'Close', 'tcd-w' ); ?></a></li>
     </ul>
    </div><!-- END .theme_option_field_ac_content -->
   </div><!-- END .theme_option_field -->


</div><!-- END .tab-content -->

<?php
} // END add_member_tab_panel()


// バリデーション　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function add_member_theme_options_validate( $input ) {

	global $dp_default_options;

	// 会員制設定
	$input['users_can_register'] = ! empty( $input['users_can_register'] ) ? 1 : 0;
	$input['disable_wp_login_php'] = ! empty( $input['disable_wp_login_php'] ) ? 1 : 0;
	$input['use_registration_email_activation'] = ! empty( $input['use_registration_email_activation'] ) ? 1 : 0;
	$input['notice_sent_registration_email_activate'] = wp_filter_nohtml_kses( $input['notice_sent_registration_email_activate'] );
	$input['use_receive_email'] = ! empty( $input['use_receive_email'] ) ? 1 : 0;
	$input['label_receive_email'] = wp_filter_nohtml_kses( $input['label_receive_email'] );

	// update wp option
	if ( ! is_multisite() && get_option( 'users_can_register' ) != $input['users_can_register'] ) {
		update_option( 'users_can_register', $input['users_can_register'] );
	}

	//ログインフォーム
	$input['register_headline'] = wp_filter_nohtml_kses( $input['register_headline'] );
	$input['register_headline_font_size'] = wp_filter_nohtml_kses( $input['register_headline_font_size'] );
	$input['register_image'] = wp_filter_nohtml_kses( $input['register_image'] );
	$input['register_catch'] = wp_filter_nohtml_kses( $input['register_catch'] );
	$input['register_catch_font_size'] = wp_filter_nohtml_kses( $input['register_catch_font_size'] );
	if ( ! isset( $input['show_register_policy'] ) )
		$input['show_register_policy'] = null;
	$input['show_register_policy'] = ( $input['show_register_policy'] == 1 ? 1 : 0 );
	$input['register_policy_url'] = wp_kses_post( $input['register_policy_url'] );
	$input['register_button_label'] = wp_filter_nohtml_kses( $input['register_button_label'] );

	// メールの設定
	$input['mail_from_name'] = trim( $input['mail_from_name'] );
	$input['mail_from_email'] = trim( $input['mail_from_email'] );
	if ( ! $input['mail_from_email'] || ! is_email( $input['mail_from_email'] ) ) {
		$input['mail_from_email'] = '';
	}
	// othor mail inputs are no varidate

	return $input;
}

/**
 * 会員情報エクスポート実行
 */
function tcd_member_export() {
	// ノンスが無ければ終了
	if ( empty( $_POST['tcd_export_members_nonce'] ) )
		return;

	// ノンスチェック
	if ( ! wp_verify_nonce( $_POST['tcd_export_members_nonce'], 'tcd_export_members_nonce' ) )
		wp_die( __( 'Invalid nonce token.', 'tcd-w' ) );

	$dp_options = get_design_plus_option();

	// タイムアウト対策
	set_time_limit( 600 );

	$current_ts = current_time( 'timestamp', false );
	$current_ts_gmt = current_time( 'timestamp', true );
	$current_ts_offset = $current_ts - $current_ts_gmt;

	// エンコード
	$internal_encoding = mb_internal_encoding();
	if ( ! empty( $_POST['csv_encoding'] ) && 'sjis' == $_POST['csv_encoding'] ) {
		$csv_encoding = 'SJIS-win';
	} elseif ( 'UTF-8' != $internal_encoding ) {
		$csv_encoding = 'UTF-8';
	} else {
		$csv_encoding = null;
	}

	// ファイル名
	$filename = get_bloginfo( 'name' ) . '-' . 'export_members.csv';

	// ヘッダー
	header( 'Content-Type: application/octet-stream' );
	header( 'Content-Disposition: attachment; filename="' . rawurlencode( $filename ) . '"' );

	// ファイルストリーム
	$fp = fopen( 'php://output','w' );

	// 仮会員の場合
	if ( $dp_options['use_registration_email_activation'] && ! empty( $_POST['registration_type'] ) && 'provisional' == $_POST['registration_type'] ) {
		global $wpdb;

		// csvヘッダ
		$csv_header = array(
			0 => __( 'User ID', 'tcd-w' ),
			1 => __( 'Nickname', 'tcd-w' ),
			2 => __( 'E-mail', 'tcd-w' ),
			3 => $dp_options['label_receive_email'] ? $dp_options['label_receive_email'] : __( 'Receive mail from web site', 'tcd-w' ),
			4 => __( 'Registration date', 'tcd-w' )
		);

		// 通知未使用
		if ( ! $dp_options['use_receive_email'] ) {
			unset( $csv_header[3] );
		}

		// エンコード
		if ( $csv_encoding ) {
			mb_convert_variables( $csv_encoding, $internal_encoding, $csv_header );
		}

		// csvヘッダ出力
		fputcsv( $fp, $csv_header, ',', '"' );

		// オプションから仮会員情報取得
		$tcd_every_provisional_users = get_option( 'tcd_every_provisional_users', array() );

		// 登録日指定があればフィルター
		if ( $tcd_every_provisional_users && ( ! empty( $_POST['date_from'] ) || ! empty( $_POST['date_to'] ) ) ) {
			if ( ! empty( $_POST['date_from'] ) && ! empty( $_POST['date_to'] ) ) {

				$date_from_ts = strtotime( $_POST['date_from'] ) - $current_ts_offset;
				$date_to_ts = strtotime( $_POST['date_to'] ) + 86400 - 1 - $current_ts_offset;
			} elseif ( ! empty( $_POST['date_from'] ) ) {
				$date_from_ts = strtotime( $_POST['date_from'] ) - $current_ts_offset;
				$date_to_ts = null;
			} elseif ( ! empty( $_POST['date_to'] ) ) {
				$date_from_ts = null;
				$date_to_ts = strtotime( $_POST['date_to'] ) + 86400 - 1 - $current_ts_offset;
			}

			foreach ( $tcd_every_provisional_users as $key => $user ) {
				if (
					empty( $user['timestamp'] ) ||
					( $date_from_ts && $date_from_ts > $user['timestamp'] ) ||
					( $date_to_ts &&  $date_to_ts < $user['timestamp'] )
				) {
					unset( $tcd_every_provisional_users[ $key ] );
				}
			}
		}

		// データ出力
		if ( $tcd_every_provisional_users ) {
			foreach ( $tcd_every_provisional_users as $key => $user ) {
				$row_data = array(
					0 => '',
					1 => $user['display_name'],
					2 => $user['email'],
					3 => isset( $user['receive_email'] ) ? $user['receive_email'] : '',
					4 => isset( $user['timestamp'] ) ? $user['timestamp'] : ''
				);

				// 通知未使用
				if ( ! $dp_options['use_receive_email'] ) {
					unset( $row_data[3] );
				} elseif ( 'yes' === $row_data[3] ) {
					$row_data[3] = __( 'Do receive', 'tcd-w' );
				} else {
					$row_data[3] = __( 'Do not receive', 'tcd-w' );
				}

				// 登録日時をGMTからローカル時間に
				if ( $row_data[4] && 0 != $current_ts_offset ) {
					$row_data[4] = date( 'Y-m-d H:i:s', $row_data[4] + $current_ts_offset );
				}

				// エンコード
				if ( $csv_encoding ) {
					mb_convert_variables( $csv_encoding, $internal_encoding, $row_data );
				}

				fputcsv( $fp, $row_data, ',', '"' );
			}
		}

	// 本会員の場合
	} else {
		// csvヘッダ
		$csv_header = array(
			0 => __( 'User ID', 'tcd-w' ),
			1 => __( 'Nickname', 'tcd-w' ),
			2 => __( 'E-mail', 'tcd-w' ),
			3 => $dp_options['label_receive_email'] ? $dp_options['label_receive_email'] : __( 'Receive mail from web site', 'tcd-w' ),
			4 => __( 'Registration date', 'tcd-w' )
		);

		// 通知未使用
		if ( ! $dp_options['use_receive_email'] ) {
			unset( $csv_header[3] );
		}

		// エンコード
		if ( $csv_encoding ) {
			mb_convert_variables( $csv_encoding, $internal_encoding, $csv_header );
		}

		// csvヘッダ出力
		fputcsv( $fp, $csv_header, ',', '"' );

		$args = array(
			'orderby' => 'ID',
			'order' => 'ASC',
			'role' => apply_filters( 'tcd_membership_account_create_user_role', 'subscriber' )
		);

		// 登録日指定 wp_users.user_registeredはgmtなので注意
		if ( ! empty( $_POST['date_from'] ) && ! empty( $_POST['date_to'] ) ) {
			$date_from_ts = strtotime( $_POST['date_from'] ) - $current_ts_offset;
			$date_to_ts = strtotime( $_POST['date_to'] ) + 86400 - 1 - $current_ts_offset;
			$args['date_query'] = array(
				'after' => array(
					'year'		=> date( 'Y', $date_from_ts ),
					'month'		=> (int) date( 'm', $date_from_ts ),
					'day'		=> (int) date( 'd', $date_from_ts ),
					'hour'		=> (int) date( 'H', $date_from_ts ),
					'minute'	=> (int) date( 'i', $date_from_ts ),
					'second'	=> (int) date( 's', $date_from_ts )
				),
				'before' => array(
					'year'		=> date( 'Y', $date_to_ts ),
					'month'		=> (int) date( 'm', $date_to_ts ),
					'day'		=> (int) date( 'd', $date_to_ts ),
					'hour'		=> (int) date( 'H', $date_to_ts ),
					'minute'	=> (int) date( 'i', $date_to_ts ),
					'second'	=> (int) date( 's', $date_to_ts )
				),
				'inclusive' => true,
			);
		} elseif ( ! empty( $_POST['date_from'] ) ) {
			$date_from_ts = strtotime( $_POST['date_from'] ) - $current_ts_offset;
			$args['date_query'] = array(
				'after' => array(
					'year'		=> date( 'Y', $date_from_ts ),
					'month'		=> (int) date( 'm', $date_from_ts ),
					'day'		=> (int) date( 'd', $date_from_ts ),
					'hour'		=> (int) date( 'H', $date_from_ts ),
					'minute'	=> (int) date( 'i', $date_from_ts ),
					'second'	=> (int) date( 's', $date_from_ts )
				),
				'inclusive' => true,
			);
		} elseif ( ! empty( $_POST['date_to'] ) ) {
			$date_to_ts = strtotime( $_POST['date_to'] ) + 86400 - 1 - $current_ts_offset;
			$args['date_query'] = array(
				'before' => array(
					'year'		=> date( 'Y', $date_to_ts ),
					'month'		=> (int) date( 'm', $date_to_ts ),
					'day'		=> (int) date( 'd', $date_to_ts ),
					'hour'		=> (int) date( 'H', $date_to_ts ),
					'minute'	=> (int) date( 'i', $date_to_ts ),
					'second'	=> (int) date( 's', $date_to_ts )
				),
				'inclusive' => true,
			);
		}

		// 通知フィルター
		if ( $dp_options['use_receive_email'] && ! empty( $_POST['receive_email'] ) ) {
			$args['meta_key'] = 'receive_email';
			$args['meta_value'] = 'yes';

			if ( 'yes' !== $_POST['receive_email'] ) {
				$args['meta_compare'] = '!=';
			}
		}

		// ユーザー取得
		$users = get_users( $args );

		// データ出力
		if ( $users ) {
			foreach ( $users as $user ) {
				$row_data = array(
					0 => $user->ID,
					1 => $user->display_name,
					2 => $user->user_email,
					3 => $user->receive_email,
					4 => $user->user_registered
				);

				// 通知未使用
				if ( ! $dp_options['use_receive_email'] ) {
					unset( $row_data[3] );
				} elseif ( 'yes' === $row_data[3] ) {
					$row_data[3] = __( 'Do receive', 'tcd-w' );
				} else {
					$row_data[3] = __( 'Do not receive', 'tcd-w' );
				}

				// 登録日時をGMTからローカル時間に
				if ( 0 != $current_ts_offset ) {
					$row_data[4] = date( 'Y-m-d H:i:s', strtotime( $row_data[4] ) + $current_ts_offset );
				}

				// エンコード
				if ( $csv_encoding ) {
					mb_convert_variables( $csv_encoding, $internal_encoding, $row_data );
				}

				fputcsv( $fp, $row_data, ',', '"' );
			}
		}
	}

	fclose( $fp );
	exit;

}
add_action( 'admin_init', 'tcd_member_export' );


?>
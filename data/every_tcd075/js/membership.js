jQuery(function($){

	/**
	 * login, register form
	 */
	$('#header_login').on('click',function() {
		$('#modal_overlay').addClass('open');
		return false;
	});

	$('#header_register').on('click',function() {
		$('#modal_overlay').addClass('open');
		$('#register_modal_wrap').addClass('show');
		$('#login_modal_wrap').addClass('hide');
		return false;
	});

	$('.register_link').on('click',function() {
		if ($('#register_modal_wrap').length) {
			$('#modal_overlay').addClass('open');
			$('#register_modal_wrap').addClass('show');
			$('#login_modal_wrap').addClass('hide');
		} else {
			$('#modal_overlay').addClass('open');
		}
		return false;
	});

	$('#lost_password, #modal_overlay #lost_password').on('click',function() {
		if (!$(this).closest('#modal_overlay').length) {
			$('#modal_overlay').addClass('open');
		}
		$('#login_modal_wrap').addClass('hide');
		$('#password_modal_wrap').addClass('show');
		return false;
	});

	$('#create_account').on('click',function() {
		$('#login_modal_wrap').addClass('hide');
		$('#register_modal_wrap').addClass('show');
		return false;
	});

	$(document).on('click touchend', '#modal_overlay', function(event) {
		if ($(event.target).is('#modal_overlay, .close_modal_button')) {
			$('#modal_overlay').removeClass('open');
			$('#login_modal_wrap').removeClass('hide');
			$('#password_modal_wrap').removeClass('show');
			$('#register_modal_wrap').removeClass('show');
			$('#message_modal_wrap').removeClass('show');
			return false;
		}
	});

	/**
	 * フォームの確認メッセージ、重複送信対策
	 */
	$('form').on('submit.processing', function(){
		var $form = $(this);
		if ($form.hasClass('is-processing')) return false;

		if ($form.data('confirm')) {
			if (!window.confirm($form.data('confirm'))) {
				return false;
			}
		}

		$form.addClass('is-processing');
		if ($form.is('#edit-account-form, #delete-account-form')) {
			$('#edit-account-form, #delete-account-form').addClass('is-processing');
		}
	});

	/**
	 * モーダルのパスワード再設定のajax送信
	 */
	$('#js-modal-reset-password-form').on('submit', function(){
		var $form = $(this);

		// フォームデータ
		var fd = new FormData();
		$form.find(':input').not(':button, :submit').each(function(){
			fd.append(this.name, this.value);
		});

		// ajax送信フラグ
		fd.append('ajax_reset_password', 1);

		// 結果メッセージ削除
		$form.find('.ajax-result').slideUp(200, function(){
			$(this).remove();
		});

		// ajax送信
		$.ajax({
			url: $form.attr('action'),
			type: 'POST',
			data: fd,
			processData: false,
			contentType: false,
			complete: function() {
				$form.removeClass('is-processing');
			},
			success: function(data, textStatus, XMLHttpRequest) {
				if (data.success) {
					if (data.message) {
						$resultMessage = $('<div class="form-message ajax-result">'+data.message+'</div>').hide();
						$form.find('.email').before($resultMessage);
						$resultMessage.slideDown(500);
						$form.trigger('reset');
					}
				} else {
					if (data.error_message) {
						$resultMessage = $('<div class="form-error ajax-result">'+data.error_message+'</div>').hide();
						$form.find('.email').before($resultMessage);
						$resultMessage.slideDown(500);
					}
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$resultMessage = $('<div class="form-error ajax-result">'+TCD_MEMBERSHIP.ajaxSubmitError+'</div>').hide();
				$form.find('.email').before($resultMessage);
				$resultMessage.slideDown(500);
			}
		});

		return false;
	});

	/**
	 * プロフィール画像
	 */
	if ($('#edit-account-form .image_area').length) {
		var $image_area = $('#edit-account-form .image_area');
		var $profile_image_file = $image_area.find('[name="profile_image_file"]');
		var $profile_image_url = $image_area.find('[name="profile_image_url"]');
		var $uploaded_profile_image_url = $image_area.find('[name="uploaded_profile_image_url"]');
		var $image_current = $image_area.find('.image_current')
		var $image_bg = $image_area.find('.image_bg')
		var $upload_button = $image_area.find('.upload_button')
		var $delete_button = $image_area.find('.delete_button')

		// 画像開く
		$upload_button.on('click', function() {
			$profile_image_file.trigger('click');
			return false;
		});

		// 画像選択
		$profile_image_file.on('change', function() {
			if (this.files.length > 0) {
				if(!this.files[0].type.match(/^image\/(png|jpeg|gif)$/)) {
					showModalMessage(TCD_MEMBERSHIP.not_image_file);
					this.value = '';
					return false;
				}

				uploadImagePreview($image_current, this.files[0]);
				check_delete_button();
			}
		});
		// リロード対策
		$profile_image_file.trigger('change');

		// 画像削除
		$delete_button.on('click', function() {
			var replace_image_url;

			if ($profile_image_file.val()) {
				$profile_image_file.val('');
				$image_current.removeClass('has-upload-preview');

				if ($uploaded_profile_image_url.val()) {
					replace_image_url = $uploaded_profile_image_url.val();
				} else if ($profile_image_url.val()) {
					replace_image_url = $profile_image_url.val();
				}
			} else if ($uploaded_profile_image_url.val()) {
				$uploaded_profile_image_url.val('');

				if ($profile_image_url.val()) {
					replace_image_url = $profile_image_url.val();
				}
			} else if ($profile_image_url.val()) {
				$profile_image_url.val('');

				// 現画像削除フラグ追加
				$image_area.append('<input type="hidden" name="delete-image-profile_image" value="1">');
			}

			if (replace_image_url) {
				$image_current.css('background-image', 'url("' + replace_image_url + '")');
			} else {
				$image_current.attr('style', '');
			}

			check_delete_button();

			return false;
		});

		// 画像削除ボタン表示
		var check_delete_button = function() {
			if ($profile_image_file.val() || $uploaded_profile_image_url.val() || $profile_image_url.val()) {
				$image_bg.hide();
				$delete_button.show();
			} else {
				$image_bg.show();
				$delete_button.hide();
			}
		};
		check_delete_button();

		/**
		 * 画像プレビュー
		 * 参考 https://egashira.jp/image-resize-before-upload
		 */
		var uploadImagePreview = function($target, file) {
			$target = $($target);

			if (!$target.length || !file || !file.type.match(/^image\/(png|jpeg|gif)$/)) return;

			// 縮小する画像のサイズ
			var maxWidth = 300;
			var maxHeight = 300;
			var crop = 1;

			var img = new Image();
			var reader = new FileReader();

			reader.onload = function(e) {
				var data = this.result;

				img.onload = function() {
					var iw = img.naturalWidth, ih = img.naturalHeight;
					var width = iw, height = ih;
					var orientation;

					// JPEGの場合には、EXIFからOrientation（回転）情報を取得
					if (data.split(',')[0].match('jpeg')) {
						orientation = getOrientation(data);
					}
					// JPEG以外や、JPEGでもEXIFが無い場合などには、標準の値に設定
					orientation = orientation || 1;

					// 90度回転など、縦横が入れ替わる場合には事前に最大幅、高さを入れ替えておく
					if (orientation > 4) {
						var tmpMaxWidth = maxWidth;
						maxWidth = maxHeight;
						maxHeight = tmpMaxWidth;
					}

					// 縮小画像サイズ計算
					var cropWidth = 0, cropHeight = 0;
					if (width > maxWidth || height > maxHeight) {
						if (crop) {
							if (width >= maxWidth && height >= maxHeight) {
								if (height / width < maxHeight / maxWidth) {
									width = Math.round(width * maxHeight / height );
									height = maxHeight;
									cropSx = Math.floor( (width - maxWidth) / 2 );
									cropWidth = maxWidth;
									cropHeight = height;
								} else {
									height = Math.round(height * maxWidth / width );
									width = maxWidth;
									cropSy = Math.floor( (height - maxHeight) / 2 );
									cropWidth = width;
									cropHeight = maxHeight;
								}
							} else if (width > maxWidth) {
								cropWidth = maxWidth
								cropHeight = height;
							} else if (height > maxHeight) {
								cropWidth = width;
								cropHeight = maxHeight;
							}
						} else {
							var ratio = width/maxWidth;
							if (ratio <= height/maxHeight) {
								ratio = height/maxHeight;
							}

							width = Math.round(iw/ratio);
							height = Math.round(ih/ratio);
						}
					}

					var canvas = document.createElement('canvas');
					var ctx = canvas.getContext('2d');
					ctx.save();

					// EXIFのOrientation情報からCanvasを回転させておく
					transformCoordinate(canvas, width, height, orientation);

					// iPhoneのサブサンプリング問題の回避
					// see http://d.hatena.ne.jp/shinichitomita/20120927/1348726674
					var subsampled = detectSubsampling(img);
					if (subsampled) {
						iw /= 2;
						ih /= 2;
					}

					// Orientation聞かせながらタイルレンダリング
					var d = 1024; // size of tiling canvas
					var tmpCanvas = document.createElement('canvas');
					tmpCanvas.width = tmpCanvas.height = d;
					var tmpCtx = tmpCanvas.getContext('2d');
					var vertSquashRatio = detectVerticalSquash(img, iw, ih);
					var dw = Math.ceil(d * width / iw);
					var dh = Math.ceil(d * height / ih / vertSquashRatio);
					var sy = 0;
					var dy = 0;
					while (sy < ih) {
						var sx = 0;
						var dx = 0;
						while (sx < iw) {
							tmpCtx.clearRect(0, 0, d, d);
							tmpCtx.drawImage(img, -sx, -sy);
							// 何度もImageDataオブジェクトとCanvasの変換を行ってるけど、Orientation関連で仕方ない。本当はputImageDataであれば良いけどOrientation効かない
							var imageData = tmpCtx.getImageData(0, 0, d, d);
							var resampled = resampleHermite(imageData, d, d, dw, dh);
							ctx.drawImage(resampled, 0, 0, dw, dh, dx, dy, dw, dh);
							sx += d;
							dx += dw;
						}
						sy += d;
						dy += dh;
					}
					ctx.restore();

					var resizedSrc;

					// 切り抜き
					if (cropWidth && cropHeight) {
						var offsetX = Math.floor(canvas.width - cropWidth) / 2;
						var offsetY = Math.floor(canvas.height - cropHeight) / 2;

						// リサイズ後のcanvasから切り抜きしてtmpCanvasに張り付け
						var imageData = ctx.getImageData(offsetX, offsetY, cropWidth, cropHeight);
						tmpCanvas.width = cropWidth;
						tmpCanvas.height = cropHeight;
						tmpCtx.putImageData(imageData, 0, 0);

						canvas = ctx = null;

						// 切り抜き後のbase64形式の画像データ取得
						resizedSrc = tmpCtx.canvas.toDataURL('image/jpeg', 0.9);

						tmpCanvas = tmpCtx = null;
					} else {
						tmpCanvas = tmpCtx = null;

						// リサイズ後のbase64形式の画像データ取得
						resizedSrc = ctx.canvas.toDataURL('image/jpeg', 0.9);

						canvas = ctx = null;
					}

					// $targetの背景画像にセット
					$target.addClass('has-upload-preview').css('background-image', 'url("' + resizedSrc + '")');
				}
				img.src = data;
			}
			reader.readAsDataURL(file);

			// hermite filterかけてジャギーを削除する
			function resampleHermite(img, W, H, W2, H2){
				var canvas = document.createElement('canvas');
				canvas.width = W2;
				canvas.height = H2;
				var ctx = canvas.getContext('2d');
				var img2 = ctx.createImageData(W2, H2);
				var data = img.data;
				var data2 = img2.data;
				var ratio_w = W / W2;
				var ratio_h = H / H2;
				var ratio_w_half = Math.ceil(ratio_w/2);
				var ratio_h_half = Math.ceil(ratio_h/2);
				for(var j = 0; j < H2; j++){
					for(var i = 0; i < W2; i++){
						var x2 = (i + j*W2) * 4;
						var weight = 0;
						var weights = 0;
						var gx_r = 0, gx_g = 0, gx_b = 0, gx_a = 0;
						var center_y = (j + 0.5) * ratio_h;
						for(var yy = Math.floor(j * ratio_h); yy < (j + 1) * ratio_h; yy++){
							var dy = Math.abs(center_y - (yy + 0.5)) / ratio_h_half;
							var center_x = (i + 0.5) * ratio_w;
							var w0 = dy*dy;
							for(var xx = Math.floor(i * ratio_w); xx < (i + 1) * ratio_w; xx++){
								var dx = Math.abs(center_x - (xx + 0.5)) / ratio_w_half;
								var w = Math.sqrt(w0 + dx*dx);
								if(w >= -1 && w <= 1){
									weight = 2 * w*w*w - 3*w*w + 1;
									if(weight > 0){
										dx = 4*(xx + yy*W);
										gx_r += weight * data[dx];
										gx_g += weight * data[dx + 1];
										gx_b += weight * data[dx + 2];
										gx_a += weight * data[dx + 3];
										weights += weight;
									}
								}
							}
						}
						data2[x2]		 = gx_r / weights;
						data2[x2 + 1] = gx_g / weights;
						data2[x2 + 2] = gx_b / weights;
						data2[x2 + 3] = gx_a / weights;
					}
				}
				ctx.putImageData(img2, 0, 0);
				return canvas;
			};

			// JPEGのEXIFからOrientationのみを取得する
			function getOrientation(imgDataURL){
				var byteString = atob(imgDataURL.split(',')[1]);
				var orientaion = byteStringToOrientation(byteString);
				return orientaion;

				function byteStringToOrientation(img){
					var head = 0;
					var orientation;
					while (1){
						if (img.charCodeAt(head) == 255 & img.charCodeAt(head + 1) == 218) {break;}
						if (img.charCodeAt(head) == 255 & img.charCodeAt(head + 1) == 216) {
							head += 2;
						}
						else {
							var length = img.charCodeAt(head + 2) * 256 + img.charCodeAt(head + 3);
							var endPoint = head + length + 2;
							if (img.charCodeAt(head) == 255 & img.charCodeAt(head + 1) == 225) {
								var segment = img.slice(head, endPoint);
								var bigEndian = segment.charCodeAt(10) == 77;
								if (bigEndian) {
									var count = segment.charCodeAt(18) * 256 + segment.charCodeAt(19);
								} else {
									var count = segment.charCodeAt(18) + segment.charCodeAt(19) * 256;
								}
								for (var i=0;i<count;i++){
									var field = segment.slice(20 + 12 * i, 32 + 12 * i);
									if ((bigEndian && field.charCodeAt(1) == 18) || (!bigEndian && field.charCodeAt(0) == 18)) {
										orientation = bigEndian ? field.charCodeAt(9) : field.charCodeAt(8);
									}
								}
								break;
							}
							head = endPoint;
						}
						if (head > img.length){break;}
					}
					return orientation;
				}
			}

			// iPhoneのサブサンプリングを検出
			function detectSubsampling(img) {
				var iw = img.naturalWidth, ih = img.naturalHeight;
				if (iw * ih > 1024 * 1024) {
					var canvas = document.createElement('canvas');
					canvas.width = canvas.height = 1;
					var ctx = canvas.getContext('2d');
					ctx.drawImage(img, -iw + 1, 0);
					return ctx.getImageData(0, 0, 1, 1).data[3] === 0;
				} else {
					return false;
				}
			}

			// iPhoneの縦画像でひしゃげて表示される問題の回避
			function detectVerticalSquash(img, iw, ih) {
				var canvas = document.createElement('canvas');
				canvas.width = 1;
				canvas.height = ih;
				var ctx = canvas.getContext('2d');
				ctx.drawImage(img, 0, 0);
				var data = ctx.getImageData(0, 0, 1, ih).data;
				var sy = 0;
				var ey = ih;
				var py = ih;
				while (py > sy) {
					var alpha = data[(py - 1) * 4 + 3];
					if (alpha === 0) {
						ey = py;
					} else {
						sy = py;
					}
					py = (ey + sy) >> 1;
				}
				var ratio = (py / ih);
				return (ratio===0)?1:ratio;
			}

			function transformCoordinate(canvas, width, height, orientation) {
				if (orientation > 4) {
					canvas.width = height;
					canvas.height = width;
				} else {
					canvas.width = width;
					canvas.height = height;
				}
				var ctx = canvas.getContext('2d');
				switch (orientation) {
					case 2:
						// horizontal flip
						ctx.translate(width, 0);
						ctx.scale(-1, 1);
						break;
					case 3:
						// 180 rotate left
						ctx.translate(width, height);
						ctx.rotate(Math.PI);
						break;
					case 4:
						// vertical flip
						ctx.translate(0, height);
						ctx.scale(1, -1);
						break;
					case 5:
						// vertical flip + 90 rotate right
						ctx.rotate(0.5 * Math.PI);
						ctx.scale(1, -1);
						break;
					case 6:
						// 90 rotate right
						ctx.rotate(0.5 * Math.PI);
						ctx.translate(0, -height);
						break;
					case 7:
						// horizontal flip + 90 rotate right
						ctx.rotate(0.5 * Math.PI);
						ctx.translate(width, -height);
						ctx.scale(-1, 1);
						break;
					case 8:
						// 90 rotate left
						ctx.rotate(-0.5 * Math.PI);
						ctx.translate(-width, 0);
						break;
					default:
						break;
				}
			}
		};
	};

	/**
	 * my account page tab button
	 */
	$('#my_account_content_tab a').on('click',function() {
		$('#my_account_content_tab li').removeClass('active');
		$(this).parent('li').addClass('active');
		$('.my_account_content').hide();
		$($(this).attr('href')).show();
		return false;
	});

	/**
	 * my account tab activate by default hash
	 */
	var my_account_tab_hashes = ['#my_account_news', '#my_account_like', '#my_account_edit'];
	$.each(my_account_tab_hashes, function(i,v){
		if (v === location.hash) {
			var $findTab = $('#my_account_content_tab li a[href="'+v+'"]').not('.active');
			if ($findTab.length) {
				$('#my_account_content_tab li').removeClass('active');
				$findTab.parent('li').addClass('active');
				$('.my_account_content').hide();
				$(v).show();
			}
			return false;
		}
	});

	/**
	 * お気に入りクリック
	 */
	$('#recipe_main_content .like_button').on('click', function(){
		var $this = $(this);
		var post_id = $this.attr('data-post-id');
		if (!post_id) return false;
		if ($this.hasClass('is-ajaxing')) return false;

		$this.addClass('is-ajaxing').removeClass('clicked');

		// ajaxでお気に入り追加・削除
		$.ajax({
			url: TCD_MEMBERSHIP.ajax_url,
			type: 'POST',
			data: {
				action: 'toggle_like',
				post_id: post_id
			},
			success: function(data, textStatus, XMLHttpRequest) {
				$this.removeClass('is-ajaxing');
				if (data.result == 'added' || data.result == 'removed') {
					$this.addClass('clicked');
					$this.find('.like_label').text(data.like_label);
					setTimeout(function(){
						$this.removeClass('clicked');
						$this.find('.like_message').text(data.like_message);
					}, 3000);
				} else if (data.message) {
					alert(data.message);
				} else {
					alert(TCD_MEMBERSHIP.ajax_error_message);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$this.removeClass('is-ajaxing');
				alert(TCD_MEMBERSHIP.ajax_error_message);
			}
		});

		return false;
	});

	/**
	 * マイページからのお気に入り削除
	 */
	$('.remove-like').on('click', function(){
		var $this = $(this);
		var post_id = $this.attr('data-post-id');
		if (!post_id) return false;
		if ($this.hasClass('is-ajaxing')) return false;

		$this.addClass('is-ajaxing');

		// ajaxでお気に入り追加・削除
		$.ajax({
			url: TCD_MEMBERSHIP.ajax_url,
			type: 'POST',
			data: {
				action: 'remove_like',
				post_id: post_id
			},
			success: function(data, textStatus, XMLHttpRequest) {
				$this.removeClass('is-ajaxing');
				if (data.result == 'removed') {
					$this.closest('.item').fadeOut(800, function(){
						$(this).remove();
					});
				} else if (data.message) {
					alert(data.message);
				} else {
					alert(TCD_MEMBERSHIP.ajax_error_message);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$this.removeClass('is-ajaxing');
				alert(TCD_MEMBERSHIP.ajax_error_message);
			}
		});

		return false;
	});

	/**
	 * モーダルアラート表示
	 */
	var showModalMessage = function(msg) {
		if (!msg) return false;

		var $modalOverlay = $('#modal_overlay');
		if (!$modalOverlay.length) {
			$modalOverlay = $('<div id="modal_overlay"></div>').appendTo('body');
		}

		var $modalMessage = $('#modal_overlay #message_modal_wrap');
		if (!$modalMessage.length) {
			var html = '<div class="modal_wrap" id="message_modal_wrap">';
			html += '<div class="modal_contents"></div>';
			html += '<a class="close_modal_button" href="#">CLOSE</a>';
			html += '</div>';
			$modalMessage = $(html).appendTo($modalOverlay);
		}

		$modalMessage.find('.modal_contents').html(msg);
		$modalMessage.addClass('show');
		$('#login_modal_wrap').addClass('hide');
		$('#password_modal_wrap').removeClass('show');
		$('#register_modal_wrap').removeClass('show');
		$('#modal_overlay').addClass('open');
	};

	// モーダルアラート自動表示
	if (TCD_MEMBERSHIP.auto_modal_message) {
		showModalMessage(TCD_MEMBERSHIP.auto_modal_message);

		// urlの「?message=???」を削除して差し替え
		if (location.href.indexOf('message=') > -1) {
			var repUrl = location.href;
			repUrl = repUrl.replace(/&?message=[^&]*&?/, '&');
			repUrl = repUrl.replace(/\?&+/,'?').replace(/&+$/,'').replace(/\?$/,'');

			window.history.replaceState(null, null, repUrl);
		}
	}

});

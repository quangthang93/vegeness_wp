<?php
get_header();
?>

<div id="main_contents" class="clearfix">

    <div id="main_col">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <h2 id="page_title"><?php the_title(); ?></h2>

                    <div class="post_content clearfix">
                        <?php
                        the_content();

                        $tax = 'shop_area';
                        $areas = get_terms($tax, array("orderby" => "slug", "parent" => 0, 'hide_empty' => false));
                        ?>
                        <?php foreach ($areas as $key => $area) : ?>

                            <?php echo $area->name; ?>$<?php echo $area->slug; ?><br>
                            <?php $localtions = get_terms($tax, array("orderby" => "slug", "parent" => $area->term_id, 'hide_empty' => false,)); ?>
                            <?php if ($localtions) : ?>

                                <?php foreach ($localtions as $key => $localtion) : ?>

                                    <?php $city = get_terms($tax, array("orderby" => "slug", "parent" => $localtion->term_id, 'hide_empty' => false,)); ?>
                                    <?php if ($city) : ?>

                                        <?php foreach ($city as $key2 => $c) : ?>
                                            <?php echo $area->slug; ?> -> <?php echo $localtion->name; ?>$<?php echo $localtion->slug; ?> -> <?php echo $c->name; ?>$<?php echo $c->slug; ?><br>
                                        <?php endforeach; ?>

                                    <?php endif; ?>

                                <?php endforeach; ?>

                            <?php endif; ?>

                        <?php endforeach; ?>
                    </div>

            <?php endwhile;
        endif; ?>

    </div><!-- END #main_col -->

</div><!-- END #main_contents -->

<?php get_footer(); ?>
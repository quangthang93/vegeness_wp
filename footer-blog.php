<section class="blog-more">
    <div class="blog-more--inner">
        <div class="container">
            <div class="blog-more--cate">
                <div class="blog-more--cate-tabs">
                    <h4 class="titleLv5">記事カテゴリー</h4>
                    <ul>
                        <?php
                        $blog_cats = get_terms('blog_category', 'hide_empty=0');
                        foreach ($blog_cats as $blog_cat) {
                            $term_link = get_term_link($blog_cat, 'blog_category');
                            echo '<li><a href="'.$term_link.'">' . $blog_cat->name . '</a></li>';
                        }
                        ?>                        
                    </ul>
                </div>
                <div class="blog-more--cate-cnt">
                    <div class="blog-more--cate-col">
                        <h4 class="titleLv5">最新の記事</h4>
                        <?php echo do_shortcode('[blog-list]')?>                        
                    </div>
                    <div class="blog-more--cate-col">
                        <h4 class="titleLv5">人気の記事</h4>
                        <?php                        
                        $args = array(
                            'post_type' => 'blog',
                            'limit' => 3,
                            'range' => 'last7days',
                            'wpp_start' => '<ul>',
                            'wpp_end' => '</ul>'
                        );
                        

                        wpp_get_mostpopular($args);
                        ?>                        
                    </div>
                </div>
            </div><!-- ./blog-more--cate -->
            <div class="blog-more--search">
                <div class="blog-more--search-col">
                    <?php
                    $blog_tags = get_field('blog_hashtag', 'option');
                    if($blog_tags): ?>
                    <h4 class="titleLv5">人気のタグ</h4>
                    <div>
                        <?php                        
                        foreach ($blog_tags as $blog_tag) {
                            $tag_link = get_term_link($blog_tag, 'blog_category');
                            echo '<span class="tag"><a href="'.$tag_link.'">#' . $blog_tag->name . '</a></span>';
                        }
                        ?>
                    </div>
                    <?php endif;?>
                </div>
                <div class="blog-more--search-col">
                    <h4 class="titleLv5">キーワードから記事を探す</h4>
                    <form class="form-one" action="/blog">
                        <input class="input" type="text" name="key" placeholder="記事をキーワードで探す" required="required" value="<?php echo isset($_GET['key']) ? $_GET['key']:'';?>">
                        <input class="search" type="submit" value="">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section><!-- ./section-blog-more -->
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <title><?php wp_title(''); ?></title>

        <link href="//www.google-analytics.com" rel="dns-prefetch">

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="p:domain_verify" content="b03ae36ed80e7e36fee8ce952eb6980f"/>
        
        <link href="https://fonts.googleapis.com/css2?family=EB+Garamond:wght@400;500;600;700&display=swap" rel="stylesheet">
        <script data-ad-client="ca-pub-5303046129997885" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <?php wp_head(); ?>

    </head>
    <body>
        <div class="wrapper">

            <header class="header">
                <div class="header-inner">                                    
                    <h1 class="header-logo link">
                        <a href="<?php echo home_url(); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/logo.svg" alt="">
                        </a>
                    </h1>
                    <div class="header-content">
                        <div class="header-search pc-only">
                            <form class="form-one header-search--form" action="/recipe">
                                <input class="input" type="text" name="s_name" value="<?php echo isset($_GET['s_name']) ? $_GET['s_name']:'';?>" placeholder="レシピをキーワードで探す">
                                <input class="search" type="submit" value="">
                            </form>
                        </div> <!-- ./header-search -->
                        <nav class="menu js-menu">
                            <ul class="menu-list">
                                <li class="menu-item test vertical-txt">
                                    <a class="pc-only" href="/recipe">レシピ一覧</a>
                                    <a class="sp-only" href="/recipe">レシピ</a>
                                </li>
                                <li class="menu-item vertical-txt">
                                    <a class="pc-only pen" href="/blog">ヴィーガンコラム</a>
                                    <a class="sp-only pen" href="/blog">コラム</a>
                                </li>
                                <li class="menu-item vertical-txt">
                                    <a class="pc-only fork" href="/shop_search">レストラン検索</a>
                                    <a class="sp-only fork" href="/shop_search">レストラン</a>
                                </li>
                                <li class="menu-item vertical-txt">
                                    <a class="pc-only lock js-itemMsgLink" href="<?php echo is_user_logged_in() ? '/mypage/' : '/login/'?>">クリップ</a>
                                    <a class="sp-only lock js-itemMsgLink" href="<?php echo is_user_logged_in() ? '/mypage/' : '/login/'?>">クリップ</a>
                                    <div class="menu-item--msg ani-pulse"><p class="js-menu-itemMsg">ログインしてお気に入りレシピをクリップ！</p></div>
                                </li>
                            </ul>
                        </nav> <!-- ./menu -->
                    </div><!--End .header-content-->
                </div><!--End .header-inner-->                
            </header>
            
            <?php if(is_user_logged_in()):
                $current_user = wp_get_current_user();
                ?>
                <div class="header-loginInfor">
                    <div class="header-loginInfor--name">
                        <span class="desc">ようこそ</span>
                        <span class="desc"><?php echo $current_user->exists() ? $current_user->nickname : '';?>さん</span>
                    </div>
                    <a class="link underline type2" href="<?php echo get_permalink(282)?>">登録内容の変更</a>
                    <a class="link underline" href="<?php echo wp_logout_url( home_url() ); ?>">ログアウト</a>
                </div>
            <?php endif;?>
            <div class="header-searchSP sp-only">
                <?php
                $menus = nk_type_menus();
                $ingredients = nk_type_ingredients();
                ?>
                <div class="header-search ">
                    <form class="form-one header-search--form" action="/recipe">
                        <input class="input" type="text" name="s_name" value="<?php echo isset($_GET['s_name']) ? $_GET['s_name']:'';?>" placeholder="レシピをキーワードで探す">
                        <input class="search" type="submit" value="">
                    </form>
                </div> <!-- ./header-search -->
                <div class="form-suggest">
                    <div>
                        <?php
                        while( have_rows('recipe_keys', 'option') ) : the_row();
                            printf("<a href=\"/recipe?s_name=%s\" class=\"tag\">%s</a>", get_sub_field('kname', 'option'), get_sub_field('kname', 'option'));
                        endwhile;
                        ?>
                    </div>
                </div>
                <div class="form-searchWrap">
                    <form class="form-search" action="/recipe">
                        <div class="form-search--inner">
                            <select class="input" id="select01" name="genre">
                                <option value="">ジャンル</option>
                                <?php
                                $recipe_cats = get_terms('recipe_category', 'hide_empty=0');
                                if ( ! empty( $recipe_cats ) && ! is_wp_error( $recipe_cats ) ){
                                    foreach ($recipe_cats as $recipe_cat) {
                                        printf("<option value=\"%s\" %s>%s</option>", $recipe_cat->name, $_GET['genre'] == $recipe_cat->name && $_REQUEST['keyclear'] != $recipe_cat->name ? 'selected="true"': '', $recipe_cat->name);
                                    }
                                }
                                ?>
                            </select>
                            <select class="input" id="select02" name="course">
                                <option value="-1">メニュー</option>
                                <?php
                                foreach ($menus as $k => $v) {
                                    if ((($v['skip'] ?? false) === true) || $k === 'all')
                                        continue;
                                    if ($v['n'] != '') {
                                        printf("<option value=\"%s\" %s>%s</option>", $k, $_GET['course'] == $k && $_REQUEST['keyclear'] != $v['n'] ? 'selected="true"' : '', $v['n']);
                                    }
                                }?>
                            </select>
                            <select class="input" id="select03" name="time">
                                <option value="-1">調理時間</option>
                                <?php
                                global $cooking_times;
                                foreach($cooking_times as $time_k => $cooking_time)
                                    printf("<option value=\"%s\" %s>%s</option>", $time_k, $_GET['time'] == $time_k && $_REQUEST['keyclear'] != $time_k ? 'selected="true"': '', $cooking_time);
                                ?>                    
                            </select>
                            <select class="input" id="select04" name="season">
                                <option value="">季節</option>
                                <?php
                                $seasions = ['春', '夏', '秋', '冬'];
                                foreach($seasions as $seasion)
                                    printf("<option value=\"%s\" %s>%s</option>", $seasion, $_GET['season'] == $seasion && !in_array($_REQUEST['keyclear'], $seasions) ? 'selected="true"': '', $seasion);
                                ?>
                            </select>
                        </div>
                        <div class="form-action">
                            <input class="btn-gray" type="submit" value="検索">
                            <input class="btn-gray sliver" type="reset" value="リセット">
                        </div>
                    </form>
                </div>
            </div><!-- ./form-searchSP -->

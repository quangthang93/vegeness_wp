<?php get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
				<?php wp_breadcrumb() ?>
            </div>
        </div>
    </div>
    <section class="section recipe blog">
        <div class="container">
            <div class="section-recipe--left fadeup2">
                <div class="sectionEP-head">
                    <div class="sectionEP-titleWrap">
                        <h1 class="sectionEP-title no-icon">お知らせ</h1>
                    </div>
                </div>
                <ul class="news-list">
					<?php
					$args[ 'post_type' ]		 = 'news';
					$args[ 'posts_per_page' ]	 = 16;
					$args[ 'post_status' ]		 = 'publish';
					$args[ 'orderby' ]			 = 'date';
					$args[ 'order' ]			 = 'DESC';
					$paged						 = (get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
					$args[ 'paged' ]			 = $paged;
					$args[ 'meta_query' ]		 = array(
						array(
							'key'		 => 'news_type',
							'value'		 => array( 'member' ),
							'compare'	 => 'NOT IN',
						)
					);

					$news_query	 = null;
					$news_query	 = new WP_Query( $args );

					if ( $news_query->have_posts() ):
						while ( $news_query->have_posts() ): $news_query->the_post();
							switch ( get_field( 'news_type' ) ) {
								case 'file':
									?>

									<li class="news-item">
										<a href="<?php the_field( 'news_file' ); ?>" class="link" target="_blank">
											<p class="date big"><?php the_time( 'Y.m.d' ); ?> [<?php echo strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) ?>]</p>
											<div class="news-item--ttl">
												<span class="link-icon <?php echo get_file_type( get_field( 'news_file' ) ) ?>">
													<?php the_title() ?>
												</span>
											</div>
										</a>
									</li>
									<?php
									break;
								case 'blank':
									?>

									<li class="news-item">
										<a href="<?php the_field( 'news_blank' ); ?>" class="link" target="_blank">
											<p class="date big"><?php the_time( 'Y.m.d' ); ?> [<?php echo strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) ?>]</p>
											<div class="news-item--ttl">
												<span class="link-icon blank">
													<?php the_title() ?>
												</span>
											</div>
										</a>
									</li>

									<?php
									break;

								default:
									?>
									<li class="news-item">
										<a href="<?php the_permalink(); ?>" class="link">
											<p class="date big"><?php the_time( 'Y.m.d' ); ?> [<?php echo strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) ?>]</p>
											<div class="news-item--ttl"><?php the_title() ?></div>
										</a>
									</li>
									<?php
									break;
							}
						endwhile;
						wp_reset_postdata();
					endif;
					?>
                </ul><!--End .news-list-->
                <div class="pagination">
					<?php echo pagination( $news_query, 4, 12 ) ?>
                </div>
            </div><!-- ./section-recipe--left -->
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
					<?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section><!--End .blog-->
	<?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>


<?php get_footer(); ?>

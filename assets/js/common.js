$( function () {

    /* --------------------
     GLOBAL VARIABLE
     --------------------- */
    // Selector
    var $ctrlToggle = $( '.js-ctrlToggle' ),
	$cboxList = $( '.js-cboxList' ),
	$menuItemMsg = $( '.js-menu-itemMsg' ),
	$menuItemMsgWrap = $menuItemMsg.parent(),
	$itemMsgLink = $( '.js-itemMsgLink' ),
	$restSlider = $( '.js-rest-slider' ),
	$restSliderNav = $( '.js-rest-slider-nav' ),
	$ctrlToggleMenuRtoc = $( '.js-rtoc_open' ),
	$menuRtoc = $( '.js-rtoc-menu' ),
	$modal = $( '.js-modal' ),
	$modalWrapper = $modal.parent(),
	$modalOverlay = $( '.js-modal-overlay' ),
	$modalClose = $( '.js-modal-close' ),
	$btnAlert = $( '.js-btn-alert' ),
	$btnClipDel = $( '.js-btn-clip2' ),
	$modalDelMsg = $( '.js-modal-del-msg' ),
	$modalDelMsgCnt = $( '.js-modal-del-cnt' ),
	$cboxState = $( '.js-cbox-state' ),
	$cboxStateFor = $( '.js-cbox-state-for' );

    // Init Value
    var breakpoint = 1050,
	wWindow = $( window ).outerWidth();

    /* --------------------
     FUNCTION COMMON
     --------------------- */
    // Setting anchor link
    var anchorLink = function () {
	// Scroll to section
	$( 'a[href^="#"]' ).not( "a[class*='carousel-control-']" ).click( function () {
	    var href = $( this ).attr( "href" );
	    var hash = href == "#" || href == "" ? 'html' : href;
	    var position = $( hash ).offset().top - 100;
	    $( 'body,html' ).stop().animate( { scrollTop: position }, 1000 );
	    return false;
	} );
    }

    // Trigger checkboxList
    var triggerCboxList = function () {
	if ( wWindow <= breakpoint ) {
	    $ctrlToggle.click( function () {
		var ctrlHtml = $( this ).html();
		ctrlHtml = ( ctrlHtml === "閉じる" ) ? "開く" : "閉じる";
		$( this ).html( ctrlHtml ).parent().siblings( $cboxList ).slideToggle();
	    } );
	}
    }


    // Set status selected option, and input text
    var setStatusInput = function () {
	$( 'select' ).each( function () {
	    var defaultVal = $( this ).find( "option:first-child" ).val();
	    var selectedVal = $( this ).find( ":selected" ).text();
	    if ( defaultVal === selectedVal ) {
		$( this ).removeClass( 'active' );
	    } else {
		$( this ).addClass( 'active' );
	    }
	} );

	$( 'input' ).each( function () {
	    if ( $( this ).val() != "" ) {
		$( this ).addClass( 'active' );
	    } else {
		$( this ).removeClass( 'active' );
	    }
	} );
    }

    // Change status select option
    var changeStatusSelect = function () {
	$( 'select' ).on( 'change', function ( e ) {
	    var defaultVal = $( this ).find( "option:first-child" ).val();
	    var selectedVal = $( this ).find( ":selected" ).text();
	    if ( defaultVal === selectedVal ) {
		$( this ).removeClass( 'active' );
	    } else {
		$( this ).addClass( 'active' );
	    }
	} );
    }

    // Change status input
    var changeStatusInput = function () {
	$( 'input' ).on( 'change', function ( e ) {
	    if ( $( this ).val() != "" ) {
		$( this ).addClass( 'active' );
	    } else {
		$( this ).removeClass( 'active' );
	    }
	} );
    }

    // Animation
    var triggerMenuItemMsg = function () {
	$( document ).click( function ( e ) {
	    if ( !$( e.target ).is( $menuItemMsg ) ) {
		$menuItemMsgWrap.hide();
	    }
	} );

	$itemMsgLink.mouseenter( function () {
	    $menuItemMsgWrap.show();
	} );
    }

    var triggerMenuItemMsgSP = function ( scroll ) {
	if ( wWindow <= breakpoint ) {
	    if ( scroll > 50 ) {
		$menuItemMsgWrap.show();
	    } else {
		$menuItemMsgWrap.hide();
	    }
	}
    }


    /* --------------------
     RESTAURANT
     --------------------- */
    // Tabs controls (Restaurant detail page)
    var tabsCtrl = function () {
	$( 'ul.tabs li' ).click( function () {
	    var tab_id = $( this ).attr( 'data-tab' );

	    $( 'ul.tabs li' ).removeClass( 'current' );
	    $( '.tab-content' ).removeClass( 'current' );

	    $( this ).addClass( 'current' );
	    $( "#" + tab_id ).addClass( 'current' );
	} )
    }

    // Slider (Restaurant detail page)
    var initSliderRest = function () {
	if ( $restSlider.length ) {
	    // Tabs slider
	    $restSlider.slick( {
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		swipeToSlide: true,
		// fade: true,
		// asNavFor: '.js-rest-slider-nav'
		prevArrow: '<a class="slick-prev" href="#"><span></span></a>',
		nextArrow: '<a class="slick-next" href="#"><span></span></a>',
	    } );
	}

	if ( $restSlider.length && $restSliderNav.length ) {
	    $restSliderNav.slick( {
		slidesToShow: 6,
		slidesToScroll: 1,
		asNavFor: '.js-rest-slider',
		arrows: false,
		dots: false,
		// centerMode: true,
		focusOnSelect: true,
		responsive: [ {
			breakpoint: 768,
			settings: {
			    slidesToShow: 4
			}
		    } ]
	    } );
	}
    }

    // Trigger menu content
    var triggerMenuCnt = function () {
	$ctrlToggleMenuRtoc.click( function () {
	    var ctrlHtml = $( this ).html();
	    ctrlHtml = ( ctrlHtml === "閉じる" ) ? "開く" : "閉じる";
	    $( this ).toggleClass( 'active' ).html( ctrlHtml ).parent().siblings( $menuRtoc ).slideToggle();
	} );
    }

    /* --------------------
     MY PAGE  (CLIP)
     --------------------- */
    var triggerModalAdv = function () {
	$modalClose.click( function () {
	    $modalOverlay.addClass( 'hide' );
	    $modalWrapper.addClass( 'hide' );
	} );
    }

    var deleteClip = function () {
	$btnClipDel.click( function () {
	    $( this ).parent().hide();
	    $modalDelMsg.addClass( 'show' );
	    $modalDelMsgCnt.addClass( 'active' );
	    setTimeout( function () {
		$modalDelMsgCnt.removeClass( 'active' );
		setTimeout( function () {
		    $modalDelMsg.removeClass( 'show' );
		}, 1000 );
	    }, 1000 );
	} );
    }

    // Filter controls
    var mypageFilter = function () {
	$( '.js-tabsList2 a' ).click( function ( e ) {
	    e.preventDefault();
	    $( this ).addClass( 'active' ).siblings().removeClass( 'active' );
	    var $tabType = $( this ).attr( 'data-type' );
	    if ( $tabType === "type00" ) {
		$( '.js-clipList li' ).show();
	    } else {
		$( '.js-clipList li' ).hide();
		$( '.' + $tabType ).parent().parent().parent().show();
	    }
	} )
    }

    // LOGIN
    var changeStatusCbox = function () {
	$cboxState.change( function ( e ) {
	    $cboxStateFor.toggleClass( 'disabled' );
	} );
    }

    // Edit profile
    var alertMsg = function () {
	$btnAlert.click( function () {
	    $modalDelMsg.addClass( 'show' );
	    $modalDelMsgCnt.addClass( 'active' );
	    setTimeout( function () {
		$modalDelMsgCnt.removeClass( 'active' );
		setTimeout( function () {
		    $modalDelMsg.removeClass( 'show' );
		}, 1000 );
	    }, 1000 );
	} );
    }


    /* --------------------
     RUN INIT
     --------------------- */
    var init = function () {
	anchorLink();
	triggerCboxList();
	triggerMenuItemMsg();
	changeStatusSelect();
	changeStatusInput();
	setStatusInput();
	tabsCtrl();
	initSliderRest();
	triggerMenuCnt();
	triggerModalAdv();
	deleteClip();
	alertMsg();
	mypageFilter();
//    changeStatusCbox();
    }
    // Run all script when DOM has loaded.
    init();

    /* --------------------
     WINDOW ON RESIZE
     --------------------- */
    $( window ).resize( function () {
	wWindow = $( window ).outerWidth();
	triggerMenuItemMsg();
    } );


    /* --------------------
     WINDOW ON SCROLL
     --------------------- */
    $( window ).scroll( function () {
	var scroll = $( this ).scrollTop();
	// Scroll slide up out
	$( '.fadeup' ).each( function () {
	    var elemPos = $( this ).offset().top;
	    var windowHeight = $( window ).height();
	    if ( scroll > elemPos - windowHeight + 100 ) {
		$( this ).addClass( 'in' );
	    }
	} );

	triggerMenuItemMsgSP( scroll );
    } );


} );
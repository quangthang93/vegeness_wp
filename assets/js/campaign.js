$( function () {
    var date = new Date();
    date.setTime( date.getTime() + ( 12 * 60 * 60 * 1000 ) ); //12h

    // Selector
    var $ctrlToggle = $( '.js-ctrlToggle' ),
	$cboxList = $( '.js-cboxList' ),
	$menuItemMsg = $( '.js-menu-itemMsg' ),
	$menuItemMsgWrap = $menuItemMsg.parent(),
	$itemMsgLink = $( '.js-itemMsgLink' ),
	$restSlider = $( '.js-rest-slider' ),
	$restSliderNav = $( '.js-rest-slider-nav' ),
	$ctrlToggleMenuRtoc = $( '.js-rtoc_open' ),
	$menuRtoc = $( '.js-rtoc-menu' ),
	$modal = $( '.js-modal' ),
	$modalWrapper = $modal.parent(),
	$modalOverlay = $( '.js-modal-overlay' ),
	$modalClose = $( '.js-modal-close' ),
	$btnAlert = $( '.js-btn-alert' ),
	$btnClipDel = $( '.js-btn-clip2' ),
	$modalDelMsg = $( '.js-modal-del-msg' ),
	$modalDelMsgCnt = $( '.js-modal-del-cnt' ),
	$cboxState = $( '.js-cbox-state' ),
	$cboxStateFor = $( '.js-cbox-state-for' );


    if ( $.cookie( 'campain_time' ) !== 'yes' ) {
	$modalOverlay.removeClass( 'hide' );
	$modalWrapper.removeClass( 'hide' );
	$.cookie( "campain_time", "yes", { expires: date } );
    }
} );

<?php get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
				<?php wp_breadcrumb() ?>
            </div>
        </div>
    </div>
    <section class="section recipe end">
        <div class="container">
			<?php
			if ( have_posts() ): while ( have_posts() ) : the_post();
					?>
					<div class="section-recipe--left fadeup2">

						<div class="sectionEP-head">
							<div class="sectionEP-titleWrap type2">
								<h1 class="sectionEP-title type2"><?php the_title() ?></h1>
							</div>
							<div class="sectionEP-titleInfor mgt-10">
								<span class="desc3"><?php the_time( 'Y.m.d' ); ?> [<?php echo strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) ?>]</span>
							</div>
						</div>
						<div class="news-cnt">
							<div class="main-content no-reset">
								<?php the_content(); ?>
							</div>
						</div><!-- ./news-cnt -->
						<div class="btn-view-moreWrap align-center mgt-70">
							<a href="/news" class="btn-view-more">お知らせ一覧へ</a>
						</div>
					</div><!-- ./section-recipe--left -->
				<?php endwhile; ?>
			<?php endif; ?>
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
					<?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section><!--End .blog-->
	<?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>

<?php get_footer(); ?>

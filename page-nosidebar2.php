<?php /* Template Name: サイドバーなし2 */ get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
                <?php wp_breadcrumb()?>
            </div>
        </div>
    </div><!--End .breadcrumbWrap-->
    <section class="section">
        <div class="container">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <div class="main-content">
                    <?php the_content(); ?>            
                </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </section>
</main>

<?php get_footer(); ?>

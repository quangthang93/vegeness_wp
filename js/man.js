jQuery(function ($) {

    if($('#createuser [name="last_name"]').length > 0){
        $('#createuser [name="last_name"]').attr('placeholder', '全角10文字以内');
    }
    if($('#createuser [name="user_email"]').length > 0){
        $('#createuser [name="user_email"]').attr('placeholder', '例）members@vegeness.com');
    }
    if($('#createuser [name="pass1"]').length > 0){
        $('#createuser [name="pass1"]').attr('placeholder', '半角英数字6文字以上');
    }

});
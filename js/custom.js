jQuery(function ($) {
    $('.js-tabsList a').off('click').on('click', function (e) {
        e.preventDefault();
        $(this).parent().children().removeClass("active");
        $(this).addClass("active");
        $.ajax({
            type: 'POST',
            url: urlPage.ajaxurl,
            data: {
                action: 'blog_ajax',
                id: $(this).attr("data-attr"),
            },
            beforeSend: function () {
                $('.top-blog').addClass("loading");
            },
            success: function (data) {
                $('.loading').removeClass('loading');
                $(".section-column--list").html(data);
            }
        });

    });
    
    /**
    * お気に入りクリック
    */
    $('.btn-like').on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        var post_id = $this.attr('data-post-id');
        if (!post_id) return false;
        if ($this.hasClass('is-ajaxing')) return false;
        $this.addClass('is-ajaxing').removeClass('clicked');
        
        $.ajax({
            url: urlPage.ajaxurl,
            type: 'POST',
            data: {
                action: 'recipe_like',
                post_id: post_id
            },
            success: function (data, textStatus, XMLHttpRequest) {
                $this.removeClass('is-ajaxing');
                if (data.result === 'added' || data.result === 'removed' || data.result === 'maximum') {
                    $this.addClass('clicked');
                    $this.find('span').text(data.like_label);
                    $('.btn-like').find('span').text(data.like_label);
                    setTimeout(function () {
                        $this.removeClass('clicked');
                        $this.closest('.recipe').find('.like_message').html(data.like_message);
                        $('.js-modal-del-msg').addClass('show');
                        $('.js-modal-del-cnt').addClass('active');
                        setTimeout(function() {
                          $('.js-modal-del-cnt').removeClass('active');
                          setTimeout(function() {
                            $('.js-modal-del-msg').removeClass('show');
                          }, 1000);
                        }, 1000);
                    }, 1000);
                } else if (data.message) {
                    alert(data.message);
                } else {
                    alert(urlPage.ajax_error_message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $this.removeClass('is-ajaxing');
                alert(urlPage.ajax_error_message);
            }
        });

    });
    
    /**
    * マイページからのお気に入り削除
    */
    $('.remove-like').on('click', function () {
        var $this = $(this);
        var post_id = $this.attr('data-post-id');
        if (!post_id)
            return false;
        if ($this.hasClass('is-ajaxing'))
            return false;

        $this.addClass('is-ajaxing');

        // ajaxでお気に入り追加・削除
        $.ajax({
            url: urlPage.ajaxurl,
            type: 'POST',
            data: {
                action: 'remove_like',
                post_id: post_id
            },
            success: function (data, textStatus, XMLHttpRequest) {
                console.log(data);
                $this.removeClass('is-ajaxing');
                if (data.result === 'removed') {
                    $this.closest('li').fadeOut(800, function () {
                        $(this).remove();                        
                    });
                    $this.closest('.recipe').find('.like_message').html(data.like_message);
                    $('.js-modal-del-msg').addClass('show');
                    $('.js-modal-del-cnt').addClass('active');
                    setTimeout(function() {
                      $('.js-modal-del-cnt').removeClass('active');
                      setTimeout(function() {
                        $('.js-modal-del-msg').removeClass('show');
                      }, 1000);
                    }, 1000);
                } else if (data.message) {
                    alert(data.message);
                } else {
                    alert(urlPage.ajax_error_message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $this.removeClass('is-ajaxing');
                alert(urlPage.ajax_error_message);
            }
        });

        return false;
    });
    
    $("input[type=reset]").click(function () {
        return $('select.input option:selected').removeAttr('selected');
    });
    
    $(".recipe-list span.tagX").click(function(){
        var keywork = $(this).next('input').val();
        $("input[name='keyclear']").val(keywork);
        $("#frmRecipe").submit();
    });
});
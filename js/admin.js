jQuery(function ($) {
    $('.categorychecklist').find('input').each(function (index, input) {
        $(input).bind('change', function () {
            var checkbox = $(this);
            var is_checked = $(checkbox).is(':checked');
            if (is_checked) {
                $(checkbox).parents('li').children('label').children('input').attr('checked', 'checked');
            } else {
                $(checkbox).parentsUntil('ul').find('input').removeAttr('checked');
            }
        });
    });
});
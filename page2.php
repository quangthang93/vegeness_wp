<?php /* Template Name: テンプレート２ */ get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
				<?php wp_breadcrumb() ?>
            </div>
        </div>
    </div><!--End .breadcrumbWrap-->
    <section class="section recipe rest">
        <div class="container">
            <div class="section-recipe--left fadeup2">
                <div class="sectionEP-head">
                    <div class="sectionEP-titleWrap">
                        <h1 class="sectionEP-title no-icon"><?php the_title() ?></h1>
                    </div>
                </div>
				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
						<div class="main-content">
							<?php the_content(); ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
            </div><!-- ./section-recipe--left -->
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
					<?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>

        </div>
		<div class="section-news--advers fadeup container">
			<?php get_template_part( 'template-parts/fronts/advertising' ); ?>
		</div>
    </section>

</main>

<?php get_footer(); ?>

<?php get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
                <?php wp_breadcrumb() ?>
            </div>
        </div>
    </div>
    <section class="section recipe rest">
        <div class="container">
            <?php 
            if (have_posts()): while (have_posts()) : the_post(); 
                $shop_areas =  get_the_terms(get_the_ID(), 'shop_area');
                $shop_icons =  get_the_terms(get_the_ID(), 'shop_icon');
                $shop_genres =  get_the_terms(get_the_ID(), 'shop_genre');
                ?>
                    <div class="section-recipe--left fadeup2">
                        <div class="p-restaurant rest-detail">
                            <div class="sectionEP-head">                                
                                <div class="sectionEP-titleWrap">
                                    <h1 class="sectionEP-title no-icon"><?php the_title()?></h1>
                                </div>                                
                            </div>
                            <div class="section-recipe--row">
                                <ul class="tabs">
                                    <li class="tab-link current" data-tab="tab-1"><span>店舗詳細</span></li>
                                    <?php if(!empty( get_the_content())):?>
                                        <li class="tab-link" data-tab="tab-2"><span>レポート記事を読む</span></li>
                                    <?php endif;?>
                                </ul>
                                
                                <div class="tab-contentWrap">
                                    <div id="tab-1" class="tab-content current">
                                        <h3 class="tab-content--ttl titleLv4 sp-only"><?php the_title()?></h3>
                                        <?php
                                        $shop_gallery = get_field('store_gallery');                                        
                                        if ($shop_gallery):
                                            ?>
                                            <ul class="rest-slider js-rest-slider">
                                                <?php foreach ($shop_gallery as $shop_img): ?>
                                                    <li><?php echo wp_get_attachment_image($shop_img, 'large') ?></li>
                                                <?php endforeach; ?>
                                            </ul>
                                            <ul class="rest-slider-nav js-rest-slider-nav">
                                                <?php foreach ($shop_gallery as $shop_img): ?>
                                                    <li><?php echo wp_get_attachment_image($shop_img, 'thumbnail') ?></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                        
                                        <div class="rest-detail--tags">
                                            <?php if ( $shop_icons && ! is_wp_error( $shop_icons ) ) :
                                                foreach ( $shop_icons as $shop_icon ): ?>
                                                    <span class="tag2"><?php echo $shop_icon->name?></span>
                                                <?php 
                                                endforeach; 
                                            endif;?>                                            
                                        </div><!--End .rest-detail--tags-->
                                        
                                        <div class="rest-detail--table">
                                            <table class="desc">
                                                <tr>
                                                    <th>エリア</th>
                                                    <td>
                                                        <?php if ( $shop_areas && ! is_wp_error( $shop_areas ) ) :
                                                            $shop_area = current($shop_areas);
                                                            echo $shop_area->name;
                                                        endif;?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>ジャンル</th>
                                                    <td>
                                                        <?php if ( $shop_genres && ! is_wp_error( $shop_genres ) ) :
                                                            foreach ( $shop_genres as $shop_genre ): ?>
                                                                <?php echo $shop_genre->name?>
                                                            <?php
                                                            endforeach; 
                                                        endif;?>
                                                    </td>
                                                </tr>
                                                <?php if(get_field('store_address')):?>
                                                    <tr>
                                                        <th>住所</th>
                                                        <td><?php the_field('store_address')?><br>
                                                            <?php echo getGoogleEmbed(get_field('store_address'))?>                                                        
                                                        </td>
                                                    </tr>
                                                <?php endif;?>
                                                <?php if(get_field('store_access')):?>
                                                <tr>
                                                    <th>アクセス</th>
                                                    <td><?php the_field('store_access')?></td>
                                                </tr>
                                                <?php endif;?>
                                                <?php if(get_field('store_marketing')):?>
                                                <tr>
                                                    <th>営業時間</th>
                                                    <td><?php echo nl2br(get_field('store_marketing'))?></td>
                                                </tr>
                                                <?php endif;?>
                                                <?php if(get_field('store_holiday')):?>
                                                <tr>
                                                    <th>定休日</th>
                                                    <td><?php the_field('store_holiday')?></td>
                                                </tr>
                                                <?php endif;?>
                                                <?php if(get_field('store_tel')):?>
                                                <tr>
                                                    <th>電話番号</th>
                                                    <td><a href="tel:<?php the_field('store_tel')?>"><?php the_field('store_tel')?></a></td>
                                                </tr>
                                                <?php endif;?>                                                
                                                <tr>
                                                    <th>平均予算</th>
                                                    <td>
                                                        <?php
                                                        if(get_field('store_average')) {
                                                            echo nl2br(get_field('store_average'));
                                                        } else {
                                                            echo get_field('price_d') ? 'ランチ : '.get_field('price_d') : '';
                                                            echo get_field('price_n') ? 'ディナー : '.get_field('price_n') : '';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php if(get_field('store_paid')):?>
                                                <tr>
                                                    <th>支払方法</th>
                                                    <td><?php the_field('store_paid')?></td>
                                                </tr>
                                                <?php endif;?>
                                                <?php if(get_field('store_seat')):?>
                                                <tr>
                                                    <th>座席数</th>
                                                    <td><?php the_field('store_seat')?></td>
                                                </tr>
                                                <?php endif;?>
                                                <?php if(get_field('store_parking')):?>
                                                <tr>
                                                    <th>駐車場</th>
                                                    <td><?php the_field('store_parking')?></td>
                                                </tr>
                                                <?php endif;?>
                                                <?php if(get_field('store_url')):?>
                                                <tr>
                                                    <th>URL</th>
                                                    <td><a class="link-icon blank" href="<?php the_field('store_url')?>" target="_blank"><?php the_field('store_url')?></a></td>
                                                </tr>
                                                <?php endif;?>
                                            </table>
                                        </div><!--End .rest-detail--table-->
                                    </div><!--End #tab-1-->
                                    <?php if(!empty( get_the_content())):?>
                                        <div id="tab-2" class="tab-content">
                                            <div class="no-reset">
                                                <div class="desc"><?php the_content()?></div>
                                            </div>
                                        </div><!--End #tab-2-->
                                    <?php endif;?>
                                </div><!--End .tab-contentWrap-->
                                
                                <div class="btn-view-moreWrap">
                                    <a href="/shop" class="btn-view-more">検索結果一覧へ</a>
                                </div>
                                
                                <div class="rest-detail--suggest">
                                    <?php
                                    $area_ids = wp_list_pluck($shop_areas, 'term_id');

                                    $args = array(
                                        'post_type' => 'shop',
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => 'shop_area',
                                                'field' => 'id',
                                                'terms' => $area_ids,
                                                'operator' => 'IN',
                                            ),                                            
                                        ),
                                        'post_status' => 'publish',
                                        'orderby'   => 'rand',
                                        'posts_per_page' => 4,
                                        'post__not_in' => array(get_the_ID())
                                    );
                                    $area_query = null;
                                    $area_query = new WP_Query($args);
                                    if ($area_query->have_posts()) {?>
                                        <h2 class="titleLv2">同じエリアのレストラン</h2>
                                        <ul class="rest-detail--suggest-list">
                                            <?php
                                            while ($area_query->have_posts()) : $area_query->the_post();
                                                $shop_genre =  get_the_terms(get_the_ID(), 'shop_genre');
                                                ?>
                                                <li>
                                                    <a href="<?php the_permalink() ?>" class="link">
                                                        <div class="rest-detail--suggest-thumb">
                                                            <?php if ( has_post_thumbnail() ) : ?>
                                                                <?php the_post_thumbnail('medium'); ?>
                                                            <?php else:?>
                                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-img.jpg" />
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="rest-detail--suggest-tag">
                                                            <span>[<?php echo $shop_genre[0]->name?>]</span>
                                                        </div>
                                                        <h4 class="titleLv5"><?php the_title() ?></h4>
                                                    </a>
                                                </li>
                                                                                                
                                            <?php
                                            endwhile;
                                            wp_reset_query();?>                        
                                        </ul>
                                    <?php }?>
                                        
                                </div><!--Emd .rest-detail--suggest-->
                                                                                 
                            </div><!--End .section-recipe--row-->
                        </div><!--End .p-restaurant-->
                    </div><!-- ./section-recipe--left -->
                    <?php endwhile; ?>
                <?php endif; ?>
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
                    <?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section><!--End .blog-->
    <?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>

<?php get_footer(); ?>

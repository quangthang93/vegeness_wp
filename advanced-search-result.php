<?php get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
                <?php wp_breadcrumb() ?>
            </div>
        </div>
    </div>
    <section class="section recipe end">
        <div class="container">
            <div class="section-recipe--left fadeup2">
                <div class="sectionEP-head">
                    <div class="sectionEP-titleWrap">
                        <h1 class="sectionEP-title">レシピ一覧</h1>
                        <p class="sectionEP-resultSumary"></p>
                    </div>
                    <div class="sectionEP-sort">
                        <div class="sectionEP-newOrders">
                            <div class="form-search">
                                <select class="input" name="sort" onchange="document.location.href=this.options[this.selectedIndex].value;">
                                    <option value="newer">新着順</option>
                                    <option value="<?php echo get_permalink(55733)?>">人気順</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-recipe--row">
                    <div class="section-recipe--pickupList">
            
                    <?php
                    $lists_post_ids = [];
                    $post_ids = [];
                    $lists = [
                      ['えだまめ','枝豆'],
                      ['かぼちゃ','南瓜'],
                      ['きゅうり','胡瓜'],
                      ['こまつな','小松菜'],
                      ['ごぼう','牛蒡'],
                      ['さつまいも','さつま芋','薩摩芋'],
                      ['さといも','さと芋','里芋'],
                      ['しいたけ','椎茸'],
                      ['じゃがいも','じゃが芋'],
                      ['しゅんぎく','春菊'],
                      ['しょうが','生姜'],
                      ['そらまめ','そら豆','空豆'],
                      ['たまねぎ','玉ねぎ','玉葱'],
                      ['だいこん','大根'],
                      ['ちんげんさい','青梗菜','ちんげん菜'],
                      ['とうがらし','唐辛子'],
                      ['とうがん','冬瓜'],
                      ['なす','茄子'],
                      ['にんじん','人参'],
                      ['ねぎ','葱'],
                      ['はくさい','白菜'],
                      ['ほうれんそう','ほうれん草'],
                      ['みょうが','茗荷'],
                      ['みずな','水菜'],
                      ['むらさききゃべつ','紫きゃべつ'],
                      ['むらさきたまねぎ','紫たまねぎ','紫玉葱'],
                      ['やまいも','山芋'],
                      ['れんこん','蓮根'],
                      ['なのはな','菜の花'],
                      ['みつば','三つ葉'],
                      ['めきゃべつ','芽きゃべつ'],
                      ['ながいも','長芋','長いも'],
                      ['わさび','山葵'],
                      ['ゆりね','百合根','ゆり根'],
                      ['きぬさや','絹さや'],
                      ['ひよこまめ','ひよこ豆'],
                      ['たけのこ','筍','竹の子'],
                      ['きのこ','木の子'],
                      ['きくらげ','木耳'],
                      ['みそ','味噌'],
                      ['さとう','砂糖'],
                      ['しょうゆ','醤油'],
                      ['しお','塩'],
                      ['こしょう','胡椒'],
                      ['おりーぶおいる','おりーぶ油'],
                      ['こうじ','麹'],
                      ['まいたけ','舞茸'],
                    ];




                    $_keyword = $_GET['s_name'];

                    $_keyword_array = [];
                    $_keyword  = preg_replace("/( |　)/", ",", $_keyword);
                    $_l_keyword_array = explode(",", $_keyword);


                    foreach ($lists as $k => &$v) {
                      $l = [];
                      foreach ($v as $k2 => $v2) {
                        $l[] = $v2;
                        $lv = mb_convert_kana($v2, "KVC");
                        if ($v2 !== $lv) {
                          $l[] = $lv;
                        }
                      }
                      $v = $l;

                    }
                    unset($v);


                    foreach ($_l_keyword_array as $k => $v) {
                      $lv = trim($v);
                      $lv2 = [];
                      if ($lv != '') {
                        $lv2[] = $lv;
                        $llv_kana = mb_convert_kana($v,"cH");
                        if ($lv !== $llv_kana) {
                          $lv2[] = $llv_kana;
                        }
                        $llv = mb_convert_kana($v,"KVC");
                        if ($lv !== $llv) {
                          $lv2[] = $llv;
                        }

                        foreach ($lists as $k2 => $v2) {
                          if (in_array($llv_kana,$v2,true)) {
                            foreach ($v2 as $k3 => $v3) {
                              if (in_array($v3, $lv2 ,true)) {
                                continue;
                              }
                              $lv2[] = $v3;
                            }
                            break;
                          }
                        }


                        $_keyword_array[] = $lv2;
                      }
                    }

                    if (count($_keyword_array) > 1) {
                      $operator_str = '('.$operator_str.')';
                    }else{
                      $operator_str = '';
                    }

                    global $wpdb;

                    //20200116start
                    $base_sql = "SELECT distinct object_id FROM $wpdb->terms as a INNER JOIN $wpdb->term_taxonomy as b ON `a`.`term_id` = `b`.`term_id` INNER JOIN $wpdb->term_relationships as c ON `a`.`term_id` = `c`.`term_taxonomy_id` INNER JOIN $wpdb->posts as p ON `p`.`ID` = `c`.`object_id` WHERE ( `b`.taxonomy='recipe_category' OR `b`.taxonomy='wprm_course' OR `b`.taxonomy='wprm_cuisine' OR `b`.taxonomy='wprm_ingredient' )";

                    $base_sql .= " AND ";

                    foreach($_keyword_array as $k => $v){

                      if (count($v)===0) {
                        continue;
                      }

                      $sql = $base_sql;

                      $sql .= ' ( ';

                      foreach ($v as $k2 => $v2) {
                        if($k2!=0){
                          $sql .=  ' OR';
                        }

                        $sql .= " a.name LIKE '%".$wpdb->esc_like( $v2 )."%' ";
                      }

                      $sql .= ' ) ';

                      $results = $wpdb->get_results($sql);

                      foreach($results as $r){
                        $l_id = (int)$r->object_id;
                        if(!in_array($l_id, $lists_post_ids[$k]?:[] ,true)){
                          $lists_post_ids[$k][] = $l_id;
                        }
                      }
                    }

                    //20200116start
                    $base_sql = "SELECT post_id FROM $wpdb->postmeta INNER JOIN $wpdb->posts ON `$wpdb->posts`.`ID` = `$wpdb->postmeta`.`post_id` WHERE (meta_key = 'recipe_desc' OR meta_key = 'wprm_ingredients' OR meta_key = 'wprm_author_name' )  AND ";
                    foreach($_keyword_array as $k => $v){
                      if (count($v)===0) {
                        continue;
                      }

                      $sql = $base_sql;
                      $sql .= ' ( ';

                      foreach ($v as $k2 => $v2) {
                        if($k2!=0){
                          $sql .=  ' OR';
                        }
                        $sql .= " meta_value LIKE '%".$wpdb->esc_like( $v2 )."%' ";
                      }

                      $sql .= ' ) ';

                      $results = $wpdb->get_results($sql);

                      foreach($results as $r){
                        $l_id = (int)$r->post_id;
                        if(!in_array($l_id, $lists_post_ids[$k]?:[] ,true)){
                          $lists_post_ids[$k][] = $l_id;
                        }
                      }
                    }

                    //20200116start
                    $base_sql = "SELECT ID FROM $wpdb->posts WHERE `$wpdb->posts`.`post_type` = 'wprm_recipe' AND ";
                    foreach($_keyword_array as $k => $v){
                      if (count($v)===0) {
                        continue;
                      }

                      $sql = $base_sql;
                      $sql .= ' ( ';

                      foreach ($v as $k2 => $v2) {
                        if($k2!=0){
                          $sql .=  ' OR';
                        }
                        $sql .= " post_title LIKE '%".$wpdb->esc_like( $v2 )."%' ";
                        $sql .= " OR post_content LIKE '%".$wpdb->esc_like( $v2 )."%' ";
                      }

                      $sql .= ' ) ';


                      $results = $wpdb->get_results($sql);

                      foreach($results as $r){
                        $l_id = (int)$r->ID;
                        if(!in_array($l_id, $lists_post_ids[$k]?:[] ,true)){
                          $lists_post_ids[$k][] = $l_id;
                        }
                      }

                    }

                    foreach($_keyword_array as $k => $v){
                      if (count($v)===0) {
                        continue;
                      }
                      $l = $lists_post_ids[$k]?:[];
                      if (count($l) > 0) {
                        $results = $wpdb->get_results("SELECT meta_value FROM $wpdb->postmeta INNER JOIN $wpdb->posts ON `$wpdb->posts`.`ID` = `$wpdb->postmeta`.`meta_value` WHERE post_id IN (". join(',', $l) .") AND meta_key='wprm_parent_post_id'");
                        foreach($results as $r){
                          $l_id = (int)$r->meta_value;
                          if(!in_array($l_id, $lists_post_ids[$k]?:[] ,true)){
                            $lists_post_ids[$k][] = $l_id;
                          }
                        }
                      }
                    }

                    if ($operator == 2) {
                      foreach ($lists_post_ids as $k => $v) {
                        foreach ($v as $k2 => $v2) {
                          if (!in_array($v2, $post_ids , true)) {
                            $post_ids[] = $v2;
                          }
                        }
                      }
                    }else{
                      $post_ids = $lists_post_ids[0]?:[];
                      $l_num = count($lists_post_ids);
                      if ($l_num > 1) {
                        for ($i=1; $i < $l_num; $i++) { 
                          $post_ids = array_intersect($post_ids,$lists_post_ids[$i]);
                        }
                      }
                    }

                    echo '<pre>';
//                    print_r($post_ids);
                    echo '</pre>';
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                    $v_args = [];
                    $v_args = array(
                      'post_type' => 'recipe',
                      'orderby' => 'ASC',
                      'post_status' => 'publish',
                      'posts_per_page' => 16,
                      'paged' => $paged
                    );
                    if (count($post_ids) > 0) {
                      $v_args['post__in'] = $post_ids;
                    }
                    
                    echo '<pre>';
//                    print_r($v_args);
                    echo '</pre>';
                    $recipe_query = null;
                    $recipe_query = new WP_Query($v_args);
                    
                    if ($recipe_query->have_posts() ) :
                        while ( $recipe_query->have_posts() ) : $recipe_query->the_post();           
                            $recipe_cat = get_the_terms(get_the_ID(), 'recipe_category');

                            $recipes = WPRM_Recipe_Manager::get_recipe_ids_from_post();
                            if(!empty($recipes))
                                $recipe_id = $recipes[0];
                            else {
                                $recipe_id = preg_replace('/[^0-9]/', '', get_field('recipe_desc'));
                            }

                            $recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );  
                        ?>
      
                        <div class="section-recipe--pickup">
                            <div class="section-recipe--pickup-inner">
                                <div class="section-recipe--pickup-thumb">
                                    <a href="<?php the_permalink(); ?>" class="link">
                                        <?php
                                        if (has_post_thumbnail()) :
                                            the_post_thumbnail('medium');
                                        else:
                                            echo do_shortcode('[wprm-recipe-image size=\'medium\']');
                                        endif;
                                        ?>
                                    </a>
                                </div>
                                <div class="section-recipe--pickup-cnt">
                                    <p class="date big"><?php the_time('Y.m.d'); ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d'))) ?>]</p>
                                    <h3 class="pickup-title big link"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>
                                    <div class="pc-only">
                                        <?php
                                        if ($recipe || $recipe->tags($key)) {
                                            $terms_course = $recipe->tags('course');
                                            foreach ($terms_course as $term_course) {
                                                printf("<span class=\"tag\">#%s</span>", $term_course->name);
                                            }
                                        }
                                        ?>                                                
                                    </div>
                                    <div class="pickup-infor pc-only">                                                
                                        <span><?php echo esc_html($recipe_cat[0]->name); ?></span>
                                        <span class="time"><?php echo $recipe->cook_time(); ?>分</span>
                                        <span class="kcal"><?php echo $recipe->calories(); ?>kcal</span>
                                    </div>
                                </div>
                            </div>
                            <div class="sp-only">
                                <?php
                                if ($recipe || $recipe->tags($key)) {
                                    $terms_course = $recipe->tags('course');
                                    foreach ($terms_course as $term_course) {
                                        printf("<span class=\"tag\">#%s</span>", $term_course->name);
                                    }
                                }
                                ?>
                            </div>
                        </div><!-- ./section-recipe--pickup -->
                        
                        <?php
                          endwhile;
                          wp_reset_postdata();
                      endif;
                      ?>
                    </div><!-- ./section-recipe--pickupList -->
                    <?php if($recipe_query->found_posts > 0) pagination($recipe_query);?>
                </div><!-- ./section-recipe--row -->                
            </div><!-- ./section-recipe--left -->
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
                    <?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section><!--End .recipe-->

<?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>

<?php get_footer(); ?>
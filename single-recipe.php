<?php get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
				<?php wp_breadcrumb() ?>
            </div>
        </div>
    </div>
    <section class="section recipe end">
        <div class="container">
			<?php
			if ( have_posts() ): while ( have_posts() ) : the_post();
					$recipe_cat = get_the_terms( get_the_ID(), 'recipe_category' );

					$recipes	 = WPRM_Recipe_Manager::get_recipe_ids_from_post();
					if ( !empty( $recipes ) )
						$recipe_id	 = $recipes[ 0 ];
					else {
						$recipe_id = preg_replace( '/[^0-9]/', '', get_field( 'recipe_desc' ) );
					}

					$recipe				 = WPRM_Recipe_Manager::get_recipe( $recipe_id );
					$nutrition_fields	 = WPRM_Nutrition::get_fields();
					$nutrition			 = $recipe->nutrition();

					$nutrition_focus = get_field( 'items_focus' );
					?>
					<div class="section-recipe--left fadeup2">
						<div class="sectionEP-head">
							<div class="sectionEP-head">
								<div class="sectionEP-titleWrap type2">
									<div class="sectionEP-titleInfor">
										<?php
										if ( $recipe_cat && !is_wp_error( $recipe_cat ) ) :
											foreach ( $recipe_cat as $recipe_c ):
												?>
												<span class="tag type2"><?php echo $recipe_c->name; ?></span>
												<?php
											endforeach;
										endif;
										?>
										<span class="date big"><?php the_time( 'Y.m.d' ); ?> [<?php echo strtolower( get_day_txt( get_the_time( 'Y-m-d' ) ) ) ?>]</span>
									</div>
									<h1 class="sectionEP-title type2"><?php the_title() ?></h1>
								</div>
							</div>
							<div class="section-recipe--row">
								<div class="recipe-mv">
									<div class="recipe-mv--thumbWrap">
										<div class="recipe-mv--thumb">
											<?php
											if ( has_post_thumbnail() ) :
												the_post_thumbnail( 'medium_large' );
											else:
												echo do_shortcode( '[wprm-recipe-image size=\'medium_large\']' );
											endif;
											?>
										</div>
										<?php if ( is_user_logged_in() ) { ?>
											<a href="/recipe-favorite" class="link btn-clip js-btn-alert btn-like" data-post-id="<?php the_ID() ?>"><span><?php echo user_is_liked( get_the_ID() ) ? 'クリップ済み' : 'クリップする' ?></span></a>
										<?php } else { ?>
											<a href="javascript:void:(0)" class="link btn-clip" data-remodal-target="modal-clip"><span>クリップする</span></a>
										<?php } ?>
									</div>
									<div class="recipe-mv--des"><?php echo $recipe->summary(); ?></div>
								</div><!-- ./recipe-mv -->
								<div class="recipe-inforMV">
									<div class="recipe-inforMV--row">
										<p class="recipe-inforMV--row-ttl">このレシピのキーワード</p>
										<div>
											<?php
											if ( $recipe || $recipe->tags( $key ) ) {
												$terms_course = $recipe->tags( 'course' );
												foreach ( $terms_course as $term_course ) {
													printf( "<p class=\"tag\">#%s</p>", $term_course->name );
												}
											}
											?>
										</div>
									</div>
									<div class="recipe-inforMV--row">
										<div class="recipe-inforMV--detail">
											<div class="recipe-inforMV--detail-item">
												<p class="recipe-inforMV--row-ttl">カロリー</p>
												<div class="recipe-item--time">
													<span class="recipe-item kcal"><?php echo $recipe->calories(); ?>kcal</span>
												</div>
											</div>
											<div class="recipe-inforMV--detail-item">
												<p class="recipe-inforMV--row-ttl">所要時間</p>
												<div class="recipe-item--time">
													<span class="recipe-item time"><?php echo $recipe->prep_time() + $recipe->cook_time(); ?>分</span>
													<p class="recipe-inforMV--summary">準備時間：<?php echo $recipe->prep_time(); ?>分<?php if ( $recipe->custom_time_label() ) printf( "／%s：%s分", $recipe->custom_time_label(), $recipe->custom_time() ) ?><span class="pc-only2">／</span><br class="sp-only">調理時間：<?php echo $recipe->cook_time(); ?>分</p>
												</div>
											</div>
										</div>
									</div>
								</div><!--End .recipe-inforMV-->
								<div class="printBox">
									<button id="wprm-print-button-print" class="printBox-btn" type="button">レシピを印刷する</button>
								</div>

								<div class="recipe-inforTable">
									<div class="recipe-inforTable--col">
										<div class="recipe-inforTable--titleWrap">
											<h2 class="titleLv3">材料</h2>
											<?php
											$servings	 = $recipe ? $recipe->servings() : false;
											?>
											<div id="wprm-print-servings-container">
												<span class="wprm-print-servings-decrement wprm-print-servings-change">–</span>
												<input id="wprm-print-servings" type="number" value="<?php echo esc_attr( $servings ); ?>" min="1" max="40">
												<span id="wprm-print-servings-unit"><?php echo $recipe->servings_unit(); ?></span>
												<span class="wprm-print-servings-increment wprm-print-servings-change">+</span>
											</div>
										</div>
										<div class="recipe-inforTable--cnt" id="wprm-print-content">
											<div data-recipe-id="<?php echo $recipe_id; ?>" class="wprm-print-recipe wprm-print-recipe-<?php echo $recipe_id; ?>" data-servings="<?php echo $servings ?>">
												<?php
												$ingredients = $recipe->ingredients();
												foreach ( $ingredients as $ingredient_group ):
													?>

													<p class="recipe-inforTable--cnt-label"><?php echo $ingredient_group[ 'name' ]; ?></p>
													<table>
														<?php foreach ( $ingredient_group[ 'ingredients' ] as $ingredient ) : ?>

															<tr>
																<th>
																	<?php
																	if ( $ingredient[ 'link' ][ 'url' ] != "" )
																		printf( "<a href=\"%s\" target=\"_blank\">%s</a>", $ingredient[ 'link' ][ 'url' ], str_replace( '　', '', $ingredient[ 'name' ] ) );
																	else
																		echo str_replace( '　', '', $ingredient[ 'name' ] );
																	?>
																<td>
																	<?php
																	$ingredients_au = preg_split( '/(?<=[0-9])(?=[a-z]+)/i', $ingredient[ 'amount' ] );
																	?>
																	<span class="wprm-recipe-ingredient-amount">
																		<?php echo $ingredient[ 'amount' ] ?>
																	</span>
																	<span class="wprm-recipe-ingredient-unit"><?php echo $ingredient[ 'unit' ]; ?></span>
																</td>
															</tr>

														<?php endforeach; ?>
													</table>
													<?php
												endforeach;
												?>
											</div>
										</div>
									</div>
									<div class="recipe-inforTable--col type2">
										<div class="recipe-inforTable--titleWrap type2">
											<h2 class="titleLv3">栄養素</h2>
											<p class="recipe-inforTable--note">※<?php echo $nutrition[ 'serving_size' ] . $nutrition[ 'serving_unit' ]; ?>あたり</p>
											<span class="recipe-inforTable--ctrl js-ctrlToggle sp-only">開く</span>
										</div>
										<div class="recipe-inforTable--cnt ctrl js-cboxList">
											<table class="nutrients">
												<?php
												foreach ( $nutrition_fields as $field => $options ) {
													if ( $field == 'serving_size' )
														continue;
													if ( isset( $nutrition[ $field ] ) && false !== $nutrition[ $field ] ) {
														$percentage = false;
														if ( $show_daily && isset( $options[ 'daily' ] ) && $options[ 'daily' ] ) {
															$percentage = round( floatval( $nutrition[ $field ] ) / $options[ 'daily' ] * 100 );
														}
														?>

														<tr class="<?php echo in_array( $field, $nutrition_focus ) ? 'active' : ''; ?>">
															<th><?php echo $options[ 'label' ] ?></th>
															<td><?php echo $nutrition[ $field ] ?><?php echo $options[ 'unit' ] ?></td>
														</tr>

														<?php
													}
												}
												?>
											</table>
										</div>
									</div>
								</div><!--End .recipe-inforTable-->
								<?php
								if ( $recipe || $recipe->instructions() ):
									$instructions = $recipe->instructions();
									?>

									<div class="recipe-howtomake">
										<h2 class="titleLv4">作り方</h2>
										<?php foreach ( $instructions as $group_index => $instruction_group ): ?>
											<p class="recipe-howtomake--label"><?php echo $instruction_group[ 'name' ] ?></p>
											<ol class="recipe-howtomake--list">
												<?php foreach ( $instruction_group[ 'instructions' ] as $index => $instruction ) { ?>

													<li>
														<div class="recipe-howtomake--des"><?php echo nl2br( $instruction[ 'text' ] ) ?></div>
														<?php if ( wp_get_attachment_image( $instruction[ 'image' ] ) ) { ?>
															<div class="recipe-howtomake--thumb">
																<?php echo wp_get_attachment_image( $instruction[ 'image' ], 'medium', '', array( 'class' => 'cover' ) ) ?>
															</div>
														<?php } ?>
													</li>
												<?php } ?>
											</ol>
										<?php endforeach; ?>

									</div><!--End .recipe-howtomake-->
								<?php endif ?>
								<div class="recipe-messageBox">
									<div class="recipe-messageBox--feeling"><?php echo do_shortcode( "[wprm-recipe-notes id=\"{$recipe_id}\"]" ); ?></div>
									<?php if ( is_user_logged_in() ) { ?>
										<a href="/recipe-favorite" class="link btn-clip btn-like" data-post-id="<?php the_ID() ?>"><span><?php echo user_is_liked( get_the_ID() ) ? 'クリップ済み' : 'このレシピをクリップする' ?></span></a>
									<?php } else { ?>
										<a href="javascript:void(0)" class="link btn-clip" data-remodal-target="modal-clip"><span>このレシピをクリップする</span></a>
									<?php } ?>
									<div class="socialBox">
										<a href="https://twitter.com/share?url=<?php echo get_the_permalink() ?>&text=<?php the_title(); ?>" target="_blank" class="socialBox-item link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-twitter.png" alt=""></a>
										<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink() ?>" target="_blank" class="socialBox-item link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-facebook.png" alt=""></a>
									</div>
									<a href="/recipe/" class="btn-view-more">レシピ一覧へ</a>
								</div><!--End .recipe-messageBox-->

								<div class="pickup-suggest">
									<?php
									$ingredients = get_post_meta( $recipe_id, 'wprm_ingredients' );
									foreach ( $ingredients as $key => $value ) {
										foreach ( $value as $key2 => $value2 ) {
											foreach ( $value2[ 'ingredients' ] as $key3 => $value3 ) {
												$item[] = str_replace( "　", "", $value3[ 'name' ] );
											}
										}
									}
									$count = 0;
									foreach ( $item as $key => $value ) {
										$args			 = array(
											'post_type'		 => 'wprm_recipe',
											'post_status'	 => 'publish',
											'orderby'		 => 'rand',
											'posts_per_page' => 4,
											'post__not_in'	 => array( $recipe_id ),
											'meta_key'		 => 'wprm_ingredients',
											'meta_value'	 => $value,
											'meta_compare'	 => 'LIKE'
										);
										$second_query	 = null;
										$second_query	 = new WP_Query( $args );
										if ( $second_query->have_posts() ) {
											while ( $second_query->have_posts() ) : $second_query->the_post();
												$recipe_ids[] = get_post_meta( get_the_ID(), 'wprm_parent_post_id', true );
												$count++;
												if ( $count >= 4 ) {
													break;
												}
											endwhile;
										}
										wp_reset_query();
										if ( $count >= 4 ) {
											break;
										}
									}
									if ( $count < 4 ) {
										$cat_ids		 = wp_list_pluck( $recipe_cat, 'term_id' );
										$args			 = array(
											'post_type'		 => 'recipe',
											'tax_query'		 => array(
												array(
													'taxonomy'	 => 'recipe_category',
													'field'		 => 'id',
													'terms'		 => $cat_ids,
													'operator'	 => 'IN',
												),
											),
											'post_status'	 => 'publish',
											'orderby'		 => 'rand',
											'posts_per_page' => 4,
											'post__not_in'	 => array( get_the_ID() )
										);
										$second_query	 = null;
										$second_query	 = new WP_Query( $args );
										while ( $second_query->have_posts() ) : $second_query->the_post();
											$recipe_ids[] = get_the_ID();
											$count++;
											if ( $count >= 4 ) {
												break;
											}
										endwhile;
									}
									$args			 = array(
										'post_type'		 => 'recipe',
										'post_status'	 => 'publish',
										'orderby'		 => 'rand',
										'posts_per_page' => 4,
										'post__in'		 => $recipe_ids,
									);
									$second_query	 = null;
									$second_query	 = new WP_Query( $args );
									if ( $second_query->have_posts() ) {
										?>
										<h3 class="titleLv2">関連するレシピ</h3>
										<ul class="pickup-suggest--list">
											<?php
											while ( $second_query->have_posts() ) : $second_query->the_post();
                                                                                            $recipe_pos = get_field('recipe_display');
												?>

												<li>
                                                                                                    <?php if(!is_user_logged_in() && $recipe_pos === 'is_not_member') {?>
                                                                                                        <a href="javascript:void:(0)" data-remodal-target="modal-clip">
                                                                                                            <div class="pickup-suggest--thumb">
                                                                                                                <?php
                                                                                                                if (has_post_thumbnail()) :
                                                                                                                    the_post_thumbnail('medium', array('class' => 'cover'));
                                                                                                                else:
                                                                                                                    echo do_shortcode('[wprm-recipe-image size=\'medium\' class=\'cover\']');
                                                                                                                endif;
                                                                                                                ?>
                                                                                                            </div>
                                                                                                            <p class="date big"><?php the_time('Y.m.d'); ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d'))) ?>] <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-lock-orange.svg" style="width: 12px; position: relative; bottom: 4px; margin-left: 2px;"></p>
                                                                                                            <p class="pickup-title"><?php the_title() ?></p>
                                                                                                        </a>
                                                                                                    <?php } else {?>
                                                                                                        <a href="<?php the_permalink() ?>" class="link">
                                                                                                            <div class="pickup-suggest--thumb">
                                                                                                                <?php
                                                                                                                if (has_post_thumbnail()) :
                                                                                                                    the_post_thumbnail('medium', array('class' => 'cover'));
                                                                                                                else:
                                                                                                                    echo do_shortcode('[wprm-recipe-image size=\'medium\' class=\'cover\']');
                                                                                                                endif;
                                                                                                                ?>
                                                                                                            </div>
                                                                                                            <p class="date big"><?php the_time('Y.m.d'); ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d'))) ?>]</p>
                                                                                                            <p class="pickup-title"><?php the_title() ?></p>
                                                                                                        </a>
                                                                                                    <?php }?>
												</li>

												<?php
											endwhile;
											wp_reset_query();
											?>
										</ul><!--End .pickup-suggest--list-->
									<?php } ?>
								</div><!--End .pickup-suggest-->

								<?php
								$blog_args	 = array(
									'post_type'		 => 'blog',
									'post_status'	 => 'publish',
									'posts_per_page' => 3,
									'orderby'		 => 'rand'
								);
								$blog_query	 = null;
								$blog_query	 = new WP_Query( $blog_args );
								if ( $blog_query->have_posts() ) :
									?>

									<div class="recipe-selection">
										<h3 class="titleLv2">合わせて読みたいコラム</h3>
										<div class="section-recipe--selectionList">
											<?php
											while ( $blog_query->have_posts() ) : $blog_query->the_post();
												?>

												<div class="section-recipe--selection">
													<a href="<?php the_permalink() ?>" class="section-recipe--selection-inner link">
														<div class="section-recipe--selection-thumb">
															<?php if ( in_array( 'pr_status', get_field( 'pr_icon' ) ) ): ?>
																<span class="blog-item--thumb-label">PR</span>
															<?php endif; ?>
															<?php if ( has_post_thumbnail() ) : ?>
																<?php the_post_thumbnail( 'medium', array( 'class' => 'cover' ) ); ?>
															<?php else: ?>
																<img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-img.jpg" class="cover" />
															<?php endif; ?>
														</div>
														<div class="section-recipe--selection-cnt pickup-title"><?php the_title() ?></div>
													</a>
												</div>

											<?php endwhile;
											?>
										</div>
									</div><!--End .recipe-selection-->
									<?php
								endif;
								wp_reset_postdata();
								?>

							</div><!--End .section-recipe--row-->
						</div><!--End .sectionEP-head-->
					</div><!-- ./section-recipe--left -->
				<?php endwhile; ?>
			<?php endif; ?>
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
					<?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>

		<?php if ( !is_user_logged_in() ) { ?>
			<!-- modal box -->
			<div class="remodal" data-remodal-id="modal-clip"
				 data-remodal-options="hashTracking: false, closeOnOutsideClick: false">

				<button data-remodal-action="close" class="remodal-close btn-close"></button>
				<h3 class="remodal-ttl">お気に入りのレシピを手軽に”クリップ”</h3>
				<p class="desc align-center">クリップはお気に入りのレシピを保存できる機能です。 <br>ご利用には無料の会員登録が必要です。 <br>ログイン後はどなたでもご利用いただけます。</p>
				<a href="/register" class="btn-gray">会員登録する</a><br>
				<a class="link underline" href="/login">既に登録済みの方はこちら</a>
			</div>
			<!-- ./modal box -->
		<?php } else { ?>

			<!-- modal box -->
			<div class="modal-wrapper type2 js-modal-del-msg">
				<div class="modal js-modal-del-cnt animate fadein">
					<div class="modal-cnt">
						<div class="modal-cnt--inner">
							<div class="modal-cnt--icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-tick.png" alt=""></div>
							<p class="modal-cnt--msg like_message">クリップに<br>追加されました</p>
						</div>
					</div>
				</div>
			</div>
			<!-- ./modal box -->

		<?php } ?>

    </section><!--End .recipe-->

	<?php get_template_part( 'template-parts/fronts/advertising' ); ?>

</main>

<?php get_footer(); ?>

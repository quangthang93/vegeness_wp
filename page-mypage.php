<?php get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
				<?php wp_breadcrumb() ?>
            </div>
        </div>
    </div><!--End .breadcrumbWrap-->
    <section class="section recipe end">
        <div class="container">
            <div class="section-recipe--left fadeup2">
                <div class="tabs-listWrap type2">
                    <div class="tabs-list js-tabsList2">
                        <a href="" class="tabs-item active" data-type="type00">すべて</a>
						<?php
						$recipe_cats = get_terms( 'recipe_category', 'hide_empty=1' );
						foreach ( $recipe_cats as $recipe_cat ) {
							$recipe_link = get_term_link( $recipe_cat, 'blog_category' );
							echo '<a href="" class="tabs-item" data-type="type' . $recipe_cat->term_id . '">' . $recipe_cat->name . '</a>';
						}
						?>
                    </div>
                </div><!-- ./tabs-listWrap -->
                <ul class="clip-list js-clipList">
					<?php
					$user_id	 = get_current_user_id();
					// ユーザーメタからお気に入りデータ取得
					$posts_liked = get_user_meta( $user_id, 'tcd_likes', true );
					if ( $posts_liked ) {
						$posts_liked = array_map( 'intval', explode( ',', $posts_liked ) );
					} else {
						$posts_liked = array( 0 );
					}

					$args[ 'post_type' ]		 = 'recipe';
					$args[ 'post__in' ]			 = $posts_liked;
					$args[ 'posts_per_page' ]	 = 100;
					$args[ 'post_status' ]		 = 'publish';
					$args[ 'orderby' ]			 = 'date';
					$args[ 'order' ]			 = 'DESC';

					$recipe_query	 = null;
					$recipe_query	 = new WP_Query( $args );

					if ( $recipe_query->have_posts() ):
						while ( $recipe_query->have_posts() ): $recipe_query->the_post();
							$recipe_terms = get_the_terms( get_the_ID(), 'recipe_category' );
							?>

							<li>
								<a href="<?php the_permalink(); ?>" class="link">
									<div class="clip-list--thumbWrap">
										<div class="clip-list--thumb">
											<?php
											if ( has_post_thumbnail() ) :
												the_post_thumbnail( 'medium' );
											else:
												echo do_shortcode( '[wprm-recipe-image size=\'medium\']' );
											endif;
											?>
										</div>
									</div>
									<p><span class="tagLB type<?php echo $recipe_terms[ 0 ]->term_id ?>">[<?php echo esc_html( $recipe_terms[ 0 ]->name ); ?>]</span></p>
									<p class="titleLv5"><?php the_title() ?></p>
								</a>
								<a href="javascript:void:(0)" class="btn-clip2 js-btn-clip2 remove-like" data-post-id="<?php the_ID() ?>"></a>
							</li>
							<?php
						endwhile;
						wp_reset_postdata();
					endif;
					?>

                </ul><!--End .clip-list-->
            </div><!--End .section-recipe--left-->
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
					<?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
        <div class="modal-wrapper type2 js-modal-del-msg">
            <div class="modal js-modal-del-cnt animate fadein">
                <div class="modal-cnt">
                    <div class="modal-cnt--inner">
                        <div class="modal-cnt--icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-tick.png" alt=""></div>
                        <p class="modal-cnt--msg like_message">クリップから<br>削除しました</p>
                    </div>
                </div>
            </div>
        </div>
    </section><!--End .recipe-->
	<?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>

<?php if ( get_field( 'campaign_img', "option" ) ): ?>
	<div class="modal-overlay js-modal-overlay hide"></div>
	<div class="modal-wrapper type3 hide">
		<div class="modal js-modal">
			<a href="javascript:void(0)" class="modal-close btn-close js-modal-close"></a>
			<div class="modal-cnt">
				<a href="<?php the_field( "campaign_url", "option" ) ?>" target="_blank"><img src="<?php the_field( "campaign_img", "option" ) ?>" alt=""></a>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>

<?php get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
                <?php wp_breadcrumb() ?>
            </div>
        </div>
    </div>
    <section class="section recipe end">
        <div class="container">
            <div class="section-recipe--left fadeup2">
                <div class="sectionEP-head">
                    <div class="sectionEP-titleWrap">
                        <h1 class="sectionEP-title">レシピ一覧</h1>
                        <p class="sectionEP-resultSumary"></p>
                    </div>
                    <div class="sectionEP-sort">
                        <div class="sectionEP-newOrders">
                            <div class="form-search">
                                <select class="input" name="sort" onchange="document.location.href='?view='+this.options[this.selectedIndex].value;">
                                    <option value="newer" <?php selected( $_GET['view'], 'newer' )?>>新着順</option>
                                    <option value="popular" <?php selected( $_GET['view'], 'popular' )?>>人気順</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-recipe--row">
                    <div class="section-recipe--pickupList">
                        <?php                                
                        $args['post_type'] = 'recipe';
                        $args['post_status'] = 'publish';
                        $args['orderby'] = 'date';
                        $args['order'] = 'DESC';                                
                        $args['posts_per_page'] = 16;
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args['paged'] = $paged;                                

                        $recipe_query = null;
                        $recipe_query = new WP_Query($args);

                        if ($recipe_query->have_posts()):
                            while ($recipe_query->have_posts()): $recipe_query->the_post();
                                $recipe_cat = get_the_terms(get_the_ID(), 'recipe_category');
                                
                                $recipes = WPRM_Recipe_Manager::get_recipe_ids_from_post();
                                $recipe_id = $recipes[0];
                                $recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );
                                ?>
                                <div class="section-recipe--pickup">
                                    <div class="section-recipe--pickup-inner">
                                        <div class="section-recipe--pickup-thumb">
                                            <a href="<?php the_permalink(); ?>" class="link">
                                                <?php if (has_post_thumbnail()) : 
                                                    the_post_thumbnail('medium_large');
                                                else: 
                                                    echo do_shortcode('[wprm-recipe-image size=\'medium\']');
                                                endif;?>
                                            </a>
                                        </div>
                                        <div class="section-recipe--pickup-cnt">
                                            <p class="date big"><?php the_time('Y.m.d'); ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d')))?>]</p>
                                            <h3 class="pickup-title big link"><a href="<?php the_permalink(); ?>"><?php the_title()?></a></h3>
                                            <div class="pc-only">
                                                <?php
                                                if ($recipe || $recipe->tags($key)) {
                                                    $terms_course = $recipe->tags( 'course' );
                                                    foreach ( $terms_course as $term_course ) {
                                                        printf("<span class=\"tag\">#%s</span>", $term_course->name);
                                                    }
                                                }
                                                ?>                                                
                                            </div>
                                            <div class="pickup-infor pc-only">                                                
                                                <span><?php echo esc_html( $recipe_cat[0]->name );?></span>
                                                <span class="time"><?php echo $recipe->prep_time() + $recipe->cook_time(); ?>分</span>
                                                <span class="kcal"><?php echo $recipe->calories();?>kcal</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sp-only">
                                        <?php
                                        if ($recipe || $recipe->tags($key)) {
                                            $terms_course = $recipe->tags( 'course' );
                                            foreach ( $terms_course as $term_course ) {
                                                printf("<span class=\"tag\">#%s</span>", $term_course->name);
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="pickup-infor sp-only">
                                        <span><?php echo esc_html( $recipe_cat[0]->name );?></span>
                                        <span class="time"><?php echo $recipe->prep_time() + $recipe->cook_time(); ?>分</span>
                                        <span class="kcal"><?php echo $recipe->calories();?>kcal</span>
                                    </div>
                                </div><!-- ./section-recipe--pickup -->                                

                            <?php
                            endwhile;
                            wp_reset_postdata();
                        endif;
                        ?>                        
                    </div><!-- ./section-recipe--pickupList -->
                    <?php if($recipe_query->found_posts > 0) pagination($recipe_query);?>
                </div><!-- ./section-recipe--row -->                
            </div><!-- ./section-recipe--left -->
            <div class="section-recipe--right fadeup2 pc-only">
                <div class="section-recipe--right-inner">
                    <?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section><!--End .recipe-->
    
    <?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>

<?php get_footer(); ?>

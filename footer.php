        <footer class="footer">
            <div class="footer-navWrapper">
                <div class="footer-nav">
                    <?php echo footer_nav()?>
                    <?php echo footer_nav2()?>
                    
                    <div class="footer-sns">
                        <a class="link" href="https://www.instagram.com/veganrecipes.jp/?hl=ja" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-insta.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="footer-logo link">
                    <a href="<?php echo home_url(); ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/logo.svg" alt="">
                    </a>
                </div>
            </div>
            <p class="footer-copy">Copyright © <?php echo date('Y'); ?> Vegeness-ベジネス</p>            
        </footer><!-- ./footer -->

    </div>
    <!-- /wrapper -->
    <?php wp_footer(); ?>

    <?php if (!is_user_logged_in()) { ?>
        <!-- modal box -->
        <div class="remodal" data-remodal-id="modal-clip"
             data-remodal-options="hashTracking: false, closeOnOutsideClick: false">

            <button data-remodal-action="close" class="remodal-close btn-close"></button>
            <h3 class="remodal-ttl">会員の方だけにお届けする限定レシピ</h3>
            <p class="desc align-center">会員限定レシピをご覧いただくには無料の会員登録が必要です。<br>ログイン後はどなたでもご利用いただけます。</p>
            <a href="/register" class="btn-gray">会員登録する</a><br>
            <a class="link underline" href="/login?nonce=<?php echo wp_create_nonce('recipe_nonce')?>">既に登録済みの方はこちら</a>
        </div>
        <!-- ./modal box -->
    <?php } ?>
        
    </body>
</html>

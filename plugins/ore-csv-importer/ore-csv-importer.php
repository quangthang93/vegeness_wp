<?php

/*
  Plugin Name: ORE CSV Importer
  Description: Import posts, categories, tags, custom fields from simple csv file.
  Author: kurozuim
  Author URI: http://a-zumi.net
  Plugin URI: http://a-zumi.net
  Text Domain: really-simple-csv-importer
 */

// Load Really Simple CSV Importer Plugin
$really_simple_csv_importer = ABSPATH . 'wp-content/plugins/really-simple-csv-importer/rs-csv-importer.php';

if (!file_exists($really_simple_csv_importer))
	wp_die('Really Simple CSV Importerプラグインをインストールして下さい。');

require $really_simple_csv_importer;

/**
 * CSV Importer
 *
 * @package WordPress
 * @subpackage Importer
 */
if (class_exists('RS_CSV_Importer')) {

	class ORE_CSV_Importer extends RS_CSV_Importer {

		// process parse csv ind insert posts
		function process_posts()
		{
			$h = new RS_CSV_Helper;

			$handle = $h->fopen($this->file, 'r');
			if ($handle == false) {
				echo '<p><strong>' . __('Failed to open file.', 'really-simple-csv-importer') . '</strong></p>';
				wp_import_cleanup($this->id);
				return false;
			}

			$is_first = true;
			$post_statuses = get_post_stati();

			echo '<ol>';

			while (($data = $h->fgetcsv($handle)) !== FALSE) {
				if ($is_first) {
					$h->parse_columns($this, $data);
					$is_first = false;
				} else {
					echo '<li>';

					$post = array();
					$is_update = false;
					$error = new WP_Error();

					// (string) (required) post type
					$post_type = $h->get_data($this, $data, 'post_type');
					if ($post_type) {
						if (post_type_exists($post_type)) {
							$post['post_type'] = $post_type;
						} else {
							$error->add('post_type_exists', sprintf(__('Invalid post type "%s".', 'really-simple-csv-importer'), $post_type));
						}
					} else {
						echo __('Note: Please include post_type value if that is possible.', 'really-simple-csv-importer') . '<br>';
					}

					// (int) post id
					$post_id = $h->get_data($this, $data, 'ID');
					$post_id = ($post_id) ? $post_id : $h->get_data($this, $data, 'post_id');
					if ($post_id) {
						$post_exist = get_post($post_id);
						if (is_null($post_exist)) { // if the post id is not exists
							$post['import_id'] = $post_id;
						} else {
							if (!$post_type || $post_exist->post_type == $post_type) {
								$post['ID'] = $post_id;
								$is_update = true;
							} else {
								$error->add('post_type_check', sprintf(__('The post type value from your csv file does not match the existing data in your database. post_id: %d, post_type(csv): %s, post_type(db): %s', 'really-simple-csv-importer'), $post_id, $post_type, $post_exist->post_type));
							}
						}
					}

					// (string) post slug
					$post_name = $h->get_data($this, $data, 'post_name');
					if ($post_name) {
						$post['post_name'] = $post_name;
					}

					// (login or ID) post_author
					$post_author = $h->get_data($this, $data, 'post_author');
					if ($post_author) {
						if (is_numeric($post_author)) {
							$user = get_user_by('id', $post_author);
						} else {
							$user = get_user_by('login', $post_author);
						}
						if (isset($user) && is_object($user)) {
							$post['post_author'] = $user->ID;
							unset($user);
						}
					}

					// (string) publish date
					$post_date = $h->get_data($this, $data, 'post_date');
					if ($post_date) {
						$post['post_date'] = date("Y-m-d H:i:s", strtotime($post_date));
					}

					// (string) post status
					$post_status = $h->get_data($this, $data, 'post_status');
					if ($post_status) {
						if (in_array($post_status, $post_statuses)) {
							$post['post_status'] = $post_status;
						}
					}

					// (string) post title
					$post_title = $h->get_data($this, $data, 'post_title');
					if ($post_title) {
						$post['post_title'] = $post_title;
					}

					// (string) post content
					$post_content = $h->get_data($this, $data, 'post_content');
					if ($post_content) {
						$post['post_content'] = $post_content;
					}

					// (string) post excerpt
					$post_excerpt = $h->get_data($this, $data, 'post_excerpt');
					if ($post_excerpt) {
						$post['post_excerpt'] = $post_excerpt;
					}

					// (int) post parent
					$post_parent = $h->get_data($this, $data, 'post_parent');
					if ($post_parent) {
						$post['post_parent'] = $post_parent;
					}

					// (int) menu order
					$menu_order = $h->get_data($this, $data, 'menu_order');
					if ($menu_order) {
						$post['menu_order'] = $menu_order;
					}

					// (string, comma separated) slug of post categories
					// $post_category = $h->get_data($this, $data, 'post_category');
					$post_category = $h->get_data($this, $data, 'shop_area');
					if ($post_category) {
						// $parent_categories = array();
						
						// $categories = preg_split("/,+/", $post_category);
						// if ($categories) {
						// 	// $parent_categories = wp_create_categories($categories);
						// 	$parent_categories = wp_create_term($categories, 'shop_area');
						// 	$post['post_category'] = $parent_categories;
						// }

						$categories = preg_split("/,+/", $post_category);
						$categories_parent = $categories[0];

						if ($categories_parent) {
							$parent_categories = wp_create_term($categories_parent, 'shop_area');
							$post['post_category'] = $parent_categories;
						}

						/**
						 * 小カテゴリ登録用のCSV項目追加
						 */
						// $post_child_category = $h->get_data($this, $data, 'post_child_category');
						// if ($post_child_category) {
						// 	$categories = preg_split("/,+/", $post_child_category);
						// 	if ($categories) {
						// 		foreach ($parent_categories as $parent_category) {
						// 			foreach ($categories as $category) {
						// 				// $post['post_category'][] = wp_create_category($category, $parent_category);
						// 				$post['post_category'][] = wp_insert_term($category, 'shop_area', array('parent' => $parent_category));
						// 			}
						// 		}
						// 	}
						// }

						echo count($categories);
						if(!empty(array_filter($categories))) {
							// echo '<pre>';
							// print_r($categories);
							// echo '</pre>';
							array_shift($categories);
							if (count($categories) > 0) {
								$categories_level_second = $categories[0];
								$term_check1 = term_exists( $categories[0] );
								if ( $term_check1 == 0 || $term_check1 == null )
									$parent_categories2 = $post['post_category'][] = wp_insert_term($categories[0], 'shop_area', array('parent' => $parent_categories['term_id']));
							}

							array_shift($categories);
							// echo '<pre>__';
							// print_r($categories);
							// echo '</pre>';
							if (count($categories) > 0) {
								$term_check2 = term_exists( $categories[0] );
								if ( $term_check2 == 0 || $term_check2 == null )
									$parent_categories3 = $post['post_category'][] = wp_insert_term($categories[0], 'shop_area', array('parent' => $parent_categories2['term_id']));
							}
						}

					}
					unset($categories, $parent_categories, $parent_categories2);

					// (string, comma separated) name of post tags
					$post_tags = $h->get_data($this, $data, 'post_tags');
					if ($post_tags) {
						$post['post_tags'] = $post_tags;
					}

					// (string) post thumbnail image uri
					$post_thumbnail = $h->get_data($this, $data, 'post_thumbnail');

					$meta = array();
					$tax = array();

					// add any other data to post meta
					foreach ($data as $key => $value) {
						if ($value !== false && isset($this->column_keys[$key])) {
							// check if meta is custom taxonomy
							if (substr($this->column_keys[$key], 0, 4) == 'tax_') {
								// (string, comma divided) name of custom taxonomies 
								$customtaxes = preg_split("/,+/", $value);
								$taxname = substr($this->column_keys[$key], 4);
								$tax[$taxname] = array();
								foreach ($customtaxes as $key => $value) {
									$tax[$taxname][] = $value;
								}
							} else {
								$meta[$this->column_keys[$key]] = $value;
							}
						}
					}

					/**
					 * Filter post data.
					 *
					 * @param array $post (required)
					 * @param bool $is_update
					 */
					$post = apply_filters('really_simple_csv_importer_save_post', $post, $is_update);
					/**
					 * Filter meta data.
					 *
					 * @param array $meta (required)
					 * @param array $post
					 * @param bool $is_update
					 */
					$meta = apply_filters('really_simple_csv_importer_save_meta', $meta, $post, $is_update);
					/**
					 * Filter taxonomy data.
					 *
					 * @param array $tax (required)
					 * @param array $post
					 * @param bool $is_update
					 */
					$tax = apply_filters('really_simple_csv_importer_save_tax', $tax, $post, $is_update);

					/**
					 * Option for dry run testing
					 *
					 * @since 0.5.7
					 *
					 * @param bool false
					 */
					$dry_run = apply_filters('really_simple_csv_importer_dry_run', false);

					if (!$error->get_error_codes() && $dry_run == false) {

						/**
						 * Get Alternative Importer Class name.
						 *
						 * @since 0.6
						 *
						 * @param string Class name to override Importer class. Default to null (do not override).
						 */
						$class = apply_filters('really_simple_csv_importer_class', null);

						// save post data
						if ($class && class_exists($class, false)) {
							$importer = new $class;
							$result = $importer->save_post($post, $meta, $tax, $post_thumbnail, $is_update);
						} else {
							$result = $this->save_post($post, $meta, $tax, $post_thumbnail, $is_update);
						}

						if ($result->isError()) {
							$error = $result->getError();
						} else {
							$post_object = $result->getPost();

							if (is_object($post_object)) {
								/**
								 * Fires adter the post imported.
								 *
								 * @since 1.0
								 *
								 * @param WP_Post $post_object
								 */
								do_action('really_simple_csv_importer_post_saved', $post_object);
							}

							echo esc_html(sprintf(__('Processing "%s" done.', 'really-simple-csv-importer'), $post_title));
						}
					}

					// show error messages
					foreach ($error->get_error_messages() as $message) {
						echo esc_html($message) . '<br>';
					}

					echo '</li>';
				}
			}

			echo '</ol>';

			$h->fclose($handle);

			wp_import_cleanup($this->id);

			echo '<h3>' . __('All Done.', 'really-simple-csv-importer') . '</h3>';
		}

	}

	// Initialize
	function ore_csv_importer()
	{
		load_plugin_textdomain('really-simple-csv-importer', false, './really-simple-csv-importer/languages');

		$ore_csv_importer = new ORE_CSV_Importer();
		register_importer('csv', __('CSV', 'really-simple-csv-importer'), __('Import posts, categories, tags, custom fields from simple csv file.', 'really-simple-csv-importer'), array($ore_csv_importer, 'dispatch'));
	}

	remove_action('plugins_loaded', 'really_simple_csv_importer');
	add_action('plugins_loaded', 'ore_csv_importer');
} // class_exists( 'RS_CSV_Importer' )
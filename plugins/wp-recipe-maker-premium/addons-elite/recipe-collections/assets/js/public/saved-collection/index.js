import ReactDOM from 'react-dom';
import React from 'react';
import { HashRouter } from 'react-router-dom';

import App from './App';

let appContainers = document.getElementsByClassName( 'wprm-recipe-saved-collections-app' );
let existingAppContainer = document.getElementById( 'wprm-recipe-collections-app' );

if (  ! existingAppContainer ) {
	for ( let appContainer of appContainers ) {
		const id = appContainer.dataset.id;

		if ( id ) {
			ReactDOM.render(
				<HashRouter
					basename={ `/collection-${id}` }
					hashType="noslash"
				>
					<App
						id={ id }
					/>
				</HashRouter>,
				appContainer
			);
		}
	}
}
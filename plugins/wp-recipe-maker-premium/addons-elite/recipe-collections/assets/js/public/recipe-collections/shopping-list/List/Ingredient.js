import React, { Fragment } from 'react';
import { Draggable } from 'react-beautiful-dnd';
import he from 'he';

import Checkbox from '../../general/Checkbox';
import Icon from '../../general/Icon';

const Ingredient = (props) => {
    const { ingredient, index, editing } = props;

    if ( ! ingredient.name && ! editing ) {
        return null;
    }

    return (
        <Draggable
            draggableId={ `ingredient-${ingredient.id}` }
            index={ index }
            key={ ingredient.id }
            type="INGREDIENT"
            isDragDisabled={ ! editing }
        >
            {(provided, snapshot) => (
                <div
                    className={`wprmprc-shopping-list-list-ingredient${ ingredient.checked && ! editing ? ' wprmprc-shopping-list-list-ingredient-checked' : ''}`}
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                >
                    <div className="wprmprc-shopping-list-list-ingredient-name-container">
                        {
                            editing
                            ?
                            <Fragment>
                                <div className="wprmprc-shopping-list-editing-actions">
                                    <div
                                        className="wprmprc-shopping-list-editing-handle"
                                        {...provided.dragHandleProps}
                                    ><Icon type="drag" /></div>
                                    <div
                                        className="wprmprc-shopping-list-editing-delete"
                                        onClick={() => {
                                            props.onIngredientDelete();
                                        }}
                                    ><Icon type="delete" /></div>
                                </div>
                                <input
                                    type="text"
                                    value={ he.decode( ingredient.name ) }
                                    onChange={(event) => {
                                        props.onIngredientChange({
                                            ...ingredient,
                                            name: event.target.value,
                                        });
                                    }}
                                />
                            </Fragment>
                            :
                            <Fragment>
                                <Checkbox
                                    checked={ ingredient.checked }
                                    onChange={ ( checked ) => {
                                        props.onIngredientChange({
                                            ...ingredient,
                                            checked,
                                        });
                                    } }
                                    id={ `wprmprc-shopping-list-list-ingredient-${ ingredient.id }` }
                                />
                                <label
                                    className="wprmprc-shopping-list-list-ingredient-name"
                                    htmlFor={ `wprmprc-shopping-list-list-ingredient-${ ingredient.id }` }
                                >
                                    {
                                        ingredient.link && ingredient.link.url
                                        ?
                                        <a href={ingredient.link.url} target="_blank" rel="nofollow">{ he.decode( ingredient.name ) }</a>
                                        :
                                        he.decode( ingredient.name )
                                    }
                                </label>
                            </Fragment>
                        }
                    </div>
                    <div className="wprmprc-shopping-list-list-ingredient-variations">
                        {
                            ingredient.variations.map((variation, index) => {
                                let variationDisplay = variation.hasOwnProperty( 'display' ) ? variation.display : `${ variation.amount ? variation.amount + ' ' : '' } ${ variation.unit ? variation.unit : '' }`;

                                // Trim (but only when not editing or you can't type spaces).
                                if ( ! editing ) {
                                    variationDisplay = variationDisplay.trim();
                                }

                                if ( ! variationDisplay && ! editing ) {
                                    return null;
                                }

                                if ( editing ) {
                                    return (
                                        <div className="wprmprc-shopping-list-list-ingredient-variation" key={index}>
                                            <input
                                                type="text"
                                                value={ variationDisplay }
                                                onChange={(event) => {
                                                    let newVariations = JSON.parse( JSON.stringify( ingredient.variations ) );

                                                    newVariations[index].display = event.target.value;

                                                    props.onIngredientChange({
                                                        ...ingredient,
                                                        variations: newVariations,
                                                    });
                                                }}
                                            />
                                        </div>
                                    )
                                } else {
                                    return (
                                        <div className="wprmprc-shopping-list-list-ingredient-variation" key={index}>{ variationDisplay }</div>
                                    )
                                }
                            })
                        }
                    </div>
                </div>
            )}
        </Draggable>
    );
}
export default Ingredient;
import React from 'react';
import SVG from 'react-inlinesvg';

import IconCheckboxEmpty from '../../../../icons/checkbox-empty.svg';
import IconCheckboxChecked from '../../../../icons/checkbox-checked.svg';
import IconDelete from '../../../../icons/delete.svg';
import IconDuplicate from '../../../../icons/duplicate.svg';
import IconDrag from '../../../../icons/drag.svg';
import IconMinus from '../../../../icons/minus.svg';
import IconPlus from '../../../../icons/plus.svg';

import '../../../../css/public/icon.scss';
 
const icons = {
    checkboxEmpty: IconCheckboxEmpty,
    checkboxChecked: IconCheckboxChecked,
    delete: IconDelete,
    duplicate: IconDuplicate,
    drag: IconDrag,
    minus: IconMinus,
    plus: IconPlus,
};

const Icon = (props) => {
    let icon = icons.hasOwnProperty(props.type) ? icons[props.type] : false;

    if ( !icon ) {
        return null;
    }

    return (
        <span className='wprmprc-icon'>
            <SVG
                src={icon}
            />
        </span>
    );
}
export default Icon;
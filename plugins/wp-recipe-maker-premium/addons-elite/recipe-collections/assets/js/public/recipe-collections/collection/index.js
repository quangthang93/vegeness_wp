import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { DragDropContext } from 'react-beautiful-dnd';
import Hashids from 'hashids'

import { __wprm } from 'Shared/Translations';

import Loader from '../general/Loader';
import Print from '../general/Print';
import Actions from './Actions';
import AddItem from './Actions/AddItem';
import Group from './Group';
import Nutrition from './Nutrition';
import '../../../../css/public/collection.scss';

class Collection extends Component {
    constructor(props) {
        super(props);

        const showNutrition = this.props.collection.showNutrition ? this.props.collection.showNutrition : wprmprc_public.settings.recipe_collections_nutrition_facts_hidden_default;
        this.state = {
            addItems: {
                'collection': [],
                'search': [],
                'ingredient': [],
                'custom': [],
                'note': [],
            },
            mode: 'overview',
            modeOptions: {
                'add-item': {
                    mode: 'admin' === this.props.type || 'inbox' === this.props.type ? 'search' : wprmprc_public.settings.recipe_collections_default_add,
                    group: false,
                    collection: 'inbox',
                    searchRecipe: '',
                    searchIngredient: '',
                },
            },
            showNutrition, // Only used when displaying saved nutrition.
            print: false,
        }

        this.printCollection = React.createRef();
        this.onPrintRecipes = this.onPrintRecipes.bind(this);
    }
    
    onDragEnd(result) {
        if ( result.destination && 'RECIPE' === result.type ) {
            let items = JSON.parse(JSON.stringify(this.props.collection.items));
            let [ destinationColumn, destinationGroup ] = result.destination.droppableId.split('-');
            destinationColumn = parseInt( destinationColumn );
            destinationGroup = parseInt( destinationGroup );

            const destination = `${this.props.collection.columns[destinationColumn].id}-${this.props.collection.groups[destinationGroup].id}`;
            const destinationIndex = result.destination.hasOwnProperty( 'index' ) ? result.destination.index : false;

            // Make sure items is an object and destination exists.
            if ( Object !== items.constructor ) {
                items = {};
            }
            if ( ! items.hasOwnProperty(destination) ) {
                items[destination] = [];
            }

            let item;

            if ( 'click' === result.source || 'select-items' === result.source.droppableId ) {
                // Adding item.
                // Get maxId in use.
                const allItems = Object.values(items).reduce( (allItems, groupItems) => allItems.concat(groupItems), [] );
                let maxId = Math.max.apply( Math, allItems.map( function(item) { return item.id; } ) );
                maxId = maxId < 0 ? -1 : maxId;

                if ( 'select-items' === result.source.droppableId ) {
                    const itemId = parseInt(result.draggableId.substr(7));
                    item = { ...this.state.addItems[ this.state.modeOptions['add-item'].mode ].find((item) => itemId === item.id) };
                } else {
                    item = result.item;
                }

                // Give item a unique ID.
                item.id = maxId + 1;
            } else {
                const [ sourceColumn, sourceGroup ] = result.source.droppableId.split('-');
                const source = `${this.props.collection.columns[sourceColumn].id}-${this.props.collection.groups[sourceGroup].id}`;
                const sourceIndex = result.source.index;

                // Remove item from source.
                item = items[source].splice(sourceIndex, 1)[0];
            }

            // Add item to destination.
            if ( false === destinationIndex ) {
                items[destination].push( item );
            } else {
                items[destination].splice(destinationIndex, 0, item);
            }

            // Update collection.
            this.props.onChangeCollection(this.props.type, this.props.collection.id, { items } );
        }
    }

    onChangeAddItems(items) {
        // Clean up items and add index.
        items = items.map( (item, index) => {
            item.id = index;
            item.servings = 0 < parseInt( item.servings ) ? parseInt( item.servings ) : 1;

            if ( 'recipe' === item.type ) {
                item.recipeId = parseInt( item.recipeId );
            }

            return item;
        } );

        this.setState({
            addItems: {
                ...this.state.addItems,
                [this.state.modeOptions['add-item'].mode]: items,
            },
        })
    }

    onChangeModeOptions(options) {
        let modeOptions = JSON.parse( JSON.stringify( this.state.modeOptions ) );
        modeOptions[ this.state.mode ] = options;

        this.setState({
            modeOptions,
        });
    }

    onDeleteItem( columnId, groupId, index ) {
        let items = { ...this.props.collection.items };
        items[`${columnId}-${groupId}`].splice(index, 1);

        this.props.onChangeCollection( this.props.type, this.props.collection.id, { items } );
    }

    onChangeItem( columnId, groupId, index, field, value ) {
        let items = JSON.parse(JSON.stringify(this.props.collection.items));

        if ( items[`${columnId}-${groupId}`] && items[`${columnId}-${groupId}`][index] && 0 <= value ) {
            items[`${columnId}-${groupId}`][index][field] = value;

            this.props.onChangeCollection( this.props.type, this.props.collection.id, { items } );
        }
    }

    onPrintRecipes() {
        // Get recipes to print.
        let recipesToPrint = [];

        if ( this.props.collection.items ) {
            const items = Object.values(this.props.collection.items).reduce( (allItems, groupItems) => allItems.concat(groupItems), [] );

            for ( let item of items ) {
                if ( 'recipe' === item.type && item.recipeId && item.servings > 0 ) {
                    recipesToPrint.push(item.recipeId);
                    recipesToPrint.push(item.servings);
                }
            }
        }

        // Get Print URL.
        const urlParts = wprm_public.home_url.split(/\?(.+)/);
        let printUrl = urlParts[0];

        // Encode recipes to print.
        const hashids = new Hashids( 'wp-recipe-maker' );
        const recipesToPrintEncoded = hashids.encode( recipesToPrint );

        if ( wprm_public.permalinks ) {
            printUrl += wprm_public.print_slug + '/recipes';
            printUrl += '/' + recipesToPrintEncoded;

            if ( urlParts[1] ) {
                printUrl += '?' + urlParts[1];
            }
        } else {
            printUrl += '?' + wprm_public.print_slug + '=recipes';
            printUrl += '&' + recipesToPrintEncoded;

            if ( urlParts[1] ) {
                printUrl += '&' + urlParts[1];
            }
        }

        window.open( printUrl, '_blank' );
    }

    render() {
        const { collection } = this.props;
        const isLoading = ! collection.columns || ! collection.groups;

        // Use value from collection unless displaying a saved collection in frontend.
        let showNutrition = collection.hasOwnProperty( 'showNutrition' ) ? collection.showNutrition : wprmprc_public.settings.recipe_collections_nutrition_facts_hidden_default;
        if ( 'saved' === this.props.type ) {
            showNutrition = this.state.showNutrition;
        }

        return (
            <Fragment>
                <div ref={this.printCollection}>
                    {
                        'admin' !== this.props.type
                        &&
                        <div className="wprmprc-container-header">
                            {
                                'saved' !== this.props.type
                                &&
                                <Fragment>
                                    <span className="wprmprc-header-link"
                                        onClick={() => {
                                            this.props.history.push(`/`);
                                        }}
                                    >{ __wprm( 'Your Collections' ) }{ parseInt( wprmprc_public.user ) !== parseInt( wprmprc_public.collections_user ) ? ` (${ __wprm( 'Editing User' ) } #${ wprmprc_public.collections_user })` : ''}</span>
                                    <span className="wprmprc-header-link-separator">&gt;</span>
                                </Fragment>
                            }
                            {collection.name}
                        </div>
                    }
                    {
                        isLoading
                        ?
                        <Loader />
                        :
                        <div className="wprmprc-collection">
                            <DragDropContext
                                onDragEnd={this.onDragEnd.bind(this)}
                            >
                                {
                                    collection.columns.map( (column, columnIndex) => {
                                        let itemsInColumn = [];
                                        const isAddingItemsInColumn = false !== this.state.modeOptions['add-item'].group;
                                        const showAddItemsColumn = isAddingItemsInColumn && columnIndex === this.state.modeOptions['add-item'].group.column;

                                        let style = {};
                                        if ( isAddingItemsInColumn ) {
                                            style = showAddItemsColumn ? { display: 'none' } : { opacity: 0.2 };
                                        }

                                        return (
                                            <Fragment key={ columnIndex }>
                                                {
                                                    showAddItemsColumn
                                                    &&
                                                    <div className="wprmprc-collection-actions wprmprc-collection-actions-add-item">
                                                        <div className="wprmprc-collection-action-header">
                                                            <span className="wprmprc-header-link" onClick={ () => {
                                                                let modeOptions = JSON.parse( JSON.stringify( this.state.modeOptions ) );
                                                                modeOptions['add-item'].group = false;

                                                                this.setState({
                                                                    modeOptions,
                                                                });
                                                            } }>{
                                                                '' !== column.name
                                                                ?
                                                                column.name
                                                                :
                                                                __wprm( 'Cancel' )
                                                            }</span>
                                                            <span className="wprmprc-header-link-separator">&gt;</span>
                                                            { __wprm( 'Add Item' ) }
                                                        </div>
                                                        <AddItem
                                                            collections={this.props.collections}
                                                            type={this.props.type}
                                                            collection={this.props.collection}
                                                            addItems={this.state.addItems[ this.state.modeOptions['add-item'].mode ]}
                                                            onChangeAddItems={this.onChangeAddItems.bind(this)}
                                                            options={this.state.modeOptions['add-item']}
                                                            onChangeModeOptions={(options) => {
                                                                let modeOptions = JSON.parse( JSON.stringify( this.state.modeOptions ) );
                                                                modeOptions['add-item'] = options;

                                                                this.setState({
                                                                    modeOptions,
                                                                });
                                                            }}
                                                            onAddItem={(item) => {
                                                                // Emulate dragEnd.
                                                                this.onDragEnd({
                                                                    type: 'RECIPE',
                                                                    destination: {
                                                                        droppableId: `${this.state.modeOptions['add-item'].group.column}-${this.state.modeOptions['add-item'].group.row}`,
                                                                    },
                                                                    source: 'click',
                                                                    item,
                                                                });

                                                                // Cancel adding.
                                                                let modeOptions = JSON.parse( JSON.stringify( this.state.modeOptions ) );
                                                                modeOptions['add-item'].group = false;

                                                                this.setState({
                                                                    modeOptions,
                                                                });
                                                            }}
                                                            interface="click"
                                                        />
                                                    </div>
                                                }
                                                <div className="wprmprc-collection-column" style={ style }>
                                                    {
                                                        '' !== column.name
                                                        &&
                                                        <div className="wprmprc-collection-column-header">
                                                            <div className="wprmprc-collection-column-name">
                                                                {column.name}
                                                            </div>
                                                        </div>
                                                    }
                                                    <div className="wprmprc-collection-column-groups">
                                                        {
                                                            collection.groups.map( (group, groupIndex) => {
                                                                const groupItems = collection.items[`${column.id}-${group.id}`] ? collection.items[`${column.id}-${group.id}`] : [];
                                                                itemsInColumn = [
                                                                    ...itemsInColumn,
                                                                    ...groupItems,
                                                                ];
    
                                                                const onDeleteItem = 'remove-items' === this.state.mode ? (id, index) => {
                                                                    this.onDeleteItem(column.id, group.id, index);
                                                                } : false;
    
                                                                return (
                                                                    <Group
                                                                        mode={ this.state.mode }
                                                                        type={this.props.type}
                                                                        collection={collection}
                                                                        group={group}
                                                                        items={groupItems}
                                                                        onDeleteItem={onDeleteItem}
                                                                        onChangeAmount={(index, amount) => {
                                                                            this.onChangeItem( column.id, group.id, index, 'amount', amount );
                                                                        }}
                                                                        onChangeServings={(index, servings) => {
                                                                            this.onChangeItem( column.id, group.id, index, 'servings', servings );
                                                                        }}
                                                                        onAddItem={() => {
                                                                            let modeOptions = JSON.parse( JSON.stringify( this.state.modeOptions ) );
                                                                            modeOptions['add-item'].group = {
                                                                                column: columnIndex,
                                                                                row: groupIndex,
                                                                            };

                                                                            this.setState({
                                                                                modeOptions,
                                                                            });
                                                                        }}
                                                                        index={`${columnIndex}-${groupIndex}`}
                                                                        key={`${columnIndex}-${groupIndex}`}
                                                                    />
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                    {
                                                        showNutrition
                                                        &&
                                                        <Nutrition
                                                            items={itemsInColumn}
                                                            recipes={this.props.recipes}
                                                            onUpdateRecipes={this.props.onUpdateRecipes}
                                                        />
                                                    }
                                                </div>
                                            </Fragment>
                                        )
                                    } )
                                }
                                <Actions
                                    collections={this.props.collections}
                                    collection={collection}
                                    type={this.props.type}
                                    mode={this.state.mode}
                                    modeOptions={this.state.modeOptions}
                                    columns={collection.columns}
                                    groups={collection.groups}
                                    addItems={this.state.addItems[ this.state.modeOptions['add-item'].mode ]}
                                    onChangeColumns={(columns) => this.props.onChangeCollection(this.props.type, collection.id, { columns })}
                                    onChangeGroups={(groups) => this.props.onChangeCollection(this.props.type, collection.id, { groups })}
                                    onChangeAddItems={this.onChangeAddItems.bind(this)}
                                    onChangeMode={(mode) => {
                                        let modeOptions = JSON.parse( JSON.stringify( this.state.modeOptions ) );
                                        modeOptions['add-item'].group = false;

                                        this.setState({
                                            mode,
                                            modeOptions,
                                        });
                                    }}
                                    onChangeModeOptions={this.onChangeModeOptions.bind(this)}
                                    showNutrition={showNutrition}
                                    onChangeShowNutrition={(showNutrition) => {
                                        // Store value in collection unless displaying a saved collection in frontend.
                                        if ( 'saved' === this.props.type ) {
                                            this.setState({
                                                showNutrition,
                                            });
                                        } else {
                                            this.props.onChangeCollection(this.props.type, this.props.collection.id, { showNutrition } );
                                        }
                                    }}
                                    onPrint={() => {
                                        this.setState({
                                            print: true,
                                        });
                                    }}
                                    onPrintRecipes={this.onPrintRecipes}
                                />
                            </DragDropContext>
                            <div className="wprmprc-collection-column-balancer"></div>
                            <div className="wprmprc-collection-column-balancer"></div>
                            <div className="wprmprc-collection-column-balancer"></div>
                            <div className="wprmprc-collection-column-balancer"></div>
                            <div className="wprmprc-collection-column-balancer"></div>
                            <div className="wprmprc-collection-column-balancer"></div>
                            <div className="wprmprc-collection-column-balancer"></div>
                        </div>
                    }
                </div>
                {
                    this.state.print
                    && <Print onFinished={() => this.setState({ print: false }) } print={this.printCollection} className="wprmprc-collection" />
                }
            </Fragment>
        );
    }
}

export default withRouter(Collection);
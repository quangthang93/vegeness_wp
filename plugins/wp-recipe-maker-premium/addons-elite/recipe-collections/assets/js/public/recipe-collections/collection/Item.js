import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { Draggable } from 'react-beautiful-dnd';

import { __wprm } from 'Shared/Translations';

import Icon from '../general/Icon';

import '../../../../css/public/item.scss';

class Item extends Component {
    render() {
        const { item, index } = this.props;
        const addInterface = this.props.hasOwnProperty( 'interface' ) ? this.props.interface : 'drag';

        // Allow clicking on item.
        const clickSetting = 'admin' === this.props.type ? 'parent' : wprmprc_public.settings.recipe_collections_recipe_click;
        let allowClick = this.props.allowClick && 'disabled' !== clickSetting;

        switch ( clickSetting ) {
            case 'disabled':
                allowClick = false;
                break;
            case 'parent':
                if ( ! item.parent_url ) {
                    allowClick = false;
                }
                break;
        }

        // Item classes.
        let itemClass = 'wprmprc-collection-item';
        itemClass += ` wprmprc-collection-item-${wprmprc_public.settings.recipe_collections_recipe_style}`;
        itemClass += ` wprmprc-collection-item-${item.type}`;
        
        if ( item.hasOwnProperty( 'color' ) ) {
            itemClass += ` wprmprc-collection-item-color-${item.color}`;
        }
        if ( item.hasOwnProperty( 'image' ) && item.image ) {
            itemClass += ` wprmprc-collection-item-has-image`;
        }

        // Ingredient Text
        let ingredientText = false;
        if ( 'ingredient' === item.type ) {
            ingredientText = '';
            item.ingredients.map((ingredient, index) => {
                if ( 0 < index ) { ingredientText += '\n'; }
                if ( ingredient.amount ) { ingredientText += `${ ingredient.amount } `; }
                if ( ingredient.unit ) { ingredientText += `${ ingredient.unit } `; }
                if ( ingredient.name ) { ingredientText += `${ ingredient.name }`; }

                ingredientText = ingredientText.trim();
            });
        }

        //
        let changeServings = false;
        if ( this.props.hasOwnProperty( 'onChangeServings' ) ) {
            changeServings = (e, index, plus) => {
                e.preventDefault();
    
                const servings = plus ? item.servings + 1 : item.servings - 1;
                this.props.onChangeServings(index, servings);
    
                return false;
            }
        }

        return (
            <Draggable
                draggableId={`${item.id}`}
                index={index}
                type='RECIPE'
                isDragDisabled={'saved' === this.props.type}
            >
                {(provided, snapshot) => (
                    <div
                        className={ itemClass }
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                    >
                        {
                            'saved' !== this.props.type
                            &&
                            <div className="wprmprc-collection-item-actions">
                                <div
                                    className="wprmprc-collection-item-action wprmprc-collection-item-action-order"
                                    style={ this.props.onDeleteItem || 'drag' !== addInterface ? { display: 'none' } : {}}
                                    {...provided.dragHandleProps}
                                ><Icon type="drag" /></div>
                                {
                                    'click' === addInterface
                                    &&
                                    <div
                                        className="wprmprc-collection-item-action wprmprc-collection-item-action-add"
                                        onClick={() => {
                                            if ( this.props.onAddItem ) {
                                                this.props.onAddItem( item );
                                            }
                                        }}
                                    ><Icon type="plus" /></div>
                                }
                                {
                                    this.props.onDeleteItem
                                    &&
                                    <div
                                        className="wprmprc-collection-item-action wprmprc-collection-item-action-delete"
                                        onClick={() => this.props.onDeleteItem(item.id, index)}
                                    ><Icon type="delete" /></div>
                                }
                            </div>
                        }
                        <div
                            className={`wprmprc-collection-item-details${ allowClick ? ' wprmprc-collection-item-details-allow-click' : ''}`}
                            onClick={(e) => {
                                if ( 'recipe' === item.type && allowClick ) {
                                    switch ( clickSetting ) {
                                        case 'recipe':
                                            if ( 'inbox' === this.props.type ) {
                                                this.props.history.push(`/collection/inbox/${item.recipeId}`);
                                            } else {
                                                this.props.history.push(`/collection/${this.props.type}/${this.props.collection.id}/${item.recipeId}`);
                                            }
                                            break;
                                        case 'parent':
                                            if ( 'admin' === this.props.type || e.metaKey || wprmprc_public.settings.recipe_collections_recipe_click_new_tab ) {
                                                window.open( item.parent_url );
                                            } else {
                                                location.href = item.parent_url;
                                            }
                                            break;
                                    }
                                }
                            }}
                        >
                            <div className="wprmprc-collection-item-name">
                                {
                                    'ingredient' === item.type
                                    && ingredientText
                                    ?
                                    <Fragment>
                                        {
                                            '' !== item.name
                                            &&
                                            <Fragment>
                                                <strong>{ item.name }</strong><br/>
                                            </Fragment>
                                        }
                                        { ingredientText }
                                    </Fragment>
                                    :
                                    <Fragment>
                                        {
                                            'nutrition-ingredient' === item.type
                                            ?
                                            <Fragment>
                                                {
                                                    this.props.hasOwnProperty( 'onChangeAmount' )
                                                    ?
                                                    <a
                                                        href="#"
                                                        onClick={ (e) => {
                                                            e.preventDefault();
                                                            let amount = prompt( __wprm( 'Set a new amount for this ingredient:' ), item.amount ).trim();
                                                            if( amount ) {
                                                                this.props.onChangeAmount( index, parseFloat( amount ) );
                                                            }
                                                        }}
                                                    >{ `${item.amount}${ item.unit ? ` ${ item.unit }` : '' }` }</a>
                                                    :
                                                    <Fragment>{ `${item.amount}${ item.unit ? ` ${ item.unit }` : '' }` }</Fragment>
                                                } { item.name }
                                            </Fragment>
                                            :
                                            item.name
                                        }
                                    </Fragment>
                                }
                            </div>
                            {
                                item.image
                                &&
                                <div className="wprmprc-collection-item-image">
                                    <img src={item.image} alt={ item.name } />
                                </div>
                            }
                        </div>
                        {
                            'note' !== item.type
                            && item.hasOwnProperty( 'servings' )
                            &&
                            <div className="wprmprc-collection-item-servings">
                                {
                                    false !== changeServings
                                    ?
                                    <Fragment>
                                        <div className="wprmprc-collection-item-servings-minus" onClick={(e) => changeServings(e, index, false)}><Icon type="minus" /></div>
                                        <div
                                            className="wprmprc-collection-item-servings-value wprmprc-collection-item-servings-value-click"
                                            onClick={(e) => {
                                                e.preventDefault();
                                                let servings = prompt( __wprm( 'Set the number of servings' ), item.servings ).trim();
                                                if( servings ) {
                                                    this.props.onChangeServings( index, parseFloat( servings ) );
                                                }
                                            }}
                                        >{item.servings}</div>
                                        <div className="wprmprc-collection-item-servings-plus" onClick={(e) => changeServings(e, index, true)}><Icon type="plus" /></div>
                                    </Fragment>
                                    :
                                    <div className="wprmprc-collection-item-servings-value">{item.servings}</div>
                                }
                            </div>
                        }
                    </div>
                )}
            </Draggable>
        );
    }
}

export default withRouter(Item);
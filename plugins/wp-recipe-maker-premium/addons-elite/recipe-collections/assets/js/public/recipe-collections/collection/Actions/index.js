import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';

import { __wprm } from 'Shared/Translations';

import AddItem from './AddItem';
import ColumnsGroups from './ColumnsGroups';
import SaveCollection from './SaveCollection';

import '../../../../../css/public/actions.scss';

class Actions extends Component {
    render() {        
        return (
            <div className={`wprmprc-collection-actions wprmprc-collection-actions-${this.props.mode}`}>
                {
                    'overview' === this.props.mode
                    &&
                    <Fragment>
                        {
                            'saved' === this.props.type
                            ?
                            <SaveCollection
                                collection={this.props.collection}
                            />
                            :
                            <Fragment>
                                <div className="wprmprc-collection-action" onClick={ () => this.props.onChangeMode('add-item' ) }>{ __wprm( 'Add Item' ) }</div>
                                <div className="wprmprc-collection-action" onClick={ () => this.props.onChangeMode('remove-items' ) }>{ __wprm( 'Remove Items' ) }</div>
                                {
                                    'inbox' !== this.props.type
                                    && <div className="wprmprc-collection-action" onClick={ () => this.props.onChangeMode('columns-groups' ) }>{ __wprm( 'Columns & Groups' ) }</div>
                                }
                            </Fragment>
                        }
                        {
                            wprmprc_public.settings.recipe_collections_nutrition_facts && 0 < wprmprc_public.settings.recipe_collections_nutrition_facts_fields.length
                            &&
                            <div
                                className="wprmprc-collection-action"
                                onClick={() => {
                                    this.props.onChangeShowNutrition( ! this.props.showNutrition );
                                }}
                            >{ this.props.showNutrition ? __wprm( 'Hide Nutrition Facts' ) : __wprm( 'Show Nutrition Facts' ) }</div>
                        }
                        {
                            wprmprc_public.settings.recipe_collections_print
                            &&
                            <div
                                className="wprmprc-collection-action"
                                onClick={() => {
                                    this.props.onPrint();
                                }}
                            >{ __wprm( 'Print Collection' ) }</div>
                        }
                        {
                            wprmprc_public.settings.recipe_collections_print_recipes
                            &&
                            <div
                                className="wprmprc-collection-action"
                                onClick={() => {
                                    this.props.onPrintRecipes();
                                }}
                            >{ __wprm( 'Print Recipes' ) }</div>
                        }
                        {
                            wprmprc_public.settings.recipe_collections_shopping_list
                            &&
                            'admin' !== this.props.type
                            &&
                            <div
                                className="wprmprc-collection-action"
                                onClick={() => {
                                    if ( 'inbox' === this.props.type ) {
                                        this.props.history.push(`/shopping-list/inbox/`);
                                    } else {
                                        this.props.history.push(`/shopping-list/${this.props.type}/${this.props.collection.id}`);
                                    }
                                }}
                            >{ __wprm( 'Shopping List' ) }</div>
                        }
                    </Fragment>
                }
                {
                    'remove-items' === this.props.mode
                    &&
                    <div className="wprmprc-collection-action" onClick={ () => this.props.onChangeMode('overview' ) }>{ __wprm( 'Stop Removing Items' ) }</div>
                }
                {
                    'add-item' === this.props.mode
                    &&
                    <Fragment>
                        <div className="wprmprc-collection-action-header">
                            <span className="wprmprc-header-link" onClick={ () => this.props.onChangeMode('overview') }>{ __wprm( 'Actions' ) }</span>
                            <span className="wprmprc-header-link-separator">&gt;</span>
                            { __wprm( 'Add Item' ) }
                        </div>
                        <AddItem
                            collections={this.props.collections}
                            type={this.props.type}
                            collection={this.props.collection}
                            addItems={this.props.addItems}
                            onChangeAddItems={this.props.onChangeAddItems}
                            options={this.props.modeOptions['add-item']}
                            onChangeModeOptions={this.props.onChangeModeOptions}
                            interface="drag"
                        />
                    </Fragment>
                }
                {
                    'inbox' !== this.props.type && 'columns-groups' === this.props.mode
                    &&
                    <Fragment>
                        <div className="wprmprc-collection-action-header">
                            <span className="wprmprc-header-link" onClick={ () => this.props.onChangeMode('overview') }>{ __wprm( 'Actions' ) }</span>
                            <span className="wprmprc-header-link-separator">&gt;</span>
                            { __wprm( 'Columns & Groups' ) }
                        </div>
                        <ColumnsGroups
                            columns={this.props.columns}
                            onChangeColumns={this.props.onChangeColumns}
                            groups={this.props.groups}
                            onChangeGroups={this.props.onChangeGroups}
                        />
                    </Fragment>
                }
            </div>
        );
    }
}

export default withRouter( Actions );
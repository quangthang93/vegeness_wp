import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';

import { __wprm } from 'Shared/Translations';
import Api from '../general/Api';
import Loader from '../general/Loader';
import AddToCollection from '../../add-to-collection';

import '../../../../css/public/recipe.scss';
class Recipe extends Component {

    constructor(props) {
        super(props);

        if ( ! props.recipe.hasOwnProperty('html') || ! props.recipe.html ) {
            this.getRecipeHtml(props.recipe.id);
        }

        this.initRecipeFeatures = this.initRecipeFeatures.bind(this);
    }

    componentDidMount() {
        AddToCollection.checkInbox(this.props.recipe.id);
        this.initRecipeFeatures();
    }

    componentDidUpdate() {
        AddToCollection.checkInbox(this.props.recipe.id);
        this.initRecipeFeatures();
    }

    initRecipeFeatures() {
        if ( this.props.recipe.html ) {
            if ( window.WPRecipeMaker && window.WPRecipeMaker.hasOwnProperty( 'quantities' ) ) {
                window.WPRecipeMaker.quantities.init();
            }

            if ( window.WPRecipeMaker && window.WPRecipeMaker.hasOwnProperty( 'timer' ) ) {
                window.WPRecipeMaker.timer.init();
            }
        }
    }

    getRecipeHtml(recipeId) {
        Api.getRecipe(recipeId).then((recipe) => {
            let recipes = {}
            recipes[recipeId] = recipe;

            this.props.onUpdateRecipes(recipes);
        });
    }

    render() {
        const { type, collection, shoppingList, recipe } = this.props;

        if ( recipe && recipe.javascript ) {
            for ( let key of Object.keys( recipe.javascript ) ) {
                window[key] = recipe.javascript[ key ];
            }
        }
        
        return (
            <Fragment>
                <div className="wprmprc-container-header">
                    <span className="wprmprc-header-link"
                        onClick={() => {
                            if ( 'shopping' === type ) {
                                this.props.history.push(`/shopping-list/${shoppingList}`);
                            } else if ( 'inbox' === type ) {
                                this.props.history.push(`/collection/inbox/`);
                            } else {
                                this.props.history.push(`/collection/${type}/${collection.id}`);
                            }
                        }}
                    >{ collection ? collection.name : __wprm( 'Go Back' ) }</span>
                    <span className="wprmprc-header-link-separator">&gt;</span>
                    { __wprm( 'Recipe' ) }
                </div>
                <div className="wprmprc-recipe">
                    {
                        recipe.html
                        ?
                        <div dangerouslySetInnerHTML={{__html: recipe.html}} />
                        :
                        <Loader />
                    }
                </div>
            </Fragment>
        );
    }
}

export default withRouter(Recipe);
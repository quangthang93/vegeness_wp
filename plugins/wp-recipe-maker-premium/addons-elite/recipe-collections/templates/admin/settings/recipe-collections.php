<?php
/**
 * Template for the Recipe Collections settings sub page.
 *
 * @link       http://bootstrapped.ventures
 * @since      4.1.0
 *
 * @package    WP_Recipe_Maker_Premium/addons-elite/recipe-collections
 * @subpackage WP_Recipe_Maker_Premium/addons-elite/recipe-collections/templates/admin/settings
 */

$nutrition_fields = array(
	'calories' => __( 'Calories', 'wp-recipe-maker-premium' ),
	'carbohydrates' => __( 'Carbohydrates', 'wp-recipe-maker-premium' ),
	'protein' => __( 'Protein', 'wp-recipe-maker-premium' ),
	'fat' => __( 'Fat', 'wp-recipe-maker-premium' ),
);

if ( class_exists( 'WPRM_Nutrition' ) ) {
	$all_fields = WPRM_Nutrition::get_fields();
	$nutrition_fields = array_map( function( $nutrient ) { return $nutrient['label']; }, $all_fields );

	unset( $nutrition_fields['serving_size'] );
}

$recipe_collections = array(
	'id' => 'recipeCollections',
	'icon' => 'book',
	'name' => __( 'Recipe Collections', 'wp-recipe-maker' ),
	'required' => 'elite',
	'subGroups' => array(
		array(
			'name' => '',
			'description' => __( 'Add the Recipe Collections block or [wprm-recipe-collections] shortcode to a regular WordPress page to display the Recipe Collections feature.', 'wp-recipe-maker-premium' ),
			'documentation' => 'https://help.bootstrapped.ventures/article/148-recipe-collections',
			'settings' => array(
				array(
					'id' => 'recipe_collections_link',
					'name' => __( 'Link to Collections feature', 'wp-recipe-maker' ),
					'description' => __( "Full URL of the page where you've added the Recipe Collections shortcode.", 'wp-recipe-maker' ),
					'type' => 'text',
					'default' => '',
				),
				array(
					'id' => 'recipe_collections_access',
					'name' => __( 'Access to Recipe Collections', 'wp-recipe-maker' ),
					'type' => 'dropdown',
					'options' => array(
						'everyone' => __( 'Everyone', 'wp-recipe-maker' ),
						'logged_in' => __( 'Logged In Users', 'wp-recipe-maker' ),
					),
					'default' => 'everyone',
				),
				array(
					'id' => 'recipe_collections_no_access_message',
					'name' => __( 'No Access Message', 'wp-recipe-maker' ),
					'description' => __( 'Optional text to show instead of the Recipe Collections feature for visitors with no access.', 'wp-recipe-maker' ),
					'type' => 'richTextarea',
					'default' => '',
					'dependency' => array(
						'id' => 'recipe_collections_access',
						'value' => 'logged_in',
					),
				),
			),
		),
		array(
			'name' => __( 'Collections', 'wp-recipe-maker-premium' ),
			'settings' => array(
				array(
					'id' => 'recipe_collections_inbox_name',
					'name' => __( 'Default Inbox Name', 'wp-recipe-maker' ),
					'description' => __( 'Name of the inbox collection that exists for everyone.', 'wp-recipe-maker' ),
					'type' => 'text',
					'default' => __( 'Inbox', 'wp-recipe-maker-premium' ),
				),
				array(
					'id' => 'recipe_collections_recipe_click',
					'name' => __( 'Click on recipe', 'wp-recipe-maker' ),
					'description' => __( 'What happens when clicking on a recipe in the collection.', 'wp-recipe-maker' ),
					'type' => 'dropdown',
					'options' => array(
						'disabled' => __( 'Does nothing', 'wp-recipe-maker' ),
						'recipe' => __( 'Shows the recipe box', 'wp-recipe-maker' ),
						'parent' => __( 'Opens the parent post', 'wp-recipe-maker' ),
					),
					'default' => 'recipe',
				),
				array(
					'id' => 'recipe_collections_template_modern',
					'name' => __( 'Recipe template to show', 'wp-recipe-maker' ),
					'type' => 'dropdownTemplateModern',
					'default' => 'chic',
					'dependency' => array(
						array(
							'id' => 'recipe_template_mode',
							'value' => 'modern',
						),
						array(
							'id' => 'recipe_collections_recipe_click',
							'value' => 'recipe',
						),
					),
				),
				array(
					'id' => 'recipe_collections_template_legacy',
					'name' => __( 'Recipe template to show', 'wp-recipe-maker' ),
					'type' => 'dropdownTemplateLegacy',
					'default' => 'simple',
					'dependency' => array(
						array(
							'id' => 'recipe_template_mode',
							'value' => 'legacy',
						),
						array(
							'id' => 'recipe_collections_recipe_click',
							'value' => 'recipe',
						),
					),
				),
				array(
					'id' => 'recipe_collections_recipe_click_new_tab',
					'name' => __( 'Force recipe click to open in new tab', 'wp-recipe-maker' ),
					'type' => 'toggle',
					'default' => false,
					'dependency' => array(
						'id' => 'recipe_collections_recipe_click',
						'value' => 'parent',
					),
				),
				array(
					'id' => 'recipe_collections_print',
					'name' => __( 'Show "Print Collection" button', 'wp-recipe-maker' ),
					'description' => __( 'This prints the collection over with columns and rows', 'wp-recipe-maker' ),
					'type' => 'toggle',
					'default' => false,
				),
				array(
					'id' => 'recipe_collections_print_recipes',
					'name' => __( 'Show "Print Recipes" button', 'wp-recipe-maker' ),
					'description' => __( 'This prints the recipes used in the collection.', 'wp-recipe-maker' ),
					'type' => 'toggle',
					'default' => false,
				),
				array(
					'id' => 'recipe_collections_print_recipes_template_modern',
					'name' => __( 'Recipe template to print', 'wp-recipe-maker' ),
					'type' => 'dropdownTemplateModern',
					'default' => 'chic',
					'dependency' => array(
						array(
							'id' => 'recipe_template_mode',
							'value' => 'modern',
						),
						array(
							'id' => 'recipe_collections_print_recipes',
							'value' => true,
						),
					),
				),
				array(
					'id' => 'recipe_collections_print_recipes_template',
					'name' => __( 'Recipe template print', 'wp-recipe-maker' ),
					'type' => 'dropdownTemplateLegacy',
					'default' => 'clean',
					'dependency' => array(
						array(
							'id' => 'recipe_template_mode',
							'value' => 'legacy',
						),
						array(
							'id' => 'recipe_collections_print_recipes',
							'value' => true,
						),
					),
				),
			),
		),
		array(
			'name' => __( 'Collection Items', 'wp-recipe-maker-premium' ),
			'settings' => array(
				array(
					'id' => 'recipe_collections_default_add',
					'name' => __( 'Default Add Item Selection', 'wp-recipe-maker-premium' ),
					'description' => __( 'Default selection when adding items to a collection (inbox excluded).', 'wp-recipe-maker-premium' ),
					'type' => 'dropdown',
					'options' => array(
						'collection' => __( 'Add from Collection', 'wp-recipe-maker-premium' ),
						'search' => __( 'Search Recipes', 'wp-recipe-maker-premium' ),
						'ingredient' => __( 'Search Ingredients', 'wp-recipe-maker-premium' ),
						'custom' => __( 'Add Custom Recipe', 'wp-recipe-maker-premium' ),
						'note' => __( 'Add Note', 'wp-recipe-maker-premium' ),
					),
					'default' => 'collection',
				),
				array(
					'id' => 'recipe_collections_items_allow_ingredient',
					'name' => __( 'Allow Ingredients', 'wp-recipe-maker-premium' ),
					'description' => __( 'Allow nutrition ingredients to be added to a collection. These can be added on the WP Recipe Maker > Manage > Your Custom Fields > Custom Nutrition page.', 'wp-recipe-maker-premium' ),
					'type' => 'toggle',
					'default' => false,
				),
				array(
					'id' => 'recipe_collections_items_allow_custom_recipe',
					'name' => __( 'Allow Custom Recipes', 'wp-recipe-maker-premium' ),
					'description' => __( 'Allow custom recipes to be added to a collection.', 'wp-recipe-maker-premium' ),
					'type' => 'toggle',
					'default' => true,
				),
				array(
					'id' => 'recipe_collections_items_allow_note',
					'name' => __( 'Allow Notes', 'wp-recipe-maker-premium' ),
					'description' => __( 'Allow notes to be added to a collection.', 'wp-recipe-maker-premium' ),
					'type' => 'toggle',
					'default' => true,
				),
			),
		),
		array(
			'name' => __( 'Appearance', 'wp-recipe-maker-premium' ),
			'settings' => array(
				array(
					'id' => 'recipe_collections_appearance_font_size',
					'name' => __( 'Base Font Size', 'wp-recipe-maker' ),
					'type' => 'number',
					'suffix' => 'px',
					'default' => '12',
				),
				array(
					'id' => 'recipe_collections_appearance_column_size',
					'name' => __( 'Minimum Column Width', 'wp-recipe-maker' ),
					'type' => 'number',
					'suffix' => 'px',
					'default' => '200',
				),
				array(
					'id' => 'recipe_collections_recipe_style',
					'name' => __( 'Recipe style', 'wp-recipe-maker' ),
					'type' => 'dropdown',
					'options' => array(
						'compact' => __( 'Compact', 'wp-recipe-maker' ),
						'large' => __( 'Large Image', 'wp-recipe-maker' ),
						'overlay' => __( 'Overlay', 'wp-recipe-maker' ),
					),
					'default' => 'compact',
				),
				array(
					'id' => 'recipe_collections_header_color',
					'name' => __( 'Header Color', 'wp-recipe-maker' ),
					'type' => 'color',
					'default' => '#000000',
				),
				array(
					'id' => 'recipe_collections_header_text_color',
					'name' => __( 'Header Text Color', 'wp-recipe-maker' ),
					'type' => 'color',
					'default' => '#ffffff',
				),
				array(
					'id' => 'recipe_collections_button_color',
					'name' => __( 'Button Color', 'wp-recipe-maker' ),
					'type' => 'color',
					'default' => '#d3d3d3',
				),
				array(
					'id' => 'recipe_collections_button_text_color',
					'name' => __( 'Button Text Color', 'wp-recipe-maker' ),
					'type' => 'color',
					'default' => '#666666',
				),
			),
		),
		array(
			'name' => __( 'Nutrition Facts', 'wp-recipe-maker-premium' ),
			'description' => __( 'In each column, show the added totals for the nutrition facts.', 'wp-recipe-maker-premium' ),
			'settings' => array(
				array(
					'id' => 'recipe_collections_nutrition_facts',
					'name' => __( 'Enable button to show nutrition facts', 'wp-recipe-maker-premium' ),
					'type' => 'toggle',
					'default' => false,
				),
				array(
					'id' => 'recipe_collections_nutrition_facts_hidden_default',
					'name' => __( 'Button is enabled by default', 'wp-recipe-maker-premium' ),
					'description' => __( 'When disabled an extra click is required to show the nutrition facts.', 'wp-recipe-maker-premium' ),
					'type' => 'toggle',
					'default' => false,
					'dependency' => array(
						'id' => 'recipe_collections_nutrition_facts',
						'value' => true,
					),
				),
				array(
					'id' => 'recipe_collections_nutrition_facts_count',
					'name' => __( 'Nutrition Facts Totals', 'wp-recipe-maker' ),
					'type' => 'dropdown',
					'options' => array(
						'serving' => __( 'Show totals per serving', 'wp-recipe-maker' ),
						'total' => __( 'Show totals including all servings', 'wp-recipe-maker' ),
					),
					'default' => 'serving',
					'dependency' => array(
						'id' => 'recipe_collections_nutrition_facts',
						'value' => true,
					),
				),
				array(
					'id' => 'recipe_collections_nutrition_facts_fields',
					'name' => __( 'Nutrition fields to show', 'wp-recipe-maker-premium' ),
					'type' => 'dropdownMultiselect',
					'options' => $nutrition_fields,
					'default' => array(
						'calories',
						'carbohydrates',
						'protein',
						'fat',
					),
					'dependency' => array(
						'id' => 'recipe_collections_nutrition_facts',
						'value' => true,
					),
				),
				array(
					'id' => 'recipe_collections_nutrition_facts_round_to_decimals',
					'name' => __( 'Round quantity to', 'wp-recipe-maker' ),
					'description' => __( 'Number of decimals to round a quantity to when adding up nutrition facts.', 'wp-recipe-maker' ),
					'type' => 'number',
					'suffix' => 'decimals',
					'default' => '1',
				),
			),
		),
		array(
			'name' => __( 'Shopping List', 'wp-recipe-maker-premium' ),
			'settings' => array(
				array(
					'id' => 'recipe_collections_shopping_list',
					'name' => __( 'Allow shopping list generation', 'wp-recipe-maker' ),
					'type' => 'toggle',
					'default' => true,
				),
				array(
					'id' => 'recipe_collections_shopping_list_options',
					'name' => __( 'Show shopping list generation options', 'wp-recipe-maker' ),
					'description' => __( 'Allows user to choose how the shopping list gets generated.', 'wp-recipe-maker' ),
					'type' => 'toggle',
					'default' => true,
					'dependency' => array(
						'id' => 'recipe_collections_shopping_list',
						'value' => true,
					),
				),
				array(
					'id' => 'recipe_collections_shopping_list_links',
					'name' => __( 'Ingredient Links', 'wp-recipe-maker' ),
					'description' => __( 'Use ingredient links in the shopping list.', 'wp-recipe-maker' ),
					'type' => 'toggle',
					'default' => true,
					'dependency' => array(
						'id' => 'recipe_collections_shopping_list',
						'value' => true,
					),
				),
				array(
					'id' => 'recipe_collections_shopping_list_round_to_decimals',
					'name' => __( 'Round quantity to', 'wp-recipe-maker' ),
					'description' => __( 'Number of decimals to round a quantity to in the shopping list.', 'wp-recipe-maker' ),
					'type' => 'number',
					'suffix' => 'decimals',
					'default' => '2',
					'dependency' => array(
						'id' => 'recipe_collections_shopping_list',
						'value' => true,
					),
				),
				array(
					'id' => 'recipe_collections_shopping_list_print',
					'name' => __( 'Show print buttons', 'wp-recipe-maker' ),
					'type' => 'toggle',
					'default' => true,
					'dependency' => array(
						'id' => 'recipe_collections_shopping_list',
						'value' => true,
					),
				),
				array(
					'id' => 'recipe_collections_shopping_list_remove',
					'name' => __( "Remove shopping lists that haven't been updated in", 'wp-recipe-maker' ),
					'description' => __( 'Automatically remove inactive shopping lists to save database storage. Mininum of 7 days.', 'wp-recipe-maker' ),
					'type' => 'number',
					'default' => 31,
					'suffix' => __( 'days', 'wp-recipe-maker' ),
					'dependency' => array(
						'id' => 'recipe_collections_shopping_list',
						'value' => true,
					),
				),
			),
		),
		array(
			'name' => __( 'Saved Collections', 'wp-recipe-maker-premium' ),
			'description' => __( 'Create your own collections to display to your visitors.', 'wp-recipe-maker-premium' ),
			'documentation' => 'https://help.bootstrapped.ventures/article/149-saved-recipe-collection',
			'settings' => array(
				array(
					'id' => 'recipe_collections_save_button',
					'name' => __( 'Allow save to own collections', 'wp-recipe-maker' ),
					'type' => 'toggle',
					'default' => true,
				),
			),
		),
	),
);

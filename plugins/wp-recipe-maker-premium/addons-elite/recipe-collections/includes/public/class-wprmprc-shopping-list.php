<?php
/**
 * Handle the shopping list for collections.
 *
 * @link       http://bootstrapped.ventures
 * @since      6.3.0
 *
 * @package    WP_Recipe_Maker_Premium/addons-elite/recipe-collections
 * @subpackage WP_Recipe_Maker_Premium/addons-elite/recipe-collections/includes/public
 */

/**
 * Handle the shopping list for collections.
 *
 * @since      6.3.0
 * @package    WP_Recipe_Maker_Premium/addons-elite/recipe-collections
 * @subpackage WP_Recipe_Maker_Premium/addons-elite/recipe-collections/includes/public
 * @author     Brecht Vandersmissen <brecht@bootstrapped.ventures>
 */
class WPRMPRC_Shopping_List {

	/**
	 * Get a shopping list.
	 *
	 * @since    6.3.0
	 * @param    mixed $uid	UID of the shopping list to get.
	 */
	public static function get( $uid ) {
		$shopping_list = false;

		$data = WPRMPRC_Shopping_List_Database::get( $uid );	

		if ( is_array( $data ) ) {
			return array(
				'created' => $data['created'],
				'updated' => $data['updated'],
				'collection' => maybe_unserialize( $data['collection'] ),
				'groups' => maybe_unserialize( $data['groups'] ),
			);
		}

		return $shopping_list;
	}

	/**
	 * Get a shopping list by collection and user.
	 *
	 * @since    6.3.0
	 * @param    int $user_id			ID of the user.
	 * @param    int $collection_id		ID of the collection.
	 * @param    mixed $collection_type	Type of the collection.
	 */
	public static function get_uid_by_collection_and_user( $user_id, $collection_id, $collection_type ) {
		$data = WPRMPRC_Shopping_List_Database::get_by_collection_and_user( $user_id, $collection_id, $collection_type );

		if ( is_array( $data ) ) {
			return $data['uid'];
		}

		return false;
	}

	/**
	 * Save a shopping list.
	 *
	 * @since    6.3.0
	 * @param    mixed $uid		UID of the shopping list to get.
	 * @param    mixed $data	Data to save in the shopping list.
	 */
	public static function save( $uid, $data ) {
		// Make sure the data we need is actually set.
		if ( isset( $data['groups'] ) ) {
			$old_data = WPRMPRC_Shopping_List_Database::get( $uid );

			if ( is_array( $old_data ) ) {
				return WPRMPRC_Shopping_List_Database::update( $old_data['id'], $data );
			}
		}

		return false;
	}

	/**
	 * Generate a shopping list.
	 *
	 * @since    6.3.0
	 * @param    mixed $type		Type of collection to generate the shopping list for.
	 * @param    mixed $collection	Collection to generate the shopping list for.
	 * @param    mixed $options		Options for the shopping list.
	 */
	public static function generate( $type, $collection, $options ) {
		$items = self::get_items( $collection );
		$ingredients = self::get_ingredients( $items, $options );
		$groups = self::get_groups( $ingredients, $options );

		$data = array(
			'collection_id' => $collection['id'],
			'collection_type' => $type,
			'collection' => $collection,
			'groups' => $groups,
		);
		
		return WPRMPRC_Shopping_List_Database::create( $data );
	}

	/**
	 * Get items to show in shopping list.
	 *
	 * @since    6.3.0
	 * @param    mixed $collection	Collection to get the items from.
	 */
	public static function get_items( $collection ) {
		$items = array();

		foreach ( $collection['columns'] as $column ) {
			// Check if this column is part of the shopping list.
			$in_shopping_list = false;

			if ( isset( $column['inShoppingList'] ) ) {
				$in_shopping_list = $column['inShoppingList'];
			} else {
				$in_shopping_list = 1 === count( $collection['columns'] );
			}
			if ( ! $in_shopping_list ) { continue; }

			foreach ( $collection['groups'] as $group ) {
				$group_items = isset( $collection['items'][ $column['id'] . '-' . $group['id'] ] ) ? $collection['items'][ $column['id'] . '-' . $group['id'] ] : array();

				// Only items with more than 0 servings.
				$group_items = array_filter( $group_items, function( $item ) {
					return 0 < intval( $item['servings'] );
				} );

				$items = array_merge( $items, $group_items );
			}
		}

		return $items;
	}

	/**
	 * Get ingredients to show in shopping list.
	 *
	 * @since    6.3.0
	 * @param    mixed $items	Items to get the ingredients from.
	 * @param    mixed $options	Options for the shopping list.
	 */
	public static function get_ingredients( $items, $options ) {
		$ingredients = array();

		$unit_system = isset( $options['system'] ) ? max( array( 1, intval( $options['system'] ) ) ) : 1;

		foreach ( $items as $item ) {
			$item_servings = intval( $item['servings'] );

			switch ( $item['type'] ) {
				case 'recipe':
					$recipe = WPRM_Recipe_Manager::get_recipe( $item['recipeId'] );

					if ( $recipe ) {
						$recipe_servings = intval( $recipe->servings() ) ? intval( $recipe->servings() ) : 1;
						$recipe_ingredients = $recipe->ingredients_without_groups();

						foreach ( $recipe_ingredients as $ingredient ) {
							$amount = WPRM_Recipe_Parser::parse_quantity( $ingredient['amount'] );
							$unit = wp_strip_all_tags( strip_shortcodes( $ingredient['unit'] ) );
							$name = wp_strip_all_tags( strip_shortcodes( $ingredient['name'] ) );
							$notes = wp_strip_all_tags( strip_shortcodes( $ingredient['notes'] ) );

							// Use different unit system.
							if ( 1 !== $unit_system && isset( $ingredient['converted'] ) && isset( $ingredient['converted'][ $unit_system ] ) ) {
								$converted_amount = isset( $ingredient['converted'][ $unit_system ]['amount'] ) ? wp_strip_all_tags( strip_shortcodes( $ingredient['converted'][ $unit_system ]['amount'] ) ) : '';
								$converted_unit = isset( $ingredient['converted'][ $unit_system ]['unit'] ) ? wp_strip_all_tags( strip_shortcodes( $ingredient['converted'][ $unit_system ]['unit'] ) ) : '';

								// Only if at least amount or unit was set.
								if ( $converted_amount || $converted_amount ) {
									$amount = WPRM_Recipe_Parser::parse_quantity( $converted_amount );
									$unit = $converted_unit;
								}
							}

							// Adjust amount with serving size.
							if ( $item_servings !== $recipe_servings ) {
								$amount = $amount / $recipe_servings * $item_servings;
							}

							$ingredients[] = array(
								'id' => $ingredient['id'],
								'amount' => $amount,
								'unit' => $unit,
								'name' => $name,
								'notes' => $notes,
							);
						}
					}
					break;
				case 'ingredient':
					foreach ( $item['ingredients'] as $ingredient ) {
						$amount = WPRM_Recipe_Parser::parse_quantity( $ingredient['amount'] );
						$unit = trim( $ingredient['unit'] );
						$name = trim( $ingredient['name'] );

						$amount = $item_servings * $amount;

						$ingredients[] = array(
							'id' => WPRM_Recipe_Sanitizer::get_ingredient_id( $name ),
							'amount' => $amount,
							'unit' => $unit,
							'name' => $name,
							'notes' => '',
						);
					}
					break;
				case 'nutrition-ingredient':
					$amount = WPRM_Recipe_Parser::parse_quantity( $item['amount'] );
					$unit = trim( $item['unit'] );
					$name = trim( $item['name'] );

					$amount = $item_servings * $amount;

					$ingredients[] = array(
						'id' => WPRM_Recipe_Sanitizer::get_ingredient_id( $name ),
						'amount' => $amount,
						'unit' => $unit,
						'name' => $name,
						'notes' => '',
					);
					break;
			}
		}

		return $ingredients;
	}

	/**
	 * Get groups to show in shopping list.
	 *
	 * @since    6.3.0
	 * @param    mixed $ingredients	Ingredients to get the groups for.
	 * @param    mixed $options	Options for the shopping list.
	 */
	public static function get_groups( $ingredients, $options ) {
		$groups = array();
		$ingredient_id = 0;

		// Group all ingredients.
		foreach ( $ingredients as $ingredient ) {
			$group_name = WPRMPRC_Ingredient_Groups::get_group( $ingredient['id'] );

			// Make sure group exists.
			$group_index = array_search( $group_name, array_column( $groups, 'name' ) );
			if ( false === $group_index ) {
				$group_index = count( $groups );
				$groups[] = array(
					'id' => $group_index,
					'name' => $group_name,
					'ingredients' => array(),
				);
			}

			// Make sure ingredient line exists.
			$ingredient_name = $ingredient['name'];

			if ( isset( $options['notes'] ) && $options['notes'] ) {
				$notes = trim( $ingredient['notes'] );

				if ( $notes ) {
					$ingredient_name .= ' (' . $notes . ')';
				}
			}

			$ingredient_index = array_search( $ingredient_name, array_column( $groups[ $group_index ]['ingredients'], 'name' ) );
			if ( false === $ingredient_index ) {
				// Get link for this ingredient if enabled.
				if ( WPRM_Settings::get( 'recipe_collections_shopping_list_links') ) {
					$link = WPRMP_Ingredient_Links::get_ingredient_link( $ingredient['id'] );
				}

				$ingredient_index = count( $groups[ $group_index ]['ingredients'] );
				$groups[ $group_index ]['ingredients'][] = array(
					'id' => $ingredient_id,
					'checked' => false,
					'name' => $ingredient_name,
					'link' => $link,
					'variations' => array(),
				);

				$ingredient_id++;
			}

			// Check if unit exists.
			$unit_index = array_search( $ingredient['unit'], array_column( $groups[ $group_index ]['ingredients'][ $ingredient_index ]['variations'], 'unit' ) );

			if ( false === $unit_index ) {
				$groups[ $group_index ]['ingredients'][ $ingredient_index ]['variations'][] = array(
					'amount' => $ingredient['amount'],
					'unit' => $ingredient['unit'],
				);
			} else {
				$groups[ $group_index ]['ingredients'][ $ingredient_index ]['variations'][ $unit_index ]['amount'] += $ingredient['amount'];
			}
		}

		// Format all amounts.
		foreach ( $groups as $g => $group ) {

			// Order ingredients by name.
			usort( $groups[ $g ]['ingredients'], function($a, $b) {
				return strcasecmp( $a['name'], $b['name'] );
			} );

			foreach ( $group['ingredients'] as $i => $ingredient ) {
				foreach ( $ingredient['variations'] as $v => $variation ) {
					$decimals = WPRM_Settings::get( 'recipe_collections_shopping_list_round_to_decimals' );
					$groups[ $g ]['ingredients'][ $i ]['variations'][ $v ]['amount'] = WPRM_Recipe_Parser::format_quantity( $groups[ $g ]['ingredients'][ $i ]['variations'][ $v ]['amount'], $decimals );
				}
			}
		}

		// Order groups by name (with empty first).
		usort( $groups, function($a, $b) {
			if ( '' === $a['name'] ) { return -1; }
			if ( '' === $b['name'] ) { return 1; }

			return strcasecmp( $a['name'], $b['name'] );
		} );

		return $groups;
	}
}

<?php
/**
 * Handle the Recipe Collections assets.
 *
 * @link       http://bootstrapped.ventures
 * @since      4.1.0
 *
 * @package    WP_Recipe_Maker_Premium/addons-elite/recipe-collections
 * @subpackage WP_Recipe_Maker_Premium/addons-elite/recipe-collections/includes/public
 */

/**
 * Handle the Recipe Collections assets.
 *
 * @since      4.1.0
 * @package    WP_Recipe_Maker_Premium/addons-elite/recipe-collections
 * @subpackage WP_Recipe_Maker_Premium/addons-elite/recipe-collections/includes/public
 * @author     Brecht Vandersmissen <brecht@bootstrapped.ventures>
 */
class WPRMPRC_Assets {

	/**
	 * Wether or not the assets should get loaded on this page.
	 *
	 * @since    6.3.0
	 * @access   private
	 * @var      boolean $load_assets Wether or not the assets should get loaded on this page.
	 */
	private static $load_assets = false;

	/**
	 * Register actions and filters.
	 *
	 * @since	4.1.0
	 */
	public static function init() {
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue' ) );
		add_action( 'wp_footer', array( __CLASS__, 'custom_css' ), 99 );
		add_action( 'admin_head', array( __CLASS__, 'custom_css_admin' ) );

		add_filter( 'wprmp_localize_public', array( __CLASS__, 'localize_public_data' )  );
		add_filter( 'wprm_translations_public', array( __CLASS__, 'public_translations' ) );
	}

	/**
	 * Enqueue the public assets.
	 *
	 * @since	4.2.0
	 */
	public static function enqueue() {
		wp_register_style( 'wprmprc-public', WPRMP_URL . 'dist/public-recipe-collections.css', array(), WPRMP_VERSION, 'all' );
		wp_register_script( 'wprmprc-public', WPRMP_URL . 'dist/public-recipe-collections.js', array( 'wprmp-public' ), WPRMP_VERSION, true );
	}

	/**
	 * Actually load the public assets.
	 *
	 * @since	5.5.0
	 */
	public static function load() {
		// Make sure regular recipe assets are loaded as well.
		WPRM_Assets::load();

		self::$load_assets = true;

		wp_enqueue_style( 'wprmprc-public' );
		wp_enqueue_script( 'wprmprc-public' );
	}

	/**
	 * Filter the public translations.
	 *
	 * @since	5.9.0
	 */
	public static function public_translations( $public_translations ) {
		require( WPRMPRC_DIR . 'templates/translations.php' );
		$translations = $translations ? $translations : array();

		return array_merge( $public_translations , $translations );
	}

	/**
	 * Localize the public JS file.
	 *
	 * @since	4.1.0
	 */
	public static function localize_public_data( $data ) {
		$data['endpoints']['collections'] = rtrim( get_rest_url( null, 'wp/v2/' . WPRMPRC_POST_TYPE ), '/' );
		$data['endpoints']['collections_helper'] = rtrim( get_rest_url( null, 'wp-recipe-maker/v1/recipe-collections' ), '/' );
		$data['collections'] = array(
			'default' => WPRMPRC_Manager::get_default_collections(),
		);
		$data['user'] = get_current_user_id();

		return $data;
	}

	/**
	 * Data for localizing the shortcode.
	 *
	 * @since	4.1.0
	 */
	public static function localize_shortcode_data( $include_user_collections = false, $specific_user = false ) {
		$current_user = get_current_user_id();

		$data = array(
			'user' => $current_user,
			'collections_user' => $specific_user ? $specific_user : $current_user,
			'settings' => array(
				'recipe_collections_link' => WPRM_Settings::get( 'recipe_collections_link' ),
				'recipe_collections_print' => WPRM_Settings::get( 'recipe_collections_print' ),
				'recipe_collections_print_recipes' => WPRM_Settings::get( 'recipe_collections_print_recipes' ),
				'recipe_collections_recipe_style' => WPRM_Settings::get( 'recipe_collections_recipe_style' ),
				'recipe_collections_recipe_click' => WPRM_Settings::get( 'recipe_collections_recipe_click' ),
				'recipe_collections_recipe_click_new_tab' => WPRM_Settings::get( 'recipe_collections_recipe_click_new_tab' ),
				'recipe_collections_default_add' => WPRM_Settings::get( 'recipe_collections_default_add' ),
				'recipe_collections_items_allow_ingredient' => WPRM_Settings::get( 'recipe_collections_items_allow_ingredient' ),
				'recipe_collections_items_allow_custom_recipe' => WPRM_Settings::get( 'recipe_collections_items_allow_custom_recipe' ),
				'recipe_collections_items_allow_note' => WPRM_Settings::get( 'recipe_collections_items_allow_note' ),
				'recipe_collections_nutrition_facts' => WPRM_Settings::get( 'recipe_collections_nutrition_facts' ),
				'recipe_collections_nutrition_facts_hidden_default' => WPRM_Settings::get( 'recipe_collections_nutrition_facts_hidden_default' ),
				'recipe_collections_nutrition_facts_count' => WPRM_Settings::get( 'recipe_collections_nutrition_facts_count' ),
				'recipe_collections_nutrition_facts_fields' => WPRM_Settings::get( 'recipe_collections_nutrition_facts_fields' ),
				'recipe_collections_nutrition_facts_round_to_decimals' => WPRM_Settings::get( 'recipe_collections_nutrition_facts_round_to_decimals' ),
				'recipe_collections_shopping_list' => WPRM_Settings::get( 'recipe_collections_shopping_list' ),
				'recipe_collections_shopping_list_print' => WPRM_Settings::get( 'recipe_collections_shopping_list_print' ),
				'recipe_collections_save_button' => WPRM_Settings::get( 'recipe_collections_save_button' ),
				'recipe_collections_shopping_list_options' => WPRM_Settings::get( 'recipe_collections_shopping_list_options' ),
				'unit_conversion_enabled' => WPRM_Settings::get( 'unit_conversion_enabled' ),
				'unit_conversion_system_1' => WPRM_Settings::get( 'unit_conversion_system_1' ),
				'unit_conversion_system_2' => WPRM_Settings::get( 'unit_conversion_system_2' ),
			),
			'labels' => array(
				'nutrition_fields' => WPRM_Nutrition::get_fields(),
			),
		);

		if ( true === $include_user_collections ) {
			$data['collections'] = WPRMPRC_Manager::get_user_collections( $specific_user, false );
			$data['collections_default'] = WPRMPRC_Manager::get_default_collections();

			// Get all saved collections.
			if ( isset( $_GET['wprmprc_user'] ) ) {
				$collections = array();

				$args = array(
					'post_type' => WPRMPRC_POST_TYPE,
					'post_status' => 'any',
					'posts_per_page' => -1,
					'orderby' => 'date',
					'order' => 'DESC',
				);

				$query = new WP_Query( $args );
				$posts = $query->posts;
				foreach ( $posts as $post ) {
					$collection = WPRMPRC_Manager::get_collection( $post );

					if ( ! $collection ) {
						continue;
					}

					$collections[] = array(
						'value' => $collection->id(),
						'label' => $collection->id() . ' - ' . $collection->name(),
						'data' => $collection->get_data(),
					);
				}

				$data['saved_collections'] = $collections;
			}
		}

		return $data;
	}

	/**
	 * Localize the collections feature.
	 *
	 * @since	5.9.0
	 */
	public static function localize_collections( $specific_user = false ) {
		wp_localize_script( 'wprmp-public', 'wprmprc_public', self::localize_shortcode_data( true, $specific_user ) );
	}

	/**
	 * Localize the saved collections feature.
	 *
	 * @since	5.9.0
	 */
	public static function localize_saved_collection( $collection ) {
		if ( false !== $collection ) {
			wp_localize_script( 'wprmp-public', 'wprmprc_public', self::localize_shortcode_data( false ) );
			wp_localize_script( 'wprmp-public', 'wprmprc_public_collection_' . $collection->id(), $collection->get_data() );
		}
	}

	/**
	 * Custom CSS from settings.
	 *
	 * @since	4.1.0
	 */
	public static function custom_css() {
		if ( self::$load_assets ) {
			$css = '';

			// Font and column size.
			$css .= '#wprm-recipe-collections-app, .wprm-recipe-saved-collections-app { font-size: ' . WPRM_Settings::get( 'recipe_collections_appearance_font_size' ) . 'px; }';
			$css .= '.wprmprc-collection-column-balancer, .wprmprc-collection-column, .wprmprc-collection-actions { flex: 1; flex-basis: ' . WPRM_Settings::get( 'recipe_collections_appearance_column_size' ) . 'px; }';

			$header_color = WPRM_Settings::get( 'recipe_collections_header_color' );
			if ( $header_color !== WPRM_Settings::get_default( 'recipe_collections_header_color' ) ) {
				$css .= '.wprmprc-overview, .wprmprc-collection .wprmprc-collection-column { border-color: ' . $header_color  . '; }';
				$css .= '.wprmprc-overview-header, .wprmprc-collection .wprmprc-collection-column-header { background-color: ' . $header_color  . '; }';
				$css .= '.wprmprc-shopping-list .wprmprc-shopping-list-collection, .wprmprc-shopping-list .wprmprc-shopping-list-list { border-color: ' . $header_color  . '; }';
				$css .= '.wprmprc-shopping-list .wprmprc-shopping-list-collection-header, .wprmprc-shopping-list .wprmprc-shopping-list-list-header { background-color: ' . $header_color  . '; }';
			}

			$header_text_color = WPRM_Settings::get( 'recipe_collections_header_text_color' );
			if ( $header_text_color !== WPRM_Settings::get_default( 'recipe_collections_header_text_color' ) ) {
				$css .= '.wprmprc-overview-header, .wprmprc-collection .wprmprc-collection-column-header { color: ' . $header_text_color  . '; }';
				$css .= '.wprmprc-shopping-list .wprmprc-shopping-list-collection-header, .wprmprc-shopping-list .wprmprc-shopping-list-list-header { color: ' . $header_text_color  . '; }';
			}

			$button_color = WPRM_Settings::get( 'recipe_collections_button_color' );
			if ( $button_color !== WPRM_Settings::get_default( 'recipe_collections_button_color' ) ) {
				$css .= '.wprmprc-collection-action { background-color: ' . $button_color  . '; }';
				$css .= '.wprmprc-shopping-list .wprmprc-shopping-list-actions .wprmprc-shopping-list-action { background-color: ' . $button_color  . '; }';
			}

			$button_text_color = WPRM_Settings::get( 'recipe_collections_button_text_color' );
			if ( $button_text_color !== WPRM_Settings::get_default( 'recipe_collections_button_text_color' ) ) {
				$css .= '.wprmprc-collection-action { color: ' . $button_text_color  . '; border-color: ' . $button_text_color . '; }';
				$css .= '.wprmprc-collection-action:hover { color: ' . $button_color  . '; background-color: ' . $button_text_color . '; }';
				$css .= '.wprmprc-shopping-list .wprmprc-shopping-list-actions .wprmprc-shopping-list-action { color: ' . $button_text_color  . '; border-color: ' . $button_text_color . '; }';
				$css .= '.wprmprc-shopping-list .wprmprc-shopping-list-actions .wprmprc-shopping-list-action:not(.wprmprc-shopping-list-action-disabled):hover { color: ' . $button_color  . '; background-color: ' . $button_text_color . '; }';
			}

			echo '<style type="text/css">' . $css . '</style>';
		}
	}

	/**
	 * Custom CSS from settings on admin page.
	 *
	 * @since	4.1.0
	 */
	public static function custom_css_admin() {
		$screen = get_current_screen();
		
		if ( 'admin_page_wprm_recipe_collections' === $screen->id ) {
			self::$load_assets = true;
			echo self::custom_css();
		}
	}
}

WPRMPRC_Assets::init();

if (!global._babelPolyfill) { require('babel-polyfill'); }

import '../css/print/print.scss';
import './print/index.js';

// For adjustable servings and unit conversion on the print page.
import './public/servings-changer.js';
import '../../addons-pro/unit-conversion/assets/js/public.js';
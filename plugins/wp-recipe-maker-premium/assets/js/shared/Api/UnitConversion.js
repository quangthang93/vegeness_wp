const conversionEndpoint = wprmp_admin.endpoints.unit_conversion;

import ApiWrapper from 'Shared/ApiWrapper';

export default {
    get(ingredients) {
        const data = {
            ingredients,
        };

        return ApiWrapper.call( `${conversionEndpoint}`, 'POST', data );
    },    
};

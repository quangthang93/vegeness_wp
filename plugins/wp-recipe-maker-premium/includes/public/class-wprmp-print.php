<?php
/**
 * Handle Premium features for recipe printing.
 *
 * @link       http://bootstrapped.ventures
 * @since      6.1.0
 *
 * @package    WP_Recipe_Maker_Premium
 * @subpackage WP_Recipe_Maker_Premium/includes/public
 */

/**
 * Handle Premium features for recipe printing.
 *
 * @since      6.1.0
 * @package    WP_Recipe_Maker_Premium
 * @subpackage WP_Recipe_Maker_Premium/includes/public
 * @author     Brecht Vandersmissen <brecht@bootstrapped.ventures>
 */
class WPRMP_Print {

	/**
	 * Register actions and filters.
	 *
	 * @since    6.1.0
	 */
	public static function init() {
		add_filter( 'wprm_print_output', array( __CLASS__, 'output' ), 2, 2 );
	}

	/**
	 * Get output for the print page.
	 *
	 * @since    6.1.0
	 * @param	array $output 	Current output for the print page.
	 * @param	array $args	 	Arguments for the print page.
	 */
	public static function output( $output, $args ) {
		// Load premium styling.
		$output['assets'][] = array(
			'type' => 'css',
			'url' => WPRMP_URL . 'dist/public-' . strtolower( WPRMP_BUNDLE ) . '.css',
		);
		$output['assets'][] = array(
			'type' => 'css',
			'url' => WPRMP_URL . 'dist/print.css',
		);
		$output['assets'][] = array(
			'type' => 'js',
			'url' => WPRMP_URL . 'dist/print.js',
		);
		$output['assets'][] = array(
			'type' => 'custom',
			'html' => self::print_accent_color_styling(),
		);

		if ( 'recipe' === $output['type'] ) {
			// Needed for adjustable servings.
			$output['assets'][] = array(
				'type' => 'custom',
				'html' => '<script>wprmp_public = { settings : { recipe_template_mode: "' . WPRM_Settings::get( 'recipe_template_mode' ) . '", features_adjustable_servings : true, adjustable_servings_round_to_decimals: ' . WPRM_Settings::get( 'adjustable_servings_round_to_decimals' ) . ' } };</script>',
			);

			// Needed for Unit Conversion.
			$recipe = $output['recipe'];

			if ( $recipe ) {
				$ingredients = $recipe->ingredients_without_groups();

				foreach ( (array) $ingredients as $key => $value ) {
					if ( ! is_scalar( $value ) ) continue;
					$ingredients[$key] = html_entity_decode( (string) $value, ENT_QUOTES, 'UTF-8');
				}
				$output['assets'][] = array(
					'type' => 'custom',
					'html' => '<script>var wprmpuc_recipe_' . $recipe->id() . ' = ' . wp_json_encode( array( 'ingredients' => $ingredients ) ) . ';</script>',
				);
			}

			// Adjustable servings header.
			$servings = $recipe ? $recipe->servings() : false;

			if ( $servings && WPRM_Settings::get( 'print_adjustable_servings' ) ) {
				$output['header'] .= '<div id="wprm-print-servings-container">';
				$output['header'] .= '<span class="wprm-print-servings-decrement wprm-print-servings-change">–</span><input id="wprm-print-servings" type="text" value="' . esc_attr( $servings ) . '" min="1"><span class="wprm-print-servings-increment wprm-print-servings-change">+</span>';
				$output['header'] .= '&nbsp;<span id="wprm-print-servings-unit">' . __( 'servings', 'wp-recipe-maker' ) . '</span>';
				$output['header'] .= '</div>';
			}
		}

		if ( 'recipes' === $output['type'] ) {
			// Needed for adjustable servings.
			$output['assets'][] = array(
				'type' => 'custom',
				'html' => '<script>wprmp_public = { settings : { recipe_template_mode: "' . WPRM_Settings::get( 'recipe_template_mode' ) . '", features_adjustable_servings : true, adjustable_servings_round_to_decimals: ' . WPRM_Settings::get( 'adjustable_servings_round_to_decimals' ) . ' } };</script>',
			);

			// Need serving information for each recipe.
			$output['assets'][] = array(
				'type' => 'custom',
				'html' => '<script>var wprmp_print_recipes = ' . wp_json_encode( $output['recipes'] ) . ';</script>',
			);
		}

		return $output;
	}

	/**
	 * Get custom styling for the print accent color.
	 *
	 * @since    6.1.0
	 */
	public static function print_accent_color_styling() {
		$output = '';
		$color = WPRM_Settings::get( 'print_accent_color' );
		$color_default = WPRM_Settings::get_default( 'print_accent_color' );

		if ( $color !== $color_default ) {
			$output .= '<style>';
			$output .= ' #wprm-print-servings-container #wprm-print-servings { border-color: ' . $color . ' !important; }';
			$output .= ' #wprm-print-servings-container .wprm-print-servings-change { border-color: ' . $color . ' !important; background-color: ' . $color . ' !important; }';
			$output .= '</style>';
		}

		return $output;
	}
}

WPRMP_Print::init();
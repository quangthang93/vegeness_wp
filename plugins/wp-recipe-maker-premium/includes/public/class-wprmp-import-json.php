<?php
/**
 * Handle the import of recipes from JSON.
 *
 * @link       http://bootstrapped.ventures
 * @since      5.2.0
 *
 * @package    WP_Recipe_Maker_Premium
 * @subpackage WP_Recipe_Maker_Premium/includes/public
 */

/**
 * Handle the import of recipes from JSON.
 *
 * @since      5.2.0
 * @package    WP_Recipe_Maker_Premium
 * @subpackage WP_Recipe_Maker_Premium/includes/public
 * @author     Brecht Vandersmissen <brecht@bootstrapped.ventures>
 */
class WPRMP_Import_JSON {

	/**
	 *  Number of recipes to import at a time.
	 *
	 * @since    5.3.0
	 * @access   private
	 * @var      int $import_limit Number of recipes to import at a time.
	 */
	private static $import_limit = 3;

	/**
	 * Register actions and filters.
	 *
	 * @since    5.2.0
	 */
	public static function init() {
		add_action( 'admin_menu', array( __CLASS__, 'add_submenu_page' ), 20 );
		add_action( 'wp_ajax_wprm_import_json', array( __CLASS__, 'ajax_import_json' ) );
	}

	/**
	 * Add the JSON import page.
	 *
	 * @since	5.2.0
	 */
	public static function add_submenu_page() {
		add_submenu_page( null, __( 'WPRM Import from JSON', 'wp-recipe-maker' ), __( 'WPRM Import from JSON', 'wp-recipe-maker' ), WPRM_Settings::get( 'features_import_access' ), 'wprm_import_json', array( __CLASS__, 'import_json_page_template' ) );
	}

	/**
	 * Get the template for the edit saved collection page.
	 *
	 * @since	5.2.0
	 */
	public static function import_json_page_template() {
		$importing = false;

		if ( isset( $_POST['wprm_import_json'] ) && wp_verify_nonce( $_POST['wprm_import_json'], 'wprm_import_json' ) ) { // Input var okay.
			$filename = $_FILES['json']['tmp_name'];
			if ( $filename ) {
				$json = false;

				$str = file_get_contents(
					$filename,
					false,
					stream_context_create( array(
						'http' => array(
							'ignore_errors' => true,
						),
					))
				);
				if ( $str ) {
					$json = json_decode( $str, true );
				}

				if ( ! $json || ! is_array( $json ) || ! count( $json ) ) {
					echo '<p>We were not able to read this file or find any recipes. Is it using the correct JSON format?</p>';
				} else {
					$importing = true;
					delete_transient( 'wprm_import_recipes_json' );
					$transient = json_encode( $json );
					set_transient( 'wprm_import_recipes_json', $transient, 60 * 60 * 24 );

					$recipes = count ( $json );
					$pages = ceil( $recipes / self::$import_limit );

					// Handle via AJAX.
					wp_localize_script( 'wprmp-admin', 'wprm_import_json', array(
						'pages' => $pages,
					));

					echo '<p>Importing ' . $recipes . ' recipes.</p>';
					echo '<div id="wprm-tools-progress-container"><div id="wprm-tools-progress-bar"></div></div>';
					echo '<p id="wprm-tools-finished">Import finished!. <a href="' . admin_url( 'admin.php?page=wprecipemaker' ) . '">View on the manage page</a>.</p>';
					
					// foreach ( $json as $json_recipe ) {
					// 	self::import_json_recipe( $json_recipe );
					// }

					// echo '<p>Imported ' . count( $json ) . ' recipes. <a href="' . admin_url( 'admin.php?page=wprecipemaker' ) . '">View on the manage page</a>.</p>';
				}
			} else {
				echo '<p>No file selected.</p>';
			}
		}
		
		if ( ! $importing ) {
			include WPRMP_DIR . 'templates/admin/import-json.php';
		}
	}

	/**
	 * Import recipes through AJAX.
	 *
	 * @since	5.3.0
	 */
	public static function ajax_import_json() {
		if ( check_ajax_referer( 'wprm', 'security', false ) ) {
			$page = isset( $_POST['page'] ) ? intval( $_POST['page'] ) : false; // Input var okay.

			if ( false !== $page ) {
				$transient = get_transient( 'wprm_import_recipes_json' );
				$json = json_decode( $transient, true );

				if ( $json && is_array( $json ) ) {
					$start = $page * self::$import_limit;
					$end = $start + self::$import_limit;

					for ( $i = $start; $i < $end; $i++ ) {
						if ( isset( $json[ $i ] ) ) {
							self::import_json_recipe( $json[ $i ] );
						}
					}

					wp_send_json_success();
				}
			}

			wp_send_json_error();
		}
		wp_die();
	}

	/**
	 * Import a single recipe from JSON.
	 *
	 * @since	5.2.0
	 * @param	mixed $json_recipe  Recipe to import from JSON.
	 */
	public static function import_json_recipe( $json_recipe ) {
		// Create new recipe.
		$post = array(
			'post_type' => WPRM_POST_TYPE,
			'post_status' => 'draft',
		);

		// Try to reuse the ID if set.
		if ( isset ( $json_recipe['id'] ) && $json_recipe['id'] ) {
			$post['import_id'] = $json_recipe['id'];
		}

		$recipe_id = wp_insert_post( $post );

		// Import recipe images.
		if ( isset( $json_recipe['image_url'] ) && $json_recipe['image_url'] ) {
			$json_recipe['image_id'] = WPRM_Import_Helper::get_or_upload_attachment( $recipe_id, $json_recipe['image_url'] );
		}
		if ( isset( $json_recipe['pin_image_url'] ) && $json_recipe['pin_image_url'] ) {
			$json_recipe['pin_image_id'] = WPRM_Import_Helper::get_or_upload_attachment( $recipe_id, $json_recipe['pin_image_url'] );
		}

		// Import instruction images.
		if ( isset( $json_recipe['instructions_flat'] ) ) {
			foreach ( $json_recipe['instructions_flat'] as $index => $instruction ) {
				if ( isset( $instruction['image_url'] ) && $instruction['image_url'] ) {
					$json_recipe['instructions_flat'][ $index ]['image'] = WPRM_Import_Helper::get_or_upload_attachment( $recipe_id, $instruction['image_url'] );
				} 
			}
		}

		// Import custom field images.
		if ( isset( $json_recipe['custom_fields'] ) ) {
			foreach ( $json_recipe['custom_fields'] as $index => $custom_field ) {
				if ( is_array( $custom_field ) && isset( $custom_field['url'] ) && $custom_field['url'] ) {
					$json_recipe['custom_fields'][ $index ]['id'] = WPRM_Import_Helper::get_or_upload_attachment( $recipe_id, $custom_field['url'] );
				}
			}
		}

		$recipe = WPRM_Recipe_Sanitizer::sanitize( $json_recipe );
		WPRM_Recipe_Saver::update_recipe( $recipe_id, $recipe );
	}
}
WPRMP_Import_JSON::init();

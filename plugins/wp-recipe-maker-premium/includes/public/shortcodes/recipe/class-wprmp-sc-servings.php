<?php
/**
 * Handle the Premium servings shortcode.
 *
 * @link       http://bootstrapped.ventures
 * @since      5.6.0
 *
 * @package    WP_Recipe_Maker_Premium
 * @subpackage WP_Recipe_Maker_Premium/includes/public/shortcodes/recipe
 */

/**
 * Handle the Premium servings shortcode.
 *
 * @since      5.6.0
 * @package    WP_Recipe_Maker_Premium
 * @subpackage WP_Recipe_Maker_Premium/includes/public/shortcodes/recipe
 * @author     Brecht Vandersmissen <brecht@bootstrapped.ventures>
 */
class WPRMP_SC_Servings {
	public static function init() {
		add_filter( 'wprm_recipe_servings_shortcode', array( __CLASS__, 'shortcode' ), 10, 3 );
		add_filter( 'wprm_recipe_adjustable_servings_shortcode', array( __CLASS__, 'adjustable_shortcode' ), 10, 3 );
	}

	/**
	 * Filter the servings shortcode.
	 *
	 * @since	5.6.0
	 * @param	mixed $output Current output.
	 * @param	array $atts   Options passed along with the shortcode.
	 * @param	mixed $recipe Recipe the shortcode is getting output for.
	 */
	public static function shortcode( $output, $atts, $recipe ) {
		// Output.
		$classes = array(
			'wprm-recipe-servings',
			'wprm-recipe-details',
			'wprm-recipe-servings-' . $recipe->id(),
			'wprm-recipe-servings-adjustable-' . $atts['adjustable'],
			'wprm-block-text-' . $atts['text_style'],
		);

		// Style in Preview.
		$output = '';
		if ( $atts['is_template_editor_preview'] ) {
			switch ( $atts['adjustable'] ) {
				case 'text':
					$output = '<input type="number" value="' . esc_attr( $recipe->servings() ) . '" class="' . implode( ' ', $classes ) . '" data-recipe="' . $recipe->id() . '" aria-label="' . esc_attr__( 'Adjust recipe servings', 'wp-recipe-maker' ) . '">';
					break;
				case 'tooltip':
					$output = '<a class="' . implode( ' ', $classes ) . '" data-recipe="' . $recipe->id() . '">' . $recipe->servings() . '</a>';
					break;
			}
		}

		if ( ! $output ) {
			$output = '<span class="' . implode( ' ', $classes ) . '" data-recipe="' . $recipe->id() . '" aria-label="' . esc_attr__( 'Adjust recipe servings', 'wp-recipe-maker' ) . '">' . $recipe->servings() . '</span>';
		}

		if ( (bool) $atts['label_container'] ) {
			$unit = WPRM_SC_Servings_Unit::shortcode( $atts );
			if ( $unit ) {
				$output = '<span class="wprm-recipe-servings-with-unit">' . $output . ' ' . $unit . '</span>';
			}

			$output = WPRM_Shortcode_Helper::get_label_container( $atts, 'servings', $output );
		}

		return $output;
	}

	/**
	 * Filter the adjustable servings shortcode.
	 *
	 * @since	5.6.0
	 * @param	mixed $output Current output.
	 * @param	array $atts   Options passed along with the shortcode.
	 * @param	mixed $recipe Recipe the shortcode is getting output for.
	 */
	public static function adjustable_shortcode( $output, $atts, $recipe ) {
		if ( ! $recipe || ! $recipe->servings() ) {
			return '';
		}

		$classes = array(
			'wprm-recipe-adjustable-servings-container',
			'wprm-recipe-adjustable-servings-' . $recipe->id() . '-container',
			'wprm-toggle-container',
			'wprm-block-text-' . $atts['text_style'],
		);

		// Custom style.
		$style = '';
		$style .= 'background-color: ' . $atts['button_background'] . ';';
		$style .= 'border-color: ' . $atts['button_accent'] . ';';
		$style .= 'color: ' . $atts['button_accent'] . ';';
		$style .= 'border-radius: ' . $atts['button_radius'] . ';';

		// Buttons.
		$buttons = array();
		// $serving_options = explode( ';', $atts['serving_options'] );

		// foreach ( $serving_options as $option ) {
		// 	$option = trim( $option );
		// 	$value = floatval( str_replace( ',', '.', $option ) );

		// 	if ( 0 < $value ) {
		// 		$buttons[ '' . $value ] = $option . 'x'; // Make sure key is string or floats get truncated.
		// 	}
		// }

		// Set default buttons if not at least 2 valid options set.
		if ( 2 > count( $buttons ) ) {
			$buttons = array(
				'1' => '1x',
				'2' => '2x',
				'3' => '3x',
			);
		}

		$buttons_output = '';

		$first_button = true;
		foreach ( $buttons as $multiplier => $button_text ) {
			// Button style.
			$button_style = '';
			$button_style .= 'background-color: ' . $atts['button_accent'] . ';';
			$button_style .= 'color: ' . $atts['button_background'] . ';';

			if ( ! $first_button ) {
				$button_style .= 'border-left: 1px solid ' . $atts['button_accent'] . ';';
			} else {
				$first_button = false;
			}

			$active = 1 === $multiplier ? ' wprm-toggle-active' : '';
			$buttons_output .= '<button href="#" class="wprm-recipe-adjustable-servings wprm-toggle' . $active . '" data-multiplier="' . esc_attr( $multiplier ) . '" data-servings="' . esc_attr( $recipe->servings() ) . '" data-recipe="' . esc_attr( $recipe->id() ) . '" style="' . $button_style .'" aria-label="' . __( 'Adjust servings to', 'wp-recipe-maker' ) . ' ' . $button_text . '">' . $button_text . '</button>';
		}

		// Output.
		$output = '<div class="' . implode( ' ', $classes ) . '" style="' . $style . '">' . $buttons_output . '</div>';

		return $output;
	}
}

WPRMP_SC_Servings::init();
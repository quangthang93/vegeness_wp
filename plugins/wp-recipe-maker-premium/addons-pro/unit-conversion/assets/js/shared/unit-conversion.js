export function get_active_system(recipe) {
    var system = 1,
        link = recipe.querySelector('.wprm-unit-conversion.wprmpuc-active'),
        dropdown = recipe.querySelector('.wprm-unit-conversion-dropdown');

    if ( link ) {
        system = parseInt( link.dataset.system );
    } else if ( dropdown ) {
        system = parseInt( dropdown.options[dropdown.selectedIndex].value );
    }

    return system;
}
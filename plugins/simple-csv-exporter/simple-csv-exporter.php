<?php
/*
Plugin Name: Simple CSV Exporter
Version: 0.1-alpha
Description: simple csv expoeter
Author: kurozumi
Author URI: http://a-zuim.net
Plugin URI: http://a-zuim.net
Text Domain: simple-csv-exporter
Domain Path: /languages
*/

$simple_csv_exporter = new Simple_CSV_Exporter;
$simple_csv_exporter->register();

class Simple_CSV_Exporter {
	
	public function register()
	{
		add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
	}
	
	public function plugins_loaded()
	{
		add_action('admin_menu', function(){
			add_menu_page('CSVエクスポート', 'CSVエクスポート', 'manage_options', __FILE__, array($this, 'show_options_page'));
		});
	}
	
	public function show_options_page()
	{
		?>
		<div class="wrap">
		<div id="icon-options-general" class="icon32"><br /></div><h2>CSVエクスポート</h2>
			<form action="" method="post">
				<?php wp_nonce_field('csv_export');?>
				<table class="form-table">
					<tr valign="top">
						<th scope="row"><label for="inputtext">投稿タイプを選択</label></th>
						<td>
							<select name="post_type"　class="regular-text">
								<option value="post"><?php _e( 'Posts' ); ?></option>
								<option value="page"><?php _e( 'Pages' ); ?></option>
								<?php foreach ( get_post_types( array( '_builtin' => false, 'can_export' => true ), 'objects' ) as $post_type ) : ?>
								<option value="<?php echo esc_attr( $post_type->name ); ?>"><?php echo esc_attr( $post_type->label ); ?></option>
								<?php endforeach;?>
							</select>
						</td>
					</tr>
				</table>
				<p class="submit"><input type="submit" class="button-primary" value="エクスポート" /></p>
			</form>
		<?php $this->result(); ?>
		</div>
<?php
	}
	
	public function result()
	{
		if(isset($_POST['post_type']))
		{
			echo "<hr />";
			try {
				$result = $this->export();
				printf("<p>「%s」記事のCSVを作成しました。</p>", $result['post_type']);
				printf('<p><a href="%s" target="_blank">右クリックでファイルを保存して下さい。</a></p>', $result['download_url']);
			} catch (Exception $ex) {
				printf("<p>%s</p>", $ex->getMessage());
			}
		}	
	}

	public function export()
	{
		global $wpdb;
		
		check_admin_referer('csv_export');

		$post_type = get_post_type_object($_POST['post_type']);
		
		if(!$post_type)
			throw new Exception("post_typeが正しくありません。");

		// wp_postsテーブルから指定したpost_typeの公開記事を取得
// 		$query = <<< __EOS__
// 			SELECT 
// 				ID as post_id,
// 				post_name,
// 				post_type,
// 				post_status,
// 				post_title,
// 				post_content
// 			FROM  {$wpdb->posts} 
// 			WHERE post_status LIKE  'publish' AND
// 			post_type LIKE  '%s'
// __EOS__;
		$query = <<< __EOS__
			SELECT 
				ID as post_id,
				post_type,
				post_status,
				post_date,
				post_title,
				post_content
			FROM  {$wpdb->posts} 
			WHERE post_status LIKE  'publish' AND
			post_type LIKE  '%s' LIMIT 900,100
__EOS__;
		$prepare = $wpdb->prepare($query, array($post_type->name));
		$results = $wpdb->get_results($prepare, ARRAY_A);

		if(!$results)
			throw new Exception(sprintf("%sの記事が見つかりませんでした。", $post_type->label));

		// カテゴリとタグのslugを追加
		$results = array_map(function($result){

			// echo '<pre>';
			// print_r(get_the_terms($result['post_id'], 'shop_area'));
			// echo '</pre>';die(1111);
			// カテゴリ追加
			$cats = implode(',', array_map(function($cat){
				return $cat->slug;
			}, get_the_category($result['post_id'])));

			// タグがあれば追加
			if($tags = get_the_tags($result['post_id'])){
				$tags = implode(',', array_map(function($tag){
					return $tag->slug;
				}, $tags));
			}else{
				$tags = "";
			}
			$post_thumbnail = get_the_post_thumbnail_url($result['post_id'], 'full'); 
			//地域追加
			// $regions = implode(',', array_map(function($region){
			// 	// echo '<pre>';
			// 	// print_r($region);
			// 	// echo '</pre>';
			// 	// return $region;
			// 	return $region->name;
			// }, get_the_terms( $result['post_id'], 'shop_area' )));

			$recipe_cats = implode(',', array_map(function($recipe_cat){
				echo '<pre>';
				print_r($result['post_id'], 'recipe_category' );
				echo '</pre>';
				return $recipe_cat->name;
			}, get_the_terms( $result['post_id'], 'recipe_category' )));

			//ジャンル
			// $genres = implode(',', array_map(function($genre){
			// 	return $genre->name;
			// }, get_the_terms( $result['post_id'], 'shop_genre' )));

			//お店アイコン
			// $shop_icon = implode(',', array_map(function($genre){
			// 	return $genre->name;
			// }, get_the_terms( $result['post_id'], 'shop_icon' )));

			// echo '<pre>';
			// print_r($regions);
			// echo '</pre>';
			// die(1111);
			$meta_fields = implode(',', array_map(function($meta_field){
				unset($meta_field['map']);
				$img_url = $this->get_img_url($meta_field['swipe_imgs']);
				$meta_field['img_url'] = $img_url;
				echo '<pre>';
				print_r($meta_field);
				echo '</pre>';
				// exit;
				return json_encode($meta_field);
			}, get_post_meta( $result['post_id'], 'shop_meta' )));
                        
                        if(!empty(apply_filters('the_content', get_post_field('post_content', $result['post_id'])))) {
                            $recipe_content = get_post_field('post_content', $result['post_id']);
                        } else {
                            $recipe_content = get_post_meta($result['post_id'], 'recipe_desc', true);
                        }
                        
			return array_merge($result, array(
				// 'tax_shop_area'     => $regions,
				// 'meta_fields'     => $meta_fields,
				'post_thumbnail' => $post_thumbnail,
				// 'tax_shop_genre'     => $genres,
				// 'tax_shop_icon'     => $shop_icon
				'tax_recipe_category'     => $recipe_cats,
				'post_content'     => $recipe_content,
			));
		}, $results);


		// 項目名を取得
		$head[] = array_keys(current($results));

		// 先頭に項目名を追加
		$list = array_merge($head, $results);

		// ファイルの保存場所を設定
		$prefix_filename = mt_rand( 0,999 );
		$filename = "export-{$prefix_filename}.csv";
		$filepath =  dirname(__FILE__) . '/' . $filename;
		$fp = fopen($filepath, 'w');

		// 配列をカンマ区切りにしてファイルに書き込み
		foreach($list as $fields){
			//mb_convert_variables('SJIS',  'UTF-8', $fields);
			fputcsv($fp, $fields);
		}

		fclose($fp);

		return array(
			'post_type' => $post_type->label,
			'download_url' => plugins_url( '', __FILE__ ) . '/' . $filename
		);
	}
	public function print_taxonomy_ranks( $terms = '' ){

	    // check input
	    if ( empty( $terms ) || is_wp_error( $terms ) || ! is_array( $terms ) )
	        return;

	    // set id variables to 0 for easy check 
	    $order_id = $family_id = $subfamily_id = 0;

	    // get order
	    foreach ( $terms as $term ) {
	        if ( $order_id || $term->parent )
	            continue;
	        $order_id  = $term->term_id;
	        $order     = $term->name;
	    }

	    // get family
	    foreach ( $terms as $term ) { 
	        if ( $family_id || $order_id != $term->parent )
	            continue;
	        $family_id = $term->term_id;
	        $family    = $term->name;
	    }

	    // get subfamily
	    foreach ( $terms as $term ) { 
	        if ( $subfamily_id || $family_id != $term->parent ) 
	            continue;
	        $subfamily_id = $term->term_id;
	        $subfamily    = $term->name;
	    }

	    // output
	    // return array($order, $family, $subfamily);
	    return array($order,$family,$subfamily);

	}

	public function get_img_url($img_ids) {

		foreach($img_ids as $img_id) {
			$img[] = wp_get_attachment_url($img_id);
		}
		return $img;
	}
}


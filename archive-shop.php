<?php get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
                <?php wp_breadcrumb() ?>
            </div>
        </div>
    </div>
    <section class="section recipe blog">
        <div class="container">
            <div class="section-recipe--left fadeup2">
                <div class="p-restaurant">
                    <?php
                    $shop_address = array();
                    $shop__keywork = $_REQUEST['keywork'] ? sanitize_text_field($_REQUEST['keywork']) : '';
                    $shop_pref = !empty($_REQUEST['pref']) ? $_REQUEST['pref'] : '';
                    
                    $genre_arr = !empty($_REQUEST['genre']) ? $_REQUEST['genre'] : '';
                    $icon_arr = !empty($_REQUEST['icon']) ? $_REQUEST['icon'] : '';
                    
                    $genre_arr = array_diff($genre_arr, array($_REQUEST['keyclear']));
                    $shop_pref = array_diff($shop_pref, array($_REQUEST['keyclear']));
                    $icon_arr = array_diff($icon_arr, array($_REQUEST['keyclear']));
                    
                    if($_REQUEST['keyclear'] && !is_numeric($_REQUEST['keyclear']))
                        $shop__keywork = '';   
                    ?>
                    <div class="sectionEP-head">
                        <div class="sectionEP-titleWrap">
                            <h1 class="sectionEP-title fork">レストラン検索</h1>
                            <p class="sectionEP-resultSumary" style="display: none;"></p>
                        </div>
                        <div class="sectionEP-sort">
                            <div class="tagX-list">
                                <form action="/shop" id="frmShop" method="GET">
                                    <input type="hidden" name="keyclear" />
                                    
                                    <?php if ($shop_pref):
                                        foreach ($shop_pref as $pref_id):
                                            if(!is_numeric($pref_id)) continue;
                                            $term_pref = get_term($pref_id, 'shop_genre');
                                            ?>
                                        <span class="tagX"><?php echo shop_area($pref_id);?><i class="tagX-icon"></i></span>
                                        <input type="hidden" name="pref[]" value="<?php echo $pref_id?>" />
                                        <?php
                                        endforeach;
                                    endif;?>

                                    <?php if ($icon_arr):
                                        foreach ($icon_arr as $pref_id):
                                            if(!is_numeric($pref_id)) continue;
                                            $term_pref = get_term($pref_id, 'shop_icon');
                                            ?>
                                        <span class="tagX"><?php echo $term_pref->name;?><i class="tagX-icon"></i></span>
                                        <input type="hidden" name="icon[]" value="<?php echo $pref_id?>" />
                                        <?php
                                        endforeach;
                                    endif;?>
                                        
                                    <?php if($shop__keywork):?>
                                        <span class="tagX"><?php echo $shop__keywork;?><i class="tagX-icon"></i></span>
                                        <input type="hidden" name="keywork" value="<?php echo $shop__keywork?>" />
                                    <?php endif;?>
                                    <?php
                                    if ($genre_arr):
                                        foreach ($genre_arr as $genre_id):
                                            $term_genre = get_term($genre_id, 'shop_genre');
                                            ?>
                                            <span class="tagX"><?php echo $term_genre->name ?><i class="tagX-icon"></i></span>
                                            <input type="hidden" name="genre[]" value="<?php echo $genre_id?>" />
                                            <?php
                                        endforeach;
                                    endif;?>                                    
                                          
                                </form>
                                
                                <script type="text/javascript">
                                    /* <![CDATA[ */
                                    jQuery(document).ready(function($) {     
                                        $("#frmShop span.tagX").click(function(){
                                            var keywork = $(this).next('input').val();
                                            $("input[name='keyclear']").val(keywork);
                                            $("#frmShop").submit();
                                        });         
                                    });     
                                    /* ]]> */
                                </script>
                            </div>
                            <div class="sectionEP-newOrders rest">
                                <a href="/shop_search" class="link-return desc">レストラン検索に戻る</a>
                            </div>
                        </div><!--Wns .sectionEP-sort-->
                    </div><!--End .sectionEP-head-->                    
                    <div class="section-recipe--row">
                        <div class="p-restaurant--list">
                            <?php
                            $shop_pref_search = array();
                            foreach($shop_pref as $v){
                                if(is_numeric($v)){
                                    $shop_pref_search = $v;
                                }
                            }
                            
                            if($shop_pref_search) {
                                $shop_genre = array(
                                    'taxonomy' => 'shop_area',
                                    'field' => 'term_id',
                                    'terms' => $shop_pref_search,
                                );
                            }
                            if($genre_arr) {
                                $genre_cond = array(
                                    'taxonomy' => 'shop_genre',
                                    'field' => 'term_id',
                                    'terms' => $genre_arr,
                                );
                            }
                            if($icon_arr) {
                                $icon_cond = array(
                                    'taxonomy' => 'shop_icon',
                                    'field' => 'term_id',
                                    'terms' => $icon_arr,
                                );
                            }                            
                            
                            $args['post_type'] = 'shop';
                            $args['post_status'] = 'publish';
                            $args['orderby'] = 'menu_order date';
                            $args['order'] = 'ASC';
                            $args['posts_per_page'] = 12;
                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                            $args['paged'] = $paged;
                            // $args['_meta_or_title'] = $shop__keywork;
                            // $args['s'] = $shop__keywork;
                            
                            $args['tax_query'] = array(
                                'relation' => 'AND',
                                $icon_cond,
                                $genre_cond,
                                $shop_genre,
                            );

                            $search_fields = array();
                            if($shop__keywork != ""){
                                $search_fields['relation'] = 'OR';
                                $get_fields = acf_get_fields( 55480 );
                                foreach($get_fields as $item):
                                    if($item['name'] != ""){
                                        $search_fields[] = array(
                                            'key'     => $item['name'],
                                            'value'   => $shop__keywork,
                                            'compare' => 'LIKE',
                                        );
                                    }
                                endforeach;
                            }

                            if($search_fields){
                                $args['meta_query'] = $search_fields;
                            }               
                            
                            $shop_query = null;
                            $shop_query = new WP_Query($args);
                            
                            if ($shop_query->have_posts()):
                                while ($shop_query->have_posts()): $shop_query->the_post();
                                    $shop_areas =  get_the_terms(get_the_ID(), 'shop_area');
                                    $shop_icons =  get_the_terms(get_the_ID(), 'shop_icon');
                                    $shop_genres =  get_the_terms(get_the_ID(), 'shop_genre');
                                    ?>
                            
                                    <div class="p-restaurant--item">
                                        <h3 class="titleLv4"><?php the_title()?></h3>
                                        <div class="p-restaurant--item-cnt">
                                            <div class="p-restaurant--item-thumbbox">
                                                <div class="p-restaurant--item-thumb">
                                                    <a href="<?php the_permalink(); ?>">
                                                        <?php if ( has_post_thumbnail() ) : ?>
                                                            <?php the_post_thumbnail('medium_large'); ?>
                                                        <?php else:?>
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-img.jpg" />
                                                        <?php endif; ?>
                                                    </a>
                                                </div>
                                                <div class="p-restaurant--item-tagbox">
                                                    <?php if ( $shop_icons && ! is_wp_error( $shop_icons ) ) :
                                                        foreach ( $shop_icons as $shop_icon ): ?>
                                                            <span class="tag2"><?php echo $shop_icon->name?></span>
                                                        <?php 
                                                        endforeach; 
                                                    endif;?>
                                                </div>
                                            </div>
                                            <div class="p-restaurant--item-infor">
                                                <table class="desc">
                                                    <tr>
                                                        <th>エリア</th>
                                                        <td>
                                                            <?php if ( $shop_areas && ! is_wp_error( $shop_areas ) ) :
                                                                $shop_area = current($shop_areas);
                                                                echo $shop_area->name;
                                                            endif;?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>ジャンル</th>
                                                        <td>
                                                            <?php 
                                                            if ( $shop_genres && ! is_wp_error( $shop_genres ) ) : $j=0;
                                                                foreach ( $shop_genres as $shop_genre ): $j++;
                                                                    echo $shop_genre->name;
                                                                    echo $j < count($shop_genres) ? '、 ': '';                                                                    
                                                                endforeach; 
                                                            endif;
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php if(get_field('store_address')):?>
                                                    <tr>
                                                        <th>住所</th>
                                                        <td><?php the_field('store_address')?></td>
                                                    </tr>
                                                    <?php endif;?>
                                                    <?php if(get_field('store_marketing')):?>
                                                    <tr>
                                                        <th>営業時間</th>
                                                        <td><?php echo nl2br(get_field('store_marketing'))?></td>
                                                    </tr>
                                                    <?php endif;?>
                                                    <?php if(get_field('store_holiday')):?>
                                                    <tr>
                                                        <th>定休日</th>
                                                        <td><?php the_field('store_holiday')?></td>
                                                    </tr>
                                                    <?php endif;?>
                                                    <?php if(get_field('store_tel')):?>
                                                    <tr>
                                                        <th>電話番号</th>
                                                        <td><a href="tel:<?php the_field('store_tel')?>"><?php the_field('store_tel')?></a></td>
                                                    </tr>
                                                    <?php endif;?>
                                                    <tr>
                                                        <th>平均予算</th>
                                                        <td>
                                                            <?php
                                                            if(get_field('store_average')) {
                                                                echo nl2br(get_field('store_average'));
                                                            } else {
                                                                echo get_field('price_d') ? 'ランチ : '.get_field('price_d') : '';
                                                                echo get_field('price_n') ? '<br>ディナー : '.get_field('price_n') : '';
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <a href="<?php the_permalink(); ?>" class="btn-gray">店舗詳細へ</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                <?php
                                endwhile;
                                wp_reset_postdata();
                            endif;
                            ?>

                            <?php 
                            if($shop_query->found_posts > 0)
                                pagination($shop_query)
                            ?>
                        </div><!--End .p-restaurant--list-->
                    </div><!-- ./section-recipe--row -->
                </div><!-- ./p-restaurant -->

            </div><!-- ./section-recipe--left -->
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
                    <?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section><!--End .blog-->
    <?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>

<?php get_footer(); ?>

<?php get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
                <?php wp_breadcrumb()?>
            </div>
        </div>
    </div><!--End .breadcrumbWrap-->
    <section class="section recipe rest">
        <div class="container">
            <div class="section-recipe--left fadeup2">
                <div class="p-restaurant">
                    <div class="sectionEP-head">
                        <div class="sectionEP-titleWrap">
                            <h1 class="sectionEP-title no-icon">404 NOT FOUND</h1>
                        </div>
                    </div>
                    <div class="section-recipe--row">
                        <div class="registration-cnt">
                            <div class="registration-intro">
                                <p class="titleLv5 mgb-15">申し訳ございません。お探しのページは見つかりませんでした。</p>
                                <p class="desc">お客様がアクセスしようとしたページは、 掲載期間が終了し削除されたか、別の場所に移動した可能性があります。
                                    <br>メニューから引き続き当社ホームページをご覧くださいませ。</p>
                            </div>


                        </div><!-- ./registration-cnt -->
                    </div><!-- ./section-recipe--row -->
                </div>
            </div><!-- ./section-recipe--left -->
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
                    <?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>

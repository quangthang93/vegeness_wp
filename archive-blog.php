<?php get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
                <?php wp_breadcrumb() ?>
            </div>
        </div>
    </div>
    <section class="section recipe blog">
        <div class="container">
            <?php
            if (get_query_var('key')) {
                $key_search = sanitize_text_field(get_query_var('key'));
            }
            ?>
            <div class="section-recipe--left fadeup2">
                <div class="p-restaurant">
                    <div class="sectionEP-head">
                        <div class="sectionEP-titleWrap">
                            <h1 class="sectionEP-title pen"><?php echo $key_search ? $key_search : 'ヴィーガンコラム';?></h1>
                        </div>
                    </div>
                    <div class="section-recipe--row">
                        <div class="blog-cnt">
                            <div class="blog-list">
                                <?php
                                add_filter( 'posts_where', 'title_filter', 10, 2 );

                                $offset = 0;

                                $args['post_type'] = 'blog';
                                $args['post_status'] = 'publish';
                                $args['orderby'] = 'date';
                                $args['order'] = 'DESC';
                                $args['posts_per_page'] = 8;
                                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                $args['paged'] = $paged;                                
                                $args['title_filter'] = $key_search;
                                $args['title_filter_relation'] = 'AND';

                                $blog_query = null;
                                $blog_query = new WP_Query($args);
                                remove_filter( 'posts_where', 'title_filter', 10, 2 );
                                
                                if ($blog_query->have_posts()):
                                    while ($blog_query->have_posts()): $blog_query->the_post();
                                        $blog_terms =  get_the_terms(get_the_ID(), 'blog_category');
                                        $offset++;
                                        ?>
                                
                                        <div class="blog-item <?php echo $offset == 1 ? 'big': '';?>">
                                            <div class="blog-item--thumb">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php if(in_array('pr_status', get_field('pr_icon'))):?>
                                                        <span class="blog-item--thumb-label">PR</span>
                                                    <?php endif;?>
                                                    <?php if ( has_post_thumbnail() ) : ?>
                                                        <?php the_post_thumbnail('medium_large'); ?>
                                                    <?php else:?>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-img.jpg" />
                                                    <?php endif; ?>
                                                </a>
                                            </div>
                                            <div class="blog-item--infor">
                                                <div class="blog-item--dateWrap">
                                                    <span class="date big"><?php the_time('Y.m.d'); ?> [<?php echo strtolower(get_day_txt(get_the_time('Y-m-d')))?>]</span>
                                                    <span class="label"><?php echo esc_html( $blog_terms[0]->name );?></span>
                                                </div>
                                                <h3 class="titleLv3 type2"><a href="<?php the_permalink(); ?>"><?php the_title()?></a></h3>
                                                <p class="desc2"><?php echo mb_substr(get_the_excerpt(), 0, 54); ?><a href="<?php the_permalink(); ?>" class="link-blog2">… [続きを読む]</a></p>
                                            </div>
                                        </div>
                                        
                                    <?php
                                    endwhile;
                                    wp_reset_postdata();
                                endif;
                                ?>                                                                
                            </div><!--End .blog-list-->                            
                            <?php simple_navigation($blog_query)?>
                        </div><!-- ./blog-cnt -->
                    </div><!-- ./section-recipe--row -->
                </div><!-- ./p-restaurant -->

            </div><!-- ./section-recipe--left -->
            <div class="section-recipe--right fadeup2">
                <div class="section-recipe--right-inner">
                    <?php get_sidebar(); ?>
                </div><!-- ./section-recipe--right -->
            </div>
        </div>
    </section><!--End .blog-->
    <?php get_footer('blog'); ?>
    <?php get_template_part( 'template-parts/fronts/advertising' ); ?>
</main>

<?php get_footer(); ?>

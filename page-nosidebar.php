<?php /* Template Name: サイドバーなし */ get_header(); ?>

<main class="main">
    <div class="breadcrumbWrap pc-only">
        <div class="container">
            <div class="breadcrumb">
                <?php wp_breadcrumb()?>
            </div>
        </div>
    </div><!--End .breadcrumbWrap-->
    <section class="section">
        <div class="container">
            <div class="sectionEP-head">
                <div class="sectionEP-titleWrap">
                    <h1 class="sectionEP-title no-icon"><?php the_title()?></h1>
                </div>
            </div>
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <div class="main-content no-reset">
                    <?php the_content(); ?>            
                </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </section>
</main>

<?php get_footer(); ?>
